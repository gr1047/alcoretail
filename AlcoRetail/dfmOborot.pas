unit dfmOborot;

interface

uses
  Forms, Grids, AdvObj, BaseGrid, AdvGrid, ComCtrls, Buttons, StdCtrls,
  Controls, CheckLst, Classes, ExtCtrls, SysUtils, Graphics, Windows, Menus,
  AdvUtil;

type
  TfmOborot = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Panel5: TPanel;
    Panel6: TPanel;
    dtpDEnd: TDateTimePicker;
    Panel7: TPanel;
    Label2: TLabel;
    Panel8: TPanel;
    Label1: TLabel;
    Panel9: TPanel;
    cbAllKvart: TComboBox;
    Panel10: TPanel;
    dtpDBeg: TDateTimePicker;
    Panel3: TPanel;
    SpeedButton5: TSpeedButton;
    pgc1: TPageControl;
    ts_EAN: TTabSheet;
    ts_KppEan: TTabSheet;
    Panel11: TPanel;
    SpeedButton1: TSpeedButton;
    SG_EAN: TAdvStringGrid;
    SG_EANKpp: TAdvStringGrid;
    Panel12: TPanel;
    Label6: TLabel;
    Button1: TButton;
    lblVer: TLabel;
    pnl1: TPanel;
    GB_Move: TGroupBox;
    pnl3: TPanel;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn5: TSpeedButton;
    pnl2: TPanel;
    chklstVisible: TCheckListBox;
    pnl4: TPanel;
    pnl5: TPanel;
    lb1: TLabel;
    pnl6: TPanel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    pm1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure dtpDEndChange(Sender: TObject);
    procedure dtpDBegChange(Sender: TObject);
    procedure chklstVisibleClickCheck(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbMouseEnter(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure pnl3Resize(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure rb2Click(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
    procedure FillOborot(dBeg, dEnd:TDateTime; dal:Boolean);
  public
    { Public declarations }
    procedure CreateOborot;
  end;

var
  fmOborot: TfmOborot;
  doBeg, doEnd:TDateTime;
  SLOb, SLObKpp:TSTringList;
implementation

Uses Global, GlobalUtils, GlobalConst, dfmWork, dfmMove, dfmReplace;
{$R *.dfm}

procedure TfmOborot.FillOborot(dBeg, dEnd:TDateTime; dal:Boolean);
begin
  FreeStringList(SLOb);
  FreeStringList(SLObKPP);

  SLOb:=TStringList.Create;
  SLObKpp:=TStringList.Create;

  // �������� ��� ������� ��� � ������� �������� ��� ���� �����������
  DvizenieEAN(SLOb, SLAllDecl, Int(doBeg), Int(doEnd));
  //���������� �����
  if dal then
    ShowDataOborotKpp(SG_EAN,SLOb,NameColOborotEanKpp,NoResColOborotKpp,CalcColOborotKpp, HideColOborot1, FilterColOborotKpp)
  else
    ShowDataOborotKpp(SG_EAN,SLOb,NameColOborotEanKpp,NoResColOborotKpp,CalcColOborotKpp, HideColOborot2, FilterColOborotKpp);
  if SLKpp.Count > 1 then
    begin
      ts_KppEan.TabVisible:= True;
      DvizenieKppEan(SLObKpp,SLAllDecl,doBeg, doEnd);
      //���������� �����
      if dal then
        ShowDataOborotKpp(SG_EANKPP,SLObKpp,NameColOborotEanKpp, NoResColOborotKpp,CalcColOborotKpp, HideColOborotKpp1, FilterColOborotKpp)
      else
        ShowDataOborotKpp(SG_EANKPP,SLObKpp,NameColOborotEanKpp, NoResColOborotKpp,CalcColOborotKpp, HideColOborotKpp2, FilterColOborotKpp);
    end
  else
    ts_KppEan.TabVisible:= False;
end;

procedure TfmOborot.btn1Click(Sender: TObject);
begin
  fmWork.sb_MoveProductionClick(Self);
  fmMove.pgc1.ActivePageIndex := 1;
end;

procedure TfmOborot.btn2Click(Sender: TObject);
begin
  fmWork.sb_MoveProductionClick(Self);
  fmMove.pgc1.ActivePageIndex := 2;
end;

procedure TfmOborot.btn3Click(Sender: TObject);
begin
  fmWork.sb_MoveProductionClick(Self);
  fmMove.pgc1.ActivePageIndex := 4;
end;

procedure TfmOborot.btn4Click(Sender: TObject);
begin
  fmWork.sb_MoveProductionClick(Self);
  fmMove.pgc1.ActivePageIndex := 3;
end;

procedure TfmOborot.btn5Click(Sender: TObject);
var fSG:TADVStringGrid;
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  case pgc1.ActivePageIndex of
    0: fSG := SG_EANKpp;
    1: fSG := SG_EAN
  end;
  fmReplace.SGFind := fSG;
  fmReplace.Show;
end;

procedure TfmOborot.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmOborot.cbAllKvartChange(Sender: TObject);
var
  s: string;
  cKvart, cYear: string;
begin
  s := '';
  rb2.Checked:=True;
  if (Sender is TComboBox) then
    if (Sender as TComboBox).ItemIndex <> -1 then
    begin
      s := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
    end;
  if Length(s) = 0 then exit;

  cKvart := Copy(s, 0, pos(nkv, s) - 2);
  cYear := Copy(s, Length(s) - 8, 4);
  doBeg := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
  doEnd := Kvart_End(StrToInt(cKvart), StrToInt(cYear));
  dtpDBeg.Date := doBeg;
  dtpDEnd.Date := doEnd;
  pgc1.Visible:=False;
  try
    FillOborot(doBeg, doEnd, rb2.Checked);
  finally
    pgc1.Visible:=True;
  end;

end;

procedure TfmOborot.chklstVisibleClickCheck(Sender: TObject);
var idx:integer;
    ch:Boolean;
begin
  idx:=(Sender as TCheckListBox).ItemIndex;
  ch :=not (Sender as TCheckListBox).Checked[idx];
  case idx of
    0:
      if ch then
        begin
          SG_Ean.HideColumns(6,8);
          SG_EanKpp.HideColumns(6,8);
        end
      else
        begin
          SG_Ean.UnHideColumns(6,8);
          SG_EanKpp.UnHideColumns(6,8);
        end;
    1:
    if ch then
        begin
          SG_Ean.HideColumns(9,11);
          SG_EanKpp.HideColumns(9,11);
        end
      else
        begin
          SG_Ean.UnHideColumns(9,11);
          SG_EanKpp.UnHideColumns(9,11);
        end;
  end;
end;

procedure TfmOborot.dtpDBegChange(Sender: TObject);
begin
  doBeg:=dtpDBeg.Date;
end;

procedure TfmOborot.dtpDEndChange(Sender: TObject);
begin
  doEnd:=dtpDEnd.Date;
end;

procedure TfmOborot.FormCreate(Sender: TObject);
begin
  SLOb := TSTringList.Create;
  SLObKpp := TSTringList.Create;

  CreateOborot;
end;

procedure TfmOborot.CreateOborot;
begin
  rb2.Checked := True;
  AssignCBSL(cbAllKvart,SLPerRegYear);
  cbAllKvart.ItemIndex := idxKvart;
  doBeg := Kvart_Beg(curKvart, NowYear);
  doEnd := Kvart_End(curKvart, NowYear);
  dtpDBeg.Date := doBeg;
  dtpDEnd.Date := doEnd;
  FillOborot(doBeg, doEnd, rb2.Checked);
end;
procedure TfmOborot.FormDestroy(Sender: TObject);
begin
  FreeStringList(SLOb);
  FreeStringList(SLObKpp);
end;

procedure TfmOborot.pnl3Resize(Sender: TObject);
  var wid:integer;
begin
  wid := Trunc(GB_Move.ClientWidth / 4);
  btn1.Width := wid;
  btn2.Width := wid;
  btn3.Width := wid;
  btn4.Width := wid;
end;

procedure TfmOborot.rb1Click(Sender: TObject);
var ch:Boolean;
begin
  ch:= (Sender as TRadioButton).Checked;
  if ch then
    begin
      SG_Ean.HideColumns(12,20);
      SG_EanKpp.HideColumns(12,20);

      SG_Ean.UnHideColumns(21,29);
      SG_EanKpp.UnHideColumns(21,29);

      SG_Ean.HideColumns(27,28);
      SG_EanKpp.HideColumns(27,28)
    end
  else
    begin
      SG_Ean.UnHideColumns(12,20);
      SG_EanKpp.UnHideColumns(12,20);

      SG_Ean.HideColumns(21,29);
      SG_EanKpp.HideColumns(21,29);

      SG_Ean.HideColumns(27,28);
      SG_EanKpp.HideColumns(27,28)
    end;
end;

procedure TfmOborot.rb2Click(Sender: TObject);
var ch:Boolean;
begin
  ch:= (Sender as TRadioButton).Checked;
  if ch then
    begin
      SG_Ean.HideColumns(21,29);
      SG_EanKpp.HideColumns(21,29);

      SG_Ean.UnHideColumns(12,20);
      SG_EanKpp.UnHideColumns(12,20);

      SG_Ean.HideColumns(18,19);
      SG_EanKpp.HideColumns(18,19);
    end
  else
    begin
      SG_Ean.UnHideColumns(21,29);
      SG_EanKpp.UnHideColumns(21,29);

      SG_Ean.HideColumns(12,20);
      SG_EanKpp.HideColumns(12,20);

      SG_Ean.HideColumns(18,19);
      SG_EanKpp.HideColumns(18,19)
    end;
end;

procedure TfmOborot.SpeedButton1Click(Sender: TObject);
begin
  doBeg := dtpDBeg.Date;
  doEnd := dtpDEnd.Date;
  FillOborot(doBeg, doEnd, rb2.Checked);
  rb2.Checked:=True;
end;

procedure TfmOborot.SpeedButton5Click(Sender: TObject);
begin
  if fmWork.dlgSave_Export.Execute then
    begin
      if pgc1.ActivePageIndex = 0 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_EANKpp,12);
      if pgc1.ActivePageIndex = 1 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_EAN,12);
    end;
end;

procedure TfmOborot.lbMouseEnter(Sender: TObject);
begin
  (Sender as TLabel).Font.Color := clRed;
end;

procedure TfmOborot.N1Click(Sender: TObject);
var ACol, ARow:integer;
  s:string;
  fSG:TAdvStringGrid;
begin
  case pgc1.ActivePageIndex of
    0: fSG := SG_EANKpp;
    1: fSG := SG_EAN
  end;
  ACol:=fSG.Col;
  ARow:=fSG.Row;
  s:=fSG.Cells[ACol,ARow];
  fSG.FilterActive := False;
  fSG.Filter.ColumnFilter[ACol].Condition := s;
  fSG.FilterActive := True;
  fSG.ApplyFilter;
  fSG.RemoveLastFilter;
end;

end.
