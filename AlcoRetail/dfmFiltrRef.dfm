object fmFiltrRef: TfmFiltrRef
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1060#1080#1083#1100#1090#1088' "'#1057#1055#1056#1040#1042#1054#1063#1053#1048#1050'"'
  ClientHeight = 135
  ClientWidth = 641
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PrintScale = poNone
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 97
    Width = 641
    Height = 38
    Align = alBottom
    TabOrder = 0
    object btCancel: TButton
      Left = 392
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 0
    end
    object btOK: TButton
      Left = 311
      Top = 6
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 1
      OnClick = btOKClick
    end
  end
  object nbFiltr: TNotebook
    Left = 0
    Top = 0
    Width = 641
    Height = 81
    Align = alTop
    TabOrder = 1
    object TPage
      Left = 0
      Top = 0
      Caption = 'Prod'
      object pProduct: TPanel
        Left = 0
        Top = 0
        Width = 641
        Height = 81
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 121
          Top = 42
          Width = 41
          Height = 16
          Caption = #1050#1086#1076' '#1040#1055
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object leProdName: TLabeledEdit
          Left = 168
          Top = 9
          Width = 457
          Height = 24
          EditLabel.Width = 151
          EditLabel.Height = 16
          EditLabel.BiDiMode = bdRightToLeft
          EditLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
          EditLabel.Font.Charset = DEFAULT_CHARSET
          EditLabel.Font.Color = clWindowText
          EditLabel.Font.Height = -13
          EditLabel.Font.Name = 'Tahoma'
          EditLabel.Font.Style = []
          EditLabel.ParentBiDiMode = False
          EditLabel.ParentFont = False
          EditLabel.Layout = tlCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          LabelPosition = lpLeft
          ParentFont = False
          TabOrder = 0
        end
        object cbFNS: TComboBox
          Left = 168
          Top = 39
          Width = 457
          Height = 24
          Style = csDropDownList
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Saler'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 641
        Height = 81
        Align = alClient
        TabOrder = 0
        object leSalName: TLabeledEdit
          Left = 170
          Top = 29
          Width = 455
          Height = 24
          EditLabel.Width = 161
          EditLabel.Height = 16
          EditLabel.BiDiMode = bdRightToLeft
          EditLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
          EditLabel.Font.Charset = DEFAULT_CHARSET
          EditLabel.Font.Color = clWindowText
          EditLabel.Font.Height = -13
          EditLabel.Font.Name = 'Tahoma'
          EditLabel.Font.Style = []
          EditLabel.ParentBiDiMode = False
          EditLabel.ParentFont = False
          EditLabel.Layout = tlCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          LabelPosition = lpLeft
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
  end
  object pb_filtr: TProgressBar
    Left = 0
    Top = 80
    Width = 641
    Height = 17
    Align = alBottom
    TabOrder = 2
    Visible = False
  end
end
