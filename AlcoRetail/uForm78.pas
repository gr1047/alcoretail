unit uForm78;

interface

uses
  Windows, Winapi.ActiveX, Vcl.StdCtrls, System.Classes, GlobalConst, Data.Win.ADODB,
  Xml.XMLIntf, System.Math, XSDValidator, wcrypt2, FWZipWriter;
 type
      tToXml = record
        Path, exe_version_, NameProg : String;
        SLOstatkiKpp, SLAllDecl_, SLOborotKpp_, slkpp_ : TStringList;
        que2_sql_, que1_sql_ : TADOQuery;
        cbAllKvart : TComboBox;
        dB, dE : TDateTime;
        f11, rbf43, chk_1C, rbKor, findecp, chk_declBM : boolean;
        CiKvart, CiYear, seKor,CurKvart, CurYear: integer;
        forg : TOrganization;
        NowYear_ : Word;
        fSert_: TSert;
      End;


   tForm7_8=class
     Src : tToXml;
     memoErr: TStringList;
     masErr: array [0 .. 7] of integer;
     RezInf : String;
     MemoStr : String;
      fSert: TSert;
     Procedure toXml(Source : tToXml);
     procedure GetMinusOst(var sErr:array of integer; SLOst:TStringList);
     function IsMassErr:Boolean;
     procedure ChangePKProducer(DBQ:TADOQuery; PK:string);
     procedure CleanRepProducer(DBQ:TADOQuery);
     procedure GetDataDecl(var SL, SLErr: TStringList; dB, dE: TDateTime);
     procedure SortSLCol(cols: integer; SL: TStringList; aCompare: TStringListSortCompare = nil);
     function NoErrRowDel(decl: TDeclaration; var SLErr:TStringList): Boolean;
     procedure GetAllProizv(SL: TStringList; var SLPr: TStringList; f11:Boolean);
     function InDecl(grup: string; form11: Boolean): Boolean;
     function CompNumAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
     procedure GetAllSaler(SL: TStringList; var SLPost, SalErr: TStringList; f11:Boolean);
     function IsErrLic(SL:TStringList):Boolean;
     procedure SHapkaXML(node: IXMLNode; kv, yr, kor: integer; f11, kor1, f43: Boolean);
     procedure ProizXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
     procedure PostXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
     procedure XMLPost(node: IXMLNode; f11, af: Boolean; sal: TSaler_Obj);
     function ValidOrgname(s: string): Boolean;
     function ATrim(s: string): string;
     function Empty(s: string): boolean;
     function GetKPPAdres(DBQ: TADOQuery; kpp: string; var adr: TOrgAdres): string;
     procedure OrgXML(node: IXMLNode; f11, fix, f43: Boolean; d1, d2: TDateTime);
     function UseStr(s, error, use: string): string;
     procedure OrgRecXML(node: IXMLNode; f11, fix: Boolean; org: TOrganization);
     function InnUL(s: string): Boolean;
     procedure OrgOtvXML(node: IXMLNode; buh: TBuh; ruk: TRuk);
     procedure OrgLicXML(org: TOrganization; node: IXMLNode; d1, d2: TDateTime; f43: Boolean);
     function ValidLic(ser, num: string): Boolean;
     function Valid_SerLic(s: string): Boolean;
     function Valid_NumLic(s: string): Boolean;
     procedure ClearSL(SL: TStringList);
     procedure GetDataKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure GetDataOKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure KPPXML(node: IXMLNode; selfkpp: string; SL,SLO, SLErr: TStringList; f11, af: Boolean; d1, d2: TDateTime; OneC:Boolean);
     function OstKPPXML(SL: TStringList; f11: Boolean; kpp: string; Data: TDateTime): Extended;
     function OkruglDataDecl(val: Extended):Extended;
     function RashKPPXML(SL: TStringList; f11: Boolean; kpp: string;Data: TDateTime): Boolean;
     function Kvart_Start(data: TDateTime): TDate;
     function Kvart_Beg(kv: Integer; year: Integer): TDateTime;
     function Kvart_Finish(data: TDateTime): TDate;
     function Kvart_End(kv: Integer; year: Integer): TDateTime;
     procedure ShapkaKPPXML(node: IXMLNode; f11, af, ob: Boolean; selfkpp: string; OneC:Boolean);
     function Period_Otch(Kvartal: Integer): Integer;
     procedure XMLPoizvImp(node: IXMLNode; f11, af: Boolean;pr: TProducer_Obj);
     procedure OborotKppXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean; d1, d2: TDateTime);
     procedure GetDataGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure GetDataOGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure SvedPrImpXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean;d1, d2: TDateTime);
     procedure GetDataProizvXML(s: string; SL: TStringList;var SLFiltr: TStringList);
     procedure SvedPrImpPostXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime; f11: Boolean);
     function DvizPostXML(node: IXMLNode; SL, SLErr: TStringList; f11: Boolean; d1, d2: TDateTime): Boolean;
     function DBetween(d, d1, d2: TDateTime): boolean;
     procedure GetDataSalXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure FreeStringList(var SL: TStringList);
     procedure PostProdXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime);
     procedure GetDataXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     procedure PostProdDataNacl(node: IXMLNode; SL: TStringList);
     procedure GetDataNaclXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     function OborotDate(d: TDateTime; SL: TStringList): Extended;
     procedure DvizenieOneEAN(SL: TStringList; d1, d2: TDateTime; var dviz: TMove);
     procedure NullDecl(var decl: TDeclaration);
     procedure NullProd(var prod:TProduction);
     procedure NullSaler(var sal: TSaler);
     function IsNotNullDecl(dM: TMove): Boolean;
     function GenErr(dv:TMove):string;
     function MinusMove(dv: TMove): Boolean;
     procedure GetDataOProizvXML(s: string; SL: TStringList; var SLFiltr: TStringList);
     function CreateGuid: string;
     function GetCurDate(kv: integer): TDateTime;
     function FindErrXSD(fXML: string; f11, f43: Boolean;  var sErr: string): Boolean;
     function WinTemp: string;
     function WriteXML(fn: string): Boolean;
     function GetSert(fSert: TSert; var CertContext: PCCERT_CONTEXT): Boolean;
     function AddECP(fXML: string; CertContext: PCCERT_CONTEXT): Boolean;
     procedure SignXNL(Var ms, ms1: TMemoryStream; Var err: Boolean; ECP: string; CertContext: PCCERT_CONTEXT);
     procedure Show_Inf(inf : String);
     function ZipFileXML(fn: string): Boolean;
     function CryptoXML(fn: string): Boolean;
     procedure Crypt(CountS: integer; ArrayCert: array of PCCERT_CONTEXT; Var ms, ms1: TMemoryStream; Var err: Boolean); { ���� "�����������" }
     function WriteDeclBM(sfile, Inn:string; iKvart, iYear:integer):Boolean;
     function GetPasword(RegHost,sInn:string):string;
     procedure MemoAdd(Str: String);
     function ZipFileNew(fn: string): Boolean;
     Constructor Create;
   end;

implementation

uses
  Winapi.ShlObj, System.SysUtils, Xml.XMLDoc, Vcl.Forms, Vcl.Controls, IdFTP,
  IdTCPClient, IdFTPCommon, IdFTPList;

Constructor tForm7_8.Create;
Begin
   src.SLOstatkiKpp := TStringList.Create;
   Src.SLAllDecl_:=  TStringList.Create;
   Src.SLOborotKpp_  := TStringList.Create;
   Src.slkpp_  := TStringList.Create;
   Src.que2_sql_:= TAdoQuery.Create(Nil);
   Src.que1_sql_ := TADOQuery.Create(nil);
   src.cbAllKvart := TComboBox.Create(Nil);
   MemoErr := TStringList.Create;
End;

//������� ��������� ��� ���������� �� �������� ��������� �� �����������.
function CompNumAsc1(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : Extended;
begin
  N1 := StrToFloatDef(aSl[aIndex1], 0);
  N2 := StrToFloatDef(aSl[aIndex2], 0);
  if N1 < N2 then Result := -1
  else if N1 > N2 then Result := 1
  else Result := 0;
end;


procedure tForm7_8.MemoAdd(Str: String);
Begin
  MemoStr := MemoStr+Str;
End;

procedure tForm7_8.Show_Inf(inf : String);
 Begin
   RezInf := RezInf + Inf;
 End;

function tForm7_8.GetCurDate(kv: integer): TDateTime;
var
  cYear, cMonth, cDay: word;
begin
  DecodeDate(Now(), cYear, cMonth, cDay);
  if cYear = Src.NowYear_ then
    Result := Now
  else
    Result := Kvart_End(kv, Src.NowYear_);
end;

function tForm7_8.CreateGuid: string;
var
  id: TGUID;
begin
  Result := '';
  if CoCreateGuid(id) = S_OK then
    Result := GUIDToString(id);
end;

function tForm7_8.IsMassErr:Boolean;
begin
  Result:=False;
  if (masErr[0] > 0) or (masErr[1] > 0) or (masErr[2] > 0) or (masErr[3] > 0) or
     (masErr[4] > 0) or (masErr[5] > 0) or (masErr[6] > 0)  then
  begin
   {
    Show_Inf
      ('� ����� �������� ��������� ���������� ������!!!' +  #10#13 +
       '��������� �� ����� ������������� ����������', fError);
      }
    Result :=True;
    { fmWork.sb_MainClick(Application); }
  end;
end;

procedure tForm7_8.GetMinusOst(var sErr:array of integer; SLOst:TStringList);
    var i:integer;
      mov:TMove;
 begin
  sErr[5]:=0;
  sErr[6]:=0;
  for i := 0 to SLOst.Count - 1 do
   begin
    mov:=TMove(SLOst.Objects[i]);
    if mov.dv.ost0 < 0 then
    Inc(sErr[5]);
    if mov.dv.ost1 < 0 then
    Inc(sErr[6]);
   end;
end;


procedure tForm7_8.ChangePKProducer(DBQ:TADOQuery; PK:string);
var qr:TADOQuery;
  maxPK:Int64;
  sInn, sKpp:string;
  i, idx:Integer;
  SL:TStringList;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  SL:=TStringList.Create;
  try
    qr.Connection := DBQ.Connection;
    qr.SQL.Text := 'Select MAX(PK) from Producer';
    qr.Open;
    maxPK := qr.Fields[0].AsInteger + 1;

    qr.SQL.Text := 'Select * from Producer WHERE PK = ' + PK;
    qr.Open;
    if not qr.Eof then
      if qr.RecordCount > 1 then
      begin
        qr.First;
        while not qr.Eof  do
        begin
          sInn := qr.FieldByName('INN').AsString;
          sKpp := qr.FieldByName('KPP').AsString;
          SL.Add(sInn+'-'+sKpp);
          qr.Next;
        end;
      end;
    for i := 1 to SL.Count - 1 do
    begin
      idx := Pos('-',SL.Strings[i]);
      sInn := Copy(SL.Strings[i],0,idx-1);
      sKpp := Copy(SL.Strings[i],idx+1);
      qr.SQL.Text := 'UPDATE PRODUCER' + #13#10 +
            'Set PK = ' + IntToStr(maxPK) + #13#10 +
            'WHERE INN=''' + sINN + ''' and KPP=''' + sKPP + '''';
      qr.ExecSQL;
      Inc(maxPK);
    end;
  finally
    SL.Free;
    qr.Free;
  end;
end;


procedure tForm7_8.CleanRepProducer(DBQ:TADOQuery);
var qr:TADOQuery;
  SLPK:TStringList;
  i:Integer;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  SLPK:=TStringList.Create;
  try
    qr.Connection := DBQ.Connection;
    //������ �� ������������� �������
    qr.SQL.Text := 'SELECT PK FROM Producer GROUP BY PK HAVING (((Count(PK))>1)) order by PK';
    qr.Open;
    if not qr.Eof then
      while not qr.Eof do
      begin
        SLPK.Add(qr.FieldByName('PK').AsString);
        qr.Next;
      end;
  finally
    qr.Free;
  end;

  try
    for i := 0 to SLPK.Count - 1 do
      ChangePKProducer(DBQ,SLPK.Strings[i]);
  finally
    SLPK.Free;
  end;

end;


procedure tForm7_8.SortSLCol(cols: integer; SL: TStringList;
  aCompare: TStringListSortCompare = nil);
var
  SlSort: TStringList;
  decl: TDecl_obj;
  i: integer;
begin
  // ����������� ������.
  SlSort := TStringList.Create;

  // ��������� � ����������� ������ ����: "������ - ������".
  // � �������� ������ ����� ���������� �������� ����� ����
  // �������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
  // �� ����������� ������������� ������� - ����� �� ����������� ����������
  // ����� �������, ���� ��� ����.
  // � � �������� ������� ����� ������������ ����� ��������������� ����� �������.
  for i := 0 to SL.Count - 1 do
  begin
    // ������ ��������� ��� ����� ������ �������.
    decl := Pointer(SL.Objects[i]);
    // ��������� � ����������� ������ ����:
    // ������: ������ �� ������ �������� �������;
    // ������: ���������, ���������� ����� ������ �������.
    case cols of
      1:
        SlSort.AddObject(String(decl.data.PK), decl);
      2:
        SlSort.AddObject(String(decl.data.SelfKPP), decl);
      3:
        SlSort.AddObject(String(decl.data.product.EAN13), decl);
      4:
        SlSort.AddObject(String(decl.data.sal.sINN + decl.data.sal.sKpp),
          decl);
      5:
        SlSort.AddObject(String(decl.data.sal.sKpp), decl);
      6:
        SlSort.AddObject(String(decl.data.product.FNSCode), decl);
      7:
        SlSort.AddObject(DateTimeToStr(decl.data.DeclDate), decl);
      8:
        SlSort.AddObject(String(decl.data.product.prod.pId), decl);
    end;
  end;

  // ��������� �������.
  if Assigned(aCompare) then
    SlSort.CustomSort(aCompare)
  else
    SlSort.Sort;

  SL.Clear;
  SL.Assign(SlSort);

  // ���������� ����������� ������.
  FreeAndNil(SlSort);
end;


function tForm7_8.NoErrRowDel(decl: TDeclaration; var SLErr:TStringList): Boolean;
var s:string;
begin
  Result := false;
  s := '';
  if (decl.RepDate >=Src.dB) and (decl.RepDate <= Src.dE) then
    begin
      if decl.product.prod.pName = NoProducer then
      begin
        Result := True;
        s:= deStart + deProd + ' ��� ' + String(decl.product.prod.pInn) + ' ��� ' +
            String(decl.product.prod.pKpp) + deNone + deRef;
        exit;
      end;
      if decl.product.Productname = NoProd then
      begin
        Result := True;
        s:= deStart + deProduction + ' EAN13 ' + String(decl.product.EAN13) + deNone + deRef;
        if SLErr.IndexOf(s) = -1 then
          SLErr.Add(s);

        exit;
      end;
      if decl.TypeDecl = 1 then
      begin
        if decl.sal.sName = NoSal then
        begin
          Result := True;
          s:= deStart + deProd + ' ��� ' + String(decl.sal.sInn) + ' ��� ' +
            String(decl.sal.sKpp) + deNone + deRef;
          if SLErr.IndexOf(s) = -1 then
            SLErr.Add(s);
          exit;
        end;
       if (decl.sal.lic.dEnd < Src.dB) and (decl.sal.sName <> NoSal)  then
        begin
          s:= deStart + deSal + ' ��� ' + String(decl.sal.sInn) + ' ��� ' +
            String(decl.sal.sKpp) + ' ' + String(decl.sal.sName) + ' ' +
            deNone + deLic + ' (' + DateToStr(decl.sal.lic.dEnd) + ')';
          Result := True;
          if SLErr.IndexOf(s) = -1 then
            SLErr.Add(s);
          exit;
        end;
      end;
    end;
end;

procedure tForm7_8.GetDataDecl(var SL, SLErr: TStringList; dB, dE: TDateTime);
var
  i: integer;
  decl: TDeclaration;
begin
  SortSLCol(1,Src.SLAllDecl_);
  for i := 0 to Src.SLAllDecl_.Count - 1 do
  begin
    decl:=TDecl_obj(Src.SLAllDecl_.Objects[i]).Data;
    if (decl.RepDate <= dE)  then
      if not NoErrRowDel(decl, SLErr) then
        begin
          SL.AddObject(Src.SLAllDecl_.Strings[i],TDecl_Obj.Create);
          TDecl_Obj(SL.Objects[SL.Count - 1]).data := decl;
        end;
  end;
end;


function tForm7_8.InDecl(grup: string; form11: Boolean): Boolean;
begin
  InDecl := False;
  if (grup = '261') or (grup = '262') or (grup = '263') then
  begin
    if form11 then
      InDecl := False
    else
      InDecl := True;
    Exit;
  end;

  if not form11 and ((grup = '261') or (grup = '262') or (grup = '263')) then
  begin
    InDecl := True;
    exit;
  end;
  if form11 and ((grup > '000') and (grup < '500')) then
    InDecl := True;
  if not form11 and ((grup >= '500') and (grup <= '999')) then
    InDecl := True;
end;


//������� ��������� ��� ���������� �� �������� ��������� �� �����������.
function tForm7_8.CompNumAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : Extended;
begin
  N1 := StrToFloatDef(aSl[aIndex1], 0);
  N2 := StrToFloatDef(aSl[aIndex2], 0);
  if N1 < N2 then Result := -1
  else if N1 > N2 then Result := 1
  else Result := 0;
end;


procedure tForm7_8.GetAllSaler(SL: TStringList; var SLPost, SalErr: TStringList; f11:Boolean);
var
  i: integer;
  decl: TDeclaration;
  fd:integer;
begin
  if f11 then fd := 11 else fd := 12;

  SalErr.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    //decl.forma := FormDecl(decl.product.FNSCode);
    if (decl.TypeDecl in [1,3]) and (decl.forma = fd) then
      if (decl.sal.sName <> NoSal) and (decl.sal.sInn <> '1234567894') then
        if InDecl(String(decl.product.FNSCode), f11) then
          if SLPost.IndexOf(String(decl.sal.sId)) = -1 then
          begin
            SLPost.AddObject(string(decl.sal.sId),TSaler_Obj.Create);
            TSaler_Obj(SLPost.Objects[SLPost.Count - 1]).data := decl.sal;
          end;
  end;
  if SLPost.Count > 0 then
    begin
     SLPost.CustomSort(CompNumAsc1);
    end;
end;


function tForm7_8.IsErrLic(SL:TStringList):Boolean;
var i:Integer;
begin
  Result := False;
  if Sl.Count > 0 then
  begin
    MemoErr.Add('������ ���������� �� ������ � ����������, ��� ������ � ��������');
    for i := 0 to SL.Count - 1 do
     MemoErr.Add(SL.Strings[i]);
      MemoErr.Add('');
    Result:=True;
  end;
end;


function tForm7_8.Period_Otch(Kvartal: Integer): Integer;
var
  p_ot: Integer;
begin
  p_ot := 0;
  case Kvartal of
    1:
      p_ot := 03;
    2:
      p_ot := 06;
    3:
      p_ot := 09;
    4:
      p_ot := 00;
  end;
  Period_Otch := p_ot;
end;

procedure tForm7_8.SHapkaXML(node: IXMLNode; kv, yr, kor: integer; f11, kor1, f43: Boolean);
var
  s: string;
begin
  with node do
  begin
    Attributes['�������'] := DateToStr(Date());
    if f43 then
      Attributes['��������'] := '4.4'
    else
     Attributes['��������'] := '4.4';
{     Attributes['��������'] := '4.20'; }
    Attributes['��������'] := '���������� ������ ' + Src.exe_version_;
    with AddChild('��������') do
    begin
      if f11 then
        s := '37'
      else
        s := '38';
      if not f43 then
        s := s + '-�';

      Attributes['�������'] := s;
      Attributes['�������������'] := IntToStr(Period_Otch(kv));
      if not f43 then
        Attributes['��������'] := '4';
      Attributes['������������'] := IntToStr(yr);

      if not kor1 then
        ChildValues['���������'] := ''
      else
        with AddChild('��������������') do
          Attributes['���������'] := IntToStr(kor);
    end;
  end;
end;

// ������������ ������ �������������� ����������� � ����������
procedure tForm7_8.GetAllProizv(SL: TStringList; var SLPr: TStringList; f11:Boolean);
var
  i: integer;
  mv: TMove;
begin
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if InDecl(string(mv.product.FNSCode),f11) then
      if mv.product.prod.pName <> NoProducer then
        if SLPr.IndexOf(String(mv.product.prod.pId)) = -1 then
        begin
          SLPr.AddObject(string(mv.product.prod.pId),TProducer_Obj.Create);
          TProducer_Obj(SLPr.Objects[SLPr.Count - 1]).data := mv.product.prod;
        end;
  end;
  if SLPr.Count > 0 then
    SLPr.CustomSort(CompNumAsc1);
end;


procedure tForm7_8.XMLPoizvImp(node: IXMLNode; f11, af: Boolean; pr: TProducer_Obj);
begin
  if af and not ValidOrgname(String(pr.data.pName)) then
    pr.data.pName := '��� "�������"';
  with node do
  begin
    Attributes['�����������'] := pr.data.pId;
    Attributes['�000000000004'] := pr.data.pName;
    if f11 then
    begin
      if ((pr.data.pKPP = NullKPP9) or (pr.data.pKPP = NullKPP8)) then
      begin
        if pr.data.pKPP = NullKPP8 then
          Attributes['�000000000005'] := Copy(pr.data.pInn,3,Length(pr.data.pInn)-2);
        if pr.data.pKPP = NullKPP9 then
          Attributes['�000000000005'] := Copy(pr.data.pInn,2,Length(pr.data.pInn)-1);
      end
      else
      begin
        if ((pr.data.pKPP = NullKPP7) or (pr.data.pKPP = NullKPP7)) then
        begin
          Attributes['�000000000005'] := '';
          Attributes['�000000000006'] := '';
        end
        else
        begin
          Attributes['�000000000005'] := pr.data.pInn;
          Attributes['�000000000006'] := pr.data.pKpp ;
        end;
      end;
    end
    else
    begin
      if Length(pr.data.pINN) = 12 then
        with AddChild('��') do
        begin
          Attributes['�000000000005'] := pr.data.pINN;
        end
        else
          with AddChild('��') do
          begin
            Attributes['�000000000005'] := pr.data.pINN;
            Attributes['�000000000006'] := pr.data.pKPP;
          end;
    end;
  end;
end;



procedure tForm7_8.ProizXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
var
  i: integer;
  PrImp: IXMLNode;
  pr: TProducer_Obj;
begin
  for i := 0 to SL.Count - 1 do
  begin
    pr := Pointer(SL.Objects[i]);
    if pr <> nil then
      if pr is TProducer_Obj then
      begin
        PrImp := node.AddChild('����������������������');
        with PrImp do
          XMLPoizvImp(PrImp, f11, fix, pr);
      end;
  end;
end;


function tForm7_8.ValidOrgname(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) > 3 then
    rez := True
  else
    rez := False;
  ValidOrgname := rez;
end;


function tForm7_8.ATrim(s: string): string;
var
  i: Byte;
  res: string;
begin
  s := ' ' + s + ' ';
  res := '';
  for i := 2 to Length(s) - 1 do
    if ((s[i] <> ' ') or ((s[i] = ' ') and (s[i - 1] <> ' ') and
          (i < Length(s) - 1))) then
      res := res + s[i];
  if res <> '' then
  begin
    while (Length(res) <> 1) and (res[Length(res)] = ' ') do
      SetLength(res, Length(res) - 1);
    if res = ' ' then
      res := '';
  end;
  ATrim := res;
end;

function tForm7_8.Empty(s: string): boolean;
begin
  if ATrim(s) = '' then
    Empty := true
  else
    Empty := false;
end;

procedure tForm7_8.XMLPost(node: IXMLNode; f11, af: Boolean; sal: TSaler_Obj);
begin
  try
    try
      with node do
      begin
        if af and not ValidOrgname(String(sal.data.sName)) then
          sal.data.sName := '��� "�������"';

        if not Empty(String(sal.data.sName)) then
        begin
          Attributes['��������'] := sal.data.sId;
          Attributes['�000000000007'] := sal.data.sName;

          if InnUL(String(sal.data.sINN)) then
          begin
            with AddChild('��') do
            begin
              Attributes['�000000000009'] := sal.data.sINN;
              Attributes['�000000000010'] := sal.data.sKpp;
            end
          end
          else
            with AddChild('��') do
              Attributes['�000000000009'] := sal.data.sINN;
        end;
      end;
    except
    end;
  finally
  end;
end;


procedure tForm7_8.PostXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
var
  i: integer;
  Post: IXMLNode;
  sal: TSaler_Obj;
begin
  for i := 0 to SL.Count - 1 do
  begin
    sal := Pointer(SL.Objects[i]);
    if sal <> nil then
      if sal is TSaler_Obj then
      begin
        Post := node.AddChild('����������');
        with Post do
          XMLPost(Post, f11, fix, sal);
      end;
  end;
end;


function tForm7_8.GetKPPAdres(DBQ: TADOQuery; kpp: string; var adr: TOrgAdres): string;
var
  sql: TADOQuery;
begin
  GetKPPAdres := '';

  with adr do
  begin
    kpp := '';
    name := '';
    kod_str := '';
    postindex := '';
    kod_reg := '';
    raion := '';
    gorod := '';
    naspunkt := '';
    ulica := '';
    dom := '';
    korpus := '';
    litera := '';
    kvart := '';
  end;
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text := 'SELECT top 1 * ' + #13#10 + 'FROM self_adres' +
        #13#10 + 'WHERE KPP=''' + kpp + ''' order by KPP';
      sql.Open;
      if not sql.Eof then
        with adr do
        begin
          sql.First;
          kpp := ShortString(sql.FieldByName('KPP').AsString);
          name := sql.FieldByName('namename').AsString;
          kod_str := ShortString(sql.FieldByName('Countrycode').AsString);
          postindex := ShortString(sql.FieldByName('IndexP').AsString);
          kod_reg := ShortString(sql.FieldByName('Codereg').AsString);
          raion := sql.FieldByName('Raipo').AsString;
          gorod := sql.FieldByName('City').AsString;
          naspunkt := sql.FieldByName('NasPunkt').AsString;
          ulica := sql.FieldByName('Street').AsString;
          dom := ShortString(sql.FieldByName('House').AsString);
          korpus := ShortString(sql.FieldByName('Struct').AsString);
          litera := ShortString(sql.FieldByName('ABC').AsString);
          kvart := ShortString(sql.FieldByName('Quart').AsString);
        end;
      GetKPPAdres := sql.FieldByName('namename').AsString;
    except
      GetKPPAdres := 'Null';
    end;
  finally
    sql.free;
  end;
end;


function tForm7_8.UseStr(s, error, use: string): string;
begin
  if (error = '') and Empty(s) then
    s := use
  else if pos(error, s) > 0 then
    s := use;
  UseStr := s;
end;


function tForm7_8.InnUL(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) = 10 then
    rez := True
  else
    rez := False;
  InnUL := rez;
end;


procedure tForm7_8.OrgRecXML(node: IXMLNode; f11, fix: Boolean; org: TOrganization);
begin
  with org do
    with node do
      with AddChild('���������') do
      begin
        with adres do
          with AddChild('������') do
          begin
            AddChild('���������').Text := '643';

            if not Empty(String(postindex)) then
              AddChild('������').Text := String(postindex)
            else
              AddChild('������');

            if not Empty(String(kod_reg)) then
              AddChild('���������').Text := String(kod_reg)
            else
              AddChild('���������');

            if not Empty(raion) then
              AddChild('�����').Text := raion
            else
              AddChild('�����');

            if not Empty(gorod) then
              AddChild('�����').Text := gorod
            else
              AddChild('�����');

            if not Empty(naspunkt) then
              AddChild('����������').Text := naspunkt
            else
              AddChild('����������');

            if not Empty(ulica) then
              AddChild('�����').Text := ulica
            else
              AddChild('�����');

            if not Empty(String(dom)) then
              AddChild('���').Text := String(dom)
            else
              AddChild('���');

            if not Empty(String(korpus)) then
              AddChild('������').Text := String(korpus)
            else
              AddChild('������');

            if not Empty(String(litera)) then
              AddChild('������').Text := String(litera)
            else
              AddChild('������');

            if not Empty(String(kvart)) then
              AddChild('�����').Text := String(kvart)
            else
              AddChild('�����');
          end;

        if InnUL(inn) then
          with AddChild('��') do
            begin
              Attributes['�����'] := inn;
              Attributes['�����'] := Kpp0;
            end
        else
          AddChild('��').Attributes['�����'] := inn;

        Attributes['����'] := SelfName;
        Attributes['������'] := NPhone;
        Attributes['Email����'] := Email;
      end;
end;


procedure tForm7_8.OrgOtvXML(node: IXMLNode; buh: TBuh; ruk: TRuk);
begin
  with node do
    with AddChild('���������') do
    begin
      with AddChild('������������') do
        with ruk do
        begin
          AddChild('�������').Text := RukF;
          AddChild('���').Text := RukI;
          AddChild('��������').Text := RukO;
        end;
      with AddChild('�������') do
        with buh do
        begin
          AddChild('�������').Text := BuhF;
          AddChild('���').Text := BuhI;
          AddChild('��������').Text := BuhO;
        end;
    end;
end;


function tForm7_8.Valid_SerLic(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) > 0 then
    rez := True
  else
    rez := False;
  if rez then
    if pos('*', s) > 0 then
      rez := False;
  Valid_SerLic := rez;
end;


function tForm7_8.Valid_NumLic(s: string): Boolean;
var
  rez: Boolean;
  i: integer;
begin
  if Length(s) > 3 then
    rez := True
  else
    rez := False;
  if rez then
    for i := 1 to Length(s) do
      if not(s[i] in digits) then
        rez := False;
  Valid_NumLic := rez;
end;

function tForm7_8.ValidLic(ser, num: string): Boolean;
var
  rez: Boolean;
begin
  if Valid_SerLic(ser) and Valid_NumLic(num) then
    rez := True
  else
    rez := False;
  ValidLic := rez;
end;

procedure tForm7_8.OrgLicXML(org: TOrganization; node: IXMLNode; d1, d2: TDateTime; f43: Boolean);
var
  DBQ: TADOQuery;
  StrSQL: string;
  atr: string;
begin
  DBQ := TADOQuery.Create(nil);

  try
    try
      if (Src.fOrg.lic.SerLic = '') or (Src.fOrg.lic.NumLic = '') or
        (Src.fOrg.lic.BegLic = 0) or (Src.fOrg.lic.EndLic = 0) then
      begin
        DBQ := Src.que1_sql_;
        DBQ.Close;
        DBQ.ParamCheck := False;
        DBQ.sql.Clear;
        StrSQL := 'select distinct * from self_license';
        DBQ.sql.Text := StrSQL;
        DBQ.Open;
        DBQ.First;
        while not DBQ.Eof do
        begin
          if ValidLic(DBQ.FieldByName('ser').AsString,
            DBQ.FieldByName('num').AsString) then
          begin
            Src.fOrg.lic.SerLic := DBQ.FieldByName('ser').AsString;
            Src.fOrg.lic.NumLic := DBQ.FieldByName('num').AsString;
            Src.fOrg.lic.BegLic := DBQ.FieldByName('BegLic').AsDateTime;
            Src.fOrg.lic.EndLic := DBQ.FieldByName('EndLic').AsDateTime;
          end;
          DBQ.Next;
        end
      end;

      if (Src.fOrg.lic.SerLic = '') or (Src.fOrg.lic.NumLic = '') or
        (Src.fOrg.lic.BegLic = 0) or (Src.fOrg.lic.EndLic = 0) then
      begin
        Src.fOrg.lic.SerLic := '�';
        Src.fOrg.lic.NumLic := '000000';
        Src.fOrg.lic.BegLic := d1;
        Src.fOrg.lic.EndLic := d2;
      end;
      with node do
        with AddChild('������������') do
          if InnUL(org.inn) then
            with AddChild('�������������') do
              with AddChild('��������') do
              begin
                AddChild('�������').Text := '06';
                AddChild('���������').Text :=
                  Src.fOrg.lic.SerLic + ' ' + Src.fOrg.lic.NumLic;
                AddChild('����������').Text := DateToStr
                  (Src.fOrg.lic.BegLic);
                AddChild('�����������').Text := DateToStr
                  (Src.fOrg.lic.EndLic);
              end
          else
            with AddChild('���������������') do
              AddChild('�������').Text := '12';
    except
    end;
  finally
    DBQ.free;
  end;
end;


procedure tForm7_8.OrgXML(node: IXMLNode; f11, fix, f43: Boolean; d1, d2: TDateTime);
begin
  if Length(Src.fOrg.Kpp0) > 0 then
    GetKPPAdres(Src.que1_sql_, String(Src.fOrg.Kpp0), Src.fOrg.adres);

  if Copy(Src.fOrg.inn, 1, 10) = '0000000000' then
    Src.fOrg.inn := '1234567890' + Copy(Src.fOrg.inn, 11, 2);
  Src.fOrg.adres.kod_str := ShortString
    (UseStr(String(Src.fOrg.adres.kod_str), '', '643'));
  Src.fOrg.adres.kod_reg := ShortString
    (UseStr(String(Src.fOrg.adres.kod_reg), '',
      Copy(String(Src.fOrg.inn), 1, 2)));
  if Src.fOrg.Kpp0 = '000000000' then
    Src.fOrg.Kpp0 := '123456789';

  OrgRecXML(node, f11, fix, Src.fOrg); // ���������
  OrgOtvXML(node, Src.fOrg.buh, Src.fOrg.ruk); // ���������
  if f11 then
    OrgLicXML(Src.fOrg, node, d1, d2, f43); // ��������
end;


// ������� �����
procedure tForm7_8.ClearSL(SL: TStringList);
var
  i: integer;
begin
  if SL.Count > 0 then
  begin
    for i := SL.Count - 1 downto 0 do
      if SL.Objects[i] <> nil then
        SL.Objects[i].Free;
    SL.Clear;
  end;
end;


procedure tForm7_8.GetDataKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.selfkpp) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure tForm7_8.GetDataOKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
//  ClearSL(SLFiltr);
  SLFiltr.clear;
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.selfkpp) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;


function tForm7_8.OstKPPXML(SL: TStringList; f11: Boolean; kpp: string;  Data: TDateTime): Extended;
var
  V, vol: Extended;
  i: integer;
  decl: TDeclaration;
begin
  vol := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
      begin
        if InDecl(String(decl.product.FNSCode), f11) and (decl.RepDate < Data) then
        begin
          V := 0.1 * decl.Amount * decl.product.Capacity;
          case decl.TypeDecl of
            1:
              vol := vol + V; // ������
            2:
              vol := vol - V; // ������
            3:
              vol := vol - V; // ������� ����������
            4:
              vol := vol + V; // ������� �� ����������
            5:
              vol := vol - V; // �������� (����)
            6:
              vol := vol - V; // ������� -
            7:
              vol := vol + V; // �������� +
          end;
        end;
      end;
  end;
  Result := vol;
end;


function tForm7_8.OkruglDataDecl(val: Extended):Extended;
  function RoundEx( X: Extended; Precision : Integer ): Extended;
  {Precision : 1 - �� �����, 10 - �� �������, 100 - �� �����...}
  var
   ScaledFractPart, idx : Extended;
  begin
   ScaledFractPart := Frac(X)*Precision;
   idx := Roundto(Frac(ScaledFractPart),-2);
   ScaledFractPart := Int(ScaledFractPart);
   if idx >= 0.5 then
     ScaledFractPart := ScaledFractPart + 1;
   if idx <= -0.5 then
     ScaledFractPart := ScaledFractPart - 1;
   RoundEx := Int(X) + ScaledFractPart/Precision;
  end;
var s:Extended;
begin
//  SetRoundMode(rmUp);
//  Result := RoundTo(val, -ZnFld);
  s:= RoundEx(Val,100000);
  Result:= s;
end;


function tForm7_8.Kvart_Beg(kv: Integer; year: Integer): TDateTime;
var
  d: string;
begin
  d := IntToStr(year);
  case kv of
    1:
      d := '01.01.' + d;
    2:
      d := '01.04.' + d;
    3:
      d := '01.07.' + d;
    4:
      d := '01.10.' + d;
  end;
  Kvart_Beg := StrToDate(d);
end;

function tForm7_8.Kvart_Start(data: TDateTime): TDate;
var
  Y, m, d: word;
  kv: Integer;
begin
  kv := 0;
  DecodeDate(data, Y, m, d);
  case m of
    1, 2, 3:
      kv := 1;
    4, 5, 6:
      kv := 2;
    7, 8, 9:
      kv := 3;
    10, 11, 12:
      kv := 4;
  end;
  Kvart_Start := Kvart_Beg(kv, Y);
end;

function tForm7_8.RashKPPXML(SL: TStringList; f11: Boolean; kpp: string; Data: TDateTime): Boolean;
var
  V, vol: Extended;
  i: integer;
  decl: TDeclaration;
  d1, d2:TDateTime;
begin
  vol := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    d1:=Kvart_start(Data-1);
    d2:=Kvart_finish(Data);
    if (decl.DeclDate >=d1) and
      (Int(decl.DeclDate) <= d2) then
      if InDecl(String(decl.product.FNSCode), f11) then
        begin
          V := 0.1 * decl.Amount * decl.product.Capacity;
          case decl.TypeDecl of
            2, 6:
              vol := vol + V; // ������
          end;
        end;
  end;
  if vol > 0 then
    Result := True
  else
    Result := false;
end;


function tForm7_8.Kvart_Finish(data: TDateTime): TDate;
var
  Y, m, d: word;
  kv: Integer;
begin
  kv := 0;
  DecodeDate(data, Y, m, d);
  case m of
    1, 2, 3:
      kv := 1;
    4, 5, 6:
      kv := 2;
    7, 8, 9:
      kv := 3;
    10, 11, 12:
      kv := 4;
  end;
  Kvart_Finish := Kvart_End(kv, Y);
end;

function tForm7_8.Kvart_End(kv: Integer; year: Integer): TDateTime;
var
  d: string;
begin
  d := IntToStr(year);
  if kv = 0 then kv := 1;

  case kv of
    1:
      d := '31.03.' + d;
    2:
      d := '30.06.' + d;
    3:
      d := '30.09.' + d;
    4:
      d := '31.12.' + d;
  end;
  Kvart_End := StrToDate(d);
end;


procedure tForm7_8.GetDataGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.FNSCode) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure tForm7_8.GetDataOGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
  SLFiltr.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.FNS) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;


procedure tForm7_8.GetDataProizvXML(s: string; SL: TStringList;
  var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.prod.pId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


function tForm7_8.DBetween(d, d1, d2: TDateTime): boolean;
begin
  if ((d1 <= d) and (d <= d2)) then
    Result := true
  else
    Result := false;
end;


procedure tForm7_8.GetDataSalXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.sal.sId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure tForm7_8.FreeStringList(var SL: TStringList);
begin
  if SL = nil then
    exit;
  ClearSL(SL);
  SL.free;
end;


procedure tForm7_8.GetDataXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if DateToStr(decl.DeclDate) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure tForm7_8.GetDataNaclXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.DeclNum) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


function tForm7_8.OborotDate(d: TDateTime; SL: TStringList): Extended;
var
  i: integer;
  decl: TDeclaration;
  volume: Extended;
begin
  volume := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
        if decl.DeclDate = d then
          if decl.TypeDecl = 1 then
            volume := volume + 0.1 * decl.product.Capacity * decl.Amount;
  end;
  Result := volume;
end;

procedure tForm7_8.PostProdDataNacl(node: IXMLNode; SL: TStringList);
var
  decl: TDeclaration;
  SLNacl, SLData:TStringList;
  i:integer;
  vol: Extended;
begin
  SLNacl := TStringList.Create;
  SLData := TStringList.Create;
  try
    // ���� � ���������� ����������
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]).Data;
      if TDecl_obj(SL.Objects[i]) <> nil then
        if TDecl_obj(SL.Objects[i]) is TDecl_Obj then
          if decl.TypeDecl = 1 then
            if SLNacl.IndexOf(String(decl.DeclNum)) = -1 then
              begin
                SLNacl.AddObject(String(decl.DeclNum), TDecl_obj.Create);
                TDecl_Obj(SLNacl.Objects[SLNacl.Count-1]).Data := decl;
              end;
    end;
    SLNacl.Sort;
    for i := 0 to SLNacl.Count - 1 do
    begin
      decl := TDecl_obj(SLNacl.Objects[i]).Data;
      GetDataNaclXML(String(decl.DeclNum), SL, SLData);
      vol := OborotDate(Int(decl.DeclDate), SLData);
      with node do
        { with AddChild('���������') do }
          with AddChild('��������') do
        begin
     {
          Attributes['�000000000013'] := DateToStr(decl.DeclDate);
          Attributes['�000000000014'] := decl.DeclNum;
          decl.TM := '';
          Attributes['�000000000015'] := '';
          Attributes['�000000000016'] := FloatToStrF(vol, ffFixed, maxint,
            ZnFld); ;
      }
          Attributes['�000000000013'] := DateToStr(decl.DeclDate);
          Attributes['�000000000014'] := decl.DeclNum;
          decl.TM := '';
          Attributes['�000000000015'] := '';
          Attributes['�000000000016'] := FloatToStrF(vol, ffFixed, maxint,
            ZnFld); ;

        end;
    end;

  finally
    FreeStringList(SLData);
    FreeStringList(SLNacl);
  end;
end;

procedure tForm7_8.PostProdXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime);
var
  i: integer;
  SLDate, SLDataNacl: TStringList;
  decl: TDeclaration;

//  s:string;
begin
  try
    try
      SLDate := TStringList.Create;
      SLDataNacl := TStringList.Create;

      // ���� � ������ ����������
      for i := 0 to SL.Count - 1 do
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        if TDecl_obj(SL.Objects[i]) <> nil then
          if TDecl_obj(SL.Objects[i]) is TDecl_Obj then
            if decl.TypeDecl = 1 then
              if DBetween(Int(decl.RepDate), d1, d2) then
                if SLDate.IndexOf(DateTimeToStr(decl.DeclDate)) = -1 then
                  begin
                    SLDate.AddObject(DateTimeToStr(decl.DeclDate), TDecl_obj.Create);
                    TDecl_Obj(SLDate.Objects[SLDate.Count-1]).Data := decl;
                  end;
      end;

//      SLDate.CustomSort(CompDateAsc);
      {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := '.';

      // ������� ������� �� ����
      for i := 0 to SLDate.Count - 1 do
      begin
        decl := TDecl_obj(SLDate.Objects[i]).Data;
        GetDataXML(DateToStr(decl.DeclDate), SL, SLDataNacl);
        PostProdDataNacl(node, SLDataNacl);
      end;
      {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := ',';
    except
    end;
  finally
//    SLDate.Free;
    FreeStringList(SLDate);
    FreeStringList(SLDataNacl)
  end;
end;

procedure tForm7_8.SvedPrImpPostXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime; f11: Boolean);
var
  i: integer;
  SLPrImp, SLKppGrupImpPostI: TStringList;
  Post: IXMLNode;
  decl: TDeclaration;
begin
  try
    try
      // �������� ������ ����������, ������� ��������� � �������
      SLPrImp := TStringList.Create;
      SLKppGrupImpPostI := TStringList.Create;
      for i := 0 to SL.Count - 1 do
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        if (decl.TypeDecl = 1) then
        begin
          // ���������� ������ ���� ��������� �������
          if DBetween(Int(decl.RepDate), d1, d2) then
            if decl.sal.sInn <> '1234567894' then
            begin
              if SLPrImp.IndexOf(String(decl.sal.sId)) = -1 then
                begin
                  SLPrImp.AddObject(String(decl.sal.sId), TDecl_obj.Create);
                  TDecl_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := decl;
                end;
            end;
        end;
      end;

      // ��������� ������ �� ���� ���������
      SLPrImp.Sort;
      // SortSL(SLPrImp, 9, CompNumAsc);

      with node do
      begin
        if SLPrImp.Count > 0 then
          for i := 0 to SLPrImp.Count - 1 do
          begin
            decl := TDecl_obj(SLPrImp.Objects[i]).Data;

            GetDataSalXML(String(decl.sal.sId), SL, SLKppGrupImpPostI);
            Post := AddChild('���������');
            if decl.sal.sId <> '' then
              with Post do
              begin
                Attributes['�N'] := IntToStr(i + 1);
                Attributes['������������'] := decl.sal.sId;
                {
                if f11 then
                  if decl.sal.lic.RegOrg <> NoLic then
                    Attributes['����������'] := decl.sal.lic.lId
                  else
                    Attributes['����������'] := '';
                 }

                // ��������� ������ ������� ����������
                PostProdXML(Post, SLKppGrupImpPostI, d1, d2);
              end;
          end;

      end;
    except
    end;
  finally
    FreeStringList(SLKppGrupImpPostI);
    FreeStringList(SLPrImp);
  end;
end;


procedure tForm7_8.NullProd(var prod:TProduction);
begin
  prod.EAN13:='';
  prod.FNSCode:='';
  prod.Productname:='';
  prod.ProductNameF:='';
  prod.AlcVol:=0;
  prod.Capacity:=0;
  prod.prod.pId:='';
  prod.prod.pInn:='';
  prod.prod.pKpp:='';
  prod.prod.pName:='';
  prod.prod.pAddress:='';
end;

procedure tForm7_8.NullDecl(var decl: TDeclaration);
begin
  decl.PK := '';
  decl.SelfKPP := Src.fOrg.Kpp0;
  decl.DeclDate := Src.dE;
  decl.RepDate := Src.dE;
  decl.TypeDecl := -1;
  decl.DeclNum := '';
  decl.Amount := 0;
  decl.Ost.PK := '';
  decl.Ost.PK_EAN := '';
  decl.Ost.Value:=0;
  decl.Ost.DateOst := decl.DeclDate;
  decl.vol := 0;
  decl.Amount_dal := 0;
  decl.Vol_dal := 0;
  decl.TM := '';
  decl.forma := -1;
  NullProd(decl.product);
  NullSaler(decl.sal);
end;


procedure tForm7_8.NullSaler(var sal: TSaler);
begin
  sal.sINN := '';
  sal.sKpp := '';
  sal.sName := '';
  sal.sAddress := '';
  sal.sId:='';
  sal.lic.lId:='';
  sal.lic.ser:='';
  sal.lic.num:='';
  sal.lic.RegOrg:='';
  sal.lic.dBeg:=Src.dB;
  sal.lic.dEnd:=Src.dE;
end;


function tForm7_8.IsNotNullDecl(dM: TMove): Boolean;
begin
  Result := False;
  if dM.dv.ost0 <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.prih <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.rash <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.vozv_post <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.vozv_pokup <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.brak <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.ost1 <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.move_in <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.move_out <> 0 then
  begin
    Result := True;
    exit;
  end;
end;

procedure tForm7_8.DvizenieOneEAN(SL: TStringList; d1, d2: TDateTime; var dviz: TMove);
var
  i: integer;
  decl: TDeclaration;
  V, ost, ost_dal: Extended;
  rd: TDateTime;
  dt: integer;
begin
  dviz := TMove.Create;
  V := 0;
  ost := 0;
  ost_dal := 0;

  dviz.dv.ost0 := 0;
  dviz.dv.prih := 0;
  dviz.dv.rash := 0;
  dviz.dv.vozv_post := 0;
  dviz.dv.vozv_pokup := 0;
  dviz.dv.brak := 0;
  dviz.dv.ost1 := 0;
  dviz.dv.oder := 0;
  dviz.dv.move_in := 0;
  dviz.dv.move_out := 0;
  dviz.dv.ostAM :=0;

  dviz.dv_dal.ost0 := 0;
  dviz.dv_dal.prih := 0;
  dviz.dv_dal.rash := 0;
  dviz.dv_dal.vozv_post := 0;
  dviz.dv_dal.vozv_pokup := 0;
  dviz.dv_dal.brak := 0;
  dviz.dv_dal.ost1 := 0;
  dviz.dv_dal.oder := 0;
  dviz.dv_dal.move_in := 0;
  dviz.dv_dal.move_out := 0;
  dviz.dv_dal.ostAM:=0;

//  SortSLCol(7, SL, CompDateAsc);
  for i := 0 to SL.Count - 1 do
  begin
    NullDecl(decl);
    decl := TDecl_obj(SL.Objects[i]).data;
    dviz.FNS := decl.product.FNSCode;
    dviz.forma := decl.forma;
    dt := decl.TypeDecl;
    rd := Int(decl.RepDate);
    V := decl.product.capacity * decl.Amount;
    if (rd < d1) then
    begin
      case dt of
        1:
          begin
            ost := ost + decl.Amount; // ������
            ost_dal := ost_dal + V; // ������
            dviz.sal := decl.sal;
          end;
        2:
          begin
            ost := ost - decl.Amount; // ������
            ost_dal := ost_dal - V;
          end;
        3:
          begin
            ost := ost - decl.Amount; // ������� ����������
            ost_dal := ost_dal - V;
          end;
        4:
          begin
            ost := ost + decl.Amount; // ������� �� ����������
            ost_dal := ost_dal + V;
          end;
        5:
          begin
            ost := ost - decl.Amount; // �������� (����)
            ost_dal := ost_dal - V;
          end;
        6:
          begin
            ost := ost - decl.Amount; // �����������-
            ost_dal := ost_dal - V;
          end;
        7:
          begin
            ost := ost + decl.Amount; // �����������+
            ost_dal := ost_dal + V;
          end;
      end;
    end
    else if DBetween(Int(rd), d1, d2) then
    begin
      case dt of
        1:
          begin
            dviz.dv.prih := dviz.dv.prih + decl.Amount; // ������
            dviz.dv_dal.prih := dviz.dv_dal.prih + V;
            dviz.sal := decl.sal;
          end;
        2:
          begin
            dviz.dv.rash := dviz.dv.rash + decl.Amount; // ������
            dviz.dv_dal.rash := dviz.dv_dal.rash + V;
            dviz.dv.ostAM := dviz.dv.ostAM + decl.Ost.Value;
            dviz.dv_dal.ostAM := dviz.dv_dal.ostAM + decl.Ost.Value*decl.product.Capacity;
          end;
        3:
          begin
            dviz.dv.vozv_post := dviz.dv.vozv_post + decl.Amount;
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.vozv_post := dviz.dv_dal.vozv_post + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        4:
          begin
            dviz.dv.vozv_pokup := dviz.dv.vozv_pokup + decl.Amount;
            dviz.dv.oder := dviz.dv.oder + decl.Amount;

            dviz.dv_dal.vozv_pokup := dviz.dv_dal.vozv_pokup + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder + V;
          end;
        5:
          begin
            dviz.dv.brak := dviz.dv.brak + decl.Amount; // �������� (����)
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.brak := dviz.dv_dal.brak + V; // �������� (����)
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        6:
          begin
            dviz.dv.move_out := dviz.dv.move_out + decl.Amount;  // ��������-
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.move_out := dviz.dv_dal.move_out + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        7:
          begin
            dviz.dv.move_in := dviz.dv.move_in + decl.Amount; // ��������+
            dviz.dv.oder := dviz.dv.oder + decl.Amount;

            dviz.dv_dal.move_in := dviz.dv_dal.move_in + V; // ��������+
            dviz.dv_dal.oder := dviz.dv_dal.oder + V;
          end;
      end;
    end;
  end;

  dviz.dv.ost0 := ost;
  dviz.dv_dal.ost0 := ost_dal;

  dviz.dv.ost1 := dviz.dv.ost0 + dviz.dv.prih - dviz.dv.rash -
    dviz.dv.vozv_post + dviz.dv.vozv_pokup - dviz.dv.brak +
    dviz.dv.move_in - dviz.dv.move_out;
  dviz.dv_dal.ost1 := dviz.dv_dal.ost0 + dviz.dv_dal.prih -
    dviz.dv_dal.rash - dviz.dv_dal.vozv_post + dviz.dv_dal.vozv_pokup -
    dviz.dv_dal.brak + dviz.dv_dal.move_in - dviz.dv_dal.move_out;

  dviz.dv.ost0:=OkruglDataDecl(dviz.dv.ost0);
  dviz.dv.prih:=OkruglDataDecl(dviz.dv.prih);
  dviz.dv.rash:=OkruglDataDecl(dviz.dv.rash);
  dviz.dv.vozv_post:=OkruglDataDecl(dviz.dv.vozv_post);
  dviz.dv.vozv_pokup:=OkruglDataDecl(dviz.dv.vozv_pokup);
  dviz.dv.brak:=OkruglDataDecl(dviz.dv.brak);
  dviz.dv.ost1:=OkruglDataDecl(dviz.dv.ost1);
  dviz.dv.oder:=OkruglDataDecl(dviz.dv.oder);
  dviz.dv.move_in:=OkruglDataDecl(dviz.dv.move_in);
  dviz.dv.move_out:=OkruglDataDecl(dviz.dv.move_out);
  dviz.dv.ostAM:=OkruglDataDecl(dviz.dv.ostAM);

  dviz.dv_dal.ost0 := dviz.dv_dal.ost0 * 0.1;
  dviz.dv_dal.prih := dviz.dv_dal.prih * 0.1;
  dviz.dv_dal.rash := dviz.dv_dal.rash * 0.1;
  dviz.dv_dal.vozv_post := dviz.dv_dal.vozv_post * 0.1;
  dviz.dv_dal.vozv_pokup := dviz.dv_dal.vozv_pokup * 0.1;
  dviz.dv_dal.brak := dviz.dv_dal.brak * 0.1;
  dviz.dv_dal.ost1 := dviz.dv_dal.ost1 * 0.1;
  dviz.dv_dal.oder := dviz.dv_dal.oder * 0.1;
  dviz.dv_dal.move_in := dviz.dv_dal.move_in * 0.1;
  dviz.dv_dal.move_out := dviz.dv_dal.move_out * 0.1;
  dviz.dv_dal.ostAM := dviz.dv_dal.ostAM * 0.1;

  dviz.dv_dal.ost0:=OkruglDataDecl(dviz.dv_dal.ost0);
  dviz.dv_dal.prih:=OkruglDataDecl(dviz.dv_dal.prih);
  dviz.dv_dal.rash:=OkruglDataDecl(dviz.dv_dal.rash);
  dviz.dv_dal.vozv_post:=OkruglDataDecl(dviz.dv_dal.vozv_post);
  dviz.dv_dal.vozv_pokup:=OkruglDataDecl(dviz.dv_dal.vozv_pokup);
  dviz.dv_dal.brak:=OkruglDataDecl(dviz.dv_dal.brak);
  dviz.dv_dal.ost1:=OkruglDataDecl(dviz.dv_dal.ost1);
  dviz.dv_dal.oder:=OkruglDataDecl(dviz.dv_dal.oder);
  dviz.dv_dal.move_in:=OkruglDataDecl(dviz.dv_dal.move_in);
  dviz.dv_dal.move_out:=OkruglDataDecl(dviz.dv_dal.move_out);
  dviz.dv_dal.ostAM:=OkruglDataDecl(dviz.dv_dal.ostAM);

  dviz.product := decl.product;
  dviz.SelfKPP := decl.SelfKPP;
  if not IsNotNullDecl(dviz) then
  begin
    dviz.free;
    dviz := nil;
  end;
end;


function tForm7_8.MinusMove(dv: TMove): Boolean;
begin
  Result := False;
  if dv.dv.ost0 < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.prih < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.rash < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.vozv_post < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.vozv_pokup < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.brak < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.ost1 < 0 then
  begin
    Result := True;
    exit;
  end;
end;

function tForm7_8.GenErr(dv:TMove):string;
var s:string;
begin
  Result:='';
  if not MinusMove(dv) then exit;
  s:='������� �� �� = ' + FloatToStrF(dv.dv_dal.ost0, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dv.dv_dal.prih, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dv.dv_dal.rash, ffFixed, maxint, ZnFld) +
     ' �����.����. = ' + FloatToStrF(dv.dv_dal.vozv_post, ffFixed, maxint, ZnFld) +
     ' �����.�����. = ' + FloatToStrF(dv.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld) +
     ' ���� = ' + FloatToStrF(dv.dv_dal.brak, ffFixed, maxint, ZnFld) +
     ' �������� (-) = ' + FloatToStrF(dv.dv_dal.move_out, ffFixed, maxint, ZnFld) +
     ' �������� (+) = ' + FloatToStrF(dv.dv_dal.move_in, ffFixed, maxint, ZnFld) +
     ' ������� �� �� = ' + FloatToStrF(dv.dv_dal.ost1, ffFixed, maxint, ZnFld);
  Result:=s;
end;

function tForm7_8.DvizPostXML(node: IXMLNode; SL, SLErr: TStringList; f11: Boolean; d1, d2: TDateTime): Boolean;
var
  dvol: TMove;
  decl: TDeclaration;
  s, s1:string;
begin
  try
    DvizenieOneEAN(SL, d1, d2, dvol);
    //������������� ������ � �������(���� ����)
    s:=GenErr(dvol);
    if s <> '' then
      begin
        decl:=TDecl_obj(SL.Objects[0]).data;
        s1:=String('��� = ' + decl.SelfKpp +' ��. �� = ' + decl.product.FNSCode + ' ID = ' + decl.product.prod.pId +  ' ��� = ' +
             decl.product.prod.pInn + ' ��� = ' + decl.product.prod.pKpp + ' ' +
             ' ' + decl.product.prod.pName);
        SLErr.Add(s1);
        SLErr.Add(s);
        SLErr.Add('');
      end;
    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := '.';
    with node do
      with AddChild('��������') do
        if f11 then
        begin
          Attributes['�N'] := '1';
          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed, maxint, ZnFld);
          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
          Attributes['�100000000009'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
          Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld);
          Attributes['�100000000011'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000012'] := FloatToStrF(dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000013'] := FloatToStrF(dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.rash, ffFixed, maxint, ZnFld);
          Attributes['�100000000015'] := FloatToStrF(dvol.dv_dal.brak, ffFixed, maxint, ZnFld);
          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.vozv_post, ffFixed, maxint, ZnFld);
          Attributes['�100000000017'] := FloatToStrF(dvol.dv_dal.move_out,ffFixed, maxint, ZnFld);
          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +  dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000019'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000020'] := FloatToStrF(0.00,ffFixed, maxint, ZnFld);

        end
        else
        begin
          Attributes['�N'] := '1';
          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed,maxint, ZnFld);
          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed,maxint, ZnFld);
          Attributes['�100000000009'] := FloatToStrF(0, ffFixed,maxint, ZnFld);
           Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
          Attributes['�100000000011'] := FloatToStrF(dvol.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld);
          Attributes['�100000000012'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000013'] := FloatToStrF(dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000015'] := FloatToStrF(dvol.dv_dal.rash, ffFixed, maxint, ZnFld);
          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.brak, ffFixed, maxint, ZnFld);
          Attributes['�100000000017'] := FloatToStrF(dvol.dv_dal.vozv_post, ffFixed, maxint, ZnFld);
          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.move_out,ffFixed, maxint, ZnFld);
          Attributes['�100000000019'] := FloatToStrF(dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +  dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000020'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
        end;
    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := ',';
  finally
    dvol.Free;
  end;
end;


procedure tForm7_8.GetDataOProizvXML(s: string; SL: TStringList;
  var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.product.prod.pId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;

procedure tForm7_8.SvedPrImpXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean;
  d1, d2: TDateTime);
var
  SvedPrImp: IXMLNode;
  i: integer;
  SLPrImp, SLKppGrupPrI, SLOKppGrupPrI: TStringList;
  idp: string;
  prod: TProducer;
  mv:TMove;
begin
  try
    try
      SLPrImp := TStringList.Create;
      SLKppGrupPrI := TStringList.Create;
      SLOKppGrupPrI := TStringList.Create;
      ClearSL(SLPrImp);
      SLPrImp.Duplicates := dupIgnore;

      // ���������� ������ �������������� ��� ������ ������ ��

      for i := 0 to SLO.Count - 1 do
      begin
        mv := TMove(SLO.Objects[i]);
        if SLPrImp.IndexOf(String(mv.product.prod.pId)) = -1 then
          begin
            SLPrImp.AddObject(String(mv.product.prod.pId), TProducer_Obj.Create);
            TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := mv.product.prod;
          end;
      end;

      // ��������� ���� �� ���� ������������
 //     SLPrImp.CustomSort(CompNumAsc);

      for i := 0 to SLPrImp.Count - 1 do
        with node do
        begin
          prod := TProducer_Obj(SLPrImp.Objects[i]).Data;
          idp := SLPrImp.Strings[i];
          GetDataProizvXML(idp, SL, SLKppGrupPrI);
          GetDataOProizvXML(idp, SLO, SLOKppGrupPrI);


          SvedPrImp := AddChild('������������������');

          {
            SvedPrImp := AddChild('����������������');
          }

          with SvedPrImp do
          begin
            Attributes['�N'] := IntToStr(i + 1);
            Attributes['�����������'] := prod.pId;
            // �������� ������ ���������� ��� ������ ������ �� � ������� �������������
            SvedPrImpPostXML(SvedPrImp, SLKppGrupPrI, d1, d2, f11);
            // ��������
            DvizPostXML(SvedPrImp, SLKppGrupPrI, SLErr, f11, d1, d2);
          end;
        end;
    except
    end;
  finally
    FreeStringList(SLKppGrupPrI);
    FreeStringList(SLPrImp);
  end;
end;

procedure tForm7_8.OborotKppXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean; d1, d2: TDateTime);
var
  i: integer;
  grup: string;
  SLGroup, SLKppGrupI, SLOKppGrupI: TStringList;
  obor: IXMLNode;
  mv:TMove;
begin
  SLGroup := TStringList.Create;
  SLKppGrupI := TStringList.Create;
  SLOKppGrupI := TStringList.Create;
  try
    try
      // ����������� ���� �� ������������ � ���
      for i := 0 to SLO.Count - 1 do
      begin
        mv := TMove(SLO.Objects[i]);
        if mv.FNS <> '' then
          if SLGroup.IndexOf(String(mv.FNS)) = -1 then
            if InDecl(String(mv.FNS), f11) then
              SLGroup.Add(String(mv.FNS));
      end;

      // ���������� ����� ����� �� ���������� (�������� ������ ��� �����)
    //  SLGroup.CustomSort(CompNumAsc);

      // ���������� �� ������� ��
      for i := 0 to SLGroup.Count - 1 do
        with node do
        begin
          obor := AddChild('������');
          with obor do
          begin
            grup := SLGroup.Strings[i];
            Attributes['�N'] := IntToStr(i + 1);
			Attributes['�000000000003'] := grup;
       {     Attributes['�000000000003'] := grup; }
            // ���������� ����� ������� ���������� ��� ������ ������ ��
            GetDataGroupXML(grup, SL, SLKppGrupI);
            GetDataOGroupXML(grup, SLO, SLOKppGrupI);
            // �������������
            SvedPrImpXML(obor, SLKppGrupI,SLOKppGrupI, SLErr, f11, d1, d2);
          end;
        end;

    except
    end;
  finally
//    FreeStringList(SLOKppGrupI);
//    SLOKppGrupI.Free;
    FreeStringList(SLKppGrupI);
    SLGroup.Free;
  end;
end;

procedure tForm7_8.KPPXML(node: IXMLNode; selfkpp: string; SL,SLO, SLErr: TStringList; f11, af: Boolean; d1, d2: TDateTime; OneC:Boolean);
var
  ost_d1, ost_d2: Extended;
  ob: Boolean;
begin
  // ���������� �������� �� ������ � ����� �������
  ost_d1 := OstKPPXML(SL, f11, selfkpp, d1);
  ost_d2 := OstKPPXML(SL, f11, selfkpp, d2+1);

  ost_d1:=OkruglDataDecl(ost_d1);
  ost_d2:=OkruglDataDecl(ost_d2);
  if (ost_d1 <> ost_d2) then
    ob := True
  else
  begin
    if not RashKPPXML(SL, f11, selfkpp, d2) then
      begin
        if ost_d2 <> 0 then
          ob := True
        else
          ob:=False;
      end
    else
      ob := true;
  end;
  // ��������� ��� ���
  ShapkaKPPXML(node, f11, af, ob, selfkpp, OneC);
  // ���������� ������� �� ���
  OborotKppXML(node, SL, SLO, SLErr, f11, d1, d2);
end;

procedure tForm7_8.ShapkaKPPXML(node: IXMLNode; f11, af, ob: Boolean; selfkpp: string; OneC:Boolean);
var
  kpp_i: TOrgAdres;
//  oborot: IXMLNode;
begin
  GetKPPAdres(Src.que1_sql_, selfkpp, kpp_i);
  with kpp_i do
  begin
    if af then
      if Empty(name) then
        name := '��� ' + String(kpp);
    kod_str := ShortString(UseStr(String(kod_str), '', '643'));
    with node do
    begin
      Attributes['����'] := trim(name);
      if Length(Src.fOrg.Inn) = 10  then
        Attributes['�����'] := kpp
      else
        if not OneC then
             Attributes['�����'] := kpp;
      {
      Attributes['��������������'] := Bool2Str(ob);
      }
      with AddChild('������') do
      begin
        AddChild('���������').Text := '643';
        AddChild('������').Text := String(postindex);
        AddChild('���������').Text := String(kod_reg);
        AddChild('�����').Text := String(raion);
        AddChild('�����').Text := String(gorod);
        AddChild('����������').Text := String(naspunkt);
        AddChild('�����').Text := String(ulica);
        AddChild('���').Text := String(dom);
        AddChild('������').Text := String(korpus);
        AddChild('������').Text := String(litera);
        AddChild('�����').Text := String(kvart);
      end;
    end;
  end;
end;


function tForm7_8.WinTemp: string;
begin
  SetLength(result, MAX_PATH);
  SetLength(result, GetTempPath(MAX_PATH, PChar(result)));
end;

function tForm7_8.FindErrXSD(fXML: string; f11, f43: Boolean;
  var sErr: string): Boolean;
var
  ms: TMemoryStream;
  r: TResourceStream;
  s1: integer;
  NewPath, fXsd, S2: string;
  Validator: TXSDValidator;
begin
  Result := False;

  if f11 then
    r := TResourceStream.Create(HInstance, '11_o', RT_RCDATA)
  else
    r := TResourceStream.Create(HInstance, '12_o', RT_RCDATA);

  ms := TMemoryStream.Create;
  try
    try
      ms.LoadFromStream(r);

      NewPath := WinTemp + NameProg;
      if not DirectoryExists(NewPath) then
        ForceDirectories(NewPath);

      fXsd := NewPath + '\tmp.xsd';
      if FileExists(fXsd) then
        DeleteFile(PWideChar(fXsd));

      ms.SaveToFile(fXsd);

      Validator := TXSDValidator.Create;

      if (Validator.ParseXmlFile(fXML)) then
        if (Validator.AddXsdFile(fXsd)) then
          if (Validator.Valid(s1, S2)) then
            sErr := XmlValid
          else
            sErr := Validator.error
        else
          sErr := Validator.error
      else
        sErr := Validator.error;

      if sErr <> XmlValid then
        Result := False
      else
        Result := True;
    finally
      FreeAndNil(ms);
      r.free;
      if DirectoryExists(NewPath) then
        RemoveDir(NewPath)
//        DelDir(NewPath);
//      FreeAndNil(Validator);
//      Validator.Destroy;
    end;
  except
  end;
end;


function tForm7_8.GetSert(fSert: TSert; var CertContext: PCCERT_CONTEXT): Boolean;
var
  hCert_Store: HCERTSTORE;
  hProv: HCRYPTPROV;
  hKeySig: HCRYPTKEY;
  PAC: PAnsiChar;
  PACs: AnsiString;
  CertLen, pubKeyInfoLen: DWORD;
  L: PWideChar;
  pubKeyInfo: PCERT_PUBLIC_KEY_INFO;
  find, bol: Boolean;
  Cert: PByte;
  er: Int64;
begin
  Result := False;
  PACs := AnsiString(fSert.path);
  PAC := Addr(PACs[1]);
  CryptAcquireContextA(@hProv, PAC, GOST_34, 75, CRYPT_SILENT);
  CryptGetUserKey(hProv, AT_KEYEXCHANGE, @hKeySig);
  // �������� ������ �� �������� ����
  CryptGetKeyParam(hKeySig, KP_CERTIFICATE, nil, @CertLen, 0);
  CertContext := nil;
  pubKeyInfo := nil;
  if CertContext = nil then
  begin
    L := PWideChar(WideString('MY'));
    hCert_Store := CertOpenStore(CERT_STORE_PROV_SYSTEM, encType, hProv,
      CERT_SYSTEM_STORE_CURRENT_USER, L); // ��������� ���������
    find := False;
    CertContext := CertEnumCertificatesInStore(hCert_Store, CertContext);
    if CertContext <> nil then
    begin
      CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType, nil,
        @pubKeyInfoLen);
      er := GetLastError;
      if pubKeyInfoLen > 0 then
      begin
        Screen.Cursor := crHourGlass;
        GetMem(pubKeyInfo, pubKeyInfoLen);
        CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType,
          pubKeyInfo, @pubKeyInfoLen);
        Screen.Cursor := crDefault;
      end; // if pubKeyInfoLen>0
    end; // if CertContext=nil
    bol := False;
    while (CertContext <> nil) and (not find) do
    begin
      if bol then
        CertContext := CertEnumCertificatesInStore(hCert_Store,
          CertContext);
      if (pubKeyInfoLen > 0) and (CertContext <> nil) then
        if CertComparePublicKeyInfo(CertContext.dwCertEncodingType,
          pubKeyInfo, @CertContext.pCertInfo.SubjectPublicKeyInfo) then
          find := True;
      bol := True;
    end; // while (CertContext<>nil)
  end; // if CertContext=nil
  FreeMem(pubKeyInfo, pubKeyInfoLen);
  Result := True;
  if CertContext = nil then
    if CertLen > 0 then
    begin
      GetMem(Cert, CertLen);
      CryptGetKeyParam(hKeySig, KP_CERTIFICATE, Cert, @CertLen, 0);
      CertContext := CertCreateCertificateContext(encType, Cert, CertLen);
      // ������� �������� �����������
      if CertContext <> nil then
      begin
        // Gr[ComboBox1.ItemIndex+1, 4]:='cont';
        Result := True;
      end;
      FreeMem(Cert, CertLen); // ���������� ������
    end;
end;


procedure tForm7_8.SignXNL(Var ms, ms1: TMemoryStream; Var err: Boolean;
  ECP: string; CertContext: PCCERT_CONTEXT);
var
  SigParams: CRYPT_SIGN_MESSAGE_PARA;
  hProv: HCRYPTPROV;
  hKeySig: HCRYPTKEY;
  provInfoSig: CRYPT_KEY_PROV_INFO;

  dwNameLength, dwProvLength, pdw: DWORD;
  Data_Array_pointer: pacarPByte;
  Size_Array_pointer: pacarDWORD;
  pbSignedBlob: PByte;
  pcbSignedBlob: DWORD;
  n: Int64;

  PAC: PAnsiChar;
  PACs: AnsiString;
begin
  ms1.Clear;
  ms.Position := 0;
  err := False;
  PACs := AnsiString(ECP);
  PAC := Addr(PACs[1]);
  CryptAcquireContextA(@hProv, PAC, GOST_34, 75, 0);
  CryptGetUserKey(hProv, AT_KEYEXCHANGE, @hKeySig);
  // �������� ������ �� �������� ����
  try
    try
      if CertContext <> nil then
      begin
        CryptGetProvParam(hProv, PP_CONTAINER, nil, @dwNameLength, 0);
        // �������� ���������� ��� ����������
        provInfoSig.pwszContainerName := AllocMem(dwNameLength);
        CryptGetProvParam(hProv, PP_CONTAINER,
          PByte(provInfoSig.pwszContainerName), @dwNameLength, 0);

        CryptGetProvParam(hProv, PP_NAME, nil, @dwProvLength, 0);
        // �������� ��� ����������������
        provInfoSig.pwszProvName := AllocMem(dwProvLength);
        CryptGetProvParam(hProv, PP_NAME, PByte(provInfoSig.pwszProvName),
          @dwProvLength, 0);

        provInfoSig.dwFlags := 0; // ������������� �����
        provInfoSig.dwKeySpec := AT_KEYEXCHANGE; // AT_SIGNATURE;
        pdw := SizeOf(provInfoSig.dwProvType); // �������� ��� ����������
        CryptGetProvParam(hProv, PP_PROVTYPE, @provInfoSig.dwProvType,
          @pdw, 0);
        provInfoSig.cProvParam := 0;
        provInfoSig.rgProvParam := nil;

        FillChar(SigParams, SizeOf(CRYPT_SIGN_MESSAGE_PARA), #0);

        // �������������� ��������� �������
        SigParams.cbSize := SizeOf(CRYPT_SIGN_MESSAGE_PARA);
        SigParams.dwMsgEncodingType := encType;
        SigParams.pSigningCert := CertContext;
        // SigParams.HashAlgorithm.pszObjId := szOID_CP_GOST_R3411;  //("1.2.643.2.2.9")
        SigParams.HashAlgorithm.pszObjId :=
          CertContext.pCertInfo.SignatureAlgorithm.pszObjId;
        SigParams.HashAlgorithm.Parameters.cbData := 0;
        SigParams.pvHashAuxInfo := nil;
        SigParams.cMsgCert := 1; // �������� ���������� � ��������� - 1
        SigParams.rgpMsgCert := @CertContext;
        SigParams.cMsgCrl := 0; // �������� CRL context
        SigParams.rgpMsgCrl := nil;
        SigParams.cAuthAttr := 0;
        SigParams.rgAuthAttr := nil;
        SigParams.cUnauthAttr := 0;
        SigParams.rgUnauthAttr := nil;
        SigParams.dwFlags := 0;
        SigParams.dwInnerContentType := 0;

        SetLength(Data_Array_pointer, 1);
        SetLength(Size_Array_pointer, 1);
        // ����������� ���� ��� �������
        Size_Array_pointer[0] := ms.Size; // FileSize(f1);
        GetMem(Data_Array_pointer[0], Size_Array_pointer[0]);
        ms.ReadBuffer(Data_Array_pointer[0]^, Size_Array_pointer[0]);

        if not CryptSignMessage(@SigParams, False, 1,
          @Data_Array_pointer[0], @Size_Array_pointer[0], nil,
          @pcbSignedBlob) then
        begin
          n := GetLastError;
          if (n = -2148081675) or (n = 2148081675) then
            Show_Inf(
              '�� ������� ��������� ���������. ���������� ���������� ' +
                #13#10 +
                '��� ���������� ��������� ����� � ��������� "������" �' +
                #13#10 +
                '��������� � ��������� ���������� � �������� ������.' +
                #13#10 + '(CryptSignMessage ' + IntToStr(n) + ')')
          else
            Show_inf('�� ������� ��������� ���������' + #13#10 +
                '(CryptSignMessage, ' + IntToStr(n) + ')');
          err := True;
          exit;
        end;
        GetMem(pbSignedBlob, pcbSignedBlob);
        if not CryptSignMessage(@SigParams, False, 1, Data_Array_pointer,
          Size_Array_pointer, pbSignedBlob, @pcbSignedBlob) then
        begin
          n := GetLastError;
          if (n = -2148081675) or (n = 2148081675) then
            show_inf(
              '�� ������� ��������� ���������. ���������� ���������� ' +
                #13#10 +
                '��� ���������� ��������� ����� � ��������� "������" �' +
                #13#10 +
                '��������� � ��������� ���������� � �������� ������.' +
                #13#10 + '(CryptSignMessage ' + IntToStr(n) + ')')
          else
            show_inf('�� ������� ��������� ���������' + #13#10 +
                '(CryptSignMessage, ' + IntToStr(n) + ')');
          err := True;
          exit;
        end;

        ms1.WriteBuffer(pbSignedBlob^, pcbSignedBlob);

        FreeMem(pbSignedBlob);

        CryptDestroyKey(hProv);

        CryptReleaseContext(hProv, 0);

      end
      else
      begin
        ms1.LoadFromStream(ms);
        err := True;
        Show_Inf('������ ������� �������� �������!' + #13#10 +
            '�� ������ ������ ����������');
      end;
    except
      ms1.LoadFromStream(ms);
      err := True;
      Show_inf('������ ������� �������� �������!');
    end;
  finally
    FreeMem(provInfoSig.pwszContainerName);
    FreeMem(provInfoSig.pwszProvName);
    FreeMem(Data_Array_pointer[0]);
  end;
end;


function tForm7_8.AddECP(fXML: string; CertContext: PCCERT_CONTEXT): Boolean;
var
  ms, ms1: TMemoryStream;
  er: Boolean;
  s: string;
begin
  Result := False;
  try
    try
      ms := TMemoryStream.Create;
      ms1 := TMemoryStream.Create;
      // ����� ����� XML
      ms.Clear;
      ms.LoadFromFile(fXML);

      SignXNL(ms, ms1, er, fSert.path, CertContext);
      if not er then
      begin
        s := fXML + '.sig';
        if FileExists(s) then
          DeleteFile(PWideChar(s));
        ms1.SaveToFile(s);
        if not er then // ShowMessage('������� ����� ��������� �������');
          Result := True;
      end;
    except

    end;
  finally
    ms1.free;
    ms.free;
  end;
end;


function tForm7_8.ZipFileXML(fn: string): Boolean;
var
  Zip: TFWZipWriter;
  // s: TStringStream;
  f: string;
  procedure CheckResult(value: integer);
  begin
    if value < 0 then
      raise Exception.Create('������ ���������� ������');
  end;

begin
  Result := False;
  try
    Zip := TFWZipWriter.Create;
    try
      CheckResult(Zip.AddFile(fn + '.sig'));
      f := fn + '.sig.zip';
      if FileExists(f) then
        DeleteFile(PWideChar(f));

      Zip.BuildZip(f);
      // ShowMessage('������������� ����� ��������� �������');
      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;


procedure tForm7_8.Crypt(CountS: integer; ArrayCert: array of PCCERT_CONTEXT;
  Var ms, ms1: TMemoryStream; Var err: Boolean); { ���� "�����������" }
var
  // Store: HCERTSTORE;
  EncParams: CRYPT_ENCRYPT_MESSAGE_PARA;
  cbEncrypted: DWORD;
  i: integer;
  hProv: HCRYPTPROV;
begin
  with ms do
    try
      hProv := 0;
      CryptAcquireContextW(@hProv, Nil, GOST_34, 75, CRYPT_VERIFYCONTEXT);
      try
        ZeroMemory(@EncParams, SizeOf(CRYPT_ENCRYPT_MESSAGE_PARA));
        EncParams.cbSize := SizeOf(CRYPT_ENCRYPT_MESSAGE_PARA);
        EncParams.dwMsgEncodingType := encType;
        EncParams.ContentEncryptionAlgorithm.pszObjId := CertAlgIdToOID
          (CALG_G28147);
        EncParams.HCRYPTPROV := hProv;

        Win32Check(CryptEncryptMessage(@EncParams, CountS, @ArrayCert,
            Memory, Size, nil, @cbEncrypted));
        try
          ms1.SetSize(cbEncrypted);
          Win32Check(CryptEncryptMessage(@EncParams, CountS, @ArrayCert,
              Memory, Size, ms1.Memory, @cbEncrypted));
          ms1.SetSize(cbEncrypted);
        finally
        end;
      finally
        for i := 0 to High(ArrayCert) do
          CertFreeCertificateContext(ArrayCert[i]);
      end;
    finally
    end;
end;

function tForm7_8.CryptoXML(fn: string): Boolean;
var
  ms, ms1: TMemoryStream;
  er: Boolean;
  f: string;
  r1, r2: TResourceStream;
  ACert: array of PCCERT_CONTEXT;
begin
  r1 := TResourceStream.Create(HInstance, 'res_59', RT_RCDATA);
  r2 := TResourceStream.Create(HInstance, 'res_fsrar', RT_RCDATA);
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  er := False;
  try
    try

      SetLength(ACert, 2);

      ms.LoadFromStream(r1);
      ACert[0] := CertCreateCertificateContext(encType, ms.Memory, ms.Size);
      ms.Clear;

      ms.LoadFromStream(r2);
      ACert[1] := CertCreateCertificateContext(encType, ms.Memory, ms.Size);

      ms.Clear;
      ms.LoadFromFile(fn + '.sig.zip');

      Crypt(2, ACert, ms, ms1, er);

      if not er then
      begin
        f := fn + '.sig.zip.enc';
        if FileExists(f) then
          DeleteFile(PWideChar(f));
        ms1.SaveToFile(f);

        // ShowMessage('���������� ����� ��������� �������');
      end;
    except
    end;
  finally
    FreeAndNil(ms1);
    FreeAndNil(ms);
    r1.free;
    r2.free;
  end;
end;

function tForm7_8.WriteXML(fn: string): Boolean;
var
  CertContext: PCCERT_CONTEXT;
begin
  Result := False;
  if fSert.path = '' then
    exit;
  if GetSert(fSert, CertContext) then
    if AddECP(fn, CertContext) then
      if ZipFileXML(fn) then
        if CryptoXML(fn) then
        begin
          DeleteFile(PWideChar(fn + '.sig'));
          DeleteFile(PWideChar(fn + '.sig.zip'));
          Result := True;
        end;
end;


function tForm7_8.GetPasword(RegHost,sInn:string):string;
var s:string;
  TCP: TIdTCPClient;
begin
  Result:='';
  TCP := TIdTCPClient.Create(nil);
  TCP.Host := RegHost;
  TCP.Port:=TCPPort;
  try
    try
      if not TCP.Connected then
        try
          TCP.Connect;
        except
          Result := '';
          TCP.Free;
          exit;
        end;
      s := string(AnsiString(TCP.IOHandler.ReadLn));
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');

      TCP.IOHandler.WriteLn('PCNT' + sInn );
      TCP.IOHandler.WriteLn('0');
      TCP.ReadTimeout := 500;
      s := TCP.IOHandler.ReadLn;
      if (s <> 'EPCNT') and (s <> 'NPCNT') then
        Result := s;
    except
    end;
  finally
    TCP.Socket.Close;
    TCP.Disconnect;
    TCP.Free
  end;
end;

function tForm7_8.ZipFileNew(fn: string): Boolean;
var
  Zip: TFWZipWriter;
  // s: TStringStream;
  f: string;
  procedure CheckResult(value: integer);
  begin
    if value < 0 then
      raise Exception.Create('������ ���������� ������');
  end;

begin
  Result := False;
  try
    Zip := TFWZipWriter.Create;
    try
      CheckResult(Zip.AddFile(fn));
      f := fn + '.zip';
      if FileExists(f) then
        DeleteFile(PWideChar(f));

      Zip.BuildZip(f);
      // ShowMessage('������������� ����� ��������� �������');
      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;



function tForm7_8.WriteDeclBM(sfile, Inn:string; iKvart, iYear:integer):Boolean;
var sPasw, sDir, dirName, Dir, fName, fZip:string;
  idftp: TIdFTP;
  findDir, putFile:Boolean;
  i:Integer;
begin
  Result:=False;
  sPasw := '';
  try
      //������� ������ ������� �� ftp
    try
      sPasw := GetPasword(BMHost,Inn);
    except
      exit;
    end;
    if sPasw = '' then sPasw := GetPasword(BMHost,Inn);

    if sPasw = '' then exit;

    if ZipFileNew(sfile) then
      fZip := sFile + '.zip'
    else
      fZip := sFile;

    IdFTP:=TIdFTP.Create(nil);
    IdFTP.Host:=BMHost;

    IdFTP.Username:='0-'+INN;//�����
    IdFTP.Password:=sPasw;//������
    IdFTP.Passive := True;
    IdFTP.TransferType := ftBinary;

    IdFTP.Connect;
    fName := ExtractFileName(fZip);

    try
      idFTP.List;
    except
    end;

    findDir := False;
    sDir := '0'+ IntToStr(iKvart) + IntToStr(iYear);
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = sDir then
          findDir :=True;
      end;

    if not findDir then
      IDFTP.MakeDir('/'+sDir+'/');
    IdFTP.ChangeDir(sDir);

    //��������� ������ ����������
    try
      idFTP.List;
    except
    end;

    Dir := 'Decl';

    sDir := '/'+sDir+'/'+dir+'/';

    findDir :=False;
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = Dir then
          findDir :=True;
      end;

    if not findDir then
      IDFTP.MakeDir(sDir);
    IdFTP.ChangeDir(Dir);


    //��������� ������ ����������
    try
      idFTP.List;
    except
    end;

    findDir :=False;
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditFile then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = fName then
          findDir :=True;
      end;
    putFile := False;

    try
      IdFTP.Put(fZip,fName,true); //���� ������-����;
      putFile := True;
      DeleteFile(PWideChar(fZip));
    except
    end;

    if putFile then
      Result:=True;


  finally
    if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
  end;
end;




Procedure tForm7_8.toXml(Source : tToXml);
  var SLDecl,SLErr,SlProizv,SLPost,SlLic,SLMinusEan,SLKppI,SLMinusPr, SLOKppI:TStringList;
  BrowseInfo: TBrowseInfo;
  DisplayName : array [0 .. MAX_PATH] of char;
  TitleName, s, d, sn, fname, f_name, sKvart, cKvart, sBM: string;
  lpItemID: PItemIDList;
  Xml: IXMLDocument;
  root, sprav, doc, OO, org: IXMLNode;
  i : integer;
  err, errXsd:Boolean;
begin
  RezInf := '';
  fSert:=Source.fSert_;
  MemoStr := '';


//  �������� ������� � ���������
  GetMinusOst(masErr,Source.SLOstatkiKpp);
  if IsMassErr then Exit;
  CleanRepProducer(Src.que2_sql_);
  //������������ �������� �����
  //btn_printClick(Self);

  SLDecl:=TStringList.Create;
  SLErr := TStringList.Create;
  SlProizv := TStringList.Create;
  SLPost := TStringList.Create;
  SLLic := TStringList.Create;
  SLMinusEan := TStringList.Create;
  SLKppI := TStringList.Create;
  SLMinusPr := TStringList.Create;
  SLOKppI := TStringList.Create;
  try
    if length(Source.Path)>0 then
    begin
      sKvart := Source.cbAllKvart.Items[Source.cbAllKvart.ItemIndex];

      cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
      Src.CiKvart := StrToInt(cKvart);

      Xml := TXMLDocument.Create(nil);
      Xml.Active := false;
      Xml.Options := Xml.Options + [doNodeAutoIndent];
      // ������������ ������ ����������
      GetDataDecl(SLDecl, SLErr, Source.dB, Source.dE); // ��� ������ ������� ������ ����� ��������� �������
      // ������������ ������ �������������� ����������� � ���������� (� �������)
      GetAllProizv(Src.SLOborotKpp_, SlProizv, Source.f11);
      // �������� ������ ����������� ����������� � ���������� (�� ������� �������� ������)
      GetAllSaler(SLDecl, SLPost, SlLic,Source.f11);

      if IsErrLic(SlLic) then Exit;
      // ��� ������������ XML
      with Xml do
      begin
        Active := True;
        Version := '1.0';
        Encoding := 'windows-1251';
        root := AddChild('����');
        with root do
        begin
          SHapkaXML(root, Src.CiKvart, Src.CiYear, Source.seKor, Source.f11, Source.rbKor, Source.rbf43);
          sprav := AddChild('�����������');
          ProizXML(sprav, SlProizv, Source.f11, false);
          PostXML(sprav, SLPost, Source.f11, false);
          doc := AddChild('��������');
          with doc do
          begin
            org := AddChild('�����������');
            OrgXML(org, Source.f11, false,Source.rbf43, Source.dB, Source.dE);
            SLMinusEan.clear;
            for i := 0 to Src.slkpp_.Count - 1 do
            begin
              OO := AddChild('������������');
              // ���������� ����� ������� ���
              GetDataKPPXML(Src.slkpp_[i], SLDecl, SLKppI);
              {$IFDEF DEBUG}
              SaveToFile('Test1.xml');
              {$ENDIF}

              GetDataOKPPXML(Src.SLKpp_[i], Src.SLOborotKpp_, SLOKppI);
              {$IFDEF DEBUG}
              SaveToFile('Test1.xml');
              {$ENDIF}
              // ���������� ������ ������� �� ���
              KPPXML(OO, Src.SLKpp_[i], SLKppI,SLOKppI, SLMinusPr, Source.f11,
                      false, Source.dB, Source.dE, Source.chk_1C);
             {$IFDEF DEBUG}
             SaveToFile('Test1.xml');
             {$ENDIF}
            end;

          end;
        end;
      end;

      {$IFDEF DEBUG}
       xml.SaveToFile('Test1.xml');
      {$ENDIF}

      s := createguid;
      d := DateToStr(GetCurDate(Src.CiKvart));
      sn := Src.fOrg.inn + '_0' + IntToStr(Period_Otch(Src.CiKvart)) + Copy
        (IntToStr(Src.CiYear), 4, 1) + '_' + Copy(d, 1, 2) + Copy(d, 4, 2) + Copy(d,
        7, 4) + '_' + Copy(s, 2, (Length(s) - 2));

      if (SLMinusEan.Count > 0) or (SLMinusPr.Count > 0) or (SLErr.Count > 0) then
        err:=True
      else
        err:=False;

      if err then
        begin
          s:= '����������� ���������� �� ' + Source.cbAllKvart.Text;
          if Source.f11 then
            MemoErr.Add(s + ' ����� 7')
          else
            MemoErr.Add(s + ' ����� 8');

          if SLErr.Count > 0 then
            begin
              MemoErr.Add('������� ������');
              MemoErr.AddStrings(SLErr);
              MemoErr.Add('');
            end;
          if SLMinusPr.Count > 0 then
            begin
              MemoErr.Add('������ � �������� ��������� (�� ���� �� � �������������');
              MemoErr.AddStrings(SLMinusPr);
              MemoErr.Add('');
            end;
          if SLMinusEan.Count > 0 then
            begin
              MemoErr.Add('������ � �������� ��������� (�� ���� ���13)');
              MemoErr.AddStrings(SLMinusEan);
            end;
        end;

      if Source.f11 then
      begin
        fname := Source.Path + '\07_' + sn + '.xml';
        f_name := Source.Path + '\err_07_' + sn + '.ini';
      end
      else
      begin
        fname := Source.Path + '\08_' + sn + '.xml';
        f_name := Source.Path + '\err_08_' + sn + '.ini';
      end;

      Xml.SaveToFile(fname);
      { TODO : �������� ����� vxd � /temp.vxd }
      errXsd := FindErrXSD(fname,Source.f11, Source.rbf43, s);
      s:= '������������ ���������� ��������� ��� ������';
      if errXsd then
        if err or (not errXsd) then
          s:= '������������ ���������� ��������� � ��������';

        if not Err then
          begin
            if Source.FindEcp then
              begin
                if WriteXML(fname) then
                  begin
                    sBM := '';
                    if Source.chk_declBM then
                      begin
                        try
                          if WriteDeclBM(fname, Src.fOrg.Inn, Source.CurKvart, Source.CurYear) then
                            sBM := #10#13 + '����� ���������� ��������� �� ������� ��';
                        except
                          sBM := #10#13 + '����� ���������� �� ��������� �� ������� ��';
                        end;
                      end
                    else
                      sBM := #10#13 + '����� ���������� �� ��������� �� ������� ��';

                    Show_Inf(s + #10#13 + '���������� ���������' + sBM);

                  end
                else
                  Show_Inf(s + #10#13 +
                         '���������� �� ���������. ������ ������������');
              end
            else
              begin
                Show_Inf(s + #10#13 +
                         '��� �� �������, ���������� �� ���������' + #10#13 +
                         '����� ���������� �� ��������� �� ������� ��');
              end;
          end
        else
          Show_Inf(s + #10#13 + '���������� �� ���������'+ #10#13 +
                   '����� ���������� �� ��������� �� ������� ��');
    end
  else
    Show_Inf('������ �������� ����������');

  finally
    //MemoErr.Free;
    XML:=nil;
    SLMinusPr.Free;
    FreeStringList(SLKppI);
    FreeStringList(SLMinusEan);
    SLLic.Free;
    FreeStringList(SLPost);
    FreeStringList(SlProizv);
    SLErr.Free;
    SLDecl.Free;
  end;
end;
end.
