unit dfmCorSal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ADODB;

type
  TfmCorSal = class(TForm)
    lbllb1: TLabel;
    lbllb4: TLabel;
    cbbKPP: TComboBox;
    cbbWap: TComboBox;
    leDecl: TLabeledEdit;
    btnCor: TButton;
    btnClose: TButton;
    btnForm: TButton;
    leSal: TLabeledEdit;
    chk1: TCheckBox;
    cbbKvart: TComboBox;
    leDelta: TLabeledEdit;
    tmr1: TTimer;
    lbl1: TLabel;
    cbbsInn: TComboBox;
    lbl2: TLabel;
    cbbsKpp: TComboBox;
    lbl3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbbKPPChange(Sender: TObject);
    procedure chk1Click(Sender: TObject);
    procedure cbbKvartChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCorSal: TfmCorSal;
  SLSal: TStringList;

implementation

uses Global, GlobalConst, GlobalUtils, dfmMain, dfmWork;

{$R *.dfm}

procedure TfmCorSal.cbbKPPChange(Sender: TObject);
begin
  leDecl.Text := '';
  leSal.Text := '';
  leDelta.Text := '';
  chk1.Checked := False;
end;

procedure TfmCorSal.cbbKvartChange(Sender: TObject);
var idx:Integer;
begin
  idx := cbbKvart.ItemIndex;
  if idx = -1 then Exit;
  leDecl.Text := '';
  leSal.Text := '';
  leDelta.Text := '';
  chk1.Checked := False;
end;

procedure TfmCorSal.chk1Click(Sender: TObject);
begin
  if chk1.Checked then
    begin
      leSal.Color:=$00F4FFF5;
      leSal.Text :='0';
    end
  else
    begin
      leSal.Color:=clWhite;
      leSal.Text :='';
    end;
  leSal.ReadOnly :=chk1.Checked;
end;

procedure TfmCorSal.FormCreate(Sender: TObject);
begin
  FillComboFNS(cbbWap, FNSCode, True);
  AssignCBSL(cbbKPP, SLKPP);
  AssignCBSL(cbbKPP, SLKPP);
  SLSal := TStringList.Create;
end;

procedure TfmCorSal.FormDestroy(Sender: TObject);
begin
  FreeStringList(SLSal);
end;

procedure TfmCorSal.FormShow(Sender: TObject);
var StrSQLS:string;
  qr:TADOQuery;
  SL:TStringList;
begin
  SL:=TStringList.Create;
  try
    SL.Assign(SLPerRegYear);
    SL.Delete(0);
    AssignCBSL(cbbKvart, SL);
    if fmMain.cbAllKvart.ItemIndex > 0 then
      cbbKvart.ItemIndex := fmMain.cbAllKvart.ItemIndex-1;
    cbbKvartChange(cbbKvart);
    StrSQLS := StrSQL_Saler;
    qr := TADOQuery.Create(fmWork);
    qr.Connection := fmWork.con2ADOCon;
    FillSLSQL(qr, StrSQLS, SLSal);
    AssignCBSL(cbbsInn, SLSal);
    cbbsInn.ItemIndex := -1;
  finally
    qr.Free;
    SL.Free;
  end;
end;

end.
