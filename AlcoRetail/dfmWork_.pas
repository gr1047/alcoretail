unit dfmWork;

interface

uses
  Forms, Dialogs, DB, ADODB, ImgList, Controls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, ComCtrls, StdCtrls, Buttons, JvExExtCtrls,
  JvExtComponent, JvRollOut, Classes, ExtCtrls, IniFiles, ShlObj, SysUtils,
  Registry, Windows, Grids, AdvObj, BaseGrid, AdvGrid, xmldoc, xmlintf, Menus;

type
  TfmWork = class(TForm)
    Panel_JvRoll: TPanel;
    JvRoll_Set: TJvRollOut;
    sb_Org: TSpeedButton;
    lbl2: TLabel;
    sb_Set: TSpeedButton;
    lbl3: TLabel;
    sb_Admin: TSpeedButton;
    lbl4: TLabel;
    sb_help: TSpeedButton;
    lbl1: TLabel;
    JvRoll_View: TJvRollOut;
    sb_Ost: TSpeedButton;
    lbl5: TLabel;
    sb_Obor: TSpeedButton;
    lbl6: TLabel;
    sb_Statictic: TSpeedButton;
    lbl7: TLabel;
    sb_Saler: TSpeedButton;
    lbl8: TLabel;
    JvRoll_Decl: TJvRollOut;
    lblLabRefs5: TLabel;
    sb_DataExchange: TSpeedButton;
    lbl9: TLabel;
    sb_MoveProduction: TSpeedButton;
    lbl10: TLabel;
    sb_Decl: TSpeedButton;
    l_Decl: TLabel;
    sb_Ref: TSpeedButton;
    lbl11: TLabel;
    sb_Main: TSpeedButton;
    OpenDialog1: TOpenDialog;
    dlgOpen_Import: TOpenDialog;
    con1ADOCon: TADOConnection;
    idtcpclntTCP410___1: TIdTCPClient;
    il2: TImageList;
    il3: TImageList;
    il1: TImageList;
    que1_sql: TADOQuery;
    con2ADOCon: TADOConnection;
    dlgSave_Export: TSaveDialog;
    SprADOCon: TADOConnection;
    que2_sql: TADOQuery;
    pnl1: TPanel;
    PanelForm_: TPanel;
    pbWork: TProgressBar;
    btn_Correct: TSpeedButton;
    lb1: TLabel;
    pnl2: TPanel;
    lbOrg: TLabel;
    lbVers: TLabel;
    ADOConnection1: TADOConnection;
    sb_journ: TSpeedButton;
    lb_1: TLabel;
    PanelForm: TPanel;
    pm_update: TPopupMenu;
    mni_Save: TMenuItem;
    mni_View: TMenuItem;
    mniN2: TMenuItem;
    mni_upd: TMenuItem;
    N1: TMenuItem;
    mni_Del: TMenuItem;
    mni_DeleteID: TMenuItem;
    mni_delAll: TMenuItem;
    procedure sb_MainClick(Sender: TObject);
    procedure sb_DataExchangeClick(Sender: TObject);
    procedure sb_MoveProductionClick(Sender: TObject);
    procedure sb_DeclClick(Sender: TObject);
    procedure JvRoll_DeclExpand(Sender: TObject);
    procedure JvRoll_ViewExpand(Sender: TObject);
    procedure Panel_JvRollResize(Sender: TObject);
    procedure sb_RefClick(Sender: TObject);
    procedure sb_OstClick(Sender: TObject);
    procedure sb_OborClick(Sender: TObject);
    procedure sb_SalerClick(Sender: TObject);
    procedure sb_StaticticClick(Sender: TObject);
    procedure sb_SetClick(Sender: TObject);
    procedure sb_OrgClick(Sender: TObject);
    procedure sb_AdminClick(Sender: TObject);
    procedure sb_helpClick(Sender: TObject);
    procedure JvRoll_DeclClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure JvRoll_Collapse(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CorrectClick(Sender: TObject);
    procedure sb_journClick(Sender: TObject);
    procedure JvRoll_EGAISExpand(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure btn_MAinClick(Sender: TObject);
    procedure btn_PartnerClick(Sender: TObject);
    procedure btn_WayBillClick(Sender: TObject);
    procedure btn_RestClick(Sender: TObject);
    procedure pm_updatePopup(Sender: TObject);
    procedure mni_SaveClick(Sender: TObject);
    procedure mni_ViewClick(Sender: TObject);
    procedure mni_updClick(Sender: TObject);
    procedure mni_DelClick(Sender: TObject);
    procedure mni_DeleteIDClick(Sender: TObject);
    procedure mni_delAllClick(Sender: TObject);
  private
    { Private declarations }
    SelRepId:string;
    colMouse, rowMouse : Integer;

    procedure HideIt;
    procedure SaveIni;
    procedure LoadIniF;
    procedure GetDir;

    // procedure LoadDir;
  public
    { Public declarations }
    function LoadReg: Boolean;
  end;

var
  fmWork: TfmWork;
  IsWork: Boolean;
  CountPanel, RollExp: Integer;

implementation



uses GlobalConst, Global, Global2, GlobalUtils,
  dfmMain, dfmDataExchange, dfmMove, dfmRef, dfmDecl, dfmOstatki, dfmOborot,
  dfmSaler, dfmSetting, dfmOrg, dfmAdmim, dfmShowInf, dfmGetOrg, dfmNewOrg,
  About, dfmStatistic, dfmCorrect, dfmJournal;

{$R *.dfm}
//{$R admin.res}

procedure TfmWork.sb_DataExchangeClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmDataExchange = nil then
    fmDataExchange := TfmDataExchange.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmDataExchange.Parent := PanelForm;
  fmDataExchange.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmDataExchange.Show;
end;

procedure TfmWork.sb_AdminClick(Sender: TObject);
var
  ButSel: Integer;
  pas: string;
begin
//  pnl_Query.Visible := False;
  try
    ButSel := Show_Inf('������ ��������������', fEnter, pas);
    if ButSel <> mrOk then
      exit;
    if pas <> '257302' then
    begin
      Show_Inf('�������� ������', fError);
      exit;
    end;
    if fmAdmim = nil then
      fmAdmim := TfmAdmim.Create(Self);
    HideIt;
    sb_Set.Flat := True;
    sb_Org.Flat := True;
    sb_Admin.Flat := False;
    fmAdmim.Parent := PanelForm;
    fmAdmim.Align := alClient;
    ChangeMainForm;
    fMainForm := fOder;

    fmAdmim.Show;
  finally
    fmShowInf.Edit1.Text := '';
  end;
end;

procedure TfmWork.sb_DeclClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmDecl = nil then
    fmDecl := TfmDecl.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmDecl.Parent := PanelForm;
  fmDecl.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmDecl.Show;
end;

procedure TfmWork.sb_helpClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
end;

procedure TfmWork.sb_journClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmJournal = nil then
    fmJournal := TfmJournal.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmJournal.Parent := PanelForm;
  fmJournal.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmJournal.Show;
end;

procedure TfmWork.sb_MainClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmMain = nil then
    fmMain := TfmMain.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmMain.Parent := PanelForm;
  fmMain.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmMain.Show;
end;

procedure TfmWork.sb_MoveProductionClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmMove = nil then
    fmMove := TfmMove.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmMove.Parent := PanelForm;
  fmMove.Align := alClient;
  fMainForm := fDvizenie;
  fmMove.Show;
end;

procedure TfmWork.sb_OborClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmOborot = nil then
    fmOborot := TfmOborot.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmOborot.Parent := PanelForm;
  fmOborot.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmOborot.Show;
end;

procedure TfmWork.sb_OrgClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmOrg = nil then
    fmOrg := TfmOrg.Create(Self);
  HideIt;
  sb_Set.Flat := True;
  sb_Org.Flat := False;
  sb_Admin.Flat := True;
  fmOrg.Parent := PanelForm;
  fmOrg.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmOrg.butReftClick(Application);
  fmOrg.Show;
end;

procedure TfmWork.sb_OstClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmOstatki = nil then
    fmOstatki := TfmOstatki.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmOstatki.Parent := PanelForm;
  fmOstatki.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmOstatki.Show;
end;

procedure TfmWork.sb_RefClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmRef = nil then
    fmRef := TfmRef.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmRef.Parent := PanelForm;
  fmRef.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmRef.Show;
end;

procedure TfmWork.sb_SalerClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmSaler = nil then
    fmSaler := TfmSaler.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmSaler.Parent := PanelForm;
  fmSaler.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmSaler.Show;
end;

procedure TfmWork.sb_SetClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmSetting = nil then
    fmSetting := TfmSetting.Create(Self);
  HideIt;
  sb_Set.Flat := False;
  sb_Org.Flat := True;
  sb_Admin.Flat := True;
  fmSetting.Parent := PanelForm;
  fmSetting.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmSetting.Show;
end;

procedure TfmWork.sb_StaticticClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmStatistic = nil then
    fmStatistic := TfmStatistic.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmStatistic.Parent := PanelForm;
  fmStatistic.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmStatistic.Show;
end;

procedure TfmWork.SpeedButton2Click(Sender: TObject);
begin
//  if fmEGAIS = nil then
//    fmEGAIS := TfmEGAIS.Create(Self);
//  HideIt;
//  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
//    .Flat := False;
//  fmEGAIS.Parent := PanelForm;
//  fmEGAIS.Align := alClient;
//  ChangeMainForm;
//  fMainForm := fOder;
//  fmEGAIS.Show;
end;

procedure TfmWork.btn_CorrectClick(Sender: TObject);
begin
//  pnl_Query.Visible := False;
  if fmCorrect = nil then
    fmCorrect := TfmCorrect.Create(Self);
  HideIt;
  if (Sender is TSpeedButton) then (Sender as TSpeedButton)
    .Flat := False;
  fmCorrect.Parent := PanelForm;
  fmCorrect.Align := alClient;
  ChangeMainForm;
  fMainForm := fOder;
  fmCorrect.Show;
end;

procedure TfmWork.btn_MAinClick(Sender: TObject);
begin
//  pnl_Query.Visible := True;
//  if fmMAinUtm = nil then
//    fmMAinUtm := TfmMAinUtm.Create(Self);
//  HideIt;
//  if (Sender is TSpeedButton) then (Sender as TSpeedButton).Flat := False;
//  fmMAinUtm.Parent := PanelForm;
//  fmMAinUtm.Align := alClient;
//  fmMAinUtm.Show;
end;

procedure TfmWork.btn_PartnerClick(Sender: TObject);
begin
//  pnl_Query.Visible := True;
//  if fmPartner = nil then
//    fmPartner := TfmPartner.Create(Self);
//  HideIt;
//  if (Sender is TSpeedButton) then (Sender as TSpeedButton).Flat := False;
//  fTypeFormUTM := fPartner;
//  fmPartner.Parent := PanelForm;
//  fmPartner.Align := alClient;
//  fmPartner.Show;
end;

procedure TfmWork.btn_RestClick(Sender: TObject);
begin
//  pnl_Query.Visible := True;
//  if fmRest = nil then
//    fmRest := TfmRest.Create(Self);
//  HideIt;
//  if (Sender is TSpeedButton) then (Sender as TSpeedButton).Flat := False;
//  fTypeFormUTM := fRest;
//  fmRest.Parent := PanelForm;
//  fmRest.Align := alClient;
//  fmRest.Show;
end;

procedure TfmWork.btn_WayBillClick(Sender: TObject);
begin
//  pnl_Query.Visible := True;
//  if fmWayBill = nil then
//    fmWayBill := TfmWayBill.Create(Self);
//  HideIt;
//  if (Sender is TSpeedButton) then (Sender as TSpeedButton).Flat := False;
//  fTypeFormUTM := fWayBill;
//  fmWayBill.Parent := PanelForm;
//  fmWayBill.Align := alClient;
//  fmWayBill.Show;
end;

procedure TfmWork.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if fMainForm = fDvizenie then
    if fmMove <> nil then
      ChangeMainForm;
end;

procedure TfmWork.FormCreate(Sender: TObject);
var
  Upd: Boolean;
  dfm: TfmNewOrg;
  tIniF: TIniFile;
  fn,s,StrPage, JaCartaFSRARID: string;
  WB:Boolean;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\' + NameProg +
    '_inn.ini';
  tIniF := TIniFile.Create(fn);
  VostDecl:=False;
  VostBase:=False;
  JvRoll_Decl.Expand;
  // �������� ��������
  //�������� �� ������������� ���������� ��������� � ����� ������������
//  LoadMainDir;
  CreateSL;
//  CreateSLUTM;
//  orgCount:= CountOrg(True);
//  if orgCount = 1 then
//    ReadFSRARID;
//  fOrgUTM.ShortName := CapNewOrg;
//  fOrgUtm.FSRAR_ID := CapNewOrg;

  fmGetOrg := TfmGetOrg.Create(Application);

  try
    Upd := False;
    idxKvart := -1;
    fmGetOrg.ShowModal;
    // ������ ����� ��� �������� ��������������� ������
    SetMasYear;
    if fmGetOrg.ModalResult = mrOk then
    begin
      // �������� ������ ������� ��� ��������� ������ �� ����� ����������� �� �������
      if newclient then
      begin
        dfm := TfmNewOrg.Create(Self);
        dfm.ShowModal;
//        iTimeOut := ReadIniInt(fIniMain, SecParams, SetTimeOut, defaultTimeOut);
        if (dfm.ModalResult = mrOk) then
        begin
          if not NewOrganization then
          begin
            Application.Terminate;
            exit;
          end
          else
          begin
            // ���������� ���������� ���� � Ini �����
            tIniF.WriteString('LastInn', 'INN', fOrg.Inn);
            CreateParamBase(fOrg.Inn);
            IsWork := True;
          end;
        end
        else if (dfm.ModalResult = mrRetry) then
        begin
          LoadIniF;
          // �������� ���������� �������� ����
          CreatePath(fOrg.Inn);
          // �������� ����������� ��� � ��������� ������ ����������������� ��������
          ReadKPP_Ini(SLPerReg);
          CurDate := GetCurDate(CurKvart);
          IsWork := True;
        end
        else
        begin
          Application.Terminate;
          exit;
        end;
      end
      else
      begin
        // �������� ������������� Ini �����
        ExtIniF := FileExists(GetSpecialPath(CSIDL_PROGRAM_FILES)
            + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini');
        IsWork := True;
        // �������� ���������� �������� ����
        CreatePath(fOrg.Inn);
        if not ExistBaseDecl(fOrg.Inn) then
          CreatePathReg;
        // ��������� ������ �� ������� ��� Ini �����
        if ExtIniF then
          LoadIniF
        else
        begin
          LoadReg;
          SaveIni;
        end;
        // �������� ������������� ���������� AlcoRetail
        GetDir;
        // �������� ������������� ���� ����������
        LoadDir;
        // �������� ����������� ��� � ��������� ������ ����������������� ��������
        ReadKPP_Ini(SLPerReg);
        CurDate := GetCurDate(CurKvart);
//        LoadKPPDir(fOrgUTM.Kpp);
//        ReadIniInnParams;

      end;
//      if EGAIS then
//        if SetFSRAR_Id <> '' then
//        begin
//          WB := EnabledService(nServT, s);
//          StartServAll(WB);
////          StartServAll(True);
//        end;
//      if EGAIS then
//      begin
//        if NewEgais  then
//        begin
//          LoadTmpDir;
//          fOrgUTM.Inn := fOrg.Inn;
//        end
//        else
//        begin
//          ReadDataEGAIS(False);
//          EnabledService(nServT, StrPage);
//          JaCartaFSRARID := PageJaCartaFSRARID(StrPage);
//          if JaCartaFSRARID <> SetFSRAR_Id then
//          begin
//            Show_Inf(ERR_029 + #10#13 + '����� �� ��� ' + #9#9 + SetFSRAR_Id + #10#13 +
//              '����� �� JaCarta ' + #9 + JaCartaFSRARID, fError);
//            EGAIS := False;
//            CloseEGAIS;
//          end;
//        end;
//        FullDataOrg;
//      end;
    end
    else
    begin
      IsWork := False;
      exit;
    end;

    fmWork.Caption := 'AlcoRetail, ������ ' + exe_version;
    SLKPP.Sort;
    // ���������� ��������� (���� ������� ����������)
    if UpdateApp(BMHost, fOrg.Inn, exe_version, path_exe) then
    begin
      Application.Terminate;
      Upd := True;
      exit;
    end;

    if SLKpp.Count = 1 then
      lbOrg.Caption := String(fOrg.SelfName) + '   ��� ' +  String(fOrg.Inn) + '   ��� ' + String(fOrg.Kpp0)
    else
      lbOrg.Caption := String(fOrg.SelfName) + '   ��� ' +  String(fOrg.Inn) + '   �������� ��� ' + String(fOrg.Kpp0);
    lbVers.Caption := '������ ' + exe_version + '   ';

    WriteCheckKpp;

    if not Upd then
    begin
      fMainForm := fOder;
      JvRoll_Decl.Expand;
      // ���������� ������� ����� ���������
      FillSLFNS(SLFNS, FNSCode);
      // ��������� ����� ���� Amount � ������� ���������
      LongAmount(que1_sql);
      // ���������� ������� ����������� ��� ����������� ���������
      SetMasKvartal;
      // ���� ��������� ����������� ������������ ����� ���� "� ���������"
      AboutBox := TAboutBox.Create(Application);
      AboutBox.PB.Visible := True;
      AboutBox.PB.Position := 0;
      AboutBox.PB.Min := 0;
      AboutBox.PB.Max := 0;
      AboutBox.Show;
      AboutBox.lbl_Version.Caption := FileVersion(ParamStr(0));
      Application.ProcessMessages;

      // ���������� ������ ���
      SetDataKpp(que1_sql);
      // ������ ������������� ������������
      SetECP;
      // ����� ����������� �� ���
      FindEcp := FindSert(fOrg.Inn);
      if SLPerReg.Count = 0 then
        if slKpp.Count = 0 then
          begin
            s:=string(fSert.kpp);
            if s = '' then
            begin
              s:=fOrg.Inn;
              s:=Copy(s,0,4);
              s:=s+'01001';
              SLKpp.Add(s);
            end;
          end;
      // ���������� ������� �� �����������
      if FindEcp then
        SetDataFromSert;
      //?????????????
      CreateInventTable(que1_sql);
      // ���������� �������� ������������
      Upd_LocRef(que1_sql, que2_sql, AboutBox.PB, AboutBox.lHelp);
      if (CurYear <> 0) and (CurKvart <> 0) then
      begin
        dBeg := Kvart_Beg(CurKvart, CurYear);
        dEnd := Kvart_End(CurKvart, CurYear);
        //������ ��������� � ���������� ���������
//        RashodSmallOst(que1_sql);
//        CorrectSmallOst(que1_sql)
      end;
      // �������� ������ ����������
      GetAllDataDecl(que1_sql, que2_sql, AboutBox.PB, SLAllDecl, SLDataOst,
        AboutBox.lHelp);
      // �������� �������� �� ������� ��������� �������
      GetOstAsc(fmWork.que1_sql);
      if (CurYear <> 0) and (CurKvart <> 0) then
      begin
        dBeg := Kvart_Beg(CurKvart, CurYear);
        dEnd := Kvart_End(CurKvart, CurYear);
        //������ ��������� � ���������� ���������
        ChangeKvart(dBeg, dEnd);
      end;
      PanelForm.Visible := True;
    end;
  finally
    AboutBox.free;
    fmGetOrg.free;
    tIniF.free;
  end;
end;

procedure TfmWork.FormDestroy(Sender: TObject);
var NewPath:string;
begin
  SaveIni;
  FreeSL;
//  FreeSLUTM;
//{$IFDEF RELEASE}
  Application.ProcessMessages;
  fmWork.con2ADOCon.Close;
  fmWork.con1ADOCon.Close;
  if not VostDecl then
    // �������� ��������� ����� ����
    DB2Arh(db_decl, path_arh + '\Decl_BackUp_' + fOrg.Inn + '_' + DTPart + '.rbk');

  if not VostBase then
    // �������� ��������� ����� ������������
    DB2Arh(db_spr, path_arh + '\Refs_BackUp_' + fOrg.Inn + '_' + DTPart + '.rbk');

//{$ENDIF}
  NewPath := WinTemp + NameProg;

  if DirectoryExists(NewPath) then
    DelDir(NewPath)
end;

procedure TfmWork.FormShow(Sender: TObject);
begin
  Panel_JvRollResize(Panel_JvRoll);
  sb_MainClick(sb_Main);
end;

procedure TfmWork.HideIt;
var
  i, j: Integer;
begin
  for i := 0 to Panel_JvRoll.ControlCount - 1 do
    if Panel_JvRoll.Controls[i] is TJvRollOut then
      for j := 0 to (Panel_JvRoll.Controls[i] as TJvRollOut).ControlCount - 1 do
        if (Panel_JvRoll.Controls[i] as TJvRollOut)
          .Controls[j] is TSpeedButton then
        begin ((Panel_JvRoll.Controls[i] as TJvRollOut)
              .Controls[j] as TSpeedButton)
          .Flat := True;
((Panel_JvRoll.Controls[i] as TJvRollOut).Controls[j] as TSpeedButton)
          .Invalidate;
        end;
end;

procedure TfmWork.JvRoll_DeclClick(Sender: TObject);
begin
  sb_MainClick(Self);
  sb_Main.Flat := False;
end;

procedure TfmWork.JvRoll_DeclExpand(Sender: TObject);
var CountPan:integer;
begin
  if EGAIS then
    CountPan := 4
  else
    CountPan := 3;

  RollExp := 1;
  JvRoll_View.Collapse;
  JvRoll_Set.Collapse;
//  JvRoll_EGAIS.Collapse;
  JvRoll_Decl.Height := Panel_JvRoll.Height - (ButHeigh + 1) * CountPan;
  Panel_JvRollResize(Panel_JvRoll);
end;

procedure TfmWork.JvRoll_EGAISExpand(Sender: TObject);
begin
  RollExp := 3;
  JvRoll_Decl.Collapse;
  JvRoll_Set.Collapse;
  JvRoll_View.Collapse;
//  JvRoll_EGAIS.Height := Panel_JvRoll.Height - (ButHeigh + 1) * 4;
//  if EGAIS then
//    SetFSRAR_Id := GetFSRAR_Id;
  if NewEGAIS then
  begin
//    if not QueryDataOrg(SetFSRAR_ID, fOrg.Inn,True) then
      Show_Inf('������ ������ ������� � ���', fError);
  end;
  Panel_JvRollResize(Panel_JvRoll);
end;

procedure TfmWork.JvRoll_ViewExpand(Sender: TObject);
var CountPan:integer;
begin
  if EGAIS then
    CountPan := 4
  else
    CountPan := 3;
  RollExp := 2;
  JvRoll_Decl.Collapse;
  JvRoll_Set.Collapse;
//  JvRoll_EGAIS.Collapse;
  JvRoll_View.Height := Panel_JvRoll.Height - (ButHeigh + 1) * CountPan;
  Panel_JvRollResize(Panel_JvRoll);
end;

procedure TfmWork.Panel_JvRollResize(Sender: TObject);
Var
  r, i, j: Integer;

begin
  //��������� ��������� �������
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is (TJvRollOut) then
      if ((Sender as TPanel).Controls[i] as (TJvRollOut)).Name = 'JvRoll_EGAIS' then
        if EGAIS then
          ((Sender as TPanel).Controls[i] as (TJvRollOut)).Visible := True
        else
          ((Sender as TPanel).Controls[i] as (TJvRollOut)).Visible := False;

  // ������� ������ ������ �� ������ ������
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is (TJvRollOut) then
  ((Sender as TPanel).Controls[i] as (TJvRollOut))
      .ButtonHeight := ButHeigh;

  // ������� ��������� ������ �� ������ ������
  r := ((Sender as TPanel).Width - sb_Main.Width) div 3;
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is (TJvRollOut) then
      for j := 0 to ((Sender as TPanel).Controls[i] as (TJvRollOut))
        .ControlCount - 1 do
        if (((Sender as TPanel).Controls[i] as (TJvRollOut))
            .Controls[j] is TSpeedButton) then
  (((Sender as TPanel).Controls[i] as (TJvRollOut))
              .Controls[j] as TSpeedButton)
          .Left := r;

  // ������� ��������� �������� � ������� �� ������ ������
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is (TJvRollOut) then
      for j := 0 to ((Sender as TPanel).Controls[i] as (TJvRollOut))
        .ControlCount - 1 do
        if (((Sender as TPanel).Controls[i] as (TJvRollOut))
            .Controls[j] is TLabel) then
        begin
          r := ((Sender as TPanel).Width - (((Sender as TPanel).Controls[i] as
                  (TJvRollOut)).Controls[j] as TLabel).Width) div 2;
          if r > 1 then (((Sender as TPanel).Controls[i] as (TJvRollOut))
                .Controls[j] as TLabel)
            .Left := r
          else (((Sender as TPanel).Controls[i] as (TJvRollOut))
                .Controls[j] as TLabel)
            .Left := 2;
        end;
//
  // �������� ���������� �������
  CountPanel := 0;
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is (TJvRollOut) then
      if ((Sender as TPanel).Controls[i] as (TJvRollOut)).Name = 'JvRoll_EGAIS' then
      begin
        if EGAIS then
          Inc(CountPanel);
      end
      else
        Inc(CountPanel);

  if not JvRoll_Decl.Collapsed then
  begin
    JvRoll_Decl.Height := Panel_JvRoll.Height - (ButHeigh + 1) * (CountPanel-1);
  end;
  if not JvRoll_View.Collapsed then
  begin
    JvRoll_View.Height := Panel_JvRoll.Height - (ButHeigh + 1) * (CountPanel-1);
  end;
  if not JvRoll_Set.Collapsed then
  begin
    JvRoll_Set.Height := Panel_JvRoll.Height - (ButHeigh + 1) * (CountPanel-1);
  end;

  if (JvRoll_Decl.Collapsed) and (JvRoll_View.Collapsed) and
    (JvRoll_Set.Collapsed) then
    case RollExp of
      1:
        JvRoll_Decl.Expand;
      2:
        JvRoll_View.Expand;
      3:
        JvRoll_Set.Expand;
    end;
end;

procedure TfmWork.pm_updatePopup(Sender: TObject);
begin
  if (pm_update.PopupComponent is TAdvStringGrid) then
  begin
    if SelRepId = '' then
    begin
      mni_DeleteID.Visible := False;
      mni_DeleteID.Caption := '�������';
    end
    else
    begin
      mni_DeleteID.Visible := True;
      mni_DeleteID.Enabled := True;
      mni_DeleteID.Caption := '������� ������� � �� ' + SelRepId;
    end;
    if ((pm_update.PopupComponent as TAdvStringGrid).Name = 'AdvSG_IN') then
    begin
      mni_View.Enabled := False;
      mni_Save.Enabled := False;
    end
    else
    begin
      mni_View.Enabled := True;
      mni_Save.Enabled := True;
    end;
  end;

end;

procedure TfmWork.JvRoll_Collapse(Sender: TObject);
var
  i: Integer;
begin
  if (Sender as TJvRollOut).Parent is TPanel then
  begin
    for i := 0 to ((Sender as TJvRollOut).Parent as TPanel).ControlCount - 1 do
      if ((Sender as TJvRollOut).Parent as TPanel)
        .Controls[i] is TJvRollOut then
          (((Sender as TJvRollOut).Parent as TPanel).Controls[i] as TJvRollOut)
          .Height := (ButHeigh + 1);

    Panel_JvRollResize((Sender as TJvRollOut).Parent as TPanel);
  end;
end;

function TfmWork.LoadReg: Boolean;
var
  Reg: TRegistry;
  Key: String;
  d: TDateTime;
  cYear, cMonth, cDay: word;
begin
  Result := False;
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_CURRENT_USER;
    Key := 'SOFTWARE\' + NameProg + '\' + fOrg.Inn;
    Result := Reg.KeyExists(Key);
    if Reg.OpenKey(Key, True) then
    begin
      con1ADOCon.Close;
      con2ADOCon.Close;

      con1ADOCon.ConnectionString :=
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + db_decl +
        ';Persist Security Info=False;Jet OLEDB:Database Password=' + fOrg.Inn;
      con2ADOCon.ConnectionString :=
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + db_spr +
        ';Persist Security Info=False;Jet OLEDB:Database Password=' + psw_loc;

      if Reg.ValueExists('VersionInfo') then
        exe_version := Reg.ReadString('VersionInfo')
      else
      begin
        exe_version := FileVersion(ParamStr(0));
        Reg.WriteString('VersionInfo', exe_version);
      end;

      if exe_version <> FileVersion(ParamStr(0)) then
        exe_version := FileVersion(ParamStr(0));

//      if Reg.ValueExists('RegHost') then
//        clientTCP410.Host := Reg.ReadString('RegHost')
//      else
//      begin
//        clientTCP410.Host := BMHost;
//        Reg.WriteString('RegHost', clientTCP410.Host);
//      end;

      // d:=Now();
      d := GetCurDate(CurKvart);
      if Reg.ValueExists('Date') then
        d := Reg.ReadDate('Date')
      else
      begin
        Reg.WriteDate('Date', d);
      end;

      DecodeDate(d, cYear, cMonth, cDay);
      // ������� �������
      CurKvart := Kvartal(cMonth);
      fOrg.Kvart := CurKvart;
      // ������� ���
      CurYear := cYear;
      fOrg.Year := CurYear;

      if Reg.ValueExists('ProcOst_11') then
        ProcOst_11 := Reg.ReadInteger('ProcOst_11')
      else
        ProcOst_11 := 0;

      if Reg.ValueExists('ProcOst_12') then
        ProcOst_12 := Reg.ReadInteger('ProcOst_12')
      else
        ProcOst_12 := 0;

      if Reg.ValueExists('Sv') then
        svertka := Reg.ReadInteger('Sv')
      else
        svertka := -1;

      if Reg.ValueExists('RukF') then
        fOrg.Ruk.RukF := Reg.ReadString('RukF')
      else
        fOrg.Ruk.RukF := '';

      if Reg.ValueExists('RukI') then
        fOrg.Ruk.RukI := Reg.ReadString('RukI')
      else
        fOrg.Ruk.RukI := '';

      if Reg.ValueExists('RukO') then
        fOrg.Ruk.RukO := Reg.ReadString('RukO')
      else
        fOrg.Ruk.RukO := '';

      if Reg.ValueExists('BuhF') then
        fOrg.Buh.BuhF := Reg.ReadString('BuhF')
      else
        fOrg.Buh.BuhF := '';
      if Reg.ValueExists('BuhI') then
        fOrg.Buh.BuhI := Reg.ReadString('BuhI')
      else
        fOrg.Buh.BuhI := '';
      if Reg.ValueExists('BuhO') then
        fOrg.Buh.BuhO := Reg.ReadString('BuhO')
      else
        fOrg.Buh.BuhO := '';
      if Reg.ValueExists('KPP0') then
      begin
        fOrg.kpp0 := ShortString(Reg.ReadString('KPP0'));
//        KppCur := fOrg.kpp0;
      end
      else
        fOrg.kpp0 := '';

      if Reg.ValueExists('NPhone') then
        fOrg.NPhone := Reg.ReadString('NPhone')
      else
        fOrg.NPhone := '(342)000-00-00';
      if Reg.ValueExists('SelfName') then
      begin
        orgname := Reg.ReadString('SelfName');
        fOrg.SelfName := orgname;
      end
      else
        fOrg.SelfName := NameNewOrg;

      if Reg.ValueExists('Address') then
        fOrg.Email := Reg.ReadString('Address')
      else
        fOrg.Email := '';

      if Reg.ValueExists('AddressOrg') then
        fOrg.Address := Reg.ReadString('AddressOrg')
      else
        fOrg.Address := '';

      if Reg.ValueExists('NKvart') then
        fOrg.Kvart := Reg.ReadInteger('NKvart')
      else
        fOrg.Kvart := 1;
      if Reg.ValueExists('God') then
        fOrg.Year := Reg.ReadInteger('God')
      else
        fOrg.Year := CurrentYear();

      if Reg.ValueExists('SerLic') then
        fOrg.Lic.SerLic := Reg.ReadString('SerLic')
      else
        fOrg.Lic.SerLic := '';
      if Reg.ValueExists('NumLic') then
        fOrg.Lic.NumLic := Reg.ReadString('NumLic')
      else
        fOrg.Lic.NumLic := '';

      if Reg.ValueExists('BegLic') then
        fOrg.Lic.BegLic := Reg.ReadDate('BegLic')
      else
        fOrg.Lic.BegLic := Kvart_Start(CurDate);
      if Reg.ValueExists('EndLic') then
        fOrg.Lic.EndLic := Reg.ReadDate('EndLic')
      else
        fOrg.Lic.EndLic := Kvart_Finish(CurDate);
      if Reg.ValueExists('OrgLic') then
        fOrg.Lic.OrgLic := Reg.ReadString('OrgLic')
      else
        fOrg.Lic.OrgLic := '�����';
      Reg.CloseKey;
      Result := True;
    end
    else
      Show_Inf('������ ������ �������', fError);
  finally
    Reg.free
  end;
end;

procedure TfmWork.mni_delAllClick(Sender: TObject);
var
  i:Integer;
  Reply_ID, sLink:string;
  ButSel:Integer;
begin
//  ButSel := Show_Inf(CONF_003, fConfirm);
//  if ButSel = mrOk then
//  begin
//    for i := AdvSG_IN.FixedRows to AdvSG_IN.RowCount - 1 do
//    begin
//      Reply_ID := AdvSG_IN.Cells[0,i];
//      sLink := urlQueryIn + AdvSG_IN.Cells[1,i] + '/' + AdvSG_IN.Cells[2,i];
////      ButSel := Show_Inf(CONF_0021 + Reply_ID + CONF_0022 + sLink, fConfirm);
//      DeleteQueryLink(sLink);
//    end;
//    for i := AdvSG_Out.FixedRows to AdvSG_Out.RowCount - 1 do
//    begin
//      Reply_ID := AdvSG_Out.Cells[0,i];
//      sLink := urlQueryOut + AdvSG_IN.Cells[1,i] + '/' + AdvSG_IN.Cells[2,i];
////      ButSel := Show_Inf(CONF_0021 + Reply_ID + CONF_0022 + sLink, fConfirm);
//      DeleteQueryLink(sLink);
////      DeleteQueryRepId(Reply_ID, urlQueryOut);
//    end;
//    ReadDataEGAIS(False);
//    if fmPartner <> nil then
//      fmPartner.pgc_AllDocChange(Application);
//  end;

end;

procedure TfmWork.mni_DelClick(Sender: TObject);
var
  urlQuery:string;
  ReplyId:string;
  qUrl:string;
  sLink:string;
  SG:TAdvStringGrid;
  Row, ButSel:Integer;
begin

//  if pm_update.PopupComponent.Name = 'AdvSG_Out' then
//  begin
//    qUrl:='http://localhost:8080/opt/out/';
//    SG := AdvSG_Out;
//  end;
//  if pm_update.PopupComponent.Name = 'AdvSG_IN' then
//  begin
//    qUrl:='http://localhost:8080/opt/in/';
//    SG := AdvSG_In;
//  end;
//  Row := SG.Row;
//  ReplyId := SG.Cells[0,Row];
//  sLink := qUrl + SG.Cells[1,Row] + '/' + SG.Cells[2,Row];
//  ButSel := Show_Inf(CONF_0021 + ReplyId + CONF_0022 + sLink, fConfirm);
//  if ButSel = mrOk then
//  begin
//    DeleteQueryLink(sLink);
//    mni_updClick(mni_upd);
//  end;

end;

procedure TfmWork.mni_DeleteIDClick(Sender: TObject);
var ButSel:Integer;
begin
//  ButSel := Show_Inf(CONF_005 + SelRepId, fConfirm);
//  if ButSel = mrOk then
//  begin
//    DeleteQueryRepId(SelRepId);
//    mni_updClick(mni_upd);
//  end;

end;

procedure TfmWork.mni_SaveClick(Sender: TObject);
var
  XML:IXMLDocument;
  sLink, tPath, Inn : string;
begin
//  try
//    XML:=TXMLDocument.Create(nil);
//    sLink := DocOut + AdvSG_Out.Cells[1,rowMouse] + '/' + AdvSG_Out.Cells[2,rowMouse];
//    Inn:= GetInfoQueryOrg(AdvSG_Out.Cells[1,rowMouse],AdvSG_Out.Cells[0,rowMouse]);
//    tpath := SetSavePath(Inn,AdvSG_Out.Cells[0,rowMouse], AdvSG_Out.Cells[1,rowMouse], AdvSG_Out.Cells[2,rowMouse]);;
//
//    XML:= GetXMLQuery(sLink);
//    XML.Active := True;
//    XML.SaveToFile(tpath);
//    DeleteQueryLink(sLink);
//    mni_updClick(mni_upd);
//  finally
//    XML:=nil;
//  end;
end;

procedure TfmWork.mni_updClick(Sender: TObject);
begin
//  ReadDataEGAIS(False);
//  if fmPartner <> nil then
//      fmPartner.pgc_AllDocChange(Application);
end;

procedure TfmWork.mni_ViewClick(Sender: TObject);
var XML:IXMLDocument;
  qUrl, sLink:string;
  SG:TAdvStringGrid;
  SL:TStringList;
begin
//  XML:= TXMLDocument.Create(nil);
//  SL:=TStringList.Create;
//  try
//    if fmViewSL = nil then
//      fmViewSL:=TfmViewSL.Create(Application);
//    fmViewSL.Caption := '�������� ���������';
//
//    if pm_update.PopupComponent.Name = 'AdvSG_Out' then
//    begin
//      qUrl:='http://localhost:8080/opt/out/';
//      SG := AdvSG_Out;
//    end;
//    if pm_update.PopupComponent.Name = 'AdvSG_IN' then
//    begin
//      qUrl:='http://localhost:8080/opt/in/';
//      SG := AdvSG_In;
//    end;
//    sLink := qUrl + SG.Cells[1,SG.Row] + '/' + SG.Cells[2,SG.Row];
//    XML:=GetXMLQuery(sLink);
//
//    SL.Assign(XML.XML);
//    fmViewSL.ShowDataSL(SL);
//
//    fmViewSL.ShowModal;
//  finally
//    SL.Free;
//    XML:=nil;
//  end;

end;

procedure TfmWork.SaveIni;
var
  tIniF, tIniF1: TIniFile;
  fn, fn1: string;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  fn1 := GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\' + NameProg +
    '_inn.ini';
  if fOrg.Inn <> NullInn then
  begin
    tIniF := TIniFile.Create(fn);
    tIniF1 := TIniFile.Create(fn1);
    try
      tIniF.WriteString(SectionName, 'VersionInfo', exe_version);
      tIniF.WriteString(SectionName, 'INN', fOrg.Inn);
      tIniF1.WriteString('LastInn', 'INN', fOrg.Inn);
      tIniF.WriteString(SectionName,'KPP0', string(fOrg.KPP0));
      // ������� ���
      tIniF.WriteInteger(SectionName, 'Year', CurYear);
    finally
      tIniF1.Free;
      tIniF.free;
    end;
  end;
end;

procedure TfmWork.LoadIniF;
var
  tIniF: TIniFile;
  fn: string;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    if tIniF.SectionExists(SectionName) then
    begin
      con1ADOCon.Close;
      con2ADOCon.Close;
      con1ADOCon.ConnectionString :=
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + db_decl +
        ';Persist Security Info=False;Jet OLEDB:Database Password=' + fOrg.Inn;
      con2ADOCon.ConnectionString :=
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + db_spr +
        ';Persist Security Info=False;Jet OLEDB:Database Password=' + psw_loc;

//      iTimeOut := ReadIniInt(fIniMain, SecParams, SetTimeOut, defaultTimeOut);

      // ������ ���������
      if tIniF.ValueExists(SectionName, 'VersionInfo') then
        exe_version := tIniF.ReadString(SectionName, 'VersionInfo',
          FileVersion(ParamStr(0)))
      else
      begin
        exe_version := FileVersion(ParamStr(0));
        tIniF.WriteString(SectionName, 'VersionInfo', exe_version);
      end;
      if exe_version <> FileVersion(ParamStr(0)) then
        exe_version := FileVersion(ParamStr(0));

      //��� �������� �������
      if tIniF.ValueExists(SectionName, 'TypeOst') then
        TypeOst := tIniF.ReadInteger(SectionName, 'TypeOst', 1)
      else
      begin
        TypeOst := 1;
        tIniF.WriteInteger(SectionName, 'TypeOst', 1);
      end;



      // ������� �������
      if tIniF.ValueExists(SectionName, 'ProcOst_11') then
        ProcOst_11 := tIniF.ReadInteger(SectionName, 'ProcOst_11', 0)
      else
      begin
        if tIniF.ValueExists(SectionName, 'ProcOst') then
          ProcOst_11 := tIniF.ReadInteger(SectionName, 'ProcOst', 0)
        else
          ProcOst_11 := 0;
        tIniF.WriteInteger(SectionName, 'ProcOst_11', ProcOst_11);
      end;

      if tIniF.ValueExists(SectionName, 'ProcOst_12') then
        ProcOst_12 := tIniF.ReadInteger(SectionName, 'ProcOst_12', 0)
      else
      begin
        if tIniF.ValueExists(SectionName, 'ProcOst') then
          ProcOst_12 := tIniF.ReadInteger(SectionName, 'ProcOst', 0)
        else
          ProcOst_12 := 0;
        tIniF.WriteInteger(SectionName, 'ProcOst_12', ProcOst_12);
      end;

      if TypeOst = 2 then
        tIniF.ReadSectionValues('ProcOst', SLProcOst);

      if (TypeOst = 1) or
         ((TypeOst = 2) and (SLProcOst.Count = 0)) then
        FillFNSOst;

      // ������� ����
      if tIniF.ValueExists(SectionName, 'Sv') then
        svertka := tIniF.ReadInteger(SectionName, 'Sv', -1)
      else
      begin
        svertka := -1;
        tIniF.WriteInteger(SectionName, 'Sv', svertka)
      end;

      SetCurDate(NowYear);
      fOrg.Kvart := CurKvart;

      CurYear := NowYear;
      fOrg.Year := CurYear;

      // ������������ �����
      if tIniF.ValueExists(SectionName, 'RukF') then
        fOrg.Ruk.RukF := tIniF.ReadString(SectionName, 'RukF', '')
      else
      begin
        fOrg.Ruk.RukF := '';
        tIniF.WriteString(SectionName, 'RukF', fOrg.Ruk.RukF);
      end;
      if tIniF.ValueExists(SectionName, 'RukI') then
        fOrg.Ruk.RukI := tIniF.ReadString(SectionName, 'RukI', '')
      else
      begin
        fOrg.Ruk.RukI := '';
        tIniF.WriteString(SectionName, 'RukI', fOrg.Ruk.RukI)
      end;
      if tIniF.ValueExists(SectionName, 'RukO') then
        fOrg.Ruk.RukO := tIniF.ReadString(SectionName, 'RukO', '')
      else
      begin
        fOrg.Ruk.RukO := '';
        tIniF.WriteString(SectionName, 'RukO', fOrg.Ruk.RukO)
      end;

      // ���������
      if tIniF.ValueExists(SectionName, 'BuhF') then
        fOrg.Buh.BuhF := tIniF.ReadString(SectionName, 'BuhF', '')
      else
      begin
        fOrg.Buh.BuhF := '';
        tIniF.WriteString(SectionName, 'BuhF', fOrg.Buh.BuhF)
      end;
      if tIniF.ValueExists(SectionName, 'BuhI') then
        fOrg.Buh.BuhI := tIniF.ReadString(SectionName, 'BuhI', '')
      else
      begin
        fOrg.Buh.BuhI := '';
        tIniF.WriteString(SectionName, 'BuhI', fOrg.Buh.BuhI);
      end;
      if tIniF.ValueExists(SectionName, 'BuhO') then
        fOrg.Buh.BuhO := tIniF.ReadString(SectionName, 'BuhO', '')
      else
      begin
        fOrg.Buh.BuhO := '';
        tIniF.WriteString(SectionName, 'BuhO', fOrg.Buh.BuhO);
      end;

      // �������� ���
      if tIniF.ValueExists(SectionName, 'KPP0') then
        fOrg.kpp0 := ShortString(tIniF.ReadString(SectionName, 'KPP0', ''))
      else
      begin
        fOrg.kpp0 := '';
        if (SLKPP.Count > 0) and (SLKPP.Count = 1) then
          fOrg.kpp0 := ShortString(SLKPP.Strings[0]);
        tIniF.WriteString(SectionName, 'KPP0', String(fOrg.kpp0));
      end;

      if fOrg.kpp0 = '' then
        if (SLKPP.Count > 0) and (SLKPP.Count = 1) then
        begin
          fOrg.kpp0 := ShortString(SLKPP.Strings[0]);
          KppCur := fOrg.kpp0;
        end;

      if tIniF.ValueExists(SectionName, 'NPhone') then
        fOrg.NPhone := tIniF.ReadString(SectionName, 'NPhone',
          '(342) 000-00-00')
      else
      begin
        fOrg.NPhone := '(342) 000-00-00';
        tIniF.WriteString(SectionName, 'NPhone', fOrg.NPhone);
      end;

      if tIniF.ValueExists(SectionName, 'SelfName') then
        orgname := tIniF.ReadString(SectionName, 'SelfName', NameNewOrg)
      else
      begin
        orgname := NameNewOrg;
        tIniF.WriteString(SectionName, 'SelfName', orgname);
      end;
      fOrg.SelfName := orgname;

      if tIniF.ValueExists(SectionName, 'Address') then
        fOrg.Email := tIniF.ReadString(SectionName, 'Address', '')
      else
      begin
        fOrg.Email := '';
        tIniF.WriteString(SectionName, 'Address', fOrg.Email);
      end;

      if tIniF.ValueExists(SectionName, 'AddressOrg') then
        fOrg.Address := tIniF.ReadString(SectionName, 'AddressOrg', '')
      else
      begin
        fOrg.Address := '';
        tIniF.WriteString(SectionName, 'AddressOrg', fOrg.Address);
      end;

      if tIniF.ValueExists(SectionName, 'SerLic') then
        fOrg.Lic.SerLic := tIniF.ReadString(SectionName, 'SerLic', '')
      else
      begin
        fOrg.Lic.SerLic := '';
        tIniF.WriteString(SectionName, 'SerLic', fOrg.Lic.SerLic);
      end;

      if tIniF.ValueExists(SectionName, 'NumLic') then
        fOrg.Lic.NumLic := tIniF.ReadString(SectionName, 'NumLic', '')
      else
      begin
        fOrg.Lic.NumLic := '';
        tIniF.WriteString(SectionName, 'NumLic', fOrg.Lic.NumLic);
      end;

      if tIniF.ValueExists(SectionName, 'BegLic') then
        fOrg.Lic.BegLic := tIniF.ReadDate(SectionName, 'BegLic',
          Kvart_Start(CurDate))
      else
      begin
        fOrg.Lic.BegLic := Kvart_Start(CurDate);
        tIniF.WriteDate(SectionName, 'BegLic', fOrg.Lic.BegLic);
      end;

      if tIniF.ValueExists(SectionName, 'EndLic') then
        fOrg.Lic.EndLic := tIniF.ReadDate(SectionName, 'EndLic',
          Kvart_Finish(CurDate))
      else
      begin
        fOrg.Lic.EndLic := Kvart_Finish(CurDate);
        tIniF.WriteDate(SectionName, 'EndLic', fOrg.Lic.EndLic);
      end;

      if tIniF.ValueExists(SectionName, 'OrgLic') then
        fOrg.Lic.OrgLic := tIniF.ReadString(SectionName, 'OrgLic', '�����')
      else
      begin
        fOrg.Lic.OrgLic := '�����';
        tIniF.WriteString(SectionName, 'OrgLic', fOrg.Lic.OrgLic)
      end;
    end;
  finally
    tIniF.free;
  end;
end;

procedure TfmWork.GetDir;
begin
  try
    ProgrPath := GetSpecialPath(CSIDL_PROGRAM_FILES);
    path_exe := ProgrPath + '\' + NameProg + '\';
    if not DirectoryExists(ProgrPath) then
      ForceDirectories(ProgrPath);
  except
    Show_Inf('������ �������� ����� "Program Files"', fError);
  end;
end;

// procedure TfmWork.LoadDir;
// var dir_path:string;
// begin
// try
// if not DirectoryExists(path_db) then
// begin
// dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// dir_path:=dir_path+fOrg.Inn+'\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// dir_path:=dir_path+'DataBase\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// end;
// if not DirectoryExists(path_arh) then
// begin
// dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// dir_path:=dir_path+fOrg.Inn+'\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// dir_path:=dir_path+'BackUp\';
// if not DirectoryExists(dir_path) then
// ForceDirectories(dir_path);
// end;
// if not DirectoryExists(path_arh) then
// ForceDirectories(path_arh);
// if not FileExists(db_decl) then
// Create_DBDecl(que1_sql,db_decl,fOrg.Inn);
// if not FileExists(db_spr) then
// Create_DBSpr(que2_sql,db_spr,psw_loc);
// except
// Show_Inf('������ �������� �������� ���� ������. ��������� ����� �������.', fError);
// Application.Terminate;
// end;
// end;

end.
