unit dfmSelSert;

interface

uses
  Forms, StdCtrls, ExtCtrls, Classes, Controls, Buttons, SysUtils;

type
  TfmSelSert = class(TForm)
    sbBut: TScrollBox;
    leFiltr: TLabeledEdit;
    procedure FormShow(Sender: TObject);
    procedure leFiltrChange(Sender: TObject);
  private
    { Private declarations }
    procedure SpeedButtonClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  fmSelSert: TfmSelSert;

implementation

uses Global, GlobalConst;
{$R *.dfm}

procedure TfmSelSert.FormShow(Sender: TObject);
var i:integer;
    s:string;
    p: TPanel;
    sb:TSpeedButton;
begin
  SelectCert :=False;
  if FindEcp then
    begin
      if High(afs) >= 0 then
        begin
          for i := 0 to High(afs) do
            begin
              p:= TPanel.Create(sbBut);
              p.Parent := sbBut;
              p.Height:=95;
              p.Align := alTop;
              p.Caption :='';
              p.Font.Size:=11;

              with p do
                begin
                  sb:=TSpeedButton.Create(p);
                  sb.Flat := True;
                  sb.Align:=alClient;
                  sb.Parent := p;
                  sb.Caption :='';
                  sb.Tag:=i;
                  sb.OnClick := SpeedButtonClick;
                end;
            end
        end;

    //if sbBut.ComponentCount = High(afs)+1 then
      for i := 0 to High(afs) do
        begin
          s := '�������� ' + afs[i].FIO + ';'+ ' ����������� ' + afs[i].OrgName + #10#13 +
               '���� �������� � ' + DateToStr(afs[i].dBeg) + ' �� ' + DateToStr(afs[i].dEnd) + ';'+ #10#13 +
               '��� ' + afs[i].INN + '; ��� ' + String(afs[i].kpp) + #10#13 +
               'e-mail ' + afs[i].email + #10#13 +
               '�������� ' + afs[i].izd;
          if SbBut.Components[i] is TPanel then
            if (sbBut.Components[i] as TPanel).Components[0] is TSpeedButton then
              ((sbBut.Components[i] as TPanel).Components[0] as TSpeedButton).Caption := s;
        end;
    end
  else
    begin
      //Show_Inf('�� ������� �� ������ �����������', fError);
      ModalResult := mrCancel;
    end;

end;

procedure TfmSelSert.leFiltrChange(Sender: TObject);
var s, s1, sInn, sName:string;
  i:integer;
begin
  s:=leFiltr.Text;
  s1:= AnsiUpperCase(s);
  for i := 0 to High(afs) do
  begin
    sInn := afs[i].INN;
    sName := AnsiUpperCase(afs[i].OrgName);
    if (Pos(s1,sInn) > 0) or
      (Pos(s1, sName) > 0) or
      (s = '') then
    begin
      if SbBut.Components[i] is TPanel then
        (SbBut.Components[i] as TPanel).Visible := True;
    end
    else
    begin
      if SbBut.Components[i] is TPanel then
        (SbBut.Components[i] as TPanel).Visible := False;
    end;
  end;

end;

procedure TfmSelSert.SpeedButtonClick(Sender: TObject);
var idx:integer;
begin
  idx:=(Sender as TSpeedButton).Tag;
  fSert:=afs[idx];
  ModalResult:=mrOk;
  fSert.path := afs[idx].path;
  SelectCert:=True;
  if fOrg.Inn = '0000000000' then
    fOrg.Inn := fSert.INN;
end;


end.
