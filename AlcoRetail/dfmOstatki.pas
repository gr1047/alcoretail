unit dfmOstatki;

interface

uses
  Forms, Menus, Grids, AdvObj, BaseGrid, AdvGrid, ComCtrls, Buttons, Controls,
  StdCtrls, Classes, ExtCtrls, SysUtils;

type
  TfmOstatki = class(TForm)
    Panel12: TPanel;
    Label6: TLabel;
    Button1: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    dtpDEnd: TDateTimePicker;
    Panel7: TPanel;
    Label2: TLabel;
    Panel8: TPanel;
    Panel9: TPanel;
    cbAllKvart: TComboBox;
    Panel3: TPanel;
    SpeedButton5: TSpeedButton;
    pgc1: TPageControl;
    ts_KppEan: TTabSheet;
    ts_EAN: TTabSheet;
    lblVer: TLabel;
    pnl1: TPanel;
    btn1: TSpeedButton;
    SG_EANKpp: TAdvStringGrid;
    SG_EAN: TAdvStringGrid;
    pm1: TPopupMenu;
    mniMoveOst: TMenuItem;
    btn5: TSpeedButton;
    N1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FillFormOstatki(d:TDateTime);
    procedure btn1Click(Sender: TObject);
    procedure SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
      FriendlyName: string; var FilterCondition: string);
    procedure SG_GetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
    procedure FormDestroy(Sender: TObject);
    procedure pm1Popup(Sender: TObject);
    procedure mniMoveOstClick(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateOstatki;
  end;

var
  fmOstatki: TfmOstatki;
  dOst:TDateTime;


implementation

uses dfmWork, Global, Global2, GlobalUtils, GlobalConst, dfmMoves, dfmReplace;
{$R *.dfm}

procedure TfmOstatki.btn1Click(Sender: TObject);
begin
  dOst:=dtpDEnd.Date;
  FillFormOstatki(dOst);
end;

procedure TfmOstatki.btn5Click(Sender: TObject);
var fSG:TADVStringGrid;
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  case pgc1.ActivePageIndex of
    0: fSG := SG_EANKpp;
    1: fSG := SG_EAN
  end;
  fmReplace.SGFind := fSG;
  fmReplace.Show;
end;

procedure TfmOstatki.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmOstatki.FillFormOstatki(d:TDateTime);
var  SLOb, SLObKpp, SLOst, SLOstKpp:TStringList;
begin
  SLOst:=TStringList.Create;
  SLOstKpp:=TStringList.Create;
  SLOb:=TStringList.Create;
  SLObKpp:=TStringList.Create;

  try
    // �������� ��� ������� ��� � ������� �������� ��� ���� �����������
    DvizenieEAN(SLOb, SLAllDecl, dOst, dOst);
    //������������ ������� ��������
    FilterOstatki(SLOst,SLOb, masErr);
    //  ���������� �����
    ShowDataOstatkiKpp(SG_EAN,SLOst,NameColOstatkiEanKpp,NoResColOstatkiKpp,CalcColOstatkiKpp, HideColOstatki, FilterColOstatkiKpp);

    if SLKpp.Count > 1 then
      begin
        ts_KppEan.TabVisible:= True;
        DvizenieKppEan(SLOborotKpp,SLAllDecl,dOst, dOst);
        //������������ ������� ��������
        FilterOstatki(SLOstatkiKpp,SLOborotKpp, masErr);
        //���������� �����
        ShowDataOstatkiKpp(SG_EANKpp,SLOstatkiKpp,NameColOstatkiEanKpp,NoResColOstatkiKpp,CalcColOstatkiKpp, HideColOstatkiKpp, FilterColOstatkiKpp);
      end
    else
      ts_KppEan.TabVisible:= False;

  finally
    SLOst.Free;
    SLOstKpp.Free;
    FreeStringList(SLOb);
    FreeStringList(SLObKPP);
  end;
end;

procedure TfmOstatki.cbAllKvartChange(Sender: TObject);
var
  s,cKvart, cYear: string;
begin
  pgc1.Visible := False;
  try
    s := '';
    if (Sender is TComboBox) then
      if (Sender as TComboBox).ItemIndex <> -1 then
      begin
        s := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
      end;
    if Length(s) = 0 then exit;

    cKvart := Copy(s, 0, pos(nkv, s) - 2);
    cYear := Copy(s, Length(s) - 8, 4);
    dOst := Kvart_End(StrToInt(cKvart), StrToInt(cYear))+1;
    dtpDEnd.Date := dOst;

    FillFormOstatki(dost);

  finally
    pgc1.Visible:=True;
  end;
end;

procedure TfmOstatki.FormCreate(Sender: TObject);
begin
//  SLOb := TStringList.Create;
//  SLOst := TStringList.Create;
//  SLObKpp := TStringList.Create;
//  SLOstKpp := TStringList.Create;

  CreateOstatki;
//  AssignCBSL(cbAllKvart,SLPerRegYear);
//  cbAllKvart.ItemIndex := idxKvart;
//  dOst := Kvart_End(curKvart, curYear)+1;
//  FillFormOstatki(dost);
end;

procedure TfmOstatki.CreateOstatki;
begin
  AssignCBSL(cbAllKvart,SLPerRegYear);
  cbAllKvart.ItemIndex := idxKvart;
  dOst := Kvart_End(curKvart, curYear)+1;
  dtpDEnd.Date :=dOst;
  FillFormOstatki(dOst);
end;

procedure TfmOstatki.FormDestroy(Sender: TObject);
begin
//  SLOst.Free;
//  SLOstKpp.Free;
//  FreeStringList(SLOb);
//  FreeStringList(SLObKpp);
end;

procedure TfmOstatki.mniMoveOstClick(Sender: TObject);
var i:Integer;
  mov:TMove;
begin
  if fmMoves = nil then
    fmMoves:= TfmMoves.Create(Application);
  fmMoves.EAN13 := ShortString(SG_EANKpp.Cells[2,SG_EANKpp.Row]);
  fmMoves.KPP1 := ShortString(SG_EANKpp.Cells[1,SG_EANKpp.Row]);
  fmMoves.Ost := SG_EANKpp.Floats[6,SG_EANKpp.Row];
  for i := 0 to SLOstatkiKpp.Count - 1 do
  begin
    mov := Pointer(SLOstatkiKpp.Objects[i]);
    if (mov.SelfKpp = fmMoves.KPP1) and
     (mov.product.EAN13 = fmMoves.EAN13) then
    begin
      fmMoves.sal := mov.sal;
      fmMoves.prod := mov.product;
      Break;
    end;
  end;

  if fmMoves.ShowModal = mrOk then
  begin
    if MoveEanKppCount(fmWork.que1_sql,string(fmMoves.KPP1), string(fmMoves.KPP2),
      fmMoves.Kolvo, fmMoves.sal, fmMoves.prod) then
      Show_Inf('������� ��������� ������ �������', fInfo)
  end;
end;

procedure TfmOstatki.N1Click(Sender: TObject);
var ACol, ARow:integer;
  s:string;
  fSG:TAdvStringGrid;
begin
  case pgc1.ActivePageIndex of
    0: fSG := SG_EANKpp;
    1: fSG := SG_EAN
  end;
  ACol:=fSG.Col;
  ARow:=fSG.Row;
  s:=fSG.Cells[ACol,ARow];
  fSG.FilterActive := False;
  fSG.Filter.ColumnFilter[ACol].Condition := s;
  fSG.FilterActive := True;
  fSG.ApplyFilter;
  fSG.RemoveLastFilter;
end;

procedure TfmOstatki.pm1Popup(Sender: TObject);
begin
  if (pgc1.ActivePageIndex = 0) and (SLKPP.Count>1) then
    mniMoveOst.Enabled := True
  else
    mniMoveOst.Enabled := False;
end;

procedure TfmOstatki.SpeedButton5Click(Sender: TObject);
begin
  fmWork.dlgSave_Export.Filter := '������� � XLS|*.xls';
  if fmWork.dlgSave_Export.Execute then
    begin
      if pgc1.ActivePageIndex = 0 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_EANKpp,6);
      if pgc1.ActivePageIndex = 1 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_EAN,6);
    end;
end;

procedure TfmOstatki.SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
  FriendlyName: string; var FilterCondition: string);
begin
  if IndexMas(FilterColOstatkiKpp, Column) = -1 then
    exit;
  if FilterCondition = '��������' then
    FilterCondition := '';
end;

procedure TfmOstatki.SG_GetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
var
  ColFilter: Array of Integer;
  SL: TStringList;
//  ColDate: Integer;
begin
  SL := TStringList.Create;
  try
    if (Sender as TAdvStringGrid).RowCount = (Sender as TAdvStringGrid)
      .FixedRows + (Sender as TAdvStringGrid).FixedFooters then
    begin
      exit;
    end;

    SetLength(ColFilter, High(FilterColOstatkiKpp) + 1);
    Move(FilterColOstatkiKpp[0], ColFilter[0], sizeOf(Integer) * Length(FilterColOstatkiKpp));

    if IndexMas(ColFilter, Column) = -1 then
      Filter.Clear;

  finally
    SL.Free;
  end;
end;

end.
