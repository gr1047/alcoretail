unit dfmPartner;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, ToolWin, StdCtrls, Menus;

type
  TfmPartner = class(TForm)
    pnl_Work: TPanel;
    pnl_Caption: TPanel;
    pnl_Main: TPanel;
    tlb1: TToolBar;
    btn5: TToolButton;
    pnl_Doc: TPanel;
    pnl_DocName: TPanel;
    pnl_StructDoc: TPanel;
    pgc_View: TPageControl;
    ts_Doc: TTabSheet;
    ts_DocXML: TTabSheet;
    Memo_DataOrgXML: TMemo;
    pgc_AllDoc: TPageControl;
    ts_EGAIS: TTabSheet;
    pnl_EGAIS: TPanel;
    lst_EGAIS: TListBox;
    ts_pk: TTabSheet;
    pnl_SaveDoc: TPanel;
    lst_DataOrg: TListBox;
    spl1: TSplitter;
    pm_Update: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    il1: TImageList;
    Memo_DataOrg: TMemo;
    il_DataOrg: TImageList;
    procedure pnl_DocResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pgc_AllDocChange(Sender: TObject);
    procedure lst_EGAISClick(Sender: TObject);
    procedure lst_DataOrgClick(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPartner: TfmPartner;

implementation
uses uSystem, uConst, GlobalUtils, GlobalConst, Global;

{$R *.dfm}

procedure TfmPartner.btn5Click(Sender: TObject);
var
  Inn:string;
  ButSel:Integer;
  InfoUpd:Boolean;
begin
  InfoUpd := False;
  if not IsConnected then
  begin
    Show_Inf(ERR_025,fError);
    Exit;
  end;

  ButSel := Show_Inf(ENTER_001, fEnter, INN);

  while (not InnCheckUTM(INN)) and (ButSel <> mrCancel) do
  begin
    Show_Inf(ERR_024, fError);
    ButSel := Show_Inf(ENTER_001, fEnter, INN);
  end;
  if ButSel = mrCancel then Exit;

  if (InnCheckUTM(INN)) and (ButSel = mrOk) then
  begin
    if Inn = fOrgUTM.Inn then
    begin
      ButSel := Show_Inf(CONF_001, fConfirm);
      InfoUpd := ButSel = mrOk;
    end;

    if QueryDataOrg(SetFSRAR_ID, Inn,InfoUpd) then
    begin
      if InfoUpd then
        Show_Inf(INFO_003, fInfo);
      pgc_AllDocChange(pgc_AllDoc);
    end;
  end;
end;

procedure TfmPartner.FormShow(Sender: TObject);
begin
  pgc_AllDoc.ActivePageIndex := 0;
  pgc_View.ActivePageIndex := 0;
  pgc_AllDocChange(pgc_AllDoc);
end;

procedure TfmPartner.lst_DataOrgClick(Sender: TObject);
var
  SL, SLDataOrg, SLView:TStringList;
  idx, i:Integer;
  Kpp_Obj:TKpp_Obj;
begin
  Memo_DataOrg.Clear;
  try
    SLDataOrg := TStringList.Create;
    SLView:= TStringList.Create;
    idx := (Sender as TListBox).ItemIndex;
    if idx <> -1 then
    begin
      SL:= Pointer((Sender as TListBox).Items.Objects[idx]);
      ParseXMLDataOrg(SL,SLDataOrg);

      for i := 0 to SLDataOrg.Count - 1 do
      begin
        Kpp_Obj := Pointer(SLDataOrg.Objects[i]);
        SLView.Add('���:' + Kpp_Obj.Data.Inn);
        SLView.Add('���:' + Kpp_Obj.Data.Kpp);
        SLView.Add('����� ��: ' + Kpp_Obj.Data.FSRAR_ID);
        SLView.Add('������������:' + Kpp_Obj.Data.SelfName);
        SLView.Add('�����:' + Kpp_Obj.Data.Address);
        SLView.Add('');
      end;
      Memo_DataOrg.Text := SLView.Text;
      Memo_DataOrgXML.Text := SL.Text;
    end;
  finally
    SLView.Clear;
    SLDataOrg.Free;
  end;
end;

procedure TfmPartner.lst_EGAISClick(Sender: TObject);
var
  SL, SLDataOrg, SLView:TStringList;
  idx, i:Integer;
  Kpp_Obj:TKpp_Obj;

begin
  Memo_DataOrg.Clear;
  try
    SLDataOrg := TStringList.Create;
    SLView:= TStringList.Create;
    idx := (Sender as TListBox).ItemIndex;
    if idx <> -1 then
    begin
      SL:= Pointer((Sender as TListBox).Items.Objects[idx]);
      ParseXMLDataOrg(SL,SLDataOrg);

      for i := 0 to SLDataOrg.Count - 1 do
      begin
        Kpp_Obj := Pointer(SLDataOrg.Objects[i]);
        SLView.Add('���:' + Kpp_Obj.Data.Inn);
        SLView.Add('���:' + Kpp_Obj.Data.Kpp);
        SLView.Add('����� ��: ' + Kpp_Obj.Data.FSRAR_ID);
        SLView.Add('������������:' + Kpp_Obj.Data.SelfName);
        SLView.Add('�����:' + Kpp_Obj.Data.Address);
        SLView.Add('');
      end;
      Memo_DataOrg.Text := SLView.Text;
      Memo_DataOrgXML.Text := SL.Text;
    end;
  finally
    SLView.Clear;
    SLDataOrg.Free;
  end;
end;

procedure TfmPartner.N3Click(Sender: TObject);
begin
  pgc_AllDocChange(Application);
end;

procedure TfmPartner.pgc_AllDocChange(Sender: TObject);
begin
  Memo_DataOrg.Clear;
  Memo_DataOrgXML.Clear;
  case pgc_AllDoc.ActivePageIndex of
    0:
     begin
       ReadDocInSave(lst_DataOrg);  //������ ����������� ����������
       if lst_DataOrg.Items.Count > 0 then
       begin
         lst_DataOrg.ItemIndex := 0;
         lst_DataOrg.Selected[0]:=True;
         lst_DataOrgClick(lst_DataOrg);
       end;
     end;
    1:
    begin
      UpdateQueryFormOut; //������ ������� �������� ����������
      if lst_EGAIS.Items.Count > 0 then
      begin
        lst_EGAIS.ItemIndex := 0;
        lst_EGAIS.Selected[0]:=True;
        lst_EGAISClick(lst_EGAIS);
      end;
    end;
  end;
  Application.ProcessMessages;

end;

procedure TfmPartner.pnl_DocResize(Sender: TObject);
begin
  pnl_DocName.Width := Trunc(pnl_Doc.ClientWidth/2) -1
end;

end.
