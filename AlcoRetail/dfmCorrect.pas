unit dfmCorrect;

interface

uses
  Forms, StdCtrls, Controls, Buttons, Grids, AdvObj, BaseGrid, AdvGrid, Classes,
  ExtCtrls, Dialogs, Graphics, SysUtils;

type
  TfmCorrect = class(TForm)
    pnl6: TPanel;
    SG_Saler: TAdvStringGrid;
    pnl8: TPanel;
    pnl9: TPanel;
    pnl1: TPanel;
    pnl3: TPanel;
    lb3: TLabel;
    cbbSal: TComboBox;
    pnl4: TPanel;
    lb1: TLabel;
    cbbAllKvart: TComboBox;
    pnl5: TPanel;
    lb2: TLabel;
    cbbKPP: TComboBox;
    pnl7: TPanel;
    pnl2: TPanel;
    pnl10: TPanel;
    btn1: TBitBtn;
    pnl11: TPanel;
    SG_All: TAdvStringGrid;
    pnl12: TPanel;
    GB_1: TGroupBox;
    lst_file: TListBox;
    pnl13: TPanel;
    btn2: TBitBtn;
    btnSal: TBitBtn;
    pnl14: TPanel;
    rb_delta: TRadioButton;
    rb_all: TRadioButton;
    btn3: TButton;
    btn4: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure cbbKPPChange(Sender: TObject);
    procedure cbbAllKvartChange(Sender: TObject);
    procedure btnSalClick(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure cbbSalChange(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rb_allClick(Sender: TObject);
    procedure rb_deltaClick(Sender: TObject);
    procedure btn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateCorrect;
  end;

var
  fmCorrect: TfmCorrect;
  SalerInn,SalerKpp, sKvart, SKpp:string;
  SLSumRow:TStringList;

implementation
uses Global, GlobalUtils, dfmWork, GlobalConst, dfmProgress;

{$R *.dfm}

procedure TfmCorrect.btn1Click(Sender: TObject);
var SLXML, SLSaler:TStringList;
  sal:TSaler;
  idx, i:integer;
  sKpp, s:string;
  dB, dE:TDateTime;
  pBase, pXML:Extended;
begin
  rb_all.Checked:=True;
  SG_Saler.ClearAll;
  SG_All.ClearAll;
  pBase := 0;
  pXML := 0;
  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
  ShapkaSG(SG_All, NameColCorrectSaler);

  SLSaler := TStringList.Create;

  s := cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
  GetDateKvart(s, dB, dE);
  DvizenieSalerSKpp(SLSaler,SLAllDecl,dB,dE);
  ShowDataCorectSalerFull(SG_All,SLSaler, SalerInn);

  sal.sInn := ShortString(SalerInn);
  sal.sKpp :=ShortString(SalerKpp);

  SLXML :=TStringList.Create;
  try
    idx:= cbbKPP.ItemIndex;
    if idx = -1 then
      exit
    else
      sKpp := cbbKPP.Items.Strings[idx];
    if cbbAllKvart.ItemIndex <> -1 then
    begin
      s := cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
      GetDateKvart(s, dB, dE);
      if lst_file.Items.Count > 0 then
        //������ XML ����������
        ReadXMLCorrectF(lst_file.Items,SLXML, fmWork.que2_sql, sal, sKpp);
      //�������� �� �����������
      PrihodSaler(sKpp, Sal, SLXML, SLAllDecl,dB,dE, SG_Saler);
      //������� ���� ���������� ������
      for i := SG_Saler.FixedRows to SG_Saler.RowCount - SG_Saler.FixedRows - 1 do
      begin
        if (RTrim(SG_Saler.Cells[12, i]) <> '') and 
           (SG_Saler.Alignments[12, i] = taLeftJustify) then
          pBase := pBase + SG_Saler.Floats[12,i];
        if (RTrim(SG_Saler.Cells[17, i]) <> '') and 
           (SG_Saler.Alignments[17, i] = taRightJustify) then
          pXML := pXML + SG_Saler.Floats[17, i];

        if (RTrim(SG_Saler.Cells[12, i]) <> '') and 
           (RTrim(SG_Saler.Cells[17, i]) <> '') and
           (SG_Saler.Alignments[12, i] = taRightJustify) then
          SG_Saler.Floats[18, i] := SG_Saler.Floats[17, i] - SG_Saler.Floats[12, i];  
      end;
      SG_Saler.Floats[12,SG_Saler.RowCount-1] := pBase;
      SG_Saler.Floats[17,SG_Saler.RowCount-1] := pXML;
      SG_Saler.Floats[18,SG_Saler.RowCount-1] := pXML- pBase;
    end;
  finally
    FreeStringList(SLXML);
    SLSaler.Free;
  end;
end;



//procedure TfmCorrect.btn1Click(Sender: TObject);
//var SLSaler, SLEAN, SLEANXML, SLXML:TStringList;
//    dB, dE:TDateTime;
//    sKpp,s:string;
//    sal:TSaler;
//    idx:integer;
//begin
//  rb_all.Checked:=True;
//  SG_Saler.ClearAll;
//  SG_All.ClearAll;
//  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
//  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
//  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
//  ShapkaSG(SG_All, NameColCorrectSaler);
//
//  SLSaler:=TStringList.Create;
//  SLEAN:=TStringList.Create;
//  SLXML:=TStringList.Create;
//  SLEANXML := TStringList.Create;
//  sal.sInn := ShortString(SalerInn);
//  sal.sKpp :=ShortString(SalerKpp);
//
//  idx:= cbbKPP.ItemIndex;
//  if idx = -1 then
//    exit
//  else
//    sKpp := cbbKPP.Items.Strings[idx];
//  if cbbAllKvart.ItemIndex <> -1 then
//    begin
//      s := cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
//      GetDateKvart(s, dB, dE);
//      DvizenieSalerSKpp(SLSaler,SLAllDecl,dB,dE);
//      PrihodSalerSKpp(SLEAN,SLAllDecl,SKpp,sal, dB,dE);
//
//      ShowDataCorectSalerFull(SG_All,SLSaler, SalerInn);
//      ShowDataCorectSaler(SG_Saler,SLEAN);
//      fmWork.Enabled := False;
//      try
//        try
//          if lst_file.Items.Count > 0 then
//            begin
//              if fmProgress = nil then
//                fmProgress:= TfmProgress.Create(Application);
//              fmWork.Enabled := False;
//              fmProgress.Show;
//              fmProgress.pb.Position := 0;
//
//              fmProgress.pb.UpdateControlState;
//              Application.ProcessMessages;
//              fmProgress.labAbout.Visible :=True;
//              fmProgress.pb.Min := 0;
//              fmProgress.pb.Step := 1;
//              fmProgress.pb.Max := 3;
//              fmProgress.pb.Position := 0;
//
//              fmProgress.lblcap.Caption := '����������� ������ XML ������ ... ';
//              ReadXMLCorrectF(lst_file.Items,SLXML, fmWork.que2_sql, sal, sKpp);
//              fmProgress.pb.StepIt;
//              Application.ProcessMessages;
//
//              fmProgress.lblcap.Caption := '����������� ��������� ������ ... ';
//              SumCorrectData(SLXML, SLEANXML, False);
//              fmProgress.pb.StepIt;
//              Application.ProcessMessages;
//
//              fmProgress.lblcap.Caption := '����������� ������ ������ � ������� ... ';
//              WriteXMLCorrect(SG_Saler,SLEANXML);
//              fmProgress.pb.StepIt;
//              Application.ProcessMessages;
//
//              Show_Inf('��������� ������� ������ � ������ XML ��������� �������',fInfo);
//            end;
//        except
//          Show_Inf('��� ��������� ������� ������ � ������ XML ��������� ������',fError);
//          fmWork.Enabled := True;
//        end;
//      finally
//        FreeStringList(SLEANXML);
//        FreeStringList(SLXML);
//        FreeStringList(SLSaler);
//        SLEAN.Free;
//        fmWork.Enabled := True;
//        fmProgress.labAbout.Visible :=False;
//        fmProgress.Close;
//      end;
//    end;
//
//
//end;

procedure TfmCorrect.btn2Click(Sender: TObject);
var tFile:string;
  i:integer;
begin
  fmWork.dlgOpen_Import.Options := [ofAllowMultiSelect,ofPathMustExist,ofFileMustExist,ofNoReadOnlyReturn,ofEnableSizing];
  fmWork.dlgOpen_Import.Title := '����� ����� ����������';
  fmWork.dlgOpen_Import.Filter := '���� XML|*.xml';
//  lst_file.Items.Clear;
  if fmWork.dlgOpen_Import.Execute then
    for i := 0 to fmWork.dlgOpen_Import.Files.Count-1 do
      begin
        tFile := fmWork.dlgOpen_Import.Files.Strings[i];
        if lst_file.Items.IndexOf(tFile) = -1 then
          lst_file.Items.Add(tFile);
      end;
  
  SG_Saler.ClearAll;
  SG_All.ClearAll;
  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
  ShapkaSG(SG_All, NameColCorrectSaler);
  fmWork.dlgOpen_Import.Options := [ofPathMustExist,ofFileMustExist,ofNoReadOnlyReturn,ofEnableSizing];
end;

procedure TfmCorrect.btn4Click(Sender: TObject);
begin
  FormShow(Self);
end;

procedure TfmCorrect.btnSalClick(Sender: TObject);
begin
  lst_file.Items.Clear;
  SG_Saler.ClearAll;
  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
  ShapkaSG(SG_All, NameColCorrectSaler);
end;

procedure TfmCorrect.cbbAllKvartChange(Sender: TObject);
begin
//  SG_Saler.ClearAll;
//  SG_All.ClearAll;
//  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
//  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
//  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
//  ShapkaSG(SG_All, NameColCorrectSaler);
//  sKvart := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
  FormShow(Self);
end;

procedure TfmCorrect.cbbKPPChange(Sender: TObject);
begin
  rb_all.Checked:=True;
  SG_Saler.ClearAll;
  SG_All.ClearAll;
  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
  ShapkaSG(SG_All, NameColCorrectSaler);
  SKpp := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
end;

procedure TfmCorrect.cbbSalChange(Sender: TObject);
var s:string;
  p:integer;
begin
  rb_all.Checked:=True;
  SG_Saler.ClearAll;
  SG_All.ClearAll;
  lst_file.Items.Clear;
  SG_Saler.RowCount := SG_Saler.FixedRows + SG_Saler.FixedFooters + 1;
  SG_All.RowCount := SG_All.FixedRows + SG_All.FixedFooters + 1;
  ShapkaSG(SG_Saler, NameColCorrectSalerFull);
  ShapkaSG(SG_All, NameColCorrectSaler);
  s:= (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
  p:=Pos('/',s);
  SalerInn := Copy(s,0,p-2);
  SalerKPP := Copy(s,p+2,Length(s)-p-1);
end;

procedure TfmCorrect.FormCreate(Sender: TObject);
var i,p: integer;
    SLSal:TStringList;
    s,s1,s2:string;
begin
  SalerInn := '';
  sKvart := '';
  SKpp := '';

  SLSal:=TStringList.Create;
  SLSumRow := TStringList.Create;
  try
//    AssignCBSL(cbbAllKvart, SLPerRegYear);
//    // ��������� ������������ ��������
//    cbbAllKvart.ItemIndex := SetCurKvart;
//    if cbbAllKvart.ItemIndex <> -1 then
//      sKvart := cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
    CreateCorrect;

    AssignCBSL(cbbKPP, SLKPP);
    if cbbKPP.ItemIndex <> -1 then
      SKpp := cbbKPP.Items.Strings[cbbKPP.ItemIndex];

    for i := 0 to SLSalerDecl.Count - 1 do
      begin
        if SlSalerDecl[i] <> '1234567894123400001' then
          begin
            s1 := Copy(SlSalerDecl[i],0,10);
            s2:=  Copy(SlSalerDecl[i],11,9);
            SLSal.Add(s1 + ' / ' + s2);
          end;
       end;
    SLSal.Sort;
    AssignCBSL(cbbSal, SLSal);
    if cbbSal.ItemIndex <> -1 then
      begin
        s:= cbbSal.Items.Strings[cbbSal.ItemIndex];
        p:=Pos('/',s);
        SalerInn := Copy(s,0,p-2);
        SalerKPP := Copy(s,p+2,Length(s)-p-1);
      end;

    ShapkaSG(SG_Saler, NameColCorrectSalerFull);
    ShapkaSG(SG_All, NameColCorrectSaler);
  finally
    SLSal.Free;
  end;
end;

procedure TfmCorrect.CreateCorrect;
begin
  AssignCBSL(cbbAllKvart, SLPerRegYear);
  // ��������� ������������ ��������
  cbbAllKvart.ItemIndex := SetCurKvart;
  if cbbAllKvart.ItemIndex <> -1 then
    sKvart := cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
end;
procedure TfmCorrect.FormDestroy(Sender: TObject);
begin
  SLSumRow.Free;
end;

procedure TfmCorrect.FormShow(Sender: TObject);
var p, i, idx:integer;
  s,s1:string;
  SLSal:TStringList;
  decl:TDeclaration;
begin
  SLSal:=TStringList.Create;
  try
    SG_Saler.AutoNumberOffset := 0;
    SG_Saler.AutoNumberStart := 0;
    SG_Saler.AutoNumberCol(0);
    SG_Saler.ColumnSize.Rows := arFixed;
    SG_Saler.NoDefaultDraw := True;
    SG_Saler.AutoSize := True;

    // ������� ������� ��������
    for i := 0 to High(HideColCorrectSaler) do
    begin
      idx := HideColCorrectSaler[i];
      SG_Saler.HideColumn(idx);
    end;

    if cbbAllKvart.ItemIndex = -1 then
      cbbAllKvart.ItemIndex := 0;

    if cbbKPP.ItemIndex = -1 then
      cbbKPP.ItemIndex := 0;

    for i := 0 to SLDataPrih.Count - 1 do
    begin
      decl:=TDecl_obj(SLDataPrih.Objects[i]).data;
      if (decl.sal.sInn <> '1234567894') and (decl.sal.sKpp <> '123400001') then
      begin
        s1:= String(decl.sal.sInn) + ' / ' + String(decl.sal.sKpp);
        idx := SLSal.IndexOf(s1);
        if idx = -1 then
          SLSal.Add(s1);
      end;
    end;
//    //���������� ������ ��� � ��� ����������� ��� ������� �������
//    for i := 0 to SLSalerDecl.Count - 1 do
//    begin
//      if SlSalerDecl[i] <> '1234567894123400001' then
//      begin
//        s1 := Copy(SlSalerDecl[i],0,10);
//        s2:=  Copy(SlSalerDecl[i],11,9);
//        SLSal.Add(s1 + ' / ' + s2);
//      end;
//    end;

    SLSal.Sort;
    AssignCBSL(cbbSal, SLSal);
    if cbbSal.ItemIndex <> -1 then
    begin
      s:= cbbSal.Items.Strings[cbbSal.ItemIndex];
      p:=Pos('/',s);
      SalerInn := Copy(s,0,p-2);
      SalerKPP := Copy(s,p+2,Length(s)-p-1);
    end;
  finally
    SLSal.Free;
  end;

end;



procedure TfmCorrect.rb_deltaClick(Sender: TObject);
var i,j:integer;
  s, FNS, sDate, nNacl:string;
  p1,p2:integer;
  SLRowShow:TStringList;
begin
  SLRowShow :=TStringList.Create;
  SG_Saler.Visible := False;
  try
    try
      for i := SG_Saler.FixedRows to SG_Saler.RowCount - SG_Saler.FixedFooters - 1 do
        if SG_Saler.CellProperties[18,i].BrushColor = clRed then
          begin
            s := SG_Saler.Cells[9,i];
            p1:=Length(cFNS);
            p2:=Pos(cDate,s);
            FNS :=Copy(s,p1+1,p2-p1-1);

            p1:=pos(cDate,s)+Length(cDate);
            p2:=Pos(cNacl,s);
            sDate :=Copy(s,p1,p2-p1);

            p1:=pos(cNacl,s)+Length(cNacl);
            p2:=Length(s);
            nNacl :=Copy(s,p1,p2-p1+1);

            SLRowShow.Add(IntToStr(i));
            j:=i-1;
            while (FNS = SG_Saler.Cells[2,j]) and
                  (sDate = SG_Saler.Cells[5,j]) and
                  (nNacl = SG_Saler.Cells[4,j]) do
              begin
                SLRowShow.Add(IntToStr(j));
                dec(j);
              end;
          end;
      if SLRowShow.Count > 0 then
        begin
          SG_Saler.HideRows(SG_Saler.FixedRows,SG_Saler.RowCount - SG_Saler.FixedFooters - 1);

          for i := SG_Saler.FixedRows to SG_Saler.AllRowCount - SG_Saler.FixedFooters - 1 do
            if SLRowShow.IndexOf(IntToStr(i)) <> -1 then
              sg_Saler.UnHideRow(i);
          SG_Saler.CalcFooter(12);
          SG_Saler.CalcFooter(17);
          SG_Saler.CalcFooter(18);
        end;
    except
      SG_Saler.Visible := True;
    end;
  finally
    SLRowShow.Free;
    SG_Saler.Visible := True;
  end;

end;

procedure TfmCorrect.rb_allClick(Sender: TObject);
begin
  SG_Saler.Visible := False;
  SG_Saler.UnHideRowsAll;
  SG_Saler.CalcFooter(12);
  SG_Saler.CalcFooter(17);
  SG_Saler.CalcFooter(18);
  SG_Saler.Visible := True;
end;


end.
