unit dfmMain;

interface

uses Forms, ExtCtrls, Buttons, StdCtrls, Spin, Controls, Classes, Windows,
  ShlObj, SysUtils, IniFiles, Graphics, Menus;

type
  TfmMain = class(TForm)
    Panel1: TPanel;
    GroupBox7: TGroupBox;
    P: TPanel;
    P1: TPanel;
    GroupBox1: TGroupBox;
    le_OstDE: TLabeledEdit;
    Button6: TButton;
    GroupBox2: TGroupBox;
    le_BrakCount: TLabeledEdit;
    le_BrakErr: TLabeledEdit;
    Button5: TButton;
    GroupBox3: TGroupBox;
    le_VPokCount: TLabeledEdit;
    le_VPokErr: TLabeledEdit;
    Button4: TButton;
    GroupBox4: TGroupBox;
    le_VPostCount: TLabeledEdit;
    le_VPostErr: TLabeledEdit;
    Button3: TButton;
    GroupBox5: TGroupBox;
    le_RashCount: TLabeledEdit;
    Button2: TButton;
    GroupBox6: TGroupBox;
    le_PrihCount: TLabeledEdit;
    le_PrihErr: TLabeledEdit;
    Button1: TButton;
    P2: TPanel;
    GroupBox8: TGroupBox;
    lbl_Reg: TLabel;
    Label9: TLabel;
    cbAllKvart: TComboBox;
    GroupBox10: TGroupBox;
    Notebook1: TNotebook;
    Panel4: TPanel;
    leFIO: TLabeledEdit;
    leNameOrg: TLabeledEdit;
    leINN: TLabeledEdit;
    leDateBeg: TLabeledEdit;
    leDateEnd: TLabeledEdit;
    Label1: TLabel;
    Button8: TButton;
    le_OstDB: TLabeledEdit;
    le_OstCount: TLabeledEdit;
    pnl1: TPanel;
    btn5: TSpeedButton;
    btn9: TSpeedButton;
    btn1: TSpeedButton;
    le_RashErr: TLabeledEdit;
    lb1: TLabel;
    seYear: TSpinEdit;
    sb1: TSpeedButton;
    pnl2: TPanel;
    lbledt_EndSert: TLabeledEdit;
    MainMenu1: TMainMenu;
    procedure PResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btSelectSertClick(Sender: TObject);
    procedure cbKPPChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    function SelectFolder(fComment: string; const fDirectory: string; fWnd: HWND = 0): string;
    procedure btn5Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure seYearChange(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure sb1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure LabelReg;
    procedure WriteCaptionCount;

  end;

var
  fmMain: TfmMain;

implementation

Uses Global,Global2, GlobalUtils, GlobalConst, dfmSelSert, dfmMove, dfmWork,
  dfmCorrect, dfmProgress, dfmCorOst, dfmSelPeriod, dfmSetting;
{$R *.dfm}


procedure TfmMain.btn1Click(Sender: TObject);
var pasw, pathFTP:string;
  BrowseInfo: TBrowseInfo;
  DisplayName, Path, TempPath: array [0 .. MAX_PATH] of char;
  TitleName: string;
  lpItemID: PItemIDList;
  idx:Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    try
      if fmProgress = nil then
          fmProgress := TfmProgress.Create(Application);
      fmProgress.Show;
      fmWork.Enabled := False;
      fmProgress.lblcap.Caption := '����������� �������� ������� �� ������� ��� ����������� ' + #10#13 + fOrg.SelfName;

      //���������� ����� ����������� (��� ����� �������� � ������� ������ ��������)
      UpdateNameContr(BMHost, fOrg.inn, exe_version);
      fmProgress.pb.Max := 3;
      fmProgress.pb.Step := 1;
      Application.ProcessMessages;
      fmProgress.pb.StepIt;
      fmProgress.pb.Position:=0;

      //�������� ����� ����������
      pasw := GetPasword(BMHost, fOrg.inn);
      if pasw = '' then
      begin
        Show_Inf('������ ��������, ����������� � ������ ���������', fError);
        exit;
      end;
      Application.ProcessMessages;
      fmProgress.pb.StepIt;

      pathFTP:= '/0'+IntToStr(CurKvart)+IntToStr(CurYear);
      //�������� ������� ������ ��� �����������
      idx := ExistFilesSalerFTP(pathFTP, pasw, fOrg.Inn);
      if idx = -1 then
      begin
        FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
        BrowseInfo.hwndOwner := fmWork.Handle;
        BrowseInfo.pszDisplayName := @DisplayName;
        TitleName := '�������� ����� ��� ���������� ������ ���������';
        BrowseInfo.lpszTitle := PChar(TitleName);
        BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
        lpItemID := SHBrowseForFolder(BrowseInfo);
        if lpItemID <> nil then
        begin
          SHGetPathFromIDList(lpItemID, TempPath);
          GlobalFreePtr(lpItemID);
          Path := TempPath;
        end;

        fmProgress.lblcap.Caption := '����������� �������� ������ ��� ����������� ' + #10#13 + fOrg.SelfName;

        if LoadFilesSaler(fOrg.inn,pasw,Path) then
          Show_Inf('������ ������� ���������', fInfo);
      end
      else
        case idx of
          0: Show_inf('������ ����������� � �������.'+ #10#13 +
                 '���������� � ������ ��������� ��� ���������� ����� �����.',
                 fError);
          1: Show_inf('�� ������� ��� ��� ������ �� ��������� ���������' + #10#13 +
                 '��� ����������� ' + fOrg.SelfName + #10#13 +
                 '���������� ����� �����.', fError);
        end;
    except
    end;
  finally
    Screen.Cursor := crDefault;
    fmWork.Enabled := True;
    fmProgress.Close;
  end;
end;

procedure TfmMain.btn5Click(Sender: TObject);
begin
  ProcDownload;
end;

procedure TfmMain.btn9Click(Sender: TObject);
begin
  HelpDownload;
end;

procedure TfmMain.btSelectSertClick(Sender: TObject);
var
  fn:string;
  tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  if fmSelSert = nil then fmSelSert:=TfmSelSert.Create(Self);
  try
    FindEcp:=SetECP;
    leDateBeg.Text := '';
    leDateEnd.Text := '';
    leNameOrg.Text := '';
    leFIO.Text := '';
    leINN.Text := '';
    if not FindEcp then
      begin
        Show_Inf('�� ������� �� ������ �����������', fError);
        Notebook1.PageIndex := 0;
        fmSelSert.Close;
      end
    else
      begin
        fmSelSert.ShowModal;
        if SelectCert then
          begin
            leDateBeg.Text := DateToStr(fSert.dBeg);
            leDateEnd.Text := DateToStr(fSert.dEnd);
            leNameOrg.Text := fSert.OrgName;
            leFIO.Text := fSert.FIO;
            leINN.Text := fSert.INN;
            Notebook1.PageIndex := 1;
            SetColorSertMain(leDateEnd, leINN, lbledt_EndSert);
          end;
      end;
    tIniF.WriteString(SectionName,'Sert', fSert.path);
  finally
    fmSelSert.free;
    fmSelSert := nil;
    tIniF.Free;
  end;
end;

procedure TfmMain.Button1Click(Sender: TObject);
begin
  if fmMove = nil then fmMove := TfmMove.Create(Application);

  fmWork.sb_MoveProductionClick(Application);
  fmMove.pgc1.ActivePageIndex := 0;
  fmMove.pgc1.TabIndex := 0;
  fmMove.pgc1Change(fmMove.pgc1);
end;

procedure TfmMain.Button2Click(Sender: TObject);
begin
  if fmMove = nil then fmMove := TfmMove.Create(Application);

  fmWork.sb_MoveProductionClick(Application);
  fmMove.pgc1.ActivePageIndex :=1;
  fmMove.pgc1.TabIndex := 1;
  fmMove.pgc1Change(fmMove.pgc1);
end;

procedure TfmMain.Button3Click(Sender: TObject);
begin
  if fmMove = nil then fmMove := TfmMove.Create(Application);

  fmWork.sb_MoveProductionClick(Application);
  fmMove.pgc1.ActivePageIndex :=2;
  fmMove.pgc1.TabIndex := 2;
  fmMove.pgc1Change(fmMove.pgc1);
end;

procedure TfmMain.Button4Click(Sender: TObject);
begin
  if fmMove = nil then fmMove := TfmMove.Create(Application);

  fmWork.sb_MoveProductionClick(Application);
  fmMove.pgc1.ActivePageIndex :=3;
  fmMove.pgc1.TabIndex := 3;
  fmMove.pgc1Change(fmMove.pgc1);
end;

procedure TfmMain.Button5Click(Sender: TObject);
begin
  if fmMove = nil then fmMove := TfmMove.Create(Application);

  fmWork.sb_MoveProductionClick(Application);
  fmMove.pgc1.ActivePageIndex :=4;
  fmMove.pgc1.TabIndex := 4;
  fmMove.pgc1Change(fmMove.pgc1);
end;

procedure TfmMain.Button6Click(Sender: TObject);
begin
  if fmCorOst = nil then fmCorOst := TfmCorOst.Create(Application);

  fmCorOst.ShowModal;
end;

procedure TfmMain.cbAllKvartChange(Sender: TObject);
var
  sKvart: string;
  y,m,d:word;
begin
  sKvart := '';
  if (Sender is TComboBox) then
    if (Sender as TComboBox).ItemIndex <> -1 then
    begin
      sKvart := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
      idxKvart := (Sender as TComboBox).ItemIndex;
      decodeDate(Now(),y,m,d);
      if y = NowYear then
        if idxKvart > 0 then
          WriteStringParam('Kvartal', IntToStr(idxKvart));
    end;
  cbKvartChange(sKvart);
  if idxKvart <> -1 then
  begin
    cbAllKvart.ItemIndex := idxKvart;
    if fmMove <> nil then
    begin
      fmMove.cbAllKvart.ItemIndex := idxKvart;
      fmMove.ShowDataAll;
    end;
  if fmCorrect <> nil then
    begin
      fmCorrect.cbbAllKvart.ItemIndex := idxKvart;
    end;
  end;
  LabelReg;
  WriteCaptionCount;
end;

procedure TfmMain.cbKPPChange(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    if (Sender as TComboBox).ItemIndex <> -1 then
      begin
        fOrg.Kpp0 := ShortString((Sender as TComboBox).Items.Strings[(Sender as TComboBox)
          .ItemIndex]);
        tIniF.WriteString(SectionName,'KPP0',String(fOrg.Kpp0));
        tIniF.WriteString(SectionName,'SLKPP',SLKpp.Text);
      end;
  finally
    tIniF.Free;
  end;
end;

procedure TfmMain.LabelReg;
begin
  if ExistRegPeriod then
  begin
    lbl_Reg.Caption := '����������������';
    lbl_Reg.Font.Color := clGreen;
    fmWork.sb_Decl.Enabled:=True;
    fmWork.l_Decl.Enabled:=True;
  end
  else
  begin
    lbl_Reg.Caption := '�� ������ ������ ��� �����������';
    lbl_Reg.Font.Color := clRed;
    fmWork.sb_Decl.Enabled:=False;
    fmWork.l_Decl.Enabled:=False;
  end;
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  seYear.Value := NowYear;

  if FindEcp then
  begin
    Notebook1.PageIndex := 1;
    leDateBeg.Text := DateToStr(fSert.dBeg);
    leDateEnd.Text := DateToStr(fSert.dEnd);
    leNameOrg.Text := fSert.OrgName;
    leFIO.Text := fSert.FIO;
    leINN.Text := fSert.INN;
    SetColorSertMain(leDateEnd, leINN, lbledt_EndSert);
  end
  else
    Notebook1.PageIndex := 0;

  AssignCBSL(cbAllKvart, SLPerRegYear);
//  lb_MainKPP.Caption := fOrg.Kpp0;
//  AssignCBSL(cbKPP, SLKPP);
//  if (SLKPP.Count > 0) then
//    if fOrg.Kpp0 = '' then
//    begin
//      if (SLKPP.Count = 1) then
//        cbKPP.ItemIndex := 0;
//    end
//    else
//    begin
//      s := String(fOrg.Kpp0);
//      idx := SLKPP.IndexOf(s);
//      cbKPP.ItemIndex := idx;
//    end;
  // ��������� �� ����� ������ �����������
  //SetRegForm(CurYear);
  // ��������� ������������ ��������
  cbAllKvart.ItemIndex := SetCurKvart;
  idxKvart := cbAllKvart.ItemIndex;
  cbAllKvartChange(cbAllKvart);
  LabelReg;
end;

procedure TfmMain.FormShow(Sender: TObject);
begin
  PResize(P);
  le_PrihCount.Text := IntToStr(SLDataPrih.Count);
  WriteCaptionCount;
  //seYear.Text := FormatDateTime('yyyy', Now);
  seYear.Value := CurYear;
end;



procedure TfmMain.WriteCaptionCount;
begin
  le_PrihCount.Text := IntToStr(SLDataPrih.Count);
  if masErr[0] > 0 then
    le_PrihErr.Font.Color := clErr
  else
    le_PrihErr.Font.Color := clNormal;
  le_PrihErr.Text := IntToStr(masErr[0]);

  le_RashCount.Text := IntToStr(SLDataRash.Count);
  if masErr[1] > 0 then
    le_RashErr.Font.Color := clErr
  else
    le_RashErr.Font.Color := clNormal;
  le_RashErr.Text := IntToStr(masErr[1]);

  le_VPostCount.Text := IntToStr(SLDataVPost.Count);
  if masErr[2] > 0 then
    le_VPostErr.Font.Color := clErr
  else
    le_VPostErr.Font.Color := clNormal;
  le_VPostErr.Text := IntToStr(masErr[2]);

  le_VPokCount.Text := IntToStr(SLDataVPok.Count);
  if masErr[3] > 0 then
    le_VPokErr.Font.Color := clErr
  else
    le_VPokErr.Font.Color := clNormal;
  le_VPokErr.Text := IntToStr(masErr[3]);


  le_BrakCount.Text := IntToStr(SLDataBrak.Count);
  if masErr[4] > 0 then
    le_BrakErr.Font.Color := clErr
  else
    le_BrakErr.Font.Color := clNormal;
  le_BrakErr.Text := IntToStr(masErr[4]);

  if masErr[6] > 0 then
    le_OstDE.Font.Color := clErr
  else
    le_OstDE.Font.Color := clNormal;
  le_OstDE.Text := IntToStr(masErr[6]) + ' ������� � ��������';

  if masErr[5] > 0 then
    le_OstDB.Font.Color := clErr
  else
    le_OstDB.Font.Color := clNormal;
  le_OstDB.Text := IntToStr(masErr[5]) + ' ������� � ��������';

  le_OstCount.Text := IntToStr(SLOstatkiKpp.Count);
end;
procedure TfmMain.PResize(Sender: TObject);
var
  i, CountPan: integer;
begin
  width := (Sender as TPanel).width;
  CountPan := 0;
  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is TPanel then
      Inc(CountPan);

  for i := 0 to (Sender as TPanel).ControlCount - 1 do
    if (Sender as TPanel).Controls[i] is TPanel then
  ((Sender as TPanel).Controls[i] as TPanel)
      .width := Round(width / CountPan) - 1;
end;

procedure TfmMain.sb1Click(Sender: TObject);
var pasw, pathFTP:string;
  BrowseInfo: TBrowseInfo;
  DisplayName, Path, TempPath: array [0 .. MAX_PATH] of char;
  TitleName,s: string;
  lpItemID: PItemIDList;
  idx, iKvart, iYear, cFiles:Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    try
      if fmSelPeriod = nil then
        fmSelPeriod := TfmSelPeriod.Create(Application);

      if fmSelPeriod.ShowModal = mrOk then
      begin
        if fmProgress = nil then
            fmProgress := TfmProgress.Create(Application);
        fmProgress.Show;
        fmWork.Enabled := False;
        fmProgress.lblcap.Caption := '����������� �������� ������� �� ������� ��� ����������� ' + #10#13 + fOrg.SelfName;

        //���������� ����� ����������� (��� ����� �������� � ������� ������ ��������)
        UpdateNameContr(BMHost, fOrg.inn, exe_version);
        fmProgress.pb.Max := 3;
        fmProgress.pb.Step := 1;
        Application.ProcessMessages;
        fmProgress.pb.StepIt;
        fmProgress.pb.Position:=0;

        pasw := GetPasword(BMHost, fOrg.inn);
        if pasw = '' then
        begin
          Show_Inf('������ ��������, ����������� � ������ ���������', fError);
          exit;
        end;
        Application.ProcessMessages;
        fmProgress.pb.StepIt;


        iKvart := fmSelPeriod.selKvart;
        iYear := fmSelPeriod.selYear;

        pathFTP:= '/0'+IntToStr(iKvart)+IntToStr(iYear);
        //�������� ������� ������ ��� �����������
        idx := ExistFilesDeclFTP(pathFTP, pasw, fOrg.Inn);
        if idx = -1 then
        begin
          FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
          BrowseInfo.hwndOwner := fmWork.Handle;
          BrowseInfo.pszDisplayName := @DisplayName;
          TitleName := '�������� ����� ��� ���������� ������ ����������';
          BrowseInfo.lpszTitle := PChar(TitleName);
          BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
          lpItemID := SHBrowseForFolder(BrowseInfo);
          if lpItemID <> nil then
          begin
            SHGetPathFromIDList(lpItemID, TempPath);
            GlobalFreePtr(lpItemID);
            Path := TempPath;

            fmProgress.lblcap.Caption := '����������� �������� ������ ��� ����������� ' + #10#13 + fOrg.SelfName;

            cFiles := LoadFilesDecl(fOrg.inn,pasw,Path, iKvart, iYear);
            case cFiles of
              0: s:='�� ��������� �� ������ ����� ����������';
              1: s:='�������� ���� ���� ����������';
              2: s:='��������� ��� ����� ����������';
            end;
            Show_Inf(s, fInfo);
          end;
        end
        else
          case idx of
            0: Show_inf('������ ����������� � �������.'+ #10#13 +
                   '���������� � ������ ��������� ��� ���������� ����� �����.',
                   fError);
            1: Show_inf('��� ����������� ' + fOrg.SelfName + #10#13 +
                   '�� ������� ��� ���������� �� ��������� ������' + #10#13 +
                   '���������� ����� �����.', fError);
          end;
      end;
    except
    end;
  finally
    Screen.Cursor := crDefault;
    fmWork.Enabled := True;
    fmProgress.Close;
  end;
end;

function TfmMain.SelectFolder(fComment: string; const fDirectory: string; fWnd: HWND = 0): string;
var
  fTitleName, baseDir: string;
  flpItemID: PItemIDList;
  fBrowseInfo: TBrowseInfo;
  fDisplayName: array[0..MAX_PATH] of char;
  fTempPath: array[0..MAX_PATH] of char;

function BrowseCallbackProc(hwnd: HWND; uMsg: UINT; lParam: LPARAM; lpData:LPARAM): integer; stdcall;
begin
  result:=0;
  if uMsg = BFFM_INITIALIZED then
    SendMessage(hwnd, BFFM_SETSELECTION, 1, integer(pChar(baseDir)))
end;

begin
  result:='';
  baseDir:=fDirectory;
  FillChar(fBrowseInfo, sizeof(TBrowseInfo), #0);
  with fBrowseInfo do
    begin
      hwndOwner:=fWnd;
      pszDisplayName:=@fDisplayName;
      fTitleName:=fComment;
      lpszTitle:=pChar(fTitleName);
      ulFlags:=BIF_RETURNONLYFSDIRS;
      if fDirectory <> '' then
        lpfn:=@BrowseCallbackProc
      else
        lpfn:=nil
    end;
  flpItemID:=SHBrowseForFolder(fBrowseInfo);
  if flpItemId <> nil then
    begin
      SHGetPathFromIDList(flpItemID, fTempPath);
      result:=StrPas(fTempPath); GlobalFreePtr(flpItemID)
    end
end;


procedure TfmMain.seYearChange(Sender: TObject);
begin
  SetCurrentYear(NowYear);
end;

end.
