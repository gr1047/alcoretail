unit dfmProcent;

interface

uses Forms, StdCtrls, Spin, Controls, Grids, AdvObj, BaseGrid, AdvGrid,
  ExtCtrls, Classes, Buttons, IniFiles, ShlObj;

type
  TfmProcent = class(TForm)
    btn_Cancel: TBitBtn;
    btn_Ok: TBitBtn;
    pnl_FNS: TPanel;
    SG_FNS: TAdvStringGrid;
    pnl_form: TPanel;
    pnl_11: TPanel;
    lb_11: TLabel;
    seOst_11: TSpinEdit;
    pnl_12: TPanel;
    lb_12: TLabel;
    seOst_12: TSpinEdit;
    pnl_RB: TPanel;
    rb_form: TRadioButton;
    rb_fns: TRadioButton;
    procedure FormShow(Sender: TObject);
    procedure rb_formClick(Sender: TObject);
    procedure rb_fnsClick(Sender: TObject);
    procedure seOst_11Exit(Sender: TObject);
    procedure seOst_12Exit(Sender: TObject);
    procedure SG_FNSExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmProcent: TfmProcent;

implementation
uses Global2, Global, GlobalUtils, GlobalConst;
{$R *.dfm}

procedure TfmProcent.FormShow(Sender: TObject);
var i, idx:Integer;
  kod:string;

begin
  if Length(fOrg.Inn) = 12 then
    pnl_11.Visible := False;

  if TypeOst = 1 then
    rb_form.Checked := True
  else
    rb_fns.Checked := True;

  seOst_12.Value := ProcOst_12;
  seOst_11.Value := ProcOst_11;

  GetFNSOst(SG_FNS);
  for i := SG_FNS.FixedRows to SG_FNS.RowCount - 1 do
  begin
    kod := SG_FNS.Cells[1, i];
    idx := SLProcOst.IndexOfName(kod);
    if idx <> -1 then
      SG_FNS.Cells[3, i] := SLProcOst.Values[kod];
  end;

  if (SG_FNS.Cols[1].IndexOf('500') = -1) and
     (SG_FNS.Cols[1].IndexOf('510') = -1) and
     (SG_FNS.Cols[1].IndexOf('520') = -1) then
    pnl_12.Visible := False
  else
    pnl_12.Visible := True;

  case fmProcent.Tag of
    1: begin
      rb_form.Checked := True;
      pnl_RB.Visible := False;
      btn_Ok.Visible := True;
      rb_formClick(Self);
    end;
    2: begin
      rb_fns.Checked := True;
      pnl_RB.Visible := False;
      btn_Ok.Visible := True;
      rb_fnsClick(Self);
    end;
    3: begin
      pnl_RB.Visible := True;
      btn_Ok.Visible := False;
    end;
  end;
end;

procedure TfmProcent.rb_fnsClick(Sender: TObject);
var
  tIniF: TIniFile;
  fn, kod: string;
  i, idx:integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  SLProcOst.Clear;
  try
    seOst_11.Enabled := False;
    seOst_12.Enabled := False;
    lb_11.Enabled := False;
    lb_12.Enabled := False;
    SG_FNS.Enabled := True;
    TypeOst := 2;
    tIniF.ReadSectionValues('ProcOst', SLProcOst);
    tIniF.WriteInteger(SectionName,'TypeOst',TypeOst);
    if SLProcOst.Count = 0 then
      FillFNSOst;
    for i := SG_FNS.FixedRows to SG_FNS.RowCount - 1 do
    begin
      kod := SG_FNS.Cells[1, i];
      idx := SLProcOst.IndexOfName(kod);
      if idx <> -1 then
        SG_FNS.Cells[3, i] := SLProcOst.Values[kod];
    end;
  finally
    tIniF.Free
  end;
end;

procedure TfmProcent.rb_formClick(Sender: TObject);
var i, idx:Integer;
  kod, fn:string;
  tIniF: TIniFile;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    seOst_11.Enabled := True;
    seOst_12.Enabled := True;
    lb_11.Enabled := True;
    lb_12.Enabled := True;
    SG_FNS.Enabled := False;
    TypeOst := 1;
    tIniF.WriteInteger(SectionName,'TypeOst',TypeOst);
    FillFNSOst;
    for i := SG_FNS.FixedRows to SG_FNS.RowCount - 1 do
    begin
      kod := SG_FNS.Cells[1, i];
      idx := SLProcOst.IndexOfName(kod);
      if idx <> -1 then
        SG_FNS.Cells[3, i] := SLProcOst.Values[kod];
    end;
  finally
    tIniF.Free;
  end;
end;

procedure TfmProcent.seOst_11Exit(Sender: TObject);
begin
  SaveProcOst(seOst_11.Value,11);
  ProcOst_11 := seOst_11.Value;
  FillFNSOst;
end;

procedure TfmProcent.seOst_12Exit(Sender: TObject);
begin
  SaveProcOst(seOst_12.Value,12);
  ProcOst_12 := seOst_12.Value;
  FillFNSOst;
end;

procedure TfmProcent.SG_FNSExit(Sender: TObject);
var
  fn, s:string;
  i, idx:Integer;
  SL:TStringList;
  tIniF: TIniFile;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  SL:=TStringList.Create;
  try
  SL.Assign(SG_FNS.Cols[1]);
  if TypeOst = 2 then
  begin
    for i := SG_FNS.FixedRows to SG_FNS.RowCount - 1 do
    begin
      s := SG_FNS.Cells[1,i];
      idx := SLProcOst.IndexOfName(s);
      if idx <> -1 then
        SLProcOst.Values[s] := SG_FNS.Cells[3,i]
    end;

    for i := 0 to SLProcOst.Count - 1 do
    begin
      idx := SL.IndexOf(SLProcOst.Names[i]);
      if idx <> -1 then
        tIniF.WriteString('ProcOst',SLProcOst.Names[i],
          SLProcOst.Values[SLProcOst.Names[i]]);
    end;
  end;
  finally
    SL.Free;
    tIniF.Free;
  end;
end;

end.
