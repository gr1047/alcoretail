unit CopyToSQLite;

interface
 uses
   Forms, Dialogs, DB, ADODB, ImgList, Controls, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, ComCtrls, StdCtrls, Buttons, JvExExtCtrls,
  JvExtComponent, JvRollOut, Classes, ExtCtrls, IniFiles, ShlObj, SysUtils,
  Registry, Windows, System.ImageList, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Stan.ExprFuncs,
  FireDAC.Phys.SQLiteDef, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Phys.SQLite, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.DataSet;

  type
  sqlite = class(TObject)
    constructor Create(fn : string; Q1, Q2 : TADOConnection);
    procedure AddToSQLite(Q1 : TAdoQuery; FDQ : TFDQuery; Tab : String);

  end;

implementation

procedure sqlite.AddToSQLite(Q1 : TAdoQuery; FDQ : TFDQuery; Tab : String);
 var Q : TAdoQuery;
     s, del,fl : WideString;
     strdatetime : String;
     i, n : integer;
Begin
   Q := Q1;
   n := 0;
   Q.Active := True;
   FDQ.SQL.Text := 'select * from ' + Tab;
   FDQ.Active := True;

   while Q.Eof = False do
     Begin
       inc(n);
       FDQ.Insert;
       FDQ.CopyFields(Q);
       try
         FDQ.Post;
       except
       end;

       Q.Next;
     End;

End;


constructor  sqlite.Create(fn: string; Q1, Q2 : TADOConnection);
 var FDC : TFDConnection;
     FDQ : TFDQuery;
     Qr1, Qr2 : TADOQuery;
     SL  : TStringList;
     i   : Integer;
     s   : String;
     list : TStringList;
begin
  FDC := TFDConnection.Create(Nil);
  FDQ := TFDQuery.Create(Nil);
  Qr1 := TADOQuery.Create(Nil);
  Qr2 := TADOQuery.Create(Nil);
  Qr1.Connection := Q1;
  Qr2.Connection := Q2;
  SL := TStringList.Create;
  List := TStringList.Create;

  FDC.Connected := False;
  FDC.Params.Add('DriverID=SQLite');
  FDC.Params.Add('Database=' + fn);
  FDC.Connected := True;
  FDC.Open();
  FDQ.Connection := FDC;

 //====================================================
    SL.Clear;
    SL.Add('CREATE TABLE if not exists Declaration (');
    SL.Add('PK integer primary key,');
    SL.Add('DeclType VARCHAR(1),');
    SL.Add('ReportDate TIMESTAMP,');
    SL.Add('SelfKPP VARCHAR(9),');
    SL.Add('DeclNumber VARCHAR(50),');
    SL.Add('DeclDate TIMESTAMP,');
    SL.Add('SalerINN VARCHAR(12),');
    SL.Add('SalerKPP VARCHAR(9),');
    SL.Add('EAN13 VARCHAR(13),');
    SL.Add('AMOUNT VARCHAR(10),');
    SL.Add('IsGood SMALLINT,');
    SL.Add('TM VARCHAR(50),');
    SL.Add('inn VARCHAR(12),');
    SL.Add('kpp VARCHAR(9)');
    SL.Add(');');

    SL.Add('CREATE TABLE if not exists Production (');
    SL.Add('	EAN13 VARCHAR(13),');
    SL.Add('	FNSCode VARCHAR(3),');
    SL.Add('	Productname VARCHAR(180),');
    SL.Add('	AlcVol DOUBLE,');
    SL.Add('	Capacity DOUBLE,');
    SL.Add('	ProducerCode INTEGER,');
    SL.Add('	LocRef BOOLEAN,');
    SL.Add('	SelfMade BOOLEAN,');
    SL.Add('	prodkod VARCHAR(12),');
    SL.Add('	prodcod VARCHAR(9)');
    SL.Add(');');

    SL.Add('CREATE TABLE if not exists Producer (');
    SL.Add('	PK integer primary key,');
    SL.Add('	FullName VARCHAR(180),');
    SL.Add('	INN VARCHAR(12),');
    SL.Add('	KPP VARCHAR(9),');
    SL.Add('	countrycode VARCHAR(3),');
    SL.Add('	RegionCode VARCHAR(2),');
    SL.Add('	Address VARCHAR(180),');
    SL.Add('	LocRef BOOLEAN,');
    SL.Add('	SelfMade BOOLEAN');
    SL.Add(');');

    SL.Add('CREATE TABLE if not exists Saler (');
    SL.Add('	PK integer primary key,');
    SL.Add('	OrgName VARCHAR(180),');
    SL.Add('	OrgINN VARCHAR(12),');
    SL.Add('	OrgKPP VARCHAR(9),');
    SL.Add('	RegionCode VARCHAR(2),');
    SL.Add('	Address VARCHAR(180),');
    SL.Add('	LocRef BOOLEAN,');
    SL.Add('	SelfMade BOOLEAN');
    SL.Add(');');

    SL.Add('create table if not exists Saler_License (');
    SL.Add('	pk integer primary key,');
    SL.Add('	saler_pk varchar(12),');
    SL.Add('	ser varchar(12),');
    SL.Add('	num varchar(20),');
    SL.Add('	Date_start datestamp,');
    SL.Add('	Date_finish datestamp,');
    SL.Add('	regOrg varchar(250),');
    SL.Add('	saler_kpp varchar(9)');
    SL.Add(');');
 //====================================================
      // ������ ������� ��� sqlite ����
     for i := 0 to SL.Count-1 do
        Begin
         s := SL[i];
         if trim(s)='' then continue;
         FDQ.SQL.Add(s);
         if pos(';', s)<>0 then
           begin
            FDQ.ExecSQL;
            FDQ.SQL.Clear;
           end;
        End;

   FDQ.SQL.Text := 'delete from Declaration';
   FDQ.ExecSQL;

   FDQ.SQL.Text := 'delete from Production';
   FDQ.ExecSQL;

   FDQ.SQL.Text := 'delete from Producer';
   FDQ.ExecSQL;

   FDQ.SQL.Text := 'delete from Saler';
   FDQ.ExecSQL;

   FDQ.SQL.Text := 'delete from Saler_License';
   FDQ.ExecSQL;

   qr1.SQL.Text := 'select * from Declaration';
   qr1.Active := True;
   qr1.First;

   Q2.GetTableNames(list);



   if list.IndexOf('Declaration') = -1 then
     try
      //�������� ������� ������� � ref. ������ ��� � dat
       qr2.SQL.Text := 'CREATE TABLE Declaration ( ' +
          ' PK INT PRIMARY KEY, ' +
          ' DeclType VARCHAR(1), ' +
          ' ReportDate TIMESTAMP, ' +
          ' SelfKPP VARCHAR(9), ' +
          ' DeclNumber VARCHAR(50), ' +
          ' DeclDate TIMESTAMP, ' +
          ' SalerINN VARCHAR(12), ' +
          ' SalerKPP VARCHAR(9), ' +
          ' EAN13 VARCHAR(13), ' +
          ' AMOUNT VARCHAR(10), ' +
          ' IsGood SMALLINT, ' +
          ' M VARCHAR(50), ' +
          ' inn VARCHAR(12), ' +
          ' kpp VARCHAR(9) ' +
          ' )';
       Qr2.ExecSQL;
     finally

     end;

   qr2.SQL.Text := 'delete from Declaration';
   qr2.ExecSQL;
   qr2.SQL.Text := 'select * from Declaration';
   qr2.Active := True;

  while qr1.Eof = False do
   Begin
     Qr2.Insert;
     Qr2.CopyFields(Qr1);
     qr2.Post;
     qr1.Next;
   End;


      {
   -- ���������
    select distinct t2.* from Declaration t1
    inner join
    Production t2
    on t1.EAN13 = t2.EAN13

    -- �������������
    select distinct t2.* from
    (select distinct t2.* from Declaration t1
    inner join
    Production t2
    on t1.EAN13 = t2.EAN13)t1
    inner join
    Producer t2
    on t1.prodcod = t2.KPP and t1.prodkod = t2.INN

    --Saler
    select distinct t2.* from Declaration t1
    inner join
    Saler t2
    on t1.SalerINN  = t2.OrgINN and t1.SalerKPP  = t2.OrgKPP

    --Saler_license
    select distinct t2.* from
    (select distinct t2.* from Declaration t1
    inner join
    Saler t2
    on t1.SalerINN  = t2.OrgINN and t1.SalerKPP  = t2.OrgKPP)t1
    inner join
    Saler_license t2
    on t1.OrgINN = t2.saler_pk and t1.OrgKPP = t2.saler_kpp
   }

   s := 'select * from Declaration';
   Qr1.SQL.Text := s;
   AddToSQLite(Qr1,  FDQ, 'Declaration');

   s := ' select distinct t2.* from ' +
    ' (select distinct t2.* from Declaration t1 ' +
    ' inner join ' +
    ' Production t2 ' +
    ' on t1.EAN13 = t2.EAN13)t1 ' +
    ' inner join ' +
    ' Producer t2 ' +
    ' on t1.prodcod = t2.KPP and t1.prodkod = t2.INN ';
   Qr2.SQL.Text := s;
   AddToSQLite(Qr2,  FDQ, 'Producer');


   s := 'select distinct t2.* from Declaration t1 ' +
    ' inner join ' +
    ' Production t2 ' +
    ' on t1.EAN13 = t2.EAN13 ';
   Qr2.SQL.Text := s;
   AddToSQLite(Qr2,  FDQ, 'Production');

   s := 'select distinct t2.* from Declaration t1 ' +
   ' inner join ' +
   ' Saler t2 ' +
   ' on t1.SalerINN  = t2.OrgINN and t1.SalerKPP  = t2.OrgKPP ';
   Qr2.SQL.Text := s;
   AddToSQLite(Qr2,  FDQ, 'Saler');

   s := ' select distinct t2.* from ' +
    ' (select distinct t2.* from Declaration t1 '+
    ' inner join ' +
    ' Saler t2 ' +
    ' on t1.SalerINN  = t2.OrgINN and t1.SalerKPP  = t2.OrgKPP)t1 ' +
    ' inner join ' +
    ' Saler_license t2 ' +
    ' on t1.OrgINN = t2.saler_pk and t1.OrgKPP = t2.saler_kpp ';
   Qr2.SQL.Text := s;

   AddToSQLite(Qr2,  FDQ, 'Saler_License');

end;

end.

