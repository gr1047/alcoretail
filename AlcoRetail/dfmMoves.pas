unit dfmMoves;

interface

uses
  Forms,GlobalConst, ExtCtrls, StdCtrls, Controls, Mask, AdvSpin, Classes,
  SysUtils;

type
  TfmMoves = class(TForm)
    leEAN: TLabeledEdit;
    cbKPP2: TComboBox;
    lb1: TLabel;
    leKPP1: TLabeledEdit;
    leOst: TLabeledEdit;
    rb1: TRadioButton;
    rb2: TRadioButton;
    btnSave: TButton;
    btnSave1: TButton;
    tmr1: TTimer;
    edtCount: TAdvSpinEdit;
    GB_1: TGroupBox;
    leSaInn: TLabeledEdit;
    leSalKpp: TLabeledEdit;
    lb2: TLabel;
    mmoSalName: TMemo;
    GB_2: TGroupBox;
    lb3: TLabel;
    leProdInn: TLabeledEdit;
    leProdKpp: TLabeledEdit;
    mmoProdName: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure rb2Click(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    EAN13:string[13];
    KPP1, KPP2:string[9];
    Ost, Kolvo:Extended;
    sal:TSaler;
    prod:TProduction;
  end;

var
  fmMoves: TfmMoves;

implementation

uses GlobalUtils, Global;
{$R *.dfm}

procedure TfmMoves.btnSaveClick(Sender: TObject);
begin
  KPP2 := cbKPP2.Items.Strings[cbKPP2.ItemIndex];
  Kolvo := edtCount.FloatValue;
end;

procedure TfmMoves.FormCreate(Sender: TObject);
begin
  leEAN.Text := '';
end;

procedure TfmMoves.FormShow(Sender: TObject);
var idx:Integer;
begin
  leOst.Text:=FloatToStr(Ost);
  leEAN.Text:=string(EAN13);
  leKPP1.Text:=string(KPP1);
  edtCount.Visible:=False;

  AssignCBSL(cbKPP2,SLKPP);
  idx:=cbKPP2.Items.IndexOf(string(KPP1));
  cbKPP2.Items.Delete(idx);
  cbKPP2.ItemIndex := 0;

  edtCount.FloatValue := StrToFloat(leOst.Text);
  edtCount.MaxFloatValue := StrToFloat(leOst.Text);

  leSaInn.Text := string(sal.sInn);
  leSalKpp.Text := string(sal.sKpp);
  mmoSalName.Text := string(sal.sName);

  leProdInn.Text := string(prod.prod.pInn);
  leProdKpp.Text := string(prod.prod.pKpp);
  mmoProdName.Text := string(prod.prod.pName);
end;

procedure TfmMoves.rb1Click(Sender: TObject);
begin
  if rb1.Checked then
    edtCount.Visible :=False;
  edtCount.FloatValue := Ost;
end;

procedure TfmMoves.rb2Click(Sender: TObject);
begin
  if rb2.Checked then
    edtCount.Visible :=True;
  edtCount.Value := 0;
end;

procedure TfmMoves.tmr1Timer(Sender: TObject);
begin
  if (cbKPP2.ItemIndex = -1) or (edtCount.Value = 0) then
    btnSave.Enabled :=False
  else
    btnSave.Enabled :=True;
end;

end.
