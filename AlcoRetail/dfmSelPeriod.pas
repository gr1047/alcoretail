unit dfmSelPeriod;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, AdvSpin, Buttons;

type
  TfmSelPeriod = class(TForm)
    btn1: TBitBtn;
    lb_lb1: TLabel;
    edt1: TAdvSpinEdit;
    cbAllKvart: TComboBox;
    lb_1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure edt1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    selKvart, selYear: integer;
  end;

var
  fmSelPeriod: TfmSelPeriod;

implementation

uses Global;
{$R *.dfm}

procedure TfmSelPeriod.cbAllKvartChange(Sender: TObject);
begin
  selKvart := cbAllKvart.ItemIndex+1;
end;

procedure TfmSelPeriod.edt1Change(Sender: TObject);
begin
  selYear := edt1.Value;
end;

procedure TfmSelPeriod.FormShow(Sender: TObject);
begin
  selKvart:=CurKvart;
  selYear :=CurYear;
  cbAllKvart.ItemIndex := CurKvart-1;
end;

end.
