unit dfmEditAdres;

interface

uses
  Forms, StdCtrls, Buttons, Classes, Controls, ExtCtrls;

type
  TfmEditAdres = class(TForm)
    leCountrycode: TLabeledEdit;
    leIndexP: TLabeledEdit;
    leRaipo: TLabeledEdit;
    leCity: TLabeledEdit;
    leNasPunkt: TLabeledEdit;
    leStreet: TLabeledEdit;
    leHouse: TLabeledEdit;
    leStruct: TLabeledEdit;
    leABC: TLabeledEdit;
    leQuart: TLabeledEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    leCodereg: TLabeledEdit;
    leFullAdr: TLabeledEdit;
    procedure leExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function SetAdres:string;
  public
    { Public declarations }
    IndexKpp:Integer;
  end;

var
  fmEditAdres: TfmEditAdres;

implementation

{$R *.dfm}

procedure TfmEditAdres.FormCreate(Sender: TObject);
begin
  IndexKpp:=-1;
  leCountrycode.Text := '643';
end;

procedure TfmEditAdres.leExit(Sender: TObject);
begin
  leFullAdr.Text:= SetAdres;
end;

function TfmEditAdres.SetAdres:string;
begin
  Result:='';
  if leCountrycode.Text <> '' then
    Result:= Result + leCountrycode.Text;

  if leIndexP.Text <> '' then
    Result:= Result + ',' + leIndexP.Text;

  if leCodereg.Text <> '' then
    Result:= Result + ',' + leCodereg.Text;

  if leRaipo.Text <> '' then
    Result:= Result + ',' + leRaipo.Text + ' �-�';

  if leCity.Text <> '' then
    Result:= Result + ', �.' + leCity.Text;

  if leNasPunkt.Text <> '' then
    Result:= Result + ',' + leNasPunkt.Text;

  if leStreet.Text <> '' then
    Result:= Result + ', ��.' + leStreet.Text;

  if leHouse.Text <> '' then
    Result:= Result + ', �.' + leHouse.Text;

  if leStruct.Text <> '' then
    Result:= Result + ', ����.' + leStruct.Text;

  if leABC.Text <> '' then
    Result:= Result + ', ' + leABC.Text;

  if leQuart.Text <> '' then
    Result:= Result + ', ��.' + leQuart.Text;
end;
end.
