object fmErrXML: TfmErrXML
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1054#1096#1080#1073#1082#1080' XML'
  ClientHeight = 253
  ClientWidth = 712
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmo_err: TMemo
    Left = 0
    Top = 0
    Width = 712
    Height = 201
    Align = alTop
    Color = 16056309
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Ch_XML: TCheckBox
    Left = 232
    Top = 228
    Width = 369
    Height = 17
    Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' XML '#1092#1072#1081#1083' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072' '#1074'  '#1041#1052'-'#1057#1048#1057#1058#1045#1052
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Button1: TButton
    Left = 607
    Top = 220
    Width = 97
    Height = 25
    Caption = 'Ok'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 2
    OnClick = Button1Click
  end
end
