object fmMoves: TfmMoves
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103' '#1074#1085#1091#1090#1088#1080' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 498
  ClientWidth = 574
  Color = 15794161
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object lb1: TLabel
    Left = 297
    Top = 361
    Width = 50
    Height = 18
    Caption = #1085#1072' '#1050#1055#1055
  end
  object leEAN: TLabeledEdit
    Left = 203
    Top = 8
    Width = 161
    Height = 26
    TabStop = False
    Color = 15794161
    EditLabel.Width = 48
    EditLabel.Height = 18
    EditLabel.Caption = 'EAN13 '
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 0
  end
  object cbKPP2: TComboBox
    Left = 363
    Top = 358
    Width = 187
    Height = 26
    Style = csDropDownList
    TabOrder = 1
  end
  object leKPP1: TLabeledEdit
    Left = 67
    Top = 358
    Width = 187
    Height = 26
    TabStop = False
    Color = 15794161
    EditLabel.Width = 46
    EditLabel.Height = 18
    EditLabel.Caption = #1089' '#1050#1055#1055' '
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 2
  end
  object leOst: TLabeledEdit
    Left = 131
    Top = 396
    Width = 81
    Height = 26
    TabStop = False
    Color = 15794161
    EditLabel.Width = 119
    EditLabel.Height = 18
    EditLabel.Caption = #1090#1077#1082#1091#1097#1080#1081' '#1086#1089#1090#1072#1090#1086#1082
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 3
  end
  object rb1: TRadioButton
    Left = 218
    Top = 400
    Width = 113
    Height = 17
    Caption = #1074#1077#1089#1100' '#1086#1089#1090#1072#1090#1086#1082
    Checked = True
    TabOrder = 4
    TabStop = True
    OnClick = rb1Click
  end
  object rb2: TRadioButton
    Left = 337
    Top = 400
    Width = 105
    Height = 17
    Caption = #1082#1086#1083'-'#1074#1086' ('#1073#1091#1090')'
    TabOrder = 5
    OnClick = rb2Click
  end
  object btnSave: TButton
    Left = 313
    Top = 443
    Width = 120
    Height = 25
    Caption = #1055#1077#1088#1077#1085#1077#1089#1090#1080
    Default = True
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 6
    OnClick = btnSaveClick
  end
  object btnSave1: TButton
    Left = 439
    Top = 443
    Width = 120
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 7
  end
  object edtCount: TAdvSpinEdit
    Left = 448
    Top = 396
    Width = 115
    Height = 28
    Precision = 4
    SpinType = sptFloat
    Value = 1
    FloatValue = 1.000000000000000000
    HexValue = 0
    CheckMinValue = True
    CheckMaxValue = True
    Color = clWhite
    IncrementFloat = 0.100000000000000000
    IncrementFloatPage = 1.000000000000000000
    IncrementSmart = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    MinValue = 1
    TabOrder = 8
    Visible = False
    Version = '1.5.3.4'
  end
  object GB_1: TGroupBox
    Left = 8
    Top = 200
    Width = 551
    Height = 137
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    TabOrder = 9
    object lb2: TLabel
      Left = 16
      Top = 56
      Width = 100
      Height = 18
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    end
    object leSaInn: TLabeledEdit
      Left = 51
      Top = 24
      Width = 187
      Height = 26
      TabStop = False
      Color = 15794161
      EditLabel.Width = 30
      EditLabel.Height = 18
      EditLabel.Caption = #1048#1053#1053
      LabelPosition = lpLeft
      ReadOnly = True
      TabOrder = 0
    end
    object leSalKpp: TLabeledEdit
      Left = 300
      Top = 24
      Width = 187
      Height = 26
      TabStop = False
      Color = 15794161
      EditLabel.Width = 29
      EditLabel.Height = 18
      EditLabel.Caption = #1050#1055#1055
      LabelPosition = lpLeft
      ReadOnly = True
      TabOrder = 1
    end
    object mmoSalName: TMemo
      Left = 16
      Top = 80
      Width = 521
      Height = 54
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = 15794161
      TabOrder = 2
    end
  end
  object GB_2: TGroupBox
    Left = 8
    Top = 40
    Width = 551
    Height = 137
    Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    TabOrder = 10
    object lb3: TLabel
      Left = 16
      Top = 56
      Width = 100
      Height = 18
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    end
    object leProdInn: TLabeledEdit
      Left = 51
      Top = 24
      Width = 187
      Height = 26
      TabStop = False
      Color = 15794161
      EditLabel.Width = 30
      EditLabel.Height = 18
      EditLabel.Caption = #1048#1053#1053
      LabelPosition = lpLeft
      ReadOnly = True
      TabOrder = 0
    end
    object leProdKpp: TLabeledEdit
      Left = 300
      Top = 24
      Width = 187
      Height = 26
      TabStop = False
      Color = 15794161
      EditLabel.Width = 29
      EditLabel.Height = 18
      EditLabel.Caption = #1050#1055#1055
      LabelPosition = lpLeft
      ReadOnly = True
      TabOrder = 1
    end
    object mmoProdName: TMemo
      Left = 16
      Top = 80
      Width = 521
      Height = 54
      BevelInner = bvNone
      BevelOuter = bvNone
      Color = 15794161
      TabOrder = 2
    end
  end
  object tmr1: TTimer
    OnTimer = tmr1Timer
    Left = 80
    Top = 432
  end
end
