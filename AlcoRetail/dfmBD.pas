unit dfmBD;

interface

uses Forms, StdCtrls, Buttons, Controls, ExtCtrls, Classes, ShellAPI, Windows;

type
  TfmBD = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    cbDecl: TComboBox;
    SpeedButton1: TSpeedButton;
    GroupBox4: TGroupBox;
    Label1: TLabel;
    SpeedButton2: TSpeedButton;
    cbRef: TComboBox;
    GroupBox5: TGroupBox;
    BitBtn1: TBitBtn;
    SpeedButton5: TSpeedButton;
    Panel1: TPanel;
    Panel2: TPanel;
    ePathArh: TEdit;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    ePathBD: TEdit;
    SpeedButton4: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBD: TfmBD;

implementation

uses DfmWork, Global, GlobalUtils, dfmInfo, GlobalConst;
{$R *.dfm}

procedure TfmBD.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmBD.FormCreate(Sender: TObject);
begin
  ePathBD.Text := path_db;
  ePathArh.Text := path_arh;
  UpdPointSpr(cbRef);
  UpdPointDecl(cbDecl);
end;

procedure TfmBD.SpeedButton10Click(Sender: TObject);
begin
  if NewSvertkaDB(fmWork.que1_sql) then
    Show_Inf('������� ���� ��������� �������', fInfo)
  else
    Show_Inf('������ ������� ����', fError);
end;

procedure TfmBD.SpeedButton1Click(Sender: TObject);
var dfm:TfmInfo;
    rez: boolean;
begin
  dfm:=TfmInfo.Create(Self);
  dfm.lText.Caption := '�������������� ���� ������ ���������� ' +
                       '����� �������� � ������ ������ ���������� ' +
                       '� ���������, �� �������������� � �����������.' + #10#13 +
                       '���������� ��������������?';
  if dfm.ShowModal = mrOk then
    with cbDecl do
      begin
        fmWork.con1ADOCon.Close;
        Screen.Cursor := crHourGlass;
        rez := Copy2DB(db_decl);
        Screen.Cursor := crDefault;
        if rez then
          begin
            dfm.lText.Caption := '��� ���������� � ���� ��������� ���������� ������������� ���������.' + #10#13 +
                                 '�� - ����p���������� ��������� ����������' +#10#13 +
                                 '��� - ����p���������� ��������� ����� ��������������';
            if dfm.ShowModal = mrOk then
              Application.Terminate;
          end;
      end;
  dfm.Free;
end;

procedure TfmBD.SpeedButton2Click(Sender: TObject);
var dfm:TfmInfo;
    rez: boolean;
begin
  dfm:=TfmInfo.Create(Self);
  dfm.lText.Caption := '�������������� ���� ������ ������������ ' +
                       '����� �������� � ������ ������ ���������� ' +
                       '� ���������, �� �������������� � �����������.' + #10#13 +
                       '���������� ��������������?';
  if dfm.ShowModal = mrOk then
    with cbRef do
      begin
       fmWork.con2ADOCon.Close;
       Screen.Cursor := crHourGlass;
       rez := Copy2Ref(db_spr);
       Screen.Cursor := crDefault;
       //if rez then Application.Terminate;
       if rez then
          begin
            dfm.lText.Caption := '��� ���������� � ���� ��������� ���������� ������������� ���������.' + #10#13 +
                                 '�� - ����p���������� ��������� ����������' +#10#13 +
                                 '��� - ����p���������� ��������� ����� ��������������';
            if dfm.ShowModal = mrOk then
              Application.Terminate;
          end;
      end;
  dfm.Free;
end;

procedure TfmBD.SpeedButton4Click(Sender: TObject);
begin
 fmWork.con1ADOCon.Close;
 if DB2Arh(db_decl,path_arh+'\Decl_BackUp_'+fOrg.Inn+'_'+DTPart+'.rbk') then
   Show_Inf('�������� ��������� ����� ���������� ���������.', fInfo)
 else
   Show_Inf('������ ��� �������� ��������� ����� ����������.', fError);
  UpdPointDecl(cbDecl);
end;

procedure TfmBD.SpeedButton5Click(Sender: TObject);
begin
  fmWork.con2ADOCon.Close;
  if DB2Arh(db_spr,path_arh+'\Refs_BackUp_'+fOrg.Inn+'_'+DTPart+'.rbk') then
    Show_Inf('�������� ��������� ����� ������������ ���������.', fInfo)
  else
    Show_Inf('������ ��� �������� ��������� ����� ������������.', fError);
  UpdPointSpr(cbRef);
end;

procedure TfmBD.SpeedButton6Click(Sender: TObject);
var s:string;
begin
  s:=ePathArh.Text;
  ShellExecute(Self.Handle,'open',PWideChar(s),nil,nil,SW_SHOW);
end;

procedure TfmBD.SpeedButton7Click(Sender: TObject);
var s:string;
begin
  s:=ePathBD.Text;
  ShellExecute(Self.Handle,'open',PWideChar(s),nil,nil,SW_SHOW);
end;

procedure TfmBD.SpeedButton8Click(Sender: TObject);
var dfm:TfmInfo;
    rez: boolean;
begin
  dfm:=TfmInfo.Create(Self);
  dfm.lText.Caption := '�������������� ���� ������ ������������ ' +
                       '����� �������� � ������ ������ ���������� ' +
                       '� ���������, �� �������������� � �����������.' + #10#13 +
                       '���������� ��������������?';
  if dfm.ShowModal = mrOk then
    with cbRef do
      begin
       fmWork.con2ADOCon.Close;
       Screen.Cursor := crHourGlass;
       rez := Arh2DB(db_spr,path_arh+'\'+ID_Z(Items[ItemIndex]), psw_loc, True);
       Screen.Cursor := crDefault;
       VostBase:=True;
       if rez then Application.Terminate;
      end;
  dfm.Free;
end;

procedure TfmBD.SpeedButton9Click(Sender: TObject);
var dfm:TfmInfo;
    rez: boolean;
begin
  dfm:=TfmInfo.Create(Self);
  dfm.lText.Caption := '�������������� ���� ������ ���������� ' +
                       '����� �������� � ������ ������ ���������� ' +
                       '� ���������, �� �������������� � �����������.' + #10#13 +
                       '���������� ��������������?';
  if dfm.ShowModal = mrOk then
    with cbDecl do
      begin
       fmWork.con1ADOCon.Close;
       Screen.Cursor := crHourGlass;
       rez := Arh2DB(db_decl,path_arh+'\'+ID_Z(Items[ItemIndex]), fOrg.Inn, True);
       Screen.Cursor := crDefault;
       VostDecl :=True;
       if rez then Application.Terminate;
      end;
  dfm.Free;
end;


end.
