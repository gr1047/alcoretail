unit dfmMove;

interface

uses
  Forms, GlobalConst, IniFiles, ShlObj, Clipbrd, DateUtils, Menus, ExtCtrls,
  ImgList, Controls, StdCtrls, ComCtrls, Grids, AdvObj, BaseGrid, AdvGrid,
  ToolWin, SysUtils, Classes, Graphics, Windows, AdvUtil;

type
  TfmMove = class(TForm)
    pgc1: TPageControl;
    ts_Prih: TTabSheet;
    pErrPrih: TPanel;
    labErrPrih: TLabel;
    lblRowErrPrih: TLabel;
    pnl_Decl: TPanel;
    tlb_Decl: TToolBar;
    btn_Save: TToolButton;
    btn_Import: TToolButton;
    btn_Export: TToolButton;
    sep1: TToolButton;
    btn_Find: TToolButton;
    btn_Replace: TToolButton;
    sep2: TToolButton;
    btn_Copy: TToolButton;
    btn_Cut: TToolButton;
    btn_Past: TToolButton;
    sep3: TToolButton;
    btn_AddR: TToolButton;
    btn_DelR: TToolButton;
    btn_DelAll: TToolButton;
    btn_DupR: TToolButton;
    sep6: TToolButton;
    btAutoRash: TToolButton;
    sep4: TToolButton;
    btn_FirstErr: TToolButton;
    btn_LastErr: TToolButton;
    sep5: TToolButton;
    UndoBtn: TToolButton;
    RedoBtn: TToolButton;
    sep7: TToolButton;
    pCap: TPanel;
    pEdit: TPanel;
    ll: TLabel;
    pFile: TPanel;
    Label3: TLabel;
    pFind: TPanel;
    Label4: TLabel;
    pRow: TPanel;
    Label1: TLabel;
    pErr: TPanel;
    Label5: TLabel;
    pPravka: TPanel;
    Label7: TLabel;
    pOder: TPanel;
    Label10: TLabel;
    ts_Rash: TTabSheet;
    pErrRash: TPanel;
    labErrRash: TLabel;
    ts_VPost: TTabSheet;
    SG_VPost: TAdvStringGrid;
    Panel3: TPanel;
    labErrVPost: TLabel;
    ts_VPok: TTabSheet;
    SG_VPok: TAdvStringGrid;
    Panel4: TPanel;
    lblErrVPok: TLabel;
    ts_Brak: TTabSheet;
    SG_Brak: TAdvStringGrid;
    Panel5: TPanel;
    labErrBrak: TLabel;
    ts_Moving: TTabSheet;
    SG_Moving: TAdvStringGrid;
    sb: TStatusBar;
    Panel1: TPanel;
    Label8: TLabel;
    lbl3: TLabel;
    Label9: TLabel;
    cbKPP: TComboBox;
    dtp_WorkDate: TDateTimePicker;
    cbAllKvart: TComboBox;
    Panel2: TPanel;
    Label6: TLabel;
    Button1: TButton;
    il2: TImageList;
    il3: TImageList;
    il1: TImageList;
    Timer1: TTimer;
    pmRef: TPopupMenu;
    N_Ref: TMenuItem;
    N5: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N6: TMenuItem;
    N1: TMenuItem;
    N41: TMenuItem;
    N4: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    mniN10: TMenuItem;
    mniN11: TMenuItem;
    mniN12: TMenuItem;
    mniN13: TMenuItem;
    SG_Prih: TAdvStringGrid;
    pnl1: TPanel;
    SG_Rash: TAdvStringGrid;
    GB_1: TGroupBox;
    SG_Dvizh: TAdvStringGrid;
    SG_1: TAdvStringGrid;
    mniN14: TMenuItem;
    spl1: TSplitter;
    btnOst: TToolButton;
    procedure btn_SaveClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure pgc1Changing(Sender: TObject; var AllowChange: Boolean);
    procedure FormShow(Sender: TObject);
    procedure cbKPPChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure N_RefClick(Sender: TObject);
    procedure SG_MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N1Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure btn_CopyClick(Sender: TObject);
    procedure btn_PastClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure btn_FindClick(Sender: TObject);
    procedure btn_ReplaceClick(Sender: TObject);
    procedure btn_DelRClick(Sender: TObject);
    procedure btn_DelAllClick(Sender: TObject);
    procedure btn_DupRClick(Sender: TObject);
    procedure btn_FirstErrClick(Sender: TObject);
    procedure btn_LastErrClick(Sender: TObject);
    procedure btn_ExportClick(Sender: TObject);
    procedure btn_ImportClick(Sender: TObject);
    procedure btn_AddRClick(Sender: TObject);
    procedure SG_CanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure SG_CellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
      FriendlyName: string; var FilterCondition: string);
    procedure SG_GetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
    procedure SG_GetEditorProp(Sender: TObject; ACol, ARow: Integer;
      AEditLink: TEditLink);
    procedure SG_GetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure SG_KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SG_SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btAutoRashClick(Sender: TObject);
    procedure mniN11Click(Sender: TObject);
    procedure SG_PrihFilterShow(Sender: TObject; ACol: Integer;
      var Allow: Boolean);
    procedure mniN12Click(Sender: TObject);
    procedure mniN13Click(Sender: TObject);
    procedure pmRefPopup(Sender: TObject);
    procedure mniN14Click(Sender: TObject);
    procedure btnOstClick(Sender: TObject);
    procedure N7Click(Sender: TObject);
  private
    { Private declarations }
    procedure SetPropertyForm;
    procedure VisibleButton;
    procedure EnableAllButton(val: Boolean);
    procedure WidthBanelButton;
  public
    { Public declarations }
    CountNew, countEdit: integer;
    procedure ShowDataGrid;
    procedure ShowDataAll;
    procedure ErrGrid(fTF: TTypeForm; cErr: integer);
    procedure CheckErrSG(fTF: TTypeForm; SG: TAdvStringGrid;
      ACol, ARow: Integer; var Value: string);
  end;

var
  fmMove: TfmMove;
  curSheet: TTabSheet;
  fTypeF: TTypeForm;
  colMouse, rowMouse, RowCopy: Integer;
  kppCur: string[9];

implementation

uses Global, Global2, GlobalUtils,
  dfmDataExchange, dfmMain, dfmWork, dfmFiltrRef, dfmSelKPP, dfmReplace,
  dfmMoving, dfmProgress, dfmAutoRash, dfmCorrect, dfmAllOst, dfmProcent;
{$R *.dfm}

procedure TfmMove.btAutoRashClick(Sender: TObject);
begin
  if fmAutoRash = nil then
    fmAutoRash := TfmAutoRash.Create(Self);
  fmAutoRash.ShowModal;
  if CurSG.RowCount = CurSG.FixedRows + CurSG.FixedFooters + 1 then
    PropCellsSG(arFixed, CurSG, CalcColRashNew, NoResColRashNew,
      HideColRashNew)
  else
    PropCellsSG(arNormal, CurSG, CalcColRashNew, NoResColRashNew,
      HideColRashNew);
end;

procedure TfmMove.btnOstClick(Sender: TObject);
begin
  if fmProcent = nil then
    fmProcent := TfmProcent.Create(Self);
  fmProcent.Tag := 3;
  fmProcent.ShowModal;
end;

procedure TfmMove.btn_AddRClick(Sender: TObject);
var
  Row: Integer;
  SLErr: TStringList;
  y,m,d:word;
  kv:integer;
  dat:TDateTime;
begin
  try
    SLErr := TStringList.Create;
    EnabledMainBut(False);
    case fTypeF of
      fMove:
        begin
          if fmMoving = nil then
            fmMoving := TfmMoving.Create(Self);
          fmMoving.TypeF := fMoving;
          ShapkaSGCheck(fmMoving.SG_Moving, NameColAddMoving,
            FilterColAddMoving);
          AssignCBSL(fmMoving.cbSource, SLKPP);
          if kppCur <> '' then
            fmMoving.cbSource.ItemIndex := cbKPP.Items.IndexOf(String(kppCur));
          fmMoving.cbSourceChange(fmMoving.cbSource);
          if fmMoving.ShowModal = mrOk then
          begin
            Moving(dtp_WorkDate.Date, fmWork.que1_sql, fmMoving.SG_Moving,
              SLAllDecl, SLDataMoving, SLOstatkiKpp, fmMoving.KppSourse,
              fmMoving.KppRecipient);
          end;
        end;
      fRashod:
        begin
          SLErr.Assign(SLErrRash);
          CurSG.InsertRows(CurSG.RowCount - CurSG.FixedFooters, 1, True);
          Row := CurSG.RowCount - CurSG.FixedFooters - 1;

          CurSG.Cells[3, Row] := cbKPP.Text;

//          CurSG.Cells[4, Row] := DateToStr(dtp_WorkDate.Date);
          DecodeDate(Now,y,m,d);
          dat := DateOf(Kvart_End(CurKvart, NowYear));

          if (DateOf(dtp_WorkDate.Date) <= dEnd) and
            (DateOf(dtp_WorkDate.Date) >= dBeg) then
            CurSG.Cells[4, Row] := DateToStr(dtp_WorkDate.Date)
          else
            CurSG.Cells[4, Row] := DateToStr(dEnd);



          CurSG.Times[5, Row] := Time;

          CurSG.Row := Row;

          SetColorRow(CurSG.RealRowIndex(Row), CurSG);
          // �������� ����
          SetColorCells(CurSG, [4], CurSG.RealRowIndex(Row), CheckDate(StrToDate(CurSG.Cells[4, Row])));
          SetColorCells(CurSG, [5, 6,7,8, 9,10,11,12],
            CurSG.RealRowIndex(CurSG.RowCount - CurSG.FixedFooters - 1), 1);
          if CurSG.RowCount = CurSG.FixedRows + CurSG.FixedFooters + 1 then
            PropCellsSG(arFixed, CurSG, CalcColRashNew, NoResColRashNew,
              HideColRashNew)
          else
            PropCellsSG(arNormal, CurSG, CalcColRashNew, NoResColRashNew,
              HideColRashNew);

        end;
      fVPok, fBrak:
        begin
          case fTypeF of
            fVPok: SLErr.Assign(SLErrVPok);
            fBrak: SLErr.Assign(SLErrBrak);
          end;
          CurSG.InsertRows(CurSG.RowCount - CurSG.FixedFooters, 1, True);
          Row := CurSG.RowCount - CurSG.FixedFooters - 1;

          CurSG.Cells[3, Row] := cbKPP.Text;
//          decodeDate(Now(),y,m,d);
//          if y = NowYear then
//          begin
//            kv:=Kvartal(m);
//            if CurKvart = kv then
//              CurSG.Cells[5, Row] := DateToStr(dtp_WorkDate.Date)
//            else
//              CurSG.Cells[5, Row] := DateToStr(dEnd);
//          end
//          else
//            CurSG.Cells[5, Row] := DateToStr(dEnd);

          DecodeDate(Now,y,m,d);
          dat := DateOf(Kvart_End(CurKvart, NowYear));

          if (DateOf(dtp_WorkDate.Date) <= dEnd) and
            (DateOf(dtp_WorkDate.Date) >= dBeg) then
            CurSG.Cells[5, Row] := DateToStr(dtp_WorkDate.Date)
          else
            CurSG.Cells[5, Row] := DateToStr(dEnd);


          CurSG.Row := Row;
          SetColorRow(CurSG.RealRowIndex(Row), CurSG);
          // �������� ����
          SetColorCells(CurSG, [5], CurSG.RealRowIndex(Row), CheckDate(StrToDate(CurSG.Cells[5, Row])));
          SetColorCells(CurSG, [6, 7,8, 9, 11, 12, 13],
            CurSG.RealRowIndex(CurSG.RowCount - CurSG.FixedFooters - 1), 1);
          if CurSG.RowCount = CurSG.FixedRows + CurSG.FixedFooters + 1 then
            PropCellsSG(arFixed, CurSG, CalcColBrak, NoResColBrak,
              HideColBrak)
          else
            PropCellsSG(arNormal, CurSG, CalcColPrih, NoResColPrih,
              HideColPrih);
        end;
      fPrihod, fVPost:
        begin
          case fTypeF of
            fPrihod: SLErr.Assign(SLErrPrih);
            fVPost: SLErr.Assign(SLErrVPost);
          end;
          CurSG.InsertRows(CurSG.RowCount - CurSG.FixedFooters, 1, True);
          Row := CurSG.RowCount - CurSG.FixedFooters - 1;

          CurSG.Cells[3, Row] := cbKPP.Text;

          DecodeDate(Now,y,m,d);
          dat := DateOf(Kvart_End(CurKvart, NowYear));

          if (DateOf(dtp_WorkDate.Date) <= dEnd) and
            (DateOf(dtp_WorkDate.Date) >= dBeg) then
            CurSG.Cells[5, Row] := DateToStr(dtp_WorkDate.Date)
          else
            CurSG.Cells[5, Row] := DateToStr(dEnd);

          CurSG.Row := Row;

          SetColorRow(CurSG.RealRowIndex(Row), CurSG);
          // �������� ����
          SetColorCells(CurSG, [5], CurSG.RealRowIndex(Row), CheckDate(StrToDate(CurSG.Cells[5, Row])));
          SetColorCells(CurSG, [4, 6, 7,8, 9,10, 11, 12, 14, 15, 16],
            CurSG.RealRowIndex(CurSG.RowCount - CurSG.FixedFooters - 1), 1);
          if CurSG.RowCount = CurSG.FixedRows + CurSG.FixedFooters + 1 then
            PropCellsSG(arFixed, CurSG, CalcColPrih, NoResColPrih,
              HideColPrih)
          else
            PropCellsSG(arNormal, CurSG, CalcColPrih, NoResColPrih,
              HideColPrih);
        end
      else
      begin
        CurSG.InsertRows(CurSG.RowCount - CurSG.FixedFooters, 1, True);
      end;
    end;
    if fTypeF <> fMove then
    begin
      Inc(CountNew);
      CountErrSG(fTypeF, SLErr, CurSG,
        CurSG.RowCount - CurSG.FixedFooters - 1);
      ErrGrid(fTypeF, CountNamesSL(SLErr));
    end;

    sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
    case fTypeF of
      fPrihod:
        SLErrPrih.Assign(SLErr);
      fRashod:
        SLErrRash.Assign(SLErr);
      fVPost:
        SLErrVPost.Assign(SLErr);
      fVPok:
        SLErrVPok.Assign(SLErr);
      fBrak:
        SLErrBrak.Assign(SLErr);
    end;
  finally
    SLErr.Free;
  end;
end;

procedure TfmMove.btn_CopyClick(Sender: TObject);
var
  s: string;
begin
  s := '';
  s := CopyCell(CurSG);
  Clipboard.AsText := s;
  RowCopy := CurSG.RealRowIndex(CurSG.Row);
end;

procedure TfmMove.btn_DelAllClick(Sender: TObject);
var
  SLErr: TStringList;
begin
  SLErr := TStringList.Create;
  EnabledMainBut(False);
  try
    case fTypeF of
      fPrihod:
        SLErr.Assign(SLErrPrih);
      fRashod:
        SLErr.Assign(SLErrRash);
      fVPost:
        SLErr.Assign(SLErrVPost);
      fVPok:
        SLErr.Assign(SLErrVPok);
      fBrak:
        SLErr.Assign(SLErrBrak);
    end;
    SortSLCol(1,SLAllDecl, CompNumAsc);
    SortSLCol(1,CurSL, CompNumAsc);

    sb.Panels.Items[3].Text := DeleteAllRowSG(CurSG, CurSL,
      SLErr, countEdit, CountNew);

    ErrGrid(fTypeF, CountNamesSL(SLErr));
    sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
    sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
    case fTypeF of
      fPrihod:
        SLErrPrih.Assign(SLErr);
      fRashod:
        SLErrRash.Assign(SLErr);
      fVPost:
        SLErrVPost.Assign(SLErr);
      fVPok:
        SLErrVPok.Assign(SLErr);
      fBrak:
        SLErrBrak.Assign(SLErr);
    end;
  finally
    SLErr.Free;
  end;
end;

procedure TfmMove.btn_DelRClick(Sender: TObject);
var
  SLErr: TStringList;
begin
  SLErr := TStringList.Create;
  EnabledMainBut(False);
  case fTypeF of
    fPrihod:
      SLErr.Assign(SLErrPrih);
    fRashod:
      SLErr.Assign(SLErrRash);
    fVPost:
      SLErr.Assign(SLErrVPost);
    fVPok:
      SLErr.Assign(SLErrVPok);
    fBrak:
      SLErr.Assign(SLErrBrak);
  end;
  try
    CountErrSG(fTypeF, SLErr, CurSG, CurSG.RealRow);
    SortSLCol(1,SLAllDecl, CompNumAsc);
    SortSLCol(1,CurSL, CompNumAsc);
    sb.Panels.Items[3].Text := DeleteRowSG(CurSG.Row, CurSG,
      CurSL, SLErr, countEdit, CountNew);
    sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
    sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);

    case fTypeF of
      fPrihod:
        SLErrPrih.Assign(SLErr);
      fRashod:
        SLErrRash.Assign(SLErr);
      fVPost:
        SLErrVPost.Assign(SLErr);
      fVPok:
        SLErrVPok.Assign(SLErr);
      fBrak:
        SLErrBrak.Assign(SLErr);
    end;
  finally
    SLErr.Free;
  end;
end;

procedure TfmMove.btn_DupRClick(Sender: TObject);
begin
  DupRow(CurSG.Row, CurSG);
  sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
  EnabledMainBut(False);
end;

procedure TfmMove.btn_ExportClick(Sender: TObject);
var
  fname: string;
  form: Integer;
  rez: Boolean;
begin
  case fTypeF of
    fPrihod:
      fmWork.dlgSave_Export.Filter :=
        '������� � TXT|*.txt|������� � XLS|*.xls|������� � DBF|*.dbf';
  else
    fmWork.dlgSave_Export.Filter := '������� � TXT|*.txt|������� � XLS|*.xls';
  end;
  if fmWork.dlgSave_Export.Execute then
  begin
    tlb_Decl.Enabled := False;
    Application.ProcessMessages;

    fname := fmWork.dlgSave_Export.FileName;
    form := fmWork.dlgSave_Export.FilterIndex;

    rez := SaveTable(fTypeF, form, fname, CurSG, fmWork.pbWork);
    if rez then
      Show_Inf('������� ��������� � ����� ' + fname, fInfo)
    else
      Show_Inf('������� � ��������� ������ ����������.', fError);
    tlb_Decl.Enabled := True;
    Application.ProcessMessages;
  end;
end;

procedure TfmMove.btn_FindClick(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  fmReplace.SGFind := CurSG;
  fmReplace.Show;
end;

procedure TfmMove.btn_FirstErrClick(Sender: TObject);
begin
  FindFirstErr(CurSG);
end;

procedure TfmMove.btn_ImportClick(Sender: TObject);
var
  tFile: TTypeFile;
  idx:integer;
  ti:TInformation;
  Ref, ExtVozvr:Boolean;
begin
  CurSG.FilterActive := False;
  fmWork.dlgOpen_Import.Title := '����� ����� ��� �������: ';
  fmWork.dlgOpen_Import.Filter :=
    '������ �� DBF|*.dbf|������ �� XLS|*.xls|������ �� XML|*.xml|������ ����������|*.dbf; *.xls; *.xml';
  try
    try
      if fmWork.dlgOpen_Import.Execute then
      begin
        tFile := GetTypeFile
          (ANSIUpperCase(ExtractFileExt(fmWork.dlgOpen_Import.FileName)));
        Screen.Cursor := crHourGlass;
        case tFile of
          fDBF:
            begin
              try
                case fTypeF of
                  fPrihod, fVPost:
                    Ref:=DbfIsPrihod(fmWork.dlgOpen_Import.FileName, True);
                  fRashod:
                    Ref:=DbfIsPrihod(fmWork.dlgOpen_Import.FileName, False);
                end;
                // �������� ����� DBF
                if not Ref then
                begin
                  idx:=ImportDBF(fTypeF, curDate, fmWork.dlgOpen_Import.FileName,
                    CurSG, fmWork.pbWork);
                  if idx = 0 then ti := fInfo
                  else ti:= fError;
                  Show_Inf(Inf_ImportDBF[idx], ti);
                end
                else
                  Show_Inf('�������� ��������� �����.' + #10#13 +
                    '�������� �� ������� ���� �����������.' + #10#13 +
                    '�������� ������ DBF ����.',fError);
              except
                Show_Inf(
                  '������ ������ ������������� ����� �������� ���������.' +
                  #13#10 +
                  '�������� ���� DBF ��������� ��� ����� ������������ ������'
                  , fError);
              end;
            end;
          fXML:
            begin
              try
                idx := ImportXML(fTypeF, fmWork.dlgOpen_Import.FileName,
                  CurSG, fmWork.que2_sql, fmWork.pbWork, curDate, ExtVozvr);
                if idx = 0 then ti := fInfo
                else ti:= fError;
                if ti <> fError then
                  case fTypeF of
                    fPrihod:
                      if ExtVozvr then
                        Show_Inf(Inf_ImportXML[idx] + InfExtVozvr, ti)
                      else
                        Show_Inf(Inf_ImportXML[idx] + InfExtNoVozvr, ti);
                    else
                      Show_Inf(Inf_ImportXML[idx], ti);
                  end
                else
                  Show_Inf(Inf_ImportXML[idx], ti);

              except
                Show_Inf
                  ('������ ������ ������������� ����� �������� ���������.' +
                    #13#10
                    + '�������� ���� XML ��������� ��� ����� ������������ ������', fError);
              end;
            end;
          fXLS:
            begin
              try
                // �������� ����� XLS
                if ImportXLS(fTypeF, fmWork.dlgOpen_Import.FileName, CurSG,
                  fmWork.que2_sql, fmWork.pbWork) then
                  Show_Inf('������ ������� �� Excel �������', fInfo)
                else
                  Show_Inf('������ ������� ������ �� ����� Excel', fError);
              except
                Show_Inf
                  ('������ ������ ������������� ����� �������� ���������.' +
                    #13#10
                    + '�������� ���� XLS ��������� ��� ����� ������������ ������', fError);
              end;
            end;
          fNone:
            exit;
        end;
        Screen.Cursor := crDefault;
      end;
    finally
      CurSG.FilterActive := True;
      CurSG.Col := CurSG.FixedCols;
      CurSG.Row:=CurSG.RowCount-CurSG.FixedFooters - 1;
      CurSG.SetFocus;
    end;
  except
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmMove.btn_LastErrClick(Sender: TObject);
begin
  FindLastErr(CurSG);
end;

procedure TfmMove.btn_PastClick(Sender: TObject);
var
  s: string;
  SLErr: TStringList;
  ColSal, ColProd, ColProducer: array of Integer;
begin
  SetLength(ColSal, 3);
  SetLength(ColProd, 2);
  SetLength(ColProducer, 5);
  SLErr := TStringList.Create;
  try

    case fTypeF of
      fPrihod:
        begin
          ColSal[0] := 6;
          ColSal[1] := 7;
          ColSal[2] := 8;
          ColProd[0] := 9;
          ColProd[1] := 10;
          ColProducer[0] := 14;
          ColProducer[1] := 15;
          ColProducer[2] := 16;
          ColProducer[3] := 1;
          ColProducer[2] := 2;
          SLErr.Assign(SLErrPrih);
        end;
    end;
    s := Clipboard.AsText;
    PastCell(s, CurSG);
    CheckErrSG(fTypeF, CurSG, CurSG.Col, CurSG.RealRowIndex(CurSG.Row), s);

    if not NoValue(CurSG.AllCells[CurSG.AllColCount - 1, CurSG.RealRow]) then
      Inc(countEdit);

    CurSG.Objects[0, CurSG.RealRow] := Pointer(1);

    CheckErrSG(fTypeF, CurSG, CurSG.Col, CurSG.RealRow, s);
    sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
    case fTypeF of
      fPrihod:
        SLErrPrih.Assign(SLErr);
    end;
  finally
    SLErr.Free;
  end;
end;

procedure TfmMove.btn_ReplaceClick(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 1;
  fmReplace.Caption := '��������';
  fmReplace.ClientHeight := 175;
  fmReplace.SGFind := CurSG;
  fmReplace.Show;
end;

procedure TfmMove.btn_SaveClick(Sender: TObject);
var
  i, Row: Integer;
  SLErrRowDecl, SLErr:TStringList;
  PK: string;
  decl: TDeclaration;
begin
  SLErrRowDecl:= TStringList.Create;
  SLErr:=TStringList.Create;
  try
    if fmProgress = nil then
      fmProgress:= TfmProgress.Create(Application);
    fmWork.Enabled := False;
    fmProgress.Show;
    fmProgress.pb.Position := 0;
    fmProgress.lblcap.Caption :=
      '����������� ���������� ������ � ���� ... ';
    fmProgress.pb.UpdateControlState;
    Application.ProcessMessages;
    fmProgress.labAbout.Visible :=True;
    fmProgress.pb.Min := 0;
    i:=CountNew + countEdit + DelDecl.Count;
    if i > 0 then
      fmProgress.pb.Max := i
    else
      fmProgress.pb.Max := 0;

    fmProgress.pb.Step := 1;

    try
      CurSG.FilterActive := False;
      SortSLCol(1,SLAllDecl);
      // ������� ������
      DeleteDataSLBD;
      FillDataGrid(CurSG);
      // ������������ �� ������ � ����� �������� � �����������
      if (CountNew > 0) or (countEdit > 0) then
        for i := CurSG.FixedRows to CurSG.RowCount - CurSG.FixedRows -
          CurSG.FixedFooters do
        begin
          NullDecl(decl);
          case fTypeF of
            fPrihod: decl.TypeDecl:=1;
            fRashod: decl.TypeDecl:=2;
            fVPost: decl.TypeDecl:=3;
            fVPok: decl.TypeDecl :=4;
            fBrak:decl.TypeDecl:=5;
          end;
          Row := CurSG.RealRowIndex(i);
          PK := CurSG.AllCells[CurSG.AllColCount - 1, Row];

          if NoValue(PK) then // ��������� ����� ������
          begin
            if (decl.TypeDecl <> 1) and (decl.TypeDecl <> 3) then
              NullSaler(decl.sal);
            // ��������� ������ � ���������, ���������� � �������������
            RowDecl(fTypeF, decl, Row);
            //��������� ������ �� ������
            if decl.product.EAN13 <> '' then
              begin
                SaveRowNewDeclDB(fmWork.que1_sql, decl, true);

                if decl.PK <> '' then
                  SaveRowDeclNewLocal(decl);
                UpdateOstAM(fmWork.que1_sql, decl);
              end
            else
              begin
                SLErrRowDecl.AddObject(String(decl.DeclNum), TDecl_obj.Create);
                TDecl_Obj(SLErrRowDecl.Objects[SLErrRowDecl.Count-1]).Data := decl;
              end;
            CurSG.AllCells[CurSG.AllColCount - 1, Row] := String(decl.PK);
            CurSG.AllObjects[0, Row] := nil;

            SetColorRow(Row, CurSG);
            fmProgress.pb.StepIt;
          end
          else if LongInt(CurSG.AllObjects[0, Row]) = 1 then // ��������� ������
          begin
            RowDecl(fTypeF, decl, Row);
            CurSG.AllObjects[0, Row] := nil;
            if SaveRowUpdDB(fmWork.que1_sql, decl, true) then
              SaveRowUpdLocal(decl);
            UpdateOstAM(fmWork.que1_sql, decl);
            SetColorRow(Row, CurSG);
            fmProgress.pb.StepIt;
          end;
        end;
    finally
      fmWork.Enabled := True;
      EnabledMainBut(True);
      fmProgress.labAbout.Visible :=False;
      fmProgress.Close;
    end;
    cbAllKvartChange(Application);
    if SLErrRowDecl.Count > 0 then
    begin
      Row:=CurSG.RowCount - CurSG.FixedFooters;
      CurSG.RowCount := CurSG.RowCount + SLErrRowDecl.Count;
      for i := 0 to SLErrRowDecl.Count - 1 do
      begin
        decl := TDecl_Obj(Pointer(SLErrRowDecl.Objects[i])).Data;
        case fTypeF of
          fPrihod, fVPost:
            WriteRowPrihodSG(SLErr, decl, CurSG, i + Row);
          fVPok, fBrak:
            WriteRowBrakSG(SLErr, decl, CurSG, i + Row);
        end;
        SetColorRow(i+row, CurSG);
        CurSG.AutoNumberCol(0);
        ErrGrid(fTypeF, CountNamesSL(SLErr));
        case fTypeF of
          fPrihod: SLErrPrih.Assign(SLErr);
          fVPost: SLErrVPost.Assign(SLErr);
        end;
      end;
    end;

    fmMain.WriteCaptionCount;
    CountNew := 0;
    countEdit := 0;
    sb.Panels.Items[3].Text := statusDel + IntToStr(DelDecl.Count);
    sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
    sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);

    CurSG.FilterActive := True;

    for i := 0 to CurSG.AllRowCount - 1 do
      CurSG.RowEnabled[i]:=True;
  finally
    FreeStringList(SLErrRowDecl);//.Free;
    SLErr.Free;
  end;
end;

procedure TfmMove.ErrGrid(fTF: TTypeForm; cErr: integer);
var
  col: TColor;
begin
  if cErr > 0 then
  begin
    col := clErr;
    btn_FirstErr.Enabled := True;
    btn_LastErr.Enabled := True;
  end
  else
  begin
    col := clNormal;
    btn_FirstErr.Enabled := False;
    btn_LastErr.Enabled := False;
  end;

  case fTF of
    fPrihod:
      begin
        labErrPrih.Font.Color := col;
        if cErr > 0 then
          labErrPrih.Caption := '� ������� ������� ' + IntToStr(cErr)
            + ' ������'
        else
          labErrPrih.Caption := '� ������� ������� ��� ������';
      end;
    fRashod:
      begin
        labErrRash.Font.Color := col;
        if cErr > 0 then
          labErrRash.Caption := '� ������� ������� ' + IntToStr(cErr)
            + ' ������'
        else
          labErrRash.Caption := '� ������� ������� ��� ������'
      end;
    fVPost:
      begin
        labErrVPost.Font.Color := col;
        if cErr > 0 then
          labErrVPost.Caption := '� ������� �������� ����������� ' + IntToStr
            (cErr) + ' ������'
        else
          labErrVPost.Caption := '� ������� �������� ����������� ��� ������'
      end;
    fVPok:
      begin
        lblErrVPok.Font.Color := col;
        if cErr > 0 then
          lblErrVPok.Caption := '� ������� �������� �� ����������� ' + IntToStr
            (cErr) + ' ������'
        else
          lblErrVPok.Caption := '� ������� �������� �� ����������� ��� ������'
      end;
    fBrak:
      begin
        labErrBrak.Font.Color := col;
        if cErr > 0 then
          labErrBrak.Caption := '� ������� ����� ' + IntToStr(cErr) + ' ������'
        else
          labErrBrak.Caption := '� ������� ����� ��� ������'
      end;
  end;
end;

procedure TfmMove.FormCreate(Sender: TObject);
begin
  AssignCBSL(cbAllKvart, SLPerRegYear);
  pgc1.ActivePage := ts_Prih;
  // ��������� ������������ ��������
  cbAllKvart.ItemIndex := SetCurKvart;
  AssignCBSL(cbKPP, SLKPP);

//  dtp_WorkDate.Date := Now();
  dtp_WorkDate.date := GetCurDate(CurKvart);
  SG_Prih.AutoFilterUpdate := True;
  if kppCur = '' then
    kppCur := fOrg.Kpp0;
  if kppCur <> '' then
    cbKPP.ItemIndex := cbKPP.Items.IndexOf(String(kppCur))
  else
  begin
    cbKPP.ItemIndex := 0;
    kppCur := ShortString(cbKPP.Items.Strings[cbKPP.ItemIndex]);
    fOrg.Kpp0 := kppCur;
  end;
  if ExistRegPeriod then
    tlb_Decl.Enabled := True
  else
    tlb_Decl.Enabled := False;
  CurSG := SG_Prih;
  CurRowLabel := lblRowErrPrih;
  // ShapkaAllSG;
//  if ExistRegPeriod then
  ShowDataAll;
  countEdit := 0;
  CountNew := 0;
  sb.Panels.Items[3].Text := statusDel + IntToStr(DelDecl.Count);
  sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
  sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
  CurSG.col := CurSG.FixedCols;
  CurSG.Row := CurSG.FixedRows;
end;

procedure TfmMove.FormShow(Sender: TObject);
begin
  WidthBanelButton;
  if ExistRegPeriod then
    tlb_Decl.Enabled := True
  else
    tlb_Decl.Enabled := False;
  VisibleButton;
end;

procedure TfmMove.mniN11Click(Sender: TObject);
var ACol, ARow:integer;
  s:string;
begin
  ACol:=CurSG.Col;
  ARow:=CurSG.Row;
  s:=CurSG.Cells[ACol,ARow];
  CurSG.FilterActive := False;
  CurSG.Filter.ColumnFilter[ACol].Condition := s;
  CurSG.FilterActive := True;
  CurSG.ApplyFilter;
  CurSG.RemoveLastFilter;
end;

procedure TfmMove.mniN12Click(Sender: TObject);
var ACol:integer;
  s:string;
begin
  ACol:=CurSG.Col;
  s:='';
  CurSG.FilterActive := False;
  CurSG.Filter.ColumnFilter[ACol].Condition :='';
  CurSG.FilterList.Clear;
  CurSG.FilterActive := True;
  CurSG.ApplyFilter;
end;

procedure TfmMove.mniN13Click(Sender: TObject);
begin
  CurSG.FilterActive := False;
  CurSG.Filter.Clear;
  CurSG.ApplyFilter;
end;

procedure TfmMove.mniN14Click(Sender: TObject);
var prod:TProduction;
  fProduser:Boolean;
  RowSG:Integer;
begin
  if fmAllOst = nil then
    fmAllOst := TfmAllOst.Create(Application);
  fmAllOst.ShowModal;
  
  if fmAllOst.ModalResult = mrOk then
  begin
    if (CurSG.Cells[6,CurSG.RealRow] = '') then
      RowSG := CurSG.RealRow
    else
    begin  
      btn_AddRClick(CurSG);
      rowSG := CurSG.RowCount - CurSG.FixedFooters - 1;
    end;
    
    prod.EAN13 := fmAllOst.ostEAN.EAN13;
    GetProduction(fmWork.que2_sql, prod, fProduser, True);
    CurSG.Cells[1,RowSG]:=IntToStr(FormDecl(string(prod.FNSCode)));
    CurSG.Cells[2,RowSG]:=string(prod.FNSCode);
    CurSG.Cells[3,RowSG]:=string(fmAllOst.ostEAN.SelfKpp);
    CurSG.Cells[6,RowSG]:= string(prod.EAN13);
    CurSG.Cells[7,RowSG]:= string(prod.ProductnameF);
    CurSG.Floats[8,RowSG]:= fmAllOst.ostEAN.Amount;
    CurSG.Floats[9,RowSG] := 0.1 * fmAllOst.ostEAN.Amount * prod.capacity;
    CurSG.Cells[11,RowSG]:= string(prod.prod.pInn);
    CurSG.Cells[12,RowSG]:=string(prod.prod.pKpp);
  end;

  SetColorRow(CurSG.RealRow, CurSG);
  sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
end;

procedure TfmMove.N1Click(Sender: TObject);
var pk, PKo:string;
    idx:integer;
begin
  if CurSG.RowEnabled[CurSG.RealRowIndex(RowMouse)] = False then
  begin
    PK := CurSG.AllCells[CurSG.AllColCount - 1, CurSG.RealRowIndex(RowMouse)];
    idx := DelDecl.IndexOf(pk);
    if idx <> -1 then
      DelDecl.Delete(idx);

    SortSLCol(1,SLAllDecl);
    idx:=SLAllDecl.IndexOf(PK);
    if idx <> -1 then
    begin
      PKo := String(TDecl_obj(SLAllDecl.Objects[idx]).data.Ost.PK);
      if not NoValue(PKo) then
      begin
        idx := DelDecl.IndexOf(PKo+'o');
        if idx <> -1 then
          DelDecl.Delete(idx);
      end;
    end;
    CurSG.RowEnabled[CurSG.RealRowIndex(RowMouse)] :=True;
    CurSG.Invalidate;
  end;
end;

procedure TfmMove.N2Click(Sender: TObject);
begin
  btn_CopyClick(Application);
end;

procedure TfmMove.N3Click(Sender: TObject);
begin
  btn_PastClick(Application);
end;

procedure TfmMove.N7Click(Sender: TObject);
var pk, PKo:string;
    idx, i:integer;
begin
  if fmProgress = nil then
    fmProgress := TfmProgress.Create(Application);
  fmProgress.Show;
  fmProgress.lblcap.Caption := '����������� ������ �������� ���� �����';
  fmProgress.pb.Max := CurSG.RowCount - CurSG.FixedRows - CurSG.FixedFooters;
  fmProgress.pb.Step := 1;
  Application.ProcessMessages;
  SortSLCol(1,SLAllDecl);
  for i := CurSG.FixedRows to CurSG.RowCount - CurSG.FixedRows - CurSG.FixedFooters do
  begin
    fmProgress.pb.StepIt;
    fmProgress.pb.UpdateControlState;

    if CurSG.RowEnabled[CurSG.RealRowIndex(i)] = False then
    begin
      PK := CurSG.AllCells[CurSG.AllColCount - 1, CurSG.RealRowIndex(i)];
      idx := DelDecl.IndexOf(pk);
      if idx <> -1 then
        DelDecl.Delete(idx);

//      SortSLCol(1,SLAllDecl);
      idx:=SLAllDecl.IndexOf(PK);
      if idx <> -1 then
      begin
        PKo := String(TDecl_obj(SLAllDecl.Objects[idx]).data.Ost.PK);
        if not NoValue(PKo) then
        begin
          idx := DelDecl.IndexOf(PKo+'o');
          if idx <> -1 then
            DelDecl.Delete(idx);
        end;
      end;
      CurSG.RowEnabled[CurSG.RealRowIndex(i)] :=True;
    end;
    CurSG.Invalidate;
    EnabledMainBut(True);
  end;

  fmProgress.pb.Position := 0;
  fmProgress.pb.UpdateControlState;
  Application.ProcessMessages;
  fmProgress.Close;
end;

procedure TfmMove.N8Click(Sender: TObject);
var decl: TDeclaration;
    NewKPP:string;
begin
  RowDecl(fTypeF, decl, CurSG.RealRow);
  if fmSelKpp = nil then
    fmSelKpp := TfmSelKpp.Create(Application);
  fmSelKpp.fSKPPTypeF:=fKppCorrectEan;
  fmSelKpp.ShowModal;
  if fmSelKpp.ModalResult = mrOk then
    begin
      NewKpp := SLKpp.Strings[fmSelKpp.Tag];
      if Correct_ChangeKPP(decl,NewKPP, fmWork.que1_sql) then
        begin
          CurSG.FilterActive := False;
          cbAllKvartChange(Application);
          CurSG.FilterActive := True;
          Show_inf('��� �������� �������',fInfo);
        end
      else
        Show_inf('��� �������� �������',fError);
    end;

end;

procedure TfmMove.N_RefClick(Sender: TObject);
begin
    if fmFiltrRef = nil then
      fmFiltrRef.Create(Application);
    fmFiltrRef.ShowModal;
    SetColorRow(CurSG.RealRow, CurSG);
    sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
end;

procedure TfmMove.pgc1Change(Sender: TObject);
var b:Boolean;
begin
  curSheet := (Sender as TPageControl).ActivePage;
  ChangeMainForm;
  case (Sender as TPageControl).ActivePageIndex of
    0:
      begin
        fTypeF := fPrihod;
        CurSG := SG_Prih;
        CurLabel := labErrPrih;
        CurRowLabel := lblRowErrPrih;
        CurSL.Assign(SLDataPrih);
        SortSLCol(1, CurSL, CompNumAsc);
        if SLErrPrih.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    1:
      begin
        fTypeF := fRashod;
        CurSG := SG_Rash;
        CurLabel := labErrRash;
        CurSL.Assign(SLDataRash);
        SortSLCol(1,CurSL, CompNumAsc);
        if SLErrRash.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    2:
      begin
        fTypeF := fVPost;
        CurSG := SG_VPost;
        CurSL.Assign(SLDataVPost);
        SortSLCol(1,CurSL, CompNumAsc);
        if SLErrVPost.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    3:
      begin
        fTypeF := fVPok;
        CurSG := SG_VPok;
        CurSL.Assign(SLDataVPok);
        SortSLCol(1,CurSL, CompNumAsc);
        if SLErrVPok.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    4:
      begin
        fTypeF := fBrak;
        CurSG := SG_Brak;
        CurSL.Assign(SLDataBrak);
        SortSLCol(1,CurSL, CompNumAsc);
        if SLErrBrak.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    5:
      begin
        fTypeF := fMove;
        CurSG := SG_Moving;
      end;
  end;
  case fTypeF of
    fPrihod:
      ErrGrid(fTypeF, CountNamesSL(SLErrPrih));
    fRashod:
      ErrGrid(fTypeF, CountNamesSL(SLErrRash));
    fVPost:
      ErrGrid(fTypeF, CountNamesSL(SLErrVPost));
    fVPok:
      ErrGrid(fTypeF, CountNamesSL(SLErrVPok));
    fBrak:
      ErrGrid(fTypeF, CountNamesSL(SLErrBrak));
  end;
  SetPropertyForm;
  FillDataGrid(CurSG);
  CurSG.Col := CurSG.FixedCols;
  CurSG.Row := CurSG.FixedRows;
  CurSG.SelectCells(CurSG.Col,CurSG.Row,CurSG.Col,CurSG.Row);
  SG_SelectCell(CurSG,CurSG.Col,CurSG.Row,b);
end;

procedure TfmMove.pgc1Changing(Sender: TObject; var AllowChange: Boolean);
begin
  ChangeMainForm;
end;

procedure TfmMove.pmRefPopup(Sender: TObject);
var ext:Boolean;
begin
  ext:=False;
  case fTypeF of
    fPrihod, fVPost:
      begin
        if IndexMas(FilterColPrih, CurSG.Col) <> -1 then
          ext := True;
        mniN14.Enabled := False;
        N_Ref.Enabled := True;
      end;
    fVPok, fBrak:
      begin
        if IndexMas(FilterColBrak, CurSG.Col) <> -1 then
          ext := True;
        mniN14.Enabled := False;
        N_Ref.Enabled := True;
      end;
    fRashod:
      begin
        if IndexMas(FilterColRashNew, CurSG.Col) <> -1 then
          ext := True;
        mniN14.Enabled := True;
        N_Ref.Enabled := False;
      end;

    fMove:
      begin
        if IndexMas(FilterColMoving, CurSG.Col) <> -1 then
          ext := True;
        mniN14.Enabled := False;
        N_Ref.Enabled := True;
      end;
  end;
  if ext then
  begin
    mniN11.Enabled := True;
    mniN12.Enabled := True;
  end
  else
  begin
    mniN11.Enabled := False;
    mniN12.Enabled := False;
  end;
end;

procedure TfmMove.ShowDataGrid;
begin
  if fmDataExchange = nil then
  begin
    fmDataExchange := TfmDataExchange.Create(Application);
    fmDataExchange.SG_Query.ClearNormalCells;
    fmDataExchange.SG_Query.RowCount := fmDataExchange.SG_Query.FixedRows +
      fmDataExchange.SG_Query.FixedFooters;
  end;
  case fTypeF of
    fPrihod:
      begin
        CurSL.Assign(SLDataPrih);
        ErrGrid(fPrihod, ShowDataMove(SLErrPrih, SG_Prih, SLDataPrih,
            SLOborotKpp, fTypeF, NoResColPrih, CalcColPrih, HideColPrih));
      end;
    fVPost:
      begin
        CurSL.Assign(SLDataVPost);
        ErrGrid(fVPost, ShowDataMove(SLErrVPost, SG_VPost, SLDataVPost,
            SLOborotKpp, fTypeF, NoResColPrih, CalcColPrih, HideColPrih));
      end;
    fRashod:
      begin
        CurSL.Assign(SLDataRash);
        ErrGrid(fRashod, ShowDataMove(SLErrRash, SG_Rash, SLDataRash,
            SLOborotKpp, fTypeF, NoResColRashNew, CalcColRashNew, HideColRashNew));
      end;
    fVPok:
      begin
        CurSL.Assign(SLDataVPok);
        ErrGrid(fVPok, ShowDataMove(SLErrVPok, SG_VPok, SLDataVPok, SLDataRash,
            fTypeF, NoResColBrak, CalcColBrak, HideColBrak));
      end;
    fBrak:
      begin
        CurSL.Assign(SLDataBrak);
        ErrGrid(fVPok, ShowDataMove(SLErrBrak, SG_Brak, SLDataBrak, SLDataRash,
            fTypeF, NoResColBrak, CalcColBrak, HideColBrak));
      end;
  end;
  FillDataGrid(CurSG);
end;

procedure TfmMove.ShowDataAll;
begin
  ChangeKvart(DBeg, dEnd);
  if fmDataExchange = nil then
    fmDataExchange := TfmDataExchange.Create(Application);
  fmDataExchange.SG_Query.ClearNormalCells;
  fmDataExchange.SG_Query.RowCount := fmDataExchange.SG_Query.FixedRows +
    fmDataExchange.SG_Query.FixedFooters;
  case fTypeF of
    fPrihod:
      CurSL.Assign(SLDataPrih);
    fRashod:
      CurSL.Assign(SLDataRash);
    fVPost:
      CurSL.Assign(SLDataVPost);
    fVPok:
      CurSL.Assign(SLDataVPok);
    fBrak:
      CurSL.Assign(SLDataBrak);
  end;
  CurSG.FilterActive := False;
  ErrGrid(fPrihod, ShowDataMove(SLErrPrih, SG_Prih, SLDataPrih, SLOborotKpp,
      fPrihod, NoResColPrih, CalcColPrih, HideColPrih));
  ErrGrid(fRashod, ShowDataMove(SLErrRash, SG_Rash, SLDataRash, SLOborotKpp,
      fRashod, NoResColRashNew, CalcColRashNew, HideColRashNew));
  ErrGrid(fVPost, ShowDataMove(SLErrVPost, SG_VPost, SLDataVPost, SLOborotKpp,
      fVPost, NoResColPrih, CalcColPrih, HideColPrih));
  ErrGrid(fVPok, ShowDataMove(SLErrVPok, SG_VPok, SLDataVPok, SLOborotKpp,
      fVPok, NoResColBrak, CalcColBrak, HideColBrak));
  ErrGrid(fBrak, ShowDataMove(SLErrBrak, SG_Brak, SLDataBrak, SLOborotKpp,
      fBrak, NoResColBrak, CalcColBrak, HideColBrak));
  // if SLKPP.Count > 0 then
  // ShowDataMoving(SG_Moving, SLDataMoving, NoResColMoving, CalcColMoving,
  // HideColMoving);
  FillDataGrid(CurSG);
  CurSG.FilterActive := True;
end;

procedure TfmMove.SetPropertyForm;
begin
  pnl_Decl.parent := curSheet;
  VisibleButton;
end;

procedure TfmMove.VisibleButton;
begin
  EnableAllButton(True);
  UndoBtn.Enabled := False;
  RedoBtn.Enabled := False;

  case fTypeF of
    fPrihod:
      begin
        if SLErrPrih.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    fRashod:
      begin
        btn_Replace.Enabled := True;
        btn_DupR.Enabled := False;
        btn_Past.Enabled := False;
        if SLErrRash.Count > 0 then
        begin
          btn_FirstErr.Enabled := True;
          btn_LastErr.Enabled := True;
        end
        else
        begin
          btn_FirstErr.Enabled := False;
          btn_LastErr.Enabled := False;
        end;
      end;
    fVPok, fBrak:
      begin
        btn_Import.Enabled := False;
        btn_DupR.Enabled := False;
      end;
    fMove:
      begin
        btn_Import.Enabled := False;
        btn_Replace.Enabled := False;
        btn_DupR.Enabled := False;
        btn_Cut.Enabled := False;
        btn_Past.Enabled := False;
        btn_FirstErr.Enabled := False;
        btn_LastErr.Enabled := False;
        RedoBtn.Enabled := False;
        UndoBtn.Enabled := False;
      end;
  end;
  if fTypeF = fRashod then
  begin
    btAutoRash.Visible := True;
    pOder.Visible := True;
    sep4.Visible := True;
    btnOst.Visible := True;
  end
  else
  begin
    btAutoRash.Visible := False;
    pOder.Visible := False;
    sep4.Visible := False;
    btnOst.Visible := False;
  end;
  if cbAllKvart.ItemIndex = 0 then
  begin
    btn_Save.Enabled := False;
    btn_Import.Enabled := False;
    btn_Replace.Enabled := False;
    btn_Cut.Enabled := False;
    btn_Past.Enabled := False;
    btn_AddR.Enabled := False;
    btn_DelR.Enabled := False;
    btn_DelAll.Enabled := False;
    btn_DupR.Enabled := False;
    btn_FirstErr.Enabled := False;
    btn_LastErr.Enabled := False;
    btAutoRash.Enabled := False;
    btnOst.Enabled := False;
    if CurSG.RowCount > CurSG.FixedRows + CurSG.FixedFooters then
    begin
      btn_Copy.Enabled := True;
      btn_Find.Enabled := True;
      btn_Export.Enabled := True;
    end
    else
    begin
      btn_Copy.Enabled := False;
      btn_Find.Enabled := False;
      btn_Export.Enabled := False;
    end;
  end;
end;

procedure TfmMove.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmMove.cbAllKvartChange(Sender: TObject);
var
  sKvart: string;
begin
  DelDecl.Clear;

  if (Sender is TComboBox) then
    if (Sender as TComboBox).ItemIndex <> -1 then
    begin
      sKvart := (Sender as TComboBox).Items.Strings[(Sender as TComboBox)
        .ItemIndex];
      idxKvart := (Sender as TComboBox).ItemIndex;
    end;
  if idxKvart <> -1 then
  begin
    cbAllKvart.ItemIndex := idxKvart;
    if fmMain <> nil then
    begin
      fmMain.cbAllKvart.ItemIndex := idxKvart;
      fmMain.LabelReg;
    end;
    if fmCorrect <> nil then
    begin
      fmCorrect.cbbAllKvart.ItemIndex := idxKvart;
    end;
  end;
  CurSG.FilterActive := False;
  cbKvartChange(sKvart);
  ShowDataAll;
  VisibleButton;
  CurSG.FilterActive := True;
  countEdit := 0;
  CountNew := 0;
  DelDecl.Clear;
  sb.Panels.Items[3].Text := statusDel + IntToStr(DelDecl.Count);
  sb.Panels.Items[4].Text := statusAdd + IntToStr(CountNew);
  sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
  case fTypeF of
    fPrihod:
      ErrGrid(fTypeF, CountNamesSL(SLErrPrih));
    fRashod:
      ErrGrid(fTypeF, CountNamesSL(SLErrRash));
    fVPost:
      ErrGrid(fTypeF, CountNamesSL(SLErrVPost));
    fVPok:
      ErrGrid(fTypeF, CountNamesSL(SLErrVPok));
    fBrak:
      ErrGrid(fTypeF, CountNamesSL(SLErrBrak));
  end;
//  if fmMove.Visible then
//    CurSG.SetFocus;
  if ExistRegPeriod then
  begin
    fmWork.sb_Decl.Enabled := True;
    fmWork.l_Decl.Enabled := True;
    fmMove.tlb_Decl.Enabled := True;
  end
  else
  begin
    fmWork.sb_Decl.Enabled := False;
    fmWork.l_Decl.Enabled := False;
    fmMove.tlb_Decl.Enabled := False;
  end;
  if fmMain <> nil then
    begin
      fmMain.WriteCaptionCount;
      EnabledMainBut(True);
    end;
end;

procedure TfmMove.cbKPPChange(Sender: TObject);
var
  fn: string;
  tIniF: TIniFile;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    if (Sender as TComboBox).ItemIndex <> -1 then
    begin
      kppCur := ShortString((Sender as TComboBox).Items.Strings
        [(Sender as TComboBox).ItemIndex]);
      tIniF.WriteString(SectionName, 'KPP', String(kppCur));
    end;
  finally
    tIniF.Free;
  end;
end;

procedure TfmMove.EnableAllButton(val: Boolean);
var
  i: integer;
begin
  for i := 0 to tlb_Decl.ButtonCount - 1 do
    tlb_Decl.Buttons[i].Enabled := val;
end;

procedure TfmMove.WidthBanelButton;
begin
  pFile.Width := btn_Save.Width + btn_Import.Width + btn_Export.Width + 3;
  pFind.Width := btn_Find.Width + btn_Replace.Width + 8;
  pEdit.Width := btn_Copy.Width + btn_Past.Width + 8;
  pRow.Width := btn_AddR.Width + btn_DelR.Width + btn_DupR.Width +
    btn_DelAll.Width + 8;
  pErr.Width := btn_FirstErr.Width + btn_LastErr.Width + 8;
  pPravka.Width := UndoBtn.Width + RedoBtn.Width + 8;
  pOder.Width := btAutoRash.Width + btnOst.Width + 8;
end;

procedure TfmMove.CheckErrSG(fTF: TTypeForm; SG: TAdvStringGrid;
  ACol, ARow: Integer; var Value: string);
var
  decl: TDeclaration;
  p1, p2, idx, OldOBJ, NewObj: Integer;
  s: string;
  cap: Extended;
  FindLic, FindProduced: Boolean;
  SLErr: TStringList;
begin
  OldOBJ := 0;
  SLErr := TStringList.Create;
  NullDecl(decl);
  try
    case fTypeF of
      fVpok, fBrak:
        begin
          case fTypeF of
            fVpok: SLErr.Assign(SLErrVPok);
            fBrak: SLErr.Assign(SLErrBrak);
          end;
          decl.pk :=ShortString(SG.AllCells[SG.AllColCount - 1, ARow]);
          decl.SelfKpp := ShortString(SG.AllCells[3, ARow]);
          decl.product.EAN13 := ShortString(SG.AllCells[6, ARow]);
          decl.product.ProductNameF := ShortString(SG.AllCells[7, ARow]);
          decl.product.prod.pInn := ShortString(SG.AllCells[12, ARow]);
          decl.product.prod.pKpp := ShortString(SG.AllCells[13, ARow]);
          decl.product.prod.pName := ShortString(SG.AllCells[11, ARow]);
          decl.DeclNum := ShortString(SG.AllCells[4, ARow]);
          decl.Amount := SG.AllFloats[8, ARow];
          decl.Vol := SG.AllFloats[9, ARow];
          SetColorRow(ARow, SG);
          case ACol of
            3: // SelfKpp
              begin
                idx := CheckSelfKpp(Value);
                decl.SelfKpp := ShortString(Value);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
              end;
            5: // Data
              begin
                idx := CheckDate(StrToDateTime(Value));
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
                decl.DeclDate := StrToDate(SG.AllCells[ACol, ARow]);
              end;
            6: // EAN13
              begin
                decl.product.EAN13 := ShortString(Value);
                GetProduction(fmWork.que2_sql, decl.product, FindProduced,
                  True);
                SG.AllCells[7, ARow] := String(decl.product.ProductNameF);
                SG.AllCells[11, ARow] := String(decl.product.prod.pName);
                SG.AllCells[12, ARow] := String(decl.product.prod.pInn);
                SG.AllCells[13, ARow] := String(decl.product.prod.pKpp);
                SG.AllCells[2, ARow] := String(decl.product.FNSCode);
                if (decl.product.prod.pName = NoProd) then
                  decl.product.FNSCode := '0';
                SG.AllCells[1, ARow] := IntToStr
                  (FormDecl(String(decl.product.FNSCode)));
                decl.Vol := 0.1 * decl.Amount * decl.product.Capacity;
                SG.AllCells[9, ARow] := FloatToStrF(decl.Vol, ffFixed,
                  maxint, ZnFld);
                idx := CheckProduction(decl.product);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [6,7], ARow, idx);
                idx := CheckProducer(decl.product.prod);
                SetColorCells(SG, [11, 12, 13], ARow, idx);
              end;
            8: // ������
              begin
                if Value <> '' then
                  decl.Amount := StrToFloat(Value)
                else
                  decl.Amount := 0;
                if decl.product.EAN13 = '' then
                begin
                  decl.Amount := 0;
                  decl.Vol := 0;
                  SG.AllCells[8, ARow] := FloatToStrF(StrToFloat(Value),
                    ffFixed, maxint, ZnFld);
                  SG.AllCells[9, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [8, 9], ARow,
                    CheckChislo(True, StrToFloat(Value)));
                end
                else
                begin
                  p1 := Pos('����� ', String(decl.product.ProductNameF));
                  p2 := Length(decl.product.ProductNameF) - 2;
                  s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                  if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and
                    (s <> '') then
                    cap := StrToFloat(s)
                  else
                    cap := 0;
                  decl.Vol := 0.1 * cap * decl.Amount;
                  Value := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
                  SG.AllCells[9, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SG.AllCells[ACol, ARow] := Value;
                  decl.Amount := StrToFloat(SG.AllCells[ACol, ARow]);
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [8, 9], ARow,
                    CheckChislo(True, StrToFloat(Value)));
                end;
              end;
            9: // ������ � ����������
              begin
                if Value <> '' then
                  decl.Vol := StrToFloat(Value)
                else
                  decl.Vol := 0;
                p1 := Pos('����� ', String(decl.product.ProductNameF));
                p2 := Length(String(decl.product.ProductNameF)) - 2;
                s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                if (decl.product.ProductNameF <> NoProd) and (s <> '') then
                  cap := StrToFloat(s)
                else
                  cap := 0;
                decl.Amount := 10 * decl.Vol / cap;
                Value := FloatToStrF(decl.Vol, ffFixed, maxint, ZnFld);
                SG.AllCells[8, ARow] := FloatToStrF(decl.Amount, ffFixed,
                  maxint, ZnFld);
                SG.AllCells[ACol, ARow] := Value;
                SetColorRow(ARow, SG);
                SetColorCells(SG, [8, 9], ARow,
                  CheckChislo(True, decl.Vol));
              end;
          end;
        end;
      fPrihod, fVPost:
        begin
          case fTypeF of
            fPrihod : SLErr.Assign(SLErrPrih);
            fVPost: SLErr.Assign(SLErrVPost);
          end;
          decl.PK := ShortString(SG.AllCells[SG.AllColCount - 1, ARow]);
          decl.SelfKpp := ShortString(SG.AllCells[3, ARow]);
          decl.product.EAN13 := ShortString(SG.AllCells[9, ARow]);
          decl.product.ProductNameF := ShortString(SG.AllCells[10, ARow]);
          decl.product.prod.pInn := ShortString(SG.AllCells[15, ARow]);
          decl.product.prod.pKpp := ShortString(SG.AllCells[16, ARow]);
          decl.product.prod.pName := ShortString(SG.AllCells[14, ARow]);
          decl.sal.sInn := ShortString(SG.AllCells[6, ARow]);
          decl.sal.sKpp := ShortString(SG.AllCells[7, ARow]);
          decl.sal.sName := ShortString(SG.AllCells[8, ARow]);
          decl.DeclNum := ShortString(SG.AllCells[47, ARow]);
          decl.Amount := SG.AllFloats[11, ARow];
          decl.Vol := SG.AllFloats[12, ARow];
          SetColorRow(ARow, SG);
          case ACol of
            3: // SelfKpp
              begin
                idx := CheckSelfKpp(Value);
                decl.SelfKpp := ShortString(Value);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
              end;
            4: // ����� ����������
              begin
                if fTypeF = fPrihod then
                  begin
                    idx := CheckValueCells(Value);
                    decl.DeclNum := ShortString(Value);
                    SetColorRow(ARow, SG);
                    SetColorCells(SG, [ACol], ARow, idx);
                  end;
              end;
            5: // Data
              begin
                idx := CheckDate(StrToDateTime(Value));
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
                decl.DeclDate := StrToDate(SG.AllCells[ACol, ARow]);
              end;
            6: // Saler inn
              begin
                decl.sal.sInn := ShortString(Value);
                decl.sal.sKpp := ShortString(SG.AllCells[7, ARow]);
                decl.sal.sName := '';
                GetSaler(fmWork.que2_sql, decl.sal, FindLic, True,False);
                SG.AllCells[8, ARow] := String(decl.sal.sName);

                idx := CheckSaler(decl.sal);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [8,6, 7], ARow, idx);
              end;
            7: // Saler kpp
              begin
                decl.sal.sInn := ShortString(SG.AllCells[6, ARow]);
                decl.sal.sKpp := ShortString(Value);
                decl.sal.sName := '';
                GetSaler(fmWork.que2_sql, decl.sal, FindLic, True,False);
                SG.AllCells[8, ARow] := String(decl.sal.sName);
                idx := CheckSaler(decl.sal);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [6, 7,8], ARow, idx);
              end;
            8: // Saler
              begin
                SetColorRow(ARow, SG);
                SetColorCells(SG, [6, 7,8], ARow, idx);
              end;
            9: // Production
              begin
                decl.product.EAN13 := ShortString(Value);
                GetProduction(fmWork.que2_sql, decl.product, FindProduced,
                  True);
                SG.AllCells[10, ARow] := String(decl.product.ProductNameF);
                SG.AllCells[14, ARow] := String(decl.product.prod.pName);
                SG.AllCells[15, ARow] := String(decl.product.prod.pInn);
                SG.AllCells[16, ARow] := String(decl.product.prod.pKpp);
                SG.AllCells[2, ARow] := String(decl.product.FNSCode);
                if (decl.product.prod.pName = NoProd) then
                  decl.product.FNSCode := '0';
                SG.AllCells[1, ARow] := IntToStr
                  (FormDecl(String(decl.product.FNSCode)));

                decl.Vol := 0.1 * decl.Amount * decl.product.Capacity;
                SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
                  maxint, ZnFld);
                idx := CheckProduction(decl.product);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [9,10], ARow, idx);

                idx := CheckProducer(decl.product.prod);
                SetColorCells(SG, [14, 15, 16], ARow, idx);
              end;
            10: // Production
              begin
                SetColorRow(ARow, SG);
                SetColorCells(SG, [9,10], ARow, idx);
              end;
            11: // ������
              begin
                if Value <> '' then
                  decl.Amount := StrToFloat(Value)
                else
                  decl.Amount := 0;
                if decl.product.EAN13 = '' then
                begin
                  decl.Amount := 0;
                  decl.Vol := 0;
                  SG.AllCells[11, ARow] := FloatToStrF(StrToFloat(Value),
                    ffFixed, maxint, ZnFld);
                  SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [11, 12], ARow,
                    CheckChislo(True, StrToFloat(Value)));
                end
                else
                begin
                  p1 := Pos('����� ', String(decl.product.ProductNameF));
                  p2 := Length(decl.product.ProductNameF) - 2;
                  s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                  if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and
                    (s <> '') then
                    cap := StrToFloat(s)
                  else
                    cap := 0;
                  decl.Vol := 0.1 * cap * decl.Amount;
                  Value := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
                  SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SG.AllCells[ACol, ARow] := Value;
                  decl.Amount := SG.AllFloats[ACol, ARow];
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [11, 12], ARow,
                    CheckChislo(True, StrToFloat(Value)));
                end;
              end;
            12: // ������ � ����������
              begin
                if Value <> '' then
                  decl.Vol := StrToFloat(Value)
                else
                  decl.Vol:=0;
                p1 := Pos('����� ', String(decl.product.ProductNameF));
                p2 := Length(decl.product.ProductNameF) - 2;
                s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                if (decl.product.ProductNameF <> NoProd) and (s <> '') then
                  cap := StrToFloat(s)
                else
                  cap := 0;
                decl.Amount := 10 * decl.Vol / cap;
                Value := FloatToStrF(decl.Vol, ffFixed, maxint, ZnFld);
                SG.AllCells[11, ARow] := FloatToStrF(decl.Amount, ffFixed,
                  maxint, ZnFld);
                SG.AllCells[ACol, ARow] := Value;
                SetColorRow(ARow, SG);
                SetColorCells(SG, [11, 12], ARow,
                  CheckChislo(True, decl.Vol));
              end;
          end;
        end;
      fRashod:
        begin
          SLErr.Assign(SLErrRash);
          decl.PK := ShortString(SG.AllCells[SG.AllColCount - 1, ARow]);
          decl.SelfKpp := ShortString(SG.AllCells[3, ARow]);
          decl.product.EAN13 := ShortString(SG.AllCells[6, ARow]);
          decl.product.ProductNameF := ShortString(SG.AllCells[7, ARow]);
          decl.product.prod.pInn := ShortString(SG.AllCells[11, ARow]);
          decl.product.prod.pKpp := ShortString(SG.AllCells[12, ARow]);
          decl.product.prod.pName := ShortString(SG.AllCells[10, ARow]);
          decl.Amount := SG.AllFloats[8, ARow];
          decl.Vol := SG.AllFloats[9, ARow];
          SetColorRow(ARow, SG);
          case ACol of
            3: // SelfKpp
              begin
                idx := CheckSelfKpp(Value);
                decl.SelfKpp := ShortString(Value);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
              end;
            4: // Data
              begin
                idx := CheckDate(StrToDateTime(Value + ' ' + SG.AllCells[5, ARow]));
                SetColorRow(ARow, SG);
                SetColorCells(SG, [ACol], ARow, idx);
                SG.Times[ACol+1, ARow] := Time;
                decl.DeclDate := StrToDateTime(SG.AllCells[ACol, ARow] + ' ' + SG.AllCells[5, ARow]);
              end;
            6: // Production
              begin
                decl.product.EAN13 := ShortString(Value);
                GetProduction(fmWork.que2_sql, decl.product, FindProduced,
                  True);
                SG.AllCells[7, ARow] := String(decl.product.ProductNameF);
                SG.AllCells[10, ARow] := String(decl.product.prod.pName);
                SG.AllCells[11, ARow] := String(decl.product.prod.pInn);
                SG.AllCells[12, ARow] := String(decl.product.prod.pKpp);
                SG.AllCells[1, ARow] := IntToStr(decl.TypeDecl);
                SG.AllCells[2, ARow] := String(decl.product.FNSCode);
                if (decl.product.prod.pName = NoProd) then
                  decl.product.FNSCode := '0';
                SG.AllCells[1, ARow] := IntToStr
                  (FormDecl(String(decl.product.FNSCode)));

//                decl.Vol := 0.1 * decl.Amount * decl.product.Capacity;
//                SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
//                  maxint, ZnFl);
                idx := CheckProduction(decl.product);
                SetColorRow(ARow, SG);
                SetColorCells(SG, [6,7], ARow, idx);

                idx := CheckProducer(decl.product.prod);
                SetColorCells(SG, [10, 11, 12], ARow, idx);
              end;
            7: // Production
              begin
                SetColorRow(ARow, SG);
                SetColorCells(SG, [6,7], ARow, idx);
              end;
            8: // ������
              begin
                if Value <> '' then
                  decl.Amount := StrToFloat(Value)
                else
                  decl.Amount := 0;
                if decl.product.EAN13 = '' then
                begin
                  decl.Amount := 0;
                  decl.Vol := 0;
                  SG.AllCells[8, ARow] := FloatToStrF(StrToFloat(Value),
                    ffFixed, maxint, ZnFld);
                  SG.AllCells[9, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [8,9], ARow, CheckChislo(True, StrToFloat(Value)));
                end
                else
                begin
                  p1 := Pos('����� ', String(decl.product.ProductNameF));
                  p2 := Length(decl.product.ProductNameF) - 2;
                  s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                  if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and
                    (s <> '') then
                    cap := StrToFloat(s)
                  else
                    cap := 0;
                  decl.Vol := 0.1 * cap * decl.Amount;
                  Value := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
                  SG.AllCells[9, ARow] := FloatToStrF(decl.Vol, ffFixed,
                    maxint, ZnFld);
                  SG.AllCells[ACol, ARow] := Value;
                  decl.Amount := SG.AllFloats[ACol, ARow];
                  SetColorRow(ARow, SG);
//                  SetColorCells(SG, [8,9], ARow,
//                    CheckChislo(True, StrToFloat(Value)));
                end;
              end;
            9:  //�����
              begin
                if Value <> '' then
                  decl.Vol := StrToFloat(Value)
                else
                  decl.Vol := 0;
                if decl.product.EAN13 = '' then
                begin
                  decl.Amount := 0;
                  decl.Vol := 0;
                  SG.AllCells[8, ARow] := FloatToStrF(decl.Amount,
                    ffFixed, maxint, ZnFld);
                  SG.AllCells[9, ARow] := FloatToStrF(StrToFloat(Value), ffFixed,
                    maxint, ZnFld);
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [8,9], ARow, CheckChislo(True, StrToFloat(Value)));
                end
                else
                begin
                  p1 := Pos('����� ', String(decl.product.ProductNameF));
                  p2 := Length(decl.product.ProductNameF) - 2;
                  s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
                  if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and
                    (s <> '') then
                    cap := StrToFloat(s)
                  else
                    cap := 0;
                  decl.Amount := 10  * decl.Vol / cap;
                  Value := FloatToStrF(decl.Vol, ffFixed, maxint, ZnFld);
                  SG.AllCells[8, ARow] := FloatToStrF(decl.Amount, ffFixed,
                    maxint, ZnFld);
                  SG.AllCells[ACol, ARow] := Value;
                  decl.Vol := SG.AllFloats[ACol, ARow];
                  SetColorRow(ARow, SG);
                  SetColorCells(SG, [8,9], ARow, CheckChislo(True, StrToFloat(Value)));
                end;
              end;
          end;




//          decl.PK := ShortString(SG.AllCells[SG.AllColCount - 1, ARow]);
//          decl.product.EAN13 := ShortString(SG.AllCells[4, ARow]);
//          decl.product.ProductNameF := ShortString(SG.AllCells[5, ARow]);
//          decl.product.prod.pInn := ShortString(SG.AllCells[14, ARow]);
//          decl.product.prod.pKpp := ShortString(SG.AllCells[15, ARow]);
//          decl.product.prod.pName := ShortString(SG.AllCells[13, ARow]);
//
//          decl.Amount := SG.AllFloats[9, ARow];
//          decl.Ost.Value := SG.AllFloats[11, ARow];
//          idx := GetIdxOstDate(String(decl.SelfKPP+decl.product.EAN13),dEnd, SLDataOst);
//          if idx = -1 then
//            decl.Ost.PK := ''
//          else
//            decl.Ost.PK :=TOst_obj(SLDataOst.Objects[idx]).data.PK;
//
//          decl.Vol := SG.AllFloats[12, ARow];
//
//          dv.ost0 := SG.AllFloats[6, ARow];
//          dv.prih := SG.AllFloats[7, ARow];
//          dv.oder := SG.AllFloats[8, ARow];
//          dv.rash := SG.AllFloats[9, ARow];
//          dv.ost1 := SG.AllFloats[10, ARow];
//          case ACol of
//            2: // SelfKpp
//              begin
//                idx := CheckSelfKpp(Value);
//                decl.SelfKpp := ShortString(Value);
//                SetColorRow(ARow, SG);
//                SetColorCells(SG, [ACol], ARow, idx);
//              end;
//            3: // Data
//              begin
//                idx := CheckDate(StrToDateTime(Value));
//                SetColorRow(ARow, SG);
//                SetColorCells(SG, [ACol], ARow, idx);
//                decl.DeclDate := StrToDate(SG.AllCells[ACol, ARow]);
//              end;
//            9: //������
//              begin
//                Value := FloatToStrF(StrToFloat(Value), ffFixed, maxint, ZnFl);
//                SG.AllCells[ACol, ARow] := Value;
//                try
//                  dv.rash := StrToFloat(Value);
//                except
//                  dv.rash := 0;
//                end;
//                dv.ost1 := dv.ost0 + dv.prih + dv.oder - dv.rash;
//                if dv.rash > dv.ost0 + dv.prih + dv.oder + dv.ost1 then
//                begin
//                  dv.rash := dv.ost0 + dv.prih + dv.oder;
//                  dv.ost1 := 0;
//                  Value := FloatToStrF(dv.rash, ffFixed, maxint, ZnFl);
//                end;
//                if dv.rash <= 0 then
//                begin
//                  if dv.prih < 1 then
//                    dv.rash := dv.ost0 + dv.prih + dv.oder
//                  else
//                    dv.rash := 1;
//                  dv.ost1 := dv.ost0 + dv.prih + dv.oder - dv.rash;
//                end;
//
//                p1 := Pos('����� ', String(decl.product.ProductNameF));
//                p2 := Length(decl.product.ProductNameF) - 2;
//                s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
//                if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and (s <> '')
//                  then
//                  cap := StrToFloat(s)
//                else
//                  cap := 0;
//
//                decl.Vol := 0.1 * cap * dv.rash;
//
//                Value := FloatToStrF(dv.rash, ffFixed, maxint, ZnFl);
//                SG.AllCells[10, ARow] := FloatToStrF(dv.ost1, ffFixed,
//                  maxint, ZnFl);
//                SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
//                  maxint, ZnFl);
//                SG.AllCells[13, ARow] := FloatToStrF(0.1 * cap * dv.ost1,
//                  ffFixed, maxint, ZnFl);
//
//                SetColorRow(ARow, SG);
//                SetColorCells(SG, [9, 10, 12,13], ARow,
//                  CheckChislo(False, dv.ost1));
//                SetColorCells(SG, [9, 10, 12, 13], ARow,
//                  CheckChislo(True, dv.rash));
//              end;
//            10:  //�����
//              begin
//                p1 := Pos('����� ', String(decl.product.ProductNameF));
//                p2 := Length(decl.product.ProductNameF) - 2;
//                s := Copy(String(decl.product.ProductNameF), p1 + 6, p2 - p1 - 5);
//                if (Pos(NoProd, String(decl.product.ProductNameF)) = 0) and (s <> '')
//                  then
//                  cap := StrToFloat(s)
//                else
//                  cap := 0;
//
//                Value := FloatToStrF(dv.ost1, ffFixed, maxint, ZnFl);
//                SG.AllCells[ACol, ARow] := Value;
//                try
//                  dv.ost1 := StrToFloat(Value);
//                except
//                  dv.ost1 := 0;
//                end;
//                dv.rash := dv.ost0 + dv.prih + dv.oder - dv.ost1;
//                if dv.ost1 >= dv.ost0 + dv.prih + dv.oder + dv.rash then
//                begin
//                  dv.rash := 1;
//                  dv.ost1 := dv.ost0 + dv.prih + dv.oder - 1;
//                  Value := FloatToStrF(dv.ost1, ffFixed, maxint, ZnFl);
//                end;
//
//                decl.Vol := 0.1 * cap * dv.rash;
//
//                SG.AllCells[9, ARow] := FloatToStrF(dv.rash, ffFixed, maxint,
//                  ZnFl);
//                SG.AllCells[12, ARow] := FloatToStrF(decl.Vol, ffFixed,
//                  maxint, ZnFl);
//                SG.AllCells[13, ARow] := FloatToStrF(0.1 * cap * dv.ost1,
//                  ffFixed, maxint, ZnFl);
//
//                SetColorRow(ARow, SG);
//                SetColorCells(SG, [9, 10], ARow, CheckChislo(False, dv.ost1));
//                SetColorCells(SG, [9, 10], ARow, CheckChislo(True, dv.rash));
//              end;
//            11:
//              begin
//                Value := FloatToStrF(StrToFloat(Value), ffFixed, maxint, ZnFl);
//                if StrToFloat(Value) > dv.ost1 then
//                  Value := FloatToStrF(dv.ost1, ffFixed, maxint, ZnFl);
//                SetColorRow(ARow, SG);
//                if StrToFloat(Value) <> 0  then
//                begin
//                  e1 := StrToFloat(Value);
//                  e2 := StrToFloat(FloatToStrF(dv.ost1, ffFixed, maxint, ZnFl));
//                  e := e2-e1;
//                  if (e < 0) then
//                    SetColorCells(SG, [11], ARow, 1)
//                  else
//                    SetColorCells(SG, [11], ARow, 4)
//                end
//                else SetColorCells(SG, [11], ARow, -1)
//              end;
//          end;
        end;
    end;
    CountErrSG(fTF, SLErr, SG, ARow);
    ErrGrid(fTypeF, CountNamesSL(SLErr));
    NewObj := LongInt(SG.AllObjects[0, ARow]);
    if NewObj <> OldOBJ then
    begin
      if (OldOBJ = 0) and (NewObj = 1) then
        Inc(countEdit);
      if (OldOBJ = 1) and (NewObj = 0) then
      begin
        if NoValue(String(decl.PK)) then
          Inc(countEdit)
        else
          Dec(countEdit);
      end;
      sb.Panels.Items[5].Text := statusEdit + IntToStr(countEdit);
    end;
    case fTypeF of
      fPrihod: SLErrPrih.Assign(SLErr);
      fRashod: SLErrRash.Assign(SLErr);
      fVPost: SLErrVPost.Assign(SLErr);
      fVPok: SLErrVPok.Assign(SLErr);
      fBrak: SLErrBrak.Assign(SLErr);
    end;
  finally
    SLErr.Free;
  end;
end;

procedure TfmMove.SG_MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CurSG.PopupMenu := pmRef; (Sender as TAdvStringGrid)
  .MouseToCell(X, Y, colMouse, rowMouse);
  case fTypeF of
    fPrihod,fVPost:
      case colMouse of
        6, 7, 8:
          begin
            N_Ref.Enabled := True;
            fmFiltrRef.nbFiltr.PageIndex := 1;
          end;
        9, 10:
          begin
            N_Ref.Enabled := True;
            fmFiltrRef.nbFiltr.PageIndex := 0;
          end;
      else
        N_Ref.Enabled := False;
      end;
    fVPok, fBrak:
      case colMouse of
        6, 7:
          begin
            N_Ref.Enabled := True;
            fmFiltrRef.nbFiltr.PageIndex := 0;
          end;
      else
        N_Ref.Enabled := False;
      end;
  end;
end;

procedure TfmMove.SG_PrihFilterShow(Sender: TObject; ACol: Integer;
  var Allow: Boolean);
begin
//  CurSG.Filter.ColumnFilter[ACol].BuildCondition(s/);
end;

procedure TfmMove.SG_CanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
var
  idx: Integer;
  Coledit: Array of Integer;
begin
  case fTypeF of
    fPrihod, fVPost:
      begin
        SetLength(Coledit, High(EditColPrih) + 1);
        Move(EditColPrih[0], Coledit[0], sizeOf(Integer) * Length(EditColPrih));
      end;
    fRashod:
      begin
        SetLength(Coledit, High(EditColRashNew) + 1);
        Move(EditColRashNew[0], Coledit[0], sizeOf(Integer) * Length(EditColRashNew));
      end;
    fBrak, fVPok:
      begin
        SetLength(Coledit, High(EditColBrak) + 1);
        Move(EditColBrak[0], Coledit[0], sizeOf(Integer) * Length(EditColBrak));
      end;
  end;

  idx := IndexMas(Coledit, ACol);

  if idx <> -1 then
    CanEdit := True
  else
    CanEdit := False;

end;

procedure TfmMove.SG_CellValidate(Sender: TObject; ACol, ARow: Integer;
  var Value: string; var Valid: Boolean);
begin
  CheckErrSG(fTypeF, (Sender as TAdvStringGrid), ACol, CurSG.RealRowIndex(ARow), Value);
  ErrCells(ACol, ARow);
  if LongInt((Sender as TAdvStringGrid).Objects[0,ARow]) = 1 then
    EnabledMainBut(False)
  else
    EnabledMainBut(True);
end;

procedure TfmMove.SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
  FriendlyName: string; var FilterCondition: string);
begin
  case fTypeF of
    fPrihod, fVPost:
      if IndexMas(FilterColPrih, Column) = -1 then
        exit;
    fVPok, fBrak:
      if IndexMas(FilterColBrak, Column) = -1 then
        exit;
    fRashod:
      if IndexMas(FilterColRashNew, Column) = -1 then
        exit;
    fMove:
      if IndexMas(FilterColMoving, Column) = -1 then
        exit;
  end;
  if FilterCondition = '��������' then
  begin
    FilterCondition := '';
//    CurSG.Filter.ColumnFilter[Column].c
    CurSG.RemoveLastFilter;
  end;
  if FilterCondition = '������' then
  begin
    FilterCondition := '<0';
  end;
  if FilterCondition = '����' then
  begin
    FilterCondition := '=0,0000';
  end;
//  CurSG.Filter.ColumnFilter[Column].BuildCondition(FilterCondition);
//  CurSG.ApplyFilter;
end;

procedure TfmMove.SG_GetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
var
  ColFilter: Array of Integer;
  SL: TStringList;
//  ColDate: Integer;
begin
  SL := TStringList.Create;
  try
    if (Sender as TAdvStringGrid).RowCount = (Sender as TAdvStringGrid)
      .FixedRows + (Sender as TAdvStringGrid).FixedFooters then
    begin
      exit;
    end;

    case fTypeF of
      fPrihod, fVPost:
        begin
          SetLength(ColFilter, High(FilterColPrih) + 1);
          Move(FilterColPrih[0], ColFilter[0],
            sizeOf(Integer) * Length(FilterColPrih));
        end;
      fVPok, fBrak:
        begin
          SetLength(ColFilter, High(FilterColBrak) + 1);
          Move(FilterColBrak[0], ColFilter[0],
            sizeOf(Integer) * Length(FilterColBrak));
        end;
      fRashod:
        begin
          case Column of
            1,2,3,4,5,6,7,10,11,12:
              begin
                SetLength(ColFilter, High(FilterColRashNew) + 1);
                Move(FilterColRashNew[0], ColFilter[0],
                  sizeOf(Integer) * Length(FilterColRashNew));
              end;
            8,9:
              begin
                Filter.Clear;
                Filter.Add('��������');
                Filter.Add('������');
                exit;
              end;
          end;
        end;
      fMove:
        begin
          SetLength(ColFilter, High(FilterColMoving) + 1);
          Move(FilterColMoving[0], ColFilter[0],
            sizeOf(Integer) * Length(FilterColMoving));
        end;
    end;

    if IndexMas(ColFilter, Column) = -1 then
      Filter.Clear;

  finally
    SL.Free;
  end;
end;

procedure TfmMove.SG_GetEditorProp(Sender: TObject; ACol, ARow: Integer;
  AEditLink: TEditLink);
begin
  (Sender as TAdvStringGrid).ClearComboString; (Sender as TAdvStringGrid).Combobox.Items.Assign(SLKPP);
  (Sender as TAdvStringGrid).Combobox.DropDownCount := SLKPP.Count;
end;

procedure TfmMove.SG_GetEditorType(Sender: TObject; ACol, ARow: Integer;
  var AEditor: TEditorType);
begin
  case fTypeF of
    fBrak, fVPok:
      case ACol of
        1, 2, 12, 13:
          AEditor := edNumeric;
        5:
          AEditor := edDateEdit;
        4,10:
          AEditor := edNormal;
        3:
          AEditor := edComboList;
        8, 9:
          AEditor := edFloat;
      end;
    fPrihod, fVPost:
      case ACol of
        6, 7, 9, 14, 15:
          AEditor := edNumeric;
        5:
          AEditor := edDateEdit;
        4:
          AEditor := edNormal;
        3:
          AEditor := edComboList;
        11, 12:
          AEditor := edFloat;
      end;
    fRashod:
      case ACol of
        3:
          AEditor := edComboList;
        4:
          AEditor := edDateEdit;
        5:
          AEditor := edTimeEdit;
        6:
          AEditor := edNormal;
        8,9:
          AEditor := edFloat;
      end;
  end;
end;

procedure TfmMove.SG_KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Shift = [ssCtrl] then
  begin
    case Key of
      Ord('C'):
        btn_CopyClick(Application);
      Ord('V'):
        btn_PastClick(Application);
      Ord('Y'):
        btn_DelRClick(Application); // �������� ������
      VK_END:
        CurSG.Row := CurSG.RowCount - CurSG.FixedFooters - 1;
    end;
  end;
end;

procedure TfmMove.SG_SelectCell(Sender: TObject; ACol, ARow: Integer;
    var CanSelect: Boolean);
var
  PK, EAN13, KPP: string;
  i, Col, Row, idx:Integer;
  decl:TDeclaration;
  SL_decl:TStringList;
  mov:TMove;
  r, Prih, Rash, Ost0, Ost1: Extended;
begin
  sb.Panels.Items[1].Text := '������ ' + IntToStr(ARow) + ' �� ' + IntToStr(CurSG.RowCount - CurSG.FixedRows - CurSG.FixedFooters);
  PK := CurSG.Cells[CurSG.AllColCount - 1, ARow];
  if NoValue(PK) then
    sb.Panels.Items[2].Text := '����� ������ ����������'
  else
    sb.Panels.Items[2].Text := 'ID = ' + PK;
  ErrCells(ACol, ARow);
  if fTypeF = fRashod then
  begin
    SG_Dvizh.Invalidate;
    SG_Dvizh.ClearNormalCells;
    SG_Dvizh.RowCount := SG_Dvizh.FixedRows + SG_Dvizh.FixedFooters + 1;
    SG_1.Invalidate;
    SG_1.ClearNormalCells;

    if not GB_1.Visible then
      GB_1.Visible:=True;
    EAN13 := CurSG.Cells[6, ARow];
    KPP := CurSG.Cells[3, ARow];
    GB_1.Caption := 'EAN ' + EAN13 + ' ��� ' + KPP;
  end;
  try

    SL_decl := TStringList.Create;
    for i := 0 to SLAllDecl.Count-1 do
    begin
      decl:=TDecl_obj(SLAllDecl.Objects[i]).data;
      if (decl.product.EAN13 = ShortString(EAN13)) and (decl.SelfKpp = ShortString(KPP)) then
        if DBetween(Int(decl.RepDate), dBeg, dEnd) then
        begin
          SL_decl.AddObject(DateTimeToStr(decl.DeclDate), TDecl_Obj.Create);
          TDecl_Obj(SL_decl.Objects[SL_decl.Count - 1]).data := decl;
        end;
    end;

    //���������� ������� �� �������� ���������
    if SL_decl.Count > 0 then
    begin
      SortSLCol(7, SL_decl);
      SG_Dvizh.RowCount := SG_Dvizh.FixedRows +  SG_Dvizh.FixedFooters + SL_decl.Count;

      for i := 0 to SL_decl.Count - 1 do
      begin
        decl := TDecl_obj(SL_decl.Objects[i]).data;
        Col := decl.TypeDecl + 1;
        Row:= i + SG_Dvizh.FixedRows;
        SG_Dvizh.Cells[1,Row] := SL_decl.Strings[i];
        SG_Dvizh.Floats[Col,Row] := decl.Amount;
      end;
      SG_Dvizh.AutoNumberCol(0);

      SG_Dvizh.ColumnSize.Rows := arNormal;
      SG_Dvizh.NoDefaultDraw := True;
      SG_Dvizh.AutoSize := True;

      for i := 2  to 8 do
      begin
        r := SG_Dvizh.ColumnSum(i,SG_Dvizh.FixedRows,SG_Dvizh.RowCount - SG_Dvizh.FixedFooters - 1);
        SG_Dvizh.Floats[i,SG_Dvizh.RowCount - 1] := r;
      end;

      Prih := 0; Rash := 0;
      idx := SLOborotKpp.IndexOf(Kpp+Ean13);
      if idx >= 0 then

      begin
        mov := TMove(SLOborotKpp.Objects[idx]);
        Ost0 := 0; Ost1 := 0;
        if (mov.product.EAN13 = ShortString(EAN13)) and
           (mov.SelfKpp = ShortString(KPP)) then
        begin
          Ost0 := mov.dv.ost0;
          Prih := mov.dv.prih + mov.dv.move_in + mov.dv.vozv_pokup + mov.dv.move_in;
          Rash := mov.dv.rash + mov.dv.vozv_post + mov.dv.brak + mov.dv.move_out + mov.dv.move_out;
          Ost1 := mov.dv.ost1;
        end;
        SG_1.Floats[0,1] := Ost0;
        SG_1.Floats[1,1] := Prih;
        SG_1.Floats[2,1] := Rash;
        SG_1.Floats[3,1] := Ost1;

        SG_1.ColumnSize.Rows := arFixed;
        SG_1.NoDefaultDraw := True;
        SG_1.AutoSize := True;
      end;
    end;
  finally
    FreeStringList(SL_decl);
  end;
end;


end.
