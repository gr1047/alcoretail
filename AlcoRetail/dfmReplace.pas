unit dfmReplace;

interface

uses
  Forms, StdCtrls, Controls, ExtCtrls, Classes, AdvGrid, Types, SysUtils;

type
  TfmReplace = class(TForm)
    Notebook1: TNotebook;
    Panel1: TPanel;
    leFind: TLabeledEdit;
    Button2: TButton;
    Button3: TButton;
    Panel2: TPanel;
    Label2: TLabel;
    leStart: TLabeledEdit;
    leFinish: TLabeledEdit;
    ColumnCheck: TRadioButton;
    TableCheck: TRadioButton;
    Button5: TButton;
    Button6: TButton;
    Button8: TButton;
    procedure Button8Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SGFind: TADVStringGrid;
  end;

var
  fmReplace: TfmReplace;

implementation

uses Global, GlobalUtils, dfmMove, GlobalConst;
{$R *.dfm}

procedure TfmReplace.Button2Click(Sender: TObject);
var
  Loc: TPoint;
  Fp: TFindParams;
  txt: string;
begin
  txt := '*' + leFind.Text + '*';
  Loc := Point(SGFind.Col, SGFind.RealRow); // �������� ����� � ������� �������
  Fp := [fnMatchRegular, fnDirectionLeftRight];

  Loc := SGFind.Find(Loc, txt, Fp);
  if not((Loc.X = -1) or (Loc.Y = -1)) then
  begin
     SGFind.col := SGFind.DisplColIndex(Loc.X);
     SGFind.Row := Loc.Y;
  end
  else
  begin
    Loc := Point(0, 1);
    Loc := SGFind.Find(Loc, txt, Fp);
    if not((Loc.X = -1) or (Loc.Y = -1)) then
    begin
       SGFind.col := Loc.X;
       SGFind.Row := Loc.Y;
    end
  end;
end;

procedure TfmReplace.Button3Click(Sender: TObject);
begin
  Close
end;

procedure TfmReplace.Button5Click(Sender: TObject);
var
  Loc: TPoint;
  Fp: TFindParams;
  Warning, txt, s: string;
  row:integer;
begin
  Loc := Point(SGFind.Col - 1, SGFind.RealRow); // �������� ����� � ������� �������
  if ColumnCheck.Checked then
  begin
    Loc := Point(SGFind.Col, SGFind.RealRow);
    Fp := [fnMatchRegular, fnDirectionLeftRight, fnIncludeFixed,
      fnFindInCurrentCol];
    Warning := '��������� ������ � ������� �� �������';
  end;
  if TableCheck.Checked then
  begin
    Loc := Point(SGFind.Col - 1, SGFind.RealRow);
    Fp := [fnMatchRegular, fnDirectionLeftRight, fnIncludeFixed];
    Warning := '��������� ������ � ������� �� �������'
  end;
  Screen.Cursor := crHourGlass;
  try
    try
      fmMove.Enabled := false;
      fmReplace.Enabled := false;
      if Length(leStart.Text) > 0 then // ���� ������ ��� ������ �� ������
        if (leStart.Text = leFinish.Text) then
        // ���� ������ ��� � �� ��� ���������
        begin
          fmReplace.Visible := false;
          Show_Inf('�������� � �������� ������ ���������!' + #13#10 +
              '������ ��������� �� �����', fWarning);
          fmReplace.Visible := true;
        end
        else
        begin
          if Length(leStart.Text) > 0 then // ���� ������ ��� ������ �� ������
          begin
            txt := '*' + leStart.Text + '*';
            Loc := SGFind.Find(Loc, txt, Fp);
            if not((Loc.X = -1) or (Loc.Y = -1)) then
            begin
              //��������� ������
              //s:=Replace(SGFind.Cells[Loc.X, Loc.Y], leStart.Text, leFinish.Text);
              //SGFind.Cells[Loc.X, Loc.Y] := s;


              row := SGFind.RealRowIndex(Loc.y);
              s:=Replace(SGFind.AllCells[Loc.X, row], leStart.Text, leFinish.Text);
              SGFind.AllCells[Loc.X, row] := s;
              //SGFind.AllObjects[0,row] := Pointer(1);
              //�������� ������ �� ������
              fmMove.CheckErrSG(fTypeF, SGFind,Loc.X, row,s);


              //�������� ������ �� ������
              //fmMove.CheckErrSG(fTypeF, CurSG,Loc.X, Loc.Y,s);

              fmMove.sb.Panels.Items[5].Text := statusEdit + IntToStr(fmMove.CountEdit);;
              Loc := SGFind.Find(Loc, txt, Fp);
              if (Loc.X <> -1) and (Loc.Y <> -1) then
              begin
                SGFind.Col := Loc.X;
                SGFind.Row := Loc.Y;
              end
              else
              begin
                Loc.X := SGFind.Col;
                Loc.Y := 0;
                Loc := SGFind.Find(Loc, txt, Fp);
                if (Loc.X <> -1) and (Loc.Y <> -1) then
                begin
                  SGFind.Col := Loc.X;
                  SGFind.Row := Loc.Y;
                end;
                if ((Loc.X = -1) or (Loc.Y = -1)) then
                  Show_Inf(Warning, fWarning);
              end;
            end
            else
            begin
              Loc.X := SGFind.Col;
              Loc.Y := 0;
              Loc := SGFind.Find(Loc, txt, Fp);
              if (Loc.X <> -1) and (Loc.Y <> -1) then
              begin
                SGFind.Col := Loc.X;
                SGFind.Row := Loc.Y;
              end;
              if ((Loc.X = -1) or (Loc.Y = -1)) then
                Show_Inf(Warning, fWarning);
            end;
            try
              if fmReplace.Visible and fmReplace.Enabled then
                fmReplace.SetFocus;
            except
            end;
          end;
        end;
    finally
      Screen.Cursor := crDefault;
      fmMove.Enabled := true;
      fmReplace.Enabled := true;
    end;
  except
  end;
end;

procedure TfmReplace.Button6Click(Sender: TObject);
var
  Loc: TPoint;
  Fp: TFindParams;
  Warning, txt, s: string;
  c, row: integer;
begin
  c := 0;
  Loc := Point(SGFind.Col - 1, 0); // �������� ����� � ������� �������
  if ColumnCheck.Checked then
  begin
    Loc := Point(SGFind.Col, SGFind.Row);
    Fp := [fnMatchRegular, fnDirectionLeftRight, fnIncludeFixed,
      fnFindInCurrentCol];
   if (Loc.X = -1) or (Loc.Y = -1) then
     Warning := '��������� ������ � ������� �� �������';
  end;
  if TableCheck.Checked then
  begin
    Loc := Point(SGFind.Col - 1, SGFind.Row);
    Fp := [fnMatchRegular, fnDirectionLeftRight, fnIncludeFixed];
    if (Loc.X = -1) or (Loc.Y = -1) then
    Warning := '��������� ������ � ������� �� �������'
  end;
  Screen.Cursor := crHourGlass;
  try
    try
      fmMove.Enabled := false;
      fmReplace.Enabled := false;
      if Length(leStart.Text) > 0 then // ���� ������ ��� ������ �� ������
        if (leStart.Text = leFinish.Text) then
        // ���� ������ ��� � �� ��� ���������
        begin
          fmReplace.Visible := false;
          Show_Inf('�������� � �������� ������ ���������!' + #13#10 +
              '������ ��������� �� �����', fWarning);
          fmReplace.Visible := true;
        end
        else
        begin
          if Length(leStart.Text) > 0 then // ���� ������ ��� ������ �� ������
          begin
            repeat
              //txt := '*' + leStart.Text + '*';
              txt := leStart.Text;
              Loc := SGFind.Find(Loc, txt, Fp);
              if not((Loc.X = -1) or (Loc.Y = -1)) then
              begin
                row := Loc.y;
                s:=Replace(SGFind.Cells[Loc.X, row], leStart.Text, leFinish.Text);
                SGFind.Cells[Loc.X, row] := s;
                //SGFind.AllObjects[0,row] := Pointer(1);
                //�������� ������ �� ������
                fmMove.CheckErrSG(fTypeF, SGFind,Loc.X, SGFind.RealRowIndex(row),s);
                Inc(c);
              end;
            until (Loc.X = -1) or (Loc.Y = -1);

            if c = 0 then
            begin
              Show_Inf(Warning, fWarning);
              exit;
            end;

            Loc.X := SGFind.Col;
            Loc.Y := 0;
            repeat
              //txt := '*' + leStart.Text + '*';
              txt := leStart.Text;
              Loc := SGFind.Find(Loc, txt, Fp);
              if not((Loc.X = -1) or (Loc.Y = -1)) then
              begin
                row := Loc.y;
                s:=Replace(SGFind.Cells[Loc.X, row], leStart.Text, leFinish.Text);
                SGFind.Cells[Loc.X, row] := s;
                //SGFind.AllObjects[0,row] := Pointer(1);
                //�������� ������ �� ������
                fmMove.CheckErrSG(fTypeF, SGFind,Loc.X, SGFind.RealRowIndex(row),s);
                Inc(c);
              end;
            until (Loc.X = -1) or (Loc.Y = -1);

            if ((Loc.X = -1) or (Loc.Y = -1)) then
              begin
                Show_Inf('����������� ' + IntToStr(c) + ' �����', fWarning);
                fmMove.countEdit := fmMove.countEdit + c ;
              end;
          end
          else
            Show_Inf(Warning, fWarning);
        end;
    finally
      Screen.Cursor := crDefault;
      fmMove.Enabled := true;
      fmReplace.Enabled := true;
    end;
  except
  end;
end;

procedure TfmReplace.Button8Click(Sender: TObject);
begin
  Close;
end;

end.
