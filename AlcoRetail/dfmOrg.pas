unit dfmOrg;

interface

uses
  Forms, JvComponentBase, JvPrvwRender, Registry, IniFiles, ShlObj, ComCtrls,
  Mask, StdCtrls, Buttons, Controls, Classes, ExtCtrls, SysUtils, Graphics,
  Windows;

type
  TfmOrg = class(TForm)
    pAbout: TPanel;
    Label6: TLabel;
    Button1: TButton;
    pMain: TPanel;
    GroupBox1: TGroupBox;
    pAdr: TPanel;
    pName: TPanel;
    eNameOrg: TEdit;
    eAdress: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    pTelEmail: TPanel;
    pEmail: TPanel;
    Label1: TLabel;
    eEmail: TEdit;
    pTel: TPanel;
    Label4: TLabel;
    pPeople: TPanel;
    GroupBox2: TGroupBox;
    gbRuk: TGroupBox;
    pKpp: TPanel;
    GroupBox4: TGroupBox;
    pLic: TPanel;
    GroupBox5: TGroupBox;
    pORuk: TPanel;
    Label5: TLabel;
    eORuk: TEdit;
    pIRuk: TPanel;
    Label7: TLabel;
    eIRuk: TEdit;
    pFRuk: TPanel;
    Label8: TLabel;
    eFRuk: TEdit;
    pOBuh: TPanel;
    Label9: TLabel;
    eOBuh: TEdit;
    pIBuh: TPanel;
    Label10: TLabel;
    eIBuh: TEdit;
    pFBuh: TPanel;
    Label11: TLabel;
    eFBuh: TEdit;
    pLicAll: TPanel;
    plDEnd: TPanel;
    plDBeg: TPanel;
    plNum: TPanel;
    plSer: TPanel;
    eLicNum: TEdit;
    Label12: TLabel;
    eLicSer: TEdit;
    Label13: TLabel;
    dtpLicDBeg: TDateTimePicker;
    dtpLicDEnd: TDateTimePicker;
    Label14: TLabel;
    Label15: TLabel;
    sbKPP: TScrollBox;
    JvPreviewRenderGraphics1: TJvPreviewRenderGraphics;
    SpeedButton1: TSpeedButton;
    eTel: TMaskEdit;
    butReft: TButton;
    btn1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure pPeopleResize(Sender: TObject);
    procedure pTelEmailResize(Sender: TObject);
    procedure pMainResize(Sender: TObject);
    procedure LabeledEditClick(Sender: TObject);
    procedure eNameOrgExit(Sender: TObject);
    procedure eAdressExit(Sender: TObject);
    procedure eTelExit(Sender: TObject);
    procedure eEmailExit(Sender: TObject);
    procedure eFRukExit(Sender: TObject);
    procedure eIRukExit(Sender: TObject);
    procedure eORukExit(Sender: TObject);
    procedure eFBuhExit(Sender: TObject);
    procedure eIBuhExit(Sender: TObject);
    procedure eOBuhExit(Sender: TObject);
    procedure eLicSerExit(Sender: TObject);
    procedure eLicNumExit(Sender: TObject);
    procedure dtpLicDBegExit(Sender: TObject);
    procedure dtpLicDEndExit(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure eTelChange(Sender: TObject);
    procedure butReftClick(Sender: TObject);
    procedure LabeledNameEditExit(Sender: TObject);

  private
    { Private declarations }
    procedure Set_DataKPP;
    procedure Set_DataOrg;
  public
    { Public declarations }
  end;

var
  fmOrg: TfmOrg;

implementation

uses dfmWork, dfmMain, Global, dfmEditAdres, GlobalConst, GlobalUtils;
{$R *.dfm}

procedure TfmOrg.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmOrg.butReftClick(Sender: TObject);
begin
  pMainResize(pMain);
  //���������� ����� �������
  Set_DataOrg;
  //���������� ������� ���
  Set_DataKPP;
end;

procedure TfmOrg.dtpLicDBegExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Lic.BegLic := (Sender as TDateTimePicker).Date;
    tIniF.WriteDate(SectionName,'BegLic', fOrg.Lic.BegLic);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.dtpLicDEndExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Lic.EndLic := (Sender as TDateTimePicker).Date;
    tIniF.WriteDate(SectionName,'EndLic', fOrg.Lic.EndLic);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eAdressExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Address := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'AddressOrg', fOrg.Address);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eEmailExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Email := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'Address', fOrg.Email);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eFBuhExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Buh.BuhF := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'BuhF',fOrg.Buh.BuhF);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eFRukExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Ruk.RukF := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'RukF',fOrg.Ruk.RukF);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eIBuhExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Buh.BuhI := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'BuhI',fOrg.Buh.BuhI);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eIRukExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Ruk.RukI := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'RukI',fOrg.Ruk.RukI);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eLicNumExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Lic.NumLic := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'NumLic', fOrg.Lic.NumLic);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eLicSerExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Lic.SerLic := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'SerLic', fOrg.Lic.SerLic);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eNameOrgExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.SelfName := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'SelfName',fOrg.SelfName);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eOBuhExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Buh.BuhO := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'BuhO',fOrg.Buh.BuhO);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eORukExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    fOrg.Ruk.RukO := (Sender as TEdit).Text;
    tIniF.WriteString(SectionName,'RukO',fOrg.Ruk.RukO);
    ColorEdit('',Sender as TEdit);
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.eTelChange(Sender: TObject);
begin
  fOrg.NPhone := (Sender as TMaskEdit).Text
end;

procedure TfmOrg.eTelExit(Sender: TObject);
var fn:string;
    tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    try
      fOrg.NPhone := (Sender as TMaskEdit).Text;
      tIniF.WriteString(SectionName,'NPhone',fOrg.NPhone);
      ColorEditMask('_', Sender as TMaskEdit);

    except
    end;
  finally
    tIniF.Free;
  end;
end;

procedure TfmOrg.Set_DataOrg;
begin
  eNameOrg.Text := fOrg.SelfName;
  eAdress.Text := fOrg.Address;
  eTel.Text := fOrg.NPhone;
  eEmail.Text := fOrg.Email;
  eFRuk.Text := fOrg.Ruk.RukF;
  eIRuk.Text := fOrg.Ruk.RukI;
  eORuk.Text := fOrg.Ruk.RukO;
  eFBuh.Text := fOrg.Buh.BuhF;
  eIBuh.Text := fOrg.Buh.BuhI;
  eOBuh.Text := fOrg.Buh.BuhO;
  eLicSer.Text := fOrg.Lic.SerLic;
  eLicNum.Text := fOrg.Lic.NumLic;
  dtpLicDBeg.Date := fOrg.Lic.BegLic;
  dtpLicDEnd.Date := fOrg.Lic.EndLic;

  ColorEdit('',eNameOrg);
  ColorEdit('',eAdress);
//  ColorEditMask('(___)___-__-__',eTel);
  ColorEditMask('_',eTel);
  ColorEdit('',eEmail);
  ColorEdit('',eFRuk);
  ColorEdit('',eIRuk);
  ColorEdit('',eORuk);
  ColorEdit('',eFBuh);
  ColorEdit('',eIBuh);
  ColorEdit('',eOBuh);
  ColorEdit('',eLicSer);
  ColorEdit('',eLicNum);
end;

procedure TfmOrg.LabeledEditClick(Sender: TObject);
var idx,i:integer;
    p:TPanel;
    n:string;
    dfm:TfmEditAdres;
begin
  idx:=((Sender as TLabeledEdit).Parent as TPanel).Tag;
  p:= (Sender as TLabeledEdit).Parent as TPanel;

  for i := 0 to p.ControlCount-1 do
    if p.Controls[i] is TLabeledEdit then
      begin
        n:=(p.Controls[i] as TLabeledEdit).Name;
        if n = 'le3_0' then
          (p.Controls[i] as TLabeledEdit).Text:=aKpp[idx].KPP;
        if n = '' then
          (p.Controls[i] as TLabeledEdit).Text:=aKpp[idx].namename;
        if n = 'le2_0' then
          (p.Controls[i] as TLabeledEdit).Text :=AdressKpp(aKpp[idx]); //dfm.leFullAdr.Text;
      end;

  dfm:=TfmEditAdres.Create(Self);
  if aKpp[idx].Countrycode <> '' then
    dfm.leCountrycode.Text := aKpp[idx].Countrycode
  else
    dfm.leCountrycode.Text :='643';
  dfm.leIndexP.Text := aKpp[idx].IndexP;
  if aKpp[idx].Codereg <> '' then
    dfm.leCodereg.Text := aKpp[idx].Codereg
  else
    dfm.leCodereg.Text := '59';
  dfm.leRaipo.Text := aKpp[idx].Raipo;
  dfm.leCity.Text := aKpp[idx].City;
  dfm.leNasPunkt.Text:=aKpp[idx].NasPunkt;
  dfm.leStreet.Text := aKpp[idx].Street;
  dfm.leHouse.Text:=aKpp[idx].House;
  dfm.leStruct.Text:=aKpp[idx].Struct;
  dfm.leABC.Text:=aKpp[idx].ABC;
  dfm.leQuart.Text:=aKpp[idx].Quart;
  if dfm.ShowModal = mrOk then
    begin
      aKpp[idx].Countrycode := dfm.leCountrycode.Text;
      aKpp[idx].IndexP := dfm.leIndexP.Text;
      aKpp[idx].Codereg := dfm.leCodereg.Text;
      aKpp[idx].Raipo := dfm.leRaipo.Text;
      aKpp[idx].City := dfm.leCity.Text;
      aKpp[idx].NasPunkt := dfm.leNasPunkt.Text;
      aKpp[idx].Street := dfm.leStreet.Text;
      aKpp[idx].House := dfm.leHouse.Text;
      aKpp[idx].Struct := dfm.leStruct.Text;
      aKpp[idx].ABC := dfm.leABC.Text;
      aKpp[idx].Quart := dfm.leQuart.Text;
    end;

  for i := 0 to p.ControlCount-1 do
    if p.Controls[i] is TLabeledEdit then
      begin
        n:=(p.Controls[i] as TLabeledEdit).Name;
        if Pos('le1',n)>0 then
          aKpp[idx].KPP:=(p.Controls[i] as TLabeledEdit).Text;
        if Pos('le3',n)>0 then
          aKpp[idx].namename:=(p.Controls[i] as TLabeledEdit).Text;
        if Pos('le2',n)>0 then
          (p.Controls[i] as TLabeledEdit).Text :=AdressKpp(aKpp[idx]);
      end;

  dfm.Free;
  UpdateKpp(aKpp[idx], fmWork.que1_sql);
end;

procedure TfmOrg.pMainResize(Sender: TObject);
begin
  pPeopleResize(pPeople);
  pTelEmailResize(pTelEmail);
end;

procedure TfmOrg.Set_DataKPP;
var i:integer;
    p:TPanel;
    le1, le2,le3:TLabeledEdit;
begin
  for i := sbKPP.ControlCount-1 downto 0 do
    sbKPP.Controls[i].Destroy;


  if SLKpp.Count > 0 then
    begin
      for i := 0 to SLKpp.Count - 1 do
        begin
          p:=TPanel.Create(sbKPP);
          p.Parent := sbKPP;
          p.Height := 60;
          p.Align := alTop;
          p.Name := 'KPP_'+IntToStr(i);
          p.Caption:='';
          p.Tag :=i;

          le1:=TLabeledEdit.Create(p);
          le1.Parent := p;
          le1.Left :=50;
          le1.Top:=5;
          le1.Width:=92;
          le1.LabelPosition := lpLeft;
          le1.Font.Size :=10;
          le1.Font.Style:=[];
          le1.Name:='le1_'+IntToStr(i);
          le1.EditLabel.Caption := '���';
          le1.EditLabel.Font.Size:=11;
          le1.EditLabel.Font.Style := [];
          le1.ReadOnly:=True;
          le1.Text:=SLKpp.Strings[i];

          le2:=TLabeledEdit.Create(p);
          le2.Parent := p;
          le2.Left :=50;
          le2.Top:=33;
          le2.Width:=951;
          le2.LabelPosition := lpLeft;
          le2.Font.Size :=10;
          le2.Font.Style:=[];
          le2.Name:='le2_'+IntToStr(i);
          le2.EditLabel.Caption := '�����';
          le2.EditLabel.Font.Size:=11;
          le2.EditLabel.Font.Style := [];
          //le2.Text:='�����';
          le2.OnClick := LabeledEditClick;
          le2.Text:=AdressKpp(aKpp[i]);
          if (aKpp[i].Countrycode = '') or (aKpp[i].IndexP =  '')  then
            le2.Color := clCheck
          else
            le2.Color := clWindow;
          le2.OnExit := LabeledNameEditExit;

          le3:=TLabeledEdit.Create(p);
          le3.Parent := p;
          le3.Left :=250;
          le3.Top:=5;
          le3.Width:=752;
          le3.LabelPosition := lpLeft;
          le3.Font.Size :=10;
          le3.Font.Style:=[];
          le3.Name:='le3_'+IntToStr(i);
          le3.EditLabel.Caption := '������������';
          le3.EditLabel.Font.Size:=11;
          le3.EditLabel.Font.Style := [];
          //le3.Text:='������������';
          le3.Text:=aKpp[i].namename;
          le3.OnExit := LabeledNameEditExit;
          if (aKpp[i].namename = '') then
            le3.Color := clCheck
          else
            le3.Color := clWindow;
        end;
    end;
end;

procedure TfmOrg.SpeedButton1Click(Sender: TObject);
var fn, Key:string;
    tIniF : TIniFile;
    r:Boolean;
    i:integer;
    Reg:TRegistry;
    buf1: PByte;
    ms1:TMemoryStream;
begin
  r:=fmWork.LoadReg;
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  reg := TRegistry.Create;
  if r then
    begin
      Set_DataOrg;
      try
        tIniF.WriteString(SectionName,'SelfName',fOrg.SelfName);
        tIniF.WriteString(SectionName,'AddressOrg', fOrg.Address);
        tIniF.WriteString(SectionName,'NPhone',fOrg.NPhone);
        tIniF.WriteString(SectionName,'Address', fOrg.Email);
        tIniF.WriteString(SectionName,'RukF',fOrg.Ruk.RukF);
        tIniF.WriteString(SectionName,'RukI',fOrg.Ruk.RukI);
        tIniF.WriteString(SectionName,'RukO',fOrg.Ruk.RukO);
        tIniF.WriteString(SectionName,'BuhF',fOrg.Buh.BuhF);
        tIniF.WriteString(SectionName,'BuhI',fOrg.Buh.BuhI);
        tIniF.WriteString(SectionName,'BuhO',fOrg.Buh.BuhO);
        tIniF.WriteString(SectionName,'SerLic', fOrg.Lic.SerLic);
        tIniF.WriteString(SectionName,'NumLic', fOrg.Lic.NumLic);
        tIniF.WriteDate(SectionName,'BegLic', fOrg.Lic.BegLic);
        tIniF.WriteDate(SectionName,'EndLic', fOrg.Lic.EndLic);

        eNameOrg.Text := fOrg.SelfName;
        eAdress.Text := fOrg.Address;
        eTel.Text := fOrg.NPhone;
        eEmail.Text := fOrg.Email;
        eFRuk.Text := fOrg.Ruk.RukF;
        eIRuk.Text := fOrg.Ruk.RukI;
        eORuk.Text := fOrg.Ruk.RukO;
        eFBuh.Text := fOrg.Buh.BuhF;
        eIBuh.Text := fOrg.Buh.BuhI;
        eOBuh.Text := fOrg.Buh.BuhO;
        eLicSer.Text := fOrg.Lic.SerLic;
        eLicNum.Text := fOrg.Lic.NumLic;
        dtpLicDBeg.Date := fOrg.Lic.BegLic;
        dtpLicDEnd.Date := fOrg.Lic.EndLic;


        Reg.RootKey := HKEY_CURRENT_USER;
        Key := 'SOFTWARE\' + NameProg + '\' + fOrg.inn + '\';
        if Reg.OpenKey(Key, True) then
          if Reg.ValueExists('License') then
          begin
            i := Reg.GetDataSize('License');
            GetMem(buf1, i);
            ms1:=TMemoryStream.Create;
            try
              Reg.ReadBinaryData('License', buf1^, i);
              ms1.WriteBuffer(buf1^, i);
              ms1.Position := 0;
              tIniF.WriteBinaryStream('Parameters', 'Registration', ms1);
              tIniF.UpdateFile;
              FreeMem(buf1, i);
              ReadKPP_Ini(SLPerReg);
              UpdateRegData(fmMain.cbAllKvart.ItemIndex);
            finally
              ms1.free;
            end;
          end;
      finally
        tIniF.Free;
        reg.Free;
      end;
    end
  else
    Show_Inf('������ ����������� � ������� �� �������',fError);
end;

procedure TfmOrg.pPeopleResize(Sender: TObject);
var width:integer;
begin
  width:=TRunc((Sender as TPanel).Width/2) -1;
  gbRuk.Width := width;
end;

procedure TfmOrg.pTelEmailResize(Sender: TObject);
var width:integer;
begin
  width:= Trunc((Sender as TPanel).Width/2)-1;
  pTel.Width := width;
end;

procedure TfmOrg.LabeledNameEditExit(Sender: TObject);
var idx,i:integer;
    p:TPanel;
    n:string;
begin
  idx:=((Sender as TLabeledEdit).Parent as TPanel).Tag;
  p:= (Sender as TLabeledEdit).Parent as TPanel;
  for i := 0 to p.ControlCount-1 do
    if p.Controls[i] is TLabeledEdit then
      begin
        n:=(p.Controls[i] as TLabeledEdit).Name;
        if Pos('le1',n)>0 then
          aKpp[idx].KPP:=(p.Controls[i] as TLabeledEdit).Text;
        if Pos('le3',n)>0 then
          begin
            aKpp[idx].namename:=(p.Controls[i] as TLabeledEdit).Text;
            if aKpp[idx].namename = '' then
              (Sender as TLabeledEdit).Color := clCheck
            else
              (Sender as TLabeledEdit).Color := clWindow;
          end;
        if Pos('le2',n)>0 then
          begin
            (p.Controls[i] as TLabeledEdit).Text :=AdressKpp(aKpp[idx]); //dfm.leFullAdr.Text;
            if (aKpp[idx].IndexP = '') or (aKpp[idx].Codereg = '') then
              (Sender as TLabeledEdit).Color := clCheck
            else
              (Sender as TLabeledEdit).Color := clWindow;
          end;
      end;
  UpdateKpp(aKpp[idx], fmWork.que1_sql);
end;



end.
