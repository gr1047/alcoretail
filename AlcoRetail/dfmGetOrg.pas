unit dfmGetOrg;

interface

uses
  Forms, StdCtrls, Mask, AdvSpin, Controls, Classes, Registry, IniFiles,
  SysUtils, ShlObj, Windows, ExtCtrls;

type
  TfmGetOrg = class(TForm)
    lbl1: TLabel;
    lbl_Org: TLabel;
    cbb_OrgInn: TComboBox;
    btn_Ok: TButton;
    Button1: TButton;
    lb1: TLabel;
    edt1: TAdvSpinEdit;
    btn1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cbb_OrgInnChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_OkClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure edt1Change(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  fmgetOrg: TfmGetOrg;
  IsIni, IsReg:Boolean;
implementation

uses Global, GlobalConst, GlobalUtils, dfmSelSert;
{$R *.dfm}

procedure TfmGetOrg.btn1Click(Sender: TObject);
Var org,tinn, fn : String;
  Reg : TRegistry;
  tIniF:TIniFile;
  tIF : TIniFile;
  cYear, cMonth, cDay:word;
begin
  if fmSelSert = nil then fmSelSert:=TfmSelSert.Create(Self);
  NowYear := edt1.Value;
  FindEcp:=SetECP;
  CurYear:=NowYear;
  DecodeDate(Now() - 20, cYear, cMonth, cDay);
  CurKvart := Kvartal(cMonth);

  if not FindEcp then
    begin
      Show_Inf('�� ������� �� ������ �����������', fError);
      fmSelSert.Close;

    end
  else
  begin
    fmSelSert.ShowModal;
    if SelectCert then
    begin
      fOrg.Inn := fSert.INN;
      if Copy(fSert.INN,0,2) = '00' then
        fOrg.Inn :=Copy(fSert.INN,3,10);
      fOrg.Kpp0 := fSert.kpp;
      fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
      if fOrg.Inn <> NullInn then
      begin
        CreatePath(fOrg.Inn);
        LoadDir;
        tIF:=TIniFile.Create(fn);
        try
          tIF.WriteString(SectionName, 'Sert', fSert.path);
          fOrg.SelfName:=tIF.ReadString(SectionName,'SelfName', '');
          if fOrg.SelfName = '' then
          begin
            fOrg.SelfName := fSert.OrgName;
            tIF.WriteString(SectionName,'SelfName', fOrg.SelfName);
          end;

          tIF.WriteString(SectionName,'INN', fOrg.Inn );
          tIF.WriteString(SectionName,'KPP', String(fOrg.Kpp0));
          if fOrg.SelfName = '' then
            fOrg.SelfName := fSert.OrgName;

        finally
          tIF.Free;
        end;
      end;
      Reg := TRegistry.Create;
      fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
         + '\' + NameProg + '\' + NameProg + '_inn.ini';
      tIniF := TIniFile.Create(fn);
      try
        tinn := fOrg.Inn;
        if tinn=l_NewClient then
          begin
            org := l_NewClient;
            fOrg.Inn :=NullInn;
            fOrg.Kpp0 :='000000000';
            newclient := true;
            exit;
          end
        else
        begin
          newclient := false;
          fOrg.Inn := tinn;
          lbl_Org.Caption:=ReadStringParam('SelfName', '(�� �������)');
          if lbl_Org.Caption = '' then
            lbl_Org.Caption := NameNewOrg;
          fOrg.SelfName:=lbl_Org.Caption;
          tIniF.WriteString('LastInn', 'INN', tinn);
        end;
      finally
        Reg.Free;
        tIniF.Free;
      end;
    end;
  end;
end;

procedure TfmGetOrg.btn_OkClick(Sender: TObject);
begin
  NowYear := edt1.Value;
end;

procedure TfmGetOrg.Button1Click(Sender: TObject);
begin
  //Close;
  NowYear := edt1.Value;
  Application.Terminate;
end;

procedure TfmGetOrg.cbb_OrgInnChange(Sender: TObject);
Var org,tinn, fn : String;
    Reg : TRegistry;
    tIniF:TIniFile;
begin
  Reg := TRegistry.Create;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + NameProg + '_inn.ini';
  tIniF := TIniFile.Create(fn);
  try
  tinn := cbb_OrgInn.Items[cbb_OrgInn.ItemIndex];
  if tinn=l_NewClient then
    begin
      org := l_NewClient;
      fOrg.Inn :=NullInn;
      fOrg.Kpp0 :='000000000';
      newclient := true;
      lbl_Org.Caption:='(�� �������)';
      exit;
    end
  else
    begin
      newclient := false;
      fOrg.Inn := tinn;
      lbl_Org.Caption:=ReadStringParam('SelfName', '(�� �������)');
      if lbl_Org.Caption = '' then
        lbl_Org.Caption := NameNewOrg;
      fOrg.SelfName:=lbl_Org.Caption;
      tIniF.WriteString('LastInn', 'INN', tinn);
    end;
  finally
    Reg.Free;
    tIniF.Free;
  end;
end;

procedure TfmGetOrg.edt1Change(Sender: TObject);
begin
  CurYear := edt1.Value;
end;

procedure TfmGetOrg.FormCreate(Sender: TObject);
var path:string;
    masOrg : DynamicArray;
    i, idx:integer;
    Key, fn, inn, s:string;
    Reg : TRegistry;
    SLOrg, RegOrgs:TStringList;
    tIniF, tIniMain:TIniFile;
    IsBaseDecl:Boolean;
    cYear, cMonth, cDay:word;
begin
  Reg := TRegistry.Create;
  SLOrg:=TStringList.Create;
  RegOrgs:=TStringList.Create;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + NameProg + '_inn.ini';
  tIniMain:=TIniFile.Create(fn);
  DecodeDate(Now(), cYear, cMonth, cDay);
  edt1.MaxValue := cYear;
  Caption := '�������������� ������ ' + FileVersion(ParamStr(0));
  try
    //���� � ����� AlcoRetail � ���� ����� � ���������� ����������� �� ���
    path:= GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
    Reg.RootKey := HKEY_CURRENT_USER;
    user := GetUser();
    GetFiles(masOrg,path);
    for i := 0 to High(masOrg) do
      begin
        if StrInnCheck(masOrg[i]) then //������ ������������� ������� ���
          begin
            IsIni:=False;
            IsReg:=False;
            IsBaseDecl:=False;
            s:=masOrg[i];
            fn := path + s + '\'+NameProg+'.ini';
            //�������� �� ������������� ������� ����������� � Ini �����
            if FileExists(fn) then
              begin
                tIniF := TIniFile.Create(fn);
                IsIni := True;
                try
                if IsRegIniF(tIniF) then
                  IsIni := True;
                finally
                  tIniF.free;
                end;
              end;
            //�������� �� ������������� ������� ����������� � �������

            Key := 'SOFTWARE\'+NameProg+'\'+masOrg[i];
            if not IsIni then
              if Reg.OpenKeyReadOnly(Key) then
                if IsRegReestr(Reg) then
                  begin
                    IsReg:=True;
                    Reg.CloseKey;
                  end;

            if IsIni or IsReg then
              begin
                db_decl := '';
                path_db := '';
                path_arh := '';
                //�������� ������� ���� � ����������
                IsBaseDecl := ExistBaseDecl(masOrg[i]);
                SLOrg.Add(masOrg[i]);
              end;
          end;
      end;
    cbb_OrgInn.Items.Assign(SLOrg);
    if cbb_OrgInn.Items.Count = 0 then
      newclient := true
    else
      newclient := false;
    cbb_OrgInn.Items.Add(l_NewClient);
    cbb_OrgInn.DropDownCount := cbb_OrgInn.Items.Count;
    inn:=tIniMain.ReadString('LastInn', 'INN', NullInn);
    idx := 0;
    if (inn <> NullInn) and (inn <> '') then
      begin
        idx:=cbb_OrgInn.Items.IndexOf(inn);
        if idx = -1 then idx := 0;
      end;

    if cbb_OrgInn.Items.Count > 0 then cbb_OrgInn.ItemIndex := idx;
    cbb_OrgInnChange(Sender);
    //ScaleForm(fmGetOrg);
  finally
    RegOrgs.Free;
    SLOrg.Free;
    Reg.Free;
    tIniMain.Free;
  end;
end;

procedure TfmGetOrg.tmr1Timer(Sender: TObject);
var s:string;
//  b:Boolean;
begin

  if cbb_OrgInn.ItemIndex <> -1 then
  begin
    s:= cbb_OrgInn.Items.Strings[cbb_OrgInn.ItemIndex];
    if s <> l_NewClient then
      btn_Ok.Enabled := True
    else
      btn_Ok.Enabled := False;
  end
  else
    btn_Ok.Enabled := False;

end;

end.
