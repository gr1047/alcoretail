unit dfmImportRefDBF;

interface

uses
  Forms, Grids, AdvObj, BaseGrid, AdvGrid, ComCtrls, StdCtrls, ExtCtrls,
  Buttons, Classes, Controls, SysUtils, Graphics, AdvUtil;

type
  TfmImportRefDBF = class(TForm)
    pnl10: TPanel;
    pnl8: TPanel;
    btn5: TBitBtn;
    pnl11: TPanel;
    le_FileName: TLabeledEdit;
    pgcDBF: TPageControl;
    ts2: TTabSheet;
    pnl2: TPanel;
    pnl12: TPanel;
    btn6: TBitBtn;
    pnl4: TPanel;
    btn1: TBitBtn;
    ts3: TTabSheet;
    pnl3: TPanel;
    pnl13: TPanel;
    btn7: TBitBtn;
    pnl14: TPanel;
    btn8: TBitBtn;
    pnl15: TPanel;
    btn9: TBitBtn;
    SG_Cur: TAdvStringGrid;
    SG_New: TAdvStringGrid;
    Panel1: TPanel;
    BitBtn1: TBitBtn;

    procedure FormShow(Sender: TObject);
    procedure SG_NewGetEditorProp(Sender: TObject; ACol, ARow: Integer;
      AEditLink: TEditLink);
    procedure SG_NewGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure SG_NewCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure SG_NewClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure btn7Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure SG_NewCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  fmImportRefDBF: TfmImportRefDBF;
  DBFFileName:string;

implementation

uses Global, Global2, GlobalUtils, GlobalConst, DfmFNS, dfmFiltrProd,
  dfmDataExchange, dfmWork, dfmRef;
{$R *.dfm}


procedure TfmImportRefDBF.BitBtn1Click(Sender: TObject);
var tFile: TTypeFile;
  fName: string;
begin
  fmWork.dlgSave_Export.Filter := '������� � DBF|*.dbf|������� � XLS|*.xls';
  if fmWork.dlgSave_Export.Execute then
  begin
    fName := fmWork.dlgSave_Export.FileName;
    tFile := GetTypeFile(ANSIUpperCase(ExtractFileExt(fmWork.dlgOpen_Import.FileName)));
    case tFile of
      fDBF: SaveDBFRef(fName, SG_Cur);
      fXLS: SaveXLSRef(fName, SG_Cur);
    end;
  end;
end;

procedure TfmImportRefDBF.btn1Click(Sender: TObject);
var i, idx, c:integer;
  prod:TProduction;
  FullData, b1:Boolean;
  f:Extended;
  SLData, SLTmp:TStringList;
  s:string;
begin
  SLData:=TStringList.Create;
  SLTmp:=TStringList.Create;
  try
    c:= 0;
    for i := SG_New.FixedRows to SG_New.RowCount-1 do
    begin
      NullProd(prod);
      FullData:=True;
      prod.EAN13 := ShortString(SG_New.Cells[1,i]);
      if not EANCheck(String(prod.EAN13), b1) then
        FullData:= False;

      prod.Productname := ShortString(SG_New.Cells[2,i]);
      if prod.Productname = '' then
        FullData := False;

      if ValidFloat(SG_New.Cells[3,i],f) then
        prod.Capacity := f
      else
        FullData:=False;

      prod.FNSCode := ShortString(SG_New.Cells[4,i]);
      if prod.FNSCode = '' then
        FullData:=False;

      if ValidFloat(SG_New.Cells[5,i],f) then
        prod.AlcVol := f
      else
        FullData:=False;

      prod.prod.pName := ShortString(SG_New.Cells[6,i]);
      if prod.prod.pName = '' then
        FullData:=False;

      prod.prod.pInn := ShortString(SG_New.Cells[7,i]);
      If (not InnCheck(String(prod.prod.pINN), b1)) and
        (not INN12Check(String(prod.prod.pINN), b1)) then
        FullData:=False;

      prod.prod.pKpp := ShortString(SG_New.Cells[8,i]);
      if (not StrCheck(String(prod.prod.pKPP))) or (Length(String(prod.prod.pKPP)) <> 9)
        or (prod.prod.pKPP = '000000000') then
        FullData:=False;

      prod.prod.pAddress := ShortString(SG_New.Cells[9,i]);
      if prod.prod.pAddress = '' then
        prod.prod.pAddress := '������';

      if FullData then
      begin
        fmDataExchange.SG_Query.ClearNormalCells;
        fmDataExchange.SG_Query.RowCount := fmDataExchange.SG_Query.FixedRows +
         fmDataExchange.SG_Query.FixedFooters;

        SetLength(RefsAddEAN, 10, 0);
        SetLength(RefsAddEAN, Length(RefsAddEAN), Length(RefsAddEAN[0]) + 1);

        SLData.AddObject(string(prod.EAN13), TProduction_Obj.Create);
        TProduction_Obj(SLData.Objects[SLData.Count - 1]).data := prod;

        s := cQueryEAN + string(prod.EAN13);
        SLTmp.Assign(fmDataExchange.SG_Query.Cols[1]);
        idx := SLTmp.IndexOf(s);
        if idx <> -1 then
        begin
          fmDataExchange.SG_Query.RemoveRows(idx,1);
          Application.ProcessMessages;
        end;

        s := cAddEAN + string(prod.EAN13);
        fmDataExchange.SG_Query.InsertRows(fmDataExchange.SG_Query.RowCount -
          fmDataExchange.SG_Query.FixedRows -
          fmDataExchange.SG_Query.fixedfooters+1,1);
         Application.ProcessMessages;
        fmDataExchange.SG_Query.Cells[1, fmDataExchange.SG_Query.RowCount -
          fmDataExchange.SG_Query.FixedRows -
          fmDataExchange.SG_Query.fixedfooters] := s;
        Application.ProcessMessages;

        SetLength(RefsAddEAN, Length(RefsAddEAN), Length(RefsAddEAN[0]) + 1);
        RefsAddEAN[0, Length(RefsAddEAN[0]) - 1]:=string(prod.FNSCode);

        if (prod.EAN13 = '500') or (prod.EAN13 = '510') or
          (prod.EAN13 = '520') then
          RefsAddEAN[1, Length(RefsAddEAN[0]) - 1] :=
            string(prod.EAN13) + string(prod.Productname)
        else
          RefsAddEAN[1, Length(RefsAddEAN[0]) - 1] :=
            string(prod.FNSCode) + string(prod.Productname);

        RefsAddEAN[2, Length(RefsAddEAN[0]) - 1] := string(prod.EAN13);
        RefsAddEAN[3, Length(RefsAddEAN[0]) - 1] := FloatToStr(prod.AlcVol);

        if (prod.EAN13 = '500') or (prod.EAN13 = '510') or
          (prod.EAN13 = '520') then
          RefsAddEAN[4, Length(RefsAddEAN[0]) - 1] := '1'
        else
          RefsAddEAN[4, Length(RefsAddEAN[0]) - 1] := FloatToStr(prod.Capacity);

        RefsAddEAN[5, Length(RefsAddEAN[0]) - 1] := string(prod.prod.pName);
        RefsAddEAN[6, Length(RefsAddEAN[0]) - 1] := string(prod.prod.pInn);
        RefsAddEAN[7, Length(RefsAddEAN[0]) - 1] := string(prod.prod.pKpp);
        RefsAddEAN[8, Length(RefsAddEAN[0]) - 1] := string(prod.prod.pAddress);
        RefsAddEAN[9, Length(RefsAddEAN[0]) - 1] := string(prod.prod.pAddress);

        fmWork.sb_DataExchangeClick(Application);
        fmDataExchange.SG_Query.Cells[2, fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
          '������ �� ���������� �����������';
        fmDataExchange.SG_Query.Objects[2, fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
          Pointer(1);
        fmDataExchange.Enabled := False;
        ConnectServer(fmDataExchange.SG_Query,BMHost, fOrg.inn, exe_version );
        sleep(2000);

        ConnectServer(fmDataExchange.SG_Query,BMHost, fOrg.inn, exe_version );
        InsertEanLocal(fmWork.que2_sql);
        inc(c);
      end;
    end;
    fmDataExchange.btn_ConnectClick(Sender);

    fmDataExchange.Enabled := True;

    ImportDBFRef(DBFFileName, SG_New, SG_Cur);

    Show_inf('��������� ' + IntToStr(c) + ' �������', fInfo);
  finally
    FreeStringList(SLData);
    SLTmp.Free;
  end;
end;

procedure TfmImportRefDBF.btn5Click(Sender: TObject);
begin
  pgcDBF.ActivePageIndex := 0;

  fmWork.dlgOpen_Import.Title := '�������� ���� �����������';
  fmWork.dlgOpen_Import.Filter := '���� dBase (*.dbf)|*.dbf';
  fmWork.dlgOpen_Import.DefaultExt := fmWork.dlgOpen_Import.Filter;
  if fmWork.dlgOpen_Import.Execute then
    if (Length(fmWork.dlgOpen_Import.FileName)>0) and FileExists(fmWork.dlgOpen_Import.FileName) then
        begin
          Caption := fmWork.dlgOpen_Import.FileName;
          DBFFileName:=fmWork.dlgOpen_Import.FileName;
          if DbfIsREF(DBFFileName) then
            FormShow(Application)
          else
            Show_Inf('�������� ��������� �����.' + #10#13 +
               '�������� �� ������� ���� �������.' + #10#13 +
               '�������� ������ DBF ����.',fError);
        end;
end;

procedure TfmImportRefDBF.btn6Click(Sender: TObject);
begin
  Close;
end;

procedure TfmImportRefDBF.btn7Click(Sender: TObject);
begin
  Close;
end;

procedure TfmImportRefDBF.btn8Click(Sender: TObject);
var SLRow:TStringList;
  i:integer;
begin
//  UpdateServerProductionDBF
  SLRow:=TStringList.Create;
  try
    for i := SG_Cur.FixedRows to SG_Cur.RowCount - 1 do
      SLRow.Add(SG_Cur.Cells[1,i]);
    UpdateServerProductionDBF(fmWork.que2_sql,SLRow);
    ImportDBFRef(DBFFileName, SG_New, SG_Cur);
  finally
    SLRow.Free;
  end;
end;

procedure TfmImportRefDBF.FormCreate(Sender: TObject);
begin

  if SLFns.Count = 0 then
    FillSLFNS(SLFNS, FNSCode);
end;

procedure TfmImportRefDBF.FormShow(Sender: TObject);
begin
  ShapkaDBF(SG_Cur, SG_New);
  le_FileName.Text := DBFFileName;
  if not ImportDBFRef(DBFFileName, SG_New, SG_Cur) then exit;
end;

procedure TfmImportRefDBF.SG_NewCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  case ACol of
     1,3,5:
       if ((Sender as TADVStringGrid).CellProperties[ACol,ARow].BrushColor = clCheck) or
         ((Sender as TADVStringGrid).CellProperties[ACol,ARow].BrushColor = clInsert) then
         CanEdit:=True
       else
         CanEdit:=False;
     2,4,6,7,8,9: CanEdit:=False;
  end;
end;

procedure TfmImportRefDBF.SG_NewCellValidate(Sender: TObject; ACol,
  ARow: Integer; var Value: string; var Valid: Boolean);
var b1:Boolean;
begin
  case ACol of
    1: begin
      if not EANCheck(String(Value), b1) then
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck
      else
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert;
    end;
    2,3,4,5,6,9: begin
      if Value = '' then
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck
      else
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert;
    end;
//    3,5: begin
//      if (Sender as TAdvStringGrid).Floats[ACol,ARow] <> 0 then
//        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert
//      else
////        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck;
//    end;
    7: begin
      If (not InnCheck(Value, b1)) and (not INN12Check(Value, b1)) then
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck
      else
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert;
    end;
    8: begin
      if (not StrCheck(Value)) or (Length(Value) <> 9) then
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck
      else
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert;
    end;
  end;
end;

procedure TfmImportRefDBF.SG_NewClickCell(Sender: TObject; ARow, ACol: Integer);
var
  s, pInn, pKpp, pName, pAdr:string;
  p, idx, i:integer;
  b1:Boolean;
  cl:TColor;
begin
  case ACol of
    4:
    begin
      if ((Sender as TADVStringGrid).CellProperties[ACol,ARow].BrushColor <> clCheck) and
       ((Sender as TADVStringGrid).CellProperties[ACol,ARow].BrushColor <> clInsert) then exit;
      if fmFNS = nil then
        fmFNS := TfmFNS.Create(Application);
      if (Sender as TADVStringGrid).Cells[ACol,ARow] <> '' then
        fmFNS.Tag := StrToInt((Sender as TADVStringGrid).Cells[ACol,ARow])
      else
        fmFNS.Tag := 0;

      if fmFNS.ShowModal = mrOk then
        (Sender as TADVStringGrid).Cells[ACol,ARow] := IntToStr(fmFNS.Tag);


      if (Sender as TAdvStringGrid).Cells[ACol,ARow] = '' then
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clCheck
      else
        (Sender as TAdvStringGrid).CellProperties[ACol,ARow].BrushColor := clInsert;
    end;
    6,7,8:
    begin
      if (((Sender as TADVStringGrid).CellProperties[6,ARow].BrushColor <> clCheck) and
         ((Sender as TADVStringGrid).CellProperties[7,ARow].BrushColor <> clCheck) and
         ((Sender as TADVStringGrid).CellProperties[8,ARow].BrushColor <> clCheck)) and
         (((Sender as TADVStringGrid).CellProperties[6,ARow].BrushColor <> clInsert) and
         ((Sender as TADVStringGrid).CellProperties[7,ARow].BrushColor <> clInsert) and
         ((Sender as TADVStringGrid).CellProperties[8,ARow].BrushColor <> clInsert)) then
        exit;

      if fmFiltrProd = nil then
        fmFiltrProd := TfmFiltrProd.Create(Application);
      with fmFiltrProd  do
        if ShowModal = mrOk then
        begin
          idx := fmFiltrProd.cbFullName.ItemIndex;
          if idx <> -1 then
            begin
              s:=cbFullName.Items.Strings[idx];
              p:=Pos('-',s);
              pInn:=Copy(s,0,p-2);
              pKpp := cbKPP.Items.Strings[cbKPP.ItemIndex];
              pName := Copy(s,p+1,Length(s)-p);
              pAdr:= edt_Adr.Text;
              (Sender as TADVStringGrid).Cells[6,ARow] := pName;
              (Sender as TADVStringGrid).Cells[7,ARow] := pInn;
              (Sender as TADVStringGrid).Cells[8,ARow] := pKpp;
              (Sender as TADVStringGrid).Cells[9,ARow] := pAdr;
            end;
        end;

        for i := 6 to 9 do
        begin
          cl := (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor;
          if cl <> clNone then
          case i of
            6,9:
              if (Sender as TAdvStringGrid).Cells[i,ARow] = '' then
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clCheck
              else
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clInsert;

            7:
              If (not InnCheck((Sender as TAdvStringGrid).Cells[i,ARow], b1)) and
                (not INN12Check((Sender as TAdvStringGrid).Cells[i,ARow], b1)) then
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clCheck
              else
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clInsert;
            8:
              if (not StrCheck((Sender as TAdvStringGrid).Cells[i,ARow])) or
                (Length((Sender as TAdvStringGrid).Cells[i,ARow]) <> 9) or
                ((Sender as TAdvStringGrid).Cells[i,ARow] = '000000000') then
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clCheck
              else
                (Sender as TAdvStringGrid).CellProperties[i,ARow].BrushColor := clInsert;
          end;
        end;

    end;

  end;
end;

procedure TfmImportRefDBF.SG_NewGetEditorProp(Sender: TObject; ACol, ARow: Integer;
  AEditLink: TEditLink);
begin
//  (Sender as TAdvStringGrid).ClearComboString; (Sender as TAdvStringGrid).Combobox.Items.Assign(SLFNS);
//  (Sender as TAdvStringGrid).Combobox.DropDownCount := SLFNS.Count;
end;

procedure TfmImportRefDBF.SG_NewGetEditorType(Sender: TObject; ACol, ARow: Integer;
  var AEditor: TEditorType);
begin
  case ACol of
    1, 7, 8:
      AEditor := edNumeric;
    2,4,6,9:
      AEditor := edNormal;
    3, 5:
      AEditor := edFloat;
  end;
end;

end.
