object fmQuery: TfmQuery
  Left = 0
  Top = 0
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1047#1072#1087#1088#1086#1089' '#1085#1072' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1077
  ClientHeight = 336
  ClientWidth = 902
  Color = 15794161
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PrintScale = poNone
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 19
  object nb: TNotebook
    Left = 0
    Top = 0
    Width = 902
    Height = 297
    Align = alTop
    PageIndex = 1
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = '0'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 902
        Height = 297
        Align = alClient
        BevelOuter = bvNone
        Color = 15794161
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object Label3: TLabel
          Left = 12
          Top = 41
          Width = 86
          Height = 16
          Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        end
        object Label7: TLabel
          Left = 534
          Top = 243
          Width = 116
          Height = 16
          Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        end
        object le0_EAN: TLabeledEdit
          Left = 192
          Top = 68
          Width = 217
          Height = 24
          EditLabel.Width = 127
          EditLabel.Height = 16
          EditLabel.Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1045#1040#1053'13'
          LabelPosition = lpLeft
          TabOrder = 2
          OnChange = le0_EANChange
        end
        object le0_AlcVol: TLabeledEdit
          Left = 480
          Top = 68
          Width = 165
          Height = 24
          EditLabel.Width = 54
          EditLabel.Height = 16
          EditLabel.Caption = #1050#1088#1077#1087#1086#1089#1090#1100
          LabelPosition = lpLeft
          TabOrder = 3
          OnKeyPress = le0_AlcVolKeyPress
        end
        object le0_Capacity: TLabeledEdit
          Left = 712
          Top = 68
          Width = 177
          Height = 24
          EditLabel.Width = 47
          EditLabel.Height = 16
          EditLabel.Caption = #1045#1084#1082#1086#1089#1090#1100
          LabelPosition = lpLeft
          TabOrder = 4
          OnKeyPress = le0_CapacityKeyPress
        end
        object le0_INN: TLabeledEdit
          Left = 200
          Top = 235
          Width = 230
          Height = 24
          EditLabel.Width = 117
          EditLabel.Height = 16
          EditLabel.Caption = #1048#1053#1053' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
          LabelPosition = lpLeft
          TabOrder = 6
          OnChange = le0_INNChange
        end
        object le0_NameProd: TLabeledEdit
          Left = 160
          Top = 8
          Width = 729
          Height = 24
          EditLabel.Width = 151
          EditLabel.Height = 16
          EditLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
          LabelPosition = lpLeft
          TabOrder = 0
        end
        object le0_Adress: TLabeledEdit
          Left = 137
          Top = 265
          Width = 752
          Height = 24
          EditLabel.Width = 128
          EditLabel.Height = 16
          EditLabel.Caption = #1040#1076#1088#1077#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
          LabelPosition = lpLeft
          TabOrder = 8
        end
        object le0_Producer: TLabeledEdit
          Left = 264
          Top = 205
          Width = 625
          Height = 24
          AutoSelect = False
          EditLabel.Width = 256
          EditLabel.Height = 16
          EditLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103' ('#1080#1084#1087#1086#1088#1090#1077#1088#1072')'
          LabelPosition = lpLeft
          TabOrder = 5
        end
        object cb0_Wap: TComboBox
          AlignWithMargins = True
          Left = 104
          Top = 38
          Width = 785
          Height = 24
          Style = csDropDownList
          DropDownCount = 33
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object cbKPP: TComboBox
          Left = 656
          Top = 235
          Width = 233
          Height = 24
          TabOrder = 7
        end
        object GroupBox1: TGroupBox
          Left = 4
          Top = 125
          Width = 885
          Height = 74
          Caption = #1060#1080#1083#1100#1090#1088' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103' '#1087#1086' '#1048#1053#1053' '#1080#1083#1080' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1102
          Color = clMoneyGreen
          ParentBackground = False
          ParentColor = False
          TabOrder = 9
          TabStop = True
          object cbFullName: TComboBox
            AlignWithMargins = True
            Left = 3
            Top = 47
            Width = 879
            Height = 24
            Style = csDropDownList
            DropDownCount = 33
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnChange = cbFullNameChange
          end
          object leFiltr: TLabeledEdit
            Left = 3
            Top = 18
            Width = 879
            Height = 24
            EditLabel.Width = 32
            EditLabel.Height = 16
            EditLabel.Caption = 'leFiltr'
            LabelPosition = lpLeft
            TabOrder = 1
            OnChange = leFiltrChange
          end
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = '1'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 902
        Height = 297
        Align = alClient
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label1: TLabel
          Left = 676
          Top = 185
          Width = 54
          Height = 26
          Alignment = taRightJustify
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Label2: TLabel
          Left = 411
          Top = 185
          Width = 49
          Height = 32
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072
          EllipsisPosition = epPathEllipsis
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          Transparent = True
          WordWrap = True
        end
        object Label4: TLabel
          Left = 8
          Top = 163
          Width = 70
          Height = 19
          Caption = #1051#1080#1094#1077#1085#1079#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 8
          Top = 139
          Width = 102
          Height = 18
          Caption = #1042#1080#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object le1_NameOrg: TLabeledEdit
          Left = 176
          Top = 27
          Width = 713
          Height = 24
          EditLabel.Width = 164
          EditLabel.Height = 16
          EditLabel.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
          LabelPosition = lpLeft
          TabOrder = 0
        end
        object le1_Inn: TLabeledEdit
          Left = 123
          Top = 73
          Width = 270
          Height = 24
          EditLabel.Width = 102
          EditLabel.Height = 16
          EditLabel.Caption = #1048#1053#1053' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
          LabelPosition = lpLeft
          NumbersOnly = True
          TabOrder = 1
          OnChange = le1_InnChange
        end
        object le1_KPP: TLabeledEdit
          Left = 532
          Top = 79
          Width = 349
          Height = 24
          EditLabel.Width = 101
          EditLabel.Height = 16
          EditLabel.Caption = #1050#1055#1055' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
          LabelPosition = lpLeft
          NumbersOnly = True
          TabOrder = 2
          OnChange = le1_KPPChange
        end
        object le1_adress: TLabeledEdit
          Left = 123
          Top = 109
          Width = 766
          Height = 24
          EditLabel.Width = 113
          EditLabel.Height = 16
          EditLabel.Caption = #1040#1076#1088#1077#1089' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
          LabelPosition = lpLeft
          TabOrder = 3
        end
        object le1_num: TLabeledEdit
          Left = 255
          Top = 188
          Width = 121
          Height = 24
          EditLabel.Width = 37
          EditLabel.Height = 16
          EditLabel.Caption = #1053#1086#1084#1077#1088
          LabelPosition = lpLeft
          NumbersOnly = True
          ReadOnly = True
          TabOrder = 4
          Text = #1085#1077#1090
        end
        object le1_Lic: TLabeledEdit
          Left = 61
          Top = 188
          Width = 140
          Height = 24
          EditLabel.Width = 36
          EditLabel.Height = 16
          EditLabel.Caption = #1057#1077#1088#1080#1103
          LabelPosition = lpLeft
          ReadOnly = True
          TabOrder = 5
          Text = #1085#1077#1090
        end
        object dtp1_end: TDateTimePicker
          Left = 736
          Top = 188
          Width = 153
          Height = 24
          Date = 41362.608652893520000000
          Time = 41362.608652893520000000
          TabOrder = 6
        end
        object dtp1_beg: TDateTimePicker
          Left = 466
          Top = 188
          Width = 153
          Height = 24
          Date = 41362.608955034720000000
          Time = 41362.608955034720000000
          TabOrder = 7
        end
        object Button2: TButton
          Left = 632
          Top = 368
          Width = 75
          Height = 25
          Caption = 'Button2'
          TabOrder = 8
        end
        object Button4: TButton
          Left = 544
          Top = 336
          Width = 75
          Height = 25
          Caption = 'Button4'
          TabOrder = 9
        end
        object le1_org: TLabeledEdit
          Left = 77
          Top = 226
          Width = 812
          Height = 24
          EditLabel.Width = 70
          EditLabel.Height = 16
          EditLabel.Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085#1072
          EditLabel.Font.Charset = DEFAULT_CHARSET
          EditLabel.Font.Color = clWindowText
          EditLabel.Font.Height = -13
          EditLabel.Font.Name = 'Tahoma'
          EditLabel.Font.Style = []
          EditLabel.ParentFont = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          LabelPosition = lpLeft
          ParentFont = False
          ReadOnly = True
          TabOrder = 10
          Text = #1060#1077#1076#1077#1088#1072#1083#1100#1085#1072#1103' '#1089#1083#1091#1078#1073#1072' '#1087#1086' '#1088#1077#1075#1091#1083#1080#1088#1086#1074#1072#1085#1080#1102' '#1072#1083#1082#1086#1075#1086#1083#1100#1085#1086#1075#1086' '#1088#1099#1085#1082#1072
        end
      end
      object cb_beer: TCheckBox
        Left = 116
        Top = 139
        Width = 97
        Height = 17
        Caption = #1055#1080#1074#1086
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = cb_beerClick
      end
    end
  end
  object btSave: TButton
    Left = 656
    Top = 303
    Width = 120
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentFont = False
    TabOrder = 1
    OnClick = btSaveClick
  end
  object btCancel: TButton
    Left = 776
    Top = 303
    Width = 120
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 456
    Top = 256
  end
end
