unit dfmFiltrRef;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ADODB, AdvGrid, ComCtrls;

type
  TfmFiltrRef = class(TForm)
    Panel2: TPanel;
    btCancel: TButton;
    btOK: TButton;
    nbFiltr: TNotebook;
    Panel1: TPanel;
    leSalName: TLabeledEdit;
    pProduct: TPanel;
    leProdName: TLabeledEdit;
    cbFNS: TComboBox;
    Label1: TLabel;
    pb_filtr: TProgressBar;
    procedure btOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFiltrRef: TfmFiltrRef;

implementation

uses dfmRezRef, Global, GlobalConst, dfmWork;
{$R *.dfm}

procedure TfmFiltrRef.btOKClick(Sender: TObject);
var idx:integer;
     StrSQL, nf,vf, s:string;
    i, p, c, r:integer;
    DBQ:TADOQuery;
    SLFiltr:TStringList;
begin
  pb_filtr.visible:=true;
  SLFiltr:=TStringList.Create;
  with fmRezRef do
    begin
      Tag := nbFiltr.PageIndex;
      SLFiltr.Clear;
      case Tag of
        0: begin
             if leProdName.Text <> '' then
               SLFiltr.Add('Productname-'+leProdName.Text);
             if cbFNS.ItemIndex <> -1 then
               SLFiltr.Add('FNSCode-'+IntToStr(LongInt(cbFNS.Items.Objects[cbFNS.ItemIndex])));
           end;
        1: begin
             if leSalName.Text <> '' then
               SLFiltr.Add('OrgName-'+leSalName.Text);
           end;
      end;

      idx:=Tag;
      SG_Filtr.ClearAll;
      case idx of
        0: ShapkaSG(SG_Filtr,NameColProdRef1);
        1: ShapkaSG(SG_Filtr,NameColSalRef1);
      end;
      try
        try
          DBQ:=fmWork.que2_sql;
          DBQ.Close;
          DBQ.ParamCheck := false;
          DBQ.SQL.clear;

          case idx of
            0: StrSQL := StrSQLProd;
            1: StrSQL := StrSQLSal;
          end;
          if SLFiltr.Count > 0 then
            begin
              StrSQL:= STRSQL + ' WHERE ';
              for i := 0 to SLFiltr.Count - 1 do
                begin
                  s:=SLFiltr.Strings[i];
                  p:=Pos('-',s);
                  if p <> -1 then
                    begin
                      nf:=Copy(s,0,p-1);
                      vf:=Copy(s,p+1,Length(s)-p);
                    end;
                  if nf = 'FNSCode' then
                    s:= nf + ' = ''' + vf + ''' and '
                  else
                    s := nf + ' like ''%' + vf + '%'' and ';
                  StrSQL := StrSQL + s;
                end;
              StrSQL:= Copy(StrSQL,0,Length(StrSQL)-5);
            end;
          DBQ.SQL.Text := StrSQL;
          DBQ.Open;
          SG_Filtr.RowCount := DBQ.RecordCount +SG_Filtr.FixedRows+SG_Filtr.FixedFooters;
          SG_Filtr.Clear;
          pb_filtr.Position := 0;
          pb_filtr.UpdateControlState;
          Application.ProcessMessages;

          DBQ.First;
          r:=SG_Filtr.FixedRows;
          while not DBQ.Eof do
            begin
              for i := 0 to DBQ.Fields.Count-1 do
                begin
                  c:=SG_Filtr.FixedCols+i;
                  SG_Filtr.Cells[c, r]:= DBQ.Fields[i].AsString;
                end;
              pb_filtr.Position := Trunc(100*(r)/DBQ.RecordCount);
              pb_filtr.UpdateControlState;
              Application.ProcessMessages;
              inc(r);
              DBQ.Next;
            end;
          pb_filtr.Position := 0;
          pb_filtr.UpdateControlState;
          Application.ProcessMessages;
          SG_Filtr.AutoNumberOffset:=0;
          SG_Filtr.AutoNumberStart:=0;
          SG_Filtr.AutoNumberCol(0);
          fmRezRef.Panel1.OnResize(fmRezRef);

          SG_Filtr.ColumnSize.Rows := arAll;
          //SG_Filtr.AutoSize := True;
          ShowModal;
        except
        end;
      finally
        SLFiltr.Free;
        pb_filtr.visible:=false;
      end;
    end;
end;

procedure TfmFiltrRef.FormCreate(Sender: TObject);
begin
  FillComboFNS(cbFNS,FNSCode,True);
end;

procedure TfmFiltrRef.FormShow(Sender: TObject);
begin
  cbFNS.ItemIndex :=-1;
  leProdName.Text:='';
  leSalName.Text:='';
end;

end.
