unit GlobalUtils;

interface

uses Windows, ShlObj, SysUtils, ComObj, DAO2000, Classes, Zlib, StdCtrls, ComCtrls,
     Forms, Dialogs, ADODB, wcrypt2, GlobalConst, IdTCPClient, IniFiles,
     ActiveX, excel2000, AdvGrid, Variants, Graphics;

type
    DynamicArray = array of string;


procedure GetSalerMas(var sal:TSaler);
procedure GetProductionMas(var prod:TProduction);
function GetSpecialPath(CSIDL: word): string;
function FileVersion(AFileName: string): string;
Function Date98(d: TDate): boolean;
function Kvart_Beg(kv: Integer; year: Integer): TDateTime;
function Kvart_End(kv: Integer; year: Integer): TDateTime;
function Kvart_Start(data: TDateTime): TDate;
function Kvart_Finish(data: TDateTime): TDate;
function Kvartal(month:integer):integer;
procedure CreateAccessDB(DatabaseName: string);
function SetAccessDBPassword(DatabaseName: string; NewPassword: string = ''): boolean;
function UnZipFilePas(Var ms, ms1 : TMemoryStream; psw:AnsiString):Boolean;
procedure CSCA2(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;
function ReadBuf(ms: TMemoryStream; sms: Integer): string;
function GiveQuarter(Const d: TDate): String;
Function StrToDatCheck(st: String): TDate;
Function StrCheck(str11: String): boolean;
Function StrInt(str11: String): Integer;
function StrInnCheck(StrInn:string):Boolean;
procedure AssignCBSL(cb:TComboBox; SL:TSTringList);
//������� �����������
procedure PosProgressBar(pb:TProgressBar;rpos,rmax:integer);
//�������������� ����������� � ������� �������
procedure ResetProgressBar(pb:TProgressBar);
procedure NullProgressBar(pb:TProgressBar);
//�������������� Float � ������ � ����������� �� ������ ����� �������
function MyFloatToStr(Chislo:Extended):string;
//����������� �������� �������� �� ����
function Kvart_Str(data: TDateTime): string;
//������� ��������� ��� ���������� �� �������� ��������� �� �����������.
function CompNumAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
function CompNumObjAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
//������� ��������� ��� ���������� �� ���� �� �����������.
function CompDateAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
//������� ��������� ��� ���������� �� ���� �� ��������.
function CompDateDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
//������� ��������� ��� ���������� �� ��������� ��������� �� ��������.
function CompStrDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
//������� ��������� ��� ���������� �� �������� ��������� �� ��������.
function CompNumDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
procedure SortSL(SL : TStringList; const aCol : Integer; aCompare : TStringListSortCompare = nil);
function IndexMas(mas:array of integer; val:integer):integer;
//�������� ��� ����� ���
function EANCheck(str11: String; var b1: Boolean): Boolean;
//
function RTrim(s: string): string;
//
Function INNCheck(str11: String; var b1: Boolean): Boolean;
Function INN12Check(str11: String; var b1: Boolean): Boolean;
procedure DeleteDupicateStringList(SL:TStringList);
function NKvart(data: TDateTime): Integer;
Function DatToStrCheck(dt: TDate): String;
procedure GetFiles(var mOut : DynamicArray; Path:String);
//������ INN ����������� �� ����� �����������
function ReadInnFileReg(msFile:TMemoryStream;tINN:integer):string;
//�������� ���������� � ���� ��� ����� �����������
procedure CreatePathReg;
function GetUser(): string;
//procedure ReadKppReg(msReg:TMemoryStream;var SLpr);
function NewOrganization:boolean;
function ChangeAccessDBPassword(DatabaseName: string; OldPassword: string = ''; NewPassword: string = ''): boolean;
Function FileTime2DateTime(FT:_FileTime):TDateTime;
Function LoadRDN(NamInf: PCERT_NAME_INFO):String;
procedure Load_RDN(NamInf: PCERT_NAME_INFO;var fs:TSert);
function ZipFilePas(var ms, ms1: TMemoryStream; psw:AnsiString): boolean;
function DTPart(): string;
function ArhList(filemask: string; List: TStrings): boolean;
function YMDT(fname, marker: string): string;
function CompactDatabase(DatabaseName: String; Password: String = ''): boolean;
function Empty(s: string): boolean;
function ATrim(s: string): string;
function CloseTCP(TCP: TIdTCPClient): boolean;
function DBetween(d, d1, d2: TDateTime): boolean;
procedure ScaleForm(Sender: TObject);
function CreateOleObject(const ClassName: string): IDispatch;
procedure FinishExcel(XL: TExcelApplication);
function Replace(Str, X, Y: string): string;
function SumChis(i1,i2:integer):integer;
function GenNewEAN(StartEAN,Inn,Kpp,FNS:string):string;
procedure PrepareText(Var St : AnsiString);
function IndexFNS(mas:array of string; val:string;old:Boolean):integer;
procedure FillSLSQL(qr: TADOQuery; StrSQL: string; SL: TStringList);
procedure FillSLKpp(qr: TADOQuery; StrSQL: string; var SL: TStringList);
function ReplaceChr(S: string; c1, c2: Char): string;
function ValidInt(ints: string; var i: Integer): boolean;
Function ValidDate(ds: string; var d: TDate): boolean;
function NoValue(s: string): boolean;
function ValidFloat(fs: string; var f: Extended): boolean;
function FFloat(f: Extended): string;
procedure FillSLFNS(var SL:TStringList; mas:array of string);
function CopyCell(Table: TADVStringGrid): string;
function NBetween(n, n1, n2: Integer): boolean;
function CutCell(Table: TADVStringGrid): string;
function Period_Otch(Kvartal: Integer): Integer;
function Bool2Str(b: boolean): string;
function Okrugl(Chislo: Extended; znak: Integer): Extended;
function CreateGuid: string;
procedure CreateResourceFile(DataFile, ResFile: string; ResID: Integer); // id ��������
function OleVariantToString(const Value: OleVariant): string;
function StringToOleVariant(const Value: string): OleVariant;
function WinTemp: string;
function FindSL(str:string; SL:TStrings):integer;
function StrInFloat(str:string):Extended;
procedure FillFullSLFNS(var SL:TStringList; mas:array of string);
function DeleteSimv(str:Char; fullStr:string):string;
function GetDateEndOtch(cDate:TDate):TDate;
function LighterS(Color:TColor; Percent:Byte):TColor;

implementation

uses Global, dfmWork, psnMD5, CSCA;


function CreateGuid: string;
var
  id: TGUID;
begin
  Result := '';
  if CoCreateGuid(id) = S_OK then
    Result := GUIDToString(id);
end;

function Bool2Str(b: boolean): string;
begin
  if b then
    Bool2Str := 'true'
  else
    Bool2Str := 'false';
end;
function Okrugl(Chislo: Extended; znak: Integer): Extended;
var
  S: string;
begin
  S := FloatToStrF(Chislo, ffFixed, maxint, znak);
  Result := StrToFloat(S);
end;
function Period_Otch(Kvartal: Integer): Integer;
var
  p_ot: Integer;
begin
  p_ot := 0;
  case Kvartal of
    1:
      p_ot := 03;
    2:
      p_ot := 06;
    3:
      p_ot := 09;
    4:
      p_ot := 00;
  end;
  Period_Otch := p_ot;
end;

function UnZipFilePas(Var ms, ms1 : TMemoryStream; psw:AnsiString):Boolean;
var decompressStream:TDecompressionStream;
    bytesread : integer;
    mainbuffer : array[0..1023] of AnsiChar; //�����
begin
 Result:=true;
 decompressStream:=nil;
 try
   CSCA2(ms.Memory,ms.Size,pAnsiChar(psw));
   ms1.Clear;
   ms.Position:=0;
   decompressStream:=TDecompressionStream.Create(ms);
   repeat
     bytesread:=decompressStream.Read(mainbuffer, 1024);
     ms1.Write(mainbuffer,bytesread);
   until bytesread<1024;
   decompressStream.Free;
 except
   decompressStream.Free;
   Result:=false;
   exit;
 end;
end;

function ZipFile(var ms, ms1: TMemoryStream): boolean;
var
  CompressStream: TCompressionStream; // �����-���������
  bytesread: Integer;
  mainbuffer: array [0 .. 1023] of Char; // �����
begin
  Result := true;
  CompressStream := nil;
  try
    ms1.clear;
    ms.Position := 0;
    CompressStream := TCompressionStream.Create(clMax, ms1); // ������ �����-��������� c ������������ �������� ������
    repeat
      bytesread := ms.Read(mainbuffer, 1024); // ��������� �� ��������� � �����
      CompressStream.Write(mainbuffer, bytesread);
      // ���������� �� ������ � �����-���������
    until bytesread < 1024; // �� ��� �� ��� ��� ���� ���� �������� �� ����� ������
    CompressStream.free;
    CSCA1(ms1.Memory, ms1.Size, pansichar(psw_loc));
    // CSCA1(ms1.Memory,ms1.Size,pchar(psw_loc)); //psw1+psw2+psw3+psw4));
  except
    Result := false;
    CompressStream.free;
  end;
end;

procedure DeleteDupicateStringList(SL:TStringList);
begin
  with TStringList.Create do
    begin
      // ������������� ��� ��� ���������� � ����� ��� ������� �������� �������, ������� ��� ����
      Sorted := true;
      Duplicates := dupIgnore;
      //��������� ���� �����. ��� ���� ������ (�� ������) ����������� �������� ������� �� ������������.
      Text := sl.Text;
      //���������� ����� �� ��� ��� ����������
      sl.Text := Text;
      //������� ��������� ����������.
      free;
   end;
end;

Function INN12Check(str11: String; var b1: Boolean): Boolean;
const
  m1: array [1 .. 10] of SmallInt = (7, 2, 4, 10, 3, 5, 9, 4, 6, 8);
  m2: array [1 .. 11] of SmallInt = (3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8);
var
  k, n1, n2, n3, n4: SmallInt;
begin
  n1 := 0;
  n2 := 0;
  b1 := True;
  Result := False;
  try
    if (Length(str11) < 12) or (str11 = '000000000000') then
      str11 := ' ';
    b1 := StrCheck(str11);
    if b1 then
    begin
      for k := 1 to 10 do
        n1 := n1 + StrToInt(str11[k]) * m1[k];

//    n3 := (n1 mod 11) mod 10;
      n3 := (n1 mod 11);
      if n3 > 9 then n3 := (n3 mod 10);

      for k := 1 to 11 do
        n2 := n2 + StrToInt(str11[k]) * m2[k];

//    n4 := (n2 mod 11) mod 10;
      n4:= (n2 mod 11);
      if n4 > 9 then n4:=(n4 mod 10);

      if (n3 = StrToInt(str11[11])) and (n4 = StrToInt(str11[12])) then
        Result := True;
    end;
  except
    // err:='������ �������� ���-����� ��� (INN12Check)';
    Result := False;
  end;
end;

function StrInnCheck(StrInn:string):Boolean;
begin
  Result:=False;
  if (Length(StrInn) =10) or ((Length(StrInn) =12)) and
      StrCheck(StrInn) then
    Result:=True;
end;

Function INNCheck(str11: String; var b1: Boolean): Boolean;
const
  m: array [1 .. 9] of SmallInt = (2, 4, 10, 3, 5, 9, 4, 6, 8);
var
  k, N: SmallInt;
begin
  N := 0;
  b1 := True;
  Result := False;
  try
    if (Length(str11) < 10) or (str11 = '0000000000') then
      str11 := ' ';
    b1 := StrCheck(str11);
    if b1 then
    begin
      //������� ����� ������������
      for k := 1 to 9 do
        N := N + StrToInt(str11[k]) * m[k];

//    N := (N mod 11) mod 10;
      N := (N mod 11);
      if N > 9 then N := (N mod 10);

      if (N = StrToInt(str11[10])) then
        Result := True;
    end;
  except
        Result := False;
  end;
end;



function RTrim(s: string): string;
begin
  if s <> '' then
  begin
    while (Length(s) <> 1) and (s[Length(s)] = ' ') do
      SetLength(s, Length(s) - 1);
    if s = ' ' then
      s := '';
  end;
  RTrim := s;
end;

function EANCheck(str11: String; var b1: Boolean): Boolean;
begin
  if (RTrim(str11) = '500') or (RTrim(str11) = '520') then begin Result := True; exit; end;

  if (Length(str11) < 13) or (str11 = '0000000000000') or (Length(str11) > 13) then
    str11 := ' ';

  b1 := StrCheck(str11);
  if b1 then
    Result := True
  else
    Result:=False;
{
  n1 := 0;
  n2 := 0;
  b1 := True;
  Result := False;
  try
    if (Length(str11) < 13) or (str11 = '0000000000000')
      then
      str11 := ' ';
    b1 := StrCheck(str11);
    if b1 then
    begin
      for k := 1 to 6 do
      begin
        n1 := n1 + StrToInt(str11[k * 2 - 1]);
        n2 := n2 + StrToInt(str11[k * 2]);
      end;
      k := n2 * 3 + n1;
      if ((10 - (k mod 10)) mod 10) = StrToInt(str11[13]) then
        Result := True
      else
        Result := False;
    end;
  except
    Result := False;
  end;}
end;

function IndexMas(mas:array of integer; val:integer):integer;
var i:integer;
begin
  Result := -1;
  for i := 0 to High(mas) do
    if mas[i] = val then
      begin
        Result:=i;
        exit;
      end;
end;

procedure SortSL(SL : TStringList; const aCol : Integer; aCompare : TStringListSortCompare = nil);
var
  SlSort, SlRow : TStringList;
  i : Integer;
begin
  //����������� ������.
  SlSort := TStringList.Create;

  //��������� � ����������� ������ ����: "������ - ������".
  //� �������� ������ ����� ���������� �������� ����� ����
  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
  //�� ����������� ������������� ������� - ����� �� ����������� ����������
  //����� �������, ���� ��� ����.
  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
  for i := 0 to SL.Count - 1 do begin
    //������ ��������� ��� ����� ������ �������.
    //SlRow := TStringList.Create;
    SlRow:=Pointer(Sl.Objects[i]);
    //��������� � ����������� ������ ����:
    //������: ������ �� ������ �������� �������;
    //������: ���������, ���������� ����� ������ �������.
    SlSort.AddObject(SLRow.Strings[aCol], SlRow);
  end;

  //��������� �������.
  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;

  SL.Clear;
  SL.Assign(SLSort);

  //���������� ����������� ������.
  FreeAndNil(SlSort);
end;

//procedure SortSLEAN(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    //SlRow:=Pointer(Sl.Objects[i]);
//
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.data.product.EAN13), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;


//procedure SortSLSelfKpp(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.Data.SelfKpp), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;


//procedure SortSLPK(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.Data.pk), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;


//procedure SortSLSaler(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.Data.sal.sInn + decl.Data.sal.skpp), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;


//procedure SortSLFNS(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.Data.product.FNSCode), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;


//procedure SortSLDataDecl(SL : TStringList; aCompare : TStringListSortCompare = nil);
//var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(DateTimeToStr(decl.Data.DeclDate), decl);
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);
//end;

//������� ��������� ��� ���������� �� �������� ��������� �� ��������.
function CompNumDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : Extended;
begin
  N1 := StrToFloatDef(aSl[aIndex1], 0);
  N2 := StrToFloatDef(aSl[aIndex2], 0);
  if N1 > N2 then Result := -1
  else if N1 < N2 then Result := 1
  else Result := 0;
end;

//������� ��������� ��� ���������� �� ��������� ��������� �� ��������.
function CompStrDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  S1, S2 : String;
begin
  S1 := aSl[aIndex1];
  S2 := aSl[aIndex2];
  if S1 > S2 then Result := -1
  else if S1 < S2 then Result := 1
  else Result := 0;
end;

//������� ��������� ��� ���������� �� ���� �� ��������.
function CompDateDesc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : TDateTime;
begin
  N1 := StrToDateTime(aSl[aIndex1]);
  N2 := StrToDateTime(aSl[aIndex2]);
  if N1 > N2 then Result := -1
  else if N1 < N2 then Result := 1
  else Result := 0;
end;

//������� ��������� ��� ���������� �� ���� �� �����������.
function CompDateAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : TDateTime;
begin
  N1 := StrToDateTime(aSl[aIndex1]);
  N2 := StrToDateTime(aSl[aIndex2]);
  if N1 < N2 then Result := -1
  else if N1 > N2 then Result := 1
  else Result := 0;
end;

//������� ��������� ��� ���������� �� �������� ��������� �� �����������.
function CompNumAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : Extended;
begin
  N1 := StrToFloatDef(aSl[aIndex1], 0);
  N2 := StrToFloatDef(aSl[aIndex2], 0);
  if N1 < N2 then Result := -1
  else if N1 > N2 then Result := 1
  else Result := 0;
end;

function CompNumObjAsc(aSl: TStringList; aIndex1, aIndex2: Integer) : Integer;
var
  N1, N2 : LongInt;
begin
  N1 := LongInt(aSl.Objects[aIndex1]);
  N2 := LongInt(aSl.Objects[aIndex2]);
  if N1 < N2 then Result := -1
  else if N1 > N2 then Result := 1
  else Result := 0;
end;

function Kvart_Str(data: TDateTime): string;
var
  Y, m, d: word;
begin
  DecodeDate(data, Y, m, d);
  Result := IntToStr(Kvartal(m)) + ' ������� ' + IntToStr(Y) + ' ����';
end;

function Kvartal(month:integer):integer;
begin
  Result:=-1;
  case month of
    1, 2, 3:
      Result := 1;
    4, 5, 6:
      Result := 2;
    7, 8, 9:
      Result := 3;
    10, 11, 12:
      Result := 4;
  end;
end;

procedure AssignCBSL(cb:TComboBox; SL:TSTringList);
begin
  if SL = nil then exit;
  cb.Items.Assign(SL);
  if SL.Count > 0 then
    cb.ItemIndex := 0;
  if SL.Count < 20 then
    cb.DropDownCount := SL.Count;
end;

Function StrCheck(str11: String): boolean;
var
  k: SmallInt;
begin
  Result := true;
  try
    if Length(str11) = 0 then
      Result := false;
    for k := 1 to Length(str11) do
      if (str11[k] >= #48) and (str11[k] <= #57) then
      else
        Result := false;
  except
    Result := false;
  end;
end;

Function StrInt(str11: String): Integer;
var
  k: SmallInt;
  Good: boolean;
begin
  Result := 0;
  Good := true;
  try
    if Length(str11) = 0 then
      Good := false;
    for k := 1 to Length(str11) do
      if (str11[k] >= #48) and (str11[k] <= #57) then
      else
        Good := false;
    if Good then
      Result := StrToInt(str11)
    else
      Result := 0;
  except
  end;
end;


Function StrToDatCheck(st: String): TDate;
Var
  Y: String;
begin
  try
    if DateWin98 and (Length(st) < 10) then
      Y := Copy(st, 7, 2)
    else
      Y := Copy(st, 7, 4);
    if StrCheck(Copy(st, 1, 2)) and StrCheck(Copy(st, 4, 2)) and StrCheck(Y)
      and (st[3] = '.') and (st[6] = '.') then
      if (StrInt(Copy(st, 1, 2)) > 0) and (StrInt(Copy(st, 1, 2)) < 32) and
        (StrInt(Copy(st, 4, 2)) > 0) and (StrInt(Copy(st, 4, 2)) < 13) and
        (StrInt(Y) > 0) then
        Result := StrToDate(Copy(st, 1, 6) + Y)
      else
        Result := StrToDate('01.01.1900')
      else
        Result := StrToDate('01.01.1900');
  except
    Result := StrToDate('01.01.1900');
  end;
end;

function GiveQuarter(Const d: TDate): String;
var
  Y: String;
begin
  try
    Y := Copy(DateToStr(d), 7, 4);
    if (d >= StrToDatCheck('26.01.' + Y)) and (d < StrToDatCheck('16.04.' + Y))
      then
      Result := '01'
    else if (d >= StrToDatCheck('16.04.' + Y)) and
      (d < StrToDatCheck('16.07.' + Y)) then
      Result := '02'
    else if (d >= StrToDatCheck('16.07.' + Y)) and
      (d < StrToDatCheck('16.10.' + Y)) then
      Result := '03'
    else if ((d >= StrToDatCheck('16.10.' + Y)) and
        (d <= StrToDatCheck('31.12.' + Y))) or
      ((d >= StrToDatCheck('01.01.' + Y)) and (d < StrToDatCheck('26.01.' + Y))
      ) then
      Result := '04'
    else
      Result := '00';
    if ((d >= StrToDatCheck('01.01.' + Y)) and (d < StrToDatCheck('26.01.' + Y))
      ) then
      Y := IntToStr(StrInt(Y) - 1);
  except
    Result := '00';
  end;
  if Length(Y) = 2 then
    Y := '20' + Y;
  Result := Result + Y;
end;

function ReadBuf(ms: TMemoryStream; sms: Integer): string;
var
  buf2: TBytes;
  Encoding: TEncoding;
  sz: Integer;
begin
  SetLength(buf2, sms);
  ms.ReadBuffer(Pointer(buf2)^, sms);
  Encoding := TEncoding.Default;
  sz := TEncoding.GetBufferEncoding(buf2, Encoding);
  Result := Encoding.GetString(buf2, sz, Length(buf2) - sz);
end;
function SetAccessDBPassword(DatabaseName: string; NewPassword: string = ''): boolean;
var
  DAO: _DBEngine;
  DB: Database;
  ClassID: TGUID;
begin
  try
    try
      ClassID := ProgIDToClassID('DAO.DBEngine.36');
    except
      try
        ClassID := ProgIDToClassID('DAO.DBEngine.35');
      except
        raise ;
      end;
    end;
    DAO := CreateComObject(ClassID) as _DBEngine;
    DB := DAO.OpenDatabase(DatabaseName, true, false, '');
    DB.NewPassword(#0, NewPassword);
    Result := true;
  except
    Result := false;
  end;
end;
function ChangeAccessDBPassword(DatabaseName: string; OldPassword: string = ''; NewPassword: string = ''): boolean;
var
  DAO: _DBEngine;
  db: Database;
  ClassID: TGUID;
begin
 try
  try
   ClassID := ProgIDToClassID('DAO.DBEngine.36');
  except
    try
      ClassID := ProgIDToClassID('DAO.DBEngine.35');
    except
      raise;
    end;
  end;
  DAO := CreateComObject(ClassID) as _DBEngine;
  db := DAO.OpenDatabase(DatabaseName, true, false, ';pwd='+OldPassword);
  db.NewPassword(OldPassword, NewPassword);
  Result := true;
 except
  Result := false;
 end;
end;

Function Date98(d: TDate): boolean;
begin
  DateWin98 := True;
  Result:=True;
  try
    if Length(DateToStr(d)) = 10 then
      DateWin98 := false
    else
      DateWin98 := true;
    Result:=DateWin98;
  except
    Result := false;
  end;
end;

function Kvart_End(kv: Integer; year: Integer): TDateTime;
var
  d: string;
begin
  d := IntToStr(year);
  if kv = 0 then kv := 1;

  case kv of
    1:
      d := '31.03.' + d;
    2:
      d := '30.06.' + d;
    3:
      d := '30.09.' + d;
    4:
      d := '31.12.' + d;
  end;
  Kvart_End := StrToDate(d);
end;

function Kvart_Finish(data: TDateTime): TDate;
var
  Y, m, d: word;
  kv: Integer;
begin
  kv := 0;
  DecodeDate(data, Y, m, d);
  case m of
    1, 2, 3:
      kv := 1;
    4, 5, 6:
      kv := 2;
    7, 8, 9:
      kv := 3;
    10, 11, 12:
      kv := 4;
  end;
  Kvart_Finish := Kvart_End(kv, Y);
end;

function Kvart_Start(data: TDateTime): TDate;
var
  Y, m, d: word;
  kv: Integer;
begin
  kv := 0;
  DecodeDate(data, Y, m, d);
  case m of
    1, 2, 3:
      kv := 1;
    4, 5, 6:
      kv := 2;
    7, 8, 9:
      kv := 3;
    10, 11, 12:
      kv := 4;
  end;
  Kvart_Start := Kvart_Beg(kv, Y);
end;

function Kvart_Beg(kv: Integer; year: Integer): TDateTime;
var
  d: string;
begin
  d := IntToStr(year);
  case kv of
    1:
      d := '01.01.' + d;
    2:
      d := '01.04.' + d;
    3:
      d := '01.07.' + d;
    4:
      d := '01.10.' + d;
  end;
  Kvart_Beg := StrToDate(d);
end;

function GetSpecialPath(CSIDL: word): string;
var
  s: string;
begin
 SetLength(s, MAX_PATH);
  if not SHGetSpecialFolderPath(0, PChar(s), CSIDL, true) then
    s := GetSpecialPath(CSIDL_APPDATA);
//  if CSIDL = CSIDL_PROGRAM_FILES then
//      Result := s.Split(['\'])[0] + '\AlcoSoft'
//  else
    Result := PChar(s);
end;

function FileVersion(AFileName: string): string;
var
  szName: array [0 .. 255] of Char;
  p: Pointer;
  Value: Pointer;
  len: UINT;
  GetTranslationString: string;
  FFileName: PChar;
  FValid: boolean;
  FSize: DWORD;
  FHandle: DWORD;
  FBuffer: PChar;
begin
  FSize := 0;
  FBuffer := nil;
  FFileName := nil;
  try
    FFileName := StrPCopy(StrAlloc(Length(AFileName) + 1), AFileName);
    FValid := false;
    FSize := GetFileVersionInfoSize(FFileName, FHandle);
    if FSize > 0 then
      try
        GetMem(FBuffer, FSize);
        FValid := GetFileVersionInfo(FFileName, FHandle, FSize, FBuffer);
      except
        FValid := false;
        // raise;
      end;
    Result := '';
    if FValid then
      VerQueryValue(FBuffer, '\VarFileInfo\Translation', p, len)
    else
      p := nil;
    if p <> nil then
      GetTranslationString := IntToHex
        (MakeLong(HiWord(Longint(p^)), LoWord(Longint(p^))), 8);
    if FValid then
    begin
      StrPCopy(szName, '\StringFileInfo\' + GetTranslationString +
          '\FileVersion');
      if VerQueryValue(FBuffer, szName, Value, len) then
        Result := StrPas(PChar(Value));
    end;
  finally
    try
      if FBuffer <> nil then
        FreeMem(FBuffer, FSize);
    except
    end;
    try
      StrDispose(FFileName);
    except
    end;
  end;
end;

procedure CreateAccessDB(DatabaseName: string);
Var
  cat: OLEVariant;
begin
  try
    if FileExists(DatabaseName) then
    begin
      if FileExists(Copy(DatabaseName, 1, Length(DatabaseName) - 3) + 'old')
        then
      begin
        if FileIsReadOnly(Copy(DatabaseName, 1, Length(DatabaseName) - 3)
            + 'old') then
          FileSetReadOnly(Copy(DatabaseName, 1, Length(DatabaseName) - 3)
              + 'old', false);
        DeleteFile(Copy(DatabaseName, 1, Length(DatabaseName) - 3) + 'old');
      end;
      RenameFile(DatabaseName, Copy(DatabaseName, 1, Length(DatabaseName) - 3)
          + 'old');
    end;
    cat := CreateOleObject('ADOX.Catalog');
    cat.Create('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + DatabaseName);
  finally
  end;
end;


procedure CSCA2(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;
var
  PasswordCp:DWORD;
  PasswordLength:DWORD;
asm
  jmp @@Entry

  @@GET_CRC32:
        push esi
        push edi
        push ecx
        push ebx
        push edx

        cld
        xor     ecx,ecx
        dec     ecx
        mov     edx,ecx
   @@NextByteCRC:
        xor     eax,eax
        xor     ebx,ebx
        lodsb
        xor     al,cl
        mov     cl,ch
        mov     ch,dl
        mov     dl,dh
        mov     dh,8
   @@NextBitCRC:
        shr     bx,1
        rcr     ax,1
        jnc     @@NoCRC
        xor     ax,08320h
        xor     bx,0EDB8h
   @@NoCRC:
        dec     dh
        jnz     @@NextBitCRC
        xor     ecx,eax
        xor     edx,ebx
        dec     edi
        jnz     @@NextByteCRC
        not     edx
        not     ecx
        mov     eax,edx
        rol     eax,16
        mov     ax,cx

        pop edx
        pop ebx
        pop ecx
        pop edi
        pop esi
        ret


@@GetZSLength:

        push ecx
        push esi
        push edi

        cld
        xor   al, al
        mov ecx, 0FFFFFFFFh
        mov esi, edi
        repne scasb
        sub edi, esi
        mov eax, edi
        dec eax

        pop edi
        pop esi
        pop ecx
        ret

@@ExpandString:

        pushad
        mov ebx, esi
        mov edi, esi
        call @@GetZSLength
        add edi, eax
        mov ecx, eax
        rep movsb
        mov byte [edi], 0

        add ebx, eax
        mov ecx, 1
       @@rep:
        sub byte [ebx], cl
        cmp byte [ebx], 0
        jnz @@_f
        mov byte [ebx], 7Fh
       @@_f:
        inc ecx
        inc ebx
        cmp byte [ebx],0
        jz @@endrep
        jmp @@rep
       @@endrep:

        popad
        ret

@@NextPasswordMod:
        pushad
        dec edi

        xor edx, edx
        div edi
        add esi, edx
        mov bh, [esi]
        mov ecx, edi
        sub ecx, edx
   @@next:
        mov bl, [esi+1]
        mov [esi], bl
        inc esi
        loop @@next
        mov [esi], bh

        popad
        ret

@@Entry:

        pushad
        mov  edi, Password
        call @@GetZSLength
        mov ebx, eax

        shl eax, 1
        mov  [PasswordLength], eax
        inc eax

        push PAGE_READWRITE
        push MEM_COMMIT+MEM_RESERVE
        push eax
        push 0
        call VirtualAlloc

        mov edi, eax      //; eax = password offset
        mov esi, Password
        mov ecx, ebx
        rep movsb
        mov PasswordCp, eax

        mov esi, eax
        call @@ExpandString

        xor ecx, ecx
    @@next1:     //; ecx
        mov esi, PasswordCp
        mov edi, PasswordLength
        mov eax, ecx
        call @@NextPasswordMod

        mov esi, PasswordCp
        mov edi, PasswordLength
        call @@GET_CRC32   //;eax = CRC32 of current password mod
        mov edi, DataAddress
        imul edx, ecx,4  //;edx = current position
        add edi, edx     //; edi = current offset

        mov ebx, DataSize

        sub ebx, edx
        cmp ebx, 0
        jz @@end
        cmp ebx, 4
        jl @@not_full_xor

     @@full_xor:
        xor [edi], eax
        inc ecx
        jmp @@next1
     @@not_full_xor:
        mov ecx, ebx
       @@rep1:
        dec ecx
        add edi, ecx
        mov dl, [edi]
        xor dl, al
        mov [edi], dl
        sub edi, ecx
        inc ecx
        shr eax, 8
        loop @@rep1
    @@end:
        push MEM_RELEASE
        push 0
        push PasswordCp
        call VirtualFree

        popad
end;

procedure ResetProgressBar(pb:TProgressBar);
begin
  Pb.Position := 100;
  Pb.UpdateControlState;
  Application.ProcessMessages;

  Pb.Position := 0;
  Pb.UpdateControlState;
  Application.ProcessMessages;
end;

procedure PosProgressBar(pb:TProgressBar;rpos,rmax:integer);
begin
  Pb.Position := Trunc((100*rpos)/rmax);
  Pb.UpdateControlState;
  Application.ProcessMessages;
end;

procedure NullProgressBar(pb:TProgressBar);
begin
  pb.Max := 100;
  pb.Min := 0;
  pb.Position := 0;
end;

function MyFloatToStr(Chislo:Extended):string;
begin
  if frac(Chislo)=0 then
    Result := FloatToStrF(Chislo, ffFixed, maxint, 0)
  else
    Result := FloatToStrF(Chislo, ffFixed, maxint, ZnFld);
end;

function NKvart(data: TDateTime): Integer;
var
  kv: Integer;
begin
  kv := (StrInt(Copy(DatToStrCheck(data), 4, 2)) + 2) div 3;
  kv := kv + (StrInt(Copy(DatToStrCheck(data), 9, 2)) - 9) * 4;
  NKvart := kv;
end;

Function DatToStrCheck(dt: TDate): String;
begin
  try
    Result := DateToStr(dt);
    if DateWin98 then
      Result := Copy(Result, 1, 6) + '20' + Copy(Result, 7, 2);
  except
    Result := '01.01.1900';
  end;
end;



function GetUser(): string;
var
  s: string;
  MaxUserName: cardinal;
begin
  MaxUserName := 255;
  SetLength(s, MaxUserName);
  if GetUserName(PChar(s), MaxUserName) then
    s := Copy(s, 1, MaxUserName - 1)
  else
    s := 'Unknown';
  Result := PChar(s);
end;

procedure GetFiles(var mOut : DynamicArray; Path:String);
Var Rec:TSearchRec;
    n: integer;
begin
     n:=0;
     if Path[Length(Path)]<>'\' Then Path:=Path+'\';
     if FindFirst(Path+'*.*',faAnyFile,Rec)=0 then
     begin
          if (Rec.Name<>'.')and(Rec.Name<>'..') then
          begin
               inc(n);
               SetLength(mOut,n);
               mOut[n-1]:=Rec.Name;
          end;
          while FindNext(Rec)=0 do
          begin
               if (Rec.Name<>'.')and(Rec.Name<>'..') then
               begin
                    inc(n);
                    SetLength(mOut,n);
                    mOut[n-1]:=Rec.Name;
               end;
          end;
     end;
     FindClose(Rec);
end;
//������ INN ����������� �� ����� �����������
function ReadInnFileReg(msFile:TMemoryStream;tINN:integer):string;
var ms:TMemoryStream;
    st, newkpp, KPPhash:string;
    i, k:integer;
    ch: Char;
begin
  Result:='';
  ms:=TMemoryStream.Create;
  try
  if UnZipFilePas(msFile, ms, psw_loc) then
    begin
      ms.Position := 0;
      SetLength(st, ms.Size - 32);
      for i := 1 to ms.Size - 32 do
      begin
        ms.ReadBuffer(ch, 1);
        st[i] := ch;
      end;
      newkpp := st;
      SetLength(KPPhash, 32);
      for i := 1 to 32 do
        begin
          ms.ReadBuffer(ch, 1);
          KPPhash[i] := ch;
        end;
      k := 0;
      while (Copy(st, 32 * k + 44 + tINN, 10) <> '0000000000') and
            (Copy(st, 32 * k + 44 + tINN, 10) <> '1111111111') and
            (k <= 16) do
        Inc(k);
      if ((Copy(st, 32 * k + 44 + tINN, 10) = '0000000000') or
          (Copy(st, 32 * k + 44 + tINN, 10) = '1111111111')) and
          (k <= 16) and (((ms.Size - 32 * k - 85) mod (41 + tINN)) = 0) and
          (LowerCase(String(MD5DigestToStr(MD5String(AnsiString(st))))) = KPPhash) and
          (st[1] = '*') then
        Result := Copy(st, 2, 10 + tINN);
    end;
  finally
    ms.Free;
  end;
end;

//�������� ���������� � ���� ��� ����� �����������
procedure CreatePathReg;
var dir_path:string;
begin
 fmWork.con1ADOCon.Close;
 fmWork.con2ADOCon.Close;
 fmWork.con1ADOCon.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+db_decl+
         ';Persist Security Info=False;Jet OLEDB:Database Password='+''''+fOrg.Inn+'''';
 fmWork.con2ADOCon.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+db_spr+
         ';Persist Security Info=False;Jet OLEDB:Database Password='+ '''' +psw_loc+ '''';

 try
   if not DirectoryExists(path_db) then
     begin
       dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
       dir_path:=dir_path+fOrg.Inn+'\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
       dir_path:=dir_path+'DataBase\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
     end;

   if not DirectoryExists(path_arh) then
     begin
       dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
       dir_path:=dir_path+fOrg.Inn+'\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
       dir_path:=dir_path+'BackUp\';
       if not DirectoryExists(dir_path) then
         ForceDirectories(dir_path);
     end;
   if not DirectoryExists(path_arh) then
     ForceDirectories(path_arh);

   if not FileExists(db_decl) then
     Create_DBDecl(fmWork.que1_sql,db_decl,fOrg.Inn);

   if not FileExists(db_spr) then
     begin
       //���� ��� ����������� ����
       path_ref := GetSpecialPath(CSIDL_PROGRAM_FILES) + '\'+NameProg+'\Ref\RefrRefs.bmz';
       if not FileExists(path_ref) then
         Create_DBSpr(fmWork.que2_sql,db_spr,psw_loc)
       else
         MoveDistribRef(path_ref,db_spr); //������� ����� ������������
     end;
   except
    Show_Inf('������ �������� �������� ���� ������. ��������� ����� �������.', fError);
    Application.Terminate;
  end;
end;

Function FileTime2DateTime(FT:_FileTime):TDateTime;
var FileTime:_SystemTime;
begin
   FileTimeToLocalFileTime(FT, FT);
   FileTimeToSystemTime(FT,FileTime);
   Result:=EncodeDate(FileTime.wYear, FileTime.wMonth, FileTime.wDay)+
           EncodeTime(FileTime.wHour, FileTime.wMinute, FileTime.wSecond, FileTime.wMilliseconds);
end;


procedure Load_RDN(NamInf: PCERT_NAME_INFO;var fs:TSert);
Const SZ_ : array [0..6] of LPCSTR =(szOID_INN, szOID_COMMON_NAME,szOID_ORGANIZATION_NAME,
                                     szOID_RSA_emailAddr,szOID_RSA_unstructName,
                                     szOID_SUR_NAME, szOID_GIVEN_NAME);
      SZ1_:array[0..6] of String = ('INN=', 'CN','O','EMAIL','KPP', 'SN', 'G');
Var j : Integer;
    rdn: PCERT_RDN_ATTR;
    s : String;
    SZRez:array[0..6] of String;
begin
  s:='';
  j:=0;

  while j < Length(SZ_) do
    begin
      rdn:=CertFindRDNAttr(SZ_[j], NamInf);
      if rdn<>nil then begin
        case rdn.dwValueType of
          CERT_RDN_PRINTABLE_STRING : s:=String(PAnsiChar(rdn.Value.pbData));   //= 4;
          CERT_RDN_UNICODE_STRING,                                                                     //= 12;
          CERT_RDN_UTF8_STRING      : s:=WideCharToString(PWideChar(rdn.Value.pbData));  //= 13;
          else s:=String(PAnsiChar(rdn.Value.pbData));
        end;
        SZRez[j]:=s;
      end;
      Inc(j);
    end;
  if (Length(SZRez[0]) = 12) and (Copy(SZRez[0],0,2) = '00') then
    fs.INN := Copy(SZRez[0],3,10)
  else
    fs.INN := SZRez[0];

  if Length(SZRez[4]) < 8 then
    begin
      fs.OrgName := SZRez[1];
      fs.FIO := SZRez[5] + ' ' + SZRez[6];
    end
  else
    begin
      fs.OrgName := SZRez[2];
      fs.FIO := SZRez[1];
    end;
  fs.email := String(SZRez[3]);
  fs.kpp := ShortString(Copy(SZRez[4],5,Length(SZRez[4])-4));
end;

Function LoadRDN(NamInf: PCERT_NAME_INFO):String;
Const sz : array[1..14] of LPCSTR=(szOID_COMMON_NAME,szOID_COUNTRY_NAME,szOID_STATE_OR_PROVINCE_NAME,
                                   szOID_STREET_ADDRESS,szOID_LOCALITY_NAME,szOID_ORGANIZATION_NAME,
                                   szOID_ORGANIZATIONAL_UNIT_NAME,szOID_RSA_emailAddr,szOID_TITLE,
                                   szOID_GIVEN_NAME,szOID_INITIALS,szOID_SUR_NAME,szOID_DOMAIN_COMPONENT,
                                   '1.2.840.113549.1.9.2');
      sz1 : array[1..14] of AnsiString=('CN=','C=','S=','STREET=','L=','O=','OU=','Email=','T=','G=','I=','SN=','DC=','INN=');
Var i : SmallInt;
    rdn: PCERT_RDN_ATTR;
    s : AnsiString;
begin
  s:='';
  for i:=1 to Length(sz) do begin
     rdn:=CertFindRDNAttr(sz[i], NamInf);
     if rdn<>nil then begin
        case rdn.dwValueType of
          CERT_RDN_PRINTABLE_STRING : s:=s+sz1[i]+PAnsiChar(rdn.Value.pbData)+'; ';   //= 4;
          CERT_RDN_UNICODE_STRING,                                                                     //= 12;
          CERT_RDN_UTF8_STRING      : s:=s+sz1[i]+AnsiString(WideCharToString(PWideChar(rdn.Value.pbData)))+'; ';  //= 13;
          else s:=s+sz1[i]+PAnsiChar(rdn.Value.pbData)+'; ';
        end;
     end;
  end;
  Result:=String(s);
end;


function ZipFilePas(var ms, ms1: TMemoryStream; psw:Ansistring): boolean;
var
  CompressStream: TCompressionStream; // �����-���������
  bytesread: Integer;
  mainbuffer: array [0 .. 1023] of Char; // �����
begin
  Result := true;
  CompressStream := nil;
  try
    ms1.clear;
    ms.Position := 0;
    CompressStream := TCompressionStream.Create(clMax, ms1); // ������ �����-��������� c ������������ �������� ������
    repeat
      bytesread := ms.Read(mainbuffer, 1024); // ��������� �� ��������� � �����
      CompressStream.Write(mainbuffer, bytesread);
      // ���������� �� ������ � �����-���������
    until bytesread < 1024; // �� ��� �� ��� ��� ���� ���� �������� �� ����� ������
    CompressStream.free;
    CSCA1(ms1.Memory, ms1.Size, pansichar(psw));
    // CSCA1(ms1.Memory,ms1.Size,pchar(psw_loc)); //psw1+psw2+psw3+psw4));
  except
    Result := false;
    CompressStream.free;
  end;
end;

function DTPart(): string;
var
  today, totime: string;
begin
  today := DatToStrCheck(Date);
  today := Copy(today, 7, 4) + Copy(today, 4, 2) + Copy(today, 1, 2);
  totime := TimeToStr(Time);
  if Length(totime) = 7 then
    totime := '0' + totime;
  totime := Copy(totime, 1, 2) + Copy(totime, 4, 2) + Copy(totime, 7, 2);
  DTPart := today + totime;
end;


function ArhList(filemask: string; List: TStrings): boolean;
var
  SR: TSearchRec;
begin
  try
    List.clear;
    List.BeginUpdate;
    if FindFirst(filemask, faAnyFile, SR) = 0 then
      repeat
        List.Add(YMDT(SR.Name, fOrg.Inn + '_') + ID_Dvd + SR.Name);
      until FindNext(SR) <> 0;
      SysUtils.FindClose(SR);
    List.EndUpdate;
    ArhList := true;
  except
    ArhList := false;
  end;
end;


function YMDT(fname, marker: string): string;
var
  today, totime: string;
begin
  if marker <> '' then
    fname := Copy(fname, pos(marker, fname) + Length(marker), 8 + 6)
  else
    fname := Copy(fname, pos('.', fname) - (8 + 6), 8 + 6);
  today := Copy(fname, 7, 2) + '/' + Copy(fname, 5, 2) + '/' + Copy
    (fname, 1, 4);
  totime := Copy(fname, 9, 2) + ':' + Copy(fname, 11, 2) + ':' + Copy
    (fname, 13, 2);
  YMDT := today + ' ' + totime;
end;


function CompactDatabase(DatabaseName: String; Password: String = ''): boolean;
Const
  Provider = 'Provider=Microsoft.Jet.OLEDB.4.0;';
Var
  TempName: Array [0 .. MAX_PATH] of Char;
  TempPath: String;
  Name: String;
  Src, Dest: WideString;
  V: Variant;
begin
  Result := false;
  try
    Src := Provider + 'Data Source=' + DatabaseName;
    TempPath := ExtractFilePath(DatabaseName);
    if TempPath = '' Then
      TempPath := GetCurrentDir;
    GetTempFileName(PChar(TempPath), 'mdb', 0, TempName);
    Name := StrPas(TempName);
    if FileExists(Name) then
      if FileIsReadOnly(Name) then
        FileSetReadOnly(Name, false);
    DeleteFile(PChar(Name));
    Dest := Provider + 'Data Source=' + Name;
    if Password <> '' then
    begin
      Src := Src + ';Jet OLEDB:Database Password=' + Password;
      Dest := Dest + ';Jet OLEDB:Database Password=' + Password;
    end;
    V := CreateOleObject('jro.JetEngine');
    try
      V.CompactDatabase(Src, Dest);
    finally
      V := 0;
    end;
    if FileExists(DatabaseName) then
      if FileIsReadOnly(DatabaseName) then
        FileSetReadOnly(DatabaseName, false);
    DeleteFile(PChar(DatabaseName));
    RenameFile(Name, DatabaseName);
    Result := true;
  except
    Result := false;
  end;
end;

function Empty(s: string): boolean;
begin
  if ATrim(s) = '' then
    Empty := true
  else
    Empty := false;
end;

function ATrim(s: string): string;
var
  i: Byte;
  res: string;
begin
  s := ' ' + s + ' ';
  res := '';
  for i := 2 to Length(s) - 1 do
    if ((s[i] <> ' ') or ((s[i] = ' ') and (s[i - 1] <> ' ') and
          (i < Length(s) - 1))) then
      res := res + s[i];
  if res <> '' then
  begin
    while (Length(res) <> 1) and (res[Length(res)] = ' ') do
      SetLength(res, Length(res) - 1);
    if res = ' ' then
      res := '';
  end;
  ATrim := res;
end;

procedure GetProductionMas(var prod:TProduction);
var idx:integer;
    FindProduser:Boolean;
begin
  idx := SLProductionDecl.IndexOf(String(prod.EAN13));
  if idx <> -1 then
    begin
      prod := TProduction_obj(SLProductionDecl.Objects[idx]).Data;
      if prod.Productname = NoProd then
      begin
        GetProduction(fmWork.que2_sql, prod, FindProduser, True);
        TProduction_obj(SLProductionDecl.Objects[idx]).Data := prod;
      end;
    end
  else
    begin
      GetProduction(fmWork.que2_sql, prod, FindProduser, True);
      SLProductionDecl.AddObject(String(prod.EAN13), TProduction_obj.Create);
      TProduction_obj(SLProductionDecl.Objects[SLProductionDecl.Count-1]).Data := prod;
    end;
end;

procedure GetSalerMas(var sal:TSaler);
var idx:integer;
    FindLic:Boolean;
begin
  idx := SLSalerDecl.IndexOf(String(sal.sInn + sal.sKpp));
  if idx <> -1 then
    begin
      sal:=TSaler_obj(SLSalerDecl.Objects[idx]).Data;
    end
  else
    begin
      GetSaler(fmWork.que2_sql, sal, FindLic, True,False);
      SLSalerDecl.AddObject(String(sal.sInn + sal.sKpp), TSaler_Obj.Create);
      TSaler_obj(SLSalerDecl.Objects[SLSalerDecl.Count-1]).Data := sal;
    end;
end;

function NewOrganization:boolean;
var ms1, ms2, ms3: TMemoryStream;
    s,s1, fn:string;
    idx, tINN:integer;
    Ini:TIniFile;
begin
  Result :=False;
  ms1 := TMemoryStream.Create;
  ms2 := TMemoryStream.Create;
  ms3 := TMemoryStream.Create;
  try
    fmWork.dlgOpen_Import.Title := '�������� ���� ����������� �������� ����� :';
    fmWork.dlgOpen_Import.Filter := '����� ����������� (*.nrt)|*.nrt';
    if fmWork.dlgOpen_Import.Execute then
      begin
        if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.nrt') then
          begin
            s:=fmWork.dlgOpen_Import.FileName;
            s1:=ExtractFileDir(s);
            s:=Copy(s,Length(s1)+2,Length(s)-Length(s1));
            s1:='OnLine';
            idx:=Pos(UpperCase(s1),UpperCase(s));
            case idx of
              11: tINN :=0;
              13: tINN :=2;
              else tINN:=0;
            end;
            ms1.LoadFromFile(fmWork.dlgOpen_Import.filename);
            ms2.LoadFromStream(ms1);
            //if reg.OpenKey(Key, true) then
            if SetBinaryStream(ms2,ms3) then //�������������� ������ � �������� ������
              begin
                //������ INN ����������� �� ����� �����������
                fOrg.Inn:=ReadInnFileReg(ms1,tINN);
                //�������� ���������� �������� ����
                CreatePath(fOrg.Inn);
                //�������� ���������� ��� ����������� � ������ ���� � ����������
                CreatePathReg;
                //������ ����������������� ��� � �������� ����������� �� ������
                ReadKPP_MS(ms2, SLPerReg);
                fn := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn+'\'+NameProg+'.ini';
                Ini := TIniFile.Create(fn);
                try
                  ms3.Position :=0;
                  Ini.WriteBinaryStream(SectionName,'Registration', ms3);
                finally
                  Ini.free;
                end;
                Result:=True;
              end;
          end
        else
          Show_Inf('������������ ��������� ����� ����������� �������� �����', fError);
      end;
  finally
    ms3.Free;
    ms2.free;
    ms1.Free;
  end;
end;

function CloseTCP(TCP: TIdTCPClient): boolean;
begin
  Result :=False;
  try
    if TCP.Connected then
      TCP.Disconnect;
    Result := true;
  except
  end;
end;

function DBetween(d, d1, d2: TDateTime): boolean;
begin
  if ((d1 <= d) and (d <= d2)) then
    Result := true
  else
    Result := false;
end;


procedure ScaleForm(Sender: TObject);

const
  {�������� ��� ���, ����� ��� ���������������
  ������ ���������� �� ����� ����������}
  DesignScrY: LongInt = 1280;
  DesignScrX: LongInt = 800;
  DesignBorder: LongInt = 8; {�������� � ������ ���������� + 1}
var
  SystemScrY: LongInt;
  SystemScrX: LongInt;
  SystemBorder: LongInt;
  OldHeight: LongInt;
  OldWidth: LongInt;
begin
  SystemScrY := GetSystemMetrics(SM_CYSCREEN);
  SystemScrX := GetSystemMetrics(SM_CXSCREEN);
  SystemBorder := GetSystemMetrics(SM_CYFRAME);
  if Sender is TForm then
    with Sender as TForm do
    begin
      //Scaled := True;
      AutoScroll := False;
      Top := Top * SystemScrX div DesignScrX;
      Left := Left * SystemScrX div DesignScrX;
      OldHeight := Height + (DesignBorder - SystemBorder) * 2;
      OldWidth := Width + (DesignBorder - SystemBorder) * 2;
      ScaleBy((OldWidth * SystemScrX div DesignScrX - SystemBorder * 2),
        (OldWidth - DesignBorder * 2));
      {
      ��� ���� �� ������� ������ �������� ��������������
      ��� ������ ��������� ��������:

      OldHeight := Height;
      OldWidth  := Width;
      ScaleBy(SystemScrX, DesignScrX);
      }

      Height := OldHeight * SystemScrY div DesignScrY;
      Width := OldWidth * SystemScrX div DesignScrX;
    end;
end;

function CreateOleObject(const ClassName: string): IDispatch;
var
  ClassID: TCLSID;
begin
  try
    ClassID := ProgIDToClassID(ClassName);
    OleCheck(CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, IDispatch, Result));
  except
    on E: EOleSysError do
      raise EOleSysError.Create(Format('%s, ProgID: "%s"',[E.Message, ClassName]),E.ErrorCode,0) { Do not localize }
  end;
end;

procedure FinishExcel(XL: TExcelApplication);
begin
  // ������������� �� Excel'� ��� ��� ��������, ���� ���� ���� ���� �����
  if not Assigned(XL) then Exit;
  if XL.Workbooks.Count = 0 then begin
    XL.Quit;
  end
  else begin
//    (XL.ActiveWorkbook.Sheets[1] as _Worksheet).Select(True, lcid);
    XL.UserControl := True;
    XL.ScreenUpdating[lcid] := True;
    if not XL.Interactive[lcid] then XL.Interactive[lcid] := True;
    if not XL.Visible[lcid] then XL.Visible[lcid] := True;
    XL.EnableEvents := True;
//    XL.ActiveWorkbook.Saved[lcid] := True;
  end;
  XL.Disconnect;
  FreeAndNil(XL);
end;
//procedure SortSLSalerKpp(SL : TStringList; aCompare : TStringListSortCompare = nil);
//  var
//  SlSort : TStringList;
//  decl:TDecl_obj;
//  i : Integer;
//begin
//  //����������� ������.
//  SlSort := TStringList.Create;
//
//  //��������� � ����������� ������ ����: "������ - ������".
//  //� �������� ������ ����� ���������� �������� ����� ����
//  //�������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
//  //�� ����������� ������������� ������� - ����� �� ����������� ����������
//  //����� �������, ���� ��� ����.
//  //� � �������� ������� ����� ������������ ����� ��������������� ����� �������.
//  for i := 0 to SL.Count - 1 do begin
//    //������ ��������� ��� ����� ������ �������.
//    decl:=Pointer(Sl.Objects[i]);
//    //��������� � ����������� ������ ����:
//    //������: ������ �� ������ �������� �������;
//    //������: ���������, ���������� ����� ������ �������.
//    SlSort.AddObject(String(decl.Data.sal.sKpp), decl);
//    //TDecl_Obj(SlSort.Objects[SlSort.Count-1]).Data := decl;
//  end;
//
//  //��������� �������.
//  if Assigned(aCompare) then SlSort.CustomSort(aCompare) else SlSort.Sort;
//
//  SL.Clear;
//  SL.Assign(SLSort);
//
//  //���������� ����������� ������.
//  FreeAndNil(SlSort);;
//end;

function Replace(Str, X, Y: string): string;
{Str - ������, � ������� ����� ������������� ������.
 X - ���������, ������� ������ ���� ��������.
 Y - ���������, �� ������� ����� ����������� ��������}
var
  buf1, buf2, buffer: string;

begin
  buf1 := '';
  buf2 := Str;
  Buffer := Str;

  if Pos(AnsiUpperCase(X), AnsiUpperCase(buf2)) > 0 then
  begin
    buf2 := Copy(buf2, Pos(AnsiUpperCase(X), AnsiUpperCase(buf2)),
           (Length(buf2) - Pos(AnsiUpperCase(X), AnsiUpperCase(buf2))) + 1);
    buf1 := Copy(Buffer, 1, Length(Buffer) - Length(buf2)) + Y;
    Delete(buf2, Pos(AnsiUpperCase(X), AnsiUpperCase(buf2)), Length(X));
    Buffer := buf1 + buf2;
  end;

  Result := Buffer;
end;


procedure PrepareText(Var St : AnsiString);
Var k, m : SmallInt;
    sSt:string;
begin
  sSt:=String(St);
  if (Pos('`', sSt)>0) then
    sSt:=Copy(sSt, 1, Pos('`', sSt)-1)+''''+Copy(sSt, Pos('`', sSt)+1, Length(sSt)-Pos('`', sSt));
  m:=0;
  for k:=1 to Length(sSt) do if sSt[k]='"' then
    inc(m);
  if (m mod 2)>0 then
    sSt:=sSt+'"';
  m:=1;
  while m<=Length(sSt) do
    begin
      if (sSt[m]='''') then
        begin
          sSt:=Copy(sSt, 1, m)+''''+Copy(sSt, m+1, Length(sSt)-m);
          inc(m);
        end;
      inc(m);
    end;
  st:=AnsiString(sSt);
end;

//������������� ��� �� ��� ��� � ���� ������
function GenNewEAN(StartEAN,Inn,Kpp,FNS:string):string;
var s, ean:string;
    i,i1,i2,l:integer;
begin
  i2:=0;
  i1:=0;
  ean:=StartEAN;
  s:=Copy(Inn,3,8);
  s:=s + Kpp + FNS;
  if (Length(s) = 20) then
    for i := 1 to 10 do
      begin
        i1 := StrToInt(s[i]);
        i2 := StrToInt(s[i+10]);
        ean:=ean+IntToStr(SumChis(i1,i2));
      end;

  for i := 1 to 6 do
  begin
    i1 := i1 + StrToInt(ean[i * 2 - 1]);
    i2 := i2 + StrToInt(ean[i * 2]);
  end;
  i := i2 * 3 + i1;
  l:= ((10 - (i mod 10)) mod 10);

  ean:=ean+IntToStr(l);
  Result:=ean;
end;

//������� ��������� �� ���� ����� ����
function SumChis(i1,i2:integer):integer;
var i3, n1,n2,n :integer;
begin
  Result:=0;
  i3:=i1+i2;
  if i3<10 then
    Result := i3
  else
    begin
      n:= i1+i2;
      n1 := n mod 10;
      n2:= 1;
      Result:=n1+n2;
    end;
end;

function IndexFNS(mas:array of string; val:string;old:Boolean):integer;
var i, idx:integer;
begin
  Result := -1;
  idx:=0;
  for i := 0 to High(mas) do
    begin
      if not old then
        begin
          if Pos('01.07.2012',mas[i])=0 then
            begin
              Inc(idx);
              if (Copy(mas[i],0,3) = val) then
                begin
                  Dec(idx);
                  Result:=idx;
                  exit;
                end;
            end;
        end
      else
        if (Copy(mas[i],0,3) = val) then
          begin
            Result:=i;
            exit;
          end;
    end;
end;

procedure FillSLSQL(qr: TADOQuery; StrSQL: string; SL: TStringList);
var
  fKey: Integer;
  fVal: string;
begin
  SL.Clear;
  qr.SQL.Text := StrSQL;
  qr.Open;
  if qr.RecordCount > 0 then
  begin
    qr.First;
    while not qr.Eof do
    begin

      if qr.FieldCount = 2 then
      begin
        fKey := qr.Fields[0].AsInteger;
        fVal := qr.Fields[1].AsString;
        SL.AddObject(fVal, Pointer(fKey));
      end;
      if qr.FieldCount = 1 then
      begin
        fVal := qr.Fields[0].AsString;
        SL.Add(fVal);
      end;
      qr.Next;
    end;
  end;
end;

procedure FillSLKpp(qr: TADOQuery; StrSQL: string;var SL: TStringList);
var
  fVal, fObj: string;
begin
  FreeStringList(SL);
  SL:=TStringList.Create;
  qr.SQL.Text := StrSQL;
  qr.Open;

  if qr.RecordCount > 0 then
  begin
    qr.First;
    while not qr.Eof do
      begin

        fVal := qr.Fields[0].AsString;
        fObj := qr.Fields[1].AsString;

        SL.AddObject(fVal, TStrObj.Create );
        TStrObj(SL.Objects[SL.Count-1]).Data := fObj;

        qr.Next;

      end;
  end;
end;

function ReplaceChr(S: string; c1, c2: Char): string;
var
  i: Integer;
begin
  for i := 1 to Length(S) do
    if S[i] = c1 then
      S[i] := c2;
  ReplaceChr := S;
end;

function ValidInt(ints: string; var i: Integer): boolean;
var
  e: Integer;
  OK: boolean;
begin
  i := 0;
  OK := false;
  val(ints, i, e);
  if e = 0 then
    OK := true;
  ValidInt := OK;
end;

Function ValidDate(ds: string; var d: TDate): boolean;
var
  OK: boolean;
begin
  if Length(ds) = 8 then
    ds := Copy(ds, 1, 6) + '20' + Copy(ds, 7, 2);
  try
    d := StrToDate(ds);
    OK := true;
  except
    OK := false;
  end;
  ValidDate := OK;
end;

function NoValue(s: string): boolean;
begin
  if Empty(s) or (AnsiUpperCase(s) = 'NULL') then
    NoValue := true
  else
    NoValue := false;
end;

function ValidFloat(fs: string; var f: Extended): boolean;
var
  e: Integer;
  OK: boolean;
begin
  f := 0;
  OK := false;
  val(fs, f, e);
  if e <> 0 then
  begin
    e := pos({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator, fs);
    if e <> 0 then
    begin
      fs[e] := '.';
      val(fs, f, e);
      if e = 0 then
        OK := true;
    end;
  end
  else
    OK := true;
  ValidFloat := OK;
end;

function FFloat(f: Extended): string;
var
  s: string;
begin
  Str(f: 15: 3, s);
  FFloat := ATrim(s);
end;

procedure FillSLFNS(var SL:TStringList; mas:array of string);
var i,count:integer;
    s, kod:string;
begin
  count:=High(mas);
  SL.Clear;
  for i:=0 to count do
    begin
      s:=mas[i];
      kod:=Copy(s,0,3);
      s:= Copy(s,7,Length(s)-6);
      SL.AddObject(s,Pointer(StrToInt(kod)));
    end;
end;

procedure FillFullSLFNS(var SL:TStringList; mas:array of string);
var i,count:integer;
    s, kod:string;
begin
  count:=High(mas);
  SL.Clear;
  for i:=0 to count do
    begin
      s:=mas[i];
      kod:=Copy(s,0,3);
      s:= Copy(s,7,Length(s)-6);
      SL.AddObject(mas[i],Pointer(StrToInt(kod)));
    end;
end;

function CopyCell(Table: TADVStringGrid): string;
var
  r, C: Integer;
  txt: string;
begin
  txt := '';
  with Table do
  begin
    r := row;

    C := RealColIndex(Col);
    if NBetween(r, FixedRows, RowCount - 1) and NBetween(C, 1, ColCount - 1)
      then
      txt := Cells[C, r];
  end;
  Result := txt;
end;

function NBetween(n, n1, n2: Integer): boolean;
begin
  if ((n1 <= n) and (n <= n2)) then
    Result := true
  else
    Result := false;
end;

function CutCell(Table: TADVStringGrid): string;
var
  r, C: Integer;
  txt: string;
begin
  txt := '';
  with Table do
  begin
    r := row;
    C := Col;
    if NBetween(r, FixedRows, RowCount - 1) and NBetween(C, 1, ColCount - 1)
      then
      txt := Cells[C, r];
    Cells[C, r] := '';
  end;
  CutCell := txt;
end;

procedure CreateResourceFile(DataFile, ResFile: string; // ����� ������
  ResID: Integer); // id ��������
var
  FS, RS: TFileStream;
  FileHeader, ResHeader: TResHeader;
  Padding: array [0..SizeOf(DWORD)-1] of Byte;
begin

  { Open input file and create resource file }
  FS := TFileStream.Create( // ��� ������ ������ �� �����
  DataFile, fmOpenRead);
  RS := TFileStream.Create( // ��� ������ ����� ��������
  ResFile, fmCreate);

  { ������ ��������� ����� �������� - ��� ����, �� �����������
  HeaderSize, ResType � ResID }
  FillChar(FileHeader, SizeOf(FileHeader), #0);
  FileHeader.HeaderSize := SizeOf(FileHeader);
  FileHeader.ResId := $0000FFFF;
  FileHeader.ResType := $0000FFFF;

  { ������ ��������� ������ ��� RC_DATA �����
  ��������: ��� �������� ����� ������ ������� ����������
  ��������� ��������� �������, ��������� ������ ��� ���������
  ID �������� }
  FillChar(ResHeader, SizeOf(ResHeader), #0);
  ResHeader.HeaderSize := SizeOf(ResHeader);
  // id ������� - FFFF �������� "�� ������!"
  ResHeader.ResId := $0000FFFF or (ResId shl 16);
  // ��� ������� - RT_RCDATA (from Windows unit)
  ResHeader.ResType := $0000FFFF
  or (WORD(RT_RCDATA) shl 16);
  // ������ ������ - ���� ������ �����
  ResHeader.DataSize := FS.Size;
  // ������������� ����������� ����� ������
  ResHeader.MemoryFlags := $0030;

  { ���������� ��������� � ���� �������� }
  RS.WriteBuffer(FileHeader, sizeof(FileHeader));
  RS.WriteBuffer(ResHeader, sizeof(ResHeader));

  { �������� ���� � ������ }
  RS.CopyFrom(FS, FS.Size);

  { Pad data out to DWORD boundary - any old
  rubbish will do!}
  if FS.Size mod SizeOf(DWORD) <> 0 then
    RS.WriteBuffer(Padding, SizeOf(DWORD) -
    FS.Size mod SizeOf(DWORD));

  { ��������� ����� }
  FS.Free;
  RS.Free;
end;

function OleVariantToString(const Value: OleVariant): string;
var ss: TStringStream;
   Size, s1, s2: integer;
   Data: PByteArray;
begin
   Result:='';
   if Length(Value) = 0 then Exit;
   ss:=TStringStream.Create;
   try
     s1:=VarArrayHighBound (Value, 1);
     s2:=VarArrayLowBound(Value, 1);
     Size := s1 - s2 + 1;
     Data := VarArrayLock(Value);
     try
       ss.Position := 0;
       ss.WriteBuffer(Data^, Size);
       ss.Position := 0;
       Result:=ss.DataString;
     finally
       VarArrayUnlock(Value);
     end;
   finally
     ss.Free;
   end;
end;

function StringToOleVariant(const Value: string): OleVariant;
var Data: PByteArray;
   ss: TStringStream;
begin
   Result:=null;
   if Value='' then Exit;
   ss:=TStringStream.Create(Value);
   try
     Result := VarArrayCreate ([0, ss.Size - 1], varByte);
     Data := VarArrayLock(Result);
     try
       ss.Position := 0;
       ss.ReadBuffer(Data^, ss.Size);
     finally
       VarArrayUnlock(Result);
     end;
   finally
     ss.Free;
   end;
end;

function WinTemp: string;
begin
  SetLength(result, MAX_PATH);
  SetLength(result, GetTempPath(MAX_PATH, PChar(result)));
end;

function FindSL(str:string; SL:TStrings):integer;
var i:integer;
begin
  Result:=-1;
  for i := 0 to SL.Count - 1 do
    if Pos(str,SL.Strings[i]) > 0 then
    begin
      Result := i;
      exit;
    end;
end;

function StrInFloat(str:string):Extended;
var s:string;
begin
  Result:=0;
  if str = '' then exit;

  if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
    s := ReplaceChr(str, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
  if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
    s := ReplaceChr(str, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
  Result := StrToFloat(s);
end;

function DeleteSimv(str:Char; fullStr:string):string;
var Target:Char;
  p:integer;
begin
  Target:=str;
  While POS(Target,fullStr)>0 do
  begin
    P := POS(Target,fullStr);
    DELETE(fullStr,P,Length(Target));
  end;
  Result:=fullStr;
end;

function GetDateEndOtch(cDate:TDate):TDate;
var y,m,d, y1,m1,d1:Word;
  kv:Integer;
  d_end:TDate;
begin
  //����������� ������� ���� � ���, �����, ����
  DecodeDate(cDate,y,m,d);
  //��������� ������� �������
  kv:=Kvartal(m);
  //���� ��������� ��������
  d_end := Kvart_Finish(cDate);
  //����������� ���� ��������� �������� � ���, �����, ����
  DecodeDate(d_end,y1,m1,d1);
  d1:=20;
  //���� ��������� ����� ��������
  if kv = 4 then
  begin
    Inc(y1);
    m1:=1;
  end
  else
    Inc(m1);
  Result := EncodeDate(y1,m1,d1);
end;

function LighterS(Color:TColor; Percent:Byte):TColor;
var r, g, b: Byte;
  begin
   Color:=ColorToRGB(Color);
   r:=GetRValue(Color);
   g:=GetGValue(Color);
   b:=GetBValue(Color);
   r:=r+muldiv(255-r,Percent,100); //�������% ���������� �������
   g:=g+muldiv(255-g,Percent,100);
   b:=b+muldiv(255-b,Percent,100);
   result:=RGB(r,g,b);
end;



end.



