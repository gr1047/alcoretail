unit dfmAllOst;

interface

uses
  Forms, ExtCtrls, Menus, StdCtrls, Buttons, Controls, Grids, AdvObj, BaseGrid,
  AdvGrid, Classes, GlobalConst;

type
  TfmAllOst = class(TForm)
    le_EAN: TLabeledEdit;
    AdvSG_Ost: TAdvStringGrid;
    btn_Ok: TBitBtn;
    btn_Cancel: TBitBtn;
    pm_SG: TPopupMenu;
    mniN1: TMenuItem;
    btn1: TBitBtn;
    tmr1: TTimer;
    procedure FormShow(Sender: TObject);
    procedure mniN1Click(Sender: TObject);
    procedure AdvSG_OstFilterSelect(Sender: TObject; Column, ItemIndex: Integer;
      FriendlyName: string; var FilterCondition: string);
    procedure btn1Click(Sender: TObject);
    procedure le_EANChange(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure AdvSG_OstSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure AdvSG_OstDblClickCell(Sender: TObject; ARow, ACol: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    ostEAN:TEANOst;
  end;

var
  fmAllOst: TfmAllOst;

implementation

uses Global;

{$R *.dfm}

procedure TfmAllOst.AdvSG_OstDblClickCell(Sender: TObject; ARow, ACol: Integer);
begin
  ostEAN.EAN13 := ShortString(AdvSG_Ost.Cells[2,ARow]);
  ostEAN.SelfKpp := ShortString(AdvSG_Ost.Cells[1,ARow]);
  ostEAN.Amount := AdvSG_Ost.Floats[4,ARow];
  le_EAN.Text := string(ostEAN.EAN13);
  ModalResult := mrOk;
//  Close;
end;

procedure TfmAllOst.AdvSG_OstFilterSelect(Sender: TObject; Column,
  ItemIndex: Integer; FriendlyName: string; var FilterCondition: string);
begin
  if FilterCondition = '��������' then
  begin
    FilterCondition := '';
    AdvSG_Ost.RemoveLastFilter;
  end;
end;

procedure TfmAllOst.AdvSG_OstSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  ostEAN.EAN13 := ShortString(AdvSG_Ost.Cells[2,ARow]);
  ostEAN.SelfKpp := ShortString(AdvSG_Ost.Cells[1,ARow]);
  ostEAN.Amount := AdvSG_Ost.Floats[4,ARow];
  le_EAN.Text := string(ostEAN.EAN13);
end;

procedure TfmAllOst.btn1Click(Sender: TObject);
var i, idx:Integer;
  sEdit, sSg:string;
  SL:TIntList;
begin
  SL:=TIntList.Create(-1,-1);
  try
    AdvSG_Ost.UnHideRowsAll;
    for i := AdvSG_Ost.FixedRows to AdvSG_Ost.RowCount - AdvSG_Ost.FixedFooters - 1 do
    begin
      sSg := AdvSG_Ost.Cells[2,AdvSG_Ost.RealRowIndex(i)];
      sEdit := le_EAN.Text;
      idx := Pos(sEdit,sSg);
      if idx = 0  then
        SL.Add(i)
    end;
    AdvSG_Ost.HideRowList(SL);

    AdvSG_Ost.FloatingFooter.ColumnCalc[0] := acCOUNT;
    AdvSG_Ost.Invalidate;

    if AdvSG_Ost.RowCount = AdvSG_Ost.FixedRows + AdvSG_Ost.FixedFooters + 1 then
      begin
        ostEAN.EAN13 := ShortString(AdvSG_Ost.Cells[2,AdvSG_Ost.FixedRows]);
        ostEAN.SelfKpp := ShortString(AdvSG_Ost.Cells[1,AdvSG_Ost.FixedRows]);
        ostEAN.Amount := AdvSG_Ost.Floats[4,AdvSG_Ost.FixedRows];
      end
    else
      begin
        ostEAN.EAN13 := '';
        ostEAN.SelfKpp := '';
        ostEAN.Amount := 0;
      end;
  finally
    SL.Free;
  end;

end;

procedure TfmAllOst.FormShow(Sender: TObject);
var i, Row:Integer;
  mov:TMove;
  c:Extended;
//  SLO:TStringList;
begin
//  SLO := TStringList.Create;
  try
//    for i := 0 to SLOborotKpp.Count - 1 do
//    begin
//      mov := TMove(SLOborotKpp.Objects[i]);
//      if mov.dv.ost1 > 0 then
//      begin
////        SLO.AddObject(SLOborotKpp.Strings[i], SLOborotKpp.Objects[i]);
//        SLO.AddObject(SLOborotKpp.Strings[i], TMove.Create);
//        SLO.Objects[SLO.Count - 1] := mov;
//      end;
//    end;

    AdvSG_Ost.RowCount := AdvSG_Ost.FixedRows + AdvSG_Ost.FixedFooters + SLOstatkiKpp.Count;
    for i := 0 to SLOstatkiKpp.Count - 1 do
    begin
      mov := TMove(SLOstatkiKpp.Objects[i]);
      Row := i + AdvSG_Ost.FixedRows;
      AdvSG_Ost.Cells[1,Row] := string(mov.SelfKpp);
      AdvSG_Ost.Cells[2,Row] := string(mov.product.EAN13);
      AdvSG_Ost.Cells[3,Row] := string(mov.product.Productname);
      AdvSG_Ost.Floats[4,Row] := mov.dv.ost1;
    end;
    AdvSG_Ost.AutoNumberCol(0);

    AdvSG_Ost.ColumnSize.Rows := arFixed;
    AdvSG_Ost.NoDefaultDraw := True;
    AdvSG_Ost.AutoSize := True;
    AdvSG_Ost.ColWidths[3] := 450;

    c := AdvSG_Ost.ColumnCount(0,AdvSG_Ost.FixedRows,AdvSG_Ost.RowCount - AdvSG_Ost.FixedFooters - 1);
    AdvSG_Ost.Floats[i,AdvSG_Ost.RowCount - 1] := c;

    AdvSG_Ost.FloatingFooter.ColumnCalc[0] := acCount;
    ostEAN.EAN13 := '';
    ostEAN.SelfKpp := '';
    ostEAN.Amount := 0;
    le_EAN.Text := '';

    fmAllOst.ClientWidth := AdvSG_Ost.Width + 1;
  finally
//    FreeStringList(SLO);
  end;
end;

procedure TfmAllOst.le_EANChange(Sender: TObject);
begin
  AdvSG_Ost.UnHideRowsAll;
//  selEAN := le_EAN.Text;
  ostEAN.EAN13 := ShortString(le_EAN.Text);
  ostEAN.SelfKpp := '';
  ostEAN.Amount := 0;
end;

procedure TfmAllOst.mniN1Click(Sender: TObject);
var ACol, ARow:integer;
  s:string;
begin
  ACol:=AdvSG_Ost.Col;
  ARow:=AdvSG_Ost.Row;
  s:=AdvSG_Ost.Cells[ACol,ARow];
  AdvSG_Ost.FilterActive := False;
  AdvSG_Ost.Filter.ColumnFilter[ACol].Condition := s;
  AdvSG_Ost.FilterActive := True;
  AdvSG_Ost.ApplyFilter;
  AdvSG_Ost.RemoveLastFilter;
end;

procedure TfmAllOst.tmr1Timer(Sender: TObject);
var len:Integer;
begin
  len := Length(ostEAN.EAN13);
  btn_Ok.Enabled := (len = 13);
end;

end.
