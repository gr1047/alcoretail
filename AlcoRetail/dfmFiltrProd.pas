unit dfmFiltrProd;

interface

uses
  Forms, ADODB, Controls, StdCtrls, Buttons, Classes, ExtCtrls, SysUtils;

type
  TFormStat = (fEdit, fRepl);
  TfmFiltrProd = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    eInn: TEdit;
    Panel2: TPanel;
    TBCancel: TBitBtn;
    TBSave: TBitBtn;
    Panel3: TPanel;
    Label2: TLabel;
    cbFullName: TComboBox;
    Label3: TLabel;
    cbKPP: TComboBox;
    edt_Adr: TEdit;
    lb1: TLabel;
    procedure TBSaveClick(Sender: TObject);
    procedure TBCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbFullNameChange(Sender: TObject);
    procedure editChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbKPPChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
//    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
    SLProducer:TStringList;
  public
    { Public declarations }
    fStat: TFormStat;
  end;

var
  fmFiltrProd: TfmFiltrProd;

implementation

uses GlobalUtils, GlobalConst, dfmWork;
{$R *.dfm}

//procedure TfmFiltrProd.BitBtn3Click(Sender: TObject);
//var i:integer;
//begin
//  for i := 0 to Panel1.ControlCount - 1 do
//    if Panel1.Controls[i] is TEdit then
//      begin
//        (Panel1.Controls[i] as TEdit).Text := '';
//        if (Panel1.Controls[i] as TEdit).Color = clYellow then
//          (Panel1.Controls[i] as TEdit).Color :=clWindow;
//      end;
//  for i := 0 to Panel1.ControlCount - 1 do
//    if Panel1.Controls[i] is TComboBox then
//      (Panel1.Controls[i] as TComboBox).ItemIndex :=-1;
//end;

procedure TfmFiltrProd.cbFullNameChange(Sender: TObject);
var //pk, idx1, idx2, p:integer;
    p:integer;
    StrSQL, s, sName, sInn:string;
    SL:TStringList;
    qr : TADOQuery;
begin
  qr:=TADOQuery.Create(fmWork);
  qr.Connection:=fmWork.con2ADOCon;
  SL:=TStringList.Create;
  edt_Adr.text := '';
  try
    if Sender is TComboBox then
      if (Sender as TComboBox).ItemIndex <> -1 then
      begin
        s:=(Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
        p:=Pos('-',s);
        sName:=Copy(s,p+2,Length(s)-p-1);
        sInn:=Copy(s,0,p-2);
        StrSQL:='SELECT KPP FROM PRODUCER WHERE INN=' + '''' + sInn + '''';
        FillSLSQL(qr,StrSQL,SL);
        AssignCBSL(cbKPP,SL);
        if cbKPP.Items.Count=1 then
          cbKPP.ItemIndex:=0;
        if cbKPP.ItemIndex <> -1 then
          cbKPPChange(cbKPP);
      end;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmFiltrProd.cbKPPChange(Sender: TObject);
var //pk, idx1, idx2, p:integer;
    p:integer;
    StrSQL, s, sName, sInn, sKpp:string;
    SL:TStringList;
    qr : TADOQuery;
begin
  qr:=TADOQuery.Create(fmWork);
  qr.Connection:=fmWork.con2ADOCon;
  SL:=TStringList.Create;
  edt_Adr.Text:='';
  try
    if cbKPP.ItemIndex <> -1 then
    begin
      s:=cbFullName.Items.Strings[cbFullName.ItemIndex];
      p:=Pos('-',s);
      sName:=Copy(s,p+2,Length(s)-p-1);
      sInn:=Copy(s,0,p-2);
      sKpp:=cbKPP.Items.Strings[cbKPP.ItemIndex];
      StrSQL:='SELECT Address FROM PRODUCER WHERE INN = ''' + sInn + ''' and KPP = ''' + sKpp + '''' ;
      qr.SQL.Text := StrSQL;
      qr.Open;
      edt_Adr.Text := qr.FieldByName('Address').AsString;
    end;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmFiltrProd.editChange(Sender: TObject);
var s, s1, s2:string;
    i:integer;
begin
   s:=(Sender as TEdit).Text;
   s1:= AnsiUpperCase(s);
   cbFullName.Clear;
   cbKPP.Clear;
   if s <> '' then
     for i:=0 to SLProducer.Count - 1 do
       begin
         s2:=AnsiUpperCase(SLProducer.Strings[i]);
         if Pos(s1,s2) >0 then
           cbFullName.Items.AddObject(SLProducer.Strings[i],SLProducer.Objects[i]);
       end;
   if s='' then
     for i:=0 to SLProducer.Count - 1 do
       cbFullName.Items.AddObject(SLProducer.Strings[i],SLProducer.Objects[i]);
   end;

procedure TfmFiltrProd.FormActivate(Sender: TObject);
begin
  edt_Adr.text := '';
end;

procedure TfmFiltrProd.FormCreate(Sender: TObject);

begin
  SLProducer:=TStringList.Create;
end;

procedure TfmFiltrProd.FormDestroy(Sender: TObject);
begin
   SLProducer.Free;
end;

procedure TfmFiltrProd.FormShow(Sender: TObject);
var StrSQL, s, sInn, sName, sKPP:string;
    i,idx, p:integer;
    SL:TStringList;
    qr : TADOQuery;
begin
  qr:=TADOQuery.Create(fmWork);
  SL:=TStringList.Create;
  edt_Adr.text := '';
  try
    try
      qr.Connection:=fmWork.con2ADOCon;
      FillSLSQL(qr,StrSQL_Producer,SLProducer);
      AssignCBSL(cbFullName,SLProducer);
    finally
      qr.Free;
    end;

    qr:=TADOQuery.Create(fmWork);
    qr.Connection:=fmWork.con2ADOCon;
    case fStat of
      fEdit:
        begin
          s:=sInn + ' - ' + sName;
          idx := SLProducer.IndexOf(s);
          if idx <> -1 then
            begin
              //FillComboSL(cbFullName,SLProducer);
              cbFullName.ItemIndex :=idx;
              s:=cbFullName.Items.Strings[idx];
              p:=Pos('-',s);
              sName:=Copy(s,p+2,Length(s)-p-1);
              sInn:=Copy(s,0,p-2);
              StrSQL:='SELECT * FROM PRODUCER WHERE INN=' + '''' + sInn + '''';
              FillSLSQL(qr,StrSQL,SL);
              AssignCBSL(cbKPP,SL);

              i := cbKPP.Items.IndexOf(sKPP);
              if i <> -1 then
                begin
                  cbKPP.ItemIndex:=i;
                  cbKPPChange(cbKPP);
                end;
            end;
        end;
    end;
    eInn.Text:='';
    cbFullName.ItemIndex:=-1;
    cbKPP.ItemIndex:=-1;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmFiltrProd.TBCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TfmFiltrProd.TBSaveClick(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.
