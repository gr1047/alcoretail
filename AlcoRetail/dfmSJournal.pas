unit dfmSJournal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, AdvObj, BaseGrid, AdvGrid, Spin, IniFiles,
  GlobalUtils, ShlObj, Menus, Buttons;

type
  TfmSJournal = class(TForm)
    pnl1: TPanel;
    lb1: TLabel;
    lblVer: TLabel;
    btn1: TButton;
    pnl2: TPanel;
    GB_1: TGroupBox;
    pnl5: TPanel;
    pnl_11: TPanel;
    lb_11: TLabel;
    seOst_11: TSpinEdit;
    rbKor: TRadioButton;
    rb1: TRadioButton;
    pnl6: TPanel;
    SG_FNS: TAdvStringGrid;
    pnl_12: TPanel;
    lb_12: TLabel;
    seOst_12: TSpinEdit;
    sb_Main: TSpeedButton;
    procedure seOst_11Exit(Sender: TObject);
    procedure seOst_12Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure SG_FNSCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure rb1Click(Sender: TObject);
    procedure rbKorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSJournal: TfmSJournal;

implementation

uses dfmWork, GlobalConst, Global, Global2;
{$R *.dfm}

procedure TfmSJournal.btn1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
end;

procedure TfmSJournal.FormShow(Sender: TObject);
begin
  if Length(fOrg.Inn) = 12 then
    pnl_11.Visible := False;

  seOst_12.Value := ProcOst_12;
  seOst_11.Value := ProcOst_11;

  GetFNSOst(SG_FNS);
  if (SG_FNS.Cols[1].IndexOf('500') = -1) and
     (SG_FNS.Cols[1].IndexOf('510') = -1) and
     (SG_FNS.Cols[1].IndexOf('520') = -1) and
     (SG_FNS.Cols[1].IndexOf('261') = -1) and
     (SG_FNS.Cols[1].IndexOf('262') = -1) and
     (SG_FNS.Cols[1].IndexOf('263') = -1) then
    pnl_12.Visible := False
  else
    pnl_12.Visible := True;
end;

procedure TfmSJournal.rb1Click(Sender: TObject);
begin
  SG_fns.Enabled := False;
  seOst_11.Enabled := True;
  lb_11.Enabled :=True;
  seOst_12.Enabled := True;
  lb_12.Enabled:=True;
end;

procedure TfmSJournal.rbKorClick(Sender: TObject);
begin
  SG_fns.Enabled := True;
  seOst_11.Enabled := False;
  lb_11.Enabled:=False;
  seOst_12.Enabled := False;
  lb_12.Enabled:=False;
end;

procedure TfmSJournal.seOst_11Exit(Sender: TObject);
begin
  SaveProcOst(seOst_11.Value,11);
end;

procedure TfmSJournal.seOst_12Exit(Sender: TObject);
begin
  SaveProcOst(seOst_12.Value,12);
end;

procedure TfmSJournal.SG_FNSCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  case ACol of
    1,2: CanEdit := False;
    3: CanEdit := True;
  end;
end;

end.
