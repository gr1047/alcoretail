object fmReplace: TfmReplace
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1047#1072#1084#1077#1085#1080#1090#1100
  ClientHeight = 175
  ClientWidth = 386
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 18
  object Notebook1: TNotebook
    Left = 0
    Top = 0
    Width = 386
    Height = 175
    Align = alClient
    TabOrder = 0
    object TPage
      Left = 0
      Top = 0
      Caption = 'Replace'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 386
        Height = 175
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object leFind: TLabeledEdit
          Left = 16
          Top = 24
          Width = 353
          Height = 26
          EditLabel.Width = 160
          EditLabel.Height = 18
          EditLabel.Caption = #1053#1072#1081#1090#1080' '#1090#1077#1082#1089#1090' '#1074' '#1090#1072#1073#1083#1080#1094#1077
          TabOrder = 0
        end
        object Button2: TButton
          Left = 144
          Top = 56
          Width = 105
          Height = 25
          Caption = #1053#1072#1081#1090#1080
          Default = True
          ModalResult = 1
          TabOrder = 1
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 264
          Top = 56
          Width = 105
          Height = 25
          Caption = #1047#1072#1082#1088#1099#1090#1100
          ModalResult = 2
          TabOrder = 2
          OnClick = Button3Click
        end
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Find'
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 386
        Height = 175
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 16
          Top = 104
          Width = 112
          Height = 18
          Caption = #1055#1086#1080#1089#1082' '#1080' '#1079#1072#1084#1077#1085#1072':'
        end
        object leStart: TLabeledEdit
          Left = 16
          Top = 24
          Width = 345
          Height = 26
          EditLabel.Width = 184
          EditLabel.Height = 18
          EditLabel.Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1090#1077#1082#1089#1090' '#1074' '#1090#1072#1073#1083#1080#1094#1077
          TabOrder = 0
        end
        object leFinish: TLabeledEdit
          Left = 16
          Top = 72
          Width = 345
          Height = 26
          EditLabel.Width = 59
          EditLabel.Height = 18
          EditLabel.Caption = #1085#1072' '#1090#1077#1082#1089#1090
          TabOrder = 1
        end
        object ColumnCheck: TRadioButton
          Left = 152
          Top = 104
          Width = 113
          Height = 17
          Caption = #1074' '#1089#1090#1086#1083#1073#1094#1077
          TabOrder = 2
        end
        object TableCheck: TRadioButton
          Left = 248
          Top = 104
          Width = 89
          Height = 17
          Caption = #1074' '#1090#1072#1073#1083#1080#1094#1077
          Checked = True
          TabOrder = 3
          TabStop = True
        end
        object Button5: TButton
          Left = 16
          Top = 136
          Width = 105
          Height = 25
          Caption = #1047#1072#1084#1077#1085#1080#1090#1100
          Default = True
          TabOrder = 4
          OnClick = Button5Click
        end
        object Button6: TButton
          Left = 135
          Top = 136
          Width = 105
          Height = 25
          Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1074#1089#1077
          TabOrder = 5
          OnClick = Button6Click
        end
        object Button8: TButton
          Left = 256
          Top = 136
          Width = 105
          Height = 25
          Caption = #1047#1072#1082#1088#1099#1090#1100
          ModalResult = 2
          TabOrder = 6
          OnClick = Button8Click
        end
      end
    end
  end
end
