unit dfmAdmim;

interface

uses
  Forms, ExtCtrls, Buttons, StdCtrls, Controls, Classes, SysUtils;

type
  TfmAdmim = class(TForm)
    pnlKpp: TPanel;
    grpKpp: TGroupBox;
    pnlMove: TPanel;
    pnlReplacee: TPanel;
    lb1: TLabel;
    cbb_MoveKPPold: TComboBox;
    lb2: TLabel;
    cbb_MoveKPPnew: TComboBox;
    btnMoveOst: TButton;
    lbledtKpp: TLabeledEdit;
    cbbKPP: TComboBox;
    lblKPP: TLabel;
    btnKPP: TButton;
    tmr1: TTimer;
    tmr2: TTimer;
    cbb_DataMove: TComboBox;
    lb3: TLabel;
    pnl1: TPanel;
    GB_1: TGroupBox;
    btn4: TSpeedButton;
    btn1: TButton;
    procedure btnKPPClick(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbb_MoveKPPoldChange(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure btnMoveOstClick(Sender: TObject);
    procedure tmr2Timer(Sender: TObject);
    procedure cbb_DataMoveChange(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAdmim: TfmAdmim;
  datMove:TDateTime;

implementation
uses Global, Global2, dfmWork, dfmMove, dfmMain, globalConst, GlobalUtils;

{$R *.dfm}

procedure TfmAdmim.btn1Click(Sender: TObject);
begin
  CleanRepProducer(fmWork.que2_sql);
end;

procedure TfmAdmim.btn4Click(Sender: TObject);
var
  butsel: integer;
  ost, dataOrg:Boolean;
begin
  butsel := Show_Inf('��������� �� XML ������� �� ������ �������?', fConfirm);
  if butsel = mrOk then
    ost:=True
  else
    ost:=False;

  butsel := Show_Inf('��������� �� XML ������ �����������?', fConfirm);
  if butsel = mrOk then
    dataOrg:=True
  else
    dataOrg:=False;

  LoadFullXML(ost, dataOrg);
end;

procedure TfmAdmim.btnKPPClick(Sender: TObject);
var kpp1, kpp2:string;
begin
  if cbbKPP.ItemIndex <> -1 then
    kpp2:= cbbKPP.Items.Strings[cbbKPP.ItemIndex]
  else
    kpp2:='';
  kpp1:=lbledtKpp.Text;
  if kpp1 = kpp2 then
    begin
      Show_inf('��� ������ ����������', fError);
      exit;
    end;

  if UpdateKppBD(kpp1,kpp2,fmWork.que1_sql) then
    if UpdateKppDecl(kpp1,kpp2) then
      begin
        ChangeKvart(dBeg,dEnd);
        if fmMove <> nil then
          fmMove.ShowDataAll;
        if fmMain <> nil then
          fmMain.WriteCaptionCount;
        Show_Inf('������ ��� ��������� �������',fInfo);
      end;
end;


procedure TfmAdmim.btnMoveOstClick(Sender: TObject);
var kpp1,kpp2, dm, cKvart, cYear:string;
begin
  kpp1 := cbb_MoveKPPold.Items.Strings[cbb_MoveKPPold.ItemIndex];
  kpp2 := cbb_MoveKPPnew.Items.Strings[cbb_MoveKPPnew.ItemIndex];
  dm:=cbb_DataMove.Items.Strings[cbb_DataMove.ItemIndex];
  if datMove = 0 then
  begin
    cKvart := Copy(dm, 0, pos(nkv, dm) - 2);
    cYear := Copy(dm, Length(dm) - 8, 4);
    datMove := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
  end;
  MoveOstKPP(kpp1, kpp2, datMove);
end;

procedure TfmAdmim.cbb_DataMoveChange(Sender: TObject);
var cKvart, cYear, sKvart:string;
begin
  sKvart := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
  cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
  cYear := Copy(sKvart, Length(sKvart) - 8, 4);
  datMove := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
end;

procedure TfmAdmim.cbb_MoveKPPoldChange(Sender: TObject);
var s:string;
  idx:integer;
begin
  cbb_MoveKPPnew.Items.Clear;
  if cbb_MoveKPPold.ItemIndex <> -1 then
  begin
    AssignCBSL(cbb_MoveKPPnew,SLKPP);
    s:= cbb_MoveKPPold.Items.Strings[cbb_MoveKPPold.ItemIndex];
    idx := cbb_MoveKPPnew.Items.IndexOf(s);
    if idx <> -1 then
    begin
      cbb_MoveKPPnew.Items.Delete(idx);
      cbb_MoveKPPnew.ItemIndex := -1;
    end;
  end;
end;

procedure TfmAdmim.FormShow(Sender: TObject);
var SL:TSTringList;
begin
  SL:=TSTringList.Create;
  try
    AssignCBSL(cbbKPP,SLKPP);

    if SLKpp.Count > 1 then
    begin
      pnlMove.Visible := True;
      AssignCBSL(cbb_MoveKPPold,SLKPP);
      SL.Assign(fmMain.cbAllKvart.Items);
      AssignCBSL(cbb_DataMove,SL);
//      cbb_DataMove.Assign(fmMain.cbAllKvart.Items);
      cbb_MoveKPPold.ItemIndex := -1;
      cbb_MoveKPPnew.Items.Clear;
      if cbb_DataMove.Items.Count > 0 then
        cbb_DataMove.ItemIndex := cbb_DataMove.Items.Count - 1;
    end
    else
      pnlMove.Visible := False;
  finally
    SL.Free;
  end;
end;

procedure TfmAdmim.tmr1Timer(Sender: TObject);
begin
  if (cbb_MoveKPPold.ItemIndex <> -1) and
    (cbb_MoveKPPnew.ItemIndex <> -1) and (cbb_DataMove.ItemIndex <> -1) then
    btnMoveOst.Enabled := True
  else
    btnMoveOst.Enabled := False;
end;

procedure TfmAdmim.tmr2Timer(Sender: TObject);
begin
  if (cbbKPP.ItemIndex <> -1) and (Length(lbledtKpp.Text) = 9) then
    btnKPP.Enabled := True
  else
    btnKPP.Enabled := False;
end;

end.
