object fmCorSal: TfmCorSal
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1082#1072' '#1087#1086#1089#1090#1091#1087#1083#1077#1085#1080#1081' '#1086#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1086#1074
  ClientHeight = 316
  ClientWidth = 874
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object lbllb1: TLabel
    Left = 15
    Top = 40
    Width = 41
    Height = 19
    Caption = #1050#1055#1055':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbllb4: TLabel
    Left = 141
    Top = 8
    Width = 302
    Height = 19
    Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1091#1077#1084#1099#1081' '#1086#1090#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl1: TLabel
    Left = 15
    Top = 95
    Width = 101
    Height = 19
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 540
    Top = 95
    Width = 149
    Height = 19
    Caption = #1050#1055#1055' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl3: TLabel
    Left = 287
    Top = 40
    Width = 134
    Height = 19
    Caption = #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object cbbKPP: TComboBox
    Left = 62
    Top = 42
    Width = 145
    Height = 26
    Style = csDropDownList
    TabOrder = 0
    OnChange = cbbKPPChange
  end
  object cbbWap: TComboBox
    Left = 427
    Top = 42
    Width = 436
    Height = 26
    Style = csDropDownList
    TabOrder = 1
  end
  object leDecl: TLabeledEdit
    Left = 192
    Top = 144
    Width = 145
    Height = 26
    BevelOuter = bvNone
    Color = 16056309
    EditLabel.Width = 162
    EditLabel.Height = 18
    EditLabel.Caption = #1054#1073#1098#1077#1084' '#1074' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
    EditLabel.Color = 16056309
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -15
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentColor = False
    EditLabel.ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    LabelPosition = lpLeft
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
  object btnCor: TButton
    Left = 457
    Top = 181
    Width = 209
    Height = 35
    Caption = #1050#1086#1088#1088#1077#1082#1090#1080#1088#1086#1074#1082#1072
    Default = True
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object btnClose: TButton
    Left = 755
    Top = 268
    Width = 100
    Height = 35
    Caption = #1047#1072#1082#1088#1099#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 2
    ParentFont = False
    TabOrder = 4
  end
  object btnForm: TButton
    Left = 457
    Top = 140
    Width = 209
    Height = 35
    Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1072#1085#1085#1099#1077
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object leSal: TLabeledEdit
    Left = 192
    Top = 185
    Width = 145
    Height = 26
    BevelOuter = bvNone
    Color = clWhite
    EditLabel.Width = 162
    EditLabel.Height = 18
    EditLabel.Caption = #1054#1073#1098#1077#1084' '#1091' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
    EditLabel.Color = 16056309
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -15
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentColor = False
    EditLabel.ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    LabelPosition = lpLeft
    ParentFont = False
    TabOrder = 6
  end
  object chk1: TCheckBox
    Left = 343
    Top = 189
    Width = 97
    Height = 17
    Caption = #1053#1077#1090' '#1076#1072#1085#1085#1099#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnClick = chk1Click
  end
  object cbbKvart: TComboBox
    Left = 449
    Top = 8
    Width = 256
    Height = 26
    Style = csDropDownList
    TabOrder = 8
    OnChange = cbbKvartChange
  end
  object leDelta: TLabeledEdit
    Left = 192
    Top = 228
    Width = 145
    Height = 26
    BevelOuter = bvNone
    Color = 16056309
    EditLabel.Width = 64
    EditLabel.Height = 18
    EditLabel.Caption = #1056#1072#1079#1085#1080#1094#1072
    EditLabel.Color = 16056309
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -15
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentColor = False
    EditLabel.ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    LabelPosition = lpLeft
    ParentFont = False
    ReadOnly = True
    TabOrder = 9
  end
  object cbbsInn: TComboBox
    Left = 122
    Top = 97
    Width = 407
    Height = 26
    Style = csDropDownList
    TabOrder = 10
  end
  object cbbsKpp: TComboBox
    Left = 695
    Top = 97
    Width = 171
    Height = 26
    Style = csDropDownList
    TabOrder = 11
  end
  object tmr1: TTimer
    Left = 752
    Top = 128
  end
end
