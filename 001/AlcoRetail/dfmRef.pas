unit dfmRef;

interface

uses
  Forms, Menus, ImgList, Controls, StdCtrls, ExtCtrls, Grids, AdvObj, BaseGrid,
  AdvGrid, ComCtrls, Classes, ToolWin, GlobalConst, SysUtils, Clipbrd, AdvUtil;

type
  TfmRef = class(TForm)
    il1: TImageList;
    il3: TImageList;
    il2: TImageList;
    tlb_Decl: TToolBar;
    btn_Find: TToolButton;
    btn_AddR: TToolButton;
    btn_RefR: TToolButton;
    sep4: TToolButton;
    pgc1: TPageControl;
    ts_Prih: TTabSheet;
    SG_Product: TAdvStringGrid;
    ts_Rash: TTabSheet;
    pErrRash: TPanel;
    Label2: TLabel;
    SG_Proizv: TAdvStringGrid;
    ts_VPost: TTabSheet;
    SG_Saler: TAdvStringGrid;
    btn_CopyR: TToolButton;
    btn_EditR: TToolButton;
    Panel1: TPanel;
    Label1: TLabel;
    Button1: TButton;
    pmSG: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    mni_RefSal: TMenuItem;
    btn1: TToolButton;
    ts1: TTabSheet;
    SG_Prod: TAdvStringGrid;
    btn_Imp: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure SG_GetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
    procedure SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
      FriendlyName: string; var FilterCondition: string);
    procedure btn_AddRClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure btn_CopyRClick(Sender: TObject);
    procedure SG_ProductGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure SG_ProductCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure SG_SalerCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure SG_ProductSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure SG_SalerSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btn_FindClick(Sender: TObject);
    procedure SG_SalerGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure btn_EditRClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_RefRClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure SG_MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure mni_RefSalClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn_ImpClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TypeRef:TTypeRef;
  end;

var
  fmRef: TfmRef;
  colMouse, rowMouse:integer;



implementation

uses Global,Global2, GlobalUtils, dfmQuery, dfmReplace, dfmWork, dfmMain,
  dfmImportRefDBF,  dfmMove, about, dfmDataExchange;
{$R *.dfm}

procedure TfmRef.btn1Click(Sender: TObject);
begin
  if fmImportRefDBF = nil then
    fmImportRefDBF := TfmImportRefDBF.Create(Application);
  fmImportRefDBF.pgcDBF.ActivePageIndex := 0;

  fmWork.dlgOpen_Import.Title := '�������� ���� �����������';
  fmWork.dlgOpen_Import.Filter := '���� dBase (*.dbf)|*.dbf';
  fmWork.dlgOpen_Import.DefaultExt := fmWork.dlgOpen_Import.Filter;
  if fmWork.dlgOpen_Import.Execute then
    if (Length(fmWork.dlgOpen_Import.FileName)>0) and FileExists(fmWork.dlgOpen_Import.FileName) then
      with fmImportRefDBF do
        begin
          Caption := fmWork.dlgOpen_Import.FileName;
          DBFFileName:=fmWork.dlgOpen_Import.FileName;
          if DbfIsREF(DBFFileName) then
            ShowModal
          else
            Show_Inf('�������� ��������� �����.' + #10#13 +
               '�������� �� ������� ���� �������.' + #10#13 +
               '�������� ������ DBF ����.',fError);
        end;
end;

procedure TfmRef.btn_AddRClick(Sender: TObject);
var decl:TDeclaration;
begin
  TypeRef:=fNewRef;
  if fmQuery = nil then
    fmQuery := TfmQuery.Create(Application);
  FormQuery(pgc1.ActivePageIndex, fAdd, decl);
end;

procedure TfmRef.btn_CopyRClick(Sender: TObject);
var
  decl: TDeclaration;
begin
  try
    TypeRef:=fCopyRef;
    if fmQuery = nil then
      fmQuery := TfmQuery.Create(Application);
    decl.product.prod.pInn := ShortString(SG_Product.Cells[8, SG_Product.Row]);
    decl.product.prod.pKpp := ShortString(SG_Product.Cells[9, SG_Product.Row]);
    decl.product.EAN13 := ShortString(SG_Product.Cells[2, SG_Product.Row]);
    decl.product.FNSCode := ShortString(SG_Product.Cells[3, SG_Product.Row]);
    decl.product.Productname := ShortString(SG_Product.Cells[4, SG_Product.Row]);
    decl.product.AlcVol := StrToFloat(SG_Product.Cells[5, SG_Product.Row]);

    GetProducer(fmWork.que2_sql, decl.product.prod, False, False);
    FormQuery(pgc1.ActivePageIndex, fCopy, decl);
  finally
  end;
end;

procedure TfmRef.btn_EditRClick(Sender: TObject);
var
  decl: TDeclaration;
begin
  try
    if fmQuery = nil then
      fmQuery := TfmQuery.Create(Application);
    fmRef.TypeRef:=fEditRef;
    decl.sal.sName:=ShortString(SG_Saler.Cells[2,SG_Saler.Row]);
    decl.sal.sInn:=ShortString(SG_Saler.Cells[3,SG_Saler.Row]);
    decl.sal.sKpp:=ShortString(SG_Saler.Cells[4,SG_Saler.Row]);
    decl.sal.sAddress:=ShortString(SG_Saler.Cells[5,SG_Saler.Row]);
    decl.sal.lic.ser:=ShortString(SG_Saler.Cells[6,SG_Saler.Row]);
    decl.sal.lic.num:=ShortString(SG_Saler.Cells[7,SG_Saler.Row]);
    if SG_Saler.Cells[8,SG_Saler.Row] <> '' then
      decl.sal.lic.dBeg:=StrToDate(SG_Saler.Cells[8,SG_Saler.Row])
    else
      decl.sal.lic.dBeg := dBeg;
    if SG_Saler.Cells[9,SG_Saler.Row] <> '' then
      decl.sal.lic.dEnd:=StrToDate(SG_Saler.Cells[9,SG_Saler.Row])
    else
      decl.sal.lic.dEnd := dEnd;
    decl.sal.lic.RegOrg:=ShortString(SG_Saler.Cells[10,SG_Saler.Row]);
    FormQuery(pgc1.ActivePageIndex, fEdit, decl);
    ShowRefSaler(SG_Saler, fmWork.que2_sql);
  finally
  end;
end;

procedure TfmRef.btn_FindClick(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  case pgc1.ActivePageIndex of
    0:fmReplace.SGFind:=SG_Product;
    1:fmReplace.SGFind:=SG_Saler;
    3:fmReplace.SGFind:=SG_Prod;
  end;
  fmReplace.Show;
end;

procedure TfmRef.btn_ImpClick(Sender: TObject);
var SG:TAdvStringGrid;
begin
  case pgc1.ActivePageIndex of
    0: SG := SG_Product;
    1: SG := SG_Saler;
    3: SG := SG_Prod;
  end;
  fmWork.dlgSave_Export.Filter := '������� � XLS|*.xls';
  if fmWork.dlgSave_Export.Execute then
    SaveXLS(fmWork.dlgSave_Export.FileName, SG,20);
end;

procedure TfmRef.btn_RefRClick(Sender: TObject);
begin
  case pgc1.ActivePageIndex of
    0:LocRefProduction(fmWork.que1_sql, fmWork.que2_sql, fmWork.pbWork);
    1:LocRefSaler(fmWork.que1_sql, fmWork.que2_sql, fmWork.pbWork);
  end;
  UpdateDateRef;
  ShowRefProduction(SG_Product, fmWork.que2_sql);
  ShowRefSaler(SG_Saler, fmWork.que2_sql);
  ShowRefProducer(SG_Prod, fmWork.que2_sql);
  fmWork.pbWork.Position:=0;
  fmWork.pbWork.UpdateControlState;
end;

procedure TfmRef.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmRef.SG_GetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
var
  ColFilter: Array of Integer;
begin
  try
    if (Sender as TAdvStringGrid).RowCount = (Sender as TAdvStringGrid)
      .FixedRows + (Sender as TAdvStringGrid).FixedFooters then
    begin
      exit;
    end;
    case pgc1.ActivePageIndex of
      0:
        begin
          SetLength(ColFilter, High(FilterColProdRef) + 1);
          Move(FilterColProdRef[0], ColFilter[0],
            sizeOf(Integer) * Length(FilterColProdRef));
        end;
      1:
        begin
          SetLength(ColFilter, High(FilterColSalRef) + 1);
          Move(FilterColSalRef[0], ColFilter[0],
            sizeOf(Integer) * Length(FilterColSalRef));
        end;
    end;
    if IndexMas(ColFilter, Column) = -1 then
      Filter.Clear;
  finally
  end;
end;

procedure TfmRef.SG_ProductCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  if ACol = 1 then CanEdit:=False;
end;

procedure TfmRef.SG_ProductGetEditorType(Sender: TObject; ACol, ARow: Integer;
  var AEditor: TEditorType);
begin
  case ACol of
    1:
      AEditor := edCheckBox;
  end;
end;

procedure TfmRef.SG_ProductSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var ch:Boolean;
begin
  ch:=SG_Product.GetCheckBoxState(1,ARow,ch);
  //btn_RefR.Enabled := ch;
end;

procedure TfmRef.SG_SalerCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
begin
  if ACol = 1 then CanEdit:=False;
end;

procedure TfmRef.SG_SalerGetEditorType(Sender: TObject; ACol, ARow: Integer;
  var AEditor: TEditorType);
begin
  case ACol of
    1:
      AEditor := edCheckBox;
    8,9:
      AEditor := edDateEdit;
  end;
end;

procedure TfmRef.SG_MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (Sender as TAdvStringGrid).MouseToCell(X, Y, colMouse, rowMouse);
end;

procedure TfmRef.SG_SalerSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var ch:Boolean;
begin
  ch:=SG_Saler.GetCheckBoxState(1,ARow,ch);
  //btn_RefR.Enabled := ch;
end;

procedure TfmRef.SG_FilterSelect(Sender: TObject; Column, ItemIndex: Integer;
  FriendlyName: string; var FilterCondition: string);

begin
  case pgc1.ActivePageIndex of
    0:
      if IndexMas(FilterColPrih, Column) = -1 then
        exit;
    1:
      if IndexMas(FilterColBrak, Column) = -1 then
        exit;
  end;
  if FilterCondition = '��������' then
    FilterCondition := '';
end;

procedure TfmRef.FormCreate(Sender: TObject);
begin
  TypeRef:=FNoneRef;
end;

procedure TfmRef.FormShow(Sender: TObject);
var b:Boolean;
begin
  if SG_Product.RowCount > SG_Product.FixedRows + SG_Product.FixedRows then
    SG_ProductSelectCell(SG_Product, 1, SG_Product.FixedRows, b);
  ShowRefProduction(SG_Product, fmWork.que2_sql);
  ShowRefSaler(SG_Saler, fmWork.que2_sql);
  ShowRefProducer(SG_Prod, fmWork.que2_sql);
end;

procedure TfmRef.mni_RefSalClick(Sender: TObject);
var SLSal:TStringList;
begin
  SLSal:=TStringList.Create;
  try
    if fmDataExchange = nil then
      fmDataExchange := TfmDataExchange.Create(Application);
    fmDataExchange.SG_Query.ClearNormalCells;
    fmDataExchange.SG_Query.RowCount := fmDataExchange.SG_Query.FixedRows +
      fmDataExchange.SG_Query.FixedFooters;

    SLSal.Add(SG_Saler.Cells[2,RowMouse]);
    SLSal.Add(SG_Saler.Cells[3,RowMouse]);
    SLSal.Add(SG_Saler.Cells[4,RowMouse]);

    UpdateServerProductionSaler(fmWork.que2_sql,SLSal);
  finally
    SLSal.Free;
  end;
end;

procedure TfmRef.N1Click(Sender: TObject);
var Ean13, pk:string;
begin
  case pgc1.ActivePageIndex of
    0: begin
      EAN13:=SG_Product.AllCells[2,RowMouse];
      DeleteProduction(EAN13, fmWork.que2_sql,SG_Product.Rows[RowMouse]);
    end;
    1: begin
      pk := SG_Saler.AllCells[SG_Saler.AllColCount-1,RowMouse];
      DeleteSaler(PK, fmWork.que2_sql, SG_Saler.Rows[RowMouse]);
    end;
  end;
  // ���������� ������� ��� �������� ��������
  FilterData(DBeg, dEnd, masErr, SLOborotKpp);
  if fmMain <> nil then
    fmMain.WriteCaptionCount;
end;

procedure TfmRef.N2Click(Sender: TObject);
var
    s: string;
  begin
    s := '';
    case pgc1.ActivePageIndex of
      0:  s := CopyCell(SG_Product);
      1:   s := CopyCell(SG_Saler);
    end;
    Clipboard.AsText := s;
end;

procedure TfmRef.N4Click(Sender: TObject);
var Ean13, pk:string;
begin
  case pgc1.ActivePageIndex of
    0: begin
      EAN13:=SG_Product.AllCells[2,RowMouse];
      UpdateServerProduction(EAN13, fmWork.que2_sql,SG_Product.Rows[RowMouse]);
    end;
    1: begin
      pk := SG_Saler.AllCells[SG_Saler.AllColCount-1,RowMouse];
      UpdateServerSaler(PK, fmWork.que2_sql, SG_Saler.Rows[RowMouse]);
    end;
  end;
  // ���������� ������� ��� �������� ��������
  FilterData(DBeg, dEnd, masErr, SLOborotKpp);
  if fmMain <> nil then
    fmMain.WriteCaptionCount;
  Show_Inf('���������� ����������� ���������', fInfo);
end;

procedure TfmRef.pgc1Change(Sender: TObject);
var b:Boolean;
begin
  btn1.Enabled := True;

  case pgc1.ActivePageIndex of
    0: begin
      btn_CopyR.Enabled := True;
      btn_EditR.Enabled := False;
      mni_RefSal.Enabled := False;
      btn_Find.Enabled := True;
      btn_AddR.Enabled := True;
      btn_CopyR.Enabled := True;
      btn_EditR.Enabled := False;
      if SG_Product.RowCount > SG_Product.FixedRows + SG_Product.FixedRows then
      begin
        SG_Product.Col := 2;
        SG_Product.Row := SG_Product.FixedRows;
        SG_ProductSelectCell(SG_Product, 1, SG_Product.FixedRows, b);
      end;
    end;
    1: begin
      btn_CopyR.Enabled := False;
      btn_EditR.Enabled := True;
      mni_RefSal.Enabled := True;
      btn_Find.Enabled := True;
      btn_AddR.Enabled := True;
      btn_CopyR.Enabled := False;
      btn_EditR.Enabled := True;

      if SG_Saler.RowCount > SG_Saler.FixedRows + SG_Saler.FixedRows then
      begin
        SG_Saler.Col := 2;
        SG_Saler.Row := SG_Saler.FixedRows;
        SG_SalerSelectCell(SG_Saler, 1, SG_Saler.FixedRows, b);
      end;
    end;
    3: begin
      btn_Find.Enabled := True;
      btn_AddR.Enabled := False;
      btn1.Enabled := False;
      btn_CopyR.Enabled := False;
      btn_EditR.Enabled := False;
    end;
  end;
end;

end.
