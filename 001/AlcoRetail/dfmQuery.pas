unit dfmQuery;

interface

uses
  Forms, ExtCtrls, StdCtrls, ComCtrls, Controls, Classes, SysUtils, ADODB,
  Graphics;

type
  TfmQuery = class(TForm)
    nb: TNotebook;
    Panel2: TPanel;
    le1_NameOrg: TLabeledEdit;
    le0_EAN: TLabeledEdit;
    le1_Inn: TLabeledEdit;
    le0_AlcVol: TLabeledEdit;
    le1_KPP: TLabeledEdit;
    le0_Capacity: TLabeledEdit;
    le1_adress: TLabeledEdit;
    le0_INN: TLabeledEdit;
    le1_num: TLabeledEdit;
    le0_NameProd: TLabeledEdit;
    le0_Adress: TLabeledEdit;
    le1_Lic: TLabeledEdit;
    le0_Producer: TLabeledEdit;
    dtp1_end: TDateTimePicker;
    dtp1_beg: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    Button2: TButton;
    Button4: TButton;
    cb0_Wap: TComboBox;
    Label3: TLabel;
    btSave: TButton;
    btCancel: TButton;
    Panel1: TPanel;
    Label4: TLabel;
    le1_org: TLabeledEdit;
    Timer1: TTimer;
    cb_beer: TCheckBox;
    Label5: TLabel;
    Label7: TLabel;
    cbKPP: TComboBox;
    GroupBox1: TGroupBox;
    cbFullName: TComboBox;
    leFiltr: TLabeledEdit;
    procedure btSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure le0_EANChange(Sender: TObject);
    procedure le0_INNChange(Sender: TObject);
    procedure le1_InnChange(Sender: TObject);
    procedure le1_KPPChange(Sender: TObject);
    procedure le0_KPPChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cb_beerClick(Sender: TObject);
    procedure le0_AlcVolKeyPress(Sender: TObject; var Key: Char);
    procedure le0_CapacityKeyPress(Sender: TObject; var Key: Char);
    procedure cbFullNameChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure leFiltrChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmQuery: TfmQuery;
  FNSCOCO: string;
  Err1: Boolean;
  SLProd: TStringList;
  StrSQLS: string;

implementation

uses dfmDataExchange, Global, Global2, GlobalUtils, GlobalConst, dfmWork, dfmRef,
  dfmMain, dfmMove;
{$R *.dfm}

procedure TfmQuery.btSaveClick(Sender: TObject);
var
  idx: integer;
  rez:Boolean;
begin
  IsConnect:=False;
  if ModalResult = mrOK then
  begin
    fmDataExchange.SG_Query.RowCount := fmDataExchange.SG_Query.RowCount + 1;
    case nb.PageIndex of
      0:
        begin
          SetLength(RefsAddEAN, 10, 0);
          idx := cb0_Wap.ItemIndex;
          if idx = -1 then
            exit;
          FNSCOCO := IntToStr(LongInt(cb0_Wap.Items.Objects[idx]));
          Application.ProcessMessages;
          fmDataExchange.SG_Query.Cells[1,
            fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
            cAddEAN + le0_EAN.Text;
          SetLength(RefsAddEAN, Length(RefsAddEAN), Length(RefsAddEAN[0]) + 1);
          RefsAddEAN[0, Length(RefsAddEAN[0]) - 1]:=FNSCOCO;
          if (le0_EAN.Text = '500') or (le0_EAN.Text = '510') or
            (le0_EAN.Text = '520') then
            RefsAddEAN[1, Length(RefsAddEAN[0]) - 1] :=
              le0_EAN.Text + le0_NameProd.Text
          else
            RefsAddEAN[1, Length(RefsAddEAN[0]) - 1] :=
              FNSCOCO + le0_NameProd.Text;
          RefsAddEAN[2, Length(RefsAddEAN[0]) - 1] := le0_EAN.Text;
          RefsAddEAN[3, Length(RefsAddEAN[0]) - 1] := le0_AlcVol.Text;
          if (le0_EAN.Text = '500') or (le0_EAN.Text = '510') or
            (le0_EAN.Text = '520') then
            RefsAddEAN[4, Length(RefsAddEAN[0]) - 1] := '1'
          else
            RefsAddEAN[4, Length(RefsAddEAN[0]) - 1] := le0_Capacity.Text;
          RefsAddEAN[5, Length(RefsAddEAN[0]) - 1] := le0_Producer.Text;
          RefsAddEAN[6, Length(RefsAddEAN[0]) - 1] := le0_INN.Text;
          RefsAddEAN[7,Length(RefsAddEAN[0])-1]:=cbKPP.Text;
          RefsAddEAN[8, Length(RefsAddEAN[0]) - 1] := le0_Adress.Text;
          RefsAddEAN[9, Length(RefsAddEAN[0]) - 1] := le0_Adress.Text;
        end;
      1:
        begin
          SetLength(RefsAddINN, 11, 0);
          fmDataExchange.SG_Query.Cells[1,
            fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
            '���������� ��� ' + le1_Inn.Text + '/��� ' + le1_KPP.Text;
          SetLength(RefsAddINN, Length(RefsAddINN), Length(RefsAddINN[0]) + 1);
          RefsAddINN[0, Length(RefsAddINN[0]) - 1]:= le1_NameOrg.Text;
          RefsAddINN[1, Length(RefsAddINN[0]) - 1] := 'SLR' + le1_NameOrg.Text;
          RefsAddINN[2, Length(RefsAddINN[0]) - 1] := le1_Inn.Text;
          RefsAddINN[3, Length(RefsAddINN[0]) - 1] := le1_KPP.Text;
          RefsAddINN[4, Length(RefsAddINN[0]) - 1] := le1_adress.Text;
          RefsAddINN[5, Length(RefsAddINN[0]) - 1] := le1_adress.Text;
          RefsAddINN[6, Length(RefsAddINN[0]) - 1] := le1_Lic.Text;
          RefsAddINN[7, Length(RefsAddINN[0]) - 1] := le1_num.Text;
          RefsAddINN[8, Length(RefsAddINN[0]) - 1] := Copy
            (DateTimeToStr(dtp1_beg.Datetime), 1, 10);
          RefsAddINN[9, Length(RefsAddINN[0]) - 1] := Copy
            (DateTimeToStr(dtp1_end.Datetime), 1, 10);
          RefsAddINN[10, Length(RefsAddINN[0]) - 1] := le1_org.Text;
        end;
    end;

    case fmRef.TypeRef of
      fNewRef:
        begin
          try
            if ExistConnectServer then
              begin
                fmWork.sb_DataExchangeClick(Application);
                fmDataExchange.SG_Query.Cells[2, fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
                  '������ �� ���������� �����������';
                fmDataExchange.SG_Query.Objects[2, fmDataExchange.SG_Query.RowCount - fmDataExchange.SG_Query.FixedRows - fmDataExchange.SG_Query.fixedfooters] :=
                  Pointer(1);
                fmDataExchange.Enabled := False;
                ConnectServer(fmDataExchange.SG_Query,BMHost, fOrg.inn, exe_version );
                sleep(1000);
                fmDataExchange.btn_ConnectClick(Sender);
                fmDataExchange.Enabled := True;
                rez:=True;
              end;
          except
            case nb.PageIndex of
              0: rez:=InsertEanLocal(fmWork.que2_sql);
              1: rez:=InsertInnLocal(fmWork.que2_sql);
            end;
          end;
        end;
      fCopyRef:
        begin
          case nb.PageIndex of
            0: rez:=InsertEanLocal(fmWork.que2_sql);
            1: rez:=InsertInnLocal(fmWork.que2_sql);
          end;
        end;
      fEditRef:
        begin
          case nb.PageIndex of
            1: rez:=EditLicLocal(fmWork.que2_sql);
          end;
        end;
    end;

    if rez then
      begin
        ChangeKvart(DBeg, dEnd);
        if fmMove <> nil then
          fmMove.ShowDataAll;
        if fmMain <> nil then
          fmMain.WriteCaptionCount;
        Show_Inf('���������� ��������', fInfo);
      end
    else
      Show_Inf('������ ���������� �����������', fError)
  end;
end;

procedure TfmQuery.cbFullNameChange(Sender: TObject);
var
  p: integer;
  StrSQL, s, sName, sInn: string;
  SL: TStringList;
  qr: TADOQuery;
begin
  qr := TADOQuery.Create(fmWork);
  try

    qr.Connection := fmWork.con2ADOCon;
    SL := TStringList.Create;
    if Sender is TComboBox then
      if (Sender as TComboBox).ItemIndex <> -1 then
      begin
        s := (Sender as TComboBox).Items.Strings
          [(Sender as TComboBox).ItemIndex];
        p := Pos('-', s);
        sName := Copy(s, p + 2, Length(s) - p - 1);
        sInn := Copy(s, 0, p - 2);
        StrSQL :=
          'SELECT KPP, ADDRESS FROM producer WHERE INN=' + '''' + sInn + ''' ORDER BY KPP';

        FillSLKpp(qr, StrSQL, SL);
        AssignCBSL(cbKPP, SL);
        if cbKPP.Items.Count = 1 then
          cbKPP.ItemIndex := 0;
        le0_INN.Text := sInn;
        le0_Producer.Text := sName;
        le0_Adress.Text := TStrObj(cbKPP.Items.Objects[cbKPP.ItemIndex]).Data;
      end;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmQuery.cb_beerClick(Sender: TObject);
begin
  if (Sender as TCheckBox).Checked then
  begin
    le1_Lic.Text := '���';
    le1_num.Text := '���';
    le1_org.Text := '���';
    dtp1_beg.Date := StrToDateTime('01.01.2010');
    dtp1_end.Date := StrToDateTime('31.12.2016');
  end
  else
  begin
    le1_Lic.Text := '';
    le1_num.Text := '';
    le1_org.Text := '';
    dtp1_beg.Date := Date();
    dtp1_end.Date := Date();
  end;
end;

procedure TfmQuery.FormCreate(Sender: TObject);
begin
  cb0_Wap.Clear;
  FillComboFNS(cb0_Wap, FNSCode, True);

  SLProd := TStringList.Create;
  StrSQLS := '';

  SetLength(RefsAddEAN, 10, 0);
  SetLength(RefsAddINN, 11, 0);
end;

procedure TfmQuery.FormDestroy(Sender: TObject);
var
  i: integer;
begin
  SLProd.Free;
  if cbKPP.Items.Count > 0 then
    with cbKPP.Items do
      for i := 0 to cbKPP.Items.Count - 1 do
        if Assigned(Objects[0]) then
          begin
            Objects[0].Free;
            Delete(0);
          end;
end;

procedure TfmQuery.FormShow(Sender: TObject);
var
  StrSQL, s, sInn, sName, sKPP: string;
  i, idx, p: integer;
  SL: TStringList;
  qr: TADOQuery;
begin
  try
    try
      StrSQLS := StrSQL_Producer;
      qr := TADOQuery.Create(fmWork);
      qr.Connection := fmWork.con2ADOCon;
      FillSLSQL(qr, StrSQLS, SLProd);
      AssignCBSL(cbFullName, SLProd);
    finally
      qr.Free;
    end;
    SL := TStringList.Create;
    qr := TADOQuery.Create(fmWork);
    qr.Connection := fmWork.con2ADOCon;
    sInn := le0_INN.Text;
    sName := le0_Producer.Text;
    s := sInn + ' - ' + sName;
//    idx := SLSaler.IndexOf(s);
    if idx <> -1 then
    begin
      cbFullName.ItemIndex := idx;
      s := cbFullName.Items.Strings[idx];
      p := Pos('-', s);
      sName := Copy(s, p + 2, Length(s) - p - 1);
      sInn := Copy(s, 0, p - 2);
      StrSQL := 'SELECT ORGKPP FROM SALER WHERE ORGINN=' + '''' + sInn + '''';
      FillSLSQL(qr, StrSQL, SL);
      AssignCBSL(cbFullName, SLProd);
      sKPP := cbKPP.Text;
      i := cbKPP.Items.IndexOf(sKPP);
      if i <> -1 then
        cbKPP.ItemIndex := i;
    end;
    // le0_INN.Text:='';
     cbFullName.ItemIndex:=-1;
     cb_beer.Checked :=True;
    // cbKPP.ItemIndex:=-1;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmQuery.le0_AlcVolKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key <> #8) then
  begin
    if ((Key < '0') or (Key > '9')) and (Key <> '.') and (Key <> ',') then
      Key := #0;
    if ((Pos('.', (Sender as TLabeledEdit).Text) > 0) or (Pos(',',
          (Sender as TLabeledEdit).Text) > 0)) and
      ((Key = '.') or (Key = ',')) then
      if (Pos('.', (Sender as TLabeledEdit).SelText) = 0) and
        (Pos(',', (Sender as TLabeledEdit).SelText) = 0) then
        Key := #0;
  end;
end;

procedure TfmQuery.le0_CapacityKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key <> #8) then
  begin
    if ((Key < '0') or (Key > '9')) and (Key <> '.') and (Key <> ',') then
      Key := #0;
    if ((Pos('.', (Sender as TLabeledEdit).Text) > 0) or (Pos(',',
          (Sender as TLabeledEdit).Text) > 0)) and
      ((Key = '.') or (Key = ',')) then
      if (Pos('.', (Sender as TLabeledEdit).SelText) = 0) and
        (Pos(',', (Sender as TLabeledEdit).SelText) = 0) then
        Key := #0;
  end;
end;

procedure TfmQuery.le0_EANChange(Sender: TObject);
var
  s: string;
  b1: Boolean;
begin
  s := (Sender as TLabeledEdit).Text;
  if not EANCheck(s, b1) then
  begin (Sender as TLabeledEdit)
    .Color := clCheck;
    Err1 := True;
  end
  else
  begin (Sender as TLabeledEdit)
    .Color := clWindow;
    Err1 := False;
  end;
end;

procedure TfmQuery.le0_INNChange(Sender: TObject);
var
  s: string;
  b1: Boolean;
begin
  s := (Sender as TLabeledEdit).Text;
  If (not INNCheck(s, b1)) and (not INN12Check(s, b1)) then
  begin (Sender as TLabeledEdit)
    .Color := clCheck;
    Err1 := True;
  end
  else
  begin (Sender as TLabeledEdit)
    .Color := clWindow;
    Err1 := False;
  end;
end;

procedure TfmQuery.le0_KPPChange(Sender: TObject);
var
  s: string;
begin
  s := (Sender as TLabeledEdit).Text;
  If (not StrCheck(s)) or (Length(s) <> 9) or (s = '000000000') then
  begin (Sender as TLabeledEdit)
    .Color := clCheck;
    Err1 := True;
  end
  else
  begin (Sender as TLabeledEdit)
    .Color := clWindow;
    Err1 := False;
  end;
end;

procedure TfmQuery.le1_InnChange(Sender: TObject);
var
  s: string;
  b1: Boolean;
begin
  s := (Sender as TLabeledEdit).Text;
  If (not INNCheck(s, b1)) and (not INN12Check(s, b1)) then
  begin (Sender as TLabeledEdit)
    .Color := clCheck;
    Err1 := True;
  end
  else
  begin (Sender as TLabeledEdit)
    .Color := clWindow;
    Err1 := False;
  end;
end;

procedure TfmQuery.le1_KPPChange(Sender: TObject);
var
  s: string;
begin
  s := (Sender as TLabeledEdit).Text;
  If (not StrCheck(s)) or (Length(s) <> 9) or (s = '000000000') then
  begin (Sender as TLabeledEdit)
    .Color := clCheck;
    Err1 := True;
  end
  else
  begin (Sender as TLabeledEdit)
    .Color := clWindow;
    Err1 := False;
  end;
end;

procedure TfmQuery.leFiltrChange(Sender: TObject);
var
  s, s1, s2: string;
//  b1: Boolean;
  i: integer;
begin
  s := (Sender as TLabeledEdit).Text;

  s1 := AnsiUpperCase(s);
  cbFullName.Clear;
  le0_Producer.Text:='';
  le0_INN.Text:='';
  for i := 0 to cbKPP.Items.Count - 1 do
    begin
      cbKPP.Items.Objects[0].Free;
      cbKPP.Items.Objects[0]:=nil;
      cbKPP.Items.Delete(0);
    end;
  cbKPP.Text:='';
  le0_Adress.Text :='';

  if s <> '' then
    for i := 0 to SLProd.Count - 1 do
    begin
      s2 := AnsiUpperCase(SLProd.Strings[i]);
      if Pos(s1, s2) > 0 then
        cbFullName.Items.AddObject(SLProd.Strings[i], SLProd.Objects[i]);
    end;
  if s = '' then
    for i := 0 to SLProd.Count - 1 do
      cbFullName.Items.AddObject(SLProd.Strings[i], SLProd.Objects[i]);
  if cbFullName.Items.Count > 0 then
  begin
    cbFullName.ItemIndex := 0;
    cbFullNameChange(cbFullName);
  end;
end;

procedure TfmQuery.Timer1Timer(Sender: TObject);
var
  le_err, cb_err: Boolean;
begin
  le_err := False;
  cb_err := False;
  case nb.PageIndex of
    0:
      begin
        if (le0_NameProd.Text = '') or (le0_EAN.Text = '') or
          (le0_AlcVol.Text = '') or (le0_Capacity.Text = '') or
          (le0_Producer.Text = '') or (le0_INN.Text = '') or
          (Length(cbKPP.Text) <> 9) or
          (le0_Adress.Text = '') then
          le_err := True;
        if cb0_Wap.ItemIndex = -1 then
          cb_err := True;
      end;
    1:
      if (le1_NameOrg.Text = '') or (le1_Inn.Text = '') or (le1_KPP.Text = '')
        or (le1_adress.Text = '') or (le1_Lic.Text = '') or (le1_num.Text = '')
        or (le1_org.Text = '') then
        le_err := True;
  end;
  if (not le_err) and (not Err1) and (not cb_err) then
    btSave.Enabled := True
  else
    btSave.Enabled := False;
end;


end.
