unit dfmJournal;

interface

uses
  Forms, ComCtrls, Controls, StdCtrls, Buttons, Classes, ExtCtrls, AdvGrid,
  SysUtils, ShlObj, Windows;

type
  TfmJournal = class(TForm)
    pnl1: TPanel;
    lb1: TLabel;
    lblVer: TLabel;
    btn1: TButton;
    pnl2: TPanel;
    pnl3: TPanel;
    pnl4: TPanel;
    sb_print: TSpeedButton;
    sb_excel: TSpeedButton;
    pgc_KPP: TPageControl;
    pnl_pnl5: TPanel;
    lb_lb2: TLabel;
    cbAllKvart: TComboBox;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sb_printClick(Sender: TObject);
    procedure sb_excelClick(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmJournal: TfmJournal;
  jKvart, jYear:Integer;
  jBeg, jEnd:TDateTime;

implementation

uses dfmWork, Global, Global2, GlobalUtils, GlobalConst, dfmProgress,
  dfmSelAllKPP;

{$R *.dfm}

procedure TfmJournal.btn1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmJournal.cbAllKvartChange(Sender: TObject);
var i:Integer;
  SG:TAdvStringGrid;
  tSh:TTabSheet;
  sKvart, cKvart, cYear:string;
begin
  sKvart := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
  cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
  cYear := Copy(sKvart, Length(sKvart) - 8, 4);
  jKvart := StrToInt(cKvart);
  jYear := StrToInt(cYear);
  jBeg := Kvart_Beg(jKvart, jYear);
  jEnd := Kvart_End(jKvart, jYear);
  if pgc_KPP.PageCount = SLKPP.Count then
    for i := 0 to pgc_KPP.PageCount - 1 do
    begin
      tSh := pgc_KPP.Pages[i];
      if tSh.ControlCount > 0 then
        if tSh.Controls[0] is TAdvStringGrid then
        begin
          SG := tSh.Controls[0] as TAdvStringGrid;
          SG.ClearAll;
          Shapka_SGJournal(SG);
          SG.RowCount := SG.FixedRows + SG.FixedFooters + 1;
        end;
    end;
end;

procedure TfmJournal.FormCreate(Sender: TObject);
var SG:TAdvStringGrid;
  i, idx:Integer;
  tSh:TTabSheet;
  s :string;
  sKvart, cKvart, cYear:string;
begin
  AssignCBSL(cbAllKvart, SLPerRegYear);
  cbAllKvart.Items.Delete(0);
  s := IntToStr(CurKvart) + ' ' + nkv + ' ' + IntToStr(CurYear) + ' ' + ng;
  idx := cbAllKvart.Items.IndexOf(s);
  if idx <> -1 then
    begin
      cbAllKvart.ItemIndex := idx;
      sKvart := cbAllKvart.Items.Strings[idx];
      cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
      cYear := Copy(sKvart, Length(sKvart) - 8, 4);
      jKvart := StrToInt(cKvart);
      jYear := StrToInt(cYear);
      jBeg := Kvart_Beg(jKvart, jYear);
      jEnd := Kvart_End(jKvart, jYear);
    end;
  for i := 0 to SLKPP.Count - 1 do
  begin
    tSh:= TTabSheet.Create(pgc_KPP);
    tSh.Name := 'tSh_' + IntToStr(i);
    with tSh do
      begin
        Caption :='��� - '+SLKPP[i];
        pagecontrol := pgc_KPP;
        SG:=TAdvStringGrid.Create(tSh);
        SG.Name := 'SG_' + IntToStr(i);
        SG.Parent:=tSh;
        SG.Align := alClient;
        SG.FixedFooters := 1;
        SG.FloatingFooter.Visible := True;
        SG.ShowHint := True;
        SG.HintShowCells := True;
        SG.HintShowLargeText := True;
        SG.FloatFormat := '%.5f';
        Shapka_SGJournal(SG);
        tSh.TabVisible := False;
      end;
  end;
end;

procedure TfmJournal.sb_excelClick(Sender: TObject);
var i, CountVisTab:Integer;
  KPP:string;
  tSh:TTabSheet;
  SG:TAdvStringGrid;
  BrowseInfo: TBrowseInfo;
  DisplayName, TempPath: array [0 .. MAX_PATH] of char;
  TitleName, Path: string;
  lpItemID: PItemIDList;
begin
  try
    CountVisTab:=0;
    if pgc_KPP.PageCount <> SLKPP.Count then Exit;

    for i := 0 to pgc_KPP.PageCount - 1 do
    begin
      tSh := pgc_KPP.Pages[i];
      if tSh.TabVisible then
        Inc(CountVisTab);
    end;

    if CountVisTab > 0 then
    begin
      FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
      BrowseInfo.hwndOwner := fmWork.Handle;
      BrowseInfo.pszDisplayName := @DisplayName;
      TitleName := '�������� ����� ��� ���������� �������';
      BrowseInfo.lpszTitle := PChar(TitleName);
      BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
      lpItemID := SHBrowseForFolder(BrowseInfo);
      if lpItemID <> nil then
      begin
        SHGetPathFromIDList(lpItemID, TempPath);
        GlobalFreePtr(lpItemID);
        Path := TempPath;

        if fmProgress = nil then
          fmProgress := TfmProgress.Create(Application);
        fmProgress.Show;
        fmProgress.pb.Max := CountVisTab;
        fmProgress.pb.Min := 0;
        fmProgress.pb.Step := 1;
        Application.ProcessMessages;

        for i := 0 to pgc_KPP.PageCount - 1 do
        begin
          Kpp := SLKPP.Strings[i];
          fmProgress.labAbout.Caption := '�������������� ������� ��� ��� ' + KPP;
          tSh := pgc_KPP.Pages[i];

          if tSh.ControlCount > 0 then
            if tSh.Controls[0] is TAdvStringGrid then
              if tSh.TabVisible then
                begin
                  fmProgress.pb.StepIt;
                  Application.ProcessMessages;
                  SG := tSh.Controls[0] as TAdvStringGrid;
                  ImportJournalKPP(jKvart, jYear, Path, SG, Kpp);
                end;
        end;
        fmProgress.pb.Position := 0;
      end;
    end
    else
      Show_Inf('������� ���������� ������������ ������', fError);

  finally
    fmProgress.close;
  end;
end;

procedure TfmJournal.sb_printClick(Sender: TObject);
var i:Integer;
  Kpp:string;
  tSh :TTabSheet;
  SG:TAdvStringGrid;
begin
//  SLCheckKPP.Clear;
  if SLKPP.Count = 1 then
  begin
    Kpp := SLKPP.Strings[0];
    fmProgress.labAbout.Caption := '������������ ������� ��� ��� ' + KPP;
    tSh := pgc_KPP.Pages[0];
    fmProgress.pb.StepIt;
    Application.ProcessMessages;
    if tSh.ControlCount > 0 then
      if tSh.Controls[0] is TAdvStringGrid then
      begin
        SG := tSh.Controls[0] as TAdvStringGrid;
        CreateJournalSG(jBeg, jEnd, SG,KPP);
        tSh.TabVisible := True;
      end;
    Exit;
  end;
  if fmSelAllKPP = nil then
    fmSelAllKPP := TfmSelAllKPP.Create(Application);
  if fmSelAllKPP.ShowModal = mrOk then
  begin
    for i := 0 to fmSelAllKPP.chklst_KPP.Items.Count - 1 do
      if fmSelAllKPP.chklst_KPP.Checked[i] then
        SLCheckKPP.Add(1)
      else
        SLCheckKPP.Add(0);
    if fmProgress = nil then
      fmProgress := TfmProgress.Create(Application);
    fmProgress.Show;
    fmProgress.pb.Max := SLKPP.Count;
    fmProgress.pb.Min := 0;
    fmProgress.pb.Step := 1;
    Application.ProcessMessages;

    for i := 0 to pgc_KPP.PageCount - 1 do
    begin
      tSh := pgc_KPP.Pages[i];
      tSh.TabVisible := False
    end;
    try
    if pgc_KPP.PageCount = SLKPP.Count then
      for i := 0 to pgc_KPP.PageCount - 1 do
      begin
        Kpp := SLKPP.Strings[i];
        fmProgress.labAbout.Caption := '������������ ������� ��� ��� ' + KPP;
        tSh := pgc_KPP.Pages[i];
        fmProgress.pb.StepIt;
        Application.ProcessMessages;
        if tSh.ControlCount > 0 then
          if tSh.Controls[0] is TAdvStringGrid then
          begin
            SG := tSh.Controls[0] as TAdvStringGrid;
            if SLCheckKPP[i] = 1 then
              begin
                CreateJournalSG(jBeg, jEnd, SG,KPP);
                if SG.RowCount = SG.FixedRows + SG.FixedFooters then
                  tSh.TabVisible := False
                else
                  tSh.TabVisible := True;
              end
            else
              tSh.TabVisible := False;
          end;
      end;
    finally
      fmProgress.close;
    end;
  end
end;


end.
