object fmSelPeriod: TfmSelPeriod
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072' '#1076#1083#1103' '#1079#1072#1075#1088#1091#1079#1082#1080' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
  ClientHeight = 120
  ClientWidth = 287
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object lb_lb1: TLabel
    Left = 36
    Top = 8
    Width = 34
    Height = 17
    AutoSize = False
    Caption = #1043#1086#1076':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lb_1: TLabel
    Left = 8
    Top = 42
    Width = 62
    Height = 18
    Caption = #1050#1074#1072#1088#1090#1072#1083':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btn1: TBitBtn
    Left = 176
    Top = 80
    Width = 97
    Height = 32
    DoubleBuffered = True
    Kind = bkOK
    ParentDoubleBuffered = False
    TabOrder = 0
  end
  object edt1: TAdvSpinEdit
    Left = 76
    Top = 8
    Width = 65
    Height = 28
    DisabledBorder = False
    Value = 2014
    FloatValue = 2014.000000000000000000
    HexValue = 20
    Color = 16056309
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    IncrementFloat = 0.100000000000000000
    IncrementFloatPage = 1.000000000000000000
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    MaxValue = 2020
    MinValue = 2014
    ParentFont = False
    TabOrder = 1
    Visible = True
    Version = '1.5.3.4'
    OnChange = edt1Change
  end
  object cbAllKvart: TComboBox
    Left = 76
    Top = 42
    Width = 203
    Height = 26
    Style = csDropDownList
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemIndex = 0
    ParentFont = False
    TabOrder = 2
    Text = '1 '#1082#1074#1072#1088#1090#1072#1083
    OnChange = cbAllKvartChange
    Items.Strings = (
      '1 '#1082#1074#1072#1088#1090#1072#1083
      '2 '#1082#1074#1072#1088#1090#1072#1083
      '3 '#1082#1074#1072#1088#1090#1072#1083
      '4 '#1082#1074#1072#1088#1090#1072#1083)
  end
end
