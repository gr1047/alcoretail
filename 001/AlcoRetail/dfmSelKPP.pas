unit dfmSelKPP;

interface

uses
  Forms, Controls, StdCtrls, Classes, Buttons, GlobalConst;

type
  TfmSelKPP = class(TForm)
    btn_Ok: TBitBtn;
    btn_Cancel: TBitBtn;
    cbbKPP: TComboBox;
    procedure cbbKPPChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    fSKPPTypeF : TTypeSelKPP;
  end;

var
  fmSelKPP: TfmSelKPP;

implementation
uses Global, GlobalUtils;

{$R *.dfm}

procedure TfmSelKPP.cbbKPPChange(Sender: TObject);
var idx:integer;
    s:string;
begin
  if cbbKPP.ItemIndex = -1 then exit;

  s:= cbbKPP.Items.Strings[cbbKPP.ItemIndex];
  idx := SLKpp.IndexOf(s);
  fmSelKPP.Tag := idx;
end;

procedure TfmSelKPP.FormShow(Sender: TObject);
var idx:integer;
begin
  AssignCBSL(cbbKPP,SLKPP);
  idx:=cbbKpp.Items.IndexOf(String(fOrg.Kpp0));
  case fSKPPTypeF of
    fKppCorrectEan:
      begin
        if cbbKPP.Items.Count > 0 then
          cbbKPP.ItemIndex := 0;
        if idx <> -1 then
          cbbKpp.Items.Delete(idx);
      end;
    fKppXML:
      if idx <> -1 then
        cbbKpp.ItemIndex := idx;
  end;
end;

end.
