unit dfmDataExchange;

interface

uses
  Forms, Grids, AdvObj, BaseGrid, AdvGrid, Controls, StdCtrls, Buttons, Classes,
  ExtCtrls;

type
  TfmDataExchange = class(TForm)
    Panel3: TPanel;
    btn_Connect: TSpeedButton;
    Panel1: TPanel;
    Button1: TButton;
    Label1: TLabel;
    Panel2: TPanel;
    lst_Msg: TListBox;
    SG_Query: TAdvStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure btn_ConnectClick(Sender: TObject);
    procedure Panel2Resize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDataExchange: TfmDataExchange;

implementation

uses dfmWork,Global,Global2, dfmMove, GlobalConst, dfmMain;
{$R *.dfm}

procedure TfmDataExchange.btn_ConnectClick(Sender: TObject);
var RegHost:string;
    wd:TDateTime;
begin
//  RegHost:='bmsystem.dyndns.org';
//  wd := Now();
  RegHost:=BMHost;
  wd:=GetCurDate(CurKvart);
  try
    ConnectServer(SG_Query,RegHost, fOrg.inn, exe_version );
    UpdateDateRef;
    ChangeKvart(dBeg, dEnd);
    if fmMove = nil then fmMove := TfmMove.Create(Application);
    fmMove.ShowDataAll;
    if fmMain <> nil then
      fmMain.WriteCaptionCount;
    Show_inf('������ ������� ���������', fInfo);
  except
    Show_inf('������ ����������', fError);
  end;
end;

procedure TfmDataExchange.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmDataExchange.FormCreate(Sender: TObject);
begin
  SG_Query.FixedRows := 1;
  SG_Query.RowCount := SG_Query.FixedRows + SG_Query.FixedFooters;
  SG_Query.Cells[SG_Query.FixedCols,0] := '���������';
  SG_Query.Cells[SG_Query.FixedCols + 1, 0] := '������';
end;

procedure TfmDataExchange.Panel2Resize(Sender: TObject);
var width:integer;
begin
  width:=Trunc((Sender as TPanel).ClientWidth/2)-1;
  SG_Query.Width := width;
  width:=Trunc((SG_Query.ClientWidth - SG_Query.FixedColWidth) / 2)-1;
  SG_Query.ColWidths[1]:=width;
  SG_Query.ColWidths[2]:=width;
end;

end.
