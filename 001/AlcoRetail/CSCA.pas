{
--------------------------------------
  Control Sum Cript Algorithm  v1.1  - CSCA1
            Delphi realization!
--------------------------------------
  Date: 11.02.2008

  created by rpy3uH (aka ahilles)
    E-Mail: ahilles1806@rambler.ru
            gruzino-abhaz@rambler.ru

}
unit CSCA;

interface

uses windows;

{
   CSCA1 - �������� ������ � ������� ������
     DataAddress - ����� ������ � �������
     DataSize    - ������ ��������� ������
     Password    - ������
}
procedure CSCA1(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;
procedure CSCA2(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;

implementation

procedure CSCA1(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;
var
  PasswordCp:DWORD;
  PasswordLength:DWORD;
asm
  jmp @@Entry

  @@GET_CRC32:
{; IN
;       ESI = block offset
;       EDI = block size
; OUT
;       EAX = CRC32}

        push esi
        push edi
        push ecx
        push ebx
        push edx

        cld
        xor     ecx,ecx
        dec     ecx
        mov     edx,ecx
   @@NextByteCRC:
        xor     eax,eax
        xor     ebx,ebx
        lodsb
        xor     al,cl
        mov     cl,ch
        mov     ch,dl
        mov     dl,dh
        mov     dh,8
   @@NextBitCRC:
        shr     bx,1
        rcr     ax,1
        jnc     @@NoCRC
        xor     ax,08320h
        xor     bx,0EDB8h
   @@NoCRC:
        dec     dh
        jnz     @@NextBitCRC
        xor     ecx,eax
        xor     edx,ebx
        dec     edi
        jnz     @@NextByteCRC
        not     edx
        not     ecx
        mov     eax,edx
        rol     eax,16
        mov     ax,cx

        pop edx
        pop ebx
        pop ecx
        pop edi
        pop esi
        ret


@@GetZSLength:
{ get zero-string length
IN
       EDI ZS offset
OUT
      EAX ZS length}

        push ecx
        push esi
        push edi

        cld
        xor   al, al
        mov ecx, 0FFFFFFFFh
        mov esi, edi
        repne scasb
        sub edi, esi
        mov eax, edi
        dec eax

        pop edi
        pop esi
        pop ecx
        ret

@@ExpandString:
{
;expand string by addition char to end string
;ESI  - pointer to string
;new chars will be add to end string!!!
}
        pushad
        mov ebx, esi
        mov edi, esi
        call @@GetZSLength
        add edi, eax
        mov ecx, eax
        rep movsb
        mov byte [edi], 0

        add ebx, eax
        mov ecx, 1
       @@rep:
        sub byte [ebx], cl
        cmp byte [ebx], 0
        jnz @@_f
        mov byte [ebx], 7Fh
       @@_f:
        inc ecx
        inc ebx
        cmp byte [ebx],0
        jz @@endrep
        jmp @@rep
       @@endrep:

        popad
        ret

@@NextPasswordMod:
{
;IN
;   ESI offset ZS of password
;   EAX ordinal number
;   EDI password length
}
        pushad
        dec edi

        xor edx, edx
        div edi
        add esi, edx
        mov bh, [esi]
        mov ecx, edi
        sub ecx, edx
   @@next:
        mov bl, [esi+1]
        mov [esi], bl
        inc esi
        loop @@next
        mov [esi], bh

        popad
        ret

@@Entry:
{
proc CSCA1 DataAddress, DataSize, Password
; DataAddress - data offset
; DataSize -    data size
; Password -    offset ZS of password}


        pushad
        mov  edi, Password
        call @@GetZSLength
        mov ebx, eax

        shl eax, 1
        mov  [PasswordLength], eax
        inc eax

        push PAGE_READWRITE
        push MEM_COMMIT+MEM_RESERVE
        push eax
        push 0
        call VirtualAlloc

        mov edi, eax      //; eax = password offset
        mov esi, Password
        mov ecx, ebx
        rep movsb
        mov PasswordCp, eax

        mov esi, eax
        call @@ExpandString

        xor ecx, ecx
    @@next1:     //; ecx
        mov esi, PasswordCp
        mov edi, PasswordLength
        mov eax, ecx
        call @@NextPasswordMod

        mov esi, PasswordCp
        mov edi, PasswordLength
        call @@GET_CRC32   //;eax = CRC32 of current password mod
        mov edi, DataAddress
        imul edx, ecx,4  //;edx = current position
        add edi, edx     //; edi = current offset

        mov ebx, DataSize

        sub ebx, edx
        cmp ebx, 0
        jz @@end
        cmp ebx, 4
        jl @@not_full_xor

     @@full_xor:
        xor [edi], eax
        inc ecx
        jmp @@next1
     @@not_full_xor:
        mov ecx, ebx
       @@rep1:
        dec ecx
        add edi, ecx
        mov dl, [edi]
        xor dl, al
        mov [edi], dl
        sub edi, ecx
        inc ecx
        shr eax, 8
        loop @@rep1
    @@end:
        push MEM_RELEASE
        push 0
        push PasswordCp
        call VirtualFree

        popad
end;


procedure CSCA2(DataAddress:pointer; DataSize: DWORD; Password: PAnsiChar);stdcall;
var
  PasswordCp:DWORD;
  PasswordLength:DWORD;
asm
  jmp @@Entry

  @@GET_CRC32:
{; IN
;       ESI = block offset
;       EDI = block size
; OUT
;       EAX = CRC32}

        push esi
        push edi
        push ecx
        push ebx
        push edx

        cld
        xor     ecx,ecx
        dec     ecx
        mov     edx,ecx
   @@NextByteCRC:
        xor     eax,eax
        xor     ebx,ebx
        lodsb
        xor     al,cl
        mov     cl,ch
        mov     ch,dl
        mov     dl,dh
        mov     dh,8
   @@NextBitCRC:
        shr     bx,1
        rcr     ax,1
        jnc     @@NoCRC
        xor     ax,08320h
        xor     bx,0EDB8h
   @@NoCRC:
        dec     dh
        jnz     @@NextBitCRC
        xor     ecx,eax
        xor     edx,ebx
        dec     edi
        jnz     @@NextByteCRC
        not     edx
        not     ecx
        mov     eax,edx
        rol     eax,16
        mov     ax,cx

        pop edx
        pop ebx
        pop ecx
        pop edi
        pop esi
        ret


@@GetZSLength:
{ get zero-string length
IN
       EDI ZS offset
OUT
      EAX ZS length}

        push ecx
        push esi
        push edi

        cld
        xor   al, al
        mov ecx, 0FFFFFFFFh
        mov esi, edi
        repne scasb
        sub edi, esi
        mov eax, edi
        dec eax

        pop edi
        pop esi
        pop ecx
        ret

@@ExpandString:
{
;expand string by addition char to end string
;ESI  - pointer to string
;new chars will be add to end string!!!
}
        pushad
        mov ebx, esi
        mov edi, esi
        call @@GetZSLength
        add edi, eax
        mov ecx, eax
        rep movsb
        mov byte [edi], 0

        add ebx, eax
        mov ecx, 1
       @@rep:
        sub byte [ebx], cl
        cmp byte [ebx], 0
        jnz @@_f
        mov byte [ebx], 7Fh
       @@_f:
        inc ecx
        inc ebx
        cmp byte [ebx],0
        jz @@endrep
        jmp @@rep
       @@endrep:

        popad
        ret

@@NextPasswordMod:
{
;IN
;   ESI offset ZS of password
;   EAX ordinal number
;   EDI password length
}
        pushad
        dec edi

        xor edx, edx
        div edi
        add esi, edx
        mov bh, [esi]
        mov ecx, edi
        sub ecx, edx
   @@next:
        mov bl, [esi+1]
        mov [esi], bl
        inc esi
        loop @@next
        mov [esi], bh

        popad
        ret

@@Entry:
{
proc CSCA1 DataAddress, DataSize, Password
; DataAddress - data offset
; DataSize -    data size
; Password -    offset ZS of password}


        pushad
        mov  edi, Password
        call @@GetZSLength
        mov ebx, eax

        shl eax, 1
        mov  [PasswordLength], eax
        inc eax        

        push PAGE_READWRITE
        push MEM_COMMIT+MEM_RESERVE
        push eax
        push 0
        call VirtualAlloc

        mov edi, eax      //; eax = password offset
        mov esi, Password
        mov ecx, ebx
        rep movsb
        mov PasswordCp, eax

        mov esi, eax
        call @@ExpandString

        xor ecx, ecx
    @@next1:     //; ecx
        mov esi, PasswordCp
        mov edi, PasswordLength
        mov eax, ecx
        call @@NextPasswordMod

        mov esi, PasswordCp
        mov edi, PasswordLength
        call @@GET_CRC32   //;eax = CRC32 of current password mod
        mov edi, DataAddress
        imul edx, ecx,4  //;edx = current position
        add edi, edx     //; edi = current offset

        mov ebx, DataSize

        sub ebx, edx
        cmp ebx, 0
        jz @@end
        cmp ebx, 4
        jl @@not_full_xor

     @@full_xor:
        xor [edi], eax
        inc ecx
        jmp @@next1
     @@not_full_xor:
        mov ecx, ebx
       @@rep1:
        dec ecx
        add edi, ecx
        mov dl, [edi]
        xor dl, al
        mov [edi], dl
        sub edi, ecx
        inc ecx
        shr eax, 8
        loop @@rep1                                      
    @@end:
        push MEM_RELEASE
        push 0
        push PasswordCp
        call VirtualFree

        popad
end;

end.
