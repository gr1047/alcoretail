unit dfmAddOst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, AdvObj, BaseGrid, AdvGrid, ExtCtrls, ShellAPI;

type
  TfmAddOst = class(TForm)
    pnl_2: TPanel;
    btn1: TBitBtn;
    btn2: TBitBtn;
    pnl_3: TPanel;
    btn3: TBitBtn;
    btn_shabl: TBitBtn;
    btn_loadOst: TBitBtn;
    SG_Ost: TAdvStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SG_OstCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure btn3Click(Sender: TObject);
    procedure SG_OstCellValidate(Sender: TObject; ACol, ARow: Integer;
      var Value: string; var Valid: Boolean);
    procedure btn1Click(Sender: TObject);
    procedure btn_shablClick(Sender: TObject);
    procedure btn_loadOstClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SLROstRash:TStringList;
  end;

var
  fmAddOst: TfmAddOst;

implementation

uses Global, GlobalConst, GlobalUtils, dfmReplace, dfmWork, Global2;

{$R *.dfm}

procedure TfmAddOst.btn1Click(Sender: TObject);
var i:Integer;
  s:string;
begin
  SLROstRash.Clear;
  SG_Ost.FilterActive := False;
  for i := SG_Ost.FixedRows to SG_Ost.RowCount - SG_Ost.FixedFooters - 1 do
  begin
    s := SG_Ost.Cells[1,i] + SG_Ost.Cells[3,i] + '=' + SG_Ost.Cells[12,i];
    if SG_Ost.Floats[11,i] > 0  then
      SLROstRash.Add(s);
  end;
end;

procedure TfmAddOst.btn3Click(Sender: TObject);
var fSG:TADVStringGrid;
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  fSG := SG_Ost;

  fmReplace.SGFind := fSG;
  fmReplace.Show;
end;

procedure TfmAddOst.btn_loadOstClick(Sender: TObject);
var fName:string;
  rez:boolean;
  c:Integer;
begin
  fmWork.dlgOpen_Import.Title := '�������� ���� �����������';
  fmWork.dlgOpen_Import.Filter := '���� dBase (*.xls)|*.xls';
  fmWork.dlgOpen_Import.DefaultExt := fmWork.dlgOpen_Import.Filter;
  if fmWork.dlgOpen_Import.Execute then
    if (Length(fmWork.dlgOpen_Import.FileName)>0) and FileExists(fmWork.dlgOpen_Import.FileName) then
        begin
          Caption := fmWork.dlgOpen_Import.FileName;
          fName:=fmWork.dlgOpen_Import.FileName;
          rez:=LoadShOst(fName, SG_Ost,c);
          if rez then
            begin
              if c = 0  then
                Show_Inf('������� ��������� �������',fInfo)
              else
              begin
                Show_Inf('������� ��������� �� ���, �������� ���� error_ost.txt',fInfo);
//                ShellExecute(0, 'open', PChar('error_ost.txt'), nil, nil, SW_SHOWNORMAL);
                ShellExecute(Handle, 'open', 'c:\windows\notepad.exe', PWideChar(fName), nil, SW_SHOWNORMAL);
              end;
            end
          else
            Show_Inf('������ �������� ��������',fError);
        end;
end;

procedure TfmAddOst.btn_shablClick(Sender: TObject);
var fName:string;
begin
  if SaveShOst(fName) then
    Show_Inf('���� ������� �������� �������� �� ������' + #10#13 + fName, fInfo)
  else
    Show_Inf('��������� ������ ��� ���������� ����� ������� ��������', fError);
end;

procedure TfmAddOst.FormCreate(Sender: TObject);
begin
  SLROstRash := TStringList.Create;
  ShapkaSG(SG_Ost, NameOstRash);
  
  PropCellsSG(arNormal, SG_Ost, CalcColOst, NoResOst, HideColOst);
end;

procedure TfmAddOst.FormDestroy(Sender: TObject);
begin
  FreeStringList(SLROstRash);
end;

procedure TfmAddOst.SG_OstCanEditCell(Sender: TObject; ARow, ACol: Integer;
  var CanEdit: Boolean);
var Coledit: Array of Integer;
  idx: Integer;
begin
  SetLength(Coledit, High(EditColOst) + 1);
  Move(EditColOst[0], Coledit[0], sizeOf(Integer) * Length(EditColOst));
  idx := IndexMas(Coledit, ACol);

  if idx <> -1 then
    CanEdit := True
  else
    CanEdit := False;
end;

procedure TfmAddOst.SG_OstCellValidate(Sender: TObject; ACol, ARow: Integer;
  var Value: string; var Valid: Boolean);
var dv: TDvizenie;
  curRash, tmp :Extended;
begin
  dv.ost0 := SG_Ost.Floats[7,ARow];
  dv.prih := SG_Ost.Floats[8,ARow];
  dv.oder := SG_Ost.Floats[9,ARow];
  dv.rash := SG_Ost.Floats[10,ARow];
  curRash := SG_Ost.Floats[11,ARow];
  dv.ost1 := SG_Ost.Floats[12,ARow];
  tmp := StrToFloat(Value);
  if tmp < 0 then tmp := Abs(tmp);
  case ACol of
    11: begin
      curRash := tmp;
      dv.ost1 := dv.ost0 + dv.prih + dv.oder - dv.rash - curRash;

      if dv.ost1 < 0 then
      begin
        dv.ost1 := 0;
        curRash := dv.ost0 + dv.prih + dv.oder - dv.rash - dv.ost1;
        Value := FloatToStrF(curRash, ffFixed, maxint, 2);
        SG_Ost.Floats[11,ARow] := curRash;
        SG_Ost.Floats[12,ARow] := 0;
      end
      else
      begin
        Value := FloatToStrF(tmp, ffFixed, maxint, 2);
        SG_Ost.Floats[12,ARow] := dv.ost1;
      end;
    end;
    12: begin
      dv.ost1 := tmp;
      curRash := dv.ost0 + dv.prih + dv.oder - dv.rash - dv.ost1;

      if curRash < 0 then
      begin
        curRash := 0;
        dv.ost1 := dv.ost0 + dv.prih + dv.oder - dv.rash - curRash;
        Value := FloatToStrF(dv.ost1, ffFixed, maxint, 2);
        SG_Ost.Floats[11,ARow] := curRash;
        SG_Ost.Floats[12,ARow] := dv.ost1;
      end
      else
      begin
        Value := FloatToStrF(tmp, ffFixed, maxint, 2);
        SG_Ost.Floats[11,ARow] := curRash;
      end;
    end;
  end;
end;

end.
