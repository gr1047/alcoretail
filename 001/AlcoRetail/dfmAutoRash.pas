unit dfmAutoRash;

interface

uses
  Forms, Controls, Buttons, Classes, ExtCtrls, SysUtils, DateUtils;

type
  TfmAutoRash = class(TForm)
    Panel1: TPanel;
    sbOst0: TSpeedButton;
    Panel2: TPanel;
    sbCancel: TSpeedButton;
    Panel4: TPanel;
    sbOst1: TSpeedButton;
    panel5: TPanel;
    sbOst2: TSpeedButton;
    procedure sbCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbOst2Click(Sender: TObject);
    procedure sbOst1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sbOst0Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAutoRash: TfmAutoRash;

implementation

uses dfmProcent, Global, dfmMove, dfmAddOst, GlobalUtils, Global2, GlobalConst;
{$R *.dfm}

procedure TfmAutoRash.FormCreate(Sender: TObject);
begin
  sbOst0.Caption := '������ ��������� ��������';
  sbOst1.Caption := '�������������� ��������� ��������' + #10#13 +
    '� ����������� �� ���� ���������� (� %)';
  sbOst2.Caption := '�������������� ��������� ��������' + #10#13 +
    '� ����������� �� ����� ��������� (� %)';
end;

procedure TfmAutoRash.sbOst0Click(Sender: TObject);
var y,m,d:Word;
  cDate,dat:TDateTime;
begin
  if fmAddOst = nil then
    fmAddOst := TfmAddOst.Create(Self);
  DecodeDate(Now,y,m,d);
  dat := DateOf(Kvart_End(CurKvart, NowYear));

  if (DateOf(fmMove.dtp_WorkDate.Date) <= dEnd) and
    (DateOf(fmMove.dtp_WorkDate.Date) >= dBeg) then
    cDate := fmMove.dtp_WorkDate.Date
  else
    cDate := dEnd;

  fmAddOst.Caption := '�������� ������� �� ��������� �������� �� ' + DateToStr(cDate);
  fmAddOst.SG_Ost.FilterActive := False;
  fmMove.ErrGrid(fPrihod, ShowDataOst(fmAddOst.SG_Ost, NoResOst, CalcColOst,
    HideColOst));
  if fmAddOst.ShowModal = mrOk then
  begin
    CreateRashFromOst(CurSG,fmAddOst.SLROstRash);
    Close;
  end;
end;

procedure TfmAutoRash.sbOst1Click(Sender: TObject);
begin
  if CurSG.Name <> 'SG_Rash' then exit;
  fmMove.CountNew := AutoAddRashod(CurSG);
  EnabledMainBut(False);
  Close;
end;

procedure TfmAutoRash.FormShow(Sender: TObject);
begin
  case TypeOst of
    1:  begin
      sbOst1.Enabled := True;
      sbOst2.Enabled := False;
    end;
    2:  begin
      sbOst2.Enabled := True;
      sbOst1.Enabled := False;
    end;
  end;
end;

procedure TfmAutoRash.sbCancelClick(Sender: TObject);
begin
  ModalResult:=mrCancel;
  EnabledMainBut(True);
  Close;
end;

procedure TfmAutoRash.sbOst2Click(Sender: TObject);
begin
  if CurSG.Name <> 'SG_Rash' then exit;
  fmMove.CountNew := AutoAddRashod(CurSG);
  EnabledMainBut(False);
  Close;
end;

end.
