unit dfmNewOrg;

interface

uses
  Forms, IniFiles, Registry, StdCtrls, Controls, ExtCtrls, Graphics, Classes,
  ShlObj, Windows, SysUtils;

type
  TfmNewOrg = class(TForm)
    Butt1: TButton;
    Butt2: TButton;
    Edit1: TEdit;
    Conf: TImage;
    Label1: TLabel;
    Label2: TLabel;
    pnl1: TPanel;
    pnl2: TPanel;
    rb1: TRadioButton;
    rb2: TRadioButton;
    pnlRefresh: TPanel;
    rb3: TRadioButton;
    rb4: TRadioButton;
    pnl_new: TPanel;
    rb5: TRadioButton;
    rb6: TRadioButton;
    procedure FormCreate(Sender: TObject);
    procedure Butt1Click(Sender: TObject);
    procedure rb2Click(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure btnFolderClick(Sender: TObject);
    procedure rb6Click(Sender: TObject);
    procedure rb5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmNewOrg: TfmNewOrg;

implementation

uses GlobalConst, Global, GlobalUtils, dfmWork, dfmSelSert;

{$R *.dfm}

procedure TfmNewOrg.btnFolderClick(Sender: TObject);
var fDecl, fRef, path, fn, fn1, TitleName, Inn, dir_path:string;
    BrowseInfo: TBrowseInfo;
    lpItemID: PItemIDList;
    TempPath, DisplayName: array [0 .. MAX_PATH] of char;
    tIniF:TIniFile;
begin
  Inn:='';
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  BrowseInfo.hwndOwner := fmWork.Handle;
  BrowseInfo.pszDisplayName := @DisplayName;
  TitleName := '�������� ����� � ������� ��� ��������������';
  BrowseInfo.lpszTitle := PChar(TitleName);
  BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemID <> nil then
  begin
    SHGetPathFromIDList(lpItemID, TempPath);
    GlobalFreePtr(lpItemID);
    Path := TempPath;
    //����� INI  �����
    fn:=FindFiles(path, '*.ini');
    if fn <> '' then
      begin
        tIniF := TIniFile.Create(fn);
        try
          tIniF.WriteString(SectionName, 'INN', inn);
          if inn <> '' then
            begin
              fOrg.inn := Inn;
              // �������� ��������� ����� ��� �����������
              if not DirectoryExists(path_db) then
                begin
                  dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                  dir_path:=dir_path+fOrg.Inn+'\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                  dir_path:=dir_path+'DataBase\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                end;

              if not DirectoryExists(path_arh) then
                begin
                  dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                  dir_path:=dir_path+fOrg.Inn+'\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                  dir_path:=dir_path+'BackUp\';
                  if not DirectoryExists(dir_path) then
                    MkDir(dir_path);
                end;
              if not DirectoryExists(path_arh) then
                MkDir(path_arh);

              //����������� INI �����
              fn1:=GetSpecialPath(CSIDL_PROGRAM_FILES)
                 + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '_.ini';
              if FileExists(fn1) then DeleteFile(fn);
              if FileExists(fn) then MoveFile(PChar(fn), PChar(fn1));
              CopyFile(PChar(path+'\'+NameProg+'.ini'),PChar(fn), True);

              //����������� ����� ����������� � ����������
              fDecl:=FindFiles(path, 'Decl_*.*');
              CopyFile(PChar(fDecl),PChar(path_arh + '\' + ExtractFileName(fDecl)), True);

              fRef:=FindFiles(path, 'Refs_*.*');
              CopyFile(PChar(fRef),PChar(path_arh + '\' + ExtractFileName(fRef)), True);

              fmWork.con1ADOCon.Close;
              fmWork.con2ADOCon.Close;
              if fDecl <> '' then
                Arh2DB(db_decl,fDecl, fOrg.Inn, False);
              if fRef <> '' then
                Arh2DB(db_spr,fRef, psw_loc, False);
            end;
        finally
          tIniF.Free;
        end;
      end;
  end;
end;

procedure TfmNewOrg.Butt1Click(Sender: TObject);
var TitleName, path, NewPath, fn, tinn, org:string;
    lpItemID: PItemIDList;
    TempPath, DisplayName: array [0 .. MAX_PATH] of char;
    BrowseInfo: TBrowseInfo;
    tIF, tIniF : TIniFile;
    Reg : TRegistry;
    y, m, d:Word;
begin
  if rb5.Checked then
  begin
    if Edit1.Text = NameNewOrg then
      fOrg.SelfName := NameNewOrg
    else
      fOrg.SelfName :=Edit1.Text;
    if rb1.Checked then ModalResult := mrOk;
    if rb2.Checked then
    begin
      DecodeDate(Now(), y, m, d);
      CurKvart := Kvartal(m);
      CurYear := y;
      CurDate := GetCurDate(CurKvart);

      if rb4.Checked then
      begin
        FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
        BrowseInfo.hwndOwner := fmWork.Handle;
        BrowseInfo.pszDisplayName := @DisplayName;
        TitleName := '�������� ����� � ������� ��� ��������������';
        BrowseInfo.lpszTitle := PChar(TitleName);
        BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
        lpItemID := SHBrowseForFolder(BrowseInfo);
        if lpItemID <> nil then
        begin
          SHGetPathFromIDList(lpItemID, TempPath);
          GlobalFreePtr(lpItemID);
          Path := TempPath;
          RefreshFileFolder(Path);
        end;
      end;
      if rb3.Checked then
      begin
        fmWork.dlgOpen_Import.Title := '����� ������ ��� ��������������: ';
        fmWork.dlgOpen_Import.Filter := '����� ������|*.zip';
        Screen.Cursor := crHourGlass;
        fmWork.con1ADOCon.Close;
        fmWork.con2ADOCon.Close;
        try
          if fmWork.dlgOpen_Import.Execute then
            path := fmWork.dlgOpen_Import.FileName;
          if path <> '' then
          begin
            NewPath := WinTemp + NameProg;

            if UnZip(path, NewPath) then
            begin
              RefreshFileFolder(NewPath);
              if DirectoryExists(NewPath) then
                RemoveDir(NewPath);
//                DelDir(NewPath)
            end;
          end;
          Screen.Cursor := crDefault;
        except
          Screen.Cursor := crHourGlass;
        end;
      end;
      ModalResult := mrRetry;
    end;
  end;
  if rb6.Checked then
  begin
    if fmSelSert = nil then fmSelSert:=TfmSelSert.Create(Self);
    FindEcp:=SetECP;
    if not FindEcp then
      begin
        Show_Inf('�� ������� �� ������ �����������', fError);
        fmSelSert.Close;
      end
    else
    begin
      fmSelSert.ShowModal;
      if SelectCert then
      begin
        if Copy(fSert.INN,0,2) = '00' then
          fOrg.Inn :=Copy(fSert.INN,3,10);
        fOrg.Kpp0 := fSert.kpp;
        fOrg.SelfName := fSert.OrgName;
        fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
        if fOrg.Inn <> NullInn then
        begin
          CreatePath(fOrg.Inn);
          LoadDir;
          tIF:=TIniFile.Create(fn);
          try
            tIF.WriteString(SectionName,'SelfName', fOrg.SelfName);
            tIF.WriteString(SectionName,'INN', fOrg.Inn );
            tIF.WriteString(SectionName,'KPP', String(fOrg.Kpp0));
          finally
            tIF.Free;
          end;
        end;
        Reg := TRegistry.Create;
        fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
           + '\' + NameProg + '\' + NameProg + '_inn.ini';
        tIniF := TIniFile.Create(fn);
        try
          tinn := fOrg.Inn;
          if tinn=l_NewClient then
            begin
              org := l_NewClient;
              fOrg.Inn :=NullInn;
              fOrg.Kpp0 :='000000000';
              newclient := true;
              exit;
            end
          else
            begin
              newclient := false;
              fOrg.Inn := tinn;
              tIniF.WriteString('LastInn', 'INN', tinn);
            end;
        finally
            Reg.Free;
            tIniF.Free;
        end;
      end;
    end;
    ModalResult := mrRetry;
  end;

end;

procedure TfmNewOrg.FormCreate(Sender: TObject);
begin
  Edit1.Text:= NameNewOrg;
end;

procedure TfmNewOrg.rb1Click(Sender: TObject);
begin
  pnlRefresh.Visible:=False;
  pnl_New.Visible := True;
  Edit1.Enabled:=True;
  Label2.Enabled:=True;
end;

procedure TfmNewOrg.rb2Click(Sender: TObject);
begin
  pnlRefresh.Visible:=True;
  pnl_New.Visible := False;
  Edit1.Enabled:=False;
  Label2.Enabled:=False;
end;

procedure TfmNewOrg.rb5Click(Sender: TObject);
begin
  Edit1.Enabled:=True;
  Label2.Enabled:=True;
end;

procedure TfmNewOrg.rb6Click(Sender: TObject);
begin
  Edit1.Enabled:=False;
  Label2.Enabled:=False;
end;

end.
