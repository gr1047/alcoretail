object fmBD: TfmBD
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1056#1072#1073#1086#1090#1072' '#1089' '#1073#1072#1079#1072#1084#1080' '#1076#1072#1085#1085#1099#1093
  ClientHeight = 589
  ClientWidth = 467
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 18
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 467
    Height = 49
    Align = alTop
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 2
      Top = 20
      Width = 463
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object SpeedButton7: TSpeedButton
        Left = 432
        Top = 0
        Width = 31
        Height = 27
        Align = alRight
        Flat = True
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000120B0000120B00000000000000000000FFFFFFD6DBDE
          9EAAAEC4C6C7E9E9E9FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFDFDFD5CA1BB19BCF0289BBD418AA0648E9C919FA5C2C3C3E7E7E7FCFCFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF6F8F95FA2BE32CDFE32DAFF2ED9FF29CFFE22B8E326
          A2C73491AE5987988D9A9EBFC0C1E3E3E3FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F3F463A3BF3BC4F52FCEF831CF
          FA32D2FD33D5FF32D7FF30D6FF2DCFFC22C1EF1BABD6318DA95986948A9CA1B7
          BCBEDDDDDDFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF1F35CA9C9
          53C9F735CCF538D1FA34D0F930CFFA30CFFA31D0FA32D0FA32D1FB33D3FE33D7
          FF2BD5FF22CAF820A7D13590AD538B9DC2CBCEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5F0F454AED170D8FE3BC3EC55D9FC4BD5FA46D4F93FD3F937D0F931CFFA
          31D0FA32D0FA32D0FA32D0FA34D3FC35D8FF32DBFF1CD1FF68C0DBFEFEFEFFFF
          FFFFFFFFFFFFFFFFFFFFDFECF14DACCE79DCFF48C4EB6BDFFD62DAFB5DDAFA57
          D9FB52D7FB49D5FA3AD2FA30CFFA31CFFA33D0FA33D0FA33CFFA34D0FA26CCF6
          37C4E8E6F5F9FFFFFFFFFFFFFFFFFFFFFFFFDAEAEF4CADCF81E2FF5ECDF168D8
          F47DE2FD73DFFB6DDDFA67DBFA63DBFB5DDAFB4CD6F938D1F92FD0FA33D0FA33
          D0FA34D1FA28CAF323C2E8ACE5F5FEFEFFFFFFFFFFFFFFFFFFFFD5EDF54ABCE2
          89E4FF69D4F567D0EE9CECFE8BE5FC86E4FB7FE2FB7AE1FB74E0FA70DEFB64DC
          FA4AD5F934D1F931D0FA31D0FA27C8F340CEF04BCCEFECF7FBFFFFFFFFFFFFFF
          FFFFD0EDF749C1E88CE6FF73DAF76CD2EFABECFDA5EBFC9EEAFC97E8FC92E7FC
          8CE6FB85E4FC81E3FB7BE2FA6CDEFB4ED7FA3DD3FA28C8F33FCCEE36D1F8C1E8
          F4FFFFFFFFFFFFFFFFFFCBECF74FC5E98FE8FF8BE7FE61CFEEADE7F7CBF5FFBA
          EFFDB2EDFDABECFCA5EBFC9EE9FC98E8FB92E7FC8DE5FB8AE5FC7EE2FC5FD6F4
          61D4F05CDEFD54C9EBFCFEFEFFFFFFFFFFFFD7EAF053CBEE94EBFF90E9FE7CE0
          F863CCEB91DCF1B1EAF9C3F1FCC0EFFBBCEFFCBCF0FEB1EDFCABECFCA4EAFC9F
          E9FB9AE8FC8FE2F79AE4F694E7FC74DDF6D4EEF6FFFFFFFFFFFFDAE8EC5AD0F0
          99EFFF91EBFD92EAFD8BE9FC7ADEF669D4F05CCDEC66CFEB71D3EEA7E5F6D4F6
          FEC2F1FDBCEFFDB6EEFCB1EEFDA5E7F8ACE9F7ADEDFD9CE9FB93DAF0FEFFFFFF
          FFFFD2E4E963D6F39EF1FF97EEFD96EDFD95EDFD96EEFE98EDFE98EEFE99F0FF
          8EE9FB60CEECACE5F5E5FBFFE1F9FED8F8FED0F5FFC2EFFBC4F0FAC0F1FDBFF2
          FE98E0F5D7EFF7FBFDFECBDFE66EDCF5A1F4FF9BF0FE9AF0FE9AF0FE99EFFE99
          EFFE97EFFE97EEFD99F0FE91ECFC71D8F072D3ED79D4ED81D5ED96DDF1AAE3F4
          CAF2FBD3F5FDD2F5FEAEE9F9AEE2F1DFF2F9C6E1EB77DFF4A6F7FF9FF2FE9FF3
          FE9EF3FE9DF2FE9DF2FE9DF2FE9CF1FE9CF2FE9EF3FF9AF0FE92EDFC91EDFC94
          EDFD88E7FA35A3C793AFBAB0DFEF90D7ED8BD6EDBAE5F3D6EFF7CEE5ED6EDEF3
          ABFAFFA4F6FEA2F5FEA3F6FEA2F5FEA1F5FEA3F7FFA5FAFFA2F6FFA2F5FEA4F6
          FFA3F6FFA1F6FFA2F5FFA4F8FF44A5C0DFE0E1FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE6EDF057C1DBB2FFFFAEFCFFABFAFFA8F8FFA8F8FEAAFBFF82E8F957AFC8
          5ABFDB52CBEC5ECCE86CD4EB7EDFF28DE8F78FEDFD3EA0BDE2E2E2FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF82B2C178E7F895F7FF9DFAFFA7FCFFADFEFF84
          ECFC609BAEF4F5F6FAFDFEF7FCFEE6EAEBC8D4D8AAC4CD8EBDCD72C1DB9CC2CF
          FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5EDF196D9EE93C2D087C2
          D181C9DB7BD9EF7BB8CAECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFE
          E6F6FBD9E3E7BDD4DCA3CEDC8DD7EEB6CFD7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        OnClick = SpeedButton7Click
      end
      object ePathBD: TEdit
        Left = 0
        Top = 0
        Width = 432
        Height = 27
        Align = alLeft
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Color = 16056309
        ReadOnly = True
        TabOrder = 0
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 49
    Width = 467
    Height = 128
    Align = alTop
    Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1088#1077#1079#1077#1088#1074#1085#1099#1093' '#1082#1086#1087#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object SpeedButton5: TSpeedButton
      Left = 2
      Top = 86
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1088#1077#1079#1077#1088#1074#1085#1091#1102' '#1082#1086#1087#1080#1102' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton5Click
      ExplicitTop = 48
    end
    object SpeedButton4: TSpeedButton
      Left = 2
      Top = 46
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1088#1077#1079#1077#1088#1074#1085#1091#1102' '#1082#1086#1087#1080#1102' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton4Click
      ExplicitTop = 48
    end
    object Panel1: TPanel
      Left = 2
      Top = 20
      Width = 463
      Height = 26
      Align = alClient
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object SpeedButton6: TSpeedButton
        Left = 432
        Top = 0
        Width = 31
        Height = 26
        Align = alRight
        Flat = True
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000120B0000120B00000000000000000000FFFFFFD6DBDE
          9EAAAEC4C6C7E9E9E9FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFDFDFD5CA1BB19BCF0289BBD418AA0648E9C919FA5C2C3C3E7E7E7FCFCFC
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFF6F8F95FA2BE32CDFE32DAFF2ED9FF29CFFE22B8E326
          A2C73491AE5987988D9A9EBFC0C1E3E3E3FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F3F463A3BF3BC4F52FCEF831CF
          FA32D2FD33D5FF32D7FF30D6FF2DCFFC22C1EF1BABD6318DA95986948A9CA1B7
          BCBEDDDDDDFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEAF1F35CA9C9
          53C9F735CCF538D1FA34D0F930CFFA30CFFA31D0FA32D0FA32D1FB33D3FE33D7
          FF2BD5FF22CAF820A7D13590AD538B9DC2CBCEFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5F0F454AED170D8FE3BC3EC55D9FC4BD5FA46D4F93FD3F937D0F931CFFA
          31D0FA32D0FA32D0FA32D0FA34D3FC35D8FF32DBFF1CD1FF68C0DBFEFEFEFFFF
          FFFFFFFFFFFFFFFFFFFFDFECF14DACCE79DCFF48C4EB6BDFFD62DAFB5DDAFA57
          D9FB52D7FB49D5FA3AD2FA30CFFA31CFFA33D0FA33D0FA33CFFA34D0FA26CCF6
          37C4E8E6F5F9FFFFFFFFFFFFFFFFFFFFFFFFDAEAEF4CADCF81E2FF5ECDF168D8
          F47DE2FD73DFFB6DDDFA67DBFA63DBFB5DDAFB4CD6F938D1F92FD0FA33D0FA33
          D0FA34D1FA28CAF323C2E8ACE5F5FEFEFFFFFFFFFFFFFFFFFFFFD5EDF54ABCE2
          89E4FF69D4F567D0EE9CECFE8BE5FC86E4FB7FE2FB7AE1FB74E0FA70DEFB64DC
          FA4AD5F934D1F931D0FA31D0FA27C8F340CEF04BCCEFECF7FBFFFFFFFFFFFFFF
          FFFFD0EDF749C1E88CE6FF73DAF76CD2EFABECFDA5EBFC9EEAFC97E8FC92E7FC
          8CE6FB85E4FC81E3FB7BE2FA6CDEFB4ED7FA3DD3FA28C8F33FCCEE36D1F8C1E8
          F4FFFFFFFFFFFFFFFFFFCBECF74FC5E98FE8FF8BE7FE61CFEEADE7F7CBF5FFBA
          EFFDB2EDFDABECFCA5EBFC9EE9FC98E8FB92E7FC8DE5FB8AE5FC7EE2FC5FD6F4
          61D4F05CDEFD54C9EBFCFEFEFFFFFFFFFFFFD7EAF053CBEE94EBFF90E9FE7CE0
          F863CCEB91DCF1B1EAF9C3F1FCC0EFFBBCEFFCBCF0FEB1EDFCABECFCA4EAFC9F
          E9FB9AE8FC8FE2F79AE4F694E7FC74DDF6D4EEF6FFFFFFFFFFFFDAE8EC5AD0F0
          99EFFF91EBFD92EAFD8BE9FC7ADEF669D4F05CCDEC66CFEB71D3EEA7E5F6D4F6
          FEC2F1FDBCEFFDB6EEFCB1EEFDA5E7F8ACE9F7ADEDFD9CE9FB93DAF0FEFFFFFF
          FFFFD2E4E963D6F39EF1FF97EEFD96EDFD95EDFD96EEFE98EDFE98EEFE99F0FF
          8EE9FB60CEECACE5F5E5FBFFE1F9FED8F8FED0F5FFC2EFFBC4F0FAC0F1FDBFF2
          FE98E0F5D7EFF7FBFDFECBDFE66EDCF5A1F4FF9BF0FE9AF0FE9AF0FE99EFFE99
          EFFE97EFFE97EEFD99F0FE91ECFC71D8F072D3ED79D4ED81D5ED96DDF1AAE3F4
          CAF2FBD3F5FDD2F5FEAEE9F9AEE2F1DFF2F9C6E1EB77DFF4A6F7FF9FF2FE9FF3
          FE9EF3FE9DF2FE9DF2FE9DF2FE9CF1FE9CF2FE9EF3FF9AF0FE92EDFC91EDFC94
          EDFD88E7FA35A3C793AFBAB0DFEF90D7ED8BD6EDBAE5F3D6EFF7CEE5ED6EDEF3
          ABFAFFA4F6FEA2F5FEA3F6FEA2F5FEA1F5FEA3F7FFA5FAFFA2F6FFA2F5FEA4F6
          FFA3F6FFA1F6FFA2F5FFA4F8FF44A5C0DFE0E1FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE6EDF057C1DBB2FFFFAEFCFFABFAFFA8F8FFA8F8FEAAFBFF82E8F957AFC8
          5ABFDB52CBEC5ECCE86CD4EB7EDFF28DE8F78FEDFD3EA0BDE2E2E2FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF82B2C178E7F895F7FF9DFAFFA7FCFFADFEFF84
          ECFC609BAEF4F5F6FAFDFEF7FCFEE6EAEBC8D4D8AAC4CD8EBDCD72C1DB9CC2CF
          FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5EDF196D9EE93C2D087C2
          D181C9DB7BD9EF7BB8CAECECECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFE
          E6F6FBD9E3E7BDD4DCA3CEDC8DD7EEB6CFD7FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        OnClick = SpeedButton6Click
      end
      object ePathArh: TEdit
        Left = 0
        Top = 0
        Width = 432
        Height = 26
        Align = alClient
        BorderStyle = bsNone
        Color = 16056309
        ReadOnly = True
        TabOrder = 0
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 177
    Width = 467
    Height = 152
    Align = alTop
    Caption = #1042#1054#1057#1057#1058#1040#1053#1054#1042#1051#1045#1053#1048#1045' '#1041#1040#1047#1067' '#1044#1040#1053#1053#1067#1061' '#1044#1045#1050#1051#1040#1056#1048#1056#1054#1042#1040#1053#1048#1071
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    object Label2: TLabel
      Left = 3
      Top = 29
      Width = 265
      Height = 16
      Caption = #1044#1086#1089#1090#1091#1087#1085#1099' '#1089#1083#1077#1076#1091#1102#1097#1080#1077' '#1090#1086#1095#1082#1080' '#1074#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 2
      Top = 110
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093' '#1076#1077#1082#1083#1072#1088#1080#1088#1086#1074#1072#1085#1080#1103
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton1Click
      ExplicitTop = 78
    end
    object SpeedButton9: TSpeedButton
      Left = 2
      Top = 70
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093' '#1076#1077#1082#1083#1072#1088#1080#1088#1086#1074#1072#1085#1080#1103
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton9Click
      ExplicitTop = 78
    end
    object cbDecl: TComboBox
      Left = 2
      Top = 46
      Width = 463
      Height = 24
      Align = alBottom
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox4: TGroupBox
    Left = 0
    Top = 329
    Width = 467
    Height = 160
    Align = alTop
    Caption = #1042#1054#1057#1057#1058#1040#1053#1054#1042#1051#1045#1053#1048#1045' '#1041#1040#1047#1067' '#1044#1040#1053#1053#1067#1061' '#1057#1055#1056#1040#1042#1054#1063#1053#1048#1050#1054#1042
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 3
      Top = 32
      Width = 265
      Height = 16
      Caption = #1044#1086#1089#1090#1091#1087#1085#1099' '#1089#1083#1077#1076#1091#1102#1097#1080#1077' '#1090#1086#1095#1082#1080' '#1074#1086#1089#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton2: TSpeedButton
      Left = 2
      Top = 118
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton2Click
      ExplicitTop = 114
    end
    object SpeedButton8: TSpeedButton
      Left = 2
      Top = 78
      Width = 463
      Height = 40
      Align = alBottom
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton8Click
      ExplicitTop = 84
    end
    object cbRef: TComboBox
      Left = 2
      Top = 54
      Width = 463
      Height = 24
      Align = alBottom
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
  end
  object GroupBox5: TGroupBox
    Left = 0
    Top = 489
    Width = 467
    Height = 56
    Align = alTop
    Caption = #1044#1056#1059#1043#1054#1045' '#1048#1047#1052#1045#1053#1045#1053#1048#1045' '#1041#1040#1047#1067
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    object SpeedButton10: TSpeedButton
      Left = 2
      Top = 20
      Width = 463
      Height = 34
      Align = alClient
      Caption = #1057#1074#1077#1088#1085#1091#1090#1100' '#1073#1072#1079#1091' '#1076#1072#1085#1085#1099#1093' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton10Click
      ExplicitHeight = 45
    end
  end
  object BitBtn1: TBitBtn
    Left = 176
    Top = 551
    Width = 129
    Height = 33
    Caption = #1047#1072#1082#1088#1099#1090#1100
    Default = True
    DoubleBuffered = True
    NumGlyphs = 2
    ParentDoubleBuffered = False
    TabOrder = 5
    OnClick = BitBtn1Click
  end
end
