unit dfmDecl;

interface

uses
  Forms, ExtCtrls, StdCtrls, Controls, Spin, Grids, AdvObj, BaseGrid, AdvGrid,
  ComCtrls, Buttons, Classes, XLSFile, SysUtils, ShellAPI, Windows, Graphics,
  xmldoc, xmlintf, ShlObj, AdvUtil, uForm78;

type
  TfmDecl = class(TForm)
    Panel1: TPanel;
    Panel4: TPanel;
    btn_print: TSpeedButton;
    Panel2: TPanel;
    pgc1: TPageControl;
    ts_Prih: TTabSheet;
    pErrPrih: TPanel;
    lErrPrih: TLabel;
    ts_Rash: TTabSheet;
    pErrRash: TPanel;
    Label2: TLabel;
    Panel5: TPanel;
    btn_decl: TSpeedButton;
    Panel6: TPanel;
    Label6: TLabel;
    Button1: TButton;
    ts_err: TTabSheet;
    memoErr: TMemo;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel12: TPanel;
    cbAllKvart: TComboBox;
    Panel10: TPanel;
    f11: TRadioButton;
    FSRAR: TLabel;
    f12: TRadioButton;
    ts1: TTabSheet;
    ts2: TTabSheet;
    SG_Print_111: TAdvStringGrid;
    SG_Print_121: TAdvStringGrid;
    SG_Print_112: TAdvStringGrid;
    SG_Print_122: TAdvStringGrid;
    pnl1: TPanel;
    btn_excel: TSpeedButton;
    pnl2: TPanel;
    rb1: TRadioButton;
    rbKor: TRadioButton;
    seKor: TSpinEdit;
    pnl3: TPanel;
    rb2: TRadioButton;
    rbf43: TRadioButton;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    tmr1: TTimer;
    btn1: TSpeedButton;
    chk_1C: TCheckBox;
    pnl4: TPanel;
    chk_declBM: TCheckBox;
    procedure Button1Click(Sender: TObject);
    procedure btn_declClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure rb1Click(Sender: TObject);
    procedure rbKorClick(Sender: TObject);
    procedure FSRARClick(Sender: TObject);
    procedure FSRARMouseEnter(Sender: TObject);
    procedure FSRARMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn_printClick(Sender: TObject);
    procedure btn_excelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure f12Click(Sender: TObject);
    procedure f11Click(Sender: TObject);
    Procedure AddTToXml; // ���������� ������ � ��������� tToXml
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateDecl;
  end;

var
  fmDecl: TfmDecl;
  dB, dE: TDateTime;
  CiKvart, CiYear: integer;
  xls: TXLSFile;
  Form7_8 : TForm7_8;

implementation

uses dfmWork, Global, Global2, GlobalUtils, GlobalConst, dfmMove, dfmCombination;
{$R *.dfm}


Procedure TfmDecl.AddTToXml; // ���������� ������ � ��������� tToXml
 Var i : Integer;
Begin
       Form7_8.Src.SLOstatkiKpp.Text :=SLOstatkiKpp.Text;
        for i:=0 to SLOstatkiKPP.Count-1 do
            Form7_8.Src.SLOstatkiKpp.Objects[i]:=SLOstatkiKpp.Objects[i];
        Form7_8.Src.SLOborotKpp_.Text:=SLOborotKpp.Text;
        for i:=0 to SLOborotKpp.Count-1 do
            Form7_8.Src.SLOborotKpp_.Objects[i]:=SLOborotKpp.Objects[i];
        Form7_8.Src.SLAllDecl_.Text :=SLAllDecl.Text;
        for i:=0 to SLAllDecl.Count-1 do
            Form7_8.Src.SLAllDecl_.Objects[i] := SLAllDecl.Objects[i];
        Form7_8.Src.que2_sql_:=fmWork.que2_sql;
        Form7_8.Src.que1_sql_:=fmWork.que1_sql;
        Form7_8.Src.cbAllKvart:=cbAllKvart;
        Form7_8.Src.dB:=dB;
        Form7_8.Src.dE:=dE;
        Form7_8.Src.f11:=f11.Checked;
        Form7_8.Src.rbf43:=rbf43.Checked;
        Form7_8.Src.chk_1C:=chk_1C.Checked;
        Form7_8.Src.rbKor:=rbKor.Checked;
        Form7_8.Src.CiKvart:=CiKvart;
        Form7_8.Src.CiYear:=CiYear;
        Form7_8.Src.seKor:=seKor.Value;
        Form7_8.Src.CurKvart:=CurKvart;
        Form7_8.Src.CurYear:=CurYear;
        Form7_8.Src.forg:=forg;
        Form7_8.Src.slkpp_.Text:=slkpp.Text;
        Form7_8.Src.exe_version_:=exe_version;
        Form7_8.Src.NowYear_:=NowYear;
        Form7_8.Src.NameProg:=NameProg;
        Form7_8.Src.findecp:=findecp;
        Form7_8.Src.fSert_:=fSert;
        Form7_8.Src.chk_declBM:=chk_declBM.Checked;
End;


procedure TfmDecl.btn_excelClick(Sender: TObject);
begin
  if FileExists('Print_forma.xls') then
    DeleteFile(PWideChar('Print_forma.xls'));
  xls.SaveAs('Print_forma.xls');
  ShellExecute(0, 'open', PChar('Print_forma.xls'), nil, nil, SW_SHOW);
end;

procedure TfmDecl.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmDecl.cbAllKvartChange(Sender: TObject);
var
  cKvart, cYear: string;
  sKvart: string;
begin
  if cbAllKvart.ItemIndex = -1 then
    exit;
  sKvart := cbAllKvart.Items[cbAllKvart.ItemIndex];
  cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
  cYear := Copy(sKvart, Length(sKvart) - 8, 4);
  CiKvart := StrToInt(cKvart);
  CiYear := StrToInt(cYear);
  dB := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
  dE := Kvart_End(StrToInt(cKvart), StrToInt(cYear));
  ChangeKvart(dB, dE);
  SG_Print_111.ClearAll;
  SG_Print_112.ClearAll;
  SG_Print_121.ClearAll;
  SG_Print_122.ClearAll;
  Shapka_PrintTable1(SG_Print_111);
  Shapka_PrintTable2(SG_Print_112);
  Shapka_PrintTable1(SG_Print_121);
  Shapka_PrintTable2(SG_Print_122);
  SG_Print_111.RowCount := 6;
  SG_Print_112.RowCount := 5;
  SG_Print_121.RowCount := 6;
  SG_Print_122.RowCount := 5;
end;

procedure TfmDecl.FormCreate(Sender: TObject);
var
  cKvart, cYear: string;
begin
//  AssignCBSL(cbAllKvart, SLPerRegYear);
//  cbAllKvart.ItemIndex := SetCurKvart;
  CreateDecl;
  FillSLFNS(SLFNS, FNSCode);
  if cbAllKvart.ItemIndex <> -1 then
  begin
    cKvart := Copy(cbAllKvart.Items[cbAllKvart.ItemIndex], 0,
      pos(nkv, cbAllKvart.Items[cbAllKvart.ItemIndex]) - 2);
    cYear := Copy(cbAllKvart.Items[cbAllKvart.ItemIndex],
      Length(cbAllKvart.Items[cbAllKvart.ItemIndex]) - 8, 4);
    CiKvart := StrToInt(cKvart);
    CiYear := StrToInt(cYear);
    dB := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
    dE := Kvart_End(StrToInt(cKvart), StrToInt(cYear));
  end;
  xls:= TXLSFile.Create;
end;

procedure TFmDecl.CreateDecl;
var i:integer;
begin
  for i := 0 to High(masErr) do
    masErr[i] := 0;
  AssignCBSL(cbAllKvart, SLPerRegYear);
  cbAllKvart.ItemIndex := SetCurKvart;
end;
procedure TfmDecl.f11Click(Sender: TObject);
begin
  chk_1C.Visible := not f11.Checked;
end;

procedure TfmDecl.f12Click(Sender: TObject);
begin
  if Length(fOrg.Inn) = 12 then
  begin
//    chk_1C.Visible := f12.Checked;
    chk_1C.Checked := True;
  end;
end;

procedure TfmDecl.FormDestroy(Sender: TObject);
begin
  xls.Destroy;
end;

procedure TfmDecl.FormShow(Sender: TObject);
begin
  Shapka_PrintTable1(SG_Print_111);
  Shapka_PrintTable2(SG_Print_112);
  Shapka_PrintTable1(SG_Print_121);
  Shapka_PrintTable2(SG_Print_122);
  f11.Visible := True;
  f12.Visible := True;
  if Length(fOrg.Inn) = 12 then
    f12.Checked := True;
end;

procedure TfmDecl.FSRARClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, nil, 'https://service.alcolicenziat.ru/auth/login', nil, nil,SW_SHOWNOACTIVATE);
end;

procedure TfmDecl.FSRARMouseEnter(Sender: TObject);
begin
  FSRAR.Font.Color := clRed;
end;

procedure TfmDecl.FSRARMouseLeave(Sender: TObject);
begin
  FSRAR.Font.Color := clBlue;
end;

procedure TfmDecl.rb1Click(Sender: TObject);
begin
  seKor.Visible := false;
end;

procedure TfmDecl.rbKorClick(Sender: TObject);
begin
  seKor.Visible := True;
end;

procedure TfmDecl.SpeedButton1Click(Sender: TObject);
begin
  if fmCombination = nil then
    fmCombination:=TfmCombination.Create(Self);
  fmCombination.ShowModal;
end;

procedure TfmDecl.tmr1Timer(Sender: TObject);
var b:Boolean;
begin
  b := (cbAllKvart.ItemIndex = -1) or (cbAllKvart.ItemIndex = 0) ;
  btn_decl.Enabled := not b;
  btn_print.Enabled := not b;
  btn_excel.Enabled := not b;
  SpeedButton1.Enabled := not b;
end;

function NoErrRowDel(decl: TDeclaration; var SLErr:TStringList): Boolean;
var s:string;
begin
  Result := false;
  s := '';
  if (decl.RepDate >=dBeg) and (decl.RepDate <= dEnd) then
    begin
      if decl.product.prod.pName = NoProducer then
      begin
        Result := True;
        s:= deStart + deProd + ' ��� ' + String(decl.product.prod.pInn) + ' ��� ' +
            String(decl.product.prod.pKpp) + deNone + deRef;
        exit;
      end;
      if decl.product.Productname = NoProd then
      begin
        Result := True;
        s:= deStart + deProduction + ' EAN13 ' + String(decl.product.EAN13) + deNone + deRef;
        if SLErr.IndexOf(s) = -1 then
          SLErr.Add(s);

        exit;
      end;
      if decl.TypeDecl = 1 then
      begin
        if decl.sal.sName = NoSal then
        begin
          Result := True;
          s:= deStart + deProd + ' ��� ' + String(decl.sal.sInn) + ' ��� ' +
            String(decl.sal.sKpp) + deNone + deRef;
          if SLErr.IndexOf(s) = -1 then
            SLErr.Add(s);
          exit;
        end;
       if (decl.sal.lic.dEnd < dBeg) and (decl.sal.sName <> NoSal)  then
        begin
          s:= deStart + deSal + ' ��� ' + String(decl.sal.sInn) + ' ��� ' +
            String(decl.sal.sKpp) + ' ' + String(decl.sal.sName) + ' ' +
            deNone + deLic + ' (' + DateToStr(decl.sal.lic.dEnd) + ')';
          Result := True;
          if SLErr.IndexOf(s) = -1 then
            SLErr.Add(s);
          exit;
        end;
      end;
    end;
end;

procedure GetDataDecl(var SL, SLErr: TStringList; dB, dE: TDateTime);
var
  i: integer;
  decl: TDeclaration;
begin
  SortSLCol(1,SLAllDecl);
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    decl:=TDecl_obj(SLAllDecl.Objects[i]).Data;
    if (decl.RepDate <= dE)  then
      if not NoErrRowDel(decl, SLErr) then
        begin
          SL.AddObject(SLAllDecl.Strings[i],TDecl_Obj.Create);
          TDecl_Obj(SL.Objects[SL.Count - 1]).data := decl;
        end;
  end;
end;

// ������������ ������ �������������� ����������� � ����������
procedure GetAllProizv(SL: TStringList; var SLPr: TStringList; f11:Boolean);
var
  i: integer;
  mv: TMove;
begin
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if InDecl(string(mv.product.FNSCode),f11) then
      if mv.product.prod.pName <> NoProducer then
        if SLPr.IndexOf(String(mv.product.prod.pId)) = -1 then
        begin
          SLPr.AddObject(string(mv.product.prod.pId),TProducer_Obj.Create);
          TProducer_Obj(SLPr.Objects[SLPr.Count - 1]).data := mv.product.prod;
        end;
  end;
  if SLPr.Count > 0 then
    SLPr.CustomSort(CompNumAsc);
end;

procedure GetAllSaler(SL: TStringList; var SLPost, SalErr: TStringList; f11:Boolean);
var
  i: integer;
  decl: TDeclaration;
  fd:integer;
begin
  if f11 then fd := 11 else fd := 12;

  SalErr.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    //decl.forma := FormDecl(decl.product.FNSCode);
    if (decl.TypeDecl in [1,3]) and (decl.forma = fd) then
      if (decl.sal.sName <> NoSal) and (decl.sal.sInn <> '1234567894') then
        if InDecl(String(decl.product.FNSCode), f11) then
          if SLPost.IndexOf(String(decl.sal.sId)) = -1 then
          begin
            SLPost.AddObject(string(decl.sal.sId),TSaler_Obj.Create);
            TSaler_Obj(SLPost.Objects[SLPost.Count - 1]).data := decl.sal;
          end;
  end;
  if SLPost.Count > 0 then
    SLPost.CustomSort(CompNumAsc);
end;

function OstKPPXML(SL: TStringList; f11: Boolean; kpp: string;
  Data: TDateTime): Extended;
var
  V, vol: Extended;
  i: integer;
  decl: TDeclaration;
begin
  vol := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
      begin
        if InDecl(String(decl.product.FNSCode), f11) and (decl.RepDate < Data) then
        begin
          V := 0.1 * decl.Amount * decl.product.Capacity;
          case decl.TypeDecl of
            1:
              vol := vol + V; // ������
            2:
              vol := vol - V; // ������
            3:
              vol := vol - V; // ������� ����������
            4:
              vol := vol + V; // ������� �� ����������
            5:
              vol := vol - V; // �������� (����)
            6:
              vol := vol - V; // ������� -
            7:
              vol := vol + V; // �������� +
          end;
        end;
      end;
  end;
  Result := vol;
end;

function RashKPPXML(SL: TStringList; f11: Boolean; kpp: string;
  Data: TDateTime): Boolean;
var
  V, vol: Extended;
  i: integer;
  decl: TDeclaration;
  d1, d2:TDateTime;
begin
  vol := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    d1:=Kvart_start(Data-1);
    d2:=Kvart_finish(Data);
    if (decl.DeclDate >=d1) and
      (Int(decl.DeclDate) <= d2) then
      if InDecl(String(decl.product.FNSCode), f11) then
        begin
          V := 0.1 * decl.Amount * decl.product.Capacity;
          case decl.TypeDecl of
            2, 6:
              vol := vol + V; // ������
          end;
        end;
  end;
  if vol > 0 then
    Result := True
  else
    Result := false;
end;


procedure ShapkaKPPXML(node: IXMLNode; f11, af, ob: Boolean; selfkpp: string; OneC:Boolean);
var
  kpp_i: TOrgAdres;
//  oborot: IXMLNode;
begin
  GetKPPAdres(fmWork.que1_sql, selfkpp, kpp_i);
  with kpp_i do
  begin
    if af then
      if Empty(name) then
        name := '��� ' + String(kpp);
    kod_str := ShortString(UseStr(String(kod_str), '', '643'));
    with node do
    begin
      Attributes['����'] := trim(name);
      if Length(fOrg.Inn) = 10  then
        Attributes['�����'] := kpp
      else
        if not OneC then
             Attributes['�����'] := kpp;
      {
      Attributes['��������������'] := Bool2Str(ob);
      }
      with AddChild('������') do
      begin
        AddChild('���������').Text := '643';
        AddChild('������').Text := String(postindex);
        AddChild('���������').Text := String(kod_reg);
        AddChild('�����').Text := String(raion);
        AddChild('�����').Text := String(gorod);
        AddChild('����������').Text := String(naspunkt);
        AddChild('�����').Text := String(ulica);
        AddChild('���').Text := String(dom);
        AddChild('������').Text := String(korpus);
        AddChild('������').Text := String(litera);
        AddChild('�����').Text := String(kvart);
      end;
    end;
  end;
end;


//procedure ShapkaKPPXML(node: IXMLNode; f11, af, ob: Boolean; selfkpp: string; OneC:Boolean);
//var
//  kpp_i: TOrgAdres;
////  oborot: IXMLNode;
//begin
//  GetKPPAdres(fmWork.que1_sql, selfkpp, kpp_i);
//  with kpp_i do
//  begin
//    if af then
//      if Empty(name) then
//        name := '��� ' + String(kpp);
//    kod_str := ShortString(UseStr(String(kod_str), '', '643'));
//    with node do
//    begin
//      Attributes['����'] := name;
//      if Length(fOrg.Inn) = 10  then
//        Attributes['�����'] := kpp
//      else
//        if not OneC then
//             Attributes['�����'] := kpp;
//      Attributes['��������������'] := Bool2Str(ob);
//      with AddChild('������') do
//      begin
//        AddChild('���������').Text := '643';
//        AddChild('������').Text := String(postindex);
//        AddChild('���������').Text := String(kod_reg);
//        AddChild('�����').Text := String(raion);
//        AddChild('�����').Text := String(gorod);
//        AddChild('����������').Text := String(naspunkt);
//        AddChild('�����').Text := String(ulica);
//        AddChild('���').Text := String(dom);
//        AddChild('������').Text := String(korpus);
//        AddChild('������').Text := String(litera);
//        AddChild('�����').Text := String(kvart);
//      end;
//    end;
//  end;
//end;

function OborotDate(d: TDateTime; SL: TStringList): Extended;
var
  i: integer;
  decl: TDeclaration;
  volume: Extended;
begin
  volume := 0;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
        if decl.DeclDate = d then
          if decl.TypeDecl = 1 then
            volume := volume + 0.1 * decl.product.Capacity * decl.Amount;
  end;
  Result := volume;
end;

procedure GetDataNaclXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.DeclNum) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;

procedure GetDataXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if DateToStr(decl.DeclDate) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure PostProdDataNacl(node: IXMLNode; SL: TStringList);
var
  decl: TDeclaration;
  SLNacl, SLData:TStringList;
  i:integer;
  vol: Extended;
begin
  SLNacl := TStringList.Create;
  SLData := TStringList.Create;
  try
    // ���� � ���������� ����������
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]).Data;
      if TDecl_obj(SL.Objects[i]) <> nil then
        if TDecl_obj(SL.Objects[i]) is TDecl_Obj then
          if decl.TypeDecl = 1 then
            if SLNacl.IndexOf(String(decl.DeclNum)) = -1 then
              begin
                SLNacl.AddObject(String(decl.DeclNum), TDecl_obj.Create);
                TDecl_Obj(SLNacl.Objects[SLNacl.Count-1]).Data := decl;
              end;
    end;
    SLNacl.Sort;
    for i := 0 to SLNacl.Count - 1 do
    begin
      decl := TDecl_obj(SLNacl.Objects[i]).Data;
      GetDataNaclXML(String(decl.DeclNum), SL, SLData);
      vol := OborotDate(Int(decl.DeclDate), SLData);
      with node do
        { with AddChild('���������') do }
          with AddChild('��������') do
        begin
          Attributes['�200000000013'] := DateToStr(decl.DeclDate);
          Attributes['�200000000014'] := decl.DeclNum;
          decl.TM := '';
          Attributes['�200000000015'] := '';
          Attributes['�200000000016'] := FloatToStrF(vol, ffFixed, maxint,
            ZnFld); ;
        end;
    end;

  finally
    FreeStringList(SLData);
    FreeStringList(SLNacl);
  end;
end;

//procedure PostProdDataNacl(node: IXMLNode; SL: TStringList);
//var
//  decl: TDeclaration;
//  SLNacl, SLData:TStringList;
//  i:integer;
//  vol: Extended;
//begin
//  SLNacl := TStringList.Create;
//  SLData := TStringList.Create;
//  try
//    // ���� � ���������� ����������
//    for i := 0 to SL.Count - 1 do
//    begin
//      decl := TDecl_obj(SL.Objects[i]).Data;
//      if TDecl_obj(SL.Objects[i]) <> nil then
//        if TDecl_obj(SL.Objects[i]) is TDecl_Obj then
//          if decl.TypeDecl = 1 then
//            if SLNacl.IndexOf(String(decl.DeclNum)) = -1 then
//              begin
//                SLNacl.AddObject(String(decl.DeclNum), TDecl_obj.Create);
//                TDecl_Obj(SLNacl.Objects[SLNacl.Count-1]).Data := decl;
//              end;
//    end;
//    SLNacl.Sort;
//    for i := 0 to SLNacl.Count - 1 do
//    begin
//      decl := TDecl_obj(SLNacl.Objects[i]).Data;
//      GetDataNaclXML(String(decl.DeclNum), SL, SLData);
//      vol := OborotDate(Int(decl.DeclDate), SLData);
//      with node do
//        with AddChild('���������') do
//        begin
//          Attributes['�200000000013'] := DateToStr(decl.DeclDate);
//          Attributes['�200000000014'] := decl.DeclNum;
//          decl.TM := '';
//          Attributes['�200000000015'] := '';
//          Attributes['�200000000016'] := FloatToStrF(vol, ffFixed, maxint,
//            ZnFld); ;
//        end;
//    end;
//
//  finally
//    FreeStringList(SLData);
//    FreeStringList(SLNacl);
//  end;
//end;

procedure PostProdXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime);
var
  i: integer;
  SLDate, SLDataNacl: TStringList;
  decl: TDeclaration;

//  s:string;
begin
  try
    try
      SLDate := TStringList.Create;
      SLDataNacl := TStringList.Create;

      // ���� � ������ ����������
      for i := 0 to SL.Count - 1 do
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        if TDecl_obj(SL.Objects[i]) <> nil then
          if TDecl_obj(SL.Objects[i]) is TDecl_Obj then
            if decl.TypeDecl = 1 then
              if DBetween(Int(decl.RepDate), d1, d2) then
                if SLDate.IndexOf(DateTimeToStr(decl.DeclDate)) = -1 then
                  begin
                    SLDate.AddObject(DateTimeToStr(decl.DeclDate), TDecl_obj.Create);
                    TDecl_Obj(SLDate.Objects[SLDate.Count-1]).Data := decl;
                  end;
      end;

      SLDate.CustomSort(CompDateAsc);
      {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := '.';

      // ������� ������� �� ����
      for i := 0 to SLDate.Count - 1 do
      begin
        decl := TDecl_obj(SLDate.Objects[i]).Data;
        GetDataXML(DateToStr(decl.DeclDate), SL, SLDataNacl);
        PostProdDataNacl(node, SLDataNacl);
      end;
      {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := ',';
    except
    end;
  finally
//    SLDate.Free;
    FreeStringList(SLDate);
    FreeStringList(SLDataNacl)
  end;
end;

procedure GetDataSalXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.sal.sId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;


procedure SvedPrImpPostXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime;
  f11: Boolean);
var
  i: integer;
  SLPrImp, SLKppGrupImpPostI: TStringList;
  Post: IXMLNode;
  decl: TDeclaration;
begin
  try
    try
      // �������� ������ ����������, ������� ��������� � �������
      SLPrImp := TStringList.Create;
      SLKppGrupImpPostI := TStringList.Create;
      for i := 0 to SL.Count - 1 do
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        if (decl.TypeDecl = 1) then
        begin
          // ���������� ������ ���� ��������� �������
          if DBetween(Int(decl.RepDate), d1, d2) then
            if decl.sal.sInn <> '1234567894' then
            begin
              if SLPrImp.IndexOf(String(decl.sal.sId)) = -1 then
                begin
                  SLPrImp.AddObject(String(decl.sal.sId), TDecl_obj.Create);
                  TDecl_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := decl;
                end;
            end;
        end;
      end;

      // ��������� ������ �� ���� ���������
      SLPrImp.Sort;
      // SortSL(SLPrImp, 9, CompNumAsc);

      with node do
      begin
        if SLPrImp.Count > 0 then
          for i := 0 to SLPrImp.Count - 1 do
          begin
            decl := TDecl_obj(SLPrImp.Objects[i]).Data;

            GetDataSalXML(String(decl.sal.sId), SL, SLKppGrupImpPostI);
            Post := AddChild('���������');
            if decl.sal.sId <> '' then
              with Post do
              begin
                Attributes['�N'] := IntToStr(i + 1);
                Attributes['������������'] := decl.sal.sId;
                {
                if f11 then
                  if decl.sal.lic.RegOrg <> NoLic then
                    Attributes['����������'] := decl.sal.lic.lId
                  else
                    Attributes['����������'] := '';
                 }

                // ��������� ������ ������� ����������
                PostProdXML(Post, SLKppGrupImpPostI, d1, d2);
              end;
          end;

      end;
    except
    end;
  finally
    FreeStringList(SLKppGrupImpPostI);
    FreeStringList(SLPrImp);
  end;
end;

//procedure SvedPrImpPostXML(node: IXMLNode; SL: TStringList; d1, d2: TDateTime;
//  f11: Boolean);
//var
//  i: integer;
//  SLPrImp, SLKppGrupImpPostI: TStringList;
//  Post: IXMLNode;
//  decl: TDeclaration;
//begin
//  try
//    try
//      // �������� ������ ����������, ������� ��������� � �������
//      SLPrImp := TStringList.Create;
//      SLKppGrupImpPostI := TStringList.Create;
//      for i := 0 to SL.Count - 1 do
//      begin
//        decl := TDecl_obj(SL.Objects[i]).Data;
//        if (decl.TypeDecl = 1) then
//        begin
//          // ���������� ������ ���� ��������� �������
//          if DBetween(Int(decl.RepDate), d1, d2) then
//            if decl.sal.sInn <> '1234567894' then
//            begin
//              if SLPrImp.IndexOf(String(decl.sal.sId)) = -1 then
//                begin
//                  SLPrImp.AddObject(String(decl.sal.sId), TDecl_obj.Create);
//                  TDecl_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := decl;
//                end;
//            end;
//        end;
//      end;
//
//      // ��������� ������ �� ���� ���������
//      SLPrImp.Sort;
//      // SortSL(SLPrImp, 9, CompNumAsc);
//
//      with node do
//      begin
//        if SLPrImp.Count > 0 then
//          for i := 0 to SLPrImp.Count - 1 do
//          begin
//            decl := TDecl_obj(SLPrImp.Objects[i]).Data;
//
//            GetDataSalXML(String(decl.sal.sId), SL, SLKppGrupImpPostI);
//            Post := AddChild('���������');
//            if decl.sal.sId <> '' then
//              with Post do
//              begin
//                Attributes['�N'] := IntToStr(i + 1);
//                Attributes['������������'] := decl.sal.sId;
//                if f11 then
//                  if decl.sal.lic.RegOrg <> NoLic then
//                    Attributes['����������'] := decl.sal.lic.lId
//                  else
//                    Attributes['����������'] := '';
//
//                // ��������� ������ ������� ����������
//                PostProdXML(Post, SLKppGrupImpPostI, d1, d2);
//              end;
//          end;
//
//      end;
//    except
//    end;
//  finally
//    FreeStringList(SLKppGrupImpPostI);
//    FreeStringList(SLPrImp);
//  end;
//end;

//function Dvizenie(SL: TStringList; var dvol: TMove; d1, d2: TDateTime): Boolean;
//var
//  i: integer;
//  V, ost: Extended;
//  d: TDateTime;
//  decl: TDeclaration;
//begin
//  { Result := false;
//    V := 0;
//    ost := 0;
//    dvol.dv.ost0 := 0;
//    dvol.dv.prih := 0;
//    dvol.dv.rash := 0;
//    dvol.dv.vozv_post := 0;
//    dvol.dv.vozv_pokup := 0;
//    dvol.dv.brak := 0;
//    dvol.dv.ost1 := 0;
//    for i := 0 to SL.Count - 1 do
//    begin
//    SLRow := Pointer(SL.Objects[i]);
//    d := StrToDateTime(SLRow.Strings[21]); // REPORTDATE
//    dv := StrToInt(SLRow.Strings[23]); // DECLTYPE
//    Amount := StrToFloat(SLRow.Strings[22]);
//    Capacity := StrToFloat(SLRow.Strings[4]);
//    V := 0.1 * Capacity * Amount;
//    if (d < d1) then
//    begin
//    case dv of
//    1:
//    ost := ost + V; // ������
//    2:
//    ost := ost - V; // ������
//    3:
//    ost := ost - V; // ������� ����������
//    4:
//    ost := ost + V; // ������� �� ����������
//    5:
//    ost := ost - V; // �������� (����)
//    end;
//    end
//    else if DBetween(d, d1, d2) then
//    begin
//    case dv of
//    1:
//    dvol.prih := dvol.prih + V; // ������
//    2:
//    dvol.rash := dvol.rash + V; // ������
//    3:
//    dvol.vozv_post := dvol.vozv_post + V; // ������� ����������
//    4:
//    dvol.vozv_pokup := dvol.vozv_pokup + V; // ������� �� ����������
//    5:
//    dvol.brak := dvol.brak + V; // �������� (����)
//    end;
//    end;
//    end;
//
//    dvol.ost0 := ost;
//    dvol.ost1 := dvol.ost0 + dvol.prih - dvol.rash - dvol.vozv_post +
//    dvol.vozv_pokup - dvol.brak;
//
//    with dvol do
//    if (ost0 < 0) or (prih < 0) or (rash < 0) or (vozv_post < 0) or
//    (vozv_pokup < 0) or (brak < 0) or (ost1 < 0) then
//    Result := True; }
//end;



function GenErr(dv:TMove):string;
var s:string;
begin
  Result:='';
  if not MinusMove(dv) then exit;
  s:='������� �� �� = ' + FloatToStrF(dv.dv_dal.ost0, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dv.dv_dal.prih, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dv.dv_dal.rash, ffFixed, maxint, ZnFld) +
     ' �����.����. = ' + FloatToStrF(dv.dv_dal.vozv_post, ffFixed, maxint, ZnFld) +
     ' �����.�����. = ' + FloatToStrF(dv.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld) +
     ' ���� = ' + FloatToStrF(dv.dv_dal.brak, ffFixed, maxint, ZnFld) +
     ' �������� (-) = ' + FloatToStrF(dv.dv_dal.move_out, ffFixed, maxint, ZnFld) +
     ' �������� (+) = ' + FloatToStrF(dv.dv_dal.move_in, ffFixed, maxint, ZnFld) +
     ' ������� �� �� = ' + FloatToStrF(dv.dv_dal.ost1, ffFixed, maxint, ZnFld);
  Result:=s;
end;


function DvizPostXML(node: IXMLNode; SL, SLErr: TStringList; f11: Boolean;
  d1, d2: TDateTime): Boolean;
var
  dvol: TMove;
//  idx: integer;
  decl: TDeclaration;
  s, s1:string;
//  e:extended;
begin
  try
    DvizenieOneEAN(SL, d1, d2, dvol);
    //������������� ������ � �������(���� ����)
    s:=GenErr(dvol);
    if s <> '' then
      begin
        decl:=TDecl_obj(SL.Objects[0]).data;
        s1:=String('��� = ' + decl.SelfKpp +' ��. �� = ' + decl.product.FNSCode + ' ID = ' + decl.product.prod.pId +  ' ��� = ' +
             decl.product.prod.pInn + ' ��� = ' + decl.product.prod.pKpp + ' ' +
             ' ' + decl.product.prod.pName);
        SLErr.Add(s1);
        SLErr.Add(s);
        SLErr.Add('');
      end;
    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := '.';
    with node do
      with AddChild('��������') do
        if f11 then
        begin
          Attributes['�N'] := '1';
          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed, maxint, ZnFld);
          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
          Attributes['�100000000009'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
          Attributes['�100000000011'] := FloatToStrF(dvol.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld);
          Attributes['�100000000012'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000013'] := FloatToStrF(dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
          Attributes['�100000000015'] := FloatToStrF(dvol.dv_dal.rash, ffFixed, maxint, ZnFld);
          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.brak, ffFixed, maxint, ZnFld);
          Attributes['�100000000017'] := FloatToStrF(dvol.dv_dal.vozv_post, ffFixed, maxint, ZnFld);
          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.move_out,ffFixed, maxint, ZnFld);
          Attributes['�100000000019'] := FloatToStrF(dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +  dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000020'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
         {
          Attributes['�100000000021'] := FloatToStrF(dvol.dv_dal.ostAM,ffFixed, maxint, ZnFld);
         }
        end
        else
        begin
          Attributes['�N'] := '1';
          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed,
            maxint, ZnFld);
          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed,
            maxint, ZnFld);
          Attributes['�100000000009'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
          Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.prih, ffFixed,
            maxint, ZnFld);
          Attributes['�100000000011'] := FloatToStrF(dvol.dv_dal.vozv_pokup,
            ffFixed, maxint, ZnFld);
          Attributes['�100000000012'] := FloatToStrF(dvol.dv_dal.move_in,
            ffFixed, maxint, ZnFld);
          Attributes['�100000000013'] := FloatToStrF
            (dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in,
            ffFixed, maxint, ZnFld);
          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.rash, ffFixed,
            maxint, ZnFld);
          Attributes['�100000000015'] := FloatToStrF
            (dvol.dv_dal.brak + dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.vozv_post,
            ffFixed, maxint, ZnFld);
          Attributes['�100000000017'] := FloatToStrF
            (dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +
              dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
//          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.ost1, ffFixed,
//            maxint, ZnFld);
          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed,
            maxint, ZnFld);
          //----------------------------------------
          Attributes['�100000000019'] := FloatToStrF(dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +  dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
          Attributes['�100000000020'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);

        end;
    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := ',';
  finally
    dvol.Free;
  end;
end;


//function DvizPostXML(node: IXMLNode; SL, SLErr: TStringList; f11: Boolean;
//  d1, d2: TDateTime): Boolean;
//var
//  dvol: TMove;
////  idx: integer;
//  decl: TDeclaration;
//  s, s1:string;
////  e:extended;
//begin
//  try
//    DvizenieOneEAN(SL, d1, d2, dvol);
//    //������������� ������ � �������(���� ����)
//    s:=GenErr(dvol);
//    if s <> '' then
//      begin
//        decl:=TDecl_obj(SL.Objects[0]).data;
//        s1:=String('��� = ' + decl.SelfKpp +' ��. �� = ' + decl.product.FNSCode + ' ID = ' + decl.product.prod.pId +  ' ��� = ' +
//             decl.product.prod.pInn + ' ��� = ' + decl.product.prod.pKpp + ' ' +
//             ' ' + decl.product.prod.pName);
//        SLErr.Add(s1);
//        SLErr.Add(s);
//        SLErr.Add('');
//      end;
//    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := '.';
//    with node do
//      with AddChild('��������') do
//        if f11 then
//        begin
//          Attributes['�N'] := '1';
//          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
//          Attributes['�100000000009'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld);
//          Attributes['�100000000011'] := FloatToStrF(dvol.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld);
//          Attributes['�100000000012'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000013'] := FloatToStrF(dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
//          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in, ffFixed, maxint, ZnFld);
//          Attributes['�100000000015'] := FloatToStrF(dvol.dv_dal.rash, ffFixed, maxint, ZnFld);
//          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.brak, ffFixed, maxint, ZnFld);
//          Attributes['�100000000017'] := FloatToStrF(dvol.dv_dal.vozv_post, ffFixed, maxint, ZnFld);
//          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.move_out,ffFixed, maxint, ZnFld);
//          Attributes['�100000000019'] := FloatToStrF(dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +  dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
//          Attributes['�100000000020'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
//            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
//            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
//          Attributes['�100000000021'] := FloatToStrF(dvol.dv_dal.ostAM,ffFixed, maxint, ZnFld);
//        end
//        else
//        begin
//          Attributes['�N'] := '1';
//          Attributes['�100000000006'] := FloatToStrF(dvol.dv_dal.ost0, ffFixed,
//            maxint, ZnFld);
//          Attributes['�100000000007'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000008'] := FloatToStrF(dvol.dv_dal.prih, ffFixed,
//            maxint, ZnFld);
//          Attributes['�100000000009'] := FloatToStrF(0, ffFixed, maxint, ZnFld);
//          Attributes['�100000000010'] := FloatToStrF(dvol.dv_dal.prih, ffFixed,
//            maxint, ZnFld);
//          Attributes['�100000000011'] := FloatToStrF(dvol.dv_dal.vozv_pokup,
//            ffFixed, maxint, ZnFld);
//          Attributes['�100000000012'] := FloatToStrF(dvol.dv_dal.move_in,
//            ffFixed, maxint, ZnFld);
//          Attributes['�100000000013'] := FloatToStrF
//            (dvol.dv_dal.prih + dvol.dv_dal.vozv_pokup + dvol.dv_dal.move_in,
//            ffFixed, maxint, ZnFld);
//          Attributes['�100000000014'] := FloatToStrF(dvol.dv_dal.rash, ffFixed,
//            maxint, ZnFld);
//          Attributes['�100000000015'] := FloatToStrF
//            (dvol.dv_dal.brak + dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
//          Attributes['�100000000016'] := FloatToStrF(dvol.dv_dal.vozv_post,
//            ffFixed, maxint, ZnFld);
//          Attributes['�100000000017'] := FloatToStrF
//            (dvol.dv_dal.rash + dvol.dv_dal.brak + dvol.dv_dal.vozv_post +
//              dvol.dv_dal.move_out, ffFixed, maxint, ZnFld);
////          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.ost1, ffFixed,
////            maxint, ZnFld);
//          Attributes['�100000000018'] := FloatToStrF(dvol.dv_dal.ost0 + dvol.dv_dal.prih - dvol.dv_dal.rash -
//            dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup - dvol.dv_dal.brak +
//            dvol.dv_dal.move_in - dvol.dv_dal.move_out, ffFixed,
//            maxint, ZnFld);
//        end;
//    {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator := ',';
//  finally
//    dvol.Free;
//  end;
//end;

procedure GetDataOProizvXML(s: string; SL: TStringList;
  var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.product.prod.pId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;


procedure SvedPrImpXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean;
  d1, d2: TDateTime);
var
  SvedPrImp: IXMLNode;
  i: integer;
  SLPrImp, SLKppGrupPrI, SLOKppGrupPrI: TStringList;
  idp: string;
//  d: TDateTime;
//  minus: Boolean;
  prod: TProducer;
  mv:TMove;
//  decl:TDeclaration;
begin
  try
    try
      SLPrImp := TStringList.Create;
      SLKppGrupPrI := TStringList.Create;
      SLOKppGrupPrI := TStringList.Create;
//      SLPrImp.clear;
      ClearSL(SLPrImp);
      SLPrImp.Duplicates := dupIgnore;

      // ���������� ������ �������������� ��� ������ ������ ��
//      for i := 0 to SL.Count - 1 do
//      begin
//        decl := TDecl_obj(SL.Objects[i]).Data;
//        // ���������� ������ �������� � �������� ������
//        if decl.DeclDate <= d2 then
//          if SLPrImp.IndexOf(String(decl.product.prod.pId)) = -1 then
//            begin
//              SLPrImp.AddObject(String(decl.product.prod.pId), TProducer_Obj.Create);
//              TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := decl.product.prod;
//            end;
//      end;

      for i := 0 to SLO.Count - 1 do
      begin
        mv := TMove(SLO.Objects[i]);
        if SLPrImp.IndexOf(String(mv.product.prod.pId)) = -1 then
          begin
            SLPrImp.AddObject(String(mv.product.prod.pId), TProducer_Obj.Create);
            TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := mv.product.prod;
          end;
      end;

      // ��������� ���� �� ���� ������������
      SLPrImp.CustomSort(CompNumAsc);

      for i := 0 to SLPrImp.Count - 1 do
        with node do
        begin
          prod := TProducer_Obj(SLPrImp.Objects[i]).Data;
          idp := SLPrImp.Strings[i];
          GetDataProizvXML(idp, SL, SLKppGrupPrI);
          GetDataOProizvXML(idp, SLO, SLOKppGrupPrI);


          SvedPrImp := AddChild('������������������');

          {
            SvedPrImp := AddChild('����������������');
          }

          with SvedPrImp do
          begin
            Attributes['�N'] := IntToStr(i + 1);
            Attributes['�����������'] := prod.pId;
            // �������� ������ ���������� ��� ������ ������ �� � ������� �������������
            SvedPrImpPostXML(SvedPrImp, SLKppGrupPrI, d1, d2, f11);
            // ��������
            DvizPostXML(SvedPrImp, SLKppGrupPrI, SLErr, f11, d1, d2);
          end;
        end;
    except
    end;
  finally
    FreeStringList(SLKppGrupPrI);
    FreeStringList(SLPrImp);
  end;
end;


//procedure SvedPrImpXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean;
//  d1, d2: TDateTime);
//var
//  SvedPrImp: IXMLNode;
//  i: integer;
//  SLPrImp, SLKppGrupPrI, SLOKppGrupPrI: TStringList;
//  idp: string;
////  d: TDateTime;
////  minus: Boolean;
//  prod: TProducer;
//  mv:TMove;
////  decl:TDeclaration;
//begin
//  try
//    try
//      SLPrImp := TStringList.Create;
//      SLKppGrupPrI := TStringList.Create;
//      SLOKppGrupPrI := TStringList.Create;
////      SLPrImp.clear;
//      ClearSL(SLPrImp);
//      SLPrImp.Duplicates := dupIgnore;
//
//      // ���������� ������ �������������� ��� ������ ������ ��
////      for i := 0 to SL.Count - 1 do
////      begin
////        decl := TDecl_obj(SL.Objects[i]).Data;
////        // ���������� ������ �������� � �������� ������
////        if decl.DeclDate <= d2 then
////          if SLPrImp.IndexOf(String(decl.product.prod.pId)) = -1 then
////            begin
////              SLPrImp.AddObject(String(decl.product.prod.pId), TProducer_Obj.Create);
////              TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := decl.product.prod;
////            end;
////      end;
//
//      for i := 0 to SLO.Count - 1 do
//      begin
//        mv := TMove(SLO.Objects[i]);
//        if SLPrImp.IndexOf(String(mv.product.prod.pId)) = -1 then
//          begin
//            SLPrImp.AddObject(String(mv.product.prod.pId), TProducer_Obj.Create);
//            TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := mv.product.prod;
//          end;
//      end;
//
//      // ��������� ���� �� ���� ������������
//      SLPrImp.CustomSort(CompNumAsc);
//
//      for i := 0 to SLPrImp.Count - 1 do
//        with node do
//        begin
//          prod := TProducer_Obj(SLPrImp.Objects[i]).Data;
//          idp := SLPrImp.Strings[i];
//          GetDataProizvXML(idp, SL, SLKppGrupPrI);
//          GetDataOProizvXML(idp, SLO, SLOKppGrupPrI);
//
//          SvedPrImp := AddChild('����������������');
//          with SvedPrImp do
//          begin
//            Attributes['�N'] := IntToStr(i + 1);
//            Attributes['�����������'] := prod.pId;
//            // �������� ������ ���������� ��� ������ ������ �� � ������� �������������
//            SvedPrImpPostXML(SvedPrImp, SLKppGrupPrI, d1, d2, f11);
//            // ��������
//            DvizPostXML(SvedPrImp, SLKppGrupPrI, SLErr, f11, d1, d2);
//          end;
//        end;
//    except
//    end;
//  finally
//    FreeStringList(SLKppGrupPrI);
//    FreeStringList(SLPrImp);
//  end;
//end;

procedure GetDataGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.FNSCode) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;

procedure GetDataOGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
  SLFiltr.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.FNS) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;

procedure OborotKppXML(node: IXMLNode; SL,SLO, SLErr: TStringList; f11: Boolean;
  d1, d2: TDateTime);
var
  i: integer;
  grup: string;
  SLGroup, SLKppGrupI, SLOKppGrupI: TStringList;
  obor: IXMLNode;
  mv:TMove;
begin
  SLGroup := TStringList.Create;
  SLKppGrupI := TStringList.Create;
  SLOKppGrupI := TStringList.Create;
  try
    try
      // ����������� ���� �� ������������ � ���
//      for i := 0 to SL.Count - 1 do
//      begin
//        NullDecl(decl);
//        decl := TDecl_obj(SL.Objects[i]).Data;
//        if TDecl_obj(SL.Objects[i]) <> nil then
//          if TDecl_obj(SL.Objects[i]) is TDecl_obj then
//          begin
//            // ���������� ������ �������� � �������� ������
//            if DBetween(decl.RepDate, d1-1, d2) then
//              if SLGroup.IndexOf(String(decl.product.FNSCode)) = -1 then
//                if InDecl(String(decl.product.FNSCode), f11) then
//                  SLGroup.Add(String(decl.product.FNSCode));
//          end;
//      end;

      for i := 0 to SLO.Count - 1 do
      begin
        mv := TMove(SLO.Objects[i]);
        if mv.FNS <> '' then
          if SLGroup.IndexOf(String(mv.FNS)) = -1 then
            if InDecl(String(mv.FNS), f11) then
              SLGroup.Add(String(mv.FNS));
      end;

      // ���������� ����� ����� �� ���������� (�������� ������ ��� �����)
      SLGroup.CustomSort(CompNumAsc);

      // ���������� �� ������� ��
      for i := 0 to SLGroup.Count - 1 do
        with node do
        begin
          obor := AddChild('������');
          with obor do
          begin
            grup := SLGroup.Strings[i];
            Attributes['�N'] := IntToStr(i + 1);
            Attributes['�000000000003'] := grup;
            // ���������� ����� ������� ���������� ��� ������ ������ ��
            GetDataGroupXML(grup, SL, SLKppGrupI);
            GetDataOGroupXML(grup, SLO, SLOKppGrupI);
            // �������������
            SvedPrImpXML(obor, SLKppGrupI,SLOKppGrupI, SLErr, f11, d1, d2);
          end;
        end;

    except
    end;
  finally
//    FreeStringList(SLOKppGrupI);
//    SLOKppGrupI.Free;
    FreeStringList(SLKppGrupI);
    SLGroup.Free;
  end;
end;

procedure KPPXML(node: IXMLNode; selfkpp: string; SL,SLO, SLErr: TStringList;
  f11, af: Boolean; d1, d2: TDateTime; OneC:Boolean);
var
  ost_d1, ost_d2: Extended;
  ob: Boolean;
begin
  // ���������� �������� �� ������ � ����� �������
  ost_d1 := OstKPPXML(SL, f11, selfkpp, d1);
  ost_d2 := OstKPPXML(SL, f11, selfkpp, d2+1);

  ost_d1:=OkruglDataDecl(ost_d1);
  ost_d2:=OkruglDataDecl(ost_d2);
  if (ost_d1 <> ost_d2) then
    ob := True
  else
  begin
    if not RashKPPXML(SL, f11, selfkpp, d2) then
      begin
        if ost_d2 <> 0 then
          ob := True
        else
          ob:=False;
      end
    else
      ob := true;
  end;
  // ��������� ��� ���
  ShapkaKPPXML(node, f11, af, ob, selfkpp, OneC);
  // ���������� ������� �� ���
  OborotKppXML(node, SL, SLO, SLErr, f11, d1, d2);
end;

function NullOrg(var IsOrg, IsKpp:Boolean): Boolean;
var i:integer;
begin
  Result := false;
  IsOrg := False;
  IsKpp := False;
  if fOrg.Kpp0 = '' then
    Result:=True;
  if (fOrg.ruk.RukF = '') or (fOrg.ruk.RukI = '') or (fOrg.ruk.RukO = '') or
    (fOrg.buh.BuhF = '') or (fOrg.buh.BuhI = '') or (fOrg.buh.BuhO = '') or
    (fOrg.NPhone = '') or (fOrg.SelfName = '') or (fOrg.Address = '') or
    (fOrg.email = '') or (fOrg.Lic.SerLic = '') or (fOrg.Lic.NumLic = '') or
    (fOrg.Lic.OrgLic = '') then
    IsOrg :=True;
  for i := 0 to High(aKpp) do
    if (aKpp[i].namename = '') or (aKpp[i].IndexP = '') or (aKpp[i].Codereg = '') then
      IsKpp := True;
  if IsOrg or IsKpp then
    Result :=True;
end;

procedure GetDataKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.selfkpp) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;

procedure GetDataOKPPXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  mv: TMove;
begin
//  ClearSL(SLFiltr);
  SLFiltr.clear;
  for i := 0 to SL.Count - 1 do
  begin
    mv := TMove(SL.Objects[i]);
    if String(mv.selfkpp) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TMove.Create);
        SLFiltr.Objects[SLFiltr.Count-1] := mv;
      end;
  end;
end;

procedure GetDataEANXML(s: string; SL: TStringList; var SLFiltr: TStringList;
  idx: integer);
var
  i: integer;
  decl: TDeclaration;
begin
  SLFiltr.clear;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.EAN13) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;

procedure DvizenieEANXML(SL: TStringList; ean: string; d1, d2: TDateTime;
  var err: string);
var
  i: integer;
  V, ost: Extended;
//  ean_d: string;
  dvol: TMove;
  decl: TDeclaration;
begin
  //SLEan.clear;
  dvol := TMove.Create;
  try
    V := 0;
    ost := 0;
    dvol.dv_dal.ost0 := 0;
    dvol.dv_dal.prih := 0;
    dvol.dv_dal.rash := 0;
    dvol.dv_dal.vozv_post := 0;
    dvol.dv_dal.vozv_pokup := 0;
    dvol.dv_dal.brak := 0;
    dvol.dv_dal.ost1 := 0;
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]).Data;
      if String(decl.product.EAN13) = ean then
      begin
        V := 0.1 * decl.product.Capacity * decl.Amount;
        if (decl.RepDate < d1) then
        begin
          case decl.TypeDecl of
            1:
              ost := ost + V; // ������
            2:
              ost := ost - V; // ������
            3:
              ost := ost - V; // ������� ����������
            4:
              ost := ost + V; // ������� �� ����������
            5:
              ost := ost - V; // �������� (����)
            6:
              ost := ost - V; // �������� -
            7:
              ost := ost + V; // �������� +
          end;
        end
        else if DBetween(Int(decl.RepDate), d1, d2) then
        begin
          case decl.TypeDecl of
            1:
              dvol.dv_dal.prih := dvol.dv_dal.prih + V; // ������
            2:
              dvol.dv_dal.rash := dvol.dv_dal.rash + V; // ������
            3:
              dvol.dv_dal.vozv_post := dvol.dv_dal.vozv_post + V;
            // ������� ����������
            4:
              dvol.dv_dal.vozv_pokup := dvol.dv_dal.vozv_pokup + V;
            // ������� �� ����������
            5:
              dvol.dv_dal.brak := dvol.dv_dal.brak + V; // �������� (����)
            6:
              dvol.dv_dal.move_out := dvol.dv_dal.move_out + V;
            // �������� (����)
            7:
              dvol.dv_dal.move_in := dvol.dv_dal.move_in + V;
            // �������� (����)
          end;
        end;
      end;
    end;
    dvol.dv_dal.ost0 := ost;
    dvol.dv_dal.ost1 := dvol.dv_dal.ost0 + dvol.dv_dal.prih -
      dvol.dv_dal.rash - dvol.dv_dal.vozv_post + dvol.dv_dal.vozv_pokup -
      dvol.dv_dal.brak + dvol.dv_dal.move_in - dvol.dv_dal.move_out;

    if MinusMove(dvol) then
      err:= '������� �� �� = ' + FloatToStrF(dvol.dv_dal.ost0, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dvol.dv_dal.prih, ffFixed, maxint, ZnFld) +
     ' ������ = ' + FloatToStrF(dvol.dv_dal.rash, ffFixed, maxint, ZnFld) +
     ' �����.����. = ' + FloatToStrF(dvol.dv_dal.vozv_post, ffFixed, maxint, ZnFld) +
     ' �����.�����. = ' + FloatToStrF(dvol.dv_dal.vozv_pokup, ffFixed, maxint, ZnFld) +
     ' ���� = ' + FloatToStrF(dvol.dv_dal.brak, ffFixed, maxint, ZnFld) +
     ' �������� (-) = ' + FloatToStrF(dvol.dv_dal.move_out, ffFixed, maxint, ZnFld) +
     ' �������� (+) = ' + FloatToStrF(dvol.dv_dal.move_in, ffFixed, maxint, ZnFld) +
     ' ������� �� �� = ' + FloatToStrF(dvol.dv_dal.ost1, ffFixed, maxint, ZnFld);
  finally
    dvol.Free;
  end;
end;

procedure DvizenirEANKpp(kpp: string; SLKpp, SLErr: TStringList;
  d1, d2: TDateTime; f11: Boolean; pb: TProgressBar);
var
  SLEanKpp, SLEan: TStringList;
  i: integer;
  s, s1: string;
  decl: TDeclaration;
begin
  SLEanKpp := TStringList.Create;
  SLEan := TStringList.Create;
  try
    try
      // ������ ��� ��� ������������� � ������ ���
      for i := 0 to SLKpp.Count - 1 do
      begin
        decl := TDecl_obj(SLKpp.Objects[i]).Data;
        if InDecl(String(decl.product.FNSCode), f11) then
          if SLEanKpp.IndexOf(String(decl.product.EAN13)) = -1 then
            begin
              SLEanKpp.AddObject(String(decl.product.EAN13), TDecl_obj.Create);
              TDecl_Obj(SLEanKpp.Objects[SLEanKpp.Count-1]).Data := decl;
            end;
      end;

      pb.Max := 100;
      for i := 0 to SLEanKpp.Count - 1 do
      begin
        decl := TDecl_obj(SLKpp.Objects[i]).Data;
        pb.Position := Trunc(100 * (i) / SLEanKpp.Count);
        pb.UpdateControlState;
        Application.ProcessMessages;
        // ������� ��� ������ ���������� � ������ ���
        GetDataEANXML(SLEanKpp.Strings[i], SLKpp, SLEan, 2);

        s:='';
        DvizenieEANXML(SLEan, SLEanKpp.Strings[i], d1, d2, s);

        if s <> '' then
          begin
            s1 := '��� = ' + kpp + '   ��� = ' + String(decl.product.EAN13) +
              '   ���� = ' + String(decl.product.FNSCode) + ' ' + s;
            SLErr.Add(s1);
          end;
      end;
      pb.Position := 0;
      pb.UpdateControlState;
      Application.ProcessMessages;
    except
    end;
  finally
    // SLErrEan.Free;
    SLEan.Free;
    SLEanKpp.Free;
  end;
end;

procedure TfmDecl.btn_printClick(Sender: TObject);
var r: TResourceStream;
    ms : TMemoryStream;
    NewPath:string;
    error:integer;
begin
  xls.Clear;
  r := TResourceStream.Create(HInstance, 'res_printf', RT_RCDATA);
  ms := TMemoryStream.Create;
  NewPath := WinTemp + NameProg;
  try
    if not DirectoryExists(NewPath) then
    begin
      {$IOChecks off}
      MkDir(NewPath);
      error := IOResult;
    end;


    ms.LoadFromStream(r);
    NewPath := NewPath+'\print.xls';
    ms.SaveToFile(NewPath);

    xls.OpenFile(NewPath);

    Shapka_PrintTable1(SG_Print_111);
    Shapka_PrintTable2(SG_Print_112);
    Shapka_PrintTable1(SG_Print_121);
    Shapka_PrintTable2(SG_Print_122);

    DeclTable1(True,SG_Print_111, xls, 0);
    DeclTable1(False,SG_Print_121, xls, 2);
    DeclTable2(True,SG_Print_112, xls, 1);
    DeclTable2(False,SG_Print_122, xls, 3);
  finally
    ms.Free;
    r.Free;
    {$IOChecks on}
  end;
end;

function IsNullOrgKpp:Boolean;
var IsOrg, IsKpp:Boolean;
begin
  Result := NullOrg(IsOrg, IsKpp);
  if Result then
  begin
    if fOrg.Kpp0 = '' then
      Show_Inf
        ('��� ���������� ���������� ���������� ������� �������� ��� �����������', fInfo);

    if IsOrg and (not IsKpp) then
      Show_Inf
        ('��� ���������� ���������� ���������� ��������� ������ �����������', fInfo);

    if IsKpp and (not IsOrg) then
      Show_Inf
        ('��� ���������� ���������� ���������� ��������� ������ ��� (������������, ��� �������, ������)', fInfo);

    if (not IsKpp) and (not IsOrg) then
      Show_Inf
        ('��� ���������� ���������� ���������� ��������� ������ ����������� � ������ ��� (������������, ��� �������, ������)', fInfo);

    fmWork.sb_OrgClick(Application);
  end;
end;

function IsMassErr:Boolean;
begin
  Result:=False;
  if (masErr[0] > 0) or (masErr[1] > 0) or (masErr[2] > 0) or (masErr[3] > 0) or
     (masErr[4] > 0) or (masErr[5] > 0) or (masErr[6] > 0)  then
  begin
    Show_Inf
      ('� ����� �������� ��������� ���������� ������!!!' +  #10#13 +
       '��������� �� ����� ������������� ����������', fError);
    Result :=True;
    fmWork.sb_MainClick(Application);
  end;
end;

function IsErrLic(SL:TStringList):Boolean;
var i:Integer;
begin
  Result := False;
  if Sl.Count > 0 then
  begin
    fmDecl.MemoErr.Lines.Add('������ ���������� �� ������ � ����������, ��� ������ � ��������');
    for i := 0 to SL.Count - 1 do
      fmDecl.MemoErr.Lines.Add(SL.Strings[i]);
    fmDecl.MemoErr.Lines.Add('');
    Result:=True;
  end;
end;

procedure TfmDecl.btn1Click(Sender: TObject);
var tFile, sBM:string;
begin
  fmWork.dlgOpen_Import.Title := '����� ����� ���������� XML';
  fmWork.dlgOpen_Import.Filter :=
    '���������� � ������� XML|*.xml';
  try
    try
      if fmWork.dlgOpen_Import.Execute then
      begin
//        tFile := GetTypeFile(ANSIUpperCase(ExtractFileExt(fmWork.dlgOpen_Import.FileName)));
        tFile := fmWork.dlgOpen_Import.FileName;
        if FindEcp then
        begin
          if WriteXML(tFile) then
            begin
              if chk_declBM.Checked then
                begin
                  try
                    if WriteDeclBM(tFile, fOrg.Inn, CurKvart, CurYear) then
                      sBM := #10#13 + '����� ���������� ��������� �� ������� ��';
                  except
                    sBM := #10#13 + '����� ���������� �� ��������� �� ������� ��';
                  end;
                end
              else
                sBM := #10#13 + '����� ���������� �� ��������� �� ������� ��';

              Show_Inf('���������� ���������' + sBM, fInfo);
//              Show_Inf('���������� ���������', fInfo)
            end
          else
            Show_Inf('���������� �� ���������. ������ ������������', fInfo);
        end
        else
          begin
            Show_Inf('��� �� �������, ���������� �� ���������', fInfo);
          end;
      end;
    except
    end;
  finally
  end;
end;


procedure TfmDecl.btn_declClick(Sender: TObject);
var SLDecl,SLErr,SlProizv,SLPost,SlLic,SLMinusEan,SLKppI,SLMinusPr, SLOKppI:TStringList;
  BrowseInfo: TBrowseInfo;
  DisplayName, Path, TempPath: array [0 .. MAX_PATH] of char;
  TitleName, s, d, sn, fname, f_name, sKvart, cKvart, sBM: string;
  lpItemID: PItemIDList;
  Xml: IXMLDocument;
  root, sprav, doc, OO, org: IXMLNode;
  i, ciKvart:Integer;
  err, errXsd:Boolean;
begin
  MemoErr.Clear;
  if IsNullOrgKpp then Exit;
  //�������� ������� � ���������
  GetMinusOst(masErr,SLOstatkiKpp);
  if IsMassErr then Exit;
  CleanRepProducer(fmWork.que2_sql);
  //������������ �������� �����
  btn_printClick(Self);

  SLDecl:=TStringList.Create;
  SLErr := TStringList.Create;
  SlProizv := TStringList.Create;
  SLPost := TStringList.Create;
  SLLic := TStringList.Create;
  SLMinusEan := TStringList.Create;
  SLKppI := TStringList.Create;
  SLMinusPr := TStringList.Create;
  SLOKppI := TStringList.Create;
  try
    Path :='';
    Form7_8 := tForm7_8.Create;
   {$IFDEF RELEASE}
      FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
      BrowseInfo.hwndOwner := fmWork.Handle;
      BrowseInfo.pszDisplayName := @DisplayName;
      TitleName := '�������� ����� ��� ���������� ����������';
      BrowseInfo.lpszTitle := PChar(TitleName);
      BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
      lpItemID := SHBrowseForFolder(BrowseInfo);
      if lpItemID <> nil then
       Begin
        SHGetPathFromIDList(lpItemID, TempPath);
        GlobalFreePtr(lpItemID);
        Path := TempPath;
       End;
   {$ENDIF}


     {$IFDEF DEBUG}
       Path := 'd:\Install';
     {$ENDIF}

      Form7_8.Src.Path := Path;
      AddTToXml;
      Form7_8.toXml(Form7_8.Src);
      MemoErr.Clear;
      MemoErr.Text := Form7_8.memoErr.Text;
      Show_Inf(Form7_8.RezInf, fInfo);

  finally
    XML:=nil;
    SLMinusPr.Free;
    FreeStringList(SLKppI);
    FreeStringList(SLMinusEan);
    SLLic.Free;
    FreeStringList(SLPost);
    FreeStringList(SlProizv);
    SLErr.Free;
    SLDecl.Free;
    Form7_8.Free;
  end;
end;



end.
