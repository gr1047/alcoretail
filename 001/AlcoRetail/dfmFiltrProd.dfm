object fmFiltrProd: TfmFiltrProd
  Left = 0
  Top = 0
  Caption = #1060#1080#1083#1100#1090#1088' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
  ClientHeight = 295
  ClientWidth = 675
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poNone
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 675
    Height = 73
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 14
      Top = 0
      Width = 332
      Height = 18
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103' '#1080#1083#1080' '#1048#1053#1053
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object eInn: TEdit
      Left = 14
      Top = 21
      Width = 646
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = editChange
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 254
    Width = 675
    Height = 41
    Align = alBottom
    TabOrder = 1
    object TBCancel: TBitBtn
      Left = 544
      Top = 6
      Width = 123
      Height = 24
      Hint = #1054#1090#1084#1077#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      Caption = #1054#1090#1084#1077#1085#1072
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = TBCancelClick
    end
    object TBSave: TBitBtn
      Left = 414
      Top = 6
      Width = 124
      Height = 24
      Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      Caption = 'OK'
      Default = True
      DoubleBuffered = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentDoubleBuffered = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = TBSaveClick
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 73
    Width = 675
    Height = 181
    Align = alClient
    TabOrder = 2
    DesignSize = (
      675
      181)
    object Label2: TLabel
      Left = 14
      Top = 6
      Width = 261
      Height = 18
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1080' '#1048#1053#1053'  '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 14
      Top = 60
      Width = 137
      Height = 18
      Caption = #1050#1055#1055' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lb1: TLabel
      Left = 14
      Top = 114
      Width = 148
      Height = 18
      Caption = #1040#1076#1088#1077#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cbFullName: TComboBox
      Left = 14
      Top = 30
      Width = 646
      Height = 24
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = cbFullNameChange
    end
    object cbKPP: TComboBox
      Left = 14
      Top = 84
      Width = 646
      Height = 24
      Style = csDropDownList
      Anchors = [akLeft, akTop, akRight]
      DropDownCount = 20
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = cbKPPChange
    end
    object edt_Adr: TEdit
      Left = 14
      Top = 138
      Width = 646
      Height = 26
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = 16056309
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
    end
  end
end
