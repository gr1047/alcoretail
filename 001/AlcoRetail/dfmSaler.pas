unit dfmSaler;

interface

uses
  Forms, StdCtrls, Grids, AdvObj, BaseGrid, AdvGrid, ComCtrls, Buttons,
  Controls, Classes, ExtCtrls, ShlObj, XMLDoc, XMLIntf, SysUtils;

type
  TfmSaler = class(TForm)
    Panel1: TPanel;
    pgc1: TPageControl;
    ts_EAN: TTabSheet;
    ts_KppEan: TTabSheet;
    Panel2: TPanel;
    Panel3: TPanel;
    SpeedButton5: TSpeedButton;
    SG_SalerFNS: TAdvStringGrid;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Label2: TLabel;
    dtpDEnd: TDateTimePicker;
    Panel8: TPanel;
    Label1: TLabel;
    Panel9: TPanel;
    Panel10: TPanel;
    dtpDBeg: TDateTimePicker;
    cbAllKvart: TComboBox;
    Panel11: TPanel;
    SpeedButton1: TSpeedButton;
    Panel12: TPanel;
    Label6: TLabel;
    Button1: TButton;
    SG_Saler: TAdvStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure cbAllKvartChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SG_SalerFilterSelect(Sender: TObject; Column, ItemIndex: Integer;
      FriendlyName: string; var FilterCondition: string);
    procedure SG_SalerFNSFilterSelect(Sender: TObject; Column,
      ItemIndex: Integer; FriendlyName: string; var FilterCondition: string);
    procedure SG_SalerFNSGetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
    procedure SG_SalerGetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
    procedure FormDestroy(Sender: TObject);
    procedure btn_SalXMLClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillSaler(dB, dE:TDateTime);
  public
    { Public declarations }
    procedure CreateSaler;
  end;

var
  fmSaler: TfmSaler;
  dsEnd, dsBeg:TDateTime;
  SLSaler, SLSalerFNS :TStringList;
implementation

uses Global, GlobalUtils, GlobalConst, dfmWork;
{$R *.dfm}

procedure TfmSaler.btn_SalXMLClick(Sender: TObject);
var
  tFile, sId, sName, s, b, pInn, pKpp, pName, IdPr, IdPol :string;
  Xml: IXMLDocument;
  nFile, NRef, nContr, nC, nRez, nUl, NDoc, nOO, nOrg, nRekv, tmp,
    nO, nPr, nPol, nPost:IXMLNode;
  CountContr, CountOO, i,j,k,l,m, CountOb, CountPr, CountPol, CountPost,
    idx, idxContr:integer;
  decl:TDeclaration;
  SLContr, SLPr, SLRow : TStringList;
begin
  fmWork.dlgOpen_Import.Title := '����� ����� ����������';
  fmWork.dlgOpen_Import.Filter := '���� XML|*.xml';
  SLContr:=TStringList.Create;
  SLPr:=TStringList.Create;
  if fmWork.dlgOpen_Import.Execute then
    begin
      tFile := fmWork.dlgOpen_Import.FileName;
      try
        Xml := TXMLDocument.Create(nil);
        Xml.Active := false;
        Xml.Options := Xml.Options + [doNodeAutoIndent];

        Xml.LoadFromFile(tFile);

        NFile := Xml.DocumentElement; // �������� ��� ����
        if nFile <> nil then
          begin
            NRef := NFile.ChildNodes.FindNode('�����������');
            NDoc := NFile.ChildNodes.FindNode('��������');
            if nRef <> nil then
              begin
                nContr := NRef.ChildNodes.FindNode('�����������');
                if nContr <> nil then
                  begin
                    CountContr := nRef.ChildNodes.Count;
                    for i:=0 to CountContr - 1 do
                      begin
                        if NRef.ChildNodes.Nodes[i].NodeName = '�����������'  then
                          begin
                            nC := NRef.ChildNodes.Nodes[i];
                            sId:= String(nC.Attributes['�������']);
                            sName:= String(nC.Attributes['�000000000007']);
                            nRez := nC.ChildNodes.FindNode('��������');
                            if nRez <> nil then
                              begin
                                nUl := nRez.ChildNodes.FindNode('��');
                                if nUl <> nil then
                                  begin
                                    SLRow:=TStringList.Create;
                                    SLRow.Add(sName);
                                    SLRow.Add(String(nUl.Attributes['�000000000009']));
                                    SLRow.Add(String(nUl.Attributes['�000000000010']));
                                    SLContr.AddObject(sId,Pointer(SLRow));
                                  end;
                              end;
                          end;
                        if NRef.ChildNodes.Nodes[i].NodeName = '����������������������'  then
                          begin
                            nC := NRef.ChildNodes.Nodes[i];
                            SLRow:=TStringList.Create;
                            s:=String(nC.Attributes['�����������']);
                            pInn:=String(nC.Attributes['�000000000005']);
                            case Length(pInn) of
                              10:
                                begin
                                  pKpp:=String(nC.Attributes['�000000000006']);
                                  pName := String(nC.Attributes['�000000000004']);
                                end;
                              9:
                                begin
                                  pInn := MoroInn;
                                  pKpp := MoroKpp;
                                  pName := MoroName;
                                end;
                            end;
                            SLRow.Add(pName);  //Name
                            SLRow.Add(pInn);  //Inn
                            SLRow.Add(pKpp);   //Kpp
                            SLPr.AddObject(s, Pointer(SLRow));
                          end;
                      end;
                    idxContr := -1;
                    for i := 0 to SLContr.Count - 1 do
                      begin
                        SLRow:= Pointer(SLContr.Objects[i]);
                        if SLRow.Strings[1] = fOrg.Inn then
                          idxContr := i;
                      end;
                    if idxContr = -1 then
                      begin
                        Show_Inf('� ����� ���������� ��� ����������� � ��� ' + fOrg.Inn, fError);
                        exit;
                      end;
                  end;
              end;
            if NDoc <> nil then
              begin
                nOrg := NDoc.ChildNodes.FindNode('�����������');
                nOO := NDoc.ChildNodes.FindNode('������������');
                if nOrg <> nil then
                  begin
                    nRekv := nOrg.ChildNodes.FindNode('���������');
                    if nRekv <> nil then
                      decl.sal.sInn := ShortString(nRekv.Attributes['�����']);
                  end;
                if nOO <> nil then
                  begin
                    CountOO := NDoc.ChildNodes.Count;
                    for i:=0 to CountOO - 1 do
                      if NDoc.ChildNodes.Nodes[i].NodeName = '������������'  then
                        begin
                          tmp:=NDoc.ChildNodes.Nodes[i];
                          decl.sal.sKpp := ShortString(tmp.Attributes['�����']);
                          decl.sal.sName := ShortString(tmp.Attributes['������']);
                          b:=String(tmp.Attributes['���������������']);
                          if UpperCase(b) = 'TRUE'  then
                            begin
                              CountOb := tmp.ChildNodes.Count;
                              for j := 0 to CountOb - 1 do
                                begin
                                  nO:=tmp.ChildNodes.Nodes[j];
                                  s:= nO.NodeName;
                                  if s = '������' then
                                    begin
                                      decl.product.FNSCode := ShortString(nO.Attributes['�000000000003']);
                                      nPr := nO.ChildNodes.FindNode('����������������');
                                      if nPr <> nil then
                                        begin
                                          CountPr := nO.ChildNodes.Count;
                                          for k := 0 to CountPr - 1 do
                                            begin
                                              IdPr := String(nPr.Attributes['�����������']);
                                              nPol := nPr.ChildNodes.FindNode('����������');
                                              if nPol <> nil then
                                                begin
                                                  CountPol := nPr.ChildNodes.Count;
                                                  for l := 0 to CountPol - 1 do
                                                    begin
                                                      nPol := nPr.ChildNodes.Nodes[l];
                                                      IdPol:= String(nPol.Attributes['������������']);
                                                      idx := SLContr.IndexOf(IdPol);
                                                      if idx <> -1 then
                                                        begin
                                                          slRow:=Pointer(SLContr.Objects[idx]);
                                                          if SLRow.Strings[1] = fOrg.Inn then
                                                            begin
                                                              nPost := nPol.ChildNodes.FindNode('��������');
                                                              if nPost <> nil then
                                                                begin
                                                                  CountPost:=nPol.ChildNodes.Count;
                                                                  for m := 0 to CountPost - 1 do
                                                                    begin
                                                                      nPost := nPol.ChildNodes.Nodes[m];
                                                                      s:= String(nPost.Attributes['�000000000018']);
                                                                      decl.DeclDate := StrToDate(s);
                                                                      s:= String(nPost.Attributes['�000000000019']);
                                                                      decl.DeclNum := ShortString(s);
                                                                      idx := SLPr.IndexOf(IdPr);
                                                                      if idx <> -1 then
                                                                        begin
                                                                          SLRow := Pointer(SLPr.Objects[idx]);
                                                                          decl.product.prod.pName := ShortString(SLRow.Strings[0]);
                                                                          decl.product.prod.pInn := ShortString(SLRow.Strings[1]);
                                                                          decl.product.prod.pKpp := ShortString(SLRow.Strings[2]);
                                                                        end;
                                                                    end;
                                                                end;
                                                            end;
                                                        end;
                                                    end;
                                                end;
                                            end;
                                        end;
                                    end;
                                end;
                            end;
                        end;
                  end;
              end;
          end;
    finally
      Xml := nil;
      SLPr.Free;
      SLContr.Free;
    end;
  end;
end;

procedure TfmSaler.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmSaler.cbAllKvartChange(Sender: TObject);
var
  s: string;
  cKvart, cYear: string;
begin
  s := '';
  if (Sender is TComboBox) then
    if (Sender as TComboBox).ItemIndex <> -1 then
    begin
      s := (Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
    end;
  if Length(s) > 0 then
  begin
    cKvart := Copy(s, 0, pos(nkv, s) - 2);
    cYear := Copy(s, Length(s) - 8, 4);
    dsBeg := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
    dsEnd := Kvart_End(StrToInt(cKvart), StrToInt(cYear));
    dtpDBeg.Date := dsBeg;
    dtpDEnd.Date := dsEnd;
  end;

  FillSaler(dsBeg, dsEnd);
end;

procedure TfmSaler.FillSaler(dB, dE:TDateTime);
begin
  FreeStringList(SLSaler);
  FreeStringList(SLSalerFNS);

  SLSaler:=TStringList.Create;
  SLSalerFNS:=TStringList.Create;
  DvizenieSalerSKpp(SLSaler,SLAllDecl,dB,de);
  ShowDataSaler(SG_Saler,SLSaler,NameColSaler,NoResColSaler,CalcColSaler, HideColSaler, FilterColSaler);
  DvizenieSalerFNSSKpp(SLSalerFNS,SLAllDecl,db,de);
  ShowDataSalerFNS(SG_SalerFNS,SLSalerFNS,NameColSalerFNS,NoResColSaler,CalcColSalerFNS, HideColSaler,FilterColSaler);
end;

procedure TfmSaler.FormCreate(Sender: TObject);
begin
  SLSaler:=TStringList.Create;
  SLSalerFNS:=TStringList.Create;
  CreateSaler;
end;

procedure TfmSaler.CreateSaler;
begin
  AssignCBSL(cbAllKvart,SLPerRegYear);
  cbAllKvart.ItemIndex := idxKvart;

  dsBeg := Kvart_Beg(curKvart, NowYear);
  dsEnd := Kvart_End(curKvart, NowYear);

  dtpDBeg.Date := dsBeg;
  dtpDEnd.Date := dsEnd;

  FillSaler(dsBeg, dsEnd);
end;
procedure TfmSaler.FormDestroy(Sender: TObject);
begin
  FreeStringList(SLSaler);
  FreeStringList(SLSalerFNS);
end;

procedure TfmSaler.SG_SalerFilterSelect(Sender: TObject; Column,
  ItemIndex: Integer; FriendlyName: string; var FilterCondition: string);
begin
  if FilterCondition = '��������' then
    FilterCondition := '';
end;

procedure TfmSaler.SG_SalerFNSFilterSelect(Sender: TObject; Column,
  ItemIndex: Integer; FriendlyName: string; var FilterCondition: string);
begin
  if FilterCondition = '��������' then
    FilterCondition := '';
end;

procedure TfmSaler.SG_SalerFNSGetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
begin
if not (Column in [1,2,3,5]) then Filter.Clear;
end;

procedure TfmSaler.SG_SalerGetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
begin
  if not (Column in [1,2,3]) then Filter.Clear;
end;

procedure TfmSaler.SpeedButton1Click(Sender: TObject);
begin
  dsBeg := dtpDBeg.Date;
  dsEnd:= dtpDEnd.Date;
  FillSaler(dsBeg, dsEnd);
end;

procedure TfmSaler.SpeedButton5Click(Sender: TObject);
begin
  fmWork.dlgSave_Export.Filter := '������� � XLS|*.xls';
  if fmWork.dlgSave_Export.Execute then
    begin
      if pgc1.ActivePageIndex = 0 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_Saler,5);
      if pgc1.ActivePageIndex = 1 then SaveXLS(fmWork.dlgSave_Export.FileName, SG_SalerFNS,5);
    end;
end;



end.
