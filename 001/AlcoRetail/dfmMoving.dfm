object fmMoving: TfmMoving
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1074#1085#1091#1090#1088#1080' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 409
  ClientWidth = 803
  Color = 15794161
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pKPP: TPanel
    Left = 0
    Top = 0
    Width = 803
    Height = 36
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object Panel2: TPanel
      Left = 273
      Top = 5
      Width = 280
      Height = 26
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 141
        Height = 26
        Align = alClient
        Alignment = taCenter
        Caption = #1050#1055#1055' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 114
        ExplicitHeight = 18
      end
      object cbRecipient: TComboBox
        Left = 141
        Top = 0
        Width = 139
        Height = 26
        Align = alRight
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = cbRecipientChange
      end
    end
    object Panel3: TPanel
      Left = 5
      Top = 5
      Width = 268
      Height = 26
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 0
        Top = 0
        Width = 129
        Height = 26
        Align = alClient
        Alignment = taCenter
        Caption = #1050#1055#1055' '#1080#1089#1090#1086#1095#1085#1080#1082#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
        ExplicitWidth = 104
        ExplicitHeight = 18
      end
      object cbSource: TComboBox
        Left = 129
        Top = 0
        Width = 139
        Height = 26
        Align = alRight
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = cbSourceChange
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 374
    Width = 803
    Height = 35
    Align = alBottom
    TabOrder = 1
    object btn_Ok: TButton
      Left = 424
      Top = 3
      Width = 148
      Height = 25
      Caption = #1089#1086#1079#1076#1072#1090#1100' '#1072#1074#1090#1086#1088#1072#1089#1093#1086#1076
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
    end
    object Button1: TButton
      Left = 590
      Top = 3
      Width = 100
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 176
      Height = 33
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object SpeedButton11: TSpeedButton
        Left = 25
        Top = 0
        Width = 96
        Height = 33
        Align = alLeft
        Caption = #1053#1072#1081#1090#1080
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        Glyph.Data = {
          F6060000424DF606000000000000360000002800000018000000180000000100
          180000000000C0060000120B0000120B00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFE9E9E9E9E9E9E9E9E9E9E9E9E8E8E8E8E8E8E8E8E8E8E8E8E8E8
          E8E7E7E7E7E7E7E7E7E7E7E7E7E7E7E7E8E8E8E9EAE99A9B9A838685C2C4C2E7
          E7E7FFFFFFFFFFFFFFFFFFF3F3F3A7A7A7A6A6A6A6A6A6A6A6A6A5A5A5A5A5A5
          A4A4A4A3A3A3A3A3A3A1A1A1A1A1A1A1A1A1A0A0A0A1A1A1A4A4A4A7A8A88D8F
          8E9CA19FA4A8A6C2C4C2FFFFFFFFFFFFFFFFFF929292D8D8D8D0D0D0D0D0D0D2
          D2D2D1D1D1CECECECBCBCBCACACACACACAC8C8C8C5C5C5C2C2C2C4C3C3BFBFBF
          7E807F7F8180BFC1C0E7EDEA9CA19E787C79FFFFFFFFFFFFFFFFFF7D7D7DFFFF
          FFEFEFEFF0F0F0F6F6F6F7F7F7FCFCFCFBFBFCEAEAEADBDCDCD8D9D9DFE0DFF8
          F8F8F5F4F4A1A4A3979A98B8B8B8ADACACACAEAD989C9AABADABFFFFFFFFFFFF
          FFFFFF828282FAFAFAE7E7E7E8E8E8F0F0F0DCDCDCCFD0D0BABDBCC4C8C8C6CC
          CDC3CACBC2C6C7B9BCBC9699989DA09FB9BAB9A7A7A7999A99898C8AE2E3E2FD
          FDFDFFFFFFFFFFFFFFFFFF868686FAFAFAE7E7E7E8E8E8F1F1F1D6D6D6A2A7A6
          D3D6D5DACCBFC9A181C59772D5BBA5D9D6D2C3C9C8BFC1C0A8A7A78B8C8C6467
          66E5E6E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8A8A8AFBFBFBE7E7E7E8E8E8F1
          F1F1AEB2B2D8DBDACFAC90BD8F6ACAAF96CCB7A4C8A88DC19571E6DDD3CDD4D3
          8E9190AEB0AF727272FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8D8D8DFAFA
          FAEAEAEAECECECD6D7D7B8BEBDCBB5A1BB8C65D8C3AED8C7B7D8CCC0DBD0C5CC
          B29ABA906FC2C1BA919797FFFFFF7C7C7CF8F8F8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF909090FCFCFCC9C9C9ECECECBCBFBFBCBFBBBA8D68DAC1A9E6D8CBE5D9
          CEE1D5C9D9CABADDCBB9C0926CC3B2A0A6ADADF8F8F87B7B7BF8F8F8FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF939393FAFAFAE1E1E1EFEEEFB7BAB9BCBEB8B8855C
          E5D3C3EDE2D8EEE4DBEDE4DBE0D1C3D4BEA8BC8E68C9B6A4AFB5B5F1F2F27D7D
          7DF8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF969696F9F9F9E4E4E4EDEDEDC1
          C3C3C7C9C6C6A082E4CEB8F8F0E7F6EFE7F5EDE4F3E8DDE6CDB6C19169DACDC0
          A8B0AFFAFAFA7E7E7EF8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9B9B9BF9F9
          F9E4E4E4E8E8E8DCDCDCB6BDBCDCCDBEC19672EEE1D6F8F4EEF5EFE7EDE0D4C8
          A382CCAA8DD4D7D6B7BBBAFFFFFF7E7E7EF8F8F8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF9D9D9DF9F9F9E3E3E3E5E5E5F0F0F0AEB1B0C8CECFDBC3AFC8A180D5B9
          9ED6B89DCCA688CCA688E3E2DFA0A7A6E0E1E0FFFFFF808080F8F8F8FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFA1A1A1F8F8F8E2E2E2E4E4E4ECECECD7D8D7B1B4B3
          C7CDCEDDD8D1D7BAA3D4B298DDCEBFD6DBDBAFB3B2C9CAC9EEEEEEFDFDFD8282
          82F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4F7F7F7DFDFDFE4E4E4EB
          EBEBD7D7D7C9CACAB0B1B1AFB4B3B0B8BBB0B9BBB1B7B8ABAEACBDBDBDD7D6D6
          EBEBEBFDFDFD848484F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A7A7F9F9
          F9C6C6C6E0E0E0EAEAEADADADAD7D7D7DBDBDBD6D6D6D3D3D3D3D3D3D5D5D5DB
          DBDBDCDCDCDBDBDBE9E9E9FCFCFC858585F8F8F8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFA9A9A9F6F6F6E2E2E2E3E3E3E9E9E9D6D6D6D0D0D0D3D3D3D3D3D3D3D3
          D3D4D4D4D4D4D4D4D4D4D4D4D4D6D6D6E8E8E8FCFCFC878787F8F8F8FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFACACACF5F5F5DEDEDEE1E1E1E8E8E8D3D3D3CCCCCC
          CECECECECECECECECECFCFCFCFCFCFCFCFCFCFCFCFD2D2D2E6E6E6FAFAFA8787
          87F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFAFAFF6F6F6DDDDDDE0E0E0E7
          E7E7D7D7D7D3D3D3D5D5D5D5D5D5D5D5D5D6D6D6D6D6D6D6D6D6D6D6D6D8D8D8
          E5E5E5FAFAFA888888F8F8F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB1B1B1F5F5
          F5DCDCDCDFDFDFE5E5E5D2D2D2CCCCCCCDCDCDCECECECECECECECECECFCFCFCF
          CFCFCFCFCFD1D1D1E3E3E3F9F9F9898989F8F8F8FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFB1B1B1FAFAFAE1E1E1E4E4E4EAEAEAE9E9E9EBEBEBECECECEDEDEDEDED
          EDEEEEEEEFEFEFEFEFEFEFEFEFEFEFEFEAEAEAFFFFFF868686F8F8F8FFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFBEBEBEE7E7E7E5E5E5E4E4E4E6E6E6E4E4E4E3E3E3
          E2E2E2E0E0E0DFDFDFDEDEDEDCDCDCDBDBDBDADADAD8D8D8D6D6D6DBDBDB9C9C
          9CFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E3E3B5B5B5B6B6B6B7B7B7B5
          B5B5B2B2B2AEAEAEABABABA7A7A7A3A3A39F9F9F9B9B9B989898949494909090
          8C8C8C8C8C8CD5D5D5FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        ParentFont = False
        OnClick = SpeedButton11Click
        ExplicitHeight = 26
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 25
        Height = 33
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 36
    Width = 803
    Height = 338
    Align = alClient
    BorderWidth = 5
    TabOrder = 2
    object SG_Moving: TAdvStringGrid
      Left = 6
      Top = 6
      Width = 791
      Height = 326
      Cursor = crDefault
      Align = alClient
      DefaultRowHeight = 21
      DrawingStyle = gdsClassic
      FixedCols = 0
      RowCount = 5
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goColSizing, goEditing]
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
      HoverRowCells = [hcNormal, hcSelected]
      OnGetCellColor = SG_MovingGetCellColor
      OnCanEditCell = SG_MovingCanEditCell
      OnGetEditorType = SG_MovingGetEditorType
      OnCheckBoxClick = SG_MovingCheckBoxClick
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = [fsBold]
      CellNode.TreeColor = clSilver
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.Buttons = <>
      Filter = <>
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDownClear = '(All)'
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedFooters = 1
      FixedColWidth = 35
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.2f'
      FloatingFooter.Visible = True
      MouseActions.DisjunctRowSelect = True
      Multilinecells = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'MS Sans Serif'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'MS Sans Serif'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'MS Sans Serif'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'MS Sans Serif'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      SearchFooter.FindNextCaption = 'Find next'
      SearchFooter.FindPrevCaption = 'Find previous'
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = 'Close'
      SearchFooter.HintFindNext = 'Find next occurence'
      SearchFooter.HintFindPrev = 'Find previous occurence'
      SearchFooter.HintHighlight = 'Highlight occurences'
      SearchFooter.MatchCaseCaption = 'Match case'
      ShowDesignHelper = False
      SortSettings.DefaultFormat = ssAutomatic
      Version = '7.2.0.0'
      ColWidths = (
        35
        127
        95
        82
        90)
    end
  end
end
