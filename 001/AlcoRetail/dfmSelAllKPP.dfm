object fmSelAllKPP: TfmSelAllKPP
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1042#1099#1073#1086#1088' '#1050#1055#1055' '#1076#1083#1103' '#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1080#1103' '#1078#1091#1088#1085#1072#1083#1072
  ClientHeight = 308
  ClientWidth = 295
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object chklst_KPP: TCheckListBox
    Left = 0
    Top = 0
    Width = 295
    Height = 241
    OnClickCheck = chklst_KPPClickCheck
    Align = alTop
    Columns = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 18
    ParentFont = False
    TabOrder = 0
  end
  object btn_Cancel: TBitBtn
    Left = 189
    Top = 265
    Width = 98
    Height = 35
    Caption = #1054#1090#1084#1077#1085#1072
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    Kind = bkCancel
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
  end
  object btn1: TBitBtn
    Left = 72
    Top = 265
    Width = 98
    Height = 35
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    Kind = bkOK
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
    TabStop = False
  end
  object mm1: TMainMenu
    Left = 200
    Top = 224
    object mniN1: TMenuItem
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1077
      OnClick = mniN1Click
    end
    object mniN2: TMenuItem
      Caption = #1057#1085#1103#1090#1100' '#1074#1089#1077' '#1074#1077#1076#1077#1083#1077#1085#1080#1077
      OnClick = mniN2Click
    end
  end
end
