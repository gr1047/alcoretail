unit dfmShowInf;

interface

uses
  Forms, StdCtrls, Controls, Graphics, Classes, ExtCtrls;

type
  TfmShowInf = class(TForm)
    Err: TImage;
    Inf: TImage;
    Warn: TImage;
    Conf: TImage;
    Lbl: TLabel;
    Butt1: TButton;
    Butt2: TButton;
    Edit1: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmShowInf: TfmShowInf;

implementation

{$R *.dfm}

procedure TfmShowInf.FormShow(Sender: TObject);
begin
  if Edit1.Visible then
    if Edit1.Enabled then
      Edit1.SetFocus;
end;

end.
