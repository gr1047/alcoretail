unit dfmInvent;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, AdvObj, BaseGrid, AdvGrid, ComCtrls,
  ToolWin, ImgList, Clipbrd;

type
  TfmInvent = class(TForm)
    pnl1: TPanel;
    lb1: TLabel;
    lblVer: TLabel;
    btn1: TButton;
    il1: TImageList;
    il2: TImageList;
    il3: TImageList;
    pnl2: TPanel;
    pnl3: TPanel;
    lbl3: TLabel;
    dtp_WorkDate: TDateTimePicker;
    lb2: TLabel;
    cbKPP: TComboBox;
    pnl4: TPanel;
    tlb_Decl: TToolBar;
    btn_Save: TToolButton;
    btnsep1: TToolButton;
    btn_Find: TToolButton;
    btn_Replace: TToolButton;
    btnsep2: TToolButton;
    btn_Copy: TToolButton;
    btn_Past: TToolButton;
    btnsep3: TToolButton;
    btn_AddR: TToolButton;
    btn_DelR: TToolButton;
    btn_DelAll: TToolButton;
    btn_DupR: TToolButton;
    btnsep6: TToolButton;
    btnAutoRash: TToolButton;
    btnsep7: TToolButton;
    SG_Invent: TAdvStringGrid;
    procedure btn1Click(Sender: TObject);
    procedure btn_FindClick(Sender: TObject);
    procedure btn_ReplaceClick(Sender: TObject);
    procedure btn_PastClick(Sender: TObject);
    procedure btn_DupRClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInvent: TfmInvent;
  RowCopy: Integer;

implementation
  uses dfmWork, dfmReplace, Global;
{$R *.dfm}

procedure TfmInvent.btn1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
end;

procedure TfmInvent.btn_DupRClick(Sender: TObject);
begin
  DupRow(SG_Invent.Row, SG_Invent);
end;

procedure TfmInvent.btn_FindClick(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  fmReplace.SGFind := SG_Invent;
  fmReplace.Show;
end;

procedure TfmInvent.btn_PastClick(Sender: TObject);
var s: string;
begin
    s := Clipboard.AsText;
    PastCell(s, SG_Invent);
end;

procedure TfmInvent.btn_ReplaceClick(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 1;
  fmReplace.Caption := '��������';
  fmReplace.ClientHeight := 175;
  fmReplace.SGFind := SG_Invent;
  fmReplace.Show;
end;

end.
