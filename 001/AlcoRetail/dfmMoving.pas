unit dfmMoving;

interface

uses
  Windows, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, AdvObj, BaseGrid, AdvGrid, ExtCtrls, Buttons;

type
  TTypeFormOst = (fMoving, fAutoRash);
  TfmMoving = class(TForm)
    pKPP: TPanel;
    Panel2: TPanel;
    cbRecipient: TComboBox;
    Label1: TLabel;
    Panel3: TPanel;
    Label2: TLabel;
    cbSource: TComboBox;
    Panel4: TPanel;
    btn_Ok: TButton;
    Button1: TButton;
    Panel5: TPanel;
    SG_Moving: TAdvStringGrid;
    Panel6: TPanel;
    SpeedButton11: TSpeedButton;
    Panel7: TPanel;
    procedure cbSourceChange(Sender: TObject);
    procedure SG_MovingGetEditorType(Sender: TObject; ACol, ARow: Integer;
      var AEditor: TEditorType);
    procedure SG_MovingCheckBoxClick(Sender: TObject; ACol, ARow: Integer;
      State: Boolean);
    procedure SG_MovingGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure SG_MovingCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure cbRecipientChange(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    KppSourse, KppRecipient:string;
    TypeF:TTypeFormOst;
  end;

var
  fmMoving: TfmMoving;


implementation

uses GlobalUtils, Global, dfmReplace;
{$R *.dfm}

procedure TfmMoving.SG_MovingCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  CanEdit := ACol = 0;
end;

procedure TfmMoving.cbRecipientChange(Sender: TObject);
begin
   KppRecipient := cbRecipient.Items.Strings[cbRecipient.ItemIndex];
end;

procedure TfmMoving.cbSourceChange(Sender: TObject);
begin
  if cbSource.ItemIndex <> -1 then
    begin
      AssignCBSL(cbRecipient,SLKPP);
      cbRecipient.Items.Delete(cbSource.ItemIndex);
      cbRecipient.ItemIndex := 0;
      SG_Moving.RowCount := SG_Moving.FixedFooters + SG_Moving.FixedRows;
      KppSourse := cbSource.Items.Strings[cbSource.ItemIndex];
      //���������� ��������� ��� �������� ���
      ShowOstKPP(SLOstatkiKpp,SG_Moving,KppSourse);
      KppRecipient := cbRecipient.Items.Strings[cbRecipient.ItemIndex];
    end;
end;

procedure TfmMoving.FormShow(Sender: TObject);
begin
  SG_Moving.Options := SG_Moving.Options + [goRowSelect, goEditing];
  SG_Moving.ShowSelection := True;
  if (TypeF = fMoving) then
    begin
      pKPP.Visible := True;
      btn_Ok.Caption := '�����������';
      fmMoving.Caption :='����������� ��������� ������ �����������';
      SG_Moving.ColWidths[1] := 20;
    end
  else
    begin
      pKPP.Visible := False;
      btn_Ok.Caption := '������� ������';
      fmMoving.Caption :='�������� ������� � ������� EAN13';
    end;
  SG_Moving.ColWidths[0] := 20;
end;

procedure TfmMoving.SG_MovingCheckBoxClick(Sender: TObject; ACol, ARow: Integer;
  State: Boolean);
var i,c:integer;
begin
  c:=0;
  if ARow = 0 then
    begin
      (Sender as TADVStringGrid).CheckAll(0);
      exit;
    end;
  (Sender as TADVStringGrid).RowSelect[Arow] := State;
  for i := 1 to SG_Moving.RowCount - 1 do
    begin
      if (Sender as TADVStringGrid).GetCheckBoxState(0, i, state) then
      if state then
        begin
          Inc(c);
        end;
    end;
  (Sender as TADVStringGrid).Cells[0,(Sender as TADVStringGrid).RowCount-1]:=IntToStr(C);
  (Sender as TADVStringGrid).Invalidate;
end;

procedure TfmMoving.SG_MovingGetCellColor(Sender: TObject; ARow, ACol: Integer;
  AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  state: boolean;
begin
  if (Sender as TADVStringGrid).GetCheckBoxState(0, ARow, state) then
  begin
    if state then
      ABrush.Color := (Sender as TADVStringGrid).SelectionColor;
  end;
end;

procedure TfmMoving.SG_MovingGetEditorType(Sender: TObject; ACol, ARow: Integer;
  var AEditor: TEditorType);
begin
  case ACol of
    0: AEditor := edCheckBox;
  end;
end;

procedure TfmMoving.SpeedButton11Click(Sender: TObject);
begin
  if fmReplace.Visible then
    fmReplace.Close;
  fmReplace.Notebook1.PageIndex := 0;
  fmReplace.Caption := '�����';
  fmReplace.ClientHeight := 90;
  fmReplace.SGFind:=SG_Moving;
  fmReplace.Show;
end;

end.
