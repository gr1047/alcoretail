object fmProcent: TfmProcent
  Left = 0
  Top = 0
  BorderIcons = []
  Caption = #1055#1088#1086#1094#1077#1085#1090' '#1086#1089#1090#1072#1090#1082#1072
  ClientHeight = 465
  ClientWidth = 426
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btn_Cancel: TBitBtn
    Left = 320
    Top = 422
    Width = 98
    Height = 35
    Caption = #1047#1072#1082#1088#1099#1090#1100
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    Kind = bkCancel
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 0
    TabStop = False
  end
  object btn_Ok: TBitBtn
    Left = 8
    Top = 422
    Width = 185
    Height = 35
    Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1056#1040#1057#1061#1054#1044
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = []
    ModalResult = 1
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
    TabStop = False
  end
  object pnl_FNS: TPanel
    Left = 0
    Top = 90
    Width = 426
    Height = 335
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 5
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object SG_FNS: TAdvStringGrid
      AlignWithMargins = True
      Left = 9
      Top = 9
      Width = 408
      Height = 317
      Cursor = crDefault
      Align = alClient
      ColCount = 4
      Ctl3D = True
      DefaultDrawing = True
      DrawingStyle = gdsClassic
      Enabled = False
      RowCount = 3
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
      ParentCtl3D = False
      ParentShowHint = False
      ScrollBars = ssBoth
      ShowHint = True
      TabOrder = 0
      OnExit = SG_FNSExit
      ActiveRowShow = True
      ActiveRowColor = clSkyBlue
      HoverRowCells = [hcNormal, hcSelected]
      HintShowCells = True
      HintShowLargeText = True
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = []
      ActiveCellColor = clMoneyGreen
      ColumnHeaders.Strings = (
        'N'
        #1050#1086#1076' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        #1055#1088#1086#1094#1077#1085#1090' '#1086#1089#1090#1072#1090#1082#1072)
      ColumnSize.Rows = arFixed
      ColumnSize.Location = clIniFile
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.FixedDropDownButton = True
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.SizeGrip = False
      ControlLook.DropDownFooter.Buttons = <>
      EnhTextSize = True
      ExcelStyleDecimalSeparator = True
      Filter = <>
      FilterDropDown.ColumnWidth = True
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDown.TextChecked = 'Checked'
      FilterDropDown.TextUnChecked = 'Unchecked'
      FilterDropDown.Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFF808080
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF808080FFFFFFFFFFFFFFFFFF80808080808080808080808080808080808080
        8080808080808080808080808080808080808080FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDown.GlyphActive.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF00008000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000
        FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF00800000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFF008000
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        00008000FFFFFFFFFFFFFFFFFF00800000800000800000800000800000800000
        8000008000008000008000008000008000008000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDownClear = #1054#1095#1080#1089#1090#1080#1090#1100
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedColWidth = 28
      FixedRowHeight = 40
      FixedRowAlways = True
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -12
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.0f'
      FloatingFooter.BorderColor = clHotLight
      FloatingFooter.CalculateHiddenRows = False
      FloatingFooter.Column = 5
      Grouping.AutoSelectGroup = True
      Grouping.AutoCheckGroup = True
      HoverFixedCells = hfFixedColumns
      MouseActions.RowSelect = True
      MouseActions.RowSelectPersistent = True
      Navigation.AdvanceOnEnter = True
      Navigation.AllowClipboardRowGrow = False
      Navigation.AllowClipboardColGrow = False
      Navigation.AdvanceAuto = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Tahoma'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollColor = 14084566
      ScrollWidth = 21
      SearchFooter.AutoSearch = False
      SearchFooter.FindNextCaption = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.FindPrevCaption = #1055#1077#1088#1082#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = #1047#1072#1082#1088#1099#1090#1100
      SearchFooter.HintFindNext = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.HintFindPrev = #1055#1088#1077#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.HintHighlight = 'Highlight occurrences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SearchFooter.SearchActiveColumnOnly = True
      SearchFooter.SearchMatchStart = True
      SearchFooter.ShowHighLight = False
      SearchFooter.ShowMatchCase = False
      SelectionColor = 11075496
      SelectionColorMixer = True
      SelectionResizer = True
      SelectionRTFKeep = True
      ShowDesignHelper = False
      SizeWhileTyping.Width = True
      SortSettings.AutoColumnMerge = True
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.Show = True
      SortSettings.IndexShow = True
      SortSettings.InitSortDirection = sdDescending
      SortSettings.FixedCols = True
      SortSettings.SortOnVirtualCells = False
      SyncGrid.SelectionRow = True
      Version = '7.2.0.0'
      ColWidths = (
        28
        87
        104
        64)
      RowHeights = (
        40
        22
        23)
    end
  end
  object pnl_form: TPanel
    Left = 0
    Top = 49
    Width = 426
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 2
    TabOrder = 3
    object pnl_11: TPanel
      Left = 3
      Top = 3
      Width = 220
      Height = 35
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object lb_11: TLabel
        Left = 11
        Top = -1
        Width = 132
        Height = 36
        Caption = '11 '#1092#1086#1088#1084#1072' ('#1082#1088#1077#1087#1082#1080#1077' '#1072#1083#1082'. '#1085#1072#1087#1080#1090#1082#1080')'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object seOst_11: TSpinEdit
        Left = 160
        Top = 3
        Width = 60
        Height = 28
        EditorEnabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxValue = 99
        MinValue = 0
        ParentFont = False
        TabOrder = 0
        Value = 0
        OnExit = seOst_11Exit
      end
    end
    object pnl_12: TPanel
      Left = 223
      Top = 3
      Width = 200
      Height = 35
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object lb_12: TLabel
        Left = 6
        Top = 3
        Width = 116
        Height = 18
        Caption = '12 '#1092#1086#1088#1084#1072' ('#1087#1080#1074#1086')'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object seOst_12: TSpinEdit
        AlignWithMargins = True
        Left = 128
        Top = 3
        Width = 60
        Height = 28
        EditorEnabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        MaxValue = 99
        MinValue = 0
        ParentFont = False
        TabOrder = 0
        Value = 0
        OnExit = seOst_12Exit
      end
    end
  end
  object pnl_RB: TPanel
    Left = 0
    Top = 0
    Width = 426
    Height = 49
    Align = alTop
    TabOrder = 4
    object rb_form: TRadioButton
      Left = 1
      Top = 1
      Width = 424
      Height = 17
      Align = alTop
      Caption = #1054#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1090#1080#1087#1091' '#1076#1077#1082#1083#1072#1088#1072#1094#1080#1080
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      OnClick = rb_formClick
    end
    object rb_fns: TRadioButton
      Left = 1
      Top = 21
      Width = 424
      Height = 27
      Align = alBottom
      Caption = #1054#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1082#1086#1076#1072#1084' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      WordWrap = True
      OnClick = rb_fnsClick
    end
  end
end
