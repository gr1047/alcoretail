unit dfmFiltrSal;

interface

uses
  Forms, ADODB, ExtCtrls, StdCtrls, Controls, Classes, GlobalConst, SysUtils;

type
  TfmFiltrSal = class(TForm)
    Panel1: TPanel;
    eInn: TLabeledEdit;
    Panel2: TPanel;
    le_npr: TLabel;
    Label1: TLabel;
    cbFullName: TComboBox;
    cbKPP: TComboBox;
    Panel3: TPanel;
    btOK: TButton;
    Button2: TButton;
    Timer1: TTimer;
    btn2: TButton;
    procedure eInnChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbFullNameChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SLSaler:TStringList;
    fStat: TEditRecord;
    StrSQLS:string;
  end;

var
  fmFiltrSal: TfmFiltrSal;

implementation

Uses dfmWork, GlobalUtils;
{$R *.dfm}

procedure TfmFiltrSal.btn1Click(Sender: TObject);
begin
  //���������� ������ ����������
end;

procedure TfmFiltrSal.btn2Click(Sender: TObject);
begin
  FormCreate(Self);
end;

procedure TfmFiltrSal.cbFullNameChange(Sender: TObject);
var p:integer;
    StrSQL, s, sName, sInn:string;
    SL:TStringList;
    qr : TADOQuery;
begin
  SL:=TStringList.Create;
  qr:=TADOQuery.Create(fmWork);
  try

    qr.Connection:=fmWork.con2ADOCon;

    if Sender is TComboBox then
      if (Sender as TComboBox).ItemIndex <> -1 then
        begin
          s:=(Sender as TComboBox).Items.Strings[(Sender as TComboBox).ItemIndex];
          p:=Pos('-',s);
          sName:=Copy(s,p+2,Length(s)-p-1);
          sInn:=Copy(s,0,p-2);
          StrSQL:='SELECT ORGKPP FROM SALER WHERE ORGINN=' + '''' + sInn + '''';
          FillSLSQL(qr,StrSQL,SL);
          AssignCBSL(cbKPP,SL);
          if cbKPP.Items.Count=1 then
            cbKPP.ItemIndex:=0;
        end;
  finally
    SL.Free;
    qr.Free;
  end;
end;

procedure TfmFiltrSal.eInnChange(Sender: TObject);
var s, s1, s2:string;
    i:integer;
begin
   s:=eInn.Text;
   s1:= AnsiUpperCase(s);
   cbFullName.Clear;
   cbKPP.Clear;
   if s <> '' then
     for i:=0 to SLSaler.Count - 1 do
       begin
         s2:=AnsiUpperCase(SLSaler.Strings[i]);
         if Pos(s1,s2) >0 then
           cbFullName.Items.AddObject(SLSaler.Strings[i],SLSaler.Objects[i]);
       end;
   if s='' then
     for i:=0 to SLSaler.Count - 1 do
       cbFullName.Items.AddObject(SLSaler.Strings[i],SLSaler.Objects[i]);
   if cbFullName.Items.Count > 0 then
     begin
       cbFullName.ItemIndex := 0;
       cbFullNameChange(cbFullName);
     end;
end;

procedure TfmFiltrSal.FormCreate(Sender: TObject);
var qr : TADOQuery;
begin
  SLSaler:=TStringList.Create;
  fStat := fEdit;
  StrSQLS:='';
  qr:=TADOQuery.Create(fmWork);
  try
    try
      qr.Connection:=fmWork.con2ADOCon;
      FillSLSQL(qr,StrSQLSaler,SLSaler);
      AssignCBSL(cbFullName,SLSaler);
      eInnChange(Application);
    finally
      qr.Free;
    end;
  finally
  end;
end;

procedure TfmFiltrSal.FormDestroy(Sender: TObject);
begin
  SLSaler.Free;
end;

procedure TfmFiltrSal.FormShow(Sender: TObject);
//var StrSQL, s, sInn, sName, sKPP:string;
//    i,idx, p:integer;
//    SL:TStringList;
//    qr : TADOQuery;
begin
//  try
//    try
//      qr:=TADOQuery.Create(fmWork);
//      qr.Connection:=fmWork.con2ADOCon;
//      FillSLSQL(qr,StrSQLSaler,SLSaler);
//      AssignCBSL(cbFullName,SLSaler);
//      eInnChange(Application);
//    finally
//      qr.Free;
//    end;
//  finally
//  end;
end;

procedure TfmFiltrSal.Timer1Timer(Sender: TObject);
begin
  if (cbFullName.ItemIndex=-1) or
     (cbKPP.ItemIndex = -1)then
    btOK.Enabled := False
  else
    btOK.Enabled := True;
end;

end.
