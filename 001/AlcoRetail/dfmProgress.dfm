object fmProgress: TfmProgress
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  ClientHeight = 164
  ClientWidth = 428
  Color = 15794161
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 123
    Width = 428
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object labAbout: TLabel
      Left = 8
      Top = 6
      Width = 159
      Height = 18
      Caption = #1055#1086#1076#1086#1078#1076#1080#1090#1077' '#1085#1077#1084#1085#1086#1075#1086'....'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
  end
  object pb: TProgressBar
    Left = 0
    Top = 106
    Width = 428
    Height = 17
    Align = alBottom
    TabOrder = 1
  end
  object pnl1: TPanel
    Left = 8
    Top = 24
    Width = 412
    Height = 76
    BevelOuter = bvNone
    TabOrder = 2
    object lblcap: TLabel
      Left = 0
      Top = 0
      Width = 412
      Height = 76
      Align = alClient
      Caption = #1055#1086#1076#1086#1078#1076#1080#1090#1077' '#1085#1077#1084#1085#1086#1075#1086'....'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      ExplicitWidth = 159
      ExplicitHeight = 18
    end
  end
end
