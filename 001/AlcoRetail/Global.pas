unit Global;

interface

uses Controls, Forms, ADODB, Classes, AdvObj, GlobalConst, AdvGrid, StdCtrls,
  IniFiles, Registry, IdTCPClient, ComCtrls, XMLIntf, wcrypt2, DbfTable,
  XLSFile, Mask, SysUtils, DB, Graphics, ShlObj, dfmMove, Math, BaseGrid,
  Windows, ActiveX, Variants, XMLDoc, DateUtils, FWZipWriter, ShellApi,
  FWZipReader, XLSWorkbook, XLSFormat, URLMon, WinInet, JvRollOut, XSDValidator;

type
  StringMas = array of String;

var

  DateWin98, OurClient, OnLine, newclient, ExtIniF, LongText, FindEcp,
    IsConnect, SelectCert, VostDecl, VostBase: Boolean;
  EnablQuart: array [0 .. 20] of Boolean;

  CurKvart, CurYear: word;
  EGAIS, NewEGAIS:Boolean;

  ProcOst_11, ProcOst_12, svertka, idxKvart, TypeOst, orgCount: integer;
  MasYear: array of integer;
  masErr: array [0 .. 7] of integer;

  user, exe_version, orgname: string;
//
  db_decl, db_spr, path_db, path_arh, path_ref, ProgrPath, path_exe: string;
  RegHash: string;

  CurDate, dBeg, dEnd: TDateTime;
  NowYear: word;

  SLProductionDecl, SLSalerDecl: TStringList;
  SLPerReg, SLKPP, SLPerRegYear: TStringList;
  SL_ECP, CurSL, SLFNS: TStringList;
  RefProduction, RefSaler, RefProducer, RefLic, SLProcOst: TStringList;
  SLAllDecl, SLOborotKpp, SLOstatkiKpp, DelDecl: TStringList;
  SLDataPrih, SLDataRash, SLDataVPost, SLDataVPok, SLDataBrak,
    SLDataMoving, SLDataOst: TStringList;
  SLErrPrih, SLErrRash, SLErrVPost, SLErrVPok, SLErrBrak: TStringList;
  SLErrXML: TStringList;

  SLCheckKPP:TIntList;

  dError : array of TDeclErr;

  CurSG: TADVStringGrid;
  CurLabel, CurRowLabel: TLabel;

  fSert: TSert;
  fOrg: TOrganization;
  fMainForm: TTypeMainForm;
  aKpp: array of TKpp;
  afs: array of TSert;
  RefsAddEAN, RefsAddINN, DataGrid: array of StringMas;

  // �������� ����� ����������
function Create_DBDecl(DBQ1: TADOQuery; path, pass: string): Boolean;
// �������� ����� ������������
function Create_DBSpr(DBQ2: TADOQuery; path, pass: string): Boolean;
procedure MoveDistribRef(bmz_path, spr_path: string);
// ����� ������� � �����������
function Show_Inf(Text: String; tip: TInformation): integer; overload;
function Show_Inf(Text: String; tip: TInformation; var rez: string): integer;
  overload;
function GetProduction(DBQ: TADOQuery; var production: TProduction;
  var FindProducer: Boolean; BD: Boolean): Boolean;
function GetSaler(DBQ: TADOQuery; var sal: TSaler; var FindLic: Boolean;
  BD: Boolean; Ins:Boolean): Boolean;
function GetProducer(DBQ: TADOQuery; var producer: TProducer;
  BD, Ins: Boolean): Boolean;
// function GetSalerLic(var sal: TSaler): Boolean;
function GetSalerLic(DBQ: TADOQuery; var sal: TSaler; BD: Boolean; Ins:Boolean): Boolean;
function SetBinaryStream(ms1: TMemoryStream; var ms2: TMemoryStream): Boolean;
procedure CreatePath(sINN: string);
function ReadKPP_MS(ms1: TMemoryStream; List: TStringList): Boolean;
function FullNameProduction(ProdName: string; av, c: Extended): string;
function AdressKpp(sKpp: TKpp): string;
procedure UpdateKpp(sKpp: TKpp; DBQ: TADOQuery);
procedure ReadKPP_Ini(List: TStringList);
procedure UpdateRegData(oldIdx:integer);
procedure SetMasKvartal;
procedure GetMonthYearKvart(kv: string; var cKvart, cYear: string);
procedure ChangeMainForm;
function IsChangeSG(SG: TADVStringGrid): Boolean;
// ������ ��������� �� Ini ����� ��� �������
function ReadStringParam(NameParams, value: string): string;
// �������� ������� ����������� � Ini �����
function IsRegIniF(Ini: TIniFile): Boolean;
// �������� ������� ����������� � �������
function IsRegReestr(Reg: TRegistry): Boolean;
function ExistBaseDecl(inn: string): Boolean;
procedure CreateSL;
procedure FreeStringList(var SL: TStringList);
procedure ClearSL(SL: TStringList);
procedure FreeSL;
procedure SetMasYear;
procedure CreateParamBase(NewInn: string);
procedure MemoAdd(Str: String);
function ExistUpdateServer(TCP: TIdTCPClient; inn, ver: string;
  Discon: Boolean): Boolean;
function UpdateApp(RegHost, inn, ver, ProgrPath: string):Boolean;
procedure LongAmount(DBQ: TADOQuery);
procedure SetDataKpp(DBQ: TADOQuery);
procedure InsertKpp(DBQ: TADOQuery);
procedure InsertKppAdres(sKpp: string; DBQ: TADOQuery);
function GetSetECP(OldCrPro: Boolean): Boolean;
function GetDataECP(var fs: TSert): Int64;
procedure CheckBadDateKey(DateBeg, DateEnd: TDateTime);
function FindSert(sINN: string): Boolean;
// ������ ������������� ������������
function SetCurECP(inn: string; var fs: TSert): Boolean;
// ������ ��������� �� Ini ����� ��� �������
function WriteStringParam(NameParams, value: string): string;
procedure NullSert(var fs: TSert);
procedure SetDataFromSert;
procedure SetFIO(var fRuk: TRuk; FIO: string);
function DB2Arh(basefile, arhfile: string): Boolean;
function Upd_LocRef(DBQ1, DBQ2: TADOQuery; pb: TProgressBar;
  le: TLabel): Boolean;
function LocRefProduction(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
function LocRefProducer(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
function LocRefSaler(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
procedure GetDataRef(pb: TProgressBar);
procedure GetDataMas(Mas: integer; StrSQL: String; pb: TProgressBar);
function GetAllDataDecl(DBQ1, DBQ2: TADOQuery; pb: TProgressBar;
  var SL, SLOst: TStringList; le: TLabel): Boolean;
procedure NullDecl(var decl: TDeclaration);
function FormDecl(grup: string): integer;
procedure FilterData(DB, dE: TDateTime; var aErr: array of integer;
  SLOb: TStringList);
function ErrorRowDecl(TypeDecl: integer; decl: TDeclaration;
  SLOb: TStringList; dB, dE:TDateTime): Boolean;
function CheckSelfKpp(val: string): integer;
procedure ChangeKvart(DB, dE: TDateTime);
procedure DvizenieKppEan(var SL: TStringList; SLData: TStringList;
  DB, dE: TDateTime);
procedure DvizenieEAN(var SL: TStringList; SLData: TStringList;
  DB, dE: TDateTime);
procedure DvizenieOneEAN(SL: TStringList; d1, d2: TDateTime; var dviz: TMove);
function OkruglDataDecl(val: Extended):Extended;
function IsNotNullDecl(dM: TMove): Boolean;
procedure FilterOstatki(var SLOst: TStringList; SLData: TStringList;
  var aErr: array of integer);
procedure cbKvartChange(sKvart: string);
function ExistRegPeriod: Boolean;
function SetCurKvart: integer;
procedure SortSLCol(cols: integer; SL: TStringList;
  aCompare: TStringListSortCompare = nil);
function CountNamesSL(SL: TStringList): integer;
procedure FillDataGrid(SG: TADVStringGrid);
procedure WriteRowPrihodSG(var SLErr: TStringList; var decl: TDeclaration;
  SG: TADVStringGrid; row: integer);
procedure PropCellsSG(ASR: TAutoSizeRows; SG: TADVStringGrid;
  CalcCol, NoRes, HideCol: array of integer);
procedure Calc_Footer(SG: TADVStringGrid; masCalc: array of integer);
procedure SetColorCells(SG: TADVStringGrid; ACol: array of integer;
  ARow, err: integer);
function CheckValueCells(val: string): integer;
function CheckDate(dd: TDateTime): integer;
function CheckSaler(sal: TSaler): integer;
procedure AddDataExchange(err: integer; Str: string);
function CheckProduction(product: TProduction): integer;
function CheckChislo(rash: Boolean; val: Extended): integer;
function CheckProducer(prod: TProducer): integer;
function CountErrSG(fTF: TTypeForm; var SLErr: TStringList; SG: TADVStringGrid;
  ARow: integer): Boolean;
procedure ShapkaSG(SG: TADVStringGrid; NameCol: array of integer);
function ShowDataOborotKpp(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
function ShowDataOstatkiKpp(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
procedure SaveXLS(filename: string; Table: TADVStringGrid; ColFl: integer);
  overload;
procedure DvizenieSalerSKpp(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
procedure DvizenieSaler(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
procedure DvizenieSalerKpp(var SL: TStringList; SLData: TStringList);
procedure DvizenieOneSaler(SL: TStringList; var dS: TSalerDvizenie);
function ShowDataSaler(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
function DvizenieSalerFNSSKpp(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime): Boolean;
procedure DvizenieSalerFNS(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
procedure DvizenieSalerKppFNS(var SL: TStringList; SLData: TStringList);
function DvizenieSalerKppOneFNS(var SL: TStringList;
  SLData: TStringList): Boolean;
function ShowDataSalerFNS(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
procedure UpdPointSpr(cb: TComboBox);
procedure UpdPointDecl(cb: TComboBox);
function NewSvertkaDB(DBQ: TADOQuery): Boolean;
function Copy2DB(basefile: string): Boolean;
function Copy2Ref(basefile: string): Boolean;
function Arh2DB(basefile, arhfile, pas: string; Inf: Boolean): Boolean;
function ID_Z(info: string): string;
function Get_ID(info: string; First: Boolean; var id: string): Boolean;
function UpdateRefLocal(findFile: Boolean; OnLine: Boolean; sql: TADOQuery;
  SelfInn: string; var db_spr: string; pb: TProgressBar): Boolean;
function RenameSprav(path1, path2, inn: string): Boolean;
procedure UpdateDateRef;
function RegOnline(DBQ1: TADOQuery;
  List: TStringList): Boolean;
procedure WriteFullDataXML(SLEan: TStringList; DBQ: TADOQuery;
  var decl: TDeclaration);
function SaveRowNewDeclDB(DBQ: TADOQuery; var decl: TDeclaration; IsOst:Boolean): Boolean;
procedure SaveRowDeclNewLocal(decl: TDeclaration);
function UpdateRefOnLine(sql: TADOQuery; RegHost, SelfInn: string): Boolean;
procedure MemoEdit(Str: String);
function UpdateNewBaseRef(path: string; ms, ms1: TMemoryStream;
  pb: TProgressBar): Boolean;
procedure WriteIniReg(ms: TMemoryStream);
function FindEanXML(DBQ: TADOQuery; ean: string): Boolean;
function InsertProizvXML(DBQ: TADOQuery; var prod: TProducer): Boolean;
function InsertEanXML(DBQ: TADOQuery; prod: TProduction): Boolean;
Function UpdateProduction(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
Function UpdateProducer(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
Function UpdateSaler(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
procedure EnabledAllRowSG(SG: TADVStringGrid; val: Boolean);
procedure WriteRowRashodSG(var SLErr: TStringList; SLOb: TStringList;
  decl: TDeclaration; SG: TADVStringGrid; ARow: integer);
procedure WriteRowBrakSG(var SLErr: TStringList; decl: TDeclaration;
  SG: TADVStringGrid; row: integer);
function ShowDataMove(var SLErr: TStringList; SG: TADVStringGrid;
  SL, SLOb: TStringList; fTF: TTypeForm;
  NoRes, CalcCol, HideCol: array of integer): integer;
procedure FormQuery(PageIndex: integer; er: TEditRecord; decl: TDeclaration);
procedure ShowRefSaler(SG: TADVStringGrid; DBQ: TADOQuery);
procedure ShowRefProduction(SG: TADVStringGrid; DBQ: TADOQuery);
procedure DeleteProduction(EAN13: string; DBQ: TADOQuery; SLRow: TSTrings);
procedure DeleteSaler(PK: string; DBQ: TADOQuery; SLRow: TSTrings);
procedure FillComboFNS(cb: TComboBox; Mas: array of string; old: Boolean);
procedure SetColorRow(ARow: integer; SG: TADVStringGrid);
function IndexPKmasSG(PK: string): integer;
function Correct_ChangeKPP(decl: TDeclaration; NewKPP: string;
  DBQ: TADOQuery): Boolean;
procedure RowDecl(fTF: TTypeForm; var decl: TDeclaration; ARow: integer);
procedure PastCell(txt: string; Table: TADVStringGrid); overload;
function DeleteRowSG(ARow: integer; SG: TADVStringGrid;
  SLData, SLErr: TStringList; var cEdit, cNew: integer): string;
function DeleteAllRowSG(SG: TADVStringGrid; SLData, SLErr: TStringList;
  var cEdit, cNew: integer): string;
procedure DupRow(row: integer; Table: TADVStringGrid);
procedure FindFirstErr(SG: TADVStringGrid);
procedure FindLastErr(SG: TADVStringGrid);
function SaveTable(fTF: TTypeForm; format: integer; fName: string;
  Table: TADVStringGrid; Progress: TProgressBar): Boolean;
function SaveTXT(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): Boolean;
function SaveXLS(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): Boolean; overload;
function SaveDBF(fTypeF: TTypeForm; filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): Boolean;
function GetColTable(Table: TADVStringGrid; fName: string): integer;
function GetTypeFile(rFile: string): TTypeFile;
function ImportDBF(fTF: TTypeForm; cD: TDateTime; filename: string;
  SG: TADVStringGrid; pb: TProgressBar): integer;
function ImportPrihDBF(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): integer;
function ImportRashDBF(wd: TDateTime; filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): integer;
procedure ImportRowDbfPrihod(var decl: TDeclaration; dbf: TdbfTable;
  RowDbf: integer);
function ImportXML(fTF: TTypeForm; filename: string; SG: TADVStringGrid;
  DBQ: TADOQuery; pb: TProgressBar; data: TDateTime; var ExtV:Boolean): integer;
function Load_XML_f12(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
  data: TDateTime; selfKppXML:string): integer;
function Load_XML_f6(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
  data: TDateTime; sal: TSaler; selfKppXML:string): integer;
function Load_XML(fTF: TTypeForm;var SLXml: TStringList; filename: string;
  DBQ: TADOQuery; dat: TDateTime; var sal: TSaler; SelfKppXML:string;
  var ExtF:Boolean): integer;
function Load_XML_f11(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
  data: TDateTime;sal:TSaler; SelfKppXML:string): integer;
procedure GetXMLSaler(node: IXMLNode; var sINN, sKpp: string);
function GetProizXML(SLXml: TSTrings; nRef: IXMLNode;
  var SL: TStringList): Boolean;
function GetPostXML(SLXml: TSTrings; nRef: IXMLNode;
  var SL: TStringList): Boolean;
// function ImportXLS(fTF: TTypeForm; filename: string; Table: TADVStringGrid;
// DBQ2: TADOQuery; Progress: TProgressBar): Boolean;
function ImportXLS(fTF: TTypeForm; filename: string; Table: TADVStringGrid;
  DBQ2: TADOQuery; Progress: TProgressBar): Boolean;
procedure IDProiz(DBQ: TADOQuery; var prod: TProducer);
function GrupEAN(DBQ: TADOQuery; var prod: TProduction): string;
// function SalerName(DBQ: TADOQuery; var sal: TSaler): string;
function KolEAN(avol, vol, prih: Extended): Extended;
function ShowNaim(ean, grup, name: string): string;
function ShowKol(ean: string; kol, vol: Extended): string;
function ImpGrup(grup: string; to_old: Boolean): string;
procedure ShowOstKPP(SLOst: TStringList; SG: TADVStringGrid; kpp: string);
procedure ShapkaSGCheck(SG: TADVStringGrid;
  NameCol, FilterCol: array of integer);
procedure Moving(dtp: TDate; DBQ: TADOQuery; SG: TADVStringGrid;
  var SLAll, SLData, SLOst: TStringList; KppS, KppR: string);
procedure FillCurOst(SGRash, SGOst: TADVStringGrid);
procedure PrihProcOst(prod: TProduction; Cap: Extended;
  var rash, ost1, rashd, ost1d: Extended);
function AddSelectEan(SGSel, SG: TADVStringGrid): integer;
procedure DeleteDataSLBD;
function DelRowDB(PK: string; DBQ: TADOQuery): Boolean;
procedure NullSaler(var sal: TSaler);
function SaveRowUpdDB(DBQ: TADOQuery; decl: TDeclaration; IsOst:Boolean): Boolean;
procedure SaveRowUpdLocal(decl: TDeclaration);
procedure ErrCells(ACol, ARow: integer);

function GetRowXML(SLXml: TSTrings; Text: string): integer;
procedure GetErrProducer_XML(var SLErrXML: TStringList; prod: TProducer;
  row: integer);
procedure GetErrProduct_XML(var SLErrXML: TStringList; decl: TDeclaration;
  row: integer);
procedure GetErrSaler_XML(var SLErr: TStringList; sal: TSaler; row: integer);
// ������ ������������� ������������
function SetECP: Boolean;
function ExistConnectServer: Boolean;
function InsertEanLocal(DBQ: TADOQuery): Boolean;
function InsertInnLocal(DBQ: TADOQuery): Boolean;
function EditLicLocal(DBQ: TADOQuery): Boolean;
function UpdateLockRefLic(sal: TSaler; fEdit: TTypeEdit): Boolean;
function UpdateSalerLicDecl(sal: TSaler): Boolean;
function AutoAddRashod(Table: TADVStringGrid): integer;
function DeleteRash: integer;
function AutoNewRashod(Table: TADVStringGrid): integer;
function InDecl(grup: string; form11: Boolean): Boolean;
function ValidOrgname(s: string): Boolean;
function InnUL(s: string): Boolean;
function UseStr(s, error, use: string): string;
function GetKPPAdres(DBQ: TADOQuery; kpp: string; var adr: TOrgAdres): string;
function MinusMove(dv: TMove): Boolean;
procedure SHapkaXML(node: IXMLNode; kv, yr, kor: integer;
  f11, kor1, f43: Boolean);
function WriteXML(fn: string): Boolean;
function GetSert(fSert: TSert; var CertContext: PCCERT_CONTEXT): Boolean;
// ���������� ���������� �������� �������
function AddECP(fXML: string; CertContext: PCCERT_CONTEXT): Boolean;
function ZipFileXML(fn: string): Boolean;
function CryptoXML(fn: string): Boolean;
procedure SignXNL(Var ms, ms1: TMemoryStream; Var err: Boolean; ECP: string;
  CertContext: PCCERT_CONTEXT);
procedure Crypt(CountS: integer; ArrayCert: array of PCCERT_CONTEXT;
  Var ms, ms1: TMemoryStream; Var err: Boolean); { ���� "�����������" }
function ZipDir(fn: string): Boolean;
function DelDir(dir: string): Boolean;
function FindFiles(StartFolder, Mask: string): string;
function UnZip(path: string; var NewPath: string): Boolean;
procedure Shapka_PrintTable1(SG: TADVStringGrid);
procedure Shapka_PrintTable2(SG: TADVStringGrid);
procedure DeclTable1(f11: Boolean; SG1: TADVStringGrid; xf: TXLSFile;
  NumSheets: integer);
procedure FilterSLKPP(FilterStr: string; SLSourse: TStringList;
  var SLRez: TStringList);
procedure FillPrintFNSSG1(f11: Boolean; SL: TStringList; SG1: TADVStringGrid;
  var row: integer; var m: TDvizenie; xf: TXLSFile; NumSheets: integer);
procedure RefreshFileFolder(path: string);
procedure FilterDataFNSMove(FNS: string; SL: TStringList;
  var SL_Data: TStringList);
procedure WriteDataFNS_Table1(SL: TStringList; SG: TADVStringGrid;
  var row: integer; var m: TDvizenie; xf: TXLSFile; NumSheets: integer);
procedure SortSLCol_dv(cols: integer; SL: TStringList;
  aCompare: TStringListSortCompare = nil);
procedure FiltrSLPr(pId: string; SL: TStringList; var SL_Data: TStringList);
procedure SumDataDv(SL: TStringList; var dv: TMove);
function FillPrintFNSSG2(f11: Boolean; SL: TStringList; SG1: TADVStringGrid;
  var row: integer; xf: TXLSFile; NumSheets: integer): Extended;
procedure DeclTable2(f11: Boolean; SG: TADVStringGrid; xf: TXLSFile;
  NumSheets: integer);
function WriteDataFNS_Table2(SL: TStringList; SG: TADVStringGrid;
  var row: integer; xf: TXLSFile; NumSheets: integer): Extended;
procedure FilterDataFNSDecl(FNS: string; SL: TStringList;
  var SL_Data: TStringList);
procedure FiltrSLPrDecl(pId: string; SL: TStringList; var SL_Data: TStringList);
procedure NullDviz(var dv: TDvizenie);
procedure SumDviz(var dv1: TDvizenie; dv2: TDvizenie);
procedure WriteSumDv(dv: TDvizenie; SG: TADVStringGrid; row: integer;
  xf: TXLSFile; NumSheets: integer);
procedure ImportExcelSG(SG: TADVStringGrid);
procedure BorderRowXLS(row, ColStart, ColEnd, nSheet: integer; xls: TXLSFile;
  FontBold: Boolean);
procedure ShapkaStatistic(SG: TADVStringGrid);
procedure InsertNewEanXML(DBQ: TADOQuery; ean: string);
procedure ColorEdit(NullTxt: string; ed: TEdit);
procedure ColorEditMask(NullTxt: string; ed: TMaskEdit);
procedure UpdateServerProduction(EAN13: string; DBQ: TADOQuery;
  SLRow: TSTrings);
procedure UpdateServerSaler(PK: string; DBQ: TADOQuery; SLRow: TSTrings);
procedure UpdateMasProduction(Str: array of AnsiString; Updt:Boolean);
function GetInetFile(const fileURL, filename: String): Boolean;
function DownloadFile(SourceFile, DestFile: string): Boolean;
// ��������� ��������� ��� ����������
procedure FillDataStatistic(SLOborot: TStringList; var SLStat: TStringList);
procedure FillMasStatistic(SL: TStringList; SG: TADVStringGrid);
procedure EnabledMainBut(En: Boolean);
function FindErrXSD(fXML: string; f11, f43: Boolean; var sErr: string): Boolean;
function GetCurDate(kv: integer): TDateTime;
function UpdateKppBD(kpp1, kpp2: string; DBQ: TADOQuery): Boolean;
function UpdateKppDecl(kpp1, kpp2: string): Boolean;
procedure GetDateKvart(s: string; var DB, dE: TDateTime);
procedure ShowDataCorectSalerFull(SG: TADVStringGrid; SLSaler: TStringList;
  SalerInn: string);
procedure ShowDataCorectSaler(SG: TADVStringGrid; SLData: TStringList);
procedure PrihodSalerSKpp(var SLEan: TStringList; SLData: TStringList;
  sKpp: string; sal: TSaler; DB, dE: TDateTime);
procedure FillDataFNS(SLData: TStringList; FNS: string; var SLAll: TStringList;
  AllData: Boolean);
procedure FillDataCorrectDate(SLData: TStringList; sDate: string;
  var SLAll: TStringList; AllData: Boolean);
procedure FillDataCorrectDateNackl(SLData: TStringList; sDate, Nackl: string;
  var SLAll: TStringList; AllData: Boolean);
procedure WriteSGCorrect(SG: TADVStringGrid; SLData: TStringList);
function ReadXMLCorrect(fName: String; var SLRez: TStringList;
  DBQ: TADOQuery; sal: TSaler; sKppXML:string):integer;
function ReadXML(fTF: TTypeForm;Xml: IXMLDocument; var SLXml, SLErrXML: TStringList;
  DBQ: TADOQuery; sal: TSaler; dat: TDateTime; SelfKppXML:string;
  var ExtVozvr:Boolean): integer;
procedure WriteXMLCorrect(SG: TADVStringGrid; SL: TStringList);
procedure SumCorrectData(SLData: TStringList; var SLSum: TStringList;
  AllData: Boolean);
function ReadXMLCorrectF(fName: TSTrings; var SLRez: TStringList;
  DBQ: TADOQuery; sal: TSaler; sKppXML:string):integer;
procedure ProizXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
procedure PostXML(node: IXMLNode; SL: TStringList; f11, fix: Boolean);
procedure XMLPoizvImp(node: IXMLNode; f11, af: Boolean; pr: TProducer_Obj);
procedure XMLPost(node: IXMLNode; f11, af: Boolean; sal: TSaler_Obj);
procedure OrgXML(node: IXMLNode; f11, fix, f43: Boolean; d1, d2: TDateTime);
procedure OrgRecXML(node: IXMLNode; f11, fix: Boolean; org: TOrganization);
procedure OrgOtvXML(node: IXMLNode; buh: TBuh; ruk: TRuk);
procedure OrgLicXML(org: TOrganization; node: IXMLNode; d1, d2: TDateTime; f43: Boolean);
function ValidLic(ser, num: string): Boolean;
function Valid_SerLic(s: string): Boolean;
function Valid_NumLic(s: string): Boolean;
procedure ReplaceKpp(all_Node: array of IXMLNode;
  var new_Node: array of IXMLNode);
procedure LoadOstXML();
procedure LoadFullXML(ost, dataOrg: Boolean);
procedure UpdateServerProductionSaler(DBQ: TADOQuery;
  SLRow: TSTrings);
function GetSalerXML(typeXML:TTypeXML; filename:string; var sal:TSaler):Boolean;
function GetTypeXML(fname:string):TTypeXML;
function GetSalerXML_11(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
function GetSalerXML_12(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
function GetSalerXML_6(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
function GetSelfKppXML(typeXML:TTypeXML; filename:string; var sInn,sKpp:string):Boolean;
function GetSelfKppXML_11(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
function GetSelfKppXML_12(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
function GetSelfKppXML_6(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
procedure DownLoadHelp;
procedure ProcDownload;
procedure HelpDownload;
procedure LoadDir;
function CheckOst(ost1,OstAM:Extended):integer;
procedure GetDataGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
procedure GetDataProizvXML(s: string; SL: TStringList;
  var SLFiltr: TStringList);
function Load_XMLF(fTF: TTypeForm;var SLXml: TStringList; filename: string;
  DBQ: TADOQuery; dat: TDateTime; var sal:TSaler; var ExtV:Boolean):integer;
procedure PrihodSaler(sKpp:string; Sal:TSaler; SLXML,SLDecl:TStringList;
  dBeg,dEnd:TDateTime; SG:TAdvStringGrid);




implementation

uses GlobalUtils, dfmShowInf, psnMD5, dfmWork, dfmMain, dfmProgress,
  dfmDataExchange, dfmQuery, About, dfmSetting, dfmFiltrSal,
  dfmErrXML, dfmSelKpp, dfmRef, Global2;

function Create_DBDecl(DBQ1: TADOQuery; path, pass: string): Boolean;
var
  sql: TADOQuery;
begin
  fmWork.con1ADOCon.Close;
  fmWork.con1ADOCon.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+db_decl+
         ';Persist Security Info=False;Jet OLEDB:Database Password='+''''+fOrg.Inn+'''';
  Result := True;
  sql := TADOQuery.Create(DBQ1.Owner);
  try
    try
      if not FileExists(path) then
      begin
        CreateAccessDB(path);
        SetAccessDBPassword(path, pass);
      end;

      sql.Connection := DBQ1.Connection;
      sql.ParamCheck := False;
      sql.sql.Text :=
        'CREATE TABLE Declaration (PK AUTOINCREMENT Primary Key, DeclType VarChar(1), '
        +
        'ReportDate DateTime, SelfKPP VarChar(9), DeclNumber VarChar(50), '
        +
        'DeclDate DateTime, SalerINN VarChar(12), SalerKPP VarChar(9), '
        + 'EAN13 VarChar(13), Amount numeric(10,2), IsGood Byte, TM varchar(50), inn varchar(12),kpp varchar (9)) ';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE KPPRegistry (KPP VarChar(9), Hash VarChar(32), Comment VarChar(180))';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE self_license (PK AUTOINCREMENT Primary Key, ser varchar(10), '
        +
        'num VarChar(30),BegLic DateTime,EndLic DateTime,RegOrg VarChar(200))';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE self_adres (PK AUTOINCREMENT Primary Key, KPP varchar(9),namename varchar (50), ' + 'Countrycode VarChar(3),IndexP VarChar(10),Codereg VarChar(2),Raipo VarChar(50),City VarChar(50),NasPunkt VarChar(50),Street VarChar(50),House VarChar(20),Struct VarChar(20),ABC VarChar(20),Quart VarChar(20))';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Mail (PK AUTOINCREMENT Primary Key, Subject VarChar(50), CreateDate DateTime, '
        + 'SendReadDate DateTime, MailType Byte, Message Text)';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Report      (FileName VarChar(19), SendDate DateTime, ProcDate DateTime, '
        + 'ErrorCode VarChar(3), ProcCount Int)';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Rest        (Period DateTime, Country VarChar(1), GroupAP VarChar(3), Volume VarChar(15))';
      sql.ExecSQL;
    except
      Result := False;
    end;
  finally
    sql.free;
  end;
end;

procedure MoveDistribRef(bmz_path, spr_path: string);
var
  ms, ms1: TMemoryStream;
begin
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    ms.LoadFromFile(bmz_path);
    if UnZipFilePas(ms, ms1, 'afhhe[') then
    begin
      ms1.SaveToFile(spr_path);
      ChangeAccessDBPassword(spr_path, 'afhhe[', psw_loc);
    end;
  finally
    ms1.free;
    ms.free;
  end;
end;

function Show_Inf(Text: String; tip: TInformation): integer; overload;
begin
  Result := -1;
  if fmShowInf = nil then
    exit;

  with fmShowInf do
  begin
    fmShowInf.ModalResult := mrNo;
    Lbl.Caption := Text;
    if Lbl.Height < 48 then
    begin
      ClientHeight := 123;
      Lbl.Top := 44 - (Lbl.Height div 2);
    end
    else
    begin
      ClientHeight := 75 + Lbl.Height;
      Lbl.Top := 20;
    end;
    if Lbl.Width < 242 then
    begin
      ClientWidth := 350;
      if Lbl.Width < 174 then
        Lbl.Left := ((ClientWidth - Lbl.Width) div 2)
      else
        Lbl.Left := 88;
    end
    else
    begin
      ClientWidth := 108 + Lbl.Width;
      Lbl.Left := 88;
    end;

    Left := fmWork.Left + (fmWork.Width - Width) div 2;
    Top := fmWork.Top + (fmWork.Height - Height) div 2;
    err.Top := 1000;
    Inf.Top := 1000;
    Conf.Top := 1000;
    Warn.Top := 1000;
    Butt2.Enabled := False;
    Edit1.Top := 1000;
    Edit1.Enabled := False;
    Edit1.Top := 1000;
    Butt1.Caption := 'OK';
    Butt1.Top := ClientHeight - 40;
    Butt1.Left := (ClientWidth div 2) - 50;
    Butt2.Top := 1000;
    if (tip = fInfo) then
    begin
      Caption := '����������';
      Inf.Top := 20;
    end;
    if tip = fError then
    begin
      Caption := '������';
      err.Top := 20;
    end;
    if tip = fConfirm then
    begin
      Caption := '�������������';
      Conf.Top := 20;
      Butt1.Caption := '��';
      Butt1.Top := ClientHeight - 40;
      Butt1.Left := (ClientWidth div 2) - 110;
      Butt2.Caption := '���';
      Butt2.Top := ClientHeight - 40;
      Butt2.Left := (ClientWidth div 2) + 30;
      Butt2.Enabled := True;
    end;
    if tip = fWarning then
    begin
      Caption := '��������';
      Warn.Top := 20;
    end;
    if tip = fEnter then
    begin
      Caption := '���� ������';
      Conf.Top := 20;
      ClientHeight := ClientHeight + Edit1.Height;
      Edit1.Top := ClientHeight - 50 - Edit1.Height;
      Edit1.Enabled := True;
      Butt1.Top := ClientHeight - 40;
      Butt1.Left := (ClientWidth div 2) - 110;
      Butt2.Caption := '������';
      Butt2.Top := ClientHeight - 40;
      Butt2.Left := (ClientWidth div 2) + 10;
      Butt2.Enabled := True;
      Edit1.SetFocus;
    end;
    Application.ProcessMessages;
    Result := ShowModal;
  end;
end;

function Show_Inf(Text: String; tip: TInformation; var rez: string): integer;
  overload; overload;
begin
  Result := -1;
  if fmShowInf = nil then
    exit;

  with fmShowInf do
  begin
    fmShowInf.ModalResult := mrNo;
    Lbl.Caption := Text;
    if Lbl.Height < 48 then
    begin
      ClientHeight := 123;
      Lbl.Top := 44 - (Lbl.Height div 2);
    end
    else
    begin
      ClientHeight := 75 + Lbl.Height;
      Lbl.Top := 20;
    end;
    if Lbl.Width < 242 then
    begin
      ClientWidth := 350;
      if Lbl.Width < 174 then
        Lbl.Left := ((ClientWidth - Lbl.Width) div 2)
      else
        Lbl.Left := 88;
    end
    else
    begin
      ClientWidth := 108 + Lbl.Width;
      Lbl.Left := 88;
    end;

    Left := fmWork.Left + (fmWork.Width - Width) div 2;
    Top := fmWork.Top + (fmWork.Height - Height) div 2;
    err.Top := 1000;
    Inf.Top := 1000;
    Conf.Top := 1000;
    Warn.Top := 1000;
    Butt2.Enabled := False;
    Edit1.Top := 1000;
    Edit1.Enabled := False;
    Edit1.Top := 1000;
    Butt1.Caption := 'OK';
    Butt1.Top := ClientHeight - 40;
    Butt1.Left := (ClientWidth div 2) - 50;
    Butt2.Top := 1000;
    if (tip = fInfo) then
    begin
      Caption := '����������';
      Inf.Top := 20;
    end;
    if tip = fError then
    begin
      Caption := '������';
      err.Top := 20;
    end;
    if tip = fConfirm then
    begin
      Caption := '�������������';
      Conf.Top := 20;
      Butt1.Caption := '��';
      Butt1.Top := ClientHeight - 40;
      Butt1.Left := (ClientWidth div 2) - 110;
      Butt2.Caption := '���';
      Butt2.Top := ClientHeight - 40;
      Butt2.Left := (ClientWidth div 2) + 30;
      Butt2.Enabled := True;
    end;
    if tip = fWarning then
    begin
      Caption := '��������';
      Warn.Top := 20;
    end;
    if tip = fEnter then
    begin
      Caption := '���� ������';
      Conf.Top := 20;
      ClientHeight := ClientHeight + Edit1.Height;
      Edit1.Top := ClientHeight - 50 - Edit1.Height;
      Edit1.Enabled := True;
      Butt1.Top := ClientHeight - 40;
      Butt1.Left := (ClientWidth div 2) - 110;
      Butt2.Caption := '������';
      Butt2.Top := ClientHeight - 40;
      Butt2.Left := (ClientWidth div 2) + 10;
      Butt2.Enabled := True;
    end;
    Application.ProcessMessages;
    Result := ShowModal;
    rez := Edit1.Text;
  end;
end;

function GetProduction(DBQ: TADOQuery; var production: TProduction;
  var FindProducer: Boolean; BD: Boolean): Boolean;
Var
  i: integer;
  uk: String;
  qr1: TADOQuery;
  num: integer;
  find: Boolean;
  SLRow: TStringList;
begin
  Result := False;
  production.Productname := NoProd;
  production.capacity := 0;
  production.ProductNameF := NoProd;
  production.prod.pINN := '';
  production.prod.pKPP := '';
  production.prod.pName := NoProducer;
  production.FNSCode := '';

  uk := '�������';
  qr1 := nil;
  find := False;
  // num := High(RefProduction[0]) - 1;
  num := RefProduction.Count - 1;
  i := 0;
  while (i <= num) and (not find) do
  begin
    SLRow := Pointer(RefProduction.Objects[i]);
    if SLRow.Strings[0] = String(production.EAN13) then
    begin
      find := True;
      production.Productname := ShortString(SLRow.Strings[2]);
      production.FNSCode := ShortString(SLRow.Strings[1]);
      production.capacity := StrToFloat(SLRow.Strings[4]);
      production.AlcVol := StrToFloat(SLRow.Strings[3]);
      production.ProductNameF := ShortString
        (FullNameProduction(String(production.Productname), production.AlcVol,
          production.capacity));
      production.prod.pINN := ShortString(SLRow.Strings[8]);
      production.prod.pKPP := ShortString(SLRow.Strings[9]);
      FindProducer := GetProducer(fmWork.que2_sql, production.prod, BD, False);
      Result := True;
    end;
    inc(i);
  end;

  if BD then
    if (not find) then
    begin
      try
        try
          qr1 := TADOQuery.Create(DBQ.Owner);
          qr1.Connection := DBQ.Connection; ;
          qr1.sql.Text := 'select * from Production where EAN13=''' + String
            (production.EAN13) + '''';
          qr1.Open;
          if qr1.RecordCount > 1 then
          begin
            qr1.sql.Text := 'delete from production where ean13=''' + String
              (production.EAN13) + ''' and prodkod=''' + uk + '''';
            qr1.ExecSQL;
            qr1.sql.Text := 'select * from Production where EAN13=''' + String
              (production.EAN13) + '''';
            qr1.Open;
          end;
          if qr1.RecordCount > 0 then
          begin
            production.EAN13 := ShortString(qr1.FieldByName('EAN13').AsString);
            production.Productname := ShortString
              (qr1.FieldByName('productname').AsString);
            production.capacity := qr1.FieldByName('Capacity').AsFloat;
            production.AlcVol := qr1.FieldByName('AlcVol').AsFloat;
            production.ProductNameF := ShortString
              (FullNameProduction(String(production.Productname),
                production.AlcVol, production.capacity));
            production.prod.pINN := ShortString
              (qr1.FieldByName('prodkod').AsString);
            production.prod.pKPP := ShortString
              (qr1.FieldByName('prodcod').AsString);
            production.FNSCode := ShortString
              (qr1.FieldByName('FNSCode').AsString);
            SLRow := TStringList.Create;

            // SetLength(RefProduction, Length(RefProduction),
            // Length(RefProduction[0]) + 1);
            RefProduction.Add(String(production.EAN13));
            for i := 0 to qr1.FieldCount - 1 do
              if (i = 3) or (i = 4) then
                // RefProduction.Strings[i,RefProduction.Count-1] := FloatToStrF((qr1.Fields[i].AsFloat), ffFixed, maxint, 2)
                SLRow.Add(FloatToStrF((qr1.Fields[i].AsFloat), ffFixed, maxint,
                    3))
              else
                // RefProduction[i, num + 1] := qr1.Fields[i].AsString;
                SLRow.Add(qr1.Fields[i].AsString);
            RefProduction.Objects[RefProduction.Count - 1] := SLRow;

            qr1.sql.Text := 'UPDATE Production ' + #13#10 +
              'Set LocRef=true WHERE EAN13=''' + String(production.EAN13)
              + '''';

            qr1.ExecSQL;
            Result := True;
            FindProducer := GetProducer(fmWork.que2_sql, production.prod, BD,
              False);
          end;
        finally
          qr1.free;
        end;
      except
        Result := False;
      end;
    end;
end;

function GetSaler(DBQ: TADOQuery; var sal: TSaler; var FindLic: Boolean;
  BD: Boolean; Ins:Boolean): Boolean;
Var
  i, num: integer;
  qr1: TADOQuery;
  find, b1: Boolean;
  SLRow: TStringList;
  PK, StrSQL:string;
begin
  Result := False;
  if sal.sName = '' then
    sal.sName := NoSal;
  qr1 := nil;

  find := False;
  i := 0;
  // num := High(RefSaler[0]) - 1;
  num := RefSaler.Count - 1;
  while (i <= num) and (not find) do
  begin
    SLRow := Pointer(RefSaler.Objects[i]);
    if (SLRow.Strings[2] = String(sal.sINN)) and
      (SLRow.Strings[3] = String(sal.sKpp)) then
    begin
      find := True;
      sal.sName := ShortString(SLRow.Strings[1]);
      sal.sId := ShortString(SLRow.Strings[0]);
      sal.sAddress := ShortString(SLRow.Strings[5]);
      FindLic := GetSalerLic(DBQ, sal, True, False);
      Result := True;
    end;
    inc(i);
  end;
  if BD then
    if (not find) then
    begin
      try
        try
          qr1 := TADOQuery.Create(DBQ.Owner);
          qr1.Connection := DBQ.Connection;
          qr1.sql.Text := 'select * from Saler where OrgINN=''' + String
            (sal.sINN) + ''' and OrgKPP=''' + String(sal.sKpp) + '''';
          qr1.Open;
          if qr1.RecordCount > 0 then
          begin
            sal.sName := ShortString(qr1.FieldByName('orgname').AsString);
            sal.sId := ShortString(qr1.FieldByName('PK').AsString);
            sal.sAddress := ShortString(qr1.FieldByName('Address').AsString);

            RefSaler.Add(String(sal.sId));
            SLRow := TStringList.Create;
            for i := 0 to qr1.FieldCount - 1 do
              SLRow.Add(qr1.Fields[i].AsString);
            RefSaler.Objects[RefSaler.Count - 1] := SLRow;
            FindLic := GetSalerLic(DBQ, sal, True, True);
          end
          else
          if Ins and InnCheck(String(sal.sINN), b1) then
          begin
            qr1.sql.Text := 'select MAX(PK) from Saler';
            qr1.Open;
            if qr1.Fields[0].AsString = '' then
              PK:='1'
            else
              PK := IntToStr(qr1.Fields[0].AsInteger + 1);
            qr1.sql.Clear;
            qr1.ParamCheck := False;
            qr1.Parameters.Clear;

            StrSQL :=
              'INSERT INTO Saler(PK,OrgName,OrgINN,OrgKPP,RegionCode,Address,LocRef,SelfMade) ';
            StrSQL := StrSQL +
              'VALUES (:PK,:OrgName,:OrgINN,:OrgKPP,:RegionCode,:Address,:LocRef,:SelfMade)';
            qr1.sql.Text := StrSQL;

            qr1.Parameters.AddParameter.Name := 'PK';
            qr1.Parameters.ParamByName('PK').DataType := ftString;
            qr1.Parameters.ParamByName('PK').value := PK;

            qr1.Parameters.AddParameter.Name := 'OrgName';
            qr1.Parameters.ParamByName('OrgName').DataType := ftString;
            qr1.Parameters.ParamByName('OrgName').value := sal.sName;

            qr1.Parameters.AddParameter.Name := 'OrgINN';
            qr1.Parameters.ParamByName('OrgINN').DataType := ftString;
            qr1.Parameters.ParamByName('OrgINN').value := sal.sInn;

            qr1.Parameters.AddParameter.Name := 'OrgKPP';
            qr1.Parameters.ParamByName('OrgKPP').DataType := ftString;
            qr1.Parameters.ParamByName('OrgKPP').value := sal.sKpp;

            qr1.Parameters.AddParameter.Name := 'RegionCode';
            qr1.Parameters.ParamByName('RegionCode').DataType := ftString;
            qr1.Parameters.ParamByName('RegionCode').value := Copy(sal.sInn,0,2);

            qr1.Parameters.AddParameter.Name := 'Address';
            qr1.Parameters.ParamByName('Address').DataType := ftString;
            qr1.Parameters.ParamByName('Address').value := '������';

            qr1.Parameters.AddParameter.Name := 'LocRef';
            qr1.Parameters.ParamByName('LocRef').DataType := ftBoolean;
            qr1.Parameters.ParamByName('LocRef').value := True;

            qr1.Parameters.AddParameter.Name := 'SelfMade';
            qr1.Parameters.ParamByName('SelfMade').DataType := ftBoolean;
            qr1.Parameters.ParamByName('SelfMade').value := True;

            qr1.ExecSQL;

            GetSalerLic(DBQ,sal,True,True);
          end;
          Result := find;
        finally
          qr1.free;
        end;
      except
      end;
    end;
end;

function SetBinaryStream(ms1: TMemoryStream; var ms2: TMemoryStream): Boolean;
var
  Reg: TRegistry;
  Key: string;
  buf: PByte;
begin
  Result := False;
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;
  Key := 'SOFTWARE\' + NameProg;
  try
    try
      Result := False;
      ms1.Position := 0;
      if Reg.OpenKey('License', True) then
      begin
        Reg.WriteBinaryData('License', ms1.Memory^, ms1.Size);
        GetMem(buf, ms1.Size);
        Reg.ReadBinaryData('License', buf^, ms1.Size);
        ms2.WriteBuffer(buf^, ms1.Size);
        Reg.DeleteValue('License');
        Result := True;
      end;
    Except
      Result := False;
    end;
  finally
    Reg.free;
    FreeMem(buf);
  end;
end;

procedure CreatePath(sINN: string);
begin
  // ���� ��� �������� �������
  path_arh := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + sINN + '\BackUp';
  path_db := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + sINN + '\DataBase';

  // ���� ��� �������� ����
  db_decl := path_db + '\' + NameProg + '.dat';
  // ���� ��� �������� ������������
  db_spr := path_db + '\' + NameProg + '.ref';
  // ���� ��� �������� ����������� ����������
  path_ref := GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\Ref\';
end;

function ReadKPP_MS(ms1: TMemoryStream; List: TStringList): Boolean;
var
  tINN, i, k, j: integer;
  ms: TMemoryStream;
  KPPhash, cYear, s, s1, s2, s3: string;
  t: Byte;
  st: AnsiString;
begin
  SLKPP.Clear;
  Result := False;
  ms := TMemoryStream.Create;
  List.Clear;
  try
    if Length(fOrg.inn) = 12 then
      tINN := 2
    else
      tINN := 0;

    // �������� ����������
    OurClient := True;
    if UnZipFilePas(ms1, ms, psw_loc) then
    begin
      ms.Position := 0;
      SetLength(st, ms.Size - 32);

      st := AnsiString(ReadBuf(ms, ms.Size - 32));
      if st[1] = '*' then
        t := 1
      else
        t := 0;
      SetLength(KPPhash, 32);

      KPPhash := ReadBuf(ms, 32);

      k := 0;
      while (Copy(st, 32 * k + 43 + t + tINN, 10) <> NullInn) and
        (Copy(st, 32 * k + 43 + t + tINN, 10) <> '1111111111') and (k <= 16) do
        inc(k);
      if ((Copy(st, 32 * k + 43 + t + tINN, 10) = NullInn) or (Copy(st,
            32 * k + 43 + t + tINN, 10) = '1111111111')) and (k <= 16) and
        (((ms.Size - 32 * k - 84 - t) mod (41 + tINN)) = 0) and
        (LowerCase(String(MD5DigestToStr(MD5String(st)))) = KPPhash) then
      begin
        if st[1] = '*' then
          st := Copy(st, 2, Length(st) - 1);
        fOrg.inn := Copy(String(st), 1, 10 + tINN);
        st := Copy(st, 11 + tINN, Length(st) - 10 - tINN);
        k := 1;
        for i := 0 to 2 do
          EnablQuart[i] := True;
        for i := 3 to Length(EnablQuart) - 1 do
          EnablQuart[i] := False;
        RegHash := '';
        List.Clear;
        while (Copy(st, 1, 10) <> NullInn) and
          (Copy(st, 1, 10) <> '1111111111') and (k < 100) do
        begin
          KPPhash := Copy(String(st), 1, 32);
          if (KPPhash = LowerCase
              (String(MD5DigestToStr(MD5String(AnsiString
                      (fOrg.inn + GiveQuarter(Date))))))) or
            (KPPhash = LowerCase
              (String(MD5DigestToStr(MD5String(AnsiString
                      (fOrg.inn + GiveQuarter(Date) + user)))))) then
            RegHash := KPPhash;
          for j := 0 to High(MasYear) do
          begin
            cYear := IntToStr(MasYear[j]);
            for i := 1 to 4 do
            begin
              s2 := LowerCase(String(MD5DigestToStr(MD5String(AnsiString(fOrg.inn + '0' + IntToStr(i) + cYear)))));
              s3 := LowerCase(String(MD5DigestToStr(MD5String(AnsiString(fOrg.inn + '0' + IntToStr(i) + cYear + user)))));
              if (KPPhash = s2) or (KPPhash = s3) then
              begin
                EnablQuart[i] := True;
                SLPerReg.Add(IntToStr(i) + ' ' + nkv + ' ' + cYear + ' ' + ng);
              end;
            end;
          end;
          st := Copy(st, 33, Length(st) - 32);
          inc(k);
        end;
        if (Copy(st, 1, 10) = '1111111111') then
          OnLine := True
        else
          OnLine := False;
        st := Copy(st, 11, Length(st) - 10);
        k := 0;
        for i := 1 to (Length(st) div 41) do
        begin
          s := Copy(String(st), 1, 9);
          s1 := Copy(String(st), 10, 32);
          st := Copy(st, 42, Length(st) - 41);
          if (s1 = LowerCase(String(MD5DigestToStr(MD5String(AnsiString(s))))))
            then
            if SLKPP.IndexOf(s) = -1 then
              SLKPP.Add(s);
        end; // for i:=1 to (Length(st)
        Result := True;
      end
      else // OpenDialog.Execute
        Show_Inf('������������ ��������� ����� ����������� �������� �����',
          fError);
    end
    else // if Form1.UnZipFile(ms1, ms)
      Show_Inf('������������ ��������� ����� ����������� �������� �����',
        fError);
  finally
    ms.free;
  end;
end;

function FullNameProduction(ProdName: string; av, c: Extended): string;
begin
  Result := ProdName + ';  ' + MyFloatToStr(av) + '%; ����� ' + MyFloatToStr(c)
    + '�.';
end;

function GetProducer(DBQ: TADOQuery; var producer: TProducer;
  BD, Ins: Boolean): Boolean;
Var
  i, num, PK: integer;
  qr1: TADOQuery;
  find, b1: Boolean;
  SLRow: TStringList;
  StrSQL, pn: string;
begin
  Result := False;
  pn := String(producer.pName);
  producer.pName := NoProducer;
  qr1 := nil;

  find := False;
  i := 0;
  // num := High(RefProducer[0]) - 1;
  num := RefProducer.Count - 1;
  while (i <= num) and (not find) do
  begin
    SLRow := Pointer(RefProducer.Objects[i]);
    if (SLRow[2] = String(producer.pINN)) and
      (SLRow[3] = String(producer.pKPP)) then
    begin
      find := True;
      producer.pName := ShortString(SLRow[1]);
      producer.pAddress := ShortString(SLRow[6]);
      producer.pId := ShortString(SLRow[0]);
      Result := True;
    end;
    inc(i);
  end;
  if BD then
    if (not find) then
    begin
      qr1 := TADOQuery.Create(DBQ.Owner);
      try
        try
          qr1.Connection := DBQ.Connection;
          qr1.sql.Clear;
          qr1.sql.Text :=
            'Select PK, FullName, INN, KPP, countrycode, RegionCode, Address FROM Producer '
            + 'where INN=''' + String(producer.pINN) + ''' and KPP=''' + String
            (producer.pKPP) + '''';
          qr1.Open;
          if qr1.RecordCount > 0 then
          begin
            producer.pName := ShortString(qr1.FieldByName('FullName').AsString);
            producer.pAddress := ShortString
              (qr1.FieldByName('Address').AsString);
            producer.pId := ShortString(qr1.FieldByName('PK').AsString);

            RefProducer.Add(qr1.FieldByName('PK').AsString);
            SLRow := TStringList.Create;

            for i := 0 to qr1.FieldCount - 1 do
              SLRow.Add(qr1.Fields[i].AsString);

            RefProducer.Objects[RefProducer.Count - 1] := SLRow;
            qr1.sql.Text := 'UPDATE Producer ' + #13#10 +
              'Set LocRef=true WHERE INN=''' + String(producer.pINN)
              + ''' and KPP=''' + String(producer.pKPP) + '''';
            qr1.ExecSQL;
            Result := True;
          end
          else
          begin
            if Ins and InnCheck(String(producer.pINN), b1) then
            begin
              qr1.sql.Text := 'select MAX(PK) from producer';
              qr1.Open;
              PK := qr1.Fields[0].AsInteger + 1;
              qr1.sql.Clear;
              qr1.Parameters.Clear;

              qr1.ParamCheck := False;
              StrSQL :=
                'INSERT INTO producer(PK, FullName, INN, KPP, countrycode, RegionCode, Address, LocRef, SelfMade) ';
              StrSQL := StrSQL +
                'VALUES (:PK, :FullName, :INN, :KPP, :countrycode, :RegionCode, :Address, :LocRef, :SelfMade)';
              qr1.sql.Text := StrSQL;

              qr1.Parameters.AddParameter.Name := 'PK';
              qr1.Parameters.ParamByName('PK').DataType := ftInteger;

              qr1.Parameters.AddParameter.Name := 'FullName';
              qr1.Parameters.ParamByName('FullName').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'INN';
              qr1.Parameters.ParamByName('INN').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'KPP';
              qr1.Parameters.ParamByName('KPP').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'countrycode';
              qr1.Parameters.ParamByName('countrycode').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'RegionCode';
              qr1.Parameters.ParamByName('RegionCode').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'Address';
              qr1.Parameters.ParamByName('Address').DataType := ftString;

              qr1.Parameters.AddParameter.Name := 'LocRef';
              qr1.Parameters.ParamByName('LocRef').DataType := ftBoolean;

              qr1.Parameters.AddParameter.Name := 'SelfMade';
              qr1.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

              qr1.Parameters.ParamByName('PK').value := PK;
              if (pn <> '') and (pn <> NoProducer) then
                producer.pName := ShortString(pn);
              qr1.Parameters.ParamByName('FullName').value := producer.pName;
              qr1.Parameters.ParamByName('INN').value := producer.pINN;
              qr1.Parameters.ParamByName('KPP').value := producer.pKPP;
              qr1.Parameters.ParamByName('countrycode').value := 643;
              qr1.Parameters.ParamByName('RegionCode').value := 59;
              qr1.Parameters.ParamByName('Address').value := '������';
              qr1.Parameters.ParamByName('LocRef').value := True;
              qr1.Parameters.ParamByName('SelfMade').value := True;

              qr1.ExecSQL;
            end;
          end;
        finally
          qr1.free;
        end;
      except
        Result := False;
      end;
    end;
end;

function GetSalerLic(DBQ: TADOQuery; var sal: TSaler; BD: Boolean; Ins:Boolean): Boolean;
var
  i, numb, idx: integer;
  SLRow: TStringList;
  find: Boolean;
  qr1: TADOQuery;
  pk:string;
begin

  Result := False;
  if sal.lic.RegOrg = '' then
  begin
    sal.lic.RegOrg := NoLic;
    sal.lic.ser := '';
    sal.lic.num := '';
    sal.lic.dBeg := dBeg;
    sal.lic.dEnd := dEnd;
  end;

  i := 0;
  numb := RefLic.Count - 1;
  while (i <= numb) and (not Result) do
  begin
    SLRow := Pointer(RefLic.Objects[i]);
    if (SLRow[6] = String(sal.sINN)) and (SLRow[7] = String(sal.sKpp)) then
    begin
      find := True;
      sal.lic.lId := ShortString(SLRow[0]);
      sal.lic.ser := ShortString(SLRow[1]);
      sal.lic.num := ShortString(SLRow[2]);
      sal.lic.dBeg := StrToDateTime(SLRow[3]);
      sal.lic.dEnd := StrToDateTime(SLRow[4]);
      sal.lic.RegOrg := ShortString(SLRow[5]);
      Result := True;
    end;
    inc(i);
  end;
  if BD then
    if (not find) then
    begin
      qr1 := TADOQuery.Create(DBQ.Owner);
      try
        try
          qr1.Connection := DBQ.Connection;
          qr1.sql.Text :=
            'select PK,ser, num,date_start,date_finish, regOrg,saler_pk,saler_kpp  from Saler_license where saler_pk=''' + String(sal.sINN) + ''' and saler_kpp=''' + String(sal.sKpp) + '''';
          qr1.Open;
          if qr1.RecordCount > 0 then
          begin
            find := True;
            sal.lic.lId := ShortString(qr1.FieldByName('pk').AsString);
            sal.lic.ser := ShortString(qr1.FieldByName('ser').AsString);
            sal.lic.num := ShortString(qr1.FieldByName('num').AsString);
            sal.lic.dBeg := qr1.FieldByName('date_start').AsDateTime;
            sal.lic.dEnd := qr1.FieldByName('date_finish').AsDateTime;
            sal.lic.RegOrg := ShortString(qr1.FieldByName('regOrg').AsString);

            RefLic.Add(String(sal.lic.lId));
            SLRow := TStringList.Create;
            for i := 0 to qr1.FieldCount - 1 do
              SLRow.Add(qr1.Fields[i].AsString);

            idx := RefLic.IndexOf(String(sal.lic.lId));
            if idx <> -1 then
              RefLic.Objects[idx] := SLRow
            else
              RefLic.Objects[RefLic.Count - 1] := SLRow;

          end
          else
          begin
            qr1.sql.Text := 'select MAX(PK) from Saler_license';
            qr1.Open;
            if qr1.Fields[0].AsString = '' then
              PK:='1'
            else
              PK := IntToStr(qr1.Fields[0].AsInteger + 1);
            sal.lic.lId := ShortString(PK);
            qr1.sql.Clear;

            qr1.sql.Text :=
              'INSERT INTO Saler_license(PK,saler_pk, ser, num, date_start, date_finish,regorg,saler_kpp)' + #13#10 + 'Values(''' + PK + ''',''' + String(sal.sINN) + ''', ''' + String(sal.lic.ser) + ''', ''' + String(sal.lic.num) + ''', ''' + DateToStr(sal.lic.dBeg) + ''', ''' + DateToStr(sal.lic.dEnd) + ''', ''' + String(sal.lic.RegOrg) + ''',''' + String(sal.sKpp) + ''')';

            qr1.ExecSQL;
          end;
          Result := find;
        finally
          qr1.free;
        end;
      except
      end;
    end;
end;

function Create_DBSpr(DBQ2: TADOQuery; path, pass: string): Boolean;
var
  sql: TADOQuery;
begin
 fmWork.con2ADOCon.Close;
 fmWork.con2ADOCon.ConnectionString:='Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+db_spr+
         ';Persist Security Info=False;Jet OLEDB:Database Password='+ '''' +psw_loc+ '''';
  Result := True;
  sql := TADOQuery.Create(DBQ2.Owner);
  try
    try
      if not FileExists(path) then
      begin
        CreateAccessDB(path);
        SetAccessDBPassword(path, pass);
      end;

      sql.Connection := DBQ2.Connection;
      sql.sql.Text :=
        'CREATE TABLE Producer    (PK Integer, FullName VarChar(180), INN VarChar(12), KPP VarChar(9), '
        +
        'countrycode VarChar(3), RegionCode VarChar(2), Address VarChar(180), ' + 'LocRef bit, SelfMade bit)';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Production  (EAN13 VarChar(13), FNSCode VarChar(3), Productname VarChar(180), '
        + 'AlcVol Float, Capacity Float, ProducerCode integer, ' +
        'LocRef bit, SelfMade bit,prodkod varchar (12),prodcod varchar (9))';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Saler       (PK varchar(10),OrgName VarChar(180), OrgINN VarChar(12), OrgKPP VarChar(9), '
        + 'RegionCode VarChar(2), Address VarChar(180), ' +
        'LocRef bit, SelfMade bit )';
      sql.ExecSQL;
      sql.sql.Text :=
        'CREATE TABLE Saler_license (  PK varchar(10),saler_pk varchar(12), ' +
        'ser VarChar(10), ' + 'num VarChar(20), ' + 'Date_start datetime, ' +
        'Date_finish datetime, ' + 'regOrg VarChar(250),saler_kpp VarChar(9))';

      sql.ExecSQL;
    except
      Result := False;
    end;
  finally
    sql.free;
  end;
end;

function AdressKpp(sKpp: TKpp): string;
begin
  Result := '';
  if sKpp.Countrycode <> '' then
    Result := Result + sKpp.Countrycode;

  if sKpp.IndexP <> '' then
    Result := Result + ',' + sKpp.IndexP;

  if sKpp.Codereg <> '' then
    Result := Result + ',' + sKpp.Codereg;

  if sKpp.Raipo <> '' then
    Result := Result + ',' + sKpp.Raipo + ' �-�';

  if sKpp.City <> '' then
    Result := Result + ', �.' + sKpp.City;

  if sKpp.naspunkt <> '' then
    Result := Result + ',' + sKpp.naspunkt;

  if sKpp.Street <> '' then
    Result := Result + ', ��.' + sKpp.Street;

  if sKpp.House <> '' then
    Result := Result + ', �.' + sKpp.House;

  if sKpp.Struct <> '' then
    Result := Result + ', ����.' + sKpp.Struct;

  if sKpp.ABC <> '' then
    Result := Result + ', ' + sKpp.ABC;

  if sKpp.Quart <> '' then
    Result := Result + ', ��.' + sKpp.Quart;
end;

procedure UpdateKpp(sKpp: TKpp; DBQ: TADOQuery);
var
  qr: TADOQuery;
  idx: integer;
begin
  qr := TADOQuery.Create(DBQ.Owner);

  idx := SLKPP.IndexOf(sKpp.kpp);
  if idx = -1 then
    exit;
  try
    qr.Connection := DBQ.Connection;
    qr.ParamCheck := False;
    qr.Parameters.Clear;
    qr.sql.Text := 'Update self_adres ' + 'Set namename=:namename, ' +
      'Countrycode = :Countrycode, ' + 'IndexP = :IndexP, ' +
      'Codereg= :Codereg, ' + 'Raipo= :Raipo, ' + 'City= :City, ' +
      'NasPunkt= :NasPunkt, ' + 'Street= :Street, ' + 'House= :House, ' +
      'Struct= :Struct, ' + 'ABC= :ABC, ' + 'Quart= :Quart ' +
      'WHERE KPP= :KPP';

    qr.Parameters.AddParameter.Name := 'namename';
    qr.Parameters.ParamByName('namename').DataType := ftString;
    qr.Parameters.ParamByName('namename').value := aKpp[idx].namename;

    qr.Parameters.AddParameter.Name := 'Countrycode';
    qr.Parameters.ParamByName('Countrycode').DataType := ftString;
    qr.Parameters.ParamByName('Countrycode').value := aKpp[idx].Countrycode;

    qr.Parameters.AddParameter.Name := 'IndexP';
    qr.Parameters.ParamByName('IndexP').DataType := ftString;
    qr.Parameters.ParamByName('IndexP').value := aKpp[idx].IndexP;

    qr.Parameters.AddParameter.Name := 'Codereg';
    qr.Parameters.ParamByName('Codereg').DataType := ftString;
    qr.Parameters.ParamByName('Codereg').value := aKpp[idx].Codereg;

    qr.Parameters.AddParameter.Name := 'Raipo';
    qr.Parameters.ParamByName('Raipo').DataType := ftString;
    qr.Parameters.ParamByName('Raipo').value := aKpp[idx].Raipo;

    qr.Parameters.AddParameter.Name := 'City';
    qr.Parameters.ParamByName('City').DataType := ftString;
    qr.Parameters.ParamByName('City').value := aKpp[idx].City;

    qr.Parameters.AddParameter.Name := 'NasPunkt';
    qr.Parameters.ParamByName('NasPunkt').DataType := ftString;
    qr.Parameters.ParamByName('NasPunkt').value := aKpp[idx].naspunkt;

    qr.Parameters.AddParameter.Name := 'Street';
    qr.Parameters.ParamByName('Street').DataType := ftString;
    qr.Parameters.ParamByName('Street').value := aKpp[idx].Street;

    qr.Parameters.AddParameter.Name := 'House';
    qr.Parameters.ParamByName('House').DataType := ftString;
    qr.Parameters.ParamByName('House').value := aKpp[idx].House;

    qr.Parameters.AddParameter.Name := 'Struct';
    qr.Parameters.ParamByName('Struct').DataType := ftString;
    qr.Parameters.ParamByName('Struct').value := aKpp[idx].Struct;

    qr.Parameters.AddParameter.Name := 'ABC';
    qr.Parameters.ParamByName('ABC').DataType := ftString;
    qr.Parameters.ParamByName('ABC').value := aKpp[idx].ABC;

    qr.Parameters.AddParameter.Name := 'Quart';
    qr.Parameters.ParamByName('Quart').DataType := ftString;
    qr.Parameters.ParamByName('Quart').value := aKpp[idx].Quart;

    qr.Parameters.AddParameter.Name := 'KPP';
    qr.Parameters.ParamByName('KPP').DataType := ftString;
    qr.Parameters.ParamByName('KPP').value := aKpp[idx].kpp;

    qr.ExecSQL;
  finally
    qr.free;
  end;
end;

procedure ReadKPP_Ini(List: TStringList);
var
  ms1: TMemoryStream;
  ExtLic: Boolean;
  Reg: TRegistry;
  fn, Key: string;
  tIniF: TIniFile;
  buf: PByte;
  i: integer;
begin
  Reg := TRegistry.Create;
  ms1 := TMemoryStream.Create;
  try
    ExtLic := RegOnLine(fmWork.que1_sql, SLPerReg);
  except
    ExtLic:=False;
  end;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  List.Clear;
  try
//    ExtLic := False; // ���������� ������������� ��������
    // ��������� ������ ������������ �� Ini �����
    tIniF.ReadBinaryStream('Parameters', 'Registration', ms1);

    if ms1.Size = 0 then // ������ � ����������� ���
    begin
//      if RegOnLine(fmWork.clientTCP410, fmWork.que1_sql, SLPerReg) then
//        ExtLic := True
//      else
      begin
        Reg.RootKey := HKEY_CURRENT_USER;
        Key := 'SOFTWARE\' + NameProg + '\' + fOrg.inn + '\';
        if Reg.OpenKey(Key, True) then
          if Reg.ValueExists('License') then
          begin
            i := Reg.GetDataSize('License');
            GetMem(buf, i);

            Reg.ReadBinaryData('License', buf^, i);
            ms1.WriteBuffer(buf^, i);
            ms1.Position := 0;
            tIniF.WriteBinaryStream('Parameters', 'Registration', ms1);
            tIniF.UpdateFile;
            FreeMem(buf, i);
            ExtLic := True;
          end;
      end;
    end
    else
      ExtLic := True;

    // �������� ����������
    if ExtLic then
      ReadKPP_MS(ms1, List);
  finally
    tIniF.free;
    Reg.free;
    ms1.free;
  end;
end;

procedure UpdateRegData(oldIdx:integer);
var
  idx: integer;
begin
  SetMasKvartal;

  if fmSetting <> nil then
  begin
    AssignCBSL(fmSetting.cbReg, SLPerReg);
    AssignCBSL(fmSetting.cbKPP, SLKPP);
  end;

  if fmMain <> nil then
  begin
//    AssignCBSL(fmMain.cbAllKvart, SLPerRegYear);
//    fmMain.lb_MainKPP.Caption := fOrg.Kpp0;
//    AssignCBSL(fmMain.cbKPP, SLKPP);
//    if fOrg.Kpp0 = '' then
//      fmMain.cbKPP.ItemIndex := 0
//    else
//    begin
//      idx := fmMain.cbKPP.Items.IndexOf(String(fOrg.Kpp0));
//      if idx <> -1 then
//        fmMain.cbKPP.ItemIndex := idx;
//    end;
    if fmMain.cbAllKvart.Items.Count > oldIdx then
      fmMain.cbAllKvart.ItemIndex := oldIdx;
    if True then

    fmMain.cbAllKvartChange(Application);
  end;
  if fmMove <> nil then
  begin
//    AssignCBSL(fmMove.cbAllKvart, SLPerRegYear);
    AssignCBSL(fmMove.cbKPP, SLKPP);
    if fOrg.Kpp0 = '' then
      fmMove.cbKPP.ItemIndex := 0
    else
    begin
      idx := fmMove.cbKPP.Items.IndexOf(String(fOrg.Kpp0));
      if idx <> -1 then
        fmMove.cbKPP.ItemIndex := idx;
    end;
    fmMove.FormShow(fmMove);
  end;
end;

procedure SetMasKvartal;
var
  i, cKvart: integer;
  s: string;
  // s, sKvart, sYear: string;
  cMonth, cDay, cYear: word;
begin
  SLPerRegYear.Clear;
  s := IntToStr(4) + ' ' + nkv + ' ' + IntToStr(NowYear - 1) + ' ' + ng;
  SLPerRegYear.Add(s);
  DecodeDate(Now(), cYear, cMonth, cDay);
  cKvart := Kvartal(cMonth);
  for i := 1 to 4 do
  begin
    s := IntToStr(i) + ' ' + nkv + ' ' + IntToStr(NowYear) + ' ' + ng;
    if NowYear <> cYear then
      SLPerRegYear.Add(s)
    else if i <= cKvart then
      SLPerRegYear.Add(s);
  end;
end;

procedure GetMonthYearKvart(kv: string; var cKvart, cYear: string);
begin
  cKvart := Copy(kv, 0, pos(nkv, kv) - 2);
  cYear := Copy(kv, Length(kv) - 8, 4);
end;

procedure ChangeMainForm;
var
  butsel: integer;
begin
  if fMainForm = fDvizenie then
    if fmMove <> nil then
      if CurSG <> nil then
        if IsChangeSG(CurSG) then
        begin
          butsel := Show_Inf(ConfirmSave, fConfirm);
          if butsel = mrOk then
            fmMove.btn_SaveClick(Application)
          else
          begin
            fmMove.ShowDataGrid;
            EnabledMainBut(True);
          end;
        end;
end;

function IsChangeSG(SG: TADVStringGrid): Boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to SG.AllRowCount - SG.FixedRows - SG.FixedFooters do
    if LongInt(SG.AllObjects[0, i]) = 1 then
    begin
      Result := True;
      exit;
    end
    else if (SG.CellProperties[1, i].BrushColor = clInsert) then
    begin
      Result := True;
      exit;
    end

end;

// ������ ��������� �� Ini ����� ��� �������
function ReadStringParam(NameParams, value: string): string;
var
  Reg: TRegistry;
  Ini: TIniFile;
  s, fn, Key: string;

begin
  Result := value;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  Ini := TIniFile.Create(fn);
  Reg := TRegistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;
  s := '';
  try
    if FileExists(fn) then
      Result := Ini.ReadString(SectionName, NameParams, value)
    else
    begin
      Key := 'SOFTWARE\' + NameProg + '\' + fOrg.inn;
      if Reg.OpenKey(Key, False) then
        if Reg.ValueExists(NameParams) then
          s := Reg.ReadString(NameParams);
      if s <> '' then
        Result := s;
    end;
  finally
    Reg.free;
    Ini.free;
  end;
end;

// �������� ������� ����������� � Ini �����
function IsRegIniF(Ini: TIniFile): Boolean;
var
  ms: TMemoryStream;
begin
  Result := False;
  ms := TMemoryStream.Create;
  try
    Ini.ReadBinaryStream('Parameters', 'Registration', ms);
    if ms.Size > 0 then
      Result := True;
  finally
    ms.free;
  end;
end;

// �������� ������� ����������� � �������
function IsRegReestr(Reg: TRegistry): Boolean;
var
  siz: integer;
begin
  Result := False;
  if Reg.ValueExists('Registration') and not Reg.ValueExists('License') then
    Reg.RenameValue('Registration', 'License');
  siz := Reg.GetDataSize('License');
  if siz > 0 then
    Result := True;
end;

function ExistBaseDecl(inn: string): Boolean;
begin
  Result := False;
  CreatePath(inn);
  if FileExists(db_decl) and FileExists(db_spr) then
    Result := True;
end;

procedure CreateSL;
begin
  SLPerRegYear := TStringList.Create;
  SLPerReg := TStringList.Create;
  SLKPP := TStringList.Create;
  SLSalerDecl := TStringList.Create;
  SLProductionDecl := TStringList.Create;
  SL_ECP := TStringList.Create;
  RefProduction := TStringList.Create;
  RefSaler := TStringList.Create;
  RefProducer := TStringList.Create;
  RefLic := TStringList.Create;
  SLAllDecl := TStringList.Create;
  SLOborotKpp := TStringList.Create;
  SLDataPrih := TStringList.Create;

  SLDataRash := TStringList.Create;
  SLDataVPost := TStringList.Create;
  SLDataVPok := TStringList.Create;
  SLDataBrak := TStringList.Create;
  SLDataMoving := TStringList.Create;
  SLOstatkiKpp := TStringList.Create;
  CurSL := TStringList.Create;
  SLErrPrih := TStringList.Create;
  SLErrRash := TStringList.Create;
  SLErrVPost := TStringList.Create;
  SLErrVPok := TStringList.Create;
  SLErrBrak := TStringList.Create;
  DelDecl := TStringList.Create;
  SLErrXML := TStringList.Create;
  SLFNS := TStringList.Create;
  SLDataOst := TStringList.Create;
  SLProcOst := TStringList.Create;
  SLCheckKPP:=TIntList.Create(-1,-1);
end;

procedure FreeSL;
begin
  SLCheckKPP.Free;
  SLProcOst.Free;
  SLFNS.free;
  SLErrXML.free;
  FreeStringList(SLSalerDecl);
  FreeStringList(SLProductionDecl);
  SLKPP.free;
  SLPerReg.free;
  SLPerRegYear.free;
  SL_ECP.free;
  SLDataPrih.free;
//  SLDataOst.Free;
  SLDataRash.free;
  SLDataVPost.free;
  SLDataVPok.free;
  SLDataBrak.free;
  SLDataMoving.free;
  CurSL.free;
  SLErrPrih.free;
  SLErrRash.free;
  SLErrVPost.free;
  SLErrVPok.free;
  SLErrBrak.free;
  DelDecl.free;
  SLOstatkiKpp.free;

  FreeStringList(RefProduction);
  FreeStringList(RefSaler);
  FreeStringList(RefProducer);
  FreeStringList(RefLic);
  FreeStringList(SLDataOst);

  FreeStringList(SLOborotKpp);
  FreeStringList(SLAllDecl);
end;

procedure FreeStringList(var SL: TStringList);
begin
  if SL = nil then
    exit;

  ClearSL(SL);

  SL.free;
end;

procedure ClearSL(SL: TStringList);
var
  i: integer;
begin
  if SL.Count > 0 then
  begin
    for i := SL.Count - 1 downto 0 do
    begin
      SL.Objects[i].free;
      SL.Delete(i);
    end;
  end;
end;

procedure SetMasYear;
var
  i: integer;
  idx,y: word;
begin
  y:= CurYear;
  if y = 0 then y := CurrentYear();

  idx := y - YearStart;
  SetLength(MasYear, idx + 1);
  for i := 0 to High(MasYear) do
    MasYear[i] := YearStart + i;
end;

procedure CreateParamBase(NewInn: string);
var
  Ini: TIniFile;
  fn: string;
  cYear, cMonth, cDay: word;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  Ini := TIniFile.Create(fn);

  try
 //   fmWork.ClientTCP410.Host := BMHost;
    exe_version := FileVersion(ParamStr(0));
    Ini.WriteString(SectionName, 'DataBasePath', path_db);
    Ini.WriteString(SectionName, 'BackUpPath', path_arh);
    Ini.WriteString(SectionName, 'INN', fOrg.inn);
    Ini.WriteString(SectionName, 'KPP', '');
    Ini.WriteString(SectionName, 'SelfName', NameNewOrg);
    Ini.WriteString(SectionName, 'VersionInfo', exe_version);
    Ini.WriteString(SectionName, 'RegHost', BMHost);
    Ini.WriteInteger(SectionName, 'ProcOst_11', ProcOst_11);
    Ini.WriteInteger(SectionName, 'ProcOst_12', ProcOst_12);
    Ini.WriteInteger(SectionName, 'Sv', -1);
    DecodeDate(Now(), cYear, cMonth, cDay);
    CurKvart := Kvartal(cMonth);
    fOrg.kvart := CurKvart;
    fOrg.Year := cYear;
    CurYear := cYear;
    Ini.WriteInteger(SectionName, 'Kvartal', CurKvart);
    Ini.WriteInteger(SectionName, 'Year', CurYear);
    fOrg.ruk.RukF := '';
    Ini.WriteString(SectionName, 'RukF', fOrg.ruk.RukF);
    fOrg.ruk.RukI := '';
    Ini.WriteString(SectionName, 'RukI', fOrg.ruk.RukI);
    fOrg.ruk.RukO := '';
    Ini.WriteString(SectionName, 'RukO', fOrg.ruk.RukO);
    fOrg.ruk := fOrg.ruk;
    fOrg.buh.BuhF := '';
    Ini.WriteString(SectionName, 'BuhF', fOrg.buh.BuhF);
    fOrg.buh.BuhI := '';
    Ini.WriteString(SectionName, 'BuhI', fOrg.buh.BuhI);
    fOrg.buh.BuhO := '';
    Ini.WriteString(SectionName, 'BuhO', fOrg.buh.BuhO);
    fOrg.buh := fOrg.buh;
    fOrg.inn := NewInn;
    Ini.WriteString(SectionName, 'INN', fOrg.inn);
    if SLKPP.Count > 0 then
      fOrg.Kpp0 := ShortString(SLKPP.Strings[0])
    else
      fOrg.Kpp0 := '';
//    kppCur := fOrg.Kpp0;
    Ini.WriteString(SectionName, 'KPP0', String(fOrg.Kpp0));
//    kppCur := fOrg.Kpp0;
//    Ini.WriteString(SectionName, 'KPP', String(fOrg.Kpp0));
    fOrg.NPhone := '(342) 000-00-00';
    Ini.WriteString(SectionName, 'NPhone', fOrg.NPhone);
    fOrg.SelfName := fOrg.SelfName;
    Ini.WriteString(SectionName, 'SelfName', fOrg.SelfName);
    fOrg.Email := '';
    Ini.WriteString(SectionName, 'Address', fOrg.Email);
    fOrg.Address := '';
    Ini.WriteString(SectionName, 'AddressOrg', fOrg.Address);
    fOrg.lic.SerLic := '';
    Ini.WriteString(SectionName, 'SerLic', fOrg.lic.SerLic);
    fOrg.lic.NumLic := '';
    Ini.WriteString(SectionName, 'NumLic', fOrg.lic.NumLic);
    // fOrg.lic.BegLic := Kvart_Start(Now());
    fOrg.lic.BegLic := Kvart_Start(GetCurDate(CurKvart));
    Ini.WriteDate(SectionName, 'BegLic', fOrg.lic.BegLic);
    // fOrg.lic.EndLic := Kvart_Finish(Now());
    fOrg.lic.EndLic := Kvart_Finish(GetCurDate(CurKvart));
    Ini.WriteDate(SectionName, 'EndLic', fOrg.lic.EndLic);
    fOrg.lic.OrgLic := '�����';
    Ini.WriteString(SectionName, 'OrgLic', fOrg.lic.OrgLic);
    fOrg.inn := NewInn;
  finally
    Ini.free
  end;
end;

function UpdateApp(RegHost, inn, ver, ProgrPath: string)
  : Boolean;
Var
  ms, ms1: TMemoryStream;
  s: string;
  RFEX, er, BZ: Boolean;
  i: integer;
  FSize: Int64;
  a_str: AnsiString;
  PAC: PAnsiChar;
  TCP : TIdTCPClient;
begin
 try
  TCP := TIdTCPClient.Create(nil);
  TCP.Host := BMHost;
  TCP.Port:=TCPPort;
  Result := False;
  BZ := False;
  if fmProgress = nil then
    fmProgress := TfmProgress.Create(Application);
  fmProgress.Show;
  fmProgress.lblcap.Caption := '����������� ���������� ����������';
  try
    try
      fmWork.Enabled := False;
      if fmDataExchange = nil then
        fmDataExchange := TfmDataExchange.Create(Application);

      fmDataExchange.lst_Msg.Clear;
      fmProgress.pb.Max := 16;
      fmProgress.pb.Step := 1;
      Application.ProcessMessages;
      fmProgress.pb.StepIt;
      if TCP.Connected then
      begin
        TCP.ConnectTimeout := 500;
        TCP.Socket.Close;
        TCP.DisconnectNotifyPeer;
        sleep(1000);
      end;

      if not TCP.Connected then
        try
          TCP.Connect;
        except
          Result := False;
          exit;
        end;
      fmProgress.pb.StepIt;
      s := TCP.IOHandler.ReadLn;
      fmProgress.pb.StepIt;
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');
      try
        fmProgress.pb.StepIt;
        RFEX := ExistUpdateServer(TCP, inn, ver, False);
        fmProgress.pb.StepIt;
        if RFEX then
        begin
          TCP.IOHandler.WriteLn('OK'); // 5
         fmProgress.pb.StepIt;
          // MemoAdd('����������� ���������� ����������... ');
          if FileIsReadOnly(ProgrPath + 'BMS_updater.exe') then
            FileSetReadOnly(ProgrPath + 'BMS_updater.exe', False);
          if FileIsReadOnly(ProgrPath + 'alcoretail.exe') then
            FileSetReadOnly(ProgrPath + 'alcoretail.exe', False);

          // MemoAdd('����������� �������� ����������... ');

          for i := 1 to 2 do
          begin
            ms := TMemoryStream.Create;
            ms1 := TMemoryStream.Create;
            try

              s := TCP.IOHandler.ReadLn; // 1

              fmProgress.pb.StepIt;
              FSize := StrToInt(s); // TCPClient410.IOHandler.ReadLn);
              er := False;
              ms.Clear;
              TCP.IOHandler.ReadStream(ms, FSize); // 2
              fmProgress.pb.StepIt;
              if TCP.IOHandler = nil then
                TCP.Connect;
              ms.Position := 0;
              if not UnZipFilePas(ms, ms1, psw_loc) then // 3
              begin
                BZ := True;
                er := True;
              end;
              fmProgress.pb.StepIt;
              // if not TCP.Connected then  Er:=true;
              if (not BZ) and (not er) then
              begin
                if i = 1 then
                  ms1.SaveToFile(ProgrPath + 'BMS_Updater.exe') // 4
                else
                  ms1.SaveToFile(ProgrPath + 'update.exe');
                fmProgress.pb.StepIt;
                TCP.IOHandler.WriteLn('OK'); // 5
                fmProgress.pb.StepIt;
                if i = 2 then
                  Result := True;
              end
              else if TCP.Connected then
              begin
                TCP.IOHandler.WriteLn('ERRR');
                Result := True;
              end;

            finally
              ms1.free;
              ms.free
            end;
          end; // for i
          if not er then
          begin
            fmWork.Close;
          end;
        end
        else if SameText(s, 'NUPD') then
        begin
          er := True;
        end
        else if SameText(s, 'DSBL') then
        begin
          er := True;
        end
        else if SameText(s, 'NFND') then
        begin
          er := True;
        end
        else if SameText(s, 'FAULT') then
        begin
          er := True;
        end
        else if (not RFEX) and (s = 'Connected to server.') then
        begin
          Result := False;
        end;
      finally
        TCP.Socket.Close;
        TCP.Disconnect;
        TCP.Free;
      end;
    except
     on E:Exception do
      S := E.Message;
//      TCP.Socket.Close;
//      TCP.Disconnect;
//      TCP.Free;
    end;
  finally
    fmWork.Enabled := True;
    fmProgress.Close;
  end;
  if Result then
  begin
    fmWork.Enabled := True;
    a_str := AnsiString(ProgrPath + 'BMS_updater.exe');
    PAC := Addr(a_str[1]);
    WinExec(PAC, SW_ShowNormal);
    Result := True;
    MemoAdd('');
  end;
 except
  on e : Exception do
   S :=  E.Message;
 end;
end;

procedure MemoAdd(Str: String);
begin
  if Length(Str) = 0 then
    fmDataExchange.lst_Msg.Items.Add('')
  else
    fmDataExchange.lst_Msg.Items.Add(DateTimeToStr(Date + Time) + ': ' + Str);
  Application.ProcessMessages;
  fmDataExchange.lst_Msg.ItemIndex := fmDataExchange.lst_Msg.Items.Count - 1;
end;

function ExistUpdateServer(TCP: TIdTCPClient; inn, ver: string;
  Discon: Boolean): Boolean;
var
  s: string;
begin
  Result := False;
  try
    try
      if not TCP.Connected then
        TCP.IOHandler.Close;

      TCP.IOHandler.WriteLn('EXEC' + inn + ' ' + ver); // 3
      // MemoAdd('������ : ������ ���������� ����������');
      s := TCP.IOHandler.ReadLn; // 4
      if Copy(s, 1, 20) = 'Connected to server.' then
        s := 'FAULT';
      if SameText(s, 'RFEX') then
      begin
        Result := True;
      end
      else
        except
          Result := False;
        end;
    finally
      if Discon then
      begin
        TCP.Socket.Close;
        TCP.Disconnect;
      end;
    end;
  end;

procedure LongAmount(DBQ: TADOQuery);
var
  tIniF: TIniFile;
  fn: string;
  sql: TADOQuery;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini'; //
  tIniF := TIniFile.Create(fn);
  try
    try

      if tIniF.ValueExists(SectionName, 'LongText') then
        LongText := tIniF.ReadBool(SectionName, 'LongText', False)
      else
        LongText := False;
      // if LongText then exit;

      sql := TADOQuery.Create(DBQ.Owner);
      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text := 'ALTER TABLE Declaration ALTER COLUMN AMOUNT TEXT(10)';
      sql.ExecSQL;
      tIniF.WriteBool(SectionName, 'LongText', True);
    except

    end;
  finally
    tIniF.free;
  end;
end;

procedure SetDataKpp(DBQ: TADOQuery);
var
  qr: TADOQuery;
  kpp: string;
  idx: integer;
begin
  // �������� ������������ ��� � ���� ������
  InsertKpp(DBQ);
  // ����������� ������� ������ ���
  SetLength(aKpp, SLKPP.Count);
  qr := TADOQuery.Create(DBQ.Owner);
  try
    qr.Connection := DBQ.Connection; ;
    qr.sql.Text := 'select * from self_adres';
    qr.Open;
    if not qr.Eof then
    begin
      qr.First;
      while not qr.Eof do
      begin
        kpp := qr.FieldByName('KPP').AsString;
        idx := SLKPP.IndexOf(kpp);
        if idx <> -1 then
        begin
          aKpp[idx].kpp := qr.FieldByName('KPP').AsString;
          aKpp[idx].namename := qr.FieldByName('namename').AsString;
          aKpp[idx].Countrycode := qr.FieldByName('Countrycode').AsString;
          aKpp[idx].IndexP := qr.FieldByName('IndexP').AsString;
          aKpp[idx].Codereg := qr.FieldByName('Codereg').AsString;
          aKpp[idx].Raipo := qr.FieldByName('Raipo').AsString;
          aKpp[idx].City := qr.FieldByName('City').AsString;
          aKpp[idx].naspunkt := qr.FieldByName('NasPunkt').AsString;
          aKpp[idx].Street := qr.FieldByName('Street').AsString;
          aKpp[idx].House := qr.FieldByName('House').AsString;
          aKpp[idx].Struct := qr.FieldByName('Struct').AsString;
          aKpp[idx].ABC := qr.FieldByName('ABC').AsString;
          aKpp[idx].Quart := qr.FieldByName('Quart').AsString;
        end;
        qr.Next;
      end;
    end;
  finally
    qr.free;
  end;
end;

procedure InsertKpp(DBQ: TADOQuery);
var
  qr: TADOQuery;
  SL: TStringList;
var
  i: integer;
begin
  if SLKPP.Count = 0 then
    exit;
  qr := TADOQuery.Create(DBQ.Owner);
  SL := TStringList.Create;
  try
    qr.Connection := DBQ.Connection;
    qr.ParamCheck := False;
    qr.Parameters.Clear;
    qr.sql.Text := 'select * from self_adres';
    qr.Open;
    qr.First;
    while not qr.Eof do
    begin
      if SL.IndexOf(qr.FieldByName('KPP').AsString) = -1 then
        SL.Add(qr.FieldByName('KPP').AsString);
      qr.Next;
    end;
    qr.Close;
    for i := 0 to SLKPP.Count - 1 do
      if SL.IndexOf(SLKPP.Strings[i]) = -1 then
        InsertKppAdres(SLKPP.Strings[i], DBQ);
  finally
    SL.free;
    qr.free;
  end;
end;

procedure InsertKppAdres(sKpp: string; DBQ: TADOQuery);
var
  qr: TADOQuery;
begin
  if SLKPP.Count = 0 then
    exit;
  qr := TADOQuery.Create(DBQ.Owner);
  try
    qr.Connection := DBQ.Connection;
    qr.ParamCheck := False;
    qr.Parameters.Clear;
    qr.sql.Text :=
      'insert into self_adres (KPP, namename) values ' + '(''' + sKpp +
      ''', '' ��� ' + sKpp + ''')';
    qr.ExecSQL;
  finally
    qr.free;
  end;
end;

function GetSetECP(OldCrPro: Boolean): Boolean;
var
  st: string;
  nErr, n: Int64;
  i: integer;
  hProv, hContProv: HCRYPTPROV;
  Param, Param1, Param2, bufLen: DWORD;
  Readers: array of array [1 .. 2] of String;
  bbb: array [1 .. 1000] of AnsiChar;
  buf: PByte;
  s: array [1 .. 50] of PAnsiChar;
  afs_tmp: TSert;
  SL: TStringList;
begin
  SL_ECP.Clear;
  Result := False;
  SL := TStringList.Create;
  try
    // ������������� ��������� ����������������
    hContProv := 0;
    if CryptAcquireContextW(@hProv, nil, GOST_34, 75, CRYPT_VERIFYCONTEXT)
      then
      try
        nErr := GetLastError;
        if nErr <> 0 then
          Show_Inf('������ CryptAcquireContextA ' + IntToStr(nErr), fError);
        i := 1;
        SetLength(Readers, 1);
        Param := CRYPT_FIRST;
        Param1 := CRYPT_UNIQUE;
        if OldCrPro then
          Param2 := 0
        else
          Param2 := CRYPT_FQCN;

        CryptGetProvParam(hProv, PP_ENUMREADERS, nil, @bufLen,
          Param or Param2 or Param1);

        while CryptGetProvParam(hProv, PP_ENUMREADERS, @bbb, @bufLen,
          Param or Param2 or Param1) do
        begin
          Param := 0;
          st := String(bbb);
          SetLength(Readers, Length(Readers) + 1);
          n := 1;
          while st[n] <> #0 do
            inc(n);
          Readers[i, 1] := Copy(st, 1, n - 1);
          st := Copy(st, n + 1, Length(st) - n);
          n := 1;
          while st[n] <> #0 do
            inc(n);
          Readers[i, 2] := Copy(st, 1, n - 1);
          inc(i);
        end;

        i := 1;
        Param := CRYPT_FIRST;
        CryptGetProvParam(hProv, PP_ENUMCONTAINERS, nil, @bufLen,
          Param or Param2 or Param1);
        GetMem(buf, bufLen);

        while CryptGetProvParam(hProv, PP_ENUMCONTAINERS, buf, @bufLen,
          Param or Param2 or Param1) and (i < 100) do
        begin
          Param := 0;
          n := GetLastError;
          if n <> 0 then
            Show_Inf('������ CryptGetProvParam4 ' + IntToStr(n), fError);
          s[i] := PAnsiChar(buf);
          SL.Add(String(s[i]));
          inc(i);
        end;

        for i := 0 to SL.Count - 1 do
        begin
          afs_tmp.path := SL.Strings[i];
          GetDataECP(afs_tmp);
          if afs_tmp.dEnd >= Now then
            SL_ECP.Add(SL.Strings[i]);
        end;

        SetLength(afs, 0);
        SetLength(afs, SL_ECP.Count);
        for i := 0 to SL_ECP.Count - 1 do
        begin
          afs_tmp.path := SL_ECP.Strings[i];
          GetDataECP(afs_tmp);
          if afs_tmp.dEnd >= Now then
            afs[i] := afs_tmp;
        end;
        if SL_ECP.Count > 0 then
          Result := True;

      finally
        FreeMem(buf, bufLen);
        CryptReleaseContext(hProv, 0);
        CryptReleaseContext(hContProv, 0);
      end;
  finally
    SL.free;
  end;
end;

function GetDataECP(var fs: TSert): Int64;
var
find, bol: Boolean;
// NoCert:Boolean;
PAC: PAnsiChar;
PACs: AnsiString;
hProv: HCRYPTPROV;
hKeySig: HCRYPTKEY;
CertContext: PCCERT_CONTEXT;
CertLen, pubKeyInfoLen, sz: DWORD;
pubKeyInfo: PCERT_PUBLIC_KEY_INFO;
L: PWideChar;
hCert_Store: HCERTSTORE;
er: Int64;
Cert: PByte;
nameInfo: PCERT_NAME_INFO;
nameBLOB: CERT_NAME_BLOB;
// s: String;
// i: integer;
rdn: PCERT_RDN_ATTR;
CertName: string;
// rez: string;
siz: DWORD;
nameString: PChar;
begin
Result := 0;
try
  try
    // NoCert := False;
    PACs := AnsiString(fs.path);
    PAC := Addr(PACs[1]);
    CryptAcquireContextA(@hProv, PAC, GOST_34, 75, CRYPT_SILENT);
    CryptGetUserKey(hProv, AT_KEYEXCHANGE, @hKeySig);
    // �������� ������ �� �������� ����
    CryptGetKeyParam(hKeySig, KP_CERTIFICATE, nil, @CertLen, 0);
    CertContext := nil;

    pubKeyInfo := nil;
    if CertContext = nil then
    begin
      L := PWideChar(WideString('MY'));
      hCert_Store := CertOpenStore(CERT_STORE_PROV_SYSTEM, encType, hProv,
        CERT_SYSTEM_STORE_CURRENT_USER, L); // ��������� ���������
      find := False;
      CertContext := CertEnumCertificatesInStore(hCert_Store, CertContext);
      if CertContext <> nil then
      begin
        CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType, nil,
          @pubKeyInfoLen);
        er := GetLastError;
        if pubKeyInfoLen > 0 then
        begin
          Screen.Cursor := crHourGlass;
          GetMem(pubKeyInfo, pubKeyInfoLen);
          CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType,
            pubKeyInfo, @pubKeyInfoLen);
          Screen.Cursor := crDefault;
        end; // if pubKeyInfoLen>0
      end; // if CertContext=nil
      bol := False;
      while (CertContext <> nil) and (not find) do
      begin
        if bol then
          CertContext := CertEnumCertificatesInStore(hCert_Store,
            CertContext);
        if (pubKeyInfoLen > 0) and (CertContext <> nil) then
          if CertComparePublicKeyInfo(CertContext.dwCertEncodingType,
            pubKeyInfo, @CertContext.pCertInfo.SubjectPublicKeyInfo) then
          begin
            find := True;
            // Gr[ComboBox1.ItemIndex+1, 4]:='my';
          end;
        bol := True;
      end; // while (CertContext<>nil)
    end; // if CertContext=nil
    FreeMem(pubKeyInfo, pubKeyInfoLen);
    if CertContext = nil then
      if CertLen > 0 then
      begin
        GetMem(Cert, CertLen);
        CryptGetKeyParam(hKeySig, KP_CERTIFICATE, Cert, @CertLen, 0);
        CertContext := CertCreateCertificateContext(encType, Cert, CertLen);
        // ������� �������� �����������
        FreeMem(Cert, CertLen); // ���������� ������
      end;
    if CertContext <> nil then
    begin
      CryptDecodeObject(encType, X509_NAME,
        CertContext.pCertInfo.Subject.pbData,
        CertContext.pCertInfo.Subject.cbData,
        CRYPT_DECODE_NOCOPY_FLAG, nil, @sz); // or (encNameLen < 1);
      GetMem(nameInfo, sz);
      CryptDecodeObject(encType, X509_NAME,
        CertContext.pCertInfo.Subject.pbData,
        CertContext.pCertInfo.Subject.cbData,
        CRYPT_DECODE_NOCOPY_FLAG, nameInfo, @sz); // or (encNameLen < 1);

      fs.dBeg := FileTime2DateTime(CertContext.pCertInfo.NotBefore);
      fs.dEnd := FileTime2DateTime(CertContext.pCertInfo.NotAfter);

      CheckBadDateKey(fs.dBeg, fs.dEnd); // ?????????????????????????????????????????

      // nameBLOB := CertContext^.pCertInfo^.Subject;
      nameString := StrAlloc(512);
      // siz := CertNameToStr(encType, @nameBlob, CERT_SIMPLE_NAME_STR,
      // nameString, 512);

      nameBLOB := CertContext^.pCertInfo^.Issuer;
      siz := CertNameToStr(encType, @nameBLOB, CERT_SIMPLE_NAME_STR,
        nameString, 512);
      if siz > 1 then
        fs.izd := nameString
      else
        fs.izd := '�������� �� ���������';
      StrDispose(nameString);

      er := GetLastError;
      if er <> 0 then
        Show_Inf('������ ' + IntToStr(er), fError);
      if (sz > 0) then
      begin
        rdn := CertFindRDNAttr(szOID_COMMON_NAME, nameInfo);
        if (rdn.dwValueType = CERT_RDN_UNICODE_STRING) or
          (rdn.dwValueType = CERT_RDN_UTF8_STRING) then
          CertName := WideCharToString(PWideChar(rdn.value.pbData))
        else
          CertName := String(PAnsiChar(rdn.value.pbData));
        Load_RDN(nameInfo, fs);
      end;

      FreeMem(nameInfo, sz);
      CertFreeCertificateContext(CertContext);
      // ���������� �������� �����������
    end
    // else
    // NoCert := True;
    except
      er := GetLastError;
    end;
  finally

  end;
end;

procedure CheckBadDateKey(DateBeg, DateEnd: TDateTime);
// Var
// d, M, Y: word;
// DateCur: TDateTime;
// BadDateKey: integer;
begin
  // DateCur := Date + Time;
  // BadDateKey := 0;
  // if (DateBeg > DateCur) then
  // BadDateKey := 1
  // else if (DateEnd < DateCur) then
  // BadDateKey := 2
  // else
  // begin
  // DecodeDate(DateBeg, Y, M, d);
  // inc(Y);
  // inc(M, 3);
  // if M > 12 then
  // begin
  // dec(M, 12);
  // inc(Y);
  // end;
  // if ((M = 2) and (d > 28) and ((Y mod 4) <> 0)) then
  // d := 28
  // else if ((M = 2) and (d > 29) and ((Y mod 4) = 0)) then
  // d := 29
  // else if ((M = 4) or (M = 6) or (M = 9) or (M = 11)) and (d > 30) then
  // d := 30;
  // DateEnd := EncodeDate(Y, M, d);
  // DateEnd := DateEnd + DateBeg - trunc(DateBeg);
  // if (DateEnd < DateCur) then
  // BadDateKey := 3;
  // end;
end;

function FindSert(sINN: string): Boolean;
begin
  Result := False;
  fSert.FIO := '';
  fSert.inn := '';
  fSert.orgname := '';
  fSert.dBeg := 0;
  fSert.dEnd := 0;
  fSert.path := '';
  try
    fSert.path := ReadStringParam('Sert', '');
    if fSert.path = '' then
      SetCurECP(fOrg.inn, fSert)
    else
      GetDataECP(fSert);
    if fSert.inn <> '' then
    begin
      WriteStringParam('Sert', fSert.path);
      Result := True;
    end;
  except
    Result := False;
  end;
end;

// ������ ������������� ������������
function SetCurECP(inn: string; var fs: TSert): Boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to SL_ECP.Count - 1 do
  begin
    fs.path := SL_ECP.Strings[i];
    GetDataECP(fs);
    if (pos(inn, fSert.inn) > 0) then
    begin
      Result := True;
      exit;
    end
    else
      NullSert(fs);
  end;
end;

// ������ ��������� �� Ini ����� ��� �������
function WriteStringParam(NameParams, value: string): string;
var
  Ini: TIniFile;
  fn: string;
begin
  Result := value;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  Ini := TIniFile.Create(fn);
  try
    Ini.WriteString(SectionName, NameParams, value) finally Ini.free;
  end;
end;

procedure NullSert(var fs: TSert);
begin
  fs.FIO := '';
  fs.inn := '';
  fs.orgname := '';
  fs.dBeg := 0;
  fs.dEnd := 0;
  fs.path := '';
end;

procedure SetDataFromSert;
begin
  if (fOrg.Kpp0 = '') and (fSert.kpp <> '') then
    fOrg.Kpp0 := fSert.kpp;

  if (fOrg.SelfName = NameNewOrg) and (fSert.orgname <> '') then
    fOrg.SelfName := fSert.orgname;

  if (fOrg.ruk.RukF = '') and (fSert.FIO <> '') then
    SetFIO(fOrg.ruk, fSert.FIO);

  if (fOrg.Email = '') and (fSert.Email <> '') then
    fOrg.Email := fSert.Email;
end;

procedure SetFIO(var fRuk: TRuk; FIO: string);
var
  s, sFio: string;
  idx: integer;
begin
  sFio := FIO;
  idx := pos(' ', sFio);
  s := Copy(sFio, 0, idx - 1);
  fRuk.RukF := s;
  sFio := Copy(sFio, idx + 1, Length(sFio) - idx);
  idx := pos(' ', sFio);
  s := Copy(sFio, 0, idx - 1);
  fRuk.RukI := s;
  sFio := Copy(sFio, idx + 1, Length(sFio) - idx);
  fRuk.RukO := sFio;
end;

function DB2Arh(basefile, arhfile: string): Boolean;
var
  ms, ms1: TMemoryStream;
begin
  DB2Arh := False;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    try
      if FileExists(basefile) then
      begin
        ms.LoadFromFile(basefile);
        if ZipFilePas(ms, ms1, psw_loc) then
        begin
          ms1.SaveToFile(arhfile);
          DB2Arh := True;
        end;
      end;
    except
      DB2Arh := False;
    end;
  finally
    ms.free;
    ms1.free;
  end;
end;

function Upd_LocRef(DBQ1, DBQ2: TADOQuery; pb: TProgressBar;
  le: TLabel): Boolean;
var
  rez1, rez2, rez3, rez: Boolean;
begin
  le.Caption := '���������� ����������� ���������';
  Application.ProcessMessages;
  rez1 := LocRefProduction(DBQ1, DBQ2, pb);
  ResetProgressBar(pb);
  le.Caption := '���������� ����������� �������������';
  Application.ProcessMessages;
  rez2 := LocRefProducer(DBQ1, DBQ2, pb);
  ResetProgressBar(pb);
  le.Caption := '���������� ����������� �����������';
  Application.ProcessMessages;
  rez3 := LocRefSaler(DBQ1, DBQ2, pb);
  ResetProgressBar(pb);
  rez := rez1 and rez2 and rez3;
  le.Caption := '�������� ������ ������������';
  GetDataRef(pb);
  Result := rez;
end;

function LocRefProduction(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
var
  qrRef, qrDecl: TADOQuery;
  SLRef, SLDecl: TStringList;
  i: integer;
  EAN13: string;
  rmax, j: integer;
begin
  Result := True;
  SLRef := TStringList.Create;
  SLDecl := TStringList.Create;
  qrRef := TADOQuery.Create(DBQ1.Owner);
  qrDecl := TADOQuery.Create(DBQ2.Owner);
  try
    try
      qrDecl.Connection := DBQ1.Connection;
      qrRef.Connection := DBQ2.Connection;

      qrRef.sql.Text := SQLSelectProdLocRef;
      qrRef.Open;

      qrDecl.sql.Text := SQLSelectProdDecl;
      qrDecl.Open;

      j := 1;
      rmax := qrRef.RecordCount + qrDecl.RecordCount;
      NullProgressBar(pb);

      // ������� ������ ��������� ����������� � ���������� �� ������ ����������
      if not qrDecl.Eof then
      begin
        qrDecl.First;
        while not qrDecl.Eof do
        begin
          PosProgressBar(pb, j, rmax);

          EAN13 := qrDecl.FieldByName('EAN13').AsString;
          if SLDecl.IndexOf(EAN13) = -1 then
            SLDecl.Add(EAN13);
          qrDecl.Next;
          inc(j);
        end;
      end;

      // SLRef - ���� � ���������� � �����������, ������� ������������ � ����������
      if not qrRef.Eof then
      begin
        qrRef.First;
        while not qrRef.Eof do
        begin
          PosProgressBar(pb, j, rmax);
          EAN13 := qrRef.FieldByName('EAN13').AsString;
          if SLRef.IndexOf(EAN13) = -1 then
            SLRef.Add(EAN13);
          qrRef.Next;
          inc(j);
        end;
      end;

      qrRef.free;
      qrRef := TADOQuery.Create(DBQ2.Owner);
      qrRef.Connection := DBQ2.Connection;

      rmax := SLRef.Count + SLDecl.Count;
      NullProgressBar(pb);
      // ��������� �� ����� � ���������� ��� LocRef=true,
      // ���� ����� ��������� ��� � ���������� ������� ���� LocRef
      i := 0;
      j := 1;
      while i <= SLRef.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        EAN13 := SLRef.Strings[i];
        // ���� ������ �������� ��� � ���������� ������ ��� �� ������
        if SLDecl.IndexOf(EAN13) = -1 then
        begin
          qrRef.sql.Text :=
            'UPDATE Production SET LocRef=false where ean13=''' + EAN13 +
            '''';
          qrRef.ExecSQL;
          SLRef.Delete(i);
        end;
        inc(i);
        inc(j);
      end;

      // ��������� �� ����� � ���������� �� ����������,
      // �������� ����� ���� LocRef = True
      qrRef.Close;
      for i := 0 to SLDecl.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        EAN13 := SLDecl.Strings[i];
        if SLRef.IndexOf(EAN13) = -1 then
        begin
          if not qrRef.prepared then
            qrRef.prepared := True;
          qrRef.sql.Text :=
            'UPDATE Production SET LocRef=True where ean13=''' + EAN13 +
            '''';
          qrRef.ExecSQL;
          qrRef.Close;
          SLRef.Add(EAN13);
        end;
        inc(j);
      end;
    except
      Result := False;
    end;
  finally
    qrDecl.free;
    qrRef.free;
    SLDecl.free;
    SLRef.free;
    pb.Position := 0;
  end;
end;

function LocRefProducer(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
var
  qrRef, qrDecl: TADOQuery;
  i, j, rmax: integer;
  s, sINN, sKpp: string;
  SLRef, SLDecl: TStringList;
begin
  Result := True;
  SLRef := TStringList.Create;
  SLDecl := TStringList.Create;
  qrRef := TADOQuery.Create(DBQ1.Owner);
  qrDecl := TADOQuery.Create(DBQ2.Owner);
  try
    try

      qrDecl.Connection := DBQ1.Connection;
      qrRef.Connection := DBQ2.Connection;

      qrRef.sql.Text :=
        'select * from Producer  where LocRef=true order by inn, kpp';
      qrRef.Open;

      qrDecl.sql.Text := 'select distinct inn,kpp ' + #13#10 +
        'from declaration ' + #13#10 + 'where inn<>'''' and kpp<>''''' +
        #13#10 + 'order by inn,kpp';
      qrDecl.Open;

      NullProgressBar(pb);
      rmax := qrRef.RecordCount + qrDecl.RecordCount;
      j := 0;

      // ������� ������ �������������� ����������� � ���������� �� ������ ����������
      if not qrDecl.Eof then
      begin
        qrDecl.First;
        while not qrDecl.Eof do
        begin
          PosProgressBar(pb, j, rmax);
          s := qrDecl.FieldByName('INN').AsString + '_' + qrDecl.FieldByName
            ('KPP').AsString;
          if SLDecl.IndexOf(s) = -1 then
            SLDecl.Add(s);
          qrDecl.Next;
          inc(j);
        end;
      end;

      // SLRef - ���� � ��������������� � �����������, ������� ������������ � ����������
      if not qrRef.Eof then
      begin
        qrRef.First;
        while not qrRef.Eof do
        begin
          PosProgressBar(pb, j, rmax);
          s := qrRef.FieldByName('INN').AsString + '_' + qrRef.FieldByName
            ('KPP').AsString;
          if SLRef.IndexOf(s) = -1 then
            SLRef.Add(s);
          qrRef.Next;
          inc(j);
        end;
      end;

      qrRef.free;
      qrRef := TADOQuery.Create(DBQ2.Owner);
      qrRef.Connection := DBQ2.Connection;

      rmax := SLRef.Count + SLDecl.Count;
      NullProgressBar(pb);
      // ��������� �� ����� � ���������� ��� LocRef=true,
      // ���� ����� ��������� ��� � ���������� ������� ���� LocRef
      i := 0;
      j := 0;
      while i <= SLRef.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        s := SLRef.Strings[i];
        sINN := Copy(s, 0, pos('_', s) - 1);
        sKpp := Copy(s, pos('_', s) + 1, Length(s) - pos('_', s));
        if SLDecl.IndexOf(s) = -1 then
        begin
          qrRef.sql.Text :=
            'UPDATE Producer SET LocRef=false where inn=''' + sINN + ''' and kpp=''' + sKpp + '''';
          qrRef.ExecSQL;
          SLRef.Delete(i);
        end;
        inc(i);
        inc(j);
      end;

      // ��������� �� ����� � ���������� �� ����������,
      // �������� ����� ���� LocRef = True
      qrRef.Close;
      for i := 0 to SLDecl.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        s := SLDecl.Strings[i];
        sINN := Copy(s, 0, pos('_', s) - 1);
        sKpp := Copy(s, pos('_', s) + 1, Length(s) - pos('_', s));
        if SLRef.IndexOf(s) = -1 then
        begin
          qrRef.sql.Text :=
            'UPDATE Producer SET LocRef=true where inn=''' + sINN +
            ''' and kpp=''' + sKpp + '''';
          qrRef.ExecSQL;
          s := sINN + '_' + sKpp;
          SLRef.Add(s);
        end;
        inc(j);
      end;
    except
      Result := False;
    end;
  finally
    qrRef.free;
    qrDecl.free;
    SLRef.free;
    SLDecl.free;
    pb.Position := 0;
  end;
end;

function LocRefSaler(DBQ1, DBQ2: TADOQuery; pb: TProgressBar): Boolean;
var
  qrRef, qrDecl: TADOQuery;
  i, j, rmax: integer;
  s, sINN, sKpp: string;
  SLRef, SLDecl: TStringList;
begin
  Result := True;
  SLRef := TStringList.Create;
  SLDecl := TStringList.Create;
  qrRef := TADOQuery.Create(DBQ1.Owner);
  qrDecl := TADOQuery.Create(DBQ2.Owner);
  try
    try

      qrDecl.Connection := DBQ1.Connection;
      qrRef.Connection := DBQ2.Connection;

      qrRef.sql.Text :=
        'select * from Saler  where LocRef=true order by orginn, orgkpp';
      qrRef.Open;

      qrDecl.sql.Text := 'select distinct salerinn,salerkpp ' + #13#10 +
        'from declaration ' + #13#10 +
        'where salerinn<>'''' and salerkpp<>''''' + #13#10 +
        'order by salerinn,salerkpp';
      qrDecl.Open;

      NullProgressBar(pb);
      rmax := qrRef.RecordCount + qrDecl.RecordCount;
      j := 1;
      // ������� ������ ����������� ����������� � ���������� �� ������ ����������
      if not qrDecl.Eof then
      begin
        qrDecl.First;
        while not qrDecl.Eof do
        begin
          PosProgressBar(pb, j, rmax);
          s := qrDecl.FieldByName('SalerInn')
            .AsString + '_' + qrDecl.FieldByName('SalerKPP').AsString;
          if SLDecl.IndexOf(s) = -1 then
            SLDecl.Add(s);
          qrDecl.Next;
          inc(j);
        end;
      end;

      // SLRef - ���� � ������������ � �����������, ������� ������������ � ����������
      if not qrRef.Eof then
      begin
        qrRef.First;
        while not qrRef.Eof do
        begin
          PosProgressBar(pb, j, rmax);
          s := qrRef.FieldByName('orginn')
            .AsString + '_' + qrRef.FieldByName('orgkpp').AsString;
          if SLRef.IndexOf(s) = -1 then
            SLRef.Add(s);
          qrRef.Next;
          inc(j);
        end;
      end;

      qrRef.free;
      qrRef := TADOQuery.Create(DBQ2.Owner);
      qrRef.Connection := DBQ2.Connection;

      rmax := SLRef.Count + SLDecl.Count;
      NullProgressBar(pb);
      // ��������� �� ����� � ���������� ��� LocRef=true,
      // ���� ����� ��������� ��� � ���������� ������� ���� LocRef
      i := 0;
      j := 1;
      while i <= SLRef.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        s := SLRef.Strings[i];
        sINN := Copy(s, 0, pos('_', s) - 1);
        sKpp := Copy(s, pos('_', s) + 1, Length(s) - pos('_', s));
        if SLDecl.IndexOf(s) = -1 then
        begin
          qrRef.sql.Text :=
            'UPDATE Saler SET LocRef=false where orginn=''' + sINN + ''' and orgkpp=''' + sKpp + '''';
          qrRef.ExecSQL;
          SLRef.Delete(i);
        end;
        inc(i);
        inc(j);
      end;

      // ��������� �� ����� � ���������� �� ����������,
      // �������� ����� ���� LocRef = True
      qrRef.Close;
      for i := 0 to SLDecl.Count - 1 do
      begin
        PosProgressBar(pb, j, rmax);
        s := SLDecl.Strings[i];
        sINN := Copy(s, 0, pos('_', s) - 1);
        sKpp := Copy(s, pos('_', s) + 1, Length(s) - pos('_', s));
        if SLRef.IndexOf(s) = -1 then
        begin
          qrRef.sql.Text :=
            'UPDATE Saler SET LocRef=true where orginn=''' + sINN +
            ''' and orgkpp=''' + sKpp + '''';
          qrRef.ExecSQL;
          s := sINN + '_' + sKpp;
          SLRef.Add(s);
        end;
        inc(j);
      end;
    except
      Result := False;
    end;
  finally
    qrRef.free;
    qrDecl.free;
    SLDecl.free;
    SLRef.free;
    pb.Position := 0;
  end;
end;

procedure GetDataRef(pb: TProgressBar);
var
  StrSQL: string;
begin
  // ���������� �������� ������ ������������
  GetDataMas(1, SQLRefProduction, pb);
  GetDataMas(2, SQLRefProducer, pb);
  GetDataMas(3, SQLRefSaler, pb);
  // d1 := Kvart_Beg(CurKvart, CurYear);
  // d2 := Kvart_End(CurKvart, CurYear);
  StrSQL :=
    'SELECT PK AS IDL, SER, NUM, DATE_START, DATE_FINISH, REGORG, SALER_PK as SALER_INN, SALER_KPP ' + 'FROM Saler_license';
  GetDataMas(5, StrSQL, pb);
end;

procedure GetDataMas(Mas: integer; StrSQL: String; pb: TProgressBar);
var
  qr: TADOQuery;
  i, k: integer;
  SLRow: TStringList;
begin
  NullProgressBar(pb);
  qr := TADOQuery.Create(fmWork);
  try
    case Mas of
      1, 2, 3, 5:
        qr.Connection := fmWork.con2ADOCon;
      4:
        qr.Connection := fmWork.con1ADOCon;
    end;
    try
      qr.sql.Add(StrSQL);
      qr.Open;
      k := 0;
      while not qr.Eof do
      begin
        SLRow := TStringList.Create;

        for i := 0 to qr.FieldCount - 1 do
          SLRow.Add(qr.Fields[i].AsString);

        case Mas of
          1:
            RefProduction.AddObject(qr.Fields[0].AsString, SLRow);
          2:
            RefProducer.AddObject(qr.Fields[0].AsString, SLRow);
          3:
            RefSaler.AddObject(qr.Fields[0].AsString, SLRow);
          // 4:
          // DataOst[i, k] := qr.Fields[i].AsString;
          5:
            RefLic.AddObject(qr.Fields[0].AsString, SLRow);
        end;
        inc(k);
        PosProgressBar(pb, k, qr.RecordCount);
        qr.Next;
      end;
      ResetProgressBar(pb);
    except
    end;
  finally
    qr.free;
  end;
end;

function GetAllDataDecl(DBQ1, DBQ2: TADOQuery; pb: TProgressBar;
  var SL, SLOst: TStringList; le: TLabel): Boolean;
var
  SLProd, SLPr, SLSal: TStringList;
  decl: TDeclaration;
  i, j, idx: integer;
  SalObj: TSaler_Obj;
  Prod_Obj: TProduction_obj;
  FindProduct, FindProduser, FindSaler, FindLic: Boolean;
  dbegY, dendY: TDateTime;
  StrSQL: string;
begin
  le.Caption := '�������� ������ ����������';
  SLProd := TStringList.Create;
  SLPr := TStringList.Create;
  SLSal := TStringList.Create;
  Result := False;

  UpdateSimvDB(DBQ1);



  DBQ1.ParamCheck := False;
  DBQ1.sql.Clear;
  DBQ1.sql.Text := SQLSelectAllDecl;
  if CurKvart > 1 then
    dbegY := StrToDate('01.01.' + IntToStr(NowYear) + '.')
  else
    dbegY := StrToDate('01.10.' + IntToStr(NowYear-1) + '.');



  dendY := StrToDate('01.01.' + IntToStr(NowYear+1) + '.');
  try
    if SL.Count > 0 then
    begin
      FreeStringList(SL);
      SL := TStringList.Create;
    end;
    try
      DBQ1.Parameters.AddParameter.Name := 'dBeg';
      DBQ1.Parameters.ParamByName('dBeg').DataType := ftDate;
      DBQ1.Parameters.ParamByName('dBeg').value := dbegY;

      DBQ1.Parameters.AddParameter.Name := 'dEnd';
      DBQ1.Parameters.ParamByName('dEnd').DataType := ftDate;
      DBQ1.Parameters.ParamByName('dEnd').value := dendY;

      DBQ1.Open;
      if DBQ1.RecordCount > 0 then
        Result := True;
      DBQ1.First;
      i := 0;
      while not DBQ1.Eof do
      begin
        // ������� ProgressBar
        PosProgressBar(pb, i, DBQ1.RecordCount);
        // ���������
        NullDecl(decl);
        decl.PK := ShortString(DBQ1.FieldByName('PK').AsString);
        decl.SelfKPP := ShortString(DBQ1.FieldByName('SELFKPP').AsString);
        decl.product.EAN13 := ShortString(DBQ1.FieldByName('EAN').AsString);
        decl.Amount := StrInFloat(DBQ1.FieldByName('AMOUNT').AsString);
        decl.TypeDecl := DBQ1.FieldByName('DECLTYPE').AsInteger;
        decl.sal.sINN := ShortString(DBQ1.FieldByName('SALERINN').AsString);
        decl.sal.sKpp := ShortString(DBQ1.FieldByName('SALERKPP').AsString);
        decl.RepDate := DBQ1.FieldByName('REPORTDATE').AsDateTime;
        decl.DeclDate := DBQ1.FieldByName('DECLDATE').AsDateTime;
        decl.DeclNum := ShortString
          (DBQ1.FieldByName('DECLNUMBER').AsString);
        decl.TM := ShortString(DBQ1.FieldByName('TM').AsString);
        FindProduct := GetProduction(fmWork.que2_sql, decl.product,
          FindProduser, True);
        if FindProduct then
        begin
          decl.forma := FormDecl(String(decl.product.FNSCode));
          decl.vol := 0.1 * decl.Amount * decl.product.capacity;
          idx := SLProductionDecl.IndexOf(String(decl.product.EAN13));
          if idx = -1 then
          begin
            Prod_Obj := TProduction_obj.Create;
            Prod_Obj.data := decl.product;
            SLProductionDecl.AddObject(String(decl.product.EAN13),
              Pointer(Prod_Obj));
          end;
          // ������ ������������
          decl.product.ProductNameF := ShortString
            (FullNameProduction(String(decl.product.Productname),
              decl.product.AlcVol, decl.product.capacity));
        end;

        if (decl.TypeDecl = 1) or (decl.TypeDecl = 3) then
          if ((decl.sal.sINN <> 'NULL') and (decl.sal.sKpp <> 'NULL')) or
            ((decl.sal.sINN <> '') and (decl.sal.sKpp <> '')) then
          begin
            FindSaler := GetSaler(fmWork.que2_sql, decl.sal, FindLic, True, False);
            if FindSaler then
            begin
              idx := SLSalerDecl.IndexOf
                (String(decl.sal.sINN + decl.sal.sKpp));
              if idx = -1 then
              begin
                SalObj := TSaler_Obj.Create;
                SalObj.data := decl.sal;
                SLSalerDecl.AddObject
                  (String(decl.sal.sINN + decl.sal.sKpp), Pointer(SalObj))
              end;
            end;
          end;
        if decl.TypeDecl <> 8 then
        begin
          SL.AddObject(DBQ1.FieldByName('PK').AsString, TDecl_obj.Create);
          TDecl_obj(SL.Objects[SL.Count - 1]).data := decl;
        end;
        DBQ1.Next;
        inc(i);
      end;



      // ���������� ������ �������� �� ������ ����
      if CurKvart > 1 then
        dbegY := StrToDate('01.01.' + IntToStr(NowYear) + '.') - 1
      else
        dbegY := StrToDate('01.10.' + IntToStr(NowYear-1) + '.');
//
//        dbegY := StrToDate('01.01.' + IntToStr(NowYear) + '.') - 1;
      StrSQL := 'SELECT Sum((Amount)*(Switch(DeclType=''1'',1, ' +
        'DeclType=''2'',-1, DeclType=''3'',-1, ' +
        'DeclType=''4'',1,DeclType=''5'',-1, DeclType=''6'',-1, '+
        'DeclType=''7'',1))) AS Amount1, ean13, selfkpp '
        + 'FROM DECLARATION WHERE reportdate <= datevalue(''' + DateToStr
        (dbegY) + ''') ' + 'GROUP BY EAN13, selfkpp';

      DBQ1.Close;
      DBQ1.ParamCheck := False;
      DBQ1.sql.Clear;
      DBQ1.sql.Text := StrSQL;
      DBQ1.Open;
      if DBQ1.RecordCount > 0 then
        Result := True;
      DBQ1.First;
      j := 0;
      while not DBQ1.Eof do
      begin
        // ������� ProgressBar
        PosProgressBar(pb, j, DBQ1.RecordCount);
        // ���������
        if (DBQ1.FieldByName('AMOUNT1').AsFloat > 0.000001) or
          (DBQ1.FieldByName('AMOUNT1').AsFloat < -0.000001) then
        begin
          NullDecl(decl);
          decl.PK := '-1';
          decl.SelfKPP := ShortString(DBQ1.FieldByName('selfkpp').AsString);
          decl.product.EAN13 := ShortString(DBQ1.FieldByName('EAN13').AsString);
          decl.Amount := DBQ1.FieldByName('AMOUNT1').AsFloat;
          decl.TypeDecl := 1;
          decl.sal.sINN := '1234567894';
          decl.sal.sKpp := '123400001';
          decl.RepDate := dbegY;
          decl.DeclDate := dbegY;
          decl.DeclNum := '�������';
          decl.TM := '';
          FindProduct := GetProduction(fmWork.que2_sql, decl.product,
            FindProduser, True);
          if FindProduct then
          begin
            decl.forma := FormDecl(String(decl.product.FNSCode));
            decl.vol := 0.1 * decl.Amount * decl.product.capacity;
            idx := SLProductionDecl.IndexOf(String(decl.product.EAN13));
            if idx = -1 then
            begin
              Prod_Obj := TProduction_obj.Create;
              Prod_Obj.data := decl.product;
              SLProductionDecl.AddObject(String(decl.product.EAN13),
                Pointer(Prod_Obj));
            end;
            // ������ ������������
            decl.product.ProductNameF := ShortString
              (FullNameProduction(String(decl.product.Productname),
                decl.product.AlcVol, decl.product.capacity));
          end;

          if (decl.TypeDecl = 1) or (decl.TypeDecl = 3) then
            if ((decl.sal.sINN <> 'NULL') and (decl.sal.sKpp <> 'NULL')) or
              ((decl.sal.sINN <> '') and (decl.sal.sKpp <> '')) then
            begin
              FindSaler := GetSaler(fmWork.que2_sql, decl.sal, FindLic,
                True, False);
              if FindSaler then
              begin
                idx := SLSalerDecl.IndexOf
                  (String(decl.sal.sINN + decl.sal.sKpp));
                if idx = -1 then
                begin
                  SalObj := TSaler_Obj.Create;
                  SalObj.data := decl.sal;
                  SLSalerDecl.AddObject
                    (String(decl.sal.sINN + decl.sal.sKpp),
                    Pointer(SalObj))
                end;
              end;
            end;
          SL.AddObject(String(decl.PK), TDecl_obj.Create);
          TDecl_obj(SL.Objects[SL.Count - 1]).data := decl;
        end;

        DBQ1.Next;
        inc(j);
      end;
    except
      Result := False;
    end;
  finally
    SLSal.free;
    SLPr.free;
    SLProd.free;
  end;
end;

procedure NullDecl(var decl: TDeclaration);
begin
  decl.PK := '';
  decl.SelfKPP := fOrg.Kpp0;
  decl.DeclDate := dEnd;
  decl.RepDate := dEnd;
  decl.TypeDecl := -1;
  decl.DeclNum := '';
  decl.Amount := 0;
  decl.Ost.PK := '';
  decl.Ost.PK_EAN := '';
  decl.Ost.Value:=0;
  decl.Ost.DateOst := decl.DeclDate;
  decl.vol := 0;
  decl.Amount_dal := 0;
  decl.Vol_dal := 0;
  decl.TM := '';
  decl.forma := -1;
  NullProd(decl.product);
  NullSaler(decl.sal);
end;

function FormDecl(grup: string): integer;
begin
  Result := -1;
  if (grup = '261') or (grup = '262') or (grup = '263') then
  begin
    Result := 12;
    exit;
  end;
  if ((grup > '000') and (grup < '500')) then
    Result := 11;
  if ((grup >= '500') and (grup <= '999')) then
    Result := 12;
end;

procedure ChangeKvart(DB, dE: TDateTime);
var i:integer;
begin
  for i := 0 to High(masErr) do
    masErr[i]:=0;
  // �������� ��� ������� ��� � ������� �������� ��� ���
  DvizenieKppEan(SLOborotKpp, SLAllDecl, DB, dE);
//  // ���������� ������� ��� �������� ��������
  FilterData(DB, dE, masErr, SLOborotKpp);
  // ������������ ������� ��������
  FilterOstatki(SLOstatkiKpp, SLOborotKpp, masErr);
end;

procedure FilterData(DB, dE: TDateTime; var aErr: array of integer;
  SLOb: TStringList);
var
  i: integer;
  decl: TDeclaration;
  IsDeclErr: Boolean;
begin
  SLDataPrih.Clear;
  SLDataRash.Clear;
  SLDataVPost.Clear;
  SLDataVPok.Clear;
  SLDataBrak.Clear;
  SLDataMoving.Clear;

  for i := 0 to High(aErr) do
    aErr[i] := 0;
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    NullDecl(decl);
    decl := TDecl_obj(SLAllDecl.Objects[i]).data;
    if DBetween(Int(decl.RepDate), DB, dE) then
    begin
      IsDeclErr := ErrorRowDecl(decl.TypeDecl, decl, SLOb, DB, dE);
      case decl.TypeDecl of
        1,6:
          begin
            SLDataPrih.AddObject(SLAllDecl.Strings[i],
              TDecl_obj(SLAllDecl.Objects[i]));
            if IsDeclErr then
              inc(aErr[0]);
          end;
        2,7:
          begin
            SLDataRash.AddObject(SLAllDecl.Strings[i], TDecl_obj(SLAllDecl.Objects[i]));
            if IsDeclErr then
              inc(aErr[1]);
          end;
        3:
          begin
            SLDataVPost.AddObject(SLAllDecl.Strings[i],
              TDecl_obj(SLAllDecl.Objects[i]));
            if IsDeclErr then
              inc(aErr[2]);
          end;
        4:
          begin
            SLDataVPok.AddObject(SLAllDecl.Strings[i],
              TDecl_obj(SLAllDecl.Objects[i]));
            if IsDeclErr then
              inc(aErr[3]);
          end;
        5:
          begin
            SLDataBrak.AddObject(SLAllDecl.Strings[i],
              TDecl_obj(SLAllDecl.Objects[i]));
            if IsDeclErr then
              inc(aErr[4]);
          end;
//        6:
//          begin
//            SLDataMoving.AddObject(SLAllDecl.Strings[i],
//              TDecl_obj(SLAllDecl.Objects[i]));
//          end;
      end;
    end;
  end;
end;

function ErrorRowDecl(TypeDecl: integer; decl: TDeclaration;
  SLOb: TStringList; dB, dE:TDateTime): Boolean;
var
  mv: TMove;
  idx, i: integer;
  e:extended;
  sd:array of TDeclErr;
begin
  try
    SetLength(sd,0);
    Result := False;
    if TypeDecl = 2 then
    begin
      idx := SLOb.IndexOf(String(decl.SelfKPP + decl.product.EAN13));
      if idx <> -1 then
      begin
        mv := Pointer(SLOb.Objects[idx]);
        if (mv.dv.ost0 < 0) or (mv.dv.ost1 < 0) then
        begin
          Result := True;
          SetLength(sd,Length(sd) + 1);
          sd[High(sd)] := dErrOst;
          exit;
        end;
        if decl.Ost.Value <> 0 then
        begin
          e:= RoundTo(mv.dv.ost1 - decl.Ost.Value, -ZnFld);
          if e < 0 then
          begin
            Result := True;
            SetLength(sd,Length(sd) + 1);
            sd[High(sd)] := dErrOst;
            exit;
          end;
        end;
      end;

    end;
    if ((TypeDecl = 1) or (TypeDecl = 3)) and (decl.sal.sName = NoSal) then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrNoSal;
      exit;
    end;
    if ((TypeDecl = 1) or (TypeDecl = 3)) and (decl.sal.lic.dEnd < dB) then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrDataLic;
      exit;
    end;
    if decl.product.Productname = NoProd then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrNoProd;
      exit;
    end;
    if decl.product.prod.pName = NoProducer then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrNoProducer;
      exit;
    end;
    if not DBetween(Int(decl.DeclDate), dB, dE) then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrDate;
      exit;
    end;
    if decl.Amount <= 0 then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrAmount;
      exit;
    end;
    if CheckSelfKpp(String(decl.SelfKPP)) > 0 then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrSelfKPP;
      exit;
    end;
    if (TypeDecl = 1) and (decl.DeclNum = '') then
    begin
      Result := True;
      SetLength(sd,Length(sd) + 1);
      sd[High(sd)] := dErrDeclNum;
      exit;
    end;
  finally
  SetLength(dError,Length(sd));
  for i := 0 to High(sd) do
    dError[i] := sd[i];
  end;
end;

function CheckSelfKpp(val: string): integer;
begin // �������� �� ������������ ����� ���
  Result := -1;
  if (not StrCheck(val)) or (Length(val) <> 9) or (val = '000000000') then
  begin
    Result := 1;
    exit;
  end
  else if SLKPP.IndexOf(val) = -1 then
  begin
    Result := 2;
    exit;
  end;
end;

function CheckOst(ost1,OstAM:Extended):integer;
var e, e1,e2:extended;
begin
  Result := -1;
  if OstAM <> 0  then
  begin
    e1 := OstAM;
    e2 := ost1;
    e := RoundTo(e2-e1,-ZnFld);
    if (e < 0) then
      Result:=1
    else
      Result:=4;
  end;
end;

procedure DvizenieKppEan(var SL: TStringList; SLData: TStringList;
  DB, dE: TDateTime);
var
  RowStart, i, j: integer;
  decl: TDecl_obj;
  dv: TMove;
  SelfKPP: string;
  FindKpp: Boolean;
  DataKpp, SLOKpp: TStringList;
  c: integer;
begin
  if SL.Count > 0 then
  begin
    for i := 0 to SL.Count - 1 do
      if Assigned(SL.Objects[i]) then
      begin
        dv := Pointer(SL.Objects[i]);
        // dv.Destroy;
        dv.free;
        dv := nil;
      end;
    SL.Clear;
  end;

  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� selfKpp � ������� ����������
  SortSLCol(2, SLData);
  DataKpp := TStringList.Create;
  SLOKpp := TStringList.Create;
  try
    RowStart := 0;
    SelfKPP := '';

    while (SelfKPP = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if decl.data.RepDate <= dE then
      begin
        SelfKPP := String(decl.data.SelfKPP);
        DataKpp.AddObject(SelfKPP, decl);
      end;
      inc(RowStart);
    end;
    if SelfKPP = '' then
      exit;
    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if decl.data.RepDate <= dE then
      begin
        if decl.data.SelfKPP = ShortString(SelfKPP) then
        begin
          FindKpp := True;
          DataKpp.AddObject(SelfKPP, decl);
        end
        else
          FindKpp := False;

        if not FindKpp then
        begin
          SLOKpp.Clear;
          DvizenieEAN(SLOKpp, DataKpp, DB, dE);
          c := SL.Count;
          SL.AddStrings(SLOKpp);
          for j := 0 to SLOKpp.Count - 1 do
          begin
            dv := Pointer(SLOKpp.Objects[j]);
            SL.Objects[c + j] := dv;
          end;
          SelfKPP := String(decl.data.SelfKPP);
          DataKpp.Clear;
          DataKpp.AddObject(SLAllDecl.Strings[i], decl);
        end;
      end;
    end;

    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> SelfKPP then
      begin
        SLOKpp.Clear;
        DvizenieEAN(SLOKpp, DataKpp, DB, dE);
        c := SL.Count;
        SL.AddStrings(SLOKpp);
        for j := 0 to SLOKpp.Count - 1 do
        begin
          dv := Pointer(SLOKpp.Objects[j]);
          SL.Objects[c + j] := dv;
        end;
      end;
    end
    else if DataKpp.Count > 0 then
    begin
      SLOKpp.Clear;
      DvizenieEAN(SLOKpp, DataKpp, DB, dE);
      c := SL.Count;
      SL.AddStrings(SLOKpp);
      for j := 0 to SLOKpp.Count - 1 do
      begin
        dv := Pointer(SLOKpp.Objects[j]);
        SL.Objects[c + j] := dv;
      end;
    end;
  finally
    SLOKpp.free;
    DataKpp.free;
  end;
end;

procedure DvizenieEAN(var SL: TStringList; SLData: TStringList;
  DB, dE: TDateTime);
var
  i, RowStart: integer;
  FindEan: Boolean;
  decl: TDecl_obj;
  dMove: TMove;
  ean: string;
  DataEAN: TStringList;
begin
  FindEan := False;
  for i := 0 to SL.Count - 1 do
  begin
    dMove := Pointer(SL.Objects[i]);
    if dMove <> nil then
    begin
      dMove.free;
      dMove := nil;
    end;
  end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� EAN � ������� ����������
  SortSLCol(3, SLData);
  DataEAN := TStringList.Create;
  try
    RowStart := 0;
    ean := '';

    while (ean = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if decl.data.RepDate <= dE then
      begin
        ean := String(decl.data.product.EAN13);
        DataEAN.AddObject(ean, decl);
      end;
      inc(RowStart);
    end;

    if SLData.Count = 1 then
    begin
      decl := Pointer(SLData.Objects[RowStart-1]);
      DvizenieOneEAN(DataEAN, DB, dE, dMove);
      if dMove <> nil then
        SL.AddObject(String(decl.data.SelfKPP) + ean, dMove);
      exit;
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if decl.data.RepDate <= dE then
      begin
        if decl.data.product.EAN13 = ShortString(ean) then
        begin
          FindEan := True;
          DataEAN.AddObject(SLData.Strings[i], decl);
        end
        else
          FindEan := False;

        if not FindEan then
        begin
          DvizenieOneEAN(DataEAN, DB, dE, dMove);
          if dMove <> nil then
            SL.AddObject(String(decl.data.SelfKPP) + ean, dMove);
          ean := String(decl.data.product.EAN13);
          DataEAN.Clear;
          DataEAN.AddObject(SLData.Strings[i], decl);
        end;
      end;
    end;

    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> ean then
      begin
        DvizenieOneEAN(DataEAN, DB, dE, dMove);
        if dMove <> nil then
          SL.AddObject(String(decl.data.SelfKPP) + ean, dMove);
      end;
    end
    else
    begin
      if DataEAN.Count > 0 then
      begin
        DvizenieOneEAN(DataEAN, DB, dE, dMove);
        if dMove <> nil then
          SL.AddObject(String(decl.data.SelfKPP) + ean, dMove);
      end;
    end;

  finally
    DataEAN.free;
  end;
end;

procedure DvizenieOneEAN(SL: TStringList; d1, d2: TDateTime;
  var dviz: TMove);
var
  i: integer;
  decl: TDeclaration;
  V, ost, ost_dal: Extended;
  rd: TDateTime;
  dt: integer;
begin
  dviz := TMove.Create;
  V := 0;
  ost := 0;
  ost_dal := 0;

  dviz.dv.ost0 := 0;
  dviz.dv.prih := 0;
  dviz.dv.rash := 0;
  dviz.dv.vozv_post := 0;
  dviz.dv.vozv_pokup := 0;
  dviz.dv.brak := 0;
  dviz.dv.ost1 := 0;
  dviz.dv.oder := 0;
  dviz.dv.move_in := 0;
  dviz.dv.move_out := 0;
  dviz.dv.ostAM :=0;

  dviz.dv_dal.ost0 := 0;
  dviz.dv_dal.prih := 0;
  dviz.dv_dal.rash := 0;
  dviz.dv_dal.vozv_post := 0;
  dviz.dv_dal.vozv_pokup := 0;
  dviz.dv_dal.brak := 0;
  dviz.dv_dal.ost1 := 0;
  dviz.dv_dal.oder := 0;
  dviz.dv_dal.move_in := 0;
  dviz.dv_dal.move_out := 0;
  dviz.dv_dal.ostAM:=0;

  SortSLCol(7, SL, CompDateAsc);
  for i := 0 to SL.Count - 1 do
  begin
    NullDecl(decl);
    decl := TDecl_obj(SL.Objects[i]).data;
    dviz.FNS := decl.product.FNSCode;
    dviz.forma := decl.forma;
    dt := decl.TypeDecl;
    rd := Int(decl.RepDate);
    V := decl.product.capacity * decl.Amount;
    if (rd < d1) then
    begin
      case dt of
        1:
          begin
            ost := ost + decl.Amount; // ������
            ost_dal := ost_dal + V; // ������
            dviz.sal := decl.sal;
          end;
        2:
          begin
            ost := ost - decl.Amount; // ������
            ost_dal := ost_dal - V;
          end;
        3:
          begin
            ost := ost - decl.Amount; // ������� ����������
            ost_dal := ost_dal - V;
          end;
        4:
          begin
            ost := ost + decl.Amount; // ������� �� ����������
            ost_dal := ost_dal + V;
          end;
        5:
          begin
            ost := ost - decl.Amount; // �������� (����)
            ost_dal := ost_dal - V;
          end;
        6:
          begin
            ost := ost - decl.Amount; // �����������-
            ost_dal := ost_dal - V;
          end;
        7:
          begin
            ost := ost + decl.Amount; // �����������+
            ost_dal := ost_dal + V;
          end;
      end;
    end
    else if DBetween(Int(rd), d1, d2) then
    begin
      case dt of
        1:
          begin
            dviz.dv.prih := dviz.dv.prih + decl.Amount; // ������
            dviz.dv_dal.prih := dviz.dv_dal.prih + V;
            dviz.sal := decl.sal;
          end;
        2:
          begin
            dviz.dv.rash := dviz.dv.rash + decl.Amount; // ������
            dviz.dv_dal.rash := dviz.dv_dal.rash + V;
            dviz.dv.ostAM := dviz.dv.ostAM + decl.Ost.Value;
            dviz.dv_dal.ostAM := dviz.dv_dal.ostAM + decl.Ost.Value*decl.product.Capacity;
          end;
        3:
          begin
            dviz.dv.vozv_post := dviz.dv.vozv_post + decl.Amount;
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.vozv_post := dviz.dv_dal.vozv_post + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        4:
          begin
            dviz.dv.vozv_pokup := dviz.dv.vozv_pokup + decl.Amount;
            dviz.dv.oder := dviz.dv.oder + decl.Amount;

            dviz.dv_dal.vozv_pokup := dviz.dv_dal.vozv_pokup + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder + V;
          end;
        5:
          begin
            dviz.dv.brak := dviz.dv.brak + decl.Amount; // �������� (����)
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.brak := dviz.dv_dal.brak + V; // �������� (����)
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        6:
          begin
            dviz.dv.move_out := dviz.dv.move_out + decl.Amount;  // ��������-
            dviz.dv.oder := dviz.dv.oder - decl.Amount;

            dviz.dv_dal.move_out := dviz.dv_dal.move_out + V;
            dviz.dv_dal.oder := dviz.dv_dal.oder - V;
          end;
        7:
          begin
            dviz.dv.move_in := dviz.dv.move_in + decl.Amount; // ��������+
            dviz.dv.oder := dviz.dv.oder + decl.Amount;

            dviz.dv_dal.move_in := dviz.dv_dal.move_in + V; // ��������+
            dviz.dv_dal.oder := dviz.dv_dal.oder + V;
          end;
      end;
    end;
  end;

  dviz.dv.ost0 := ost;
  dviz.dv_dal.ost0 := ost_dal;

  dviz.dv.ost1 := dviz.dv.ost0 + dviz.dv.prih - dviz.dv.rash -
    dviz.dv.vozv_post + dviz.dv.vozv_pokup - dviz.dv.brak +
    dviz.dv.move_in - dviz.dv.move_out;
  dviz.dv_dal.ost1 := dviz.dv_dal.ost0 + dviz.dv_dal.prih -
    dviz.dv_dal.rash - dviz.dv_dal.vozv_post + dviz.dv_dal.vozv_pokup -
    dviz.dv_dal.brak + dviz.dv_dal.move_in - dviz.dv_dal.move_out;

  dviz.dv.ost0:=OkruglDataDecl(dviz.dv.ost0);
  dviz.dv.prih:=OkruglDataDecl(dviz.dv.prih);
  dviz.dv.rash:=OkruglDataDecl(dviz.dv.rash);
  dviz.dv.vozv_post:=OkruglDataDecl(dviz.dv.vozv_post);
  dviz.dv.vozv_pokup:=OkruglDataDecl(dviz.dv.vozv_pokup);
  dviz.dv.brak:=OkruglDataDecl(dviz.dv.brak);
  dviz.dv.ost1:=OkruglDataDecl(dviz.dv.ost1);
  dviz.dv.oder:=OkruglDataDecl(dviz.dv.oder);
  dviz.dv.move_in:=OkruglDataDecl(dviz.dv.move_in);
  dviz.dv.move_out:=OkruglDataDecl(dviz.dv.move_out);
  dviz.dv.ostAM:=OkruglDataDecl(dviz.dv.ostAM);

  dviz.dv_dal.ost0 := dviz.dv_dal.ost0 * 0.1;
  dviz.dv_dal.prih := dviz.dv_dal.prih * 0.1;
  dviz.dv_dal.rash := dviz.dv_dal.rash * 0.1;
  dviz.dv_dal.vozv_post := dviz.dv_dal.vozv_post * 0.1;
  dviz.dv_dal.vozv_pokup := dviz.dv_dal.vozv_pokup * 0.1;
  dviz.dv_dal.brak := dviz.dv_dal.brak * 0.1;
  dviz.dv_dal.ost1 := dviz.dv_dal.ost1 * 0.1;
  dviz.dv_dal.oder := dviz.dv_dal.oder * 0.1;
  dviz.dv_dal.move_in := dviz.dv_dal.move_in * 0.1;
  dviz.dv_dal.move_out := dviz.dv_dal.move_out * 0.1;
  dviz.dv_dal.ostAM := dviz.dv_dal.ostAM * 0.1;

  dviz.dv_dal.ost0:=OkruglDataDecl(dviz.dv_dal.ost0);
  dviz.dv_dal.prih:=OkruglDataDecl(dviz.dv_dal.prih);
  dviz.dv_dal.rash:=OkruglDataDecl(dviz.dv_dal.rash);
  dviz.dv_dal.vozv_post:=OkruglDataDecl(dviz.dv_dal.vozv_post);
  dviz.dv_dal.vozv_pokup:=OkruglDataDecl(dviz.dv_dal.vozv_pokup);
  dviz.dv_dal.brak:=OkruglDataDecl(dviz.dv_dal.brak);
  dviz.dv_dal.ost1:=OkruglDataDecl(dviz.dv_dal.ost1);
  dviz.dv_dal.oder:=OkruglDataDecl(dviz.dv_dal.oder);
  dviz.dv_dal.move_in:=OkruglDataDecl(dviz.dv_dal.move_in);
  dviz.dv_dal.move_out:=OkruglDataDecl(dviz.dv_dal.move_out);
  dviz.dv_dal.ostAM:=OkruglDataDecl(dviz.dv_dal.ostAM);

  dviz.product := decl.product;
  dviz.SelfKPP := decl.SelfKPP;
  if not IsNotNullDecl(dviz) then
  begin
    dviz.free;
    dviz := nil;
  end;
end;


function OkruglDataDecl(val: Extended):Extended;
  function RoundEx( X: Extended; Precision : Integer ): Extended;
  {Precision : 1 - �� �����, 10 - �� �������, 100 - �� �����...}
  var
   ScaledFractPart, idx : Extended;
  begin
   ScaledFractPart := Frac(X)*Precision;
   idx := Roundto(Frac(ScaledFractPart),-2);
   ScaledFractPart := Int(ScaledFractPart);
   if idx >= 0.5 then
     ScaledFractPart := ScaledFractPart + 1;
   if idx <= -0.5 then
     ScaledFractPart := ScaledFractPart - 1;
   RoundEx := Int(X) + ScaledFractPart/Precision;
  end;
var s:Extended;
begin
//  SetRoundMode(rmUp);
//  Result := RoundTo(val, -ZnFld);
  s:= RoundEx(Val,100000);
  Result:= s;
end;


function IsNotNullDecl(dM: TMove): Boolean;
begin
  Result := False;
  if dM.dv.ost0 <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.prih <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.rash <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.vozv_post <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.vozv_pokup <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.brak <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.ost1 <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.move_in <> 0 then
  begin
    Result := True;
    exit;
  end;
  if dM.dv.move_out <> 0 then
  begin
    Result := True;
    exit;
  end;
end;

procedure FilterOstatki(var SLOst: TStringList; SLData: TStringList;
  var aErr: array of integer);
var
  i: integer;
begin
  SLOst.Clear;
  aErr[5] := 0;
  aErr[6] := 0;
  for i := 0 to SLData.Count - 1 do
  begin
    // mov := TMove(SLData.Objects[i]);
    if TMove(SLData.Objects[i]).dv.ost1 <> 0 then
    begin
      SLOst.AddObject(SLData.Strings[i], SLData.Objects[i]);
      if TMove(SLData.Objects[i]).dv.ost0 < 0 then
        inc(aErr[5]);
      if TMove(SLData.Objects[i]).dv.ost1 < 0 then
        inc(aErr[6]);
    end;
  end;
end;

procedure cbKvartChange(sKvart: string);
var
  cKvart, cYear: string;
  fn: string;
  tIniF: TIniFile;
  y,m,d:word;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    if Length(sKvart) > 0 then
    begin
      cKvart := Copy(sKvart, 0, pos(nkv, sKvart) - 2);
      cYear := Copy(sKvart, Length(sKvart) - 8, 4);
      CurKvart := StrToInt(cKvart);
      CurYear := StrToInt(cYear);
      dBeg := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
      dEnd := Kvart_End(StrToInt(cKvart), StrToInt(cYear));

      // ������� �������
//      tIniF.WriteInteger(SectionName, 'Kvartal', CurKvart);
      decodeDate(Now(),y,m,d);
      if y = NowYear then
        if idxKvart > 0 then
          tIniF.WriteInteger(SectionName, 'Kvartal', CurKvart);
      // ������� ���
      tIniF.WriteInteger(SectionName, 'Year', CurYear);
    end;
    ChangeKvart(dBeg, dEnd);
  finally
    tIniF.free;
  end;
end;

function ExistRegPeriod: Boolean;
var
  s: string;
begin
  Result := False;
  s := IntToStr(CurKvart) + ' ' + nkv + ' ' + IntToStr(CurYear) + ' ' + ng;
  if SLPerReg.IndexOf(s) <> -1 then
    Result := True;
end;

function SetCurKvart: integer;
var
  s: string;
begin
  Result := -1;
  s := IntToStr(CurKvart) + ' ' + nkv + ' ' + IntToStr(CurYear) + ' ' + ng;
  Result := SLPerRegYear.IndexOf(s);
end;

procedure SortSLCol(cols: integer; SL: TStringList;
  aCompare: TStringListSortCompare = nil);
var
  SlSort: TStringList;
  decl: TDecl_obj;
  i: integer;
begin
  // ����������� ������.
  SlSort := TStringList.Create;

  // ��������� � ����������� ������ ����: "������ - ������".
  // � �������� ������ ����� ���������� �������� ����� ����
  // �������, �� �������� ���� �������� ����������. ����� ����� �� ������, �������
  // �� ����������� ������������� ������� - ����� �� ����������� ����������
  // ����� �������, ���� ��� ����.
  // � � �������� ������� ����� ������������ ����� ��������������� ����� �������.
  for i := 0 to SL.Count - 1 do
  begin
    // ������ ��������� ��� ����� ������ �������.
    decl := Pointer(SL.Objects[i]);
    // ��������� � ����������� ������ ����:
    // ������: ������ �� ������ �������� �������;
    // ������: ���������, ���������� ����� ������ �������.
    case cols of
      1:
        SlSort.AddObject(String(decl.data.PK), decl);
      2:
        SlSort.AddObject(String(decl.data.SelfKPP), decl);
      3:
        SlSort.AddObject(String(decl.data.product.EAN13), decl);
      4:
        SlSort.AddObject(String(decl.data.sal.sINN + decl.data.sal.sKpp),
          decl);
      5:
        SlSort.AddObject(String(decl.data.sal.sKpp), decl);
      6:
        SlSort.AddObject(String(decl.data.product.FNSCode), decl);
      7:
        SlSort.AddObject(DateTimeToStr(decl.data.DeclDate), decl);
      8:
        SlSort.AddObject(String(decl.data.product.prod.pId), decl);
    end;
  end;

  // ��������� �������.
  if Assigned(aCompare) then
    SlSort.CustomSort(aCompare)
  else
    SlSort.Sort;

  SL.Clear;
  SL.Assign(SlSort);

  // ���������� ����������� ������.
  FreeAndNil(SlSort);
end;

function CountNamesSL(SL: TStringList): integer;
var
  s: string;
  i: integer;
begin
  Result := 0;
  SL.Sort;
  if SL.Count = 0 then
    exit;
  s := SL.Names[0];
  if SL.Count = 1 then
  begin
    inc(Result);
    exit;
  end;

  for i := 1 to SL.Count - 1 do
    if SL.Names[i] <> s then
    begin
      inc(Result);
      s := SL.Names[i];
    end;
  inc(Result);
end;

procedure FillDataGrid(SG: TADVStringGrid);
var
  i, j: integer;
begin

  SetLength(DataGrid, 0, 0);
  SetLength(DataGrid, SG.AllColCount - SG.FixedCols,
    SG.AllRowCount - SG.FixedRows - SG.FixedFooters);
  for j := SG.FixedRows to SG.AllRowCount - SG.FixedRows - SG.FixedFooters
    do
    for i := SG.FixedCols to SG.AllColCount - SG.FixedCols do
      DataGrid[i - SG.FixedCols, j - SG.FixedRows] := SG.Cells[i, j];
end;

procedure WriteRowPrihodSG(var SLErr: TStringList; var decl: TDeclaration;
  SG: TADVStringGrid; row: integer);
var
  idx: integer;
begin
  SG.RowColor[row] := clNone;
  SG.Objects[0, row] := Pointer(0);
  // cRowErr := 0;
  decl.forma := FormDecl(String(decl.product.FNSCode));
  decl.vol := 0.1 * decl.Amount * decl.product.capacity;

  SG.Cells[17, row] := String(decl.PK);

  SG.Cells[1, row] := IntToStr(decl.forma);

  SG.Cells[2, row] := String(decl.product.FNSCode);
  SG.RowEnabled[row] := True;

  SG.Cells[3, row] := String(decl.SelfKPP);
  idx := CheckSelfKpp(String(decl.SelfKPP));
  SetColorCells(SG, [3], row, idx);

  SG.Cells[4, row] := String(decl.DeclNum);
  if decl.TypeDecl = 1 then
  begin
    idx := CheckValueCells(String(decl.DeclNum));
    SetColorCells(SG, [4], row, idx);
  end;

  SG.Cells[5, row] := DateToStr(decl.DeclDate);
  idx := CheckDate(decl.DeclDate);
  SetColorCells(SG, [5], row, idx);

  SG.Cells[6, row] := String(decl.sal.sINN);
  SG.Cells[7, row] := String(decl.sal.sKpp);
  SG.Cells[8, row] := String(decl.sal.sName);
  idx := CheckSaler(decl.sal);
  if idx <> -1 then
    AddDataExchange(idx, cQueryINN + String(decl.sal.sINN) + AddSal2 + String
        (decl.sal.sKpp));

  SetColorCells(SG, [6, 7, 8], row, idx);

  SG.Cells[9, row] := String(decl.product.EAN13);
  SG.Cells[10, row] := String(decl.product.ProductNameF);
  idx := CheckProduction(decl.product);
  if idx <> -1 then
    AddDataExchange(idx, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [9, 10], row, idx);

  SG.Cells[11, row] := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
  SetColorCells(SG, [11], row, CheckChislo(True, decl.Amount));

  SG.Cells[12, row] := FloatToStrF(decl.vol, ffFixed, maxint, ZnFld);
  SetColorCells(SG, [12], row, CheckChislo(True, decl.vol));

  SG.Cells[13, row] := String(decl.TM);

  SG.Cells[14, row] := String(decl.product.prod.pName);
  SG.Cells[15, row] := String(decl.product.prod.pINN);
  SG.Cells[16, row] := String(decl.product.prod.pKPP);
  idx := CheckProducer(decl.product.prod);
  if idx <> -1 then
    AddDataExchange(2, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [14, 15, 16], row, idx);

  CountErrSG(fPrihod, SLErr, SG, row);
end;

procedure PropCellsSG(ASR: TAutoSizeRows; SG: TADVStringGrid;
  CalcCol, NoRes, HideCol: array of integer);
var
  i, idx: integer;
begin
  try

    // SG.AutoNumberStart := 0;
    // SG.AutoNumberOffset := 0;
    SG.AutoNumberCol(0);

    SG.ColumnSize.Rows := ASR;
    SG.AutoSize := True;

    SG.UnHideColumnsAll;

    Calc_Footer(SG, CalcCol);

    for i := 0 to High(NoRes) do
    begin
      idx := NoRes[i];
      if not SG.IsHiddenColumn(idx) then
        SG.AllColWidths[idx] := Length(SG.ColumnHeaders.Strings[idx]) * 10;
    end;

    // ������� ������� ��������
    for i := 0 to High(HideCol) do
    begin
      idx := HideCol[i];
      SG.HideColumn(idx);
    end;
  except

  end;
end;

procedure Calc_Footer(SG: TADVStringGrid; masCalc: array of integer);
var
  i, idx: integer;
begin
  // SG.FloatingFooter.Visible := true;
  SG.FloatingFooter.ColumnCalc[0] := acCount;

  for i := 0 to High(masCalc) do
  begin
    idx := SG.RealColIndex(masCalc[i]);
    SG.FloatingFooter.ColumnCalc[idx] := acSum;
    SG.CalcFooter(idx);
  end;
  // SG.FloatingFooter.Invalidate;
end;

procedure SetColorCells(SG: TADVStringGrid; ACol: array of integer;
  ARow, err: integer);
var
  i: integer;
  PK: string;
begin
  for i := 0 to High(ACol) do
    case err of
      - 1:
        begin
          SG.AllObjects[ACol[i], ARow] := nil;
          PK := SG.AllCells[SG.AllColCount - 1, ARow];
          if NoValue(PK) then
            SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)]
              .BrushColor := clInsert
          else if LongInt(SG.AllObjects[0, ARow]) = 0 then
            SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)]
              .BrushColor := clNone
          else
            SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)]
              .BrushColor := clInsert;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clWindowText;
        end;
      1:
        begin
          SG.AllObjects[ACol[i], ARow] := Pointer(1);
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].BrushColor :=
            clCheck;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clWindowText;
        end;
      2:
        begin
          SG.AllObjects[ACol[i], ARow] := Pointer(2);
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].BrushColor :=
            clErr;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clErrFont;
        end;
      3:
        begin
          SG.AllObjects[ACol[i], ARow] := Pointer(3);
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].BrushColor :=
            clLic;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clWindowText;
        end;
      4:
        begin
          SG.AllObjects[ACol[i], ARow] := Pointer(4);
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].BrushColor :=
            clOst;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clWindowText;
        end;
      5:
        begin
          SG.AllObjects[ACol[i], ARow] := Pointer(5);
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].BrushColor :=
            clErrOst;
          SG.CellProperties[ACol[i], SG.DisplRowIndex(ARow)].FontColor :=
            clWindowText;
        end;
    end;
end;

function CheckValueCells(val: string): integer;
begin
  Result := -1;
  if val = '' then
    Result := 1;
end;

function CheckDate(dd: TDateTime): integer;
begin
  Result := -1;
  if not DBetween(Int(dd), dBeg, dEnd) then
    Result := 1;
end;

function CheckSaler(sal: TSaler): integer;
var
  b1: Boolean;
begin
  Result := -1;
  if (not StrCheck(String(sal.sKpp))) or (Length(String(sal.sKpp)) <> 9) or
    (sal.sKpp = '000000000') then
  begin
    Result := 1;
    exit;
  end;
  If (not InnCheck(String(sal.sINN), b1)) and
    (not INN12Check(String(sal.sINN), b1)) then
  begin
    Result := 1;
    exit;
  end;
  if sal.sName = NoSal then
  begin
    Result := 2;
    exit;
  end;
  if sal.lic.dEnd < dBeg then
  begin
    Result := 3;
    exit;
  end;
  if sal.lic.RegOrg = NoLic then
  begin
    Result := 3;
    exit;
  end;
end;

procedure AddDataExchange(err: integer; Str: string);
var
  SL: TStringList;
begin
  if fmDataExchange = nil then
    fmDataExchange := TfmDataExchange.Create(Application);
  SL := TStringList.Create;
  try
    SL.Assign(fmDataExchange.SG_Query.cols[1]);
    // ������� ��������� �� ������
    case err of
      2: // ���� ��� � �������, �� ���������
        if SL.IndexOf(Str) = -1 then
        begin
          fmDataExchange.SG_Query.AddRow;
          fmDataExchange.SG_Query.Cells[1,
            fmDataExchange.SG_Query.RowCount -
            fmDataExchange.SG_Query.FixedRows -
            fmDataExchange.SG_Query.FixedFooters] := Str;
          fmDataExchange.SG_Query.Cells[2,
            fmDataExchange.SG_Query.RowCount -
            fmDataExchange.SG_Query.FixedRows -
            fmDataExchange.SG_Query.FixedFooters] := AddStatus;
        end;
    end;
  finally
    SL.free;
  end;
end;

function CheckProduction(product: TProduction): integer;
var
  b1: Boolean;
begin
  Result := -1;
  if not EANCheck(String(product.EAN13), b1) then
  begin
    Result := 1;
    exit;
  end;
  if pos(NoProd, String(product.Productname)) > 0 then
  begin
    Result := 2;
    exit;
  end;
end;

function CheckChislo(rash: Boolean; val: Extended): integer;
begin
  Result := -1;
  if rash then
  begin
    if val <= 0 then
      Result := 1;
  end
  else
  begin
    if val < 0 then
      Result := 1;
  end;
end;

function CheckProducer(prod: TProducer): integer;
var
  b1: Boolean;
begin
  Result := -1;
  if (not StrCheck(String(prod.pKPP))) or (Length(String(prod.pKPP)) <> 9)
    or (prod.pKPP = '000000000') then
  begin
    Result := 1;
    exit;
  end;
  If (not InnCheck(String(prod.pINN), b1)) and
    (not INN12Check(String(prod.pINN), b1)) then
  begin
    Result := 1;
    exit;
  end;
  if prod.pName = NoProducer then
  begin
    Result := 2;
    exit;
  end;
end;

function CountErrSG(fTF: TTypeForm; var SLErr: TStringList;
  SG: TADVStringGrid; ARow: integer): Boolean;
var
  i, col: integer;
begin
  Result := False;
  // ������ ��� ������ � ����� ���������� ������ ������
  for i := SLErr.Count - 1 downto 0 do
    if SLErr.Names[i] = IntToStr(ARow) then
    begin
      if Assigned(SLErr.Objects[i]) then
        SLErr.Objects[i] := nil;
      SLErr.Delete(i);
    end;

  case fTF of
    fVPok, fBrak:
      begin
        col := 3; // SelfKPP
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject('������ ' + IntToStr(ARow) + '=' + errSelfKPP,
            Pointer(LongInt(SG.Objects[col, ARow])));
          Result := True;
        end;

        col := 5; // ����
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errDate,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 6; // ���������
        if SG.AllCells[7, ARow] = NoProd then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProduction + SG.AllCells
              [7, ARow], Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 8; // ������
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errPrihod,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 11; // �������������
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProducer + SG.AllCells
              [12, ARow] + errKpp + SG.AllCells[13, ARow],
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;
      end;
    fPrihod, fVPost:
      begin
        col := 3; // SelfKPP
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errSelfKPP,
            Pointer(LongInt(SG.Objects[col, ARow])));
          Result := True;
        end;

        col := 4; // DeclNumber
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errDeclNumber,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 5; // ����
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errDate,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 6; // ���������
        if SG.AllCells[8, ARow] = NoSal then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errSal + SG.AllCells[6,
            ARow] + errKpp + SG.AllCells[7, ARow],
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True
        end
        else if LongInt(SG.Objects[col, ARow]) = 3 then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errSal + SG.AllCells[6,
            ARow] + errKpp + SG.AllCells[7, ARow],
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 9; // ���������
        if SG.AllCells[10, ARow] = NoProd then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProduction + SG.AllCells
              [7, ARow], Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 11; // ������
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errPrihod,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 14; // �������������
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProducer + SG.AllCells
              [15, ARow] + errKpp + SG.AllCells[16, ARow],
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;
      end;
    fRashod:
      begin
        col := 3; // SelfKPP
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errSelfKPP,
            Pointer(LongInt(SG.Objects[col, ARow])));
          Result := True;
        end;

        col := 4; // ����
        if SG.AllObjects[col, ARow] <> nil then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errDate,
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 6; // ���������
        if SG.AllCells[col, ARow] = NoProd then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProduction + SG.AllCells
              [4, ARow], Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;
        if CheckErrOst(SG.AllCells[3, ARow],SG.AllCells[col, ARow]) = 5 then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errOstProduction + SG.AllCells
              [4, ARow], Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 10; // �������������
        if (SG.AllObjects[col, ARow] <> nil) and
          (SG.AllCells[col, ARow] <> '') then
        begin
          SLErr.AddObject(IntToStr(ARow) + '=' + errProducer + SG.AllCells
              [11, ARow] + errKpp + SG.AllCells[14, ARow],
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;

        col := 8; // ������
        if (SG.AllObjects[col, ARow] <> nil) then
        begin
          SLErr.AddObject
            (IntToStr(ARow) + '=' + errRashod + '/��� ' + SG.AllCells[2,
            ARow] + ' ���� ��������� ' + SG.AllCells[3,
            ARow] + ' EAN13 ' + SG.AllCells[4, ARow] + '/',
            Pointer(LongInt(SG.AllObjects[col, ARow])));
          Result := True;
        end;
      end;
  end;
end;

procedure ShapkaSG(SG: TADVStringGrid; NameCol: array of integer);
var
  i, idx: integer;
begin
  SG.UnHideColumnsAll;
  SG.ColCount := High(NameCol) + 2;
  SG.ColumnHeaders.Clear;
  with SG do
  begin
    // ���������� �������� ��������
    ColumnHeaders.Add('�');
    for i := 0 to High(NameCol) do
    begin
      idx := NameCol[i];
      ColumnHeaders.Add(GrNam[idx]);
    end;
  end;
  SG.AutoFilterUpdate := True;
end;

function ShowDataOborotKpp(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
var
  i: integer;
begin
  Result := False;
  SG.FilterActive := False;
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;

  ShapkaSG(SG, NameCol);
  if SL.Count = 0 then
    exit
  else
    Result := True;
  SortSLCol(1, SL);
  SG.RowCount := SG.FixedRows + SG.FixedFooters + +SL.Count;
  EnabledAllRowSG(SG, True);
  SG.Font.Style := [];

  for i := 0 to SL.Count - 1 do
  begin
    // move := TMove(SL.Objects[i]);
    if TMove(SL.Objects[i]) <> nil then
    begin
      SG.AllCells[1, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).SelfKPP);
      SG.AllCells[2, i + SG.FixedRows] := IntToStr
        (TMove(SL.Objects[i]).forma);
      SG.AllCells[3, i + SG.FixedRows] := String(TMove(SL.Objects[i]).FNS);
      SG.AllCells[4, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.EAN13);
      SG.AllCells[5, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.ProductNameF);
      SG.AllCells[6, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.prod.pINN);
      SG.AllCells[7, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.prod.pKpp);

      SG.AllCells[8, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.prod.pName);
      SG.AllCells[9, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).sal.sINN);
      SG.AllCells[10, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).sal.sKpp);
      SG.AllCells[11, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).sal.sName);
      SG.AllFloats[12, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.ost0;
      SG.AllFloats[13, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.prih;
      SG.AllFloats[14, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.rash;
      SG.AllFloats[15, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.vozv_post;
      SG.AllFloats[16, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.vozv_pokup;
      SG.AllFloats[17, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.brak;
      SG.AllFloats[18, i + SG.FixedRows] := TMove(SL.Objects[i]) .dv.move_out;
      SG.AllFloats[19, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.move_in;
      if TMove(SL.Objects[i]).dv.ost1 < 0 then
        SG.RowColor[i + SG.FixedRows] := clCheck
      else
        SG.RowColor[i + SG.FixedRows] := clWhite;
      SG.AllFloats[20, i + SG.FixedRows] := TMove(SL.Objects[i]).dv.ost1;
      SG.AllFloats[21, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.ost0;
      SG.AllFloats[22, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.prih;
      SG.AllFloats[23, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.rash;
      SG.AllFloats[24, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.vozv_post;
      SG.AllFloats[25, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.vozv_pokup;
      SG.AllFloats[26, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.brak;
      SG.AllFloats[27, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.move_out;
      SG.AllFloats[28, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.move_in;
      SG.AllFloats[29, i + SG.FixedRows] := TMove(SL.Objects[i])
        .dv_dal.ost1;
    end;
  end;
  SG.Refresh;
  PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);
  SG.FilterActive := True;
end;

function ShowDataOstatkiKpp(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
var
  i: integer;
begin
  Result := False;
  SG.FilterActive := False;
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;
  ShapkaSG(SG, NameCol);
  if SL.Count = 0 then
    exit
  else
    Result := True;
  SortSLCol(1, SL);
  SG.RowCount := SG.FixedRows + SG.FixedFooters + +SL.Count;
  EnabledAllRowSG(SG, True);
  SG.Font.Style := [];

  for i := 0 to SL.Count - 1 do
  begin
    if TMove(SL.Objects[i]) <> nil then
    begin
      SG.Cells[1, i + SG.FixedRows] := String(TMove(SL.Objects[i]).SelfKPP);
      SG.Cells[2, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.EAN13);
      SG.Cells[3, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.ProductNameF);
      SG.Cells[4, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.prod.pINN);
      SG.Cells[5, i + SG.FixedRows] := String
        (TMove(SL.Objects[i]).product.prod.pName);
      SG.Cells[6, i + SG.FixedRows] := FloatToStrF
        (TMove(SL.Objects[i]).dv.ost1, ffFixed, maxint, ZnFld);
      SG.Cells[7, i + SG.FixedRows] := FloatToStrF
        (TMove(SL.Objects[i]).dv_dal.ost1, ffFixed, maxint, ZnFld);
    end;
  end;
  SG.Refresh;
  PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);
  SG.FilterActive := True;
end;

procedure SaveXLS(filename: string; Table: TADVStringGrid; ColFl: integer);
  overload;
var
  XL, Workbook, ArrayData, Cell1, Cell2, Range, Sheet: Variant;
  rez: HRESULT;
  i, j, BeginCol, BeginRow, ColCnt, RowCnt, col: integer;
  ClassID: TCLSID;
begin
  rez := CLSIDFromProgID(PWideChar(WideString('Excel.Application')), ClassID);
  // ���� CLSID OLE-�������
  if rez = S_OK then
    try
      // Screen.Cursor := crHourglass;
      BeginCol := 1; // ���������� ������ �������� ���� �������, � ������� ����� �������� ������
      BeginRow := 1;
      XL := CreateOleObject('Excel.Application'); // �������� Excel
      XL.DisplayAlerts := False;
      XL.Application.EnableEvents := False; // ��������� ������� Excel �� �������, ����� �������� ����� ����������
      Workbook := XL.WorkBooks.Add;
      ColCnt := Table.ColCount;
      RowCnt := Table.RowCount - 1;

      XL.WorkBooks[1].WorkSheets[1].Name := '������� ��';
      ArrayData := VarArrayCreate([1, RowCnt, 1, ColCnt], varVariant);
      // ������� ���������� ������, ������� �������� ��������� �������
      col := 1;

      for j := 0 to RowCnt - 1 do
      begin
        col := 1;
        for i := 1 to ColCnt + Table.NumHiddenColumns - 1 do
          if Table.IsHiddenColumn(i) = False then
          begin
            if j = 0 then
                ArrayData[j + 1, col] := Table.Cells[i, j]
            else
            begin
              if i < ColFl then
                ArrayData[j + 1, col] := Table.Cells[i, j]
              else
                if Table.Cells[i, j] <> '' then
                  ArrayData[j + 1, col] := StrToFloat(Table.Cells[i, j])
            end;
            inc(col);
          end;
      end;

      Sheet := XL.ActiveWorkBook.Sheets[1];
      Sheet.Range[Sheet.Cells[1, 1], Sheet.Cells[RowCnt, ColFl-2]]
        .NumberFormat := AnsiChar('@');
      Sheet.Range[Sheet.Cells[1, ColFl-1], Sheet.Cells[RowCnt, ColCnt]]
        .NumberFormat := '0' + {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator + '000';

      Cell1 := Workbook.WorkSheets[1].Cells[BeginRow, BeginCol]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[BeginCol + RowCnt - 1,
        BeginRow + ColCnt - 2]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      // �������, � ������� ����� �������� ������
      Range.value := ArrayData; // � ��� � ��� ����� ������. ������� ������� ����������� ����������

      Sheet.Range[Sheet.Cells[1, 1], Sheet.Cells[RowCnt, ColCnt - 1]]
        .Borders.Weight := 2;
      Sheet.Range[Sheet.Cells[1, 1], Sheet.Cells[RowCnt, ColCnt - 1]]
        .HorizontalAlignment := 3;
      Sheet.Range[Sheet.Cells[1, 3], Sheet.Cells[RowCnt, 3]]
        .HorizontalAlignment := 2;
      Sheet.Range[Sheet.Cells[1, 5], Sheet.Cells[RowCnt, 5]]
        .HorizontalAlignment := 2;
      Sheet.Range[Sheet.Cells[1, 1], Sheet.Cells[RowCnt, ColCnt - 1]]
        .VerticalAlignment := 1;
      XL.ActiveWorkBook.SaveAs(filename);
      XL.DisplayAlerts := True;
      XL.Visible := True; // ������ Excel �������
      XL := UnAssigned;
      Screen.Cursor := crDefault;
    except
      XL.DisplayAlerts := False;
      XL.ActiveWorkBook.Close;
      XL.Application.Quit;
      Sheet := UnAssigned;
      XL := UnAssigned;
    end;
end;

procedure DvizenieSalerSKpp(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  sKpp: string;
  DataKpp, DataSalerKpp: TStringList;
  sd: TSalerDvizenie;
begin
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      sd := Pointer(SL.Objects[i]);
      sd.free;
      sd := nil;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� SelfKpp � ������� ����������
  SortSLCol(2, SLData);

  DataKpp := TStringList.Create;
  DataSalerKpp := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    sKpp := String(decl.data.SelfKPP);
    RowStart := 0;
    sKpp := '';

    while (sKpp = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        sKpp := String(decl.data.SelfKPP);
        DataKpp.AddObject(sKpp, decl);
      end;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        if (decl.data.SelfKPP = ShortString(sKpp)) then
        begin
          FindSaler := True;
          DataKpp.AddObject(SLData.Strings[i], decl);
        end
        else
          FindSaler := False;

        if not FindSaler then
        begin
          DataSalerKpp.Clear;
          DvizenieSaler(DataSalerKpp, DataKpp, dsB, dsE);
          SL.AddStrings(DataSalerKpp);
          sKpp := String(decl.data.SelfKPP);
          DataKpp.Clear;
          DataKpp.AddObject(SLData.Strings[i], decl);
        end;
      end;
    end;
    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> sKpp then
      begin
        DataSalerKpp.Clear;
        DvizenieSaler(DataSalerKpp, DataKpp, dsB, dsE);
        SL.AddStrings(DataSalerKpp);
      end
      else
      begin
        if DataKpp.Count > 0 then
        begin
          DataSalerKpp.Clear;
          DvizenieSaler(DataSalerKpp, DataKpp, dsB, dsE);
          SL.AddStrings(DataSalerKpp);
        end;
      end;
    end
    else if DataKpp.Count > 0 then
    begin
      DataSalerKpp.Clear;
      DvizenieSaler(DataSalerKpp, DataKpp, dsB, dsE);
      SL.AddStrings(DataSalerKpp);
    end;
  finally
    DataSalerKpp.free;
    DataKpp.free;
  end;
end;

procedure DvizenieSaler(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  sINN: string;
  DataSaler, DataSalerKpp: TStringList;
begin
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      SL.Objects[i] := nil;
      TObject(SL.Objects[i]).free;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� EAN � ������� ����������
  SortSLCol(4, SLData);

  DataSaler := TStringList.Create;
  DataSalerKpp := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    sINN := String(decl.data.sal.sINN);
    RowStart := 0;
    sINN := '';

    while (sINN = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        sINN := String(decl.data.sal.sINN);
        DataSaler.AddObject(sINN, decl);
      end;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        if (decl.data.sal.sINN = ShortString(sINN)) then
        begin
          FindSaler := True;
          DataSaler.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end
        else
          FindSaler := False;

        if not FindSaler then
        begin
          DataSalerKpp.Clear;
          DvizenieSalerKpp(DataSalerKpp, DataSaler);
          SL.AddStrings(DataSalerKpp);
          sINN := String(decl.data.sal.sINN);
          DataSaler.Clear;
          DataSaler.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end;
      end;
    end;
    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> sINN then
      begin
        DataSalerKpp.Clear;
        DvizenieSalerKpp(DataSalerKpp, DataSaler);
        SL.AddStrings(DataSalerKpp);
      end
    end
    else if DataSaler.Count > 0 then
    begin
      DataSalerKpp.Clear;
      DvizenieSalerKpp(DataSalerKpp, DataSaler);
      SL.AddStrings(DataSalerKpp);
    end;
  finally
    DataSalerKpp.free;
    DataSaler.free;
  end;
end;

procedure DvizenieSalerKpp(var SL: TStringList; SLData: TStringList);
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  dSalerDv: TSalerDvizenie;
  sKpp: string;
  DataSaler: TStringList;
begin
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      SL.Objects[i] := nil;
      TObject(SL.Objects[i]).free;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� KPP � �������
  SortSLCol(5, SLData);
  DataSaler := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    sKpp := String(decl.data.sal.sKpp);
    RowStart := 0;
    sKpp := '';

    while (sKpp = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      sKpp := String(decl.data.sal.sKpp);
      DataSaler.AddObject(sKpp, decl);
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if (decl.data.sal.sKpp = ShortString(sKpp)) then
      begin
        FindSaler := True;
        DataSaler.AddObject(SLData.Strings[i], decl);
      end
      else
        FindSaler := False;

      if not FindSaler then
      begin
        DvizenieOneSaler(DataSaler, dSalerDv);
        if dSalerDv <> nil then
          SL.AddObject(sKpp, dSalerDv);
        sKpp := String(decl.data.sal.sKpp);
        DataSaler.Clear;
        DataSaler.AddObject(SLData.Strings[i], decl);
      end;
    end;

    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> sKpp then
      begin
        DvizenieOneSaler(DataSaler, dSalerDv);
        if dSalerDv <> nil then
          SL.AddObject(sKpp, dSalerDv);
      end;
    end
    else if DataSaler.Count > 0 then
    begin
      DvizenieOneSaler(DataSaler, dSalerDv);
      if dSalerDv <> nil then
        SL.AddObject(sKpp, dSalerDv);
    end;
  finally
    DataSaler.free;
  end;
end;

procedure DvizenieOneSaler(SL: TStringList; var dS: TSalerDvizenie);
var
  i: integer;
  decl: TDeclaration;
  V, ost, ost_dal: Extended;
  rd: TDateTime;
  dt: integer;
begin
  dS := TSalerDvizenie.Create;
  V := 0;
  ost := 0;
  ost_dal := 0;

  dS.prihod := 0;
  dS.vozvr_post := 0;
  dS.itogo := 0;
  dS.prihod_dal := 0;
  dS.vozvr_post_dal := 0;
  dS.itogo_dal := 0;
  dS.sINN := '';
  dS.sKpp := '';
  dS.sName := '';
  dS.SelfKPP := '';
  dS.FNS := '';
  for i := 0 to SL.Count - 1 do
  begin
    NullDecl(decl);
    decl := TDecl_obj(SL.Objects[i]).data;

    dS.sINN := String(decl.sal.sINN);
    dS.sKpp := String(decl.sal.sKpp);
    dS.sName := String(decl.sal.sName);
    dS.SelfKPP := String(decl.SelfKPP);
    dS.FNS := String(decl.product.FNSCode);

    dt := decl.TypeDecl;
    rd := decl.RepDate;
    V := 0.1 * decl.product.capacity * decl.Amount;
    case dt of
      1:
        begin
          dS.prihod := dS.prihod + decl.Amount; // ������
          dS.prihod_dal := dS.prihod_dal + V;
          dS.itogo := dS.itogo + decl.Amount;
          dS.itogo_dal := dS.itogo_dal + V;
        end;
      3:
        begin
          dS.vozvr_post := dS.vozvr_post + decl.Amount;
          // ������� ����������
          dS.vozvr_post_dal := dS.vozvr_post_dal + V;
          dS.itogo := dS.itogo - decl.Amount;
          dS.itogo_dal := dS.itogo_dal - V;
        end;
    end;
  end;
  if (dS.prihod = 0) and (dS.vozvr_post = 0) and (dS.itogo = 0) then
  begin
    dS.free;
    dS := nil;
  end;
end;

function ShowDataSaler(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
var
  i: integer;
  // move: TSalerDvizenie;
begin
  Result := False;
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;
  ShapkaSG(SG, NameCol);
  if SL.Count = 0 then
    exit
  else
    Result := True;
  SortSLCol(1, SL);
  SG.RowCount := SG.FixedRows + SG.FixedFooters + +SL.Count;
  EnabledAllRowSG(SG, True);
  SG.Font.Style := [];

  for i := 0 to SL.Count - 1 do
  begin
    // move := TSalerDvizenie(SL.Objects[i]);
    if TSalerDvizenie(SL.Objects[i]) <> nil then
    begin
      SG.Cells[1, i + SG.FixedRows] := TSalerDvizenie(SL.Objects[i])
        .SelfKPP;
      SG.Cells[2, i + SG.FixedRows] := TSalerDvizenie(SL.Objects[i]).sINN;
      SG.Cells[3, i + SG.FixedRows] := TSalerDvizenie(SL.Objects[i]).sKpp;
      SG.Cells[4, i + SG.FixedRows] := TSalerDvizenie(SL.Objects[i]).sName;
      SG.Cells[5, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).prihod, ffFixed, maxint,
        ZnFld);
      SG.Cells[6, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).vozvr_post, ffFixed, maxint,
        ZnFld);
      SG.Cells[7, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).itogo, ffFixed, maxint, ZnFld);
      SG.Cells[8, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).prihod_dal, ffFixed, maxint,
        ZnFld);
      SG.Cells[9, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).vozvr_post_dal, ffFixed,
        maxint, ZnFld);
      SG.Cells[10, i + SG.FixedRows] := FloatToStrF
        (TSalerDvizenie(SL.Objects[i]).itogo_dal, ffFixed, maxint, ZnFld);
    end;
  end;

  // SG.Refresh;
  PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);

end;

function DvizenieSalerFNSSKpp(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime): Boolean;
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  sKpp: string;
  DataKpp, DataSalerKpp: TStringList;
  sd: TSalerDvizenie;
begin
  Result := False;
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      sd := Pointer(SL.Objects[i]);
      sd.free;
      sd := nil;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� SelfKpp � ������� ����������
  SortSLCol(2, SLData);

  DataKpp := TStringList.Create;
  DataSalerKpp := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    sKpp := String(decl.data.SelfKPP);
    RowStart := 0;
    sKpp := '';

    while (sKpp = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        sKpp := String(decl.data.SelfKPP);
        DataKpp.AddObject(sKpp, decl);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        if (decl.data.SelfKPP = ShortString(sKpp)) then
        begin
          FindSaler := True;
          DataKpp.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end
        else
          FindSaler := False;

        if not FindSaler then
        begin
          DataSalerKpp.Clear;
          DvizenieSalerFNS(DataSalerKpp, DataKpp, dsB, dsE);
          SL.AddStrings(DataSalerKpp);
          sKpp := String(decl.data.SelfKPP);
          DataKpp.Clear;
          DataKpp.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end;
      end;
    end;
    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> sKpp then
      begin
        DataSalerKpp.Clear;
        DvizenieSalerFNS(DataSalerKpp, DataKpp, dsB, dsE);
        SL.AddStrings(DataSalerKpp);
      end;
    end
    else if DataKpp.Count > 0 then
    begin
      DataSalerKpp.Clear;
      DvizenieSalerFNS(DataSalerKpp, DataKpp, dsB, dsE);
      SL.AddStrings(DataSalerKpp);
    end;
  finally
    DataSalerKpp.free;
    DataKpp.free;
  end;
end;

procedure DvizenieSalerFNS(var SL: TStringList; SLData: TStringList;
  dsB, dsE: TDateTime);
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  sINN: string;
  DataSaler, DataSalerKpp: TStringList;
begin
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      SL.Objects[i] := nil;
      TObject(SL.Objects[i]).free;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� sInn � ������� ����������
  SortSLCol(4, SLData);

  DataSaler := TStringList.Create;
  DataSalerKpp := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    sINN := String(decl.data.sal.sINN);
    RowStart := 0;
    sINN := '';

    while (sINN = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        sINN := String(decl.data.sal.sINN);
        DataSaler.AddObject(sINN, decl);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if DBetween(Int(decl.data.RepDate), dsB, dsE) then
      begin
        if (decl.data.sal.sINN = ShortString(sINN)) then
        begin
          FindSaler := True;
          DataSaler.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end
        else
          FindSaler := False;

        if not FindSaler then
        begin
          DataSalerKpp.Clear;
          DvizenieSalerKppFNS(DataSalerKpp, DataSaler);
          SL.AddStrings(DataSalerKpp);
          sINN := String(decl.data.sal.sINN);
          DataSaler.Clear;
          DataSaler.AddObject(SLData.Strings[i], decl);
          // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
        end;
      end;
    end;
    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> sINN then
      begin
        DataSalerKpp.Clear;
        DvizenieSalerKppFNS(DataSalerKpp, DataSaler);
        SL.AddStrings(DataSalerKpp);
      end;
    end
    else if DataSaler.Count > 0 then
    begin
      DataSalerKpp.Clear;
      DvizenieSalerKppFNS(DataSalerKpp, DataSaler);
      SL.AddStrings(DataSalerKpp);
    end;
  finally
    DataSalerKpp.free;
    DataSaler.free;
  end;
end;

procedure DvizenieSalerKppFNS(var SL: TStringList; SLData: TStringList);
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  kpp: string;
  DataSaler, DataKpp: TStringList;
begin
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      SL.Objects[i] := nil;
      TObject(SL.Objects[i]).free;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� KPP � �������
  SortSLCol(5, SLData);
  DataSaler := TStringList.Create;
  DataKpp := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    kpp := String(decl.data.sal.sKpp);
    RowStart := 0;
    kpp := '';

    while (kpp = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      kpp := String(decl.data.sal.sKpp);
      DataSaler.AddObject(kpp, decl);
      // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if (decl.data.sal.sKpp = ShortString(kpp)) then
      begin
        FindSaler := True;
        DataSaler.AddObject(SLData.Strings[i], decl);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end
      else
        FindSaler := False;

      if not FindSaler then
      begin
        DataKpp.Clear;
        DvizenieSalerKppOneFNS(DataKpp, DataSaler);
        SL.AddStrings(DataKpp);
        kpp := String(decl.data.sal.sKpp);
        DataSaler.Clear;
        DataSaler.AddObject(SLData.Strings[i], TDecl_obj.Create);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end;
    end;

    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> kpp then
      begin
        DataKpp.Clear;
        DvizenieSalerKppOneFNS(DataKpp, DataSaler);
        SL.AddStrings(DataKpp);
      end;
    end
    else if DataSaler.Count > 0 then
    begin
      DataKpp.Clear;
      DvizenieSalerKppOneFNS(DataKpp, DataSaler);
      SL.AddStrings(DataKpp);
    end;
  finally
    DataKpp.free;
    DataSaler.free;
  end;
end;

function DvizenieSalerKppOneFNS(var SL: TStringList;
  SLData: TStringList): Boolean;
var
  i, RowStart: integer;
  FindSaler: Boolean;
  decl: TDecl_obj;
  dSalerDv: TSalerDvizenie;
  FNS: string;
  DataSaler: TStringList;
begin
  Result := False;
  FindSaler := False;
  for i := 0 to SL.Count - 1 do
    if Assigned(SL.Objects[i]) then
    begin
      SL.Objects[i] := nil;
      TObject(SL.Objects[i]).free;
    end;

  SL.Clear;
  if SLData.Count = 0 then
    exit;

  // ����������� ������ �� KPP � �������
  SortSLCol(6, SLData);
  DataSaler := TStringList.Create;
  try
    decl := Pointer(SLData.Objects[0]);
    FNS := String(decl.data.product.FNSCode);
    RowStart := 0;
    FNS := '';

    while (FNS = '') and (RowStart < SLData.Count) do
    begin
      decl := Pointer(SLData.Objects[RowStart]);
      FNS := String(decl.data.product.FNSCode);
      DataSaler.AddObject(FNS, decl);
      // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      inc(RowStart);
    end;

    for i := RowStart to SLData.Count - 1 do
    begin
      decl := Pointer(SLData.Objects[i]);
      if (decl.data.product.FNSCode = ShortString(FNS)) then
      begin
        FindSaler := True;
        DataSaler.AddObject(SLData.Strings[i], decl);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end
      else
        FindSaler := False;

      if not FindSaler then
      begin
        DvizenieOneSaler(DataSaler, dSalerDv);
        if dSalerDv <> nil then
          SL.AddObject(FNS, dSalerDv);
        FNS := String(decl.data.product.FNSCode);
        DataSaler.Clear;
        DataSaler.AddObject(SLData.Strings[i], decl);
        // TDecl_Obj(DataSaler.Objects[DataSaler.Count-1]).Data := decl;
      end;
    end;

    if SL.Count > 0 then
    begin
      if SL.Strings[SL.Count - 1] <> FNS then
      begin
        DvizenieOneSaler(DataSaler, dSalerDv);
        if dSalerDv <> nil then
          SL.AddObject(FNS, dSalerDv);
      end;
    end
    else if DataSaler.Count > 0 then
    begin
      DvizenieOneSaler(DataSaler, dSalerDv);
      if dSalerDv <> nil then
        SL.AddObject(FNS, dSalerDv);
    end;
  finally
    DataSaler.free;
  end;
end;

function ShowDataSalerFNS(SG: TADVStringGrid; SL: TStringList;
  NameCol, NoRes, CalcCol, HideCol, FilterCol: array of integer): Boolean;
var
  i: integer;
  move: TSalerDvizenie;
begin
  Result := False;
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;
  ShapkaSG(SG, NameCol);
  if SL.Count = 0 then
    exit
  else
    Result := True;
  SortSLCol(1, SL);
  SG.RowCount := SG.FixedRows + SG.FixedFooters + +SL.Count;
  EnabledAllRowSG(SG, True);
  SG.Font.Style := [];

  SG.RowCount := SG.FixedRows + SG.FixedFooters + SL.Count;
  for i := 0 to SL.Count - 1 do
  begin
    move := Pointer(SL.Objects[i]);
    if move <> nil then
    begin
      SG.Cells[1, i + SG.FixedRows] := move.SelfKPP;
      SG.Cells[2, i + SG.FixedRows] := move.sINN;
      SG.Cells[3, i + SG.FixedRows] := move.sKpp;
      SG.Cells[4, i + SG.FixedRows] := move.sName;
      SG.Cells[5, i + SG.FixedRows] := move.FNS;
      SG.Cells[6, i + SG.FixedRows] := FloatToStrF(move.prihod, ffFixed,
        maxint, ZnFld);
      SG.Cells[7, i + SG.FixedRows] := FloatToStrF(move.vozvr_post,
        ffFixed, maxint, ZnFld);
      SG.Cells[8, i + SG.FixedRows] := FloatToStrF(move.itogo, ffFixed,
        maxint, ZnFld);
      SG.Cells[9, i + SG.FixedRows] := FloatToStrF(move.prihod_dal,
        ffFixed, maxint, ZnFld);
      SG.Cells[10, i + SG.FixedRows] := FloatToStrF(move.vozvr_post_dal,
        ffFixed, maxint, ZnFld);
      SG.Cells[11, i + SG.FixedRows] := FloatToStrF(move.itogo_dal,
        ffFixed, maxint, ZnFld);
    end;
  end;

  SG.Refresh;
  PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);
  SG.Visible := True;
end;

procedure UpdPointSpr(cb: TComboBox);
begin
  Screen.Cursor := crHourGlass;
  ArhList(path_arh + '\Refs_BackUp_' + fOrg.inn + '_' + '*.rbk', cb.Items);
  Screen.Cursor := crDefault;
  with cb do
    if Items.Count > 0 then
      ItemIndex := Items.Count - 1;
end;

procedure UpdPointDecl(cb: TComboBox);
begin
  Screen.Cursor := crHourGlass;
  ArhList(path_arh + '\Decl_BackUp_' + fOrg.inn + '_' + '*.rbk', cb.Items);
  Screen.Cursor := crDefault;
  with cb do
    if Items.Count > 0 then
      ItemIndex := Items.Count - 1;
end;

function NewSvertkaDB(DBQ: TADOQuery): Boolean;
var
  SQL1, SQL2: TADOQuery;
  s: string;
  rez: Boolean;
  myYear, myMonth, myDay, myHour, myMin, mySec, myMilli: word;
  i, PK: integer;
  data: TDate;
begin
  rez := True;
  // le.Caption := '���������� ������ ������� ��������';
  DecodeDate(Now(), myYear, myMonth, myDay);
  DecodeTime(Now(), myHour, myMin, mySec, myMilli);
  data := StrToDateTime('31.12.' + IntToStr(myYear - 1));
  SQL1 := TADOQuery.Create(DBQ.Owner);
  SQL2 := TADOQuery.Create(DBQ.Owner);
  try
    try
      s := path_arh + '\' + NameProg + '_3112' + IntToStr(myYear - 1)
        + '_' + IntToStr(myHour) + IntToStr(myMin) + IntToStr(myDay)
        + IntToStr(myMonth) + IntToStr(myYear) + '.dat';
      CopyFile(PChar(db_decl), PChar(s), True);

      SQL1.Connection := DBQ.Connection;
      SQL1.ParamCheck := False;
      SQL1.sql.Text := 'select MAX(PK) from Declaration';
      SQL1.Open;
      PK := SQL1.Fields[0].AsInteger + 1;
      SQL1.sql.Clear;
      SQL1.Parameters.Clear;

      SQL1.Parameters.Clear;
      SQL1.sql.Clear;

      SQL2.Connection := DBQ.Connection;
      SQL2.ParamCheck := False;
      SQL2.Parameters.Clear;
      SQL2.sql.Clear;

      SQL1.sql.Text :=
        'SELECT SelfKPP,EAN13, max(Inn) as pinn, max(Kpp) as pKPP, '+
          'Sum(Val(Amount)*(Switch(DeclType=''1'',1, ' +
          'DeclType=''2'',-1, DeclType=''3'',-1, DeclType=''4'',1,'+
          'DeclType=''5'',-1, , DeclType=''6'',-1, DeclType=''7'',1))) AS Kol ' +
        'FROM Declaration ' + 'WHERE reportdate<=DateValue(''' + DateToStr
        (data) + ''') ' + 'GROUP BY SelfKPP, EAN13 ORDER BY SelfKPP, EAN13';
      SQL1.Open;

      SQL2.sql.Text :=
        'DELETE from declaration WHERE reportdate<=DateValue(''' + DateToStr
        (data) + ''')';
      SQL2.ExecSQL;

      // rmax := SQL1.RecordCount;

      SQL1.First;
      i := 0;
      // pb.Max := 100;
      while not(SQL1.Eof) do
      begin
        SQL2.sql.Text :=
          'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclNumber, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, TM, inn, kpp ) '
          + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclNumber, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :TM, :inn, :kpp)';

        if StrToFloat(SQL1.FieldByName('KOL').AsString) > 0 then
        begin
          // PosProgressBar(pb, i, rmax);

          SQL2.ParamCheck := False;
          SQL2.Parameters.Clear;

          SQL2.Parameters.AddParameter.Name := 'PK';
          SQL2.Parameters.ParamByName('PK').DataType := ftInteger;
          SQL2.Parameters.ParamByName('PK').value := PK;

          SQL2.Parameters.AddParameter.Name := 'DeclType';
          SQL2.Parameters.ParamByName('DeclType').DataType := ftString;
          SQL2.Parameters.ParamByName('DeclType').value := IntToStr(1);

          SQL2.Parameters.AddParameter.Name := 'ReportDate';
          SQL2.Parameters.ParamByName('ReportDate').DataType := ftDate;
          SQL2.Parameters.ParamByName('ReportDate').value := StrToDate
            (DateToStr(data));

          SQL2.Parameters.AddParameter.Name := 'SelfKPP';
          SQL2.Parameters.ParamByName('SelfKPP').DataType := ftString;
          SQL2.Parameters.ParamByName('SelfKPP').value := SQL1.FieldByName
            ('SelfKPP').AsString;

          SQL2.Parameters.AddParameter.Name := 'DeclNumber';
          SQL2.Parameters.ParamByName('DeclNumber').DataType := ftString;
          SQL2.Parameters.ParamByName('DeclNumber').value := '�������';

          SQL2.Parameters.AddParameter.Name := 'DeclDate';
          SQL2.Parameters.ParamByName('DeclDate').DataType := ftDate;
          SQL2.Parameters.ParamByName('DeclDate').value := StrToDate
            (DateToStr(data));

          SQL2.Parameters.AddParameter.Name := 'SalerINN';
          SQL2.Parameters.ParamByName('SalerINN').DataType := ftString;
          SQL2.Parameters.ParamByName('SalerINN').value := '1234567894';

          SQL2.Parameters.AddParameter.Name := 'SalerKPP';
          SQL2.Parameters.ParamByName('SalerKPP').DataType := ftString;
          SQL2.Parameters.ParamByName('SalerKPP').value := '123400001';

          SQL2.Parameters.AddParameter.Name := 'EAN13';
          SQL2.Parameters.ParamByName('EAN13').DataType := ftString;
          SQL2.Parameters.ParamByName('EAN13').value := SQL1.FieldByName
            ('EAN13').AsString;

          SQL2.Parameters.AddParameter.Name := 'Amount';
          SQL2.Parameters.ParamByName('Amount').DataType := ftString;
          SQL2.Parameters.ParamByName('Amount').value := SQL1.FieldByName
            ('Kol').AsString;

          SQL2.Parameters.AddParameter.Name := 'IsGood';
          SQL2.Parameters.ParamByName('IsGood').DataType := ftBoolean;
          SQL2.Parameters.ParamByName('IsGood').value := True;

          SQL2.Parameters.AddParameter.Name := 'TM';
          SQL2.Parameters.ParamByName('TM').DataType := ftString;
          SQL2.Parameters.ParamByName('TM').value := '';

          SQL2.Parameters.AddParameter.Name := 'INN';
          SQL2.Parameters.ParamByName('INN').DataType := ftString;
          SQL2.Parameters.ParamByName('INN').value := SQL1.FieldByName
            ('pinn').AsString;

          SQL2.Parameters.AddParameter.Name := 'kpp';
          SQL2.Parameters.ParamByName('kpp').DataType := ftString;
          SQL2.Parameters.ParamByName('kpp').value := SQL1.FieldByName
            ('pkpp').AsString;
          inc(PK);

          SQL2.ExecSQL;
          inc(i);
        end;
        SQL1.Next;
        inc(i);
      end;

    except
    end;
  finally
    Result := rez;
    SQL1.free;
    SQL2.free;
  end;
end;

function Copy2Ref(basefile: string): Boolean;
Var
  ms, ms1: TMemoryStream;
  CopyFN: string;
begin
  Result := False;
  try
    Application.ProcessMessages;
    if FileExists(basefile) then
      if CompactDatabase(basefile, psw_loc) then
        try
          Application.ProcessMessages;
          ms := TMemoryStream.Create;
          ms1 := TMemoryStream.Create;
          fmWork.dlgOpen_Import.Title := '�������� ���� ���� ������������:';
          fmWork.dlgOpen_Import.Filter :=
            '����� ���� (*.dat; *.ref; *.rbk)|*.dat;*.ref;*.rbk';
          if fmWork.dlgOpen_Import.Execute then
          begin
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.ref') or
              (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.dat') or
              (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.rbk')
              then
            begin
              if FileIsReadOnly(basefile) then
                FileSetReadOnly(basefile, False);
              DeleteFile(PWideChar(basefile));
              CopyFN := fmWork.dlgOpen_Import.filename;
            end;
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.rbk')
              then
            begin
              ms.LoadFromFile(CopyFN);
              if UnZipFilePas(ms, ms1, psw_loc) then
              begin
                ms1.SaveToFile(basefile);
                Result := True;
              end;
            end;
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.dat') or
              (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.ref') then
            begin
              CopyFile(PChar(CopyFN), PChar(basefile), True);
              Result := True;
            end;
            if Result then
              Show_Inf
                ('���� ���� ������ ���������� ������� ����������.' +
                  #13#10 + '��������� ���������� ���������.',
                fWarning);
          end
          else
            Show_Inf('������ �������� ����� ��������� ����� ����������!' +
                #13#10 + '���� �� ������!', fError);
        finally
          ms.free;
          ms1.free;
        end
      else
        Show_Inf('������ ���������� ����� ������� ���� ������!' + #13#10 +
            '������� �������������� ���� �� ����� �������.', fError);
  except
    Show_Inf('������ �������������� ���� ���������� �� ��������� �����!',
      fError);
  end;
end;

function Arh2DB(basefile, arhfile, pas: string; Inf: Boolean): Boolean;
Var
  ms, ms1: TMemoryStream;
begin
  Result := False;
  try
    if FileExists(arhfile) then
    begin
      Application.ProcessMessages;
      if FileExists(basefile) then
        if CompactDatabase(basefile, pas) then
        begin
          try
            Application.ProcessMessages;
            ms := TMemoryStream.Create;
            ms1 := TMemoryStream.Create;

            if FileExists(arhfile) then
            begin
              ms.LoadFromFile(arhfile);
              if UnZipFilePas(ms, ms1, psw_loc) then
              begin
                if FileIsReadOnly(basefile) then
                  FileSetReadOnly(basefile, False);
                DeleteFile(PWideChar(basefile));
                ms1.SaveToFile(basefile);
                if Inf then
                  Show_Inf
                    ('���� ���� ������ ������� ������������.' +
                      #13#10 + '��������� ���������� ���������.',
                    fWarning);
                Result := True;
              end
              else if Inf then
                Show_Inf(
                  '������ ���������� ����� ��������� ����� ����������!' +
                    #13#10 +
                    '���������� ������������ ���� ������ �� ��������� �����!'
                    , fError);

            end
            else if Inf then
              Show_Inf
                ('������ �������� ����� ��������� ����� ����������!'
                  + #13#10 + '���� �� ������!', fError);
          finally
            ms.free;
            ms1.free;
          end;
        end
        else if Inf then
          Show_Inf('������ ���������� ����� ������� ���� ������!' +
              #13#10 + '������� �������������� ���� �� ����� �������.',
            fError);
    end
    else if Inf then
      Show_Inf('������ �������� ����� ��������� ����� ����������!' +
          #13#10 + '���� �� ������!', fError);
  except
    Show_Inf('������ �������������� ���� ���������� �� ��������� �����!',
      fError);
  end;
end;

function Copy2DB(basefile: string): Boolean;
Var
  ms, ms1: TMemoryStream;
  CopyFN: string;
begin
  Result := False;
  try
    Application.ProcessMessages;
    if FileExists(basefile) then
      if CompactDatabase(basefile, fOrg.inn) then
        try
          Application.ProcessMessages;
          ms := TMemoryStream.Create;
          ms1 := TMemoryStream.Create;
          fmWork.dlgOpen_Import.Title := '�������� ���� ���� ������:';
          fmWork.dlgOpen_Import.Filter :=
            '����� ���� (*.dat; *.rbk)|*.dat;*.rbk';
          if fmWork.dlgOpen_Import.Execute then
          begin
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.rbk') or
              (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.dat') then
            begin
              if FileIsReadOnly(basefile) then
                FileSetReadOnly(basefile, False);
              DeleteFile(PWideChar(basefile));
              CopyFN := fmWork.dlgOpen_Import.filename;
            end;
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.rbk')
              then
            begin
              ms.LoadFromFile(CopyFN);
              if UnZipFilePas(ms, ms1, psw_loc) then
              begin
                ms1.SaveToFile(basefile);
                Result := True;
              end;
            end;
            if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.dat')
              then
            begin
              CopyFile(PChar(CopyFN), PChar(basefile), True);
              Result := True;
            end;
            if Result then
              Show_Inf
                ('���� ���� ������ ���������� ������� ����������.' +
                  #13#10 + '��������� ���������� ���������.', fInfo);
          end
          else
            Show_Inf('������ �������� ����� ��������� ����� ����������!' +
                #13#10 + '���� �� ������!', fError);
        finally
          ms.free;
          ms1.free;
        end
      else
        Show_Inf('������ ���������� ����� ������� ���� ������!' + #13#10 +
            '������� �������������� ���� �� ����� �������.', fError);
  except
    Show_Inf('������ �������������� ���� ���������� �� ��������� �����!',
      fError);
  end;
end;

function ID_Z(info: string): string;
var
  id: string;
begin
  if Get_ID(info, False, id) then
    ID_Z := id
  else
    ID_Z := '';
end;

function Get_ID(info: string; First: Boolean; var id: string): Boolean;
var
  OK: Boolean;
  point: integer;
begin
  id := '';
  OK := False;
  point := pos(ID_Dvd, info);
  if point > 1 then
  begin
    if First then
      id := Copy(info, 1, point - 1)
    else
      id := Copy(info, point + Length(ID_Dvd),
        Length(info) - point - Length(ID_Dvd) + 1);
    if not Empty(id) then
      OK := True;
  end;
  Get_ID := OK;
end;

function UpdateRefLocal(findFile: Boolean; OnLine: Boolean; sql: TADOQuery;
  SelfInn: string; var db_spr: string; pb: TProgressBar): Boolean;
var
  TitleName: string;
  DisplayName, TempPath: array [0 .. MAX_PATH] of char;
  path: String;
  lpItemID: PItemIDList;
  ms, ms1: TMemoryStream;
  BrowseInfo: TBrowseInfo;
begin
  Result := False;
  fmWork.Enabled := False;
  path := path_ref;

  if findFile then
  begin
    FillChar(BrowseInfo, SizeOf(TBrowseInfo), #0);
    BrowseInfo.hwndOwner := fmWork.Handle;
    BrowseInfo.pszDisplayName := @DisplayName;
    TitleName := '�������� ����� � ������ ���������� ������������';
    BrowseInfo.lpszTitle := PChar(TitleName);
    BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
    lpItemID := SHBrowseForFolder(BrowseInfo);
    if lpItemID <> nil then
    begin
      TempPath := 'C:\Program Files\AlcoRetail\Ref';
      SHGetPathFromIDList(lpItemID, TempPath);
      GlobalFreePtr(lpItemID);
      path := TempPath;
      path := path + '\';
      // path_spr:=Path+'\';
      if not FileExists(path + 'RefrRefs.bmz') then
      begin
        Show_Inf('� ��������� �������� ����������� ' + #13#10 +
            '���� ���������� ������������', fError);
        fmWork.Enabled := True;
        exit;
      end;
    end
    else
    begin
      fmWork.Enabled := True;
      exit;
    end;
  end;
  pb.Max := 100;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    try
      begin
        Application.ProcessMessages;
        MemoAdd('����������� ������ ���������� ������������ �� �����... ');
        ms1.LoadFromFile(path + 'RefrRefs.bmz');
        MemoAdd('����������� ���������� ���������� ����������... ');
        if UnZipFilePas(ms1, ms, 'afhhe[') then
        begin
          MemoEdit('���������');
          if not DirectoryExists(path_ref) then
            MkDir(path_ref);
          fmProgress.pb.Position := 90;
          fmProgress.lblcap.Caption :=
            '����������� ���������� ���������� ����������... ';
          fmProgress.pb.UpdateControlState;
          Application.ProcessMessages;
          ms.SaveToFile(path_ref + 'AR_Spr.dat');
          Result := UpdateNewBaseRef(path_ref + 'AR_Spr.dat', ms, ms1, pb);
          if db_spr = '' then
            db_spr := path_ref;
        end;
      end;
    finally
      ms1.free;
      ms.free;
      fmWork.Enabled := True;
    end;
  except
    Result := False;
    MemoEdit('������');
    Show_Inf('������ ���������� ������������', fError);
  end;
  fmWork.Enabled := True;
end;

function RenameSprav(path1, path2, inn: string): Boolean;
var
  s1, S2, s3: string;
begin
  Result := False;
  fmWork.SprADOCon.Connected := False;
  fmWork.con2ADOCon.Connected := False;
  fmWork.SprADOCon.Close;
  MemoAdd('����������� ���������� ���������� ����������... ');
  // s1:=path2 +'\AlcoRetail\'+ inn+'\DataBase\AlcoRetail.ref';
  s1 := path2;
  s3 := path1 + 'AR_Spr.dat';
  ChangeAccessDBPassword(s3, 'afhhe[', psw_loc);
  // s1:='AlcoRetail.dat';
  if FileExists(s1) then
  begin
    S2 := ChangeFileExt(s1, '.old');
    if FileExists(S2) then
      DeleteFile(PWideChar(S2));

    Result := RenameFile(s1, S2);
    if Result then
    begin
      Result := MoveFile(PWideChar(s3), PWideChar(s1));
      // Result:=DeleteFile(Path1+'RefrRefs.bmz');
      MemoEdit('���������');
      // fm_Decl.con2ADOCon.Create(fm_decl);
      fmWork.con2ADOCon.ConnectionString :=
        'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + db_spr +
        ';Persist Security Info=False;Jet OLEDB:Database Password=' +
        psw_loc;
    end
    else
      Show_Inf('���� �� ������������', fError);
  end;
end;

procedure UpdateDateRef;
var
  i: integer;
  decl: TDeclaration;
  f1: Boolean;
begin
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    decl := TDecl_obj(SLAllDecl.Objects[i]).data;
    if ((decl.TypeDecl = 1) or (decl.TypeDecl = 3)) and
      (decl.sal.sName = NoSal) then
      GetSaler(fmWork.que2_sql, decl.sal, f1, True, False);
    if decl.product.prod.pName = NoProducer then
    begin
      GetProduction(fmWork.que2_sql, decl.product, f1, True);
      if decl.product.prod.pName = NoProducer then
        GetProducer(fmWork.que2_sql, decl.product.prod, True, False);
    end;
    if decl.product.Productname = NoProd then
      GetProduction(fmWork.que2_sql, decl.product, f1, True);
    TDecl_obj(SLAllDecl.Objects[i]).data := decl;
  end;
end;

function RegOnline(DBQ1: TADOQuery;  List: TStringList): Boolean;
Var
  ms, ms1: TMemoryStream;
  s: String;
  er, rez: Boolean;
  FSize: integer;
  TCP: TIdTCPClient;
begin
  er := False;
  rez := False;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  TCP := TIdTCPClient.Create(nil);
  TCP.Host := BMHost;
  TCP.Port:=TCPPort;
  try
    try
      Application.ProcessMessages;
      TCP.Connect;
      s := TCP.IOHandler.ReadLn;


      TCP.IOHandler.WriteLn('LIC ' + fOrg.inn + ' ' + user + #0);

      s := TCP.IOHandler.ReadLn;
      if SameText(Copy(s, 1, 4), 'RFLC') then
      begin
        FSize := StrToInt(Copy(s, 5, Length(s) - 4));
        er := False;
        ms.Clear;
        ms1.Clear;
        TCP.IOHandler.ReadStream(ms, FSize);
        WriteIniReg(ms);
        ms.Position := 0;
        if ms.Size > 0 then
        begin
          if ReadKPP_MS(ms, List) then
          begin
            SetDataKpp(DBQ1);
          end
          else
            er := True;
        end
        else
        begin // if ms.Size>0
          TCP.IOHandler.WriteLn('OK');
          Show_Inf('����������� ��������� �� ���������:' + #13#10 +
              '�� ������� ����������� ������', fError);
          s := '';
        end;
      end
      else // if SameText(Copy(s, 1, 6), 'Refresh')
        if SameText(s, 'CNCL') then
      begin //
        CloseTCP(TCP);
        Show_Inf('����������� �������� �������� �����������: ' + #13#10 +
            '������� � ��� ' + fOrg.inn + ' ��� ������ ' + #13#10 +
            '�������� �� ������� �������', fError);
      end
      else if SameText(s, 'DSBL') then
      begin //
        CloseTCP(TCP);
        Show_Inf('����������� �������� �������� �����������: ' + #13#10 +
            '������ ����������� ������� � ��� ', fError);
      end
      else if SameText(s, 'NFND') then
      begin //
        CloseTCP(TCP);
        Show_Inf('����������� �������� �������� �����������: ' + #13#10 +
            '������ � ��� ' + fOrg.inn + ' �� ������ � �����������',
          fError);
      end
      else if SameText(s, 'FAULT') then
      begin
        CloseTCP(TCP);
        Show_Inf('����������� �������� �������� �����������:' + #13#10 +
            '���������� ������ �������', fError);
      end
      else
      begin
        CloseTCP(TCP);
        Show_Inf('����������� �������� �������� �����������:' + #13#10 +
            '����������� ������. ("' + s + '")', fError);
      end;
      // Upd1 := False;
      CloseTCP(TCP);
      if SameText(Copy(s, 1, 4), 'RFLC') and not er then
      begin
        rez := True;
        Show_Inf('����������� ��������� ��������� �������', fInfo);
      end;
    finally
      ms.free;
      ms1.free;
      TCP.Disconnect;
      TCP.Free;
    end;
  except
    Show_Inf('����������� ��������� �� ���������:' + #13#10 +
        '������ ����������� � �������', fError);
    TCP.Socket.Close;

  end;
  Result := rez;
end;

// ���������� ������ ������ XML
procedure WriteFullDataXML(SLEan: TStringList; DBQ: TADOQuery;
  var decl: TDeclaration);
var
  ean, s: string;
  // SL_FNS: TStringList;
  idx: integer;
  FindProducer, FindEan: Boolean;
  pn: string;
begin
  try
    // FillSLFNS(SL_FNS, FNSCode);
    idx := SLFNS.IndexOfObject
      (Pointer(StrToInt(String(decl.product.FNSCode))));
    if idx <> -1 then
    begin
      s := SLFNS.Strings[idx];
      s := '��������� ������ ' + IntToStr(LongInt(SLFNS.Objects[idx]))
        + '. ''' + s + '''';
      // pXML.ProdName := s;
      decl.product.Productname := ShortString(s);
      decl.product.ProductNameF := ShortString(FullNameProduction(s,
          decl.product.AlcVol, decl.product.capacity));
    end
    else
    begin
      s := '��������� ������ ' + String(decl.product.FNSCode) + '. ';
      decl.product.Productname := ShortString(s);
      decl.product.ProductNameF := ShortString(FullNameProduction(s,
          decl.product.AlcVol, decl.product.capacity));
    end;

    ean := GenNewEAN('27', String(decl.product.prod.pINN),
      String(decl.product.prod.pKPP), String(decl.product.FNSCode));
    decl.product.EAN13 := ShortString(ean);
    // pXML.ean := ean;
    if SLEan.IndexOf(ean) = -1 then
    begin
      SLEan.Add(ean);
      FindEan := FindEanXML(DBQ, ean);
      if not FindEan then
      begin
        pn := String(decl.product.prod.pName);
        // �������� ������������� ������ ������������� � ����
        FindProducer := GetProducer(fmWork.que2_sql, decl.product.prod,
          True, False);
        // ���� ������������� ���, �� ������
        if not FindProducer then
        begin
          decl.product.prod.pName := ShortString(pn);
          InsertProizvXML(DBQ, decl.product.prod);
        end;
        // ������� ������������� ��������� � ����
        InsertEanXML(DBQ, decl.product);
      end
      else
      begin
        pn := String(decl.product.prod.pName);
        // �������� ������������� ������ ������������� � ����
        FindProducer := GetProducer(fmWork.que2_sql, decl.product.prod,
          True, False);
        // ���� ������������� ���, �� ������
        if not FindProducer then
        begin
          decl.product.prod.pName := ShortString(pn);
          InsertProizvXML(DBQ, decl.product.prod);
        end;
      end;
    end
    else
    begin
      pn := String(decl.product.prod.pName);
      // �������� ������������� ������ ������������� � ����
      FindProducer := GetProducer(fmWork.que2_sql, decl.product.prod, True,
        False);
      // ���� ������������� ���, �� ������
      if not FindProducer then
      begin
        decl.product.prod.pName := ShortString(pn);
        InsertProizvXML(DBQ, decl.product.prod);
      end;
    end;
  finally
  end;
end;

function SaveRowNewDeclDB(DBQ: TADOQuery; var decl: TDeclaration; IsOst:Boolean): Boolean;
var
  sql: TADOQuery;
  PK: integer;
begin
  Result := False;
  sql := TADOQuery.Create(DBQ.Owner);
  PK := 0;
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;

      if not Empty(String(decl.SelfKPP)) and not Empty
        (String(decl.product.EAN13)) then
      begin
        sql.sql.Text := 'select MAX(PK) from Declaration';
        sql.Open;
        PK := sql.Fields[0].AsInteger + 1;
        sql.sql.Clear;
        sql.Parameters.Clear;
        sql.sql.Text :=
          'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclNumber, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, TM, inn, kpp ) '
          + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclNumber, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :TM, :inn, :kpp)';

        sql.Parameters.AddParameter.Name := 'PK';
        sql.Parameters.ParamByName('PK').DataType := ftInteger;
        sql.Parameters.ParamByName('PK').value := PK;
        decl.PK := ShortString(IntToStr(PK));

        sql.Parameters.AddParameter.Name := 'DeclType';
        sql.Parameters.ParamByName('DeclType').DataType := ftString;
        sql.Parameters.ParamByName('DeclType').value := IntToStr
          (decl.TypeDecl);

        sql.Parameters.AddParameter.Name := 'ReportDate';
        sql.Parameters.ParamByName('ReportDate').DataType := ftDate;
        sql.Parameters.ParamByName('ReportDate').value := decl.RepDate;

        sql.Parameters.AddParameter.Name := 'SelfKPP';
        sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
        sql.Parameters.ParamByName('SelfKPP').value := decl.SelfKPP;

        sql.Parameters.AddParameter.Name := 'DeclNumber';
        sql.Parameters.ParamByName('DeclNumber').DataType := ftString;
        sql.Parameters.ParamByName('DeclNumber').value := decl.DeclNum;

        sql.Parameters.AddParameter.Name := 'DeclDate';
        sql.Parameters.ParamByName('DeclDate').DataType := ftDate;
        sql.Parameters.ParamByName('DeclDate').value := decl.DeclDate;

        sql.Parameters.AddParameter.Name := 'SalerINN';
        sql.Parameters.ParamByName('SalerINN').DataType := ftString;

        sql.Parameters.AddParameter.Name := 'SalerKPP';
        sql.Parameters.ParamByName('SalerKPP').DataType := ftString;

        if (decl.TypeDecl = 1) or (decl.TypeDecl = 3) then
        begin
          sql.Parameters.ParamByName('SalerINN').value := decl.sal.sINN;
          sql.Parameters.ParamByName('SalerKPP').value := decl.sal.sKpp;
        end
        else
        begin
          sql.Parameters.ParamByName('SalerINN').value := 'NULL';
          sql.Parameters.ParamByName('SalerKPP').value := 'NULL';
        end;

        sql.Parameters.AddParameter.Name := 'EAN13';
        sql.Parameters.ParamByName('EAN13').DataType := ftString;
        sql.Parameters.ParamByName('EAN13').value := decl.product.EAN13;

        sql.Parameters.AddParameter.Name := 'Amount';
        sql.Parameters.ParamByName('Amount').DataType := ftString;
        if not IsOst then
          sql.Parameters.ParamByName('Amount').value := FloatToStr
            (decl.Ost.Value)
        else
          sql.Parameters.ParamByName('Amount').value := FloatToStr
            (decl.Amount);

        sql.Parameters.AddParameter.Name := 'IsGood';
        sql.Parameters.ParamByName('IsGood').DataType := ftBoolean;
        sql.Parameters.ParamByName('IsGood').value := True;

        sql.Parameters.AddParameter.Name := 'TM';
        sql.Parameters.ParamByName('TM').DataType := ftString;
        sql.Parameters.ParamByName('TM').value := decl.TM;

        sql.Parameters.AddParameter.Name := 'INN';
        sql.Parameters.ParamByName('INN').DataType := ftString;
        sql.Parameters.ParamByName('INN').value := decl.product.prod.pINN;

        sql.Parameters.AddParameter.Name := 'kpp';
        sql.Parameters.ParamByName('kpp').DataType := ftString;
        sql.Parameters.ParamByName('kpp').value := decl.product.prod.pKPP;

        sql.ExecSQL;
        Result := True;
      end;
      if IsOst then
        decl.PK := ShortString(IntToStr(PK));
    except
      Result := False;
    end;
  finally
    sql.free;
  end;
end;

procedure SaveRowDeclNewLocal(decl: TDeclaration);
var
  oDecl: TDecl_obj;
begin
  oDecl := TDecl_obj.Create;
  oDecl.data := decl;
  SLAllDecl.AddObject(String(decl.PK), oDecl);
  if decl.Ost.Value > 0 then
  begin
    SLDataOst.AddObject(String(decl.SelfKpp+decl.product.EAN13), TOst_obj.Create);
    TOst_obj(SLDataOst.Objects[SLDataOst.Count - 1]).data.Value := decl.Ost.Value;
  end;
end;

function UpdateRefOnLine(sql: TADOQuery; RegHost, SelfInn: string): Boolean;
var
  er: Boolean;
  ms, ms1: TMemoryStream;
  s: string;
  FSize: Int64;
  b: Boolean;
  myYear, myMonth, myDay: word;
  fOld, dir_path: string;
  TCP: TIdTCPClient;
begin
  fmWork.Enabled := False;
  Result := False;
  er := False;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  TCP := TIdTCPClient.Create(nil);
  try
    try
      fmSetting.Enabled := False;
      Application.ProcessMessages;
      TCP.Host := RegHost;
      TCP.Port:=TCPPort;
      try
        TCP.Connect;
      except
        Show_Inf(ErrorConnect, fError);
        fmWork.pbWork.Position := 0;
        er := True;
        exit;
      end;
      s := TCP.IOHandler.ReadLn;
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');
      TCP.IOHandler.WriteLn('REFS' + SelfInn);
      MemoAdd('������ : ������ ������� ���������� ������������');
      s := TCP.IOHandler.ReadLn;
      try
        if SameText(s, 'RFRF') then
        begin
          TCP.IOHandler.WriteLn('OK');
          fmProgress.pb.Position := 30;
          fmProgress.lblcap.Caption :=
            '����������� �������� ������������... ';
          fmProgress.pb.UpdateControlState;
          Application.ProcessMessages;
          MemoAdd('����������� �������� ������������... ');
          s:=TCP.IOHandler.ReadLn;
          FSize := StrToInt(s);
          er := False;
          ms.Clear;
          ms1.Clear;
          TCP.IOHandler.ReadStream(ms1, FSize);
          ms1.Position := 0;
          if not Er then
          begin
            MemoEdit('���������');
            ms1.Position := 0;
            fmProgress.pb.Position := 30;
            fmProgress.lblcap.Caption :=
              '����������� �������� ������������... ';
            fmProgress.pb.UpdateControlState;
            Application.ProcessMessages;
            MemoAdd('����������� ���������� ������������... ');
            dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)
              + '\' + NameProg + '\';
            if not DirectoryExists(dir_path) then
              MkDir(dir_path);
            dir_path := dir_path + 'Ref\';
            if not DirectoryExists(dir_path) then
              MkDir(dir_path);
            DecodeDate(Now(), myYear, myMonth, myDay);
            fOld := IntToStr(myYear) + IntToStr(myMonth) + IntToStr(myDay);
            fOld := path_ref + 'RefrRefs_' + fOld + '.bmz';
            if FileExists(fOld) then
              DeleteFile(PWideChar(fOld));

            if FileExists(path_ref) then
              Result := RenameFile(path_ref, fOld);
            ms1.SaveToFile(path_ref + 'RefrRefs.bmz');
            fmProgress.pb.Position := 60;
            fmProgress.pb.UpdateControlState;
            Application.ProcessMessages;
            b := UpdateRefLocal(False, False, fmWork.que2_sql, fOrg.inn,
              db_spr, fmProgress.pb);
            if b then
            begin
              RenameSprav(path_ref, db_spr, fOrg.inn);
              Upd_LocRef(fmWork.que1_sql, fmWork.que2_sql, fmProgress.pb,
                AboutBox.lHelp);
              // UpdateDataDelcRef(SLAllDecl);
              // ChangeKvart(dBeg, dEnd);
              // fmMove.ShowDataAll;
              Show_Inf('����������� ������� ���������', fInfo);
            end;

            if not TCP.Connected then
              er := True;
            if not er then
            begin
              TCP.IOHandler.WriteLn('OK');
              // MemoEdit('���������');
            end
            else
            begin // if not Er
              if TCP.Connected then
                TCP.IOHandler.WriteLn('ERRR');
              MemoEdit('������ ������������ �����');
              Show_Inf('����������� �������� �������� ����������:' + #13#10 +
                  '������ ������������ ����� ', fError);
              er := True;
            end;
          end
          else
          begin
            MemoAdd(
            '����������� �������� �������� ����������: ������ ����������� ������� � ��� ' + SelfInn);
          Show_Inf('����������� �������� �������� ����������:' + #13#10 +
              '������ ����������� ������� � ��� ', fError);
          end;
        end
        else if SameText(s, 'DSBL') then
        begin
          MemoAdd(
            '����������� �������� �������� ����������: ������ ����������� ������� � ��� ' + SelfInn);
          Show_Inf('����������� �������� �������� ����������:' + #13#10 +
              '������ ����������� ������� � ��� ', fError);
          er := True;
        end
        else if SameText(s, 'NFND') then
        begin
          MemoAdd
            ('����������� �������� �������� ����������: ������ � ��� ' + SelfInn + ' �� ������ � �����������');
          Show_Inf('����������� �������� �������� ����������:' + #13#10 +
              '������ � ��� ' + SelfInn + ' �� ������ � �����������',
            fError);
          er := True;
        end
        else if SameText(s, 'FAULT') then
        begin
          MemoAdd(
            '����������� �������� �������� ����������: ������������� �� �������');
          Show_Inf('����������� �������� �������� ����������:' + #13#10 +
              '������������� �� �������', fError);
          er := True;
        end
        else
        begin
          MemoAdd(
            '����������� �������� �������� ����������: ����������� ������. ("'
              + s + '")');
          Show_Inf('����������� �������� �������� ����������:' + #13#10 +
              '����������� ������. ("' + s + '")', fError);
          er := True;
        end;
      finally
        TCP.Socket.Close;
        TCP.Disconnect;
        TCP.Free;
      end;
    except
      er := True;
      TCP.Socket.Close;
      if TCP.Connected then
        TCP.Disconnect;
      Show_Inf('���������� ������������ �� ���������: ' + #13#10 +
          '������ ����������� � �������', fError);
      MemoAdd(
        '���������� ������������ �� ���������: ������ ����������� � �������'
        );
      Result := False;
    end;
  finally
    fmSetting.Enabled := True;
    MemoAdd('');
    ms.free;
    ms1.free;
    fmWork.pbWork.Position := 0;
    if not er then
      Result := True;
    fmWork.Enabled := True;
    fmProgress.Close;

  end;
end;

procedure MemoEdit(Str: String);
// Var
// k: integer;
begin
  try
    fmDataExchange.lst_Msg.Items[fmDataExchange.lst_Msg.Items.Count - 1] :=
      fmDataExchange.lst_Msg.Items[fmDataExchange.lst_Msg.Items.Count - 1]
      + (Str);
    // k := Length(fmDataExchange.lst_Msg.Items
    // [fmDataExchange.lst_Msg.Items.Count - 1] + (Str));
    Application.ProcessMessages;
  except
    fmDataExchange.lst_Msg.Items.Add
      (DateTimeToStr(Date + Time) + ': ' + Str);
  end;
end;

function UpdateNewBaseRef(path: string; ms, ms1: TMemoryStream;
  pb: TProgressBar): Boolean;
var
  qr1, qr2: TADOQuery;
  StrSQL: String;
begin
  Result := True;
  qr1 := TADOQuery.Create(Application); // ���� �������
  qr2 := TADOQuery.Create(fmWork); // ������������� ����
  qr1.Connection := fmWork.con2ADOCon;
  if fmWork.SprADOCon.Connected then
    fmWork.SprADOCon.Close;

  fmWork.SprADOCon.ConnectionString :=
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + path +
    ';Persist Security Info=False;Jet OLEDB:Database Password=' + 'afhhe[';
  qr2.Connection := fmWork.SprADOCon;

  try
    try
      StrSQL := 'Delete * from Production where selfmade = 0';
      qr1.sql.Text := StrSQL;
      qr1.ExecSQL;

      StrSQL := 'Delete * from Producer where selfmade = 0';
      qr1.sql.Text := StrSQL;
      qr1.ExecSQL;

      StrSQL := 'Delete * from Saler where selfmade = 0';
      qr1.sql.Text := StrSQL;
      qr1.ExecSQL;

      MemoAdd('����������� ��������� ����������� ���������... ');

      pb.Position := 0;
      fmProgress.lblcap.Caption :=
        '����������� ��������� ����������� ���������... ';
      fmProgress.pb.UpdateControlState;
      Application.ProcessMessages;
      Result := UpdateProduction(qr1, qr2, pb);
      MemoEdit('���������');
      MemoAdd('����������� ��������� ����������� ��������������... ');
      fmProgress.pb.Position := 0;
      fmProgress.lblcap.Caption :=
        '����������� ��������� ����������� ��������������... ';
      fmProgress.pb.UpdateControlState;
      Application.ProcessMessages;
      Result := UpdateProducer(qr1, qr2, pb);
      MemoEdit('���������');
      MemoAdd('����������� ��������� ����������� �����������... ');
      fmProgress.pb.Position := 0;
      fmProgress.lblcap.Caption :=
        '����������� ��������� ����������� �����������... ';
      fmProgress.pb.UpdateControlState;
      Application.ProcessMessages;
      Result := UpdateSaler(qr1, qr2, pb);
      MemoEdit('���������');
    except
    end;

  finally
    qr2.free;
    qr1.free;
  end;
end;

procedure WriteIniReg(ms: TMemoryStream);
var
  ms2: TMemoryStream;
  fn: string;
  tIniF: TIniFile;
  Reg: TRegistry;
begin
  ms2 := TMemoryStream.Create;
  Reg := TRegistry.Create;
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    if SetBinaryStream(ms, ms2) then // �������������� ������ � �������� ������
    begin
      ms2.Position := 0;
      tIniF.WriteBinaryStream(SectionName, 'Registration', ms2);
    end;
  finally
    ms2.free;
    tIniF.free;
    Reg.free;
  end;
end;

function FindEanXML(DBQ: TADOQuery; ean: string): Boolean;
var
  qr: TADOQuery;
  StrSQL: string;
  c: integer;
begin
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  try
    try
      StrSQL :=
        'SELECT COUNT(EAN13) as C FROM PRODUCTION WHERE EAN13 = ''' + ean + '''';
      qr.sql.Text := StrSQL;
      qr.Open;
      c := qr.FieldByName('C').AsInteger;
      if c > 0 then
        Result := True;
    except
      Result := False;
    end;
  finally
    // qr.Free;
    FreeAndNil(qr)
  end;
end;

function InsertProizvXML(DBQ: TADOQuery; var prod: TProducer): Boolean;
var
  qr: TADOQuery;
  StrSQL: string;
  PK, RegCode: integer;
begin
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  try
    try
      RegCode := StrToInt(Copy(String(prod.pINN), 0, 2));
      qr.sql.Text := 'select MAX(PK) from producer';
      qr.Open;
      PK := qr.Fields[0].AsInteger + 1;
      qr.sql.Clear;
      qr.Parameters.Clear;

      qr.ParamCheck := False;
      StrSQL :=
        'INSERT INTO producer(PK, FullName, INN, KPP, countrycode, RegionCode, Address, LocRef, SelfMade) ';
      StrSQL := StrSQL +
        'VALUES (:PK, :FullName, :INN, :KPP, :countrycode, :RegionCode, :Address, :LocRef, :SelfMade)';
      qr.sql.Text := StrSQL;

      qr.Parameters.AddParameter.Name := 'PK';
      qr.Parameters.ParamByName('PK').DataType := ftInteger;

      qr.Parameters.AddParameter.Name := 'FullName';
      qr.Parameters.ParamByName('FullName').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'INN';
      qr.Parameters.ParamByName('INN').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'KPP';
      qr.Parameters.ParamByName('KPP').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'countrycode';
      qr.Parameters.ParamByName('countrycode').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'RegionCode';
      qr.Parameters.ParamByName('RegionCode').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'Address';
      qr.Parameters.ParamByName('Address').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'LocRef';
      qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;

      qr.Parameters.AddParameter.Name := 'SelfMade';
      qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

      qr.Parameters.ParamByName('PK').value := PK;
      qr.Parameters.ParamByName('FullName').value := prod.pName;
      qr.Parameters.ParamByName('INN').value := prod.pINN;
      qr.Parameters.ParamByName('KPP').value := prod.pKPP;
      qr.Parameters.ParamByName('countrycode').value := 643;
      qr.Parameters.ParamByName('RegionCode').value := 59;
      qr.Parameters.ParamByName('Address').value := prod.pAddress;
      prod.pAddress := '������';
      qr.Parameters.ParamByName('LocRef').value := True;
      qr.Parameters.ParamByName('SelfMade').value := True;

      qr.sql.Text := StrSQL;
      qr.ExecSQL;
    except
      Result := False;
    end;
  finally
    qr.free;
  end;
end;

function InsertEanXML(DBQ: TADOQuery; prod: TProduction): Boolean;
var
  qr: TADOQuery;
  StrSQL: string;
begin
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  try
    try
      qr.ParamCheck := False;
      qr.sql.Clear;
      qr.Parameters.Clear;

      StrSQL :=
        'INSERT INTO Production(EAN13, FNSCode, ProductName, AlcVol, Capacity, LocRef, SelfMade, prodkod, prodcod) ';
      StrSQL := StrSQL +
        'VALUES (:EAN13, :FNSCode, :ProductName, :AlcVol, :Capacity, :LocRef, :SelfMade, :prodkod, :prodcod)';
      qr.sql.Text := StrSQL;

      qr.Parameters.AddParameter.Name := 'EAN13';
      qr.Parameters.ParamByName('EAN13').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'FNSCode';
      qr.Parameters.ParamByName('FNSCode').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'ProductName';
      qr.Parameters.ParamByName('ProductName').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'AlcVol';
      qr.Parameters.ParamByName('AlcVol').DataType := ftFloat;

      qr.Parameters.AddParameter.Name := 'Capacity';
      qr.Parameters.ParamByName('Capacity').DataType := ftFloat;

      qr.Parameters.AddParameter.Name := 'LocRef';
      qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;

      qr.Parameters.AddParameter.Name := 'SelfMade';
      qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

      qr.Parameters.AddParameter.Name := 'prodkod';
      qr.Parameters.ParamByName('prodkod').DataType := ftString;

      qr.Parameters.AddParameter.Name := 'prodcod';
      qr.Parameters.ParamByName('prodcod').DataType := ftString;

      qr.Parameters.ParamByName('EAN13').value := prod.EAN13;
      qr.Parameters.ParamByName('FNSCode').value := prod.FNSCode;
      qr.Parameters.ParamByName('ProductName').value := prod.Productname;
      qr.Parameters.ParamByName('AlcVol').value := prod.AlcVol;
      qr.Parameters.ParamByName('Capacity').value := prod.capacity;
      qr.Parameters.ParamByName('LocRef').value := True;
      qr.Parameters.ParamByName('SelfMade').value := True;
      qr.Parameters.ParamByName('prodkod').value := prod.prod.pINN;
      qr.Parameters.ParamByName('prodcod').value := prod.prod.pKPP;
      qr.sql.Text := StrSQL;
      qr.ExecSQL;
    except
      Result := False;
    end;
  finally
    qr.free;
  end;
end;

Function UpdateProduction(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
var
  StrSQL, ean: string;
  i, k: integer;
  SLPr: TStringList;
begin
  Result := True;
  SLPr := TStringList.Create;
  try
    try
      // qr1     ���� �������
      // qr2     ������������� ����
      StrSQL := 'select * from Production where SelfMade = True';
      qr1.sql.Text := StrSQL;
      qr1.Open;

      // pb.Max:=qr1.RecordCount*2;
      pb.Max := 100;
      k := 1;
      if not qr1.Eof then
      begin
        qr1.First;
        pb.Position := trunc(100 * (k) / qr1.RecordCount);
        pb.UpdateControlState;
        Application.ProcessMessages;
        while not qr1.Eof do
        begin
          // ������� ���������� �� ������ � ���������� �����������
          StrSQL :=
            'Select * from Production where EAN13 = ''' + qr1.FieldByName
            ('ean13').AsString + '''';
          qr2.sql.Text := StrSQL;
          qr2.Open;

          if not qr2.Eof then
          begin
            // ��������� ��������� �� ���� ������ � ������
            if (qr1.FieldByName('FNSCode').AsString <> qr2.FieldByName
                ('FNSCode').AsString) or
              (qr1.FieldByName('Capacity').AsString <> qr2.FieldByName
                ('Capacity').AsString) then
            begin
              ean := qr1.FieldByName('EAN13').AsString;
              if SLPr.IndexOf(ean) = -1 then
                SLPr.Add(ean);
            end;
          end
          else
          begin
            ean := qr1.FieldByName('EAN13').AsString;
            if SLPr.IndexOf(ean) = -1 then
              SLPr.Add(ean);
          end;

          qr1.Next;
          inc(k);
        end;
      end;
      pb.Position := 0;
      pb.UpdateControlState;
      Application.ProcessMessages;
      // ������� ��������� ��������� �� ����������� �����������
      for i := 0 to SLPr.Count - 1 do
      begin
        ean := SLPr.Strings[i];
        pb.Position := trunc(100 * (i) / SLPr.Count);
        pb.UpdateControlState;
        Application.ProcessMessages;
        StrSQL := 'delete from Production where EAN13 = ''' + ean + '''';
        qr2.sql.Text := StrSQL;
        qr2.ExecSQL;
      end;

      // �������� �������� �� � ��������� ����������� ������
      StrSQL := 'select * from Production where SelfMade = True';
      qr1.sql.Text := StrSQL;
      qr1.Open;
      i := 1;
      pb.Position := 60;
      pb.UpdateControlState;
      Application.ProcessMessages;
      // SLPr.Clear;
      // ���� �������� ������� �� � ����������
      k := 1;
      while not qr1.Eof do
      begin
        pb.Position := trunc(100 * (k) / qr1.RecordCount);
        pb.UpdateControlState;
        Application.ProcessMessages;
        ean := qr1.FieldByName('ean13').AsString;
        if SLPr.IndexOf(ean) <> -1 then
        begin
          StrSQL :=
            'INSERT INTO Production(EAN13, FNSCode, ProductName, AlcVol, Capacity, producercode, LocRef, SelfMade, prodkod, prodcod) ';
          StrSQL := StrSQL +
            'VALUES (:EAN13, :FNSCode, :ProductName, :AlcVol, :Capacity, :producercode, :LocRef, :SelfMade, :prodkod, :prodcod)';
          qr2.sql.Text := StrSQL;

          qr2.Parameters.ParamByName('EAN13').DataType := ftString;
          qr2.Parameters.ParamByName('FNSCode').DataType := ftString;
          qr2.Parameters.ParamByName('ProductName').DataType := ftString;
          qr2.Parameters.ParamByName('AlcVol').DataType := ftFloat;
          qr2.Parameters.ParamByName('Capacity').DataType := ftFloat;
          qr2.Parameters.ParamByName('producercode').DataType := ftInteger;
          qr2.Parameters.ParamByName('LocRef').DataType := ftBoolean;
          qr2.Parameters.ParamByName('SelfMade').DataType := ftBoolean;
          qr2.Parameters.ParamByName('prodkod').DataType := ftString;
          qr2.Parameters.ParamByName('prodcod').DataType := ftString;

          qr2.Parameters.ParamByName('EAN13').value := qr1.FieldByName
            ('ean13').AsString;
          qr2.Parameters.ParamByName('FNSCode').value := qr1.FieldByName
            ('FNSCode').AsString;
          qr2.Parameters.ParamByName('ProductName').value := qr1.FieldByName
            ('ProductName').AsString;
          qr2.Parameters.ParamByName('AlcVol').value := qr1.FieldByName
            ('AlcVol').AsFloat;
          qr2.Parameters.ParamByName('Capacity').value := qr1.FieldByName
            ('Capacity').AsFloat;
          qr2.Parameters.ParamByName('ProducerCode')
            .value := qr1.FieldByName('ProducerCode').AsInteger;
          qr2.Parameters.ParamByName('LocRef').value := qr1.FieldByName
            ('LocRef').AsBoolean;
          qr2.Parameters.ParamByName('SelfMade').value := qr1.FieldByName
            ('SelfMade').AsBoolean; ;
          qr2.Parameters.ParamByName('prodkod').value := qr1.FieldByName
            ('prodkod').AsString;
          qr2.Parameters.ParamByName('prodcod').value := qr1.FieldByName
            ('prodcod').AsString;
          qr2.sql.Text := StrSQL;
          qr2.ExecSQL;
        end;
        qr1.Next;
        inc(k);
      end;
      pb.Position := 100;
      pb.UpdateControlState;
      Application.ProcessMessages;
    except
      Result := False;
    end;
  finally
    SLPr.free
    // qr1.Free;
    // qr2.Free;
  end;
end;

Function UpdateProducer(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
var
  StrSQL: string;
  i, j, n: integer;
  mas_pr: array of StringMas;
  IsSal: Boolean;
begin
  // qr1     ���� �������
  // qr2     ������������� ����
  Result := True;
  try
    try
      IsSal := False;
      StrSQL := 'select * from Producer where SelfMade = True';
      qr1.sql.Text := StrSQL;
      qr1.Open;

      // fm_TCP.pb1.Max:=qr1.RecordCount*2;
      pb.Max := 100;
      j := 0;
      if not qr1.Eof then
      begin
        qr1.First;
        IsSal := True;
        SetLength(mas_pr, qr1.RecordCount, 2);
        while not qr1.Eof do
        begin
          mas_pr[j, 0] := qr1.FieldByName('Inn').AsString;
          mas_pr[j, 1] := qr1.FieldByName('Kpp').AsString;
          qr1.Next;
          inc(j);
        end;
      end
      else
        exit;
      pb.Position := 30;
      pb.UpdateControlState;
      Application.ProcessMessages;
      n := High(mas_pr);
      if n <> -1 then

        if IsSal then
          for j := 0 to n do
          begin
            // ������� ���������� �� ������ � ���������� �����������
            StrSQL := 'Select * from Producer where Inn = ''' + mas_pr[j,
              0] + ''' and ' + 'Kpp = ''' + mas_pr[j, 1] + '''';
            qr2.sql.Text := StrSQL;
            qr2.Open;
            i := 0;
            // ���� ���������� ������� �� ����������
            if qr2.RecordCount > 0 then
            begin
              pb.Position := i;
              StrSQL := 'Delete from Producer where Inn = ''' + mas_pr[j,
                0] + ''' and ' + 'Kpp = ''' + mas_pr[j, 1] + '''';
              qr1.sql.Text := StrSQL;
              qr1.ExecSQL;
              inc(i);
            end;
          end;

      pb.Position := 60;
      pb.UpdateControlState;
      Application.ProcessMessages;
      // �������� �������� �� � ��������� ����������� ������
      StrSQL := 'select * from Producer where SelfMade = True';
      qr1.Parameters.Clear;
      qr1.sql.Clear;
      qr1.sql.Text := StrSQL;
      qr1.Open;
      i := 1;
      // ���� �������� ������� �� � ����������
      while not qr1.Eof do
      begin
        pb.Position := i + qr1.RecordCount;

        StrSQL :=
          'INSERT INTO producer(PK, FullName, INN, KPP, countrycode, RegionCode, Address, LocRef, SelfMade) ';
        StrSQL := StrSQL +
          'VALUES (:PK, :FullName, :INN, :KPP, :countrycode, :RegionCode, :Address, :LocRef, :SelfMade)';
        qr2.sql.Text := StrSQL;
        qr2.Parameters.ParamByName('PK').DataType := ftInteger;
        qr2.Parameters.ParamByName('FullName').DataType := ftString;
        qr2.Parameters.ParamByName('INN').DataType := ftString;
        qr2.Parameters.ParamByName('KPP').DataType := ftString;
        qr2.Parameters.ParamByName('countrycode').DataType := ftString;
        qr2.Parameters.ParamByName('RegionCode').DataType := ftString;
        qr2.Parameters.ParamByName('Address').DataType := ftString;
        qr2.Parameters.ParamByName('LocRef').DataType := ftBoolean;
        qr2.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

        qr2.Parameters.ParamByName('PK').value := qr1.FieldByName('pk')
          .AsInteger;
        qr2.Parameters.ParamByName('FullName').value := qr1.FieldByName
          ('fullname').AsString;
        qr2.Parameters.ParamByName('INN').value := qr1.FieldByName('inn')
          .AsString;
        qr2.Parameters.ParamByName('KPP').value := qr1.FieldByName('kpp')
          .AsString;
        qr2.Parameters.ParamByName('countrycode').value := qr1.FieldByName
          ('CountryCode').AsString;
        qr2.Parameters.ParamByName('RegionCode').value := qr1.FieldByName
          ('regioncode').AsString;
        qr2.Parameters.ParamByName('Address').value := qr1.FieldByName
          ('Address').AsString;
        qr2.Parameters.ParamByName('LocRef').value := qr1.FieldByName
          ('LocRef').AsBoolean;
        qr2.Parameters.ParamByName('SelfMade').value := qr1.FieldByName
          ('SelfMade').AsBoolean;

        qr2.sql.Text := StrSQL;
        qr2.ExecSQL;
        qr1.Next;
        inc(i);
      end;
      pb.Position := 100;
      pb.UpdateControlState;
      Application.ProcessMessages;
    except
      on E: Exception do
      begin
        Show_Inf
          ('������ ���������� ����������� �����������' + E.ClassName +
            ': ' + E.Message, fError);
      end;
    end;
  finally

  end;
end;

Function UpdateSaler(qr1, qr2: TADOQuery; pb: TProgressBar): Boolean;
var
  StrSQL: string;
  i, j, n: integer;
  mas_sal: array of StringMas;
  IsSal: Boolean;
begin
  // qr1     ���� �������
  // qr2     ������������� ����
  Result := True;
  try
    try
      StrSQL := 'select * from Saler where SelfMade = True';
      qr1.sql.Text := StrSQL;
      qr1.Open;

      // fm_TCP.pb1.Max:=qr1.RecordCount*2;
      pb.Max := 100;
      IsSal := False;
      if not qr1.Eof then
      begin
        qr1.First;
        j := 0;
        IsSal := True;
        SetLength(mas_sal, qr1.RecordCount, 2);
        while not qr1.Eof do
        begin
          mas_sal[j, 0] := qr1.FieldByName('OrgInn').AsString;
          mas_sal[j, 1] := qr1.FieldByName('OrgKpp').AsString;
          qr1.Next;
          inc(j);
        end;
      end
      else
        exit;
      pb.Position := 30;
      pb.UpdateControlState;
      Application.ProcessMessages;
      if IsSal then
        n := High(mas_sal);
      if n <> -1 then
        for j := 0 to n do
        begin
          // ������� ���������� �� ������ � ���������� �����������
          StrSQL := 'Select * from Saler where OrgInn = ''' + mas_sal[j,
            0] + ''' and ' + 'OrgKpp = ''' + mas_sal[j, 1] + '''';
          qr2.sql.Text := StrSQL;
          qr2.Open;
          i := 0;
          // ���� ���������� ������� �� ����������
          if qr2.RecordCount > 0 then
          begin
            pb.Position := i;
            StrSQL := 'Delete from Saler where OrgInn = ''' + mas_sal[j,
              0] + ''' and ' + 'OrgKpp = ''' + mas_sal[j, 1] + '''';
            qr1.sql.Text := StrSQL;
            qr1.ExecSQL;
            inc(i);
          end;
        end;

      pb.Position := 60;
      pb.UpdateControlState;
      Application.ProcessMessages;
      // �������� �������� �� � ��������� ����������� ������
      StrSQL := 'select * from Saler where SelfMade = True';
      qr1.sql.Text := StrSQL;
      qr1.Open;
      i := 1;
      // ���� �������� ������� �� � ����������
      while not qr1.Eof do
      begin
        pb.Position := i + qr1.RecordCount;

        StrSQL :=
          'INSERT INTO Saler(PK,OrgName,OrgINN,OrgKPP,RegionCode,Address,LocRef,SelfMade) ';
        StrSQL := StrSQL +
          'VALUES (:PK,:OrgName,:OrgINN,:OrgKPP,:RegionCode,:Address,:LocRef,:SelfMade)';
        qr2.sql.Text := StrSQL;
        qr2.Parameters.ParamByName('PK').DataType := ftString;
        qr2.Parameters.ParamByName('OrgName').DataType := ftString;
        qr2.Parameters.ParamByName('OrgINN').DataType := ftString;
        qr2.Parameters.ParamByName('OrgKPP').DataType := ftString;
        qr2.Parameters.ParamByName('RegionCode').DataType := ftString;
        qr2.Parameters.ParamByName('Address').DataType := ftString;
        qr2.Parameters.ParamByName('LocRef').DataType := ftBoolean;
        qr2.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

        qr2.Parameters.ParamByName('PK').value := qr1.FieldByName('pk')
          .AsString;
        qr2.Parameters.ParamByName('OrgName').value := qr1.FieldByName
          ('orgname').AsString;
        qr2.Parameters.ParamByName('OrgINN').value := qr1.FieldByName
          ('OrgINN').AsString;
        qr2.Parameters.ParamByName('OrgKPP').value := qr1.FieldByName
          ('OrgKPP').AsString;
        qr2.Parameters.ParamByName('RegionCode').value := qr1.FieldByName
          ('regioncode').AsString;
        qr2.Parameters.ParamByName('Address').value := qr1.FieldByName
          ('address').AsString;
        qr2.Parameters.ParamByName('LocRef').value := qr1.FieldByName
          ('LocRef').AsBoolean;
        qr2.Parameters.ParamByName('SelfMade').value := qr1.FieldByName
          ('SelfMade').AsBoolean;

        qr2.sql.Text := StrSQL;
        qr2.ExecSQL;
        qr1.Next;
        inc(i);
      end;
      pb.Position := 100;
      pb.UpdateControlState;
      Application.ProcessMessages;
    except
      Result := False;
    end;
  finally
    // qr1.Free;
    // qr2.Free;
  end;
end;

procedure EnabledAllRowSG(SG: TADVStringGrid; val: Boolean);
var
  i: integer;
begin
  SG.Visible := False;
  for i := 1 to SG.RowCount - 2 do
    SG.RowEnabled[SG.RealRowIndex(i)] := val;
  SG.Visible := True;
end;

procedure WriteRowRashodSG(var SLErr: TStringList; SLOb: TStringList;
  decl: TDeclaration; SG: TADVStringGrid; ARow: integer);
var
  idx: integer;
  dviz: TMove;
  cRowErr: integer;
  isNewRow: Boolean;
begin
  cRowErr := 0;
  SG.RowColor[ARow] := clNone;
  SG.Objects[0, ARow] := Pointer(0);

  SG.Cells[13, ARow] := String(decl.PK);

  SG.Cells[1, ARow] := IntToStr(FormDecl(decl.product.FNSCode));

  SG.Cells[2, ARow] := IntToStr(decl.typeDecl);
  SG.Cells[2, ARow] := String(decl.product.FNSCode);

  SG.Cells[3, ARow] := String(decl.SelfKPP);
  idx := CheckSelfKpp(String(decl.SelfKPP));
  SetColorCells(SG, [3], ARow, idx);

  SG.Cells[4, ARow] := DateToStr(decl.DeclDate);
  SG.Cells[5, ARow] := TimeToStr(decl.DeclDate);

  idx := CheckDate(decl.DeclDate);
  SetColorCells(SG, [4], ARow, idx);


  SG.Cells[6, ARow] := String(decl.product.EAN13);
  SG.Cells[7, ARow] := String(decl.product.ProductNameF);
  idx := CheckProduction(decl.product);
  if idx <> -1 then
    AddDataExchange(idx, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [6, 7], ARow, idx);
  if SG.Objects[6,ARow] = nil then
    SetColorCells(SG, [6,7], ARow, CheckErrOst(String(decl.SelfKpp),
    String(decl.product.EAN13)));

  SG.Cells[10, ARow] := String(decl.product.prod.pName);
  SG.Cells[11, ARow] := String(decl.product.prod.pINN);
  SG.Cells[12, ARow] := String(decl.product.prod.pKPP);
  idx := CheckProducer(decl.product.prod);
  if idx <> -1 then
    AddDataExchange(idx, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [10, 11, 12], ARow, idx);

  SG.Cells[8, ARow] := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
  decl.Vol := 0.1 * decl.Amount * decl.product.capacity;
  SG.Cells[9, ARow] := FloatToStrF(decl.Vol, ffFixed, maxint, ZnFld);
  SetColorCells(SG, [8,9], ARow, CheckChislo(True, decl.Amount));

  if CountErrSG(fRashod, SLErr, SG, ARow) then
    inc(cRowErr);
  if isNewRow then
    dviz.free;
end;

procedure WriteRowBrakSG(var SLErr: TStringList; decl: TDeclaration;
  SG: TADVStringGrid; row: integer);
var
  idx, cRowErr: integer;
begin
  SG.RowColor[row] := clNone;
  SG.Objects[0, row] := Pointer(0);
  cRowErr := 0;
  decl.forma := FormDecl(String(decl.product.FNSCode));
  decl.vol := 0.1 * decl.Amount * decl.product.capacity;

  SG.Cells[14, row] := String(decl.PK);

  SG.Cells[1, row] := IntToStr(decl.forma);

  SG.Cells[2, row] := String(decl.product.FNSCode);

  SG.Cells[3, row] := String(decl.SelfKPP);
  idx := CheckSelfKpp(String(decl.SelfKPP));
  SetColorCells(SG, [3], row, idx);

  SG.Cells[4, row] := String(decl.DeclNum);

  SG.Cells[5, row] := DateToStr(decl.DeclDate);
  idx := CheckDate(decl.DeclDate);
  SetColorCells(SG, [5], row, idx);

  SG.Cells[6, row] := String(decl.product.EAN13);
  SG.Cells[7, row] := String(decl.product.ProductNameF);
  idx := CheckProduction(decl.product);
  if idx <> -1 then
    AddDataExchange(idx, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [6, 7], row, idx);

  SG.Cells[8, row] := FloatToStrF(decl.Amount, ffFixed, maxint, ZnFld);
  SetColorCells(SG, [8], row, CheckChislo(True, decl.Amount));

  SG.Cells[9, row] := FloatToStrF(decl.vol, ffFixed, maxint, ZnFld);
  SetColorCells(SG, [9], row, CheckChislo(True, decl.vol));

  SG.Cells[10, row] := String(decl.TM);

  SG.Cells[11, row] := String(decl.product.prod.pName);
  SG.Cells[12, row] := String(decl.product.prod.pINN);
  SG.Cells[13, row] := String(decl.product.prod.pKPP);
  idx := CheckProducer(decl.product.prod);
  if idx <> -1 then
    AddDataExchange(2, cQueryEAN + String(decl.product.EAN13));
  SetColorCells(SG, [11, 12, 13], row, idx);

  if CountErrSG(fVPok, SLErr, SG, row) then
    inc(cRowErr);
end;

function ShowDataMove(var SLErr: TStringList; SG: TADVStringGrid;
  SL, SLOb: TStringList; fTF: TTypeForm;
  NoRes, CalcCol, HideCol: array of integer): integer;
var
  i: integer;
  decl: TDeclaration;
begin
  Result := -1;

  SLErr.Clear;
  SortSLCol(1, SL);
  SG.FilterActive := False;
  SG.ClearAll;
  case fTF of
    fPrihod, fVPost:
      ShapkaSG(SG, NameColPrih);
    fRashod:
      ShapkaSG(SG, NameColRashNew);
    fVPok, fBrak:
      ShapkaSG(SG, NameColBrak);
  end;

  SG.RowCount := SG.FixedRows + SG.FixedFooters + SL.Count;
  EnabledAllRowSG(SG, True);
  for i := 0 to SL.Count - 1 do
  begin
    NullDecl(decl);
    decl := TDecl_obj(SL.Objects[i]).data;
    if TDecl_obj(SL.Objects[i]) <> nil then
      case fTF of
        fPrihod, fVPost:
          WriteRowPrihodSG(SLErr, decl, SG, i + SG.FixedRows);
        fRashod:
          WriteRowRashodSG(SLErrRash, SLOb, decl, SG, i + SG.FixedRows);
        fVPok, fBrak:
          WriteRowBrakSG(SLErr, decl, SG, i + SG.FixedRows);
      end;
    if fmProgress <> nil then
      fmProgress.pb.StepIt;
  end;
  SG.Refresh;
  PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);
  SG.FilterActive := True;
  Result := CountNamesSL(SLErr);

end;

procedure FormQuery(PageIndex: integer; er: TEditRecord;
  decl: TDeclaration);
var
  wid, i, idx: integer;
begin
  fmQuery.le1_NameOrg.Text := '';
  fmQuery.le1_Inn.Text := '';
  fmQuery.le1_KPP.Text := '';
  fmQuery.le1_adress.Text := '';
//  fmQuery.le1_Lic.Text := '';
//  fmQuery.le1_num.Text := '';
//  fmQuery.le1_org.Text := '';
  try
    fmQuery.nb.PageIndex := PageIndex;
    case er of
      fCopy:
        begin
          fmQuery.le0_NameProd.Text := String(decl.product.Productname);

          fmQuery.le0_AlcVol.Text := FloatToStr(decl.product.AlcVol);
          idx := IndexFNS(FNSCode, String(decl.product.FNSCode), False);
          if idx <> -1 then
            fmQuery.cb0_Wap.ItemIndex := idx;

          fmQuery.le0_EAN.Text := GenNewEAN('29',
            String(decl.product.prod.pINN),
            String(decl.product.prod.pKPP), String(decl.product.FNSCode));

          fmQuery.cbFullName.ItemIndex := -1;
          fmQuery.le0_Capacity.Text := '';
          fmQuery.le0_Producer.Text := String(decl.product.prod.pName);
          fmQuery.le0_INN.Text := String(decl.product.prod.pINN);
          fmQuery.cbKPP.Text := String(decl.product.prod.pKPP);
          fmQuery.le0_Adress.Text := String(decl.product.prod.pAddress);
        end;
      fAdd:
        begin
          fmQuery.le0_NameProd.Text := '';
          fmQuery.le0_EAN.Text := '';
          fmQuery.le0_AlcVol.Text := '';
          fmQuery.le0_Capacity.Text := '';
          fmQuery.le0_Producer.Text := '';
          fmQuery.le0_INN.Text := '';
          fmQuery.cbKPP.Text := '';
          fmQuery.le0_Adress.Text := '';
        end;
      fEdit:
        begin
          fmQuery.le1_NameOrg.Text := String(decl.sal.sName);
          fmQuery.le1_Inn.Text := String(decl.sal.sINN);
          fmQuery.le1_KPP.Text := String(decl.sal.sKpp);
          fmQuery.le1_adress.Text := String(decl.sal.sAddress);
          if decl.sal.lic.ser = '���' then
            fmQuery.cb_beer.Checked := True
          else
            fmQuery.cb_beer.Checked := False;
          fmQuery.le1_Lic.Text := String(decl.sal.lic.ser);
          fmQuery.le1_num.Text := String(decl.sal.lic.num);
          fmQuery.dtp1_beg.Date := decl.sal.lic.dBeg;
          fmQuery.dtp1_end.Date := decl.sal.lic.dEnd;
          fmQuery.le1_org.Text := String(decl.sal.lic.RegOrg);
        end;
    end;

    fmDataExchange.SG_Query.RowCount :=
      fmDataExchange.SG_Query.FixedRows + 1;
    fmDataExchange.SG_Query.ColCount :=
      fmDataExchange.SG_Query.FixedCols + 2;
    fmDataExchange.SG_Query.Cells[fmDataExchange.SG_Query.FixedCols,
      0] := '���������';
    fmDataExchange.SG_Query.Cells[fmDataExchange.SG_Query.FixedCols + 1,
      0] := '������';
    if fmDataExchange.SG_Query.FixedCols > 0 then
    begin
      wid := fmDataExchange.SG_Query.Width;
      for i := 0 to fmDataExchange.SG_Query.FixedCols - 1 do
        wid := wid - fmDataExchange.SG_Query.ColWidths[i];
      wid := ROUND(wid / 2);
      for i :=
        fmDataExchange.SG_Query.FixedCols to fmDataExchange.SG_Query
        .ColCount - 1 do
        fmDataExchange.SG_Query.ColWidths[i] := wid;
      for i :=
        fmDataExchange.SG_Query.FixedRows to fmDataExchange.SG_Query
        .RowCount - 1 do
        fmDataExchange.SG_Query.RowHeights[i] := rowHeght;
    end;
    fmQuery.ShowModal;
  except
  end;
end;

procedure ShowRefSaler(SG: TADVStringGrid; DBQ: TADOQuery);
var
  i: integer;
  qr: TADOQuery;
  StrSQL: string;
  df: TDateTime;
begin
  qr := TADOQuery.Create(DBQ.Owner);
  SG.ClearAll;
  ShapkaSG(SG, NameColSalRef);
  try
    try

      qr.Connection := DBQ.Connection;
      qr.ParamCheck := False;
      StrSQL :=
        'SELECT Saler.pk as Saler_PK, Saler.OrgName, Saler.OrgINN, Saler.OrgKPP, Saler.RegionCode, '
        +
        'Saler_license.ser, Saler_license.num, Saler_license.Date_start, '
        +
        'Saler_license.Date_finish, Saler.LocRef, Saler.SelfMade, ' +
        'Saler.Address, Saler_license.regOrg ' +
        'FROM Saler LEFT JOIN Saler_license ON (Saler.OrgKPP = Saler_license.saler_kpp) ' + 'AND (Saler.OrgINN = Saler_license.saler_pk) ' + 'WHERE (((Saler.LocRef)=True)) OR (((Saler.SelfMade)=True)) ' + 'ORDER BY Saler.OrgINN, Saler.OrgKPP';
      qr.sql.Text := StrSQL;
      qr.Open;
      if qr.RecordCount > 0 then
      begin
        SG.RowCount := SG.FixedRows + SG.FixedFooters + qr.RecordCount;
        qr.First;
        i := 0;
        while not qr.Eof do
        begin
          if qr.FieldByName('SelfMade').AsBoolean = True then
            SG.AddCheckBox(1, i + SG.FixedRows, True, False);
          SG.Cells[2, i + SG.FixedRows] := qr.FieldByName('OrgName')
            .AsString;
          SG.Cells[3, i + SG.FixedRows] := qr.FieldByName('OrgINN')
            .AsString;
          SG.Cells[4, i + SG.FixedRows] := qr.FieldByName('OrgKPP')
            .AsString;
          SG.Cells[5, i + SG.FixedRows] := qr.FieldByName('Address')
            .AsString;
          SG.Cells[6, i + SG.FixedRows] := qr.FieldByName('ser').AsString;
          SG.Cells[7, i + SG.FixedRows] := qr.FieldByName('num').AsString;
          SG.Cells[8, i + SG.FixedRows] := qr.FieldByName('Date_start')
            .AsString;
          SG.Cells[9, i + SG.FixedRows] := qr.FieldByName('Date_finish')
            .AsString;
          df := qr.FieldByName('Date_finish').AsDateTime;
          if df < dBeg then
            SG.RowColor[i + SG.FixedRows] := clLic
          else
            SG.RowColor[i + SG.FixedRows] := clNone;
          SG.Cells[10, i + SG.FixedRows] := qr.FieldByName('regOrg')
            .AsString;
          SG.Cells[11, i + SG.FixedRows] := qr.FieldByName('Saler_PK')
            .AsString;
          qr.Next;
          inc(i);
        end;
      end;
    except

    end;
  finally
    qr.free
  end;

  SG.AutoNumberOffset := 0;
  SG.AutoNumberStart := 0;

  SG.AutoNumberCol(0);

  SG.ColumnSize.Rows := arNormal;
  SG.NoDefaultDraw := True;
  SG.AutoSize := True;

  SG.CalcFooter(0);
  SG.ColWidths[2] := Length(SG.ColumnHeaders.Strings[2]) * 10;
end;

procedure ShowRefProduction(SG: TADVStringGrid; DBQ: TADOQuery);
var
  i: integer;
  qr: TADOQuery;
  StrSQL: string;
begin
  qr := TADOQuery.Create(DBQ.Owner);
  SG.ClearAll;
  ShapkaSG(SG, NameColProdRef);
  try
    try

      qr.Connection := DBQ.Connection;
      qr.ParamCheck := False;
      StrSQL := 'SELECT Production.EAN13, Production.FNSCode, ' +
        'Production.Productname, Production.AlcVol, ' +
        'Production.Capacity, Production.ProducerCode, ' +
        'Production.SelfMade, Production.LocRef, ' +
        'Production.prodkod, ' +
        'Production.prodcod, Producer.FullName ' +
        'FROM Production LEFT JOIN Producer ON ' +
        '(Production.prodcod = Producer.KPP) AND ' +
        '(Production.prodkod = Producer.INN) ' +
        'WHERE (((Production.SelfMade)=True)) OR ' +
        '(((Production.LocRef)=True))';
      qr.sql.Text := StrSQL;
      qr.Open;
      if qr.RecordCount > 0 then
      begin
        SG.RowCount := SG.FixedRows + SG.FixedFooters + qr.RecordCount;
        qr.First;
        i := 0;
        while not qr.Eof do
        begin
          if qr.FieldByName('SelfMade').AsBoolean = True then
            SG.AddCheckBox(1, i + SG.FixedRows, True, False);
          SG.Cells[2, i + SG.FixedRows] := qr.FieldByName('EAN13').AsString;
          SG.Cells[3, i + SG.FixedRows] := qr.FieldByName('FNSCode')
            .AsString;
          SG.Cells[4, i + SG.FixedRows] := qr.FieldByName('Productname')
            .AsString;
          SG.Cells[5, i + SG.FixedRows] := qr.FieldByName('AlcVol')
            .AsString;
          SG.Cells[6, i + SG.FixedRows] := qr.FieldByName('Capacity')
            .AsString;
          SG.Cells[7, i + SG.FixedRows] := qr.FieldByName('FullName')
            .AsString;
          SG.Cells[8, i + SG.FixedRows] := qr.FieldByName('prodkod')
            .AsString;
          SG.Cells[9, i + SG.FixedRows] := qr.FieldByName('prodcod')
            .AsString;
          qr.Next;
          inc(i);
        end;
      end;

    except

    end;
  finally
    qr.free;
  end;

  SG.AutoNumberOffset := 0;
  SG.AutoNumberStart := 0;

  SG.AutoNumberCol(0);

  SG.ColumnSize.Rows := arNormal;
  SG.NoDefaultDraw := True;
  SG.AutoSize := True;

  SG.CalcFooter(0);
  SG.ColWidths[4] := Length(SG.ColumnHeaders.Strings[4]) * 10;
  SG.ColWidths[7] := Length(SG.ColumnHeaders.Strings[7]) * 10;
end;

procedure DeleteProduction(EAN13: string; DBQ: TADOQuery; SLRow: TSTrings);
var
  qr: TADOQuery;
  butsel: integer;
  s, StrSQL: string;
  i: integer;
  decl: TDeclaration;
begin
  s := '����� ������� ������:' + #10#13 + '������������ ��������� = ''' +
    SLRow.Strings[4] + #10#13 + ''' EAN13 = ' + EAN13 + #10#13;
  butsel := Show_Inf(s + #10#13 + '����������?', fConfirm);
  if butsel <> mrOk then
    exit;
  StrSQL := 'DELETE from Production WHERE EAN13=''' + EAN13 + '''';
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  qr.ParamCheck := False;
  try
    qr.sql.Text := StrSQL;
    qr.ExecSQL;
    ShowRefProduction(fmRef.SG_Product, fmWork.que2_sql);
    // ������ ���������� �� ����������
    for i := 0 to SLAllDecl.Count - 1 do
    begin
      decl := TDecl_obj(SLAllDecl.Objects[i]).data;
      if decl.product.EAN13 = ShortString(EAN13) then
      begin
        decl.product.Productname := NoProd;
        decl.product.ProductNameF := NoProd;
        decl.product.prod.pName := NoProducer;
        decl.product.FNSCode := '';
        decl.product.AlcVol := 0;
        decl.product.capacity := 0;
        decl.product.prod.pId := '';
        decl.product.prod.pAddress := '';
        TDecl_obj(SLAllDecl.Objects[i]).data := decl;
      end;
    end;
    LocRefProduction(fmWork.que1_sql, fmWork.que2_sql, fmWork.pbWork);
    if fmMove <> nil then
      fmMove.ShowDataAll;
  finally
    qr.free;
  end;
end;

procedure DeleteSaler(PK: string; DBQ: TADOQuery; SLRow: TSTrings);
var
  qr: TADOQuery;
  butsel: integer;
  s, StrSQL: string;
  i: integer;
  decl: TDeclaration;
begin
  s := '����� ������� ������:' + #10#13 + '������������ ���������� = ''' +
    SLRow.Strings[2] + '''' + #10#13 + '��� = ' + SLRow.Strings[3]
    + '    ��� = ' + SLRow.Strings[4] + '.' + #10#13;
  butsel := Show_Inf(s + #10#13 + '����������?', fConfirm);
  if butsel <> mrOk then
    exit;
  StrSQL := 'Delete from Saler where pk = ''' + PK + '''';
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  qr.ParamCheck := False;
  try
    qr.sql.Text := StrSQL;
    qr.ExecSQL;
    ShowRefSaler(fmRef.SG_Saler, fmWork.que2_sql);
    // ������ ���������� �� ����������
    for i := 0 to SLAllDecl.Count - 1 do
    begin
      decl := TDecl_obj(SLAllDecl.Objects[i]).data;
      if decl.sal.sId = ShortString(PK) then
      begin
        decl.sal.sId := '';
        decl.sal.sName := NoSal;
        decl.sal.sAddress := '';
        decl.sal.lic.lId := '';
        decl.sal.lic.ser := '';
        decl.sal.lic.num := '';
        decl.sal.lic.dBeg := 0;
        decl.sal.lic.dEnd := 0;
        decl.sal.lic.RegOrg := '';
        TDecl_obj(SLAllDecl.Objects[i]).data := decl;
      end;
    end;
    LocRefSaler(fmWork.que1_sql, fmWork.que2_sql, fmWork.pbWork);
    if fmMove <> nil then
      fmMove.ShowDataAll;
  finally
    qr.free;
  end;
end;

procedure FillComboFNS(cb: TComboBox; Mas: array of string; old: Boolean);
var
  i, Count, p, kod: integer;
  s: string;
begin
  Count := High(Mas);
  cb.Clear;
  for i := 0 to Count do
  begin
    s := Mas[i];
    p := pos('01.07.2012', s);
    if not old then
    begin
      if p = 0 then
      begin
        kod := StrToInt(Copy(s, 0, 3));
        cb.Items.AddObject(s, Pointer(kod));
      end;
    end
    else
    begin
      kod := StrToInt(Copy(s, 0, 3));
      cb.Items.AddObject(s, Pointer(kod));
    end;
  end;
end;

procedure SetColorRow(ARow: integer; SG: TADVStringGrid);
var
  i, idx, r: integer;
  IsChange: Boolean;
  PK: string;
begin
  if High(DataGrid) = 0 then
    exit;

  IsChange := False;
  SortSLCol(1, CurSL);
  PK := SG.AllCells[SG.AllColCount - 1, ARow];
  try
    if not NoValue(PK) then
    begin
      idx := IndexPKmasSG(PK); // ������ ������ � �������
      if (idx <> -1) and (ARow <> -1) then
        for i := SG.FixedCols to SG.AllColCount - 1 do
        begin
          if DataGrid[i - SG.FixedCols, idx] <> SG.AllCells[i, ARow] then
          begin
            IsChange := True;
            Break;
          end;
        end;

      if IsChange then
      begin
        r := SG.DisplRowIndex(ARow);
        SG.RowColor[r] := clInsert;
        SG.RowFontColor[r] := clNormal;
        SG.AllObjects[0, ARow] := Pointer(1);
      end
      else
      begin
        r := SG.DisplRowIndex(ARow);
        SG.RowColor[r] := clNone;
        SG.RowFontColor[r] := clNormal;
        SG.AllObjects[0, ARow] := nil;
      end;
    end
    else
    begin
      SG.RowColor[SG.DisplRowIndex(ARow)] := clInsert;
    end;
  finally
  end;
end;

function IndexPKmasSG(PK: string): integer;
var
  i, col: integer;
begin
  Result := -1;
  col := CurSG.AllColCount - CurSG.FixedCols - 1;
  for i := 0 to High(DataGrid[0]) do
    if DataGrid[col, i] = PK then
    begin
      Result := i;
      exit;
    end;
end;

function Correct_ChangeKPP(decl: TDeclaration; NewKPP: string;
  DBQ: TADOQuery): Boolean;
var
  qr, qr1: TADOQuery;
  StrSQL, PK, NewPk: string;
  Amount, NewAmount: Extended;
  idx: integer;
  rd: TDateTime;
begin
  Result := False;
  SortSLCol(1, SLAllDecl);
  qr := TADOQuery.Create(DBQ.Owner);
  qr1 := TADOQuery.Create(DBQ.Owner);
  try
    try
      qr.Connection := DBQ.Connection;
      qr.ParamCheck := False;
      qr1.Connection := DBQ.Connection;
      qr1.ParamCheck := False;
      // ������
      StrSQL := 'UPDATE Declaration SET SelfKpp = ''' + NewKPP + '''' +
        ' WHERE PK = ' + String(decl.PK);
      qr1.sql.Text := StrSQL;
      qr1.ExecSQL;
      idx := SLAllDecl.IndexOf(String(decl.PK));
      if idx <> -1 then
        TDecl_obj(SLAllDecl.Objects[idx]).data.SelfKPP := ShortString
          (NewKPP);

      // ������
      StrSQL :=
        'select pk,AMOUNT,ReportDate from Declaration ' +
        'where ean13 = ''' + String(decl.product.EAN13)
        + ''' and ' + 'selfKpp = ''' + NewKPP + ''' and ' +
        'ReportDate >= DateValue(''' + DateToStr(decl.RepDate)
        + ''') and ' + 'DeclType = ''' + '2' + '''';
      qr1.sql.Text := StrSQL;
      qr1.Open;
      if qr1.RecordCount > 0 then
      // while not qr1.Eof do
      begin
        rd := qr1.FieldByName('ReportDate').AsDateTime;
        Amount := qr1.FieldByName('AMOUNT').AsFloat;
        NewPk := qr1.FieldByName('PK').AsString;

        StrSQL :=
          'select pk,AMOUNT from Declaration '
          + 'where ean13 = ''' + String(decl.product.EAN13)
          + ''' and ' + 'selfKpp = ''' + String(decl.SelfKPP)
          + ''' and ' + 'ReportDate = DateValue(''' + DateToStr(rd)
          + ''') and ' + 'DeclType = ''' + '2' + '''';
        qr.sql.Text := StrSQL;
        qr.Open;
        if qr.RecordCount > 0 then
          PK := qr.FieldByName('PK').AsString
        else
          PK := '';

        NewAmount := Amount + qr.FieldByName('AMOUNT').AsFloat;

        // ��������� ������ �������
        StrSQL := 'UPDATE Declaration SET Amount = ''' + FloatToStrF
          (NewAmount, ffFixed, maxint, 5) + '''' + ' WHERE PK = ' + NewPk;
        qr.sql.Text := StrSQL;
        qr.ExecSQL;
        idx := SLAllDecl.IndexOf(String(decl.PK));
        if idx <> -1 then
          TDecl_obj(SLAllDecl.Objects[idx]).data.Amount := NewAmount;

        if PK <> '' then
        begin
          StrSQL := 'delete from Declaration where PK = ''' + PK + '''';
          idx := SLAllDecl.IndexOf(PK);
          if idx <> -1 then
            SLAllDecl.Delete(idx);
        end;
        qr1.Next;
      end;
      Result := True;
    except

    end;
  finally
    qr.free;
    qr1.free;
  end;
end;

procedure RowDecl(fTF: TTypeForm; var decl: TDeclaration; ARow: integer);
var
  PK: string;
begin
  PK := CurSG.AllCells[CurSG.AllColCount - 1, ARow];
  decl.PK := ShortString(PK);
  case fTF of
    fVPok, fBrak:
      begin
        case fTypeF of
          fVPok:
            decl.TypeDecl := 4;
          fBrak:
            decl.TypeDecl := 5;
        end;
        decl.RepDate := dEnd; ;
        decl.SelfKPP := ShortString(CurSG.AllCells[3, ARow]);
        decl.DeclNum := ShortString(CurSG.AllCells[4, ARow]);
        decl.DeclDate := StrToDate(CurSG.AllCells[5, ARow]);
        decl.product.EAN13 := ShortString(CurSG.AllCells[6, ARow]);
        decl.product.prod.pINN := ShortString(CurSG.AllCells[12, ARow]);
        decl.product.prod.pKPP := ShortString(CurSG.AllCells[13, ARow]);
        GetProductionMas(decl.product);
        decl.forma := FormDecl(String(decl.product.FNSCode));
        decl.Amount := CurSG.AllFloats[8, ARow];
        decl.TM := ShortString(CurSG.AllCells[10, ARow]);
        decl.vol := 0.1 * decl.Amount * decl.product.capacity;
      end;
    fPrihod, fVPost:
      begin
        case fTypeF of
          fPrihod:
            decl.TypeDecl := 1;
          fVPost:
            decl.TypeDecl := 3;
        end;
        decl.RepDate := dEnd; ;
        decl.SelfKPP := ShortString(CurSG.AllCells[3, ARow]);
        decl.DeclNum := ShortString(CurSG.AllCells[4, ARow]);
        decl.DeclDate := StrToDate(CurSG.AllCells[5, ARow]);
        decl.sal.sINN := ShortString(CurSG.AllCells[6, ARow]);
        decl.sal.sKpp := ShortString(CurSG.AllCells[7, ARow]);
        decl.product.EAN13 := ShortString(CurSG.AllCells[9, ARow]);
        decl.product.prod.pINN := ShortString(CurSG.AllCells[15, ARow]);
        decl.product.prod.pKPP := ShortString(CurSG.AllCells[16, ARow]);
        GetSalerMas(decl.sal);
        GetProductionMas(decl.product);
        decl.forma := FormDecl(String(decl.product.FNSCode));
        decl.Amount := CurSG.AllFloats[11, ARow];
        decl.TM := ShortString(CurSG.AllCells[13, ARow]);
      end;
    fRashod:
      begin
        decl.TypeDecl := 2;
        decl.SelfKPP := ShortString(CurSG.AllCells[3, ARow]);
        decl.DeclDate := StrToDateTime(CurSG.AllCells[4, ARow] + ' ' + CurSG.AllCells[5, ARow]);
        decl.product.EAN13 := ShortString(CurSG.AllCells[6, ARow]);
        GetProductionMas(decl.product);
        decl.forma := FormDecl(String(decl.product.FNSCode));
        decl.Amount := CurSG.AllFloats[8, ARow];
//        idx := GetIdxOstDate(String(decl.SelfKPP+decl.product.EAN13),dEnd, SLDataOst);
//        if idx = -1 then
//          decl.Ost.PK := ''
//        else
//          decl.Ost.PK :=TOst_obj(SLDataOst.Objects[idx]).data.PK;
//        decl.Ost.DateOst := decl.DeclDate;
//        decl.vol := 0.1 * decl.Amount * decl.product.capacity;
        decl.sal.sINN := NullData;
        decl.sal.sKpp := NullData;
        decl.DeclNum := NullData;
        decl.product.prod.pINN := ShortString(CurSG.AllCells[11, ARow]);
        decl.product.prod.pKPP := ShortString(CurSG.AllCells[12, ARow]);
        decl.RepDate := dEnd;
        decl.TM := '';
      end;
  end;
end;

procedure PastCell(txt: string; Table: TADVStringGrid); overload;
var
  r, c: integer;
begin
  if not Empty(txt) then
    with Table do
    begin
      r := row;
      c := col;
      if NBetween(r, FixedRows, RowCount - 1) and NBetween(c, 1,
        ColCount - 1) then
      begin
        Cells[c, r] := txt;
      end;
    end;
end;

function DeleteRowSG(ARow: integer; SG: TADVStringGrid;
  SLData, SLErr: TStringList; var cEdit, cNew: integer): string;
var
  PK, PKo, nameSL, valSL: string;
  i, idx: integer;
begin
  PK := SG.Cells[SG.AllColCount - 1, SG.row];
  SortSLCol(1,SLAllDecl, CompNumAsc);
  idx:=SLAllDecl.IndexOf(pk);
  if idx <> -1 then
  begin
    PKo := String(TDecl_obj(SLAllDecl.Objects[idx]).data.Ost.PK);
    if not NoValue(PKo) then
    begin
      DelDecl.Add(PKo+'o');
    end;
  end;
  if not NoValue(PK) then
    begin
      DelDecl.Add(PK); // ��������� ���� � ������ ����� ��� ��������
    end
  else if cNew > 0 then
    dec(cNew);
  if SG.Objects[0, ARow] <> nil then
    dec(cEdit);

  // ������ � ������� ������ ������ ��� ������ ������
  for i := SLErr.Count - 1 downto 0 do
    if SLErr.Names[i] = IntToStr(ARow) then
    begin
      if Assigned(SLErr.Objects[i]) then
        SLErr.Objects[i] := nil;
      SLErr.Delete(i);
    end;
  // � ������� ������ ��� ������ ������� ������ ������ ������ �������� �� 1
  for i := SLErr.Count - 1 downto 0 do
    if StrToInt(SLErr.Names[i]) > ARow then
    begin
      nameSL := SLErr.Names[i];
      valSL := SLErr.ValueFromIndex[i];
      nameSL := IntToStr(StrToInt(nameSL) - 1);
      SLErr.Strings[i] := nameSL + '=' + valSL;
    end;

  if not NoValue(PK) then
  begin
    SG.Enabled := False;
    SG.RowEnabled[SG.RealRowIndex(ARow)] := False;
    SG.Enabled := True;
  end
  else
    SG.RemoveRows(SG.RealRowIndex(ARow), 1);

  if ARow < SG.RowCount - SG.FixedFooters - 1 then
    SG.row := ARow + 1
  else
    SG.row := ARow - 1;

  fmMove.ErrGrid(fTypeF, CountNamesSL(SLErr));
  Result := statusDel + IntToStr(DelDecl.Count);
end;

function DeleteAllRowSG(SG: TADVStringGrid; SLData, SLErr: TStringList;
  var cEdit, cNew: integer): string;
var
  PK,PKo, nameSL, valSL: string;
  i, j, c, idx: integer;
begin
  if fmProgress = nil then
    fmProgress := TfmProgress.Create(Application);
  fmProgress.Show;
  fmProgress.lblcap.Caption := '����������� �������� ���� �����';
  fmProgress.pb.Max := SG.RowCount - SG.FixedRows - SG.FixedFooters;
  fmProgress.pb.Step := 1;
  Application.ProcessMessages;


  c := SG.RowCount - SG.FixedRows - SG.FixedFooters;
  SortSLCol(1,SLAllDecl, CompNumAsc);

  for j := SG.FixedRows to c do
  begin
    fmProgress.pb.StepIt;
    fmProgress.pb.UpdateControlState;

    PK := SG.Cells[SG.AllColCount - SG.FixedCols, j];

    idx:=SLAllDecl.IndexOf(pk);
    if idx <> -1 then
    begin
      PKo := String(TDecl_obj(SLAllDecl.Objects[idx]).data.Ost.PK);
      if not NoValue(PKo) then
      begin
          DelDecl.Add(PKo+'o');
      end;
    end;

    if not NoValue(PK) then
      DelDecl.Add(PK) // ��������� ���� � ������ ����� ��� ��������
    else if cNew > 0 then
      dec(cNew);
    if SG.Objects[0, j] <> nil then
      dec(cEdit);

    // ������ � ������� ������ ������ ��� ������ ������
    for i := SLErr.Count - 1 downto 0 do
      if SLErr.Names[i] = IntToStr(j) then
      begin
        if Assigned(SLErr.Objects[i]) then
          SLErr.Objects[i] := nil;
        SLErr.Delete(i);
      end;
    // � ������� ������ ��� ������ ������� ������ ������ ������ �������� �� 1
    for i := SLErr.Count - 1 downto 0 do
      if StrToInt(SLErr.Names[i]) > j then
      begin
        nameSL := SLErr.Names[i];
        valSL := SLErr.ValueFromIndex[i];
        nameSL := IntToStr(StrToInt(nameSL) - 1);
        SLErr.Strings[i] := nameSL + '=' + valSL;
      end;
  end;
//  ClearSL(SLDataOst);
  // SG.RemoveRows(SG.FixedRows, c);
  EnabledAllRowSG(SG, False);

  SG.Refresh;

  fmMove.ErrGrid(fTypeF, CountNamesSL(SLErr));
  Result := statusDel + IntToStr(DelDecl.Count);

  fmProgress.pb.Position := 0;
  fmProgress.pb.UpdateControlState;
  Application.ProcessMessages;
  fmProgress.Close;
end;

procedure DupRow(row: integer; Table: TADVStringGrid);
var
  i: integer;
begin
  Table.InsertRows(row + 1, 1, True);
  Table.Objects[0, row + 1] := Pointer(1);
  Table.RowColor[row + 1] := clInsert;
  inc(fmMove.CountNew);
  for i := Table.FixedCols to Table.ColCount - 1 do
    Table.Cells[i, row + 1] := Table.Cells[i, row];
  SetColorRow(row + 1, CurSG);
end;

procedure FindFirstErr(SG: TADVStringGrid);
var
  i, j, row, col, obj: integer;
begin
  if SG.row = SG.FixedRows then
    row := SG.row
  else
    row := SG.row - 1;

  if SG.col = SG.FixedCols then
    col := SG.col
  else
    col := SG.col - 1;

  // ������ ������ � �������
  for i := row downto SG.FixedRows - SG.FixedFooters do
    for j := SG.FixedCols to SG.ColCount - 1 do
    begin
      obj := LongInt(SG.Objects[j, i]);
      if obj in [1, 2, 3, 5] then
      begin
        SG.col := j;
        SG.row := i;
        exit;
      end;
    end;
end;

procedure FindLastErr(SG: TADVStringGrid);
var
  i, j, row, col, obj: integer;
begin
  if SG.row = SG.RowCount - 1 then
    row := SG.row
  else
    row := SG.row + 1;

  if SG.col = SG.ColCount - 1 then
    col := SG.FixedCols
  else
    col := SG.col + 1;

  // ������ ������ � �������
  for i := row to SG.RowCount - SG.FixedFooters - 1 do
    for j := 1 to SG.ColCount - 1 do
    begin
      obj := LongInt(SG.Objects[j, i]);
      if obj in [1, 2, 3, 5] then
      begin
        SG.col := j;
        SG.row := i;
        exit;
      end;
    end;
end;

function SaveTable(fTF: TTypeForm; format: integer; fName: string;
  Table: TADVStringGrid; Progress: TProgressBar): Boolean;
begin
  Result := False;
  case format of
    1:
      Result := SaveTXT(fName, Table, Progress); // txt
    2:
      Result := SaveXLS(fName, Table, Progress); // xls
    3:
      Result := SaveDBF(fTF, fName, Table, Progress); // dbf
  end;
  Progress.Position:=0;
end;

function SaveTXT(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): Boolean;
var
  fl: TStringList;
  i, j: integer;
  s: string;
const
  tab = #9;
  eol = #13#10;
begin
  fl := TStringList.Create;
  try
    Progress.Position := Progress.Min;
    Progress.UpdateControlState;
    Application.ProcessMessages;

    fl.Sorted := False;
    fl.Clear;
    with Table do
      for i := 0 to RowCount - FixedRows - FixedFooters do
      begin
        s := '';
        for j := 2 to ColCount - 1 do
          s := s + tab + Cells[j, i];
        fl.Add(s);
        Progress.Position := trunc(100 * (i) / RowCount);
        Progress.UpdateControlState;
        Application.ProcessMessages;
      end;
    fl.SaveToFile(filename);

    SaveTXT := True;
    Progress.Position := Progress.Max;
    Progress.UpdateControlState;
    Application.ProcessMessages;
  finally
    fl.free;
  end;
end;

function SaveXLS(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): Boolean; overload;
var
  Excel: Variant;
  i, j: integer;
  rez: Boolean;
const
  beg_col = 1;
  beg_row = 1;
begin
  Progress.Position := Progress.Min;
  Progress.UpdateControlState;
  Application.ProcessMessages;
  rez := True;
  try
    Excel := CreateOleObject('Excel.Application');
    Excel.DisplayAlerts := False;
    Excel.WorkBooks.Add;
    with Table do
      for i := 0 to RowCount - FixedRows - FixedFooters do
      begin
        for j := 2 to ColCount - 1 do
          Excel.Cells[i + beg_row, j + beg_col - 1] := #39 + Cells[j, i];
        Progress.Position := trunc(100 * (i) / RowCount);
        Progress.UpdateControlState;
        Application.ProcessMessages;
      end;
    Excel.ActiveWorkBook.SaveAs(filename);
    Excel.ActiveWorkBook.Close;
  except
    rez := False;
  end;
  Excel.Application.Quit;
  SaveXLS := rez;
  Progress.Position := Progress.Max;
  Progress.UpdateControlState;
  Application.ProcessMessages;
  // Excel.Free;
end;

function SaveDBF(fTypeF: TTypeForm; filename: string;
  Table: TADVStringGrid; Progress: TProgressBar): Boolean;
var
  dbf: TdbfTable;
  i, j, col, rmax: integer;
  fn, s: string;
begin
  Result := False;
  // �������� DBF-����� � ������ ������ filename �� ������� Table
  // ������� ���������� True ���� �������� �������
  dbf := TdbfTable.Create(filename, dcpPoint, ltTrueFalse, dfYYYYMMDD);
  try
    if fTypeF = fPrihod then
      try

        // dbf.
        // �������� ����� DBF
        for i := 1 to High(array_dbf_prih) do
          case array_dbf_prih[i].Type_f of
            f_Ch:
              dbf.AddField(array_dbf_prih[i].Name_f, dftCharacter,
                array_dbf_prih[i].Size_f, 0);
            f_Num:
              dbf.AddField(array_dbf_prih[i].Name_f, dftNumeric,
                array_dbf_prih[i].Size_f, 0);
            f_Dat:
              dbf.AddField(array_dbf_prih[i].Name_f, dftDate,
                array_dbf_prih[i].Size_f, 0);
          end;
        dbf.CreateTable;
        rmax := Table.RowCount - Table.FixedRows - Table.FixedFooters;
        Progress.Max := 100;
        for i := Table.FixedRows to rmax do
        begin
          Progress.Position := trunc(100 * (i) / rmax);
          Progress.UpdateControlState;
          Application.ProcessMessages;
          dbf.Append;
          // ��������� �� ������� ���� ����� Dbf ���� �� ��� � �����
          for j := 1 to High(array_dbf_prih) do
          begin
            // ��������� ������� � �������
            fn := GrNam[array_dbf_prih[j].Number_f];
            // ����� ������
            col := GetColTable(Table, fn);
            if col <> -1 then
            begin
              s := Table.Cells[col, i];
              case array_dbf_prih[j].Type_f of
                f_Ch:
                  dbf.WriteCharacter(i, j, s);
                f_Num:
                  dbf.WriteNumeric(i, j, StrToFloat(s));
                f_Dat:
                  dbf.WriteDate(i, j, StrToDateTime(s));
              end;
            end;
          end;
        end;
      except
        Result := False;
      end;
  finally
    dbf.free;
    Result := True;
  end;
end;

function GetColTable(Table: TADVStringGrid; fName: string): integer;
var
  i: integer;
begin
  Result := -1;
  for i := Table.FixedCols to Table.ColCount - 1 do
    if Table.Cells[i, 0] = fName then
    begin
      Result := i;
      exit;
    end;
end;

function GetTypeFile(rFile: string): TTypeFile;
begin
  Result := fNone;
  if pos('DBF', rFile) > 0 then
    Result := fDBF;
  if pos('XML', rFile) > 0 then
    Result := fXML;
  if pos('XLS', rFile) > 0 then
    Result := fXLS;
end;

function ImportDBF(fTF: TTypeForm; cD: TDateTime; filename: string;
  SG: TADVStringGrid; pb: TProgressBar): integer;
begin
  Result := 0;
  case fTF of
    fPrihod, fVPost:
      begin
        Result := ImportPrihDBF(filename, SG, pb);
      end;
    fRashod:
      begin
        Result := ImportRashDBF(cD, filename, SG, pb);
      end;
  end;
end;

function ImportPrihDBF(filename: string; Table: TADVStringGrid;
  Progress: TProgressBar): integer;
var
  dbf: TdbfTable;
  count_r, i, row, idx: integer;
  decl: TDeclaration;
  SLDbf: TStringList;
  f1: Boolean;
begin
  Result := 0;
  SLDbf := TStringList.Create;
  dbf := TdbfTable.Create(filename, dcpPoint, ltTrueFalse, dfYYYYMMDD);
  try
    try
      // �������� DBF-����� � ������ ������ filename � ������� Table
      row := Table.RowCount - Table.FixedFooters;

      // ���������� ������� � ����� dbf
      count_r := dbf.RecordsCount;
      if count_r = 0 then
      begin
        Result:=2;
        exit;
      end;
      // Y����� ���������� ����� � �������
      Progress.Max := 100;

      for i := 1 to count_r do
      begin
        NullDecl(decl);
        decl.PK := '';
        ImportRowDbfPrihod(decl, dbf, i);

        GetProduction(fmWork.que2_sql, decl.product, f1, True);
        GetSaler(fmWork.que2_sql, decl.sal, f1, True, False);
        decl.forma := FormDecl(String(decl.product.FNSCode));

        Progress.Position := trunc(100 * (i) / count_r);

        SLDbf.AddObject(IntToStr(row), TDecl_obj.Create);
        TDecl_obj(SLDbf.Objects[SLDbf.Count - 1]).data := decl;

        Progress.UpdateControlState;
        Application.ProcessMessages;
        inc(row);
      end;

      row := Table.RowCount - Table.FixedFooters;
      Table.RowCount := Table.RowCount + count_r;
      for i := 0 to SLDbf.Count - 1 do
      begin
        decl := TDecl_obj(SLDbf.Objects[i]).data;
        if TDecl_obj(SLDbf.Objects[i]) <> nil then
          WriteRowPrihodSG(SLErrPrih, decl, Table, row + i);
        Table.Objects[0, row + i] := Pointer(1);
        SetColorRow(row + i, Table);
        inc(fmMove.CountNew);
      end;

      fmMove.sb.Panels.Items[4].Text := statusAdd + IntToStr
        (fmMove.CountNew);
      fmMove.ErrGrid(fPrihod, CountNamesSL(SLErrPrih));

      Table.AutoNumberOffset := 0;
      Table.AutoNumberStart := 0;

      Table.AutoNumberCol(0);

      Table.ColumnSize.Rows := arNormal;
      Table.NoDefaultDraw := True;
      Table.AutoSize := True;

      Calc_Footer(Table, CalcColPrih);

      Table.UnHideColumnsAll;
      for i := 0 to High(NoResColPrih) do
      begin
        idx := NoResColPrih[i];
        if not Table.IsHiddenColumn(idx) then
          Table.ColWidths[idx] := Length(Table.ColumnHeaders.Strings[idx])
            * 10;
      end;
      // ������� ������� ��������
      for i := 0 to High(HideColPrih) do
      begin
        idx := HideColPrih[i];
        Table.HideColumn(idx);
      end;
      Progress.Position := 0;
      Progress.UpdateControlState;
    except
      Result := 1;
    end;
  finally
    Progress.Position := 0;
    Progress.UpdateControlState;
    Application.ProcessMessages;
    dbf.free;
    // SLDbf.free;
    FreeStringList(SLDbf);
  end;
end;

function ImportRashDBF(wd: TDateTime; filename: string;
  Table: TADVStringGrid; Progress: TProgressBar): integer;
var
  i, j, idx, count_r, row: integer;
  decl: TDeclaration;
  SL_dbf, SL_dbfRez: TStringList;
  dbf: TdbfTable;
  EAN13, SelfKPP: string;
  TypeF: TTypeFieldDBF;
  fDBF: TDBFRash;
  Amount: Extended;
  f1: Boolean;
begin
  dbf := TdbfTable.Create(filename, dcpComma, ltTrueFalse, dfYYYYMMDD);
  Result := 0;
  try
    try
      SL_dbf := TStringList.Create;
      SL_dbfRez := TStringList.Create;

      // ���������� ������� � ����� dbf
      count_r := dbf.RecordsCount;
      if count_r=0 then
      begin
        Result:=2;
        exit;
      end;

      // ��������� �� ������� ���� ����� Dbf ���� �� ��� � �����
      for i := 1 to High(array_dbf_rash) do
      begin
        // ����� ���� � Dbf
        idx := dbf.GetFieldNumber(array_dbf_rash[i].Name_f);
        // �������� ���� Dbf
        if idx = 0 then
        begin
          Show_Inf('�� ���������� ������ ����� ������� DBF', fError);
          exit;
        end;
      end;

      // ��� ������ �� DBF ������� � ������
      Progress.Max := 200;
      for i := 1 to count_r do
      begin
        Progress.Position := trunc(100 * (i) / ((count_r - 1) * 2));
        Progress.UpdateControlState;
        Application.ProcessMessages;
        EAN13 := '';
        SelfKPP := '';
        fDBF := TDBFRash.Create;
        // ��������� �� ������� ���� ����� Dbf ���� �� ��� � �����
        for j := 1 to High(array_dbf_rash) do
        begin
          // ����� ���� � Dbf
          idx := dbf.GetFieldNumber(array_dbf_rash[j].Name_f);
          TypeF := array_dbf_rash[j].Type_f;
          // �������� ���� Dbf
          if idx <> 0 then
          begin
            case j of
              1:
                begin
                  fDBF.SelfKPP := dbf.GetField(i, idx).ValueCharacter;
                  SelfKPP := fDBF.SelfKPP;
                end;
              2:
                fDBF.reportdate := dbf.GetField(i, idx).ValueDate;
              3:
                begin
                  fDBF.EAN13 := dbf.GetField(i, idx).ValueCharacter;
                  EAN13 := fDBF.EAN13;
                end;
              4:
                fDBF.Amount := dbf.GetField(i, idx).ValueNumeric;
            end;
          end;
        end;
        SL_dbf.AddObject(SelfKPP + EAN13, fDBF);
      end;

      SL_dbf.Sort;
//      fDBF := Pointer(SL_dbf.Objects[0]);

//      decl.Amount := fDBF.Amount;
//      decl.product.EAN13 := ShortString(fDBF.EAN13);
//      GetProduction(fmWork.que2_sql, decl.product, f1, True);
//      decl.forma := FormDecl(String(decl.product.FNSCode));
//      decl.SelfKPP := ShortString(fDBF.SelfKPP);
//      decl.RepDate := dEnd;
//      decl.DeclDate := fDBF.reportdate;
//      Amount := fDBF.Amount;

      for i := 1 to SL_dbf.Count - 1 do
      begin
        Progress.Position := trunc(100 * (i + count_r) / ((count_r - 1) * 2));
        Progress.UpdateControlState;
        Application.ProcessMessages;
        fDBF := Pointer(SL_dbf.Objects[i]);
        NullDecl(decl);
        decl.Amount := fDBF.Amount;
        decl.product.EAN13 := ShortString(fDBF.EAN13);
        GetProduction(fmWork.que2_sql, decl.product, f1, True);
        decl.forma := FormDecl(String(decl.product.FNSCode));
        decl.SelfKPP := ShortString(fDBF.SelfKPP);
        decl.RepDate := dEnd;
        decl.DeclDate := fDBF.reportdate;
        Amount := fDBF.Amount;
        decl.Amount := Amount;
        decl.vol := 0.1 * decl.product.capacity * decl.Amount;
        SL_dbfRez.AddObject(FloatToStr(decl.Amount), TDecl_obj.Create);
        TDecl_obj(SL_dbfRez.Objects[SL_dbfRez.Count - 1]).data := decl;
      end;

      row := Table.RowCount - Table.FixedFooters;
      Table.RowCount := Table.RowCount + SL_dbfRez.Count;
      for i := 0 to SL_dbfRez.Count - 1 do
      begin
        decl := TDecl_obj(SL_dbfRez.Objects[i]).data;
        if TDecl_obj(SL_dbfRez.Objects[i]) <> nil then
          if TDecl_obj(SL_dbfRez.Objects[i]) is TDecl_obj then
            WriteRowRashodSG(SLErrRash, SLOborotKpp, decl, Table, row + i);
      end;
      Table.Invalidate;
      PropCellsSG(arNormal, Table, CalcColRashNew, NoResColRashNew, HideColRashNew);
      Result := 0;
      fmMove.CountNew := fmMove.CountNew + SL_dbf.Count;
      Progress.Position := 0;
      Progress.UpdateControlState;
    except
      Result := 1;
    end;
  finally
    FreeStringList(SL_dbf);
    FreeStringList(SL_dbfRez);
    dbf.free;
  end;
end;

procedure ImportRowDbfPrihod(var decl: TDeclaration; dbf: TdbfTable;
  RowDbf: integer);
var
  j, idx: integer;
  s: string;
  E: Extended;
begin
  // ��������� �� ������� ���� ����� Dbf ���� �� ��� � �����
  for j := 1 to High(array_dbf_prih) do
  begin
    // ����� ���� � Dbf
    idx := dbf.GetFieldNumber(array_dbf_prih[j].Name_f);
    // �������� ���� Dbf
    if idx <> 0 then
    begin
      case array_dbf_prih[j].Type_f of
        f_Ch:
          s := dbf.GetField(RowDbf, idx).ValueCharacter;
        f_Num:
          begin
            s := dbf.GetField(RowDbf, idx).ValueCharacter;
            if s <> '' then
            begin
              if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
                s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
              if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
                s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
            end
            else
            begin
              E := dbf.GetField(RowDbf, idx).ValueNumeric;
              s := FloatToStr(E);
            end;
          end;
        f_Dat:
          s := DateTimeToStr(dbf.GetField(RowDbf, idx).ValueDate);
      end;

      case array_dbf_prih[j].Number_f of
        0:
          begin
            if RTrim(s) = '' then
              decl.SelfKPP := fOrg.Kpp0
            else
              decl.SelfKPP := ShortString(s); // selfkpp;
          end;
        1:
          decl.DeclNum := ShortString(s); // wbnumber
        3:
          decl.DeclDate := StrToDate(s); // wbdate
        7:
          decl.sal.sINN := ShortString(s); // salerinn
        8:
          decl.sal.sKpp := ShortString(s); // salerkpp
        10:
          decl.product.EAN13 := ShortString(s); // ean13
        12:
          decl.Amount := StrToFloat(s); // amount
        13:
          decl.TM := ShortString(s); // tm
      end;
    end;
  end;
end;

function ImportXML(fTF: TTypeForm; filename: string; SG: TADVStringGrid;
  DBQ: TADOQuery; pb: TProgressBar; data: TDateTime; var ExtV:Boolean): integer;
var
  // pXML: fPrihXML;
  sal: TSaler;
  sKppXML, sInnXML: string;
  i, row: integer;
  SLXml: TStringList;
  decl: TDeclaration;
  fTypeXML: TTypeXML;
begin
  Result := 0;
  SLErrXML.Clear;
  if (fTF <> fPrihod) and (fTF <> fVPost) then
    exit;
  SLXml := TStringList.Create;
  try
    try
      fTypeXML:=GetTypeXML(fileName);
      if fTypeXML <> t_o then
        begin
          if not GetSelfKppXML(fTypeXML,fileName, sInnXML, sKppXML) then exit;
          if not GetSalerXML(fTypeXML,filename,sal) then exit;
        end;
      case fTypeXML of
        t_11o: Result := Load_XML_f11(SLXml, filename, DBQ, data, sal,sKppXML);
        t_12o: Result := Load_XML_f12(SLXml, filename, DBQ, data, sKppXML);
        t_6o: Result := Load_XML_f6(SLXml, filename, DBQ, data, sal, sKppXML);
        t_o: Result := Load_XMLF(fTF,SLXml, filename, DBQ, data, sal, ExtV);
        t_null:exit;
      end;
      if Result <> 0 then exit;

      if (SLErrXML.Count = 0) then
      begin
        row := SG.RowCount - SG.FixedFooters;
        SG.RowCount := SG.RowCount + SLXml.Count;
        if SLXml.Count = 0 then Result:=1;

        for i := 0 to SLXml.Count - 1 do
        begin
          pb.Position := trunc(100 * (i + 1) / SLXml.Count);
          pb.UpdateControlState;
          NullDecl(decl);
          decl := TDecl_obj(SLXml.Objects[i]).data;
          if TDecl_obj(SLXml.Objects[i]) <> nil then
            WriteRowPrihodSG(SLErrPrih, decl, SG, row + i);
          SG.Objects[0, row + i] := Pointer(1);
          SetColorRow(row + i, SG);
          inc(fmMove.CountNew);
        end;
        if CurSG.RowCount = CurSG.FixedRows + CurSG.FixedFooters + 1 then
          PropCellsSG(arFixed, CurSG, CalcColPrih, NoResColPrih,
            HideColPrih)
        else
          PropCellsSG(arNormal, CurSG, CalcColPrih, NoResColPrih,
            HideColPrih);

        fmMove.sb.Panels.Items[4].Text := statusAdd + IntToStr
          (fmMove.CountNew);
        fmMove.ErrGrid(fPrihod, CountNamesSL(SLErrPrih));
        pb.Position := 0;
        pb.UpdateControlState;
      end
      else
      begin
        if fmErrXML = nil then
          fmErrXML := TfmErrXML.Create(Application);
        fmErrXML.filename := filename;
        fmErrXML.sal := String(sal.sName);
        fmErrXML.Caption := '������ � XML �����';
        fmErrXML.mmo_err.Lines.Clear;
        fmErrXML.mmo_err.Lines.AddStrings(SLErrXML);
        fmErrXML.ShowModal;
        Result := 1;
      end;
    except
      Result := 1;
    end;
  finally
    FreeStringList(SLXml);
  end;
end;

function Load_XML(fTF: TTypeForm;var SLXml: TStringList; filename: string;
  DBQ: TADOQuery; dat: TDateTime; var sal: TSaler; SelfKppXML:string;
  var ExtF:Boolean): integer;
var
  Xml: IXMLDocument;
  NFile: IXMLNode;
  fl: Boolean;
  SL: TStringList;
begin

  Result := 1;
  GetSaler(fmWork.que2_sql, sal, fl, True, False);
  SL := TStringList.Create;
  SL.LoadFromFile(filename);
  SL.Text := FormatXMLData(SL.Text);
  Xml := TXMLDocument.Create(nil);
  Xml.Options := Xml.Options + [doNodeAutoIndent] - [doNodeAutoCreate,
    doAutoSave];
  Xml.NodeIndentStr := #9;
  Xml.Active := False;
  Xml.Xml.Assign(SL);
  Xml.Xml.Strings[0] := '<?xml version="1.0" encoding="windows-1251"?>';
  Xml.Active := True;
  Xml.SaveToFile(filename);
  try
    try
      NFile := Xml.DocumentElement; // �������� ��� ����
      Result := ReadXML(fTF, Xml, SLXml, SLErrXML, DBQ, sal, dat, SelfKppXML, ExtF);
    except
      Result := 1;
    end;
  finally
    Xml := nil;
    SL.free;
  end;
end;

procedure GetXMLSaler(node: IXMLNode; var sINN, sKpp: string);
var
  NSal, nUl: IXMLNode;
begin
  try
    NSal := node.ChildNodes.FindNode('����������');
    if NSal <> nil then
    begin
      nUl := NSal.ChildNodes.FindNode('��');
      if nUl <> nil then
      begin
        sINN := String(nUl.Attributes['�000000000009']);
        sKpp := String(nUl.Attributes['�000000000010']);
      end;
    end;
  finally
    NSal := nil;
    nUl := nil;
  end;
end;

function GetProizXML(SLXml: TSTrings; nRef: IXMLNode;
  var SL: TStringList): Boolean;
var
  nPr, nUl: IXMLNode;
  CountPr, i,l, rowXML: integer;
  SLRow: TStringList;
  s: string;
  errPr: Boolean;
  prod: TProducer;
begin
  Result := True;
  nPr := nRef.ChildNodes.FindNode('����������������������');
  if nPr <> nil then
  begin
    CountPr := nPr.ChildNodes.Count;

    for i := 0 to CountPr - 1 do
    begin
      errPr := False;
      SLRow := TStringList.Create;
      s := String(nPr.Attributes['�000000000004']);
      prod.pName := ShortString(s);
      SLRow.Add(s);
      nUl := nPr.ChildNodes.FindNode('��');
      errPr := False;
      if nUl <> nil then
      begin
        try
          s := String(nUl.Attributes['�000000000005']);
          prod.pINN := ShortString(s);
        except
          errPr := True;
          prod.pINN := '';
        end;


        l := Length(prod.pINN);
        case l of
          8:
            try
              prod.pInn := ShortString(CreateNewInn(string(prod.pInn)));
              prod.pKPP := NullKPP8;
            except
              prod.pKPP := NullKPP8;
            end;
          9:
            try
              prod.pInn := ShortString(CreateNewInn(string(prod.pInn)));
              prod.pKPP := NullKPP9;
            except
              prod.pKPP := NullKPP9;
            end;
          10:
            try
              s := String(nUl.Attributes['�000000000006']);
              prod.pKPP := ShortString(s);
            except
              errPr := True;
              prod.pKPP := '';
            end;
        end;
        if not errPr then
        begin
          SLRow.Add(String(prod.pINN));
          SLRow.Add(string(prod.pKPP));
        end;
//        try
//          s := String(nUl.Attributes['�000000000006']);
//          prod.pKPP := ShortString(s);
//        except
//          errPr := True;
//          prod.pKPP := '';
//        end;
        if not errPr then
          SLRow.Add(s);
        if (prod.pINN = '') or (prod.pKPP = '') then
          errPr := True;
      end;
      s := String(nPr.Attributes['�����������']);
      if not errPr then
        SL.AddObject(s, SLRow)
      else
      begin
        rowXML := GetRowXML(SLXml, nUl.Xml);
        GetErrProducer_XML(SLErrXML, prod, rowXML);
        if (prod.pKPP = '') or (prod.pINN = '') then
          Result := False;
        SLRow.free;
      end;
    end;
  end;
end;

function GetPostXML(SLXml: TSTrings; nRef: IXMLNode;
  var SL: TStringList): Boolean;
var
  nPr, nUl: IXMLNode;
  CountPr, i, rowXML: integer;
  SLRow: TStringList;
  errSal: Boolean;
  s: string;
  saler: TSaler;
begin
  Result := True;
  nPr := nRef.ChildNodes.FindNode('����������');
  if nPr <> nil then
  begin
    CountPr := nPr.ChildNodes.Count;

    for i := 0 to CountPr - 1 do
    begin
      errSal := False;
      SLRow := TStringList.Create;
      s := String(nPr.Attributes['�000000000007']);
      saler.sName := ShortString(s);
      SLRow.Add(s);
      nUl := nPr.ChildNodes.FindNode('��');
      if nUl <> nil then
      begin
        try
          s := String(nUl.Attributes['�000000000009']);
          saler.sINN := ShortString(s);
        except
          s := '';
          saler.sINN := ShortString(s);
          errSal := True;
        end;

        SLRow.Add(s);
        try
          s := String(nUl.Attributes['�000000000010']);
          saler.sKpp := ShortString(s);
        except
          s := '';
          errSal := True;
          saler.sKpp := ShortString(s);
        end;
        SLRow.Add(s);
        s := String(nPr.Attributes['��������']);
        if (saler.sINN = '') or (saler.sKpp = '') then
          errSal := True;

        if not errSal then
          SL.AddObject(s, SLRow)
        else
        begin
          // s:=TrimLeft(Copy(nPr.XML,0,Pos(#$d#$a,nPr.XML)-1));
          rowXML := GetRowXML(SLXml, nUl.Xml);
          GetErrSaler_XML(SLErrXML, saler, rowXML);
          if (saler.sKpp = '') or (saler.sINN = '') then
            Result := False;
          SLRow.free;
        end;
      end;
    end;
  end;
end;

// function ImportXLS(fTF: TTypeForm; filename: string; Table: TADVStringGrid;
// DBQ2: TADOQuery; Progress: TProgressBar): Boolean;
// var xls: TXLSFile;
// s:string;
// begin
// Result := False;
// if fTF <> fPrihod then exit;
// Progress.Position := Progress.Min;
// Progress.UpdateControlState;
// Application.ProcessMessages;
// xls:= TXLSFile.Create;
// s:=xls.Workbook.Sheets[0]/
// try
// try
// xls.OpenFile(filename);
// //if xls.Workbook.Sheets[0].Cells[0,0].Merged then
// s:= xls.Workbook.Sheets[0].Cells[12,0].Value;
// if s = '' then exit;
//
// except
// end;
// finally
// xls.Destroy;
// end;
// end;

function ImportXLS(fTF: TTypeForm; filename: string; Table: TADVStringGrid;
  DBQ2: TADOQuery; Progress: TProgressBar): Boolean;
var
  Excel: Variant;
  i, n, r: integer;
  load: Boolean;
  rmin, rmax, rstart, cmin, cmax: integer;
  c1, s1, S2: string;
  sDecl: TDeclaration;
  d: TDate;
  f1: Boolean;
begin
  if fTF <> fPrihod then
  begin
    Result := False;
    exit;
  end;
  Result := True;
  Progress.Position := Progress.Min;
  Progress.UpdateControlState;
  Application.ProcessMessages;
  try
    try
      Excel := CreateOleObject('Excel.Application');
      Excel.DisplayAlerts := False;
      Excel.WorkBooks.Open[filename];
      rmin := Excel.ActiveSheet.UsedRange.row;
      rmax := rmin + Excel.ActiveSheet.UsedRange.Rows.Count - 1;
      cmin := Excel.ActiveSheet.UsedRange.Column;
      cmax := cmin + Excel.ActiveSheet.UsedRange.Columns.Count - 1;
      rstart := -1;
      load := False;
      for i := rmin to rmax do
      begin
        c1 := Excel.Cells[i, cmin];
        if not ValidInt(c1, n) then
          n := 0;
        if not load and (n = 1) then
          rstart := i;
        if rstart > 0 then
        begin
          if ValidInt(c1, n) then
            load := True
          else
            load := False;
          if load then
          begin
            // sd.pn := c1;
            sDecl.product.FNSCode := ShortString(Excel.Cells[i, cmin + 2]);
            sDecl.product.prod.pName := ShortString
              (Excel.Cells[i, cmin + 3]);
            sDecl.product.prod.pINN := ShortString
              (Excel.Cells[i, cmin + 4]);
            sDecl.product.prod.pKPP := ShortString
              (Excel.Cells[i, cmin + 5]);
            IDProiz(DBQ2, sDecl.product.prod);
            GrupEAN(DBQ2, sDecl.product);
            sDecl.sal.sINN := ShortString(Excel.Cells[i, cmin + 7]);
            sDecl.sal.sKpp := ShortString(Excel.Cells[i, cmin + 8]);
            GetSaler(fmWork.que2_sql, sDecl.sal, f1, True,False);
            // sDecl.sal.sName := ShortString(SalerName(DBQ2, sDecl.sal));
            if not ValidDate(Excel.Cells[i, cmin + 13], d) then
              sDecl.DeclDate := 0
            else
              sDecl.DeclDate := d;
            sDecl.TM := ShortString(Excel.Cells[i, cmin + 14]);
            sDecl.DeclNum := ShortString(Excel.Cells[i, cmin + 15]);
            if not ValidFloat(Excel.Cells[i, cmin + 16], sDecl.vol) then
              sDecl.vol := 0;
            sDecl.Amount := KolEAN(sDecl.product.AlcVol,
              sDecl.product.capacity, sDecl.vol);
            r := Table.RowCount - Table.FixedFooters - 1;
            with Table do
            begin
              s1 := trim(Table.Cells[5, r]);
              S2 := trim(Table.Cells[6, r]);
              if (s1 <> '') and (S2 <> '') then
              begin
                Table.InsertRows(RowCount - FixedFooters, 1, True);
                r := RowCount - FixedFooters - 1;
              end;
              Cells[2, r] := String(fOrg.Kpp0);
              Cells[3, r] := String(sDecl.DeclNum);
              Cells[4, r] := DateToStr(sDecl.DeclDate);
              Cells[5, r] := String(sDecl.sal.sINN);
              Cells[6, r] := String(sDecl.sal.sKpp);
              Cells[7, r] := String(sDecl.sal.sName);
              Cells[8, r] := String(sDecl.product.EAN13);
              Cells[9, r] := ShowNaim(String(sDecl.product.EAN13),
                String(sDecl.product.FNSCode),
                String(sDecl.product.Productname));
              Cells[10, r] := ShowKol(String(sDecl.product.EAN13),
                sDecl.Amount, sDecl.vol);
              Cells[11, r] := FloatToStrF(sDecl.vol, ffFixed, maxint, 3);
              Cells[12, r] := String(sDecl.TM);
              Cells[13, r] := String(sDecl.product.prod.pINN);
              Cells[14, r] := String(sDecl.product.prod.pKPP);
            end;
          end;
        end;
        Progress.Position := trunc(100 * (i) / rmax);
        Progress.UpdateControlState;
        Application.ProcessMessages;
      end;
      // Excel.ActiveWorkbook.SaveAs(filename);
      Excel.ActiveWorkBook.Close;
    except
      Result := False;
    end;
  finally
    Excel.Application.Quit;
    Progress.Position := Progress.Max;
    Progress.UpdateControlState;
    Application.ProcessMessages;
  end;
end;

procedure IDProiz(DBQ: TADOQuery; var prod: TProducer);
var
  sql: TADOQuery;
begin
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text :=
        'SELECT top 1 PK AS IDP, INN, KPP, FULLNAME' + #13#10 +
        'FROM Producer' + #13#10 + 'WHERE (INN=''' + String(prod.pINN)
        + ''' and KPP=''' + String(prod.pKPP) + ''')';
      sql.Open;
      if not sql.Eof then
        sql.First;
      prod.pId := ShortString(sql.FieldByName('idp').AsString);
    except
      prod.pId := 'Null';
    end;
  finally
    sql.free;
  end;

end;

function GrupEAN(DBQ: TADOQuery; var prod: TProduction): string;
var
  sql: TADOQuery;
  proiz_part: string;
begin
  Result := '';
  prod.capacity := 0;
  prod.AlcVol := 0;
  prod.Productname := '';

  if NoValue(String(prod.prod.pId)) then
    proiz_part := ''
  else
    proiz_part := ' and (producercode=' + String(prod.prod.pId) + ')';
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;

      sql.sql.Text := 'SELECT *' + #13#10 + 'FROM PRODUCTION' + #13#10 +
        'WHERE (fnscode=''' + String(prod.FNSCode) + ''')' + proiz_part;
      // +#13#10+//;

      sql.Open;
      if sql.RecordCount > 0 then
      begin
        sql.Last;
        prod.capacity := sql.FieldByName('capacity').AsFloat;
        prod.AlcVol := sql.FieldByName('alcvol').AsFloat;
        prod.Productname := ShortString
          (sql.FieldByName('productname').AsString);
        prod.FNSCode := ShortString(sql.FieldByName('ean13').AsString);
      end;
    except
      Result := 'Null';
    end;
  finally
    sql.free;
  end;
end;

// function SalerName(DBQ: TADOQuery; var sal: TSaler): string;
// Var
// EndGrid, i, j, num: integer;
// qr1: TADOQuery;
// RefSalerKoef: integer;
// find: Boolean;
// begin
// Result := NoSal;
// qr1 := nil;
// RefSalerKoef := 0;
//
// find := False;
// i := 0;
// num := High(RefSaler[0]) - 1;
// while (i <= num) and (not find) do
// begin
// if (RefSaler[2, i] = sal.sINN) and (RefSaler[3, i] = sal.sKpp) then
// begin
// find := True;
// Result := RefSaler[1, i];
// end;
// inc(i);
// end;
// if (not find) then
// begin
// try
// try
// qr1 := TADOQuery.Create(DBQ.Owner);
// qr1.Connection := DBQ.Connection;
// qr1.sql.Text := 'select * from Saler where OrgINN=''' + sal.sINN +
// ''' and OrgKPP=''' + sal.sKpp + '''';
// qr1.Open;
// if qr1.RecordCount > 0 then
// begin
// Result := qr1.FieldByName('orgname').AsString;
// // ����������� ����� ������� �� 1
// SetLength(RefSaler, Length(RefSaler), Length(RefSaler[0]) + 1);
// for i := 0 to qr1.FieldCount - 1 do
// RefSaler[i, num + 1] := qr1.Fields[i].AsString;
//
// qr1.sql.Text := 'UPDATE Saler ' + #13#10 +
// 'Set LocRef=true WHERE OrgINN=''' + sal.sINN + ''' and OrgKPP=''' +
// sal.sKpp + '''';
// qr1.ExecSQL;
//
// end;
// finally
// qr1.free;
// end;
// except
// end;
// end;
// end;

function KolEAN(avol, vol, prih: Extended): Extended;
var
  kol: Extended;
begin
  if vol > 0 then
    kol := 10 * prih / vol // no half-bottles!!!
  else
    kol := 0;
  KolEAN := kol;
end;

function ShowNaim(ean, grup, name: string): string;
begin
  if Empty(ean) then
    // ShowNaim := '������ '+ grup
    ShowNaim := '������ ' + ImpGrup(grup, True)
  else
    ShowNaim := name;
end;

function ShowKol(ean: string; kol, vol: Extended): string;
begin
  if Empty(ean) then
    ShowKol := FFloat(10 * vol) // +' �'
  else
    ShowKol := IntToStr(Max(trunc(kol), 1));
end;

function ImpGrup(grup: string; to_old: Boolean): string;
var
  i, idx, n: integer;
  found: Boolean;
  grup_i, rez: string;
begin
  idx := -1;
  rez := grup;

  if to_old then
    n := Length(grup_inew)
  else
    n := Length(grup_iold);
  i := 0;
  found := False;
  while (i < n) and not found do
  begin
    if to_old then
      grup_i := grup_inew[i]
    else
      grup_i := grup_iold[i];
    if grup = grup_i then
    begin
      idx := i;
      found := True;
    end;
    i := i + 1;
  end;

  if to_old then
    n := Length(grup_iold)
  else
    n := Length(grup_inew);

  if found and (idx <= n) then
  begin
    if to_old then
      rez := grup_iold[idx]
    else
      rez := grup_inew[idx];
  end;

  ImpGrup := rez;
end;

procedure ShowOstKPP(SLOst: TStringList; SG: TADVStringGrid; kpp: string);
var
  i, row: integer;
  mov: TMove;
  prodObj: TProduction_obj;
begin
  row := SG.FixedRows;

  for i := 0 to SLOst.Count - 1 do
  begin
    mov := Pointer(SLOst.Objects[i]);
    if mov.SelfKPP = ShortString(kpp) then
    begin
      SG.InsertRows(row, 1);
      SG.Objects[0, row] := mov;
      // SG.AddCheckBox(1,Row,False,False);
      SG.Cells[2, row] := String(mov.product.EAN13);
      SG.Cells[3, row] := String(mov.product.ProductNameF);
      prodObj := TProduction_obj.Create;
      prodObj.data := mov.product;
      SG.Objects[3, row] := Pointer(prodObj);
      SG.Cells[4, row] := FloatToStrF(mov.dv.ost1, ffFixed, maxint, ZnFld);
      SG.Cells[5, row] := FloatToStrF(mov.dv_dal.ost1, ffFixed, maxint,
        ZnFld);
      SG.Cells[6, row] := String(mov.sal.sINN);
      SG.Cells[7, row] := String(mov.sal.sKpp);
      SG.Cells[8, row] := String(mov.sal.sName);
      inc(row);
    end;
  end;
  SG.AutoNumberOffset := 0;
  SG.AutoNumberStart := 0;
  SG.AutoNumberCol(1);

  SG.ColumnSize.Rows := arNormal;
  SG.AddCheckBoxColumn(0, False, False);
  // SG.NoDefaultDraw := True;
  // SG.AutoSize := True;
  SG.CalcFooter(1);
  SG.ColWidths[3] := Length(SG.ColumnHeaders.Strings[3]) * 10;
end;

procedure ShapkaSGCheck(SG: TADVStringGrid;
  NameCol, FilterCol: array of integer);
var
  i, idx: integer;
  SL: TStringList;
begin
  SL := TStringList.Create;
  try

    SG.RowCount := SG.FixedRows + SG.FixedFooters;
    SG.ColCount := High(NameCol) + 3;

    with SG do
    begin
      // ���������� �������� ��������
      ColumnHeaders.Add('');
      for i := 0 to High(NameCol) do
      begin
        idx := NameCol[i];
        ColumnHeaders.Add(GrNam[idx]);
      end;
    end;
    SG.AddCheckBoxColumn(0, False, True);
    SG.AutoFilterUpdate := True;
  finally
    SL.free
  end;
end;

procedure Moving(dtp: TDate; DBQ: TADOQuery; SG: TADVStringGrid;
  var SLAll, SLData, SLOst: TStringList; KppS, KppR: string);
var
  sql: TADOQuery;
  PK, i: integer;
  mv: TMove;
  Amount: Extended;
  state: Boolean;
  decl: TDeclaration;
begin
  SortSLCol(1, SLAll, CompNumAsc);
  SortSLCol(1, SLData, CompNumAsc);
  PK := 0;
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text := 'select MAX(PK) from Declaration';
      sql.Open;
      PK := sql.Fields[0].AsInteger + 1;

      for i := SG.FixedRows to SG.RowCount - SG.FixedRows - SG.FixedFooters
        do
      begin
        mv := Pointer(SG.Objects[0, i]);
        Amount := StrToFloatDef(SG.Cells[4, i], 0);
        SG.GetCheckBoxState(0, i, state);
        if state then
          if (not Empty(String(mv.product.EAN13))) and (Amount > 0) then
          begin
            sql.sql.Clear;
            sql.Parameters.Clear;
            sql.sql.Text :=
              'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, inn, kpp ) '
              + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :inn, :kpp)';

            sql.Parameters.AddParameter.Name := 'PK';
            sql.Parameters.ParamByName('PK').DataType := ftInteger;
            sql.Parameters.ParamByName('PK').value := PK;

            sql.Parameters.AddParameter.Name := 'DeclType';
            sql.Parameters.ParamByName('DeclType').DataType := ftString;
            sql.Parameters.ParamByName('DeclType').value := IntToStr(6);

            sql.Parameters.AddParameter.Name := 'ReportDate';
            sql.Parameters.ParamByName('ReportDate').DataType := ftDate;
            sql.Parameters.ParamByName('ReportDate').value := dEnd;

            sql.Parameters.AddParameter.Name := 'SelfKPP';
            sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
            sql.Parameters.ParamByName('SelfKPP').value := KppS;

            sql.Parameters.AddParameter.Name := 'DeclDate';
            sql.Parameters.ParamByName('DeclDate').DataType := ftDate;
            sql.Parameters.ParamByName('DeclDate').value := dtp;

            sql.Parameters.AddParameter.Name := 'SalerINN';
            sql.Parameters.ParamByName('SalerINN').DataType := ftString;
            sql.Parameters.ParamByName('SalerINN').value := fOrg.inn;

            sql.Parameters.AddParameter.Name := 'SalerKPP';
            sql.Parameters.ParamByName('SalerKPP').DataType := ftString;
            sql.Parameters.ParamByName('SalerKPP').value := KppR;

            sql.Parameters.AddParameter.Name := 'EAN13';
            sql.Parameters.ParamByName('EAN13').DataType := ftString;
            sql.Parameters.ParamByName('EAN13').value := mv.product.EAN13;

            sql.Parameters.AddParameter.Name := 'Amount';
            sql.Parameters.ParamByName('Amount').DataType := ftString;
            sql.Parameters.ParamByName('Amount').value := FloatToStr
              (Amount);

            sql.Parameters.AddParameter.Name := 'IsGood';
            sql.Parameters.ParamByName('IsGood').DataType := ftBoolean;
            sql.Parameters.ParamByName('IsGood').value := True;

            sql.Parameters.AddParameter.Name := 'INN';
            sql.Parameters.ParamByName('INN').DataType := ftString;
            sql.Parameters.ParamByName('INN').value := mv.product.prod.pINN;

            sql.Parameters.AddParameter.Name := 'kpp';
            sql.Parameters.ParamByName('kpp').DataType := ftString;
            sql.Parameters.ParamByName('kpp').value := mv.product.prod.pKPP;

            sql.ExecSQL;
            NullDecl(decl);
            decl.PK := ShortString(IntToStr(PK));
            decl.SelfKPP := ShortString(KppS);
            decl.sal.sINN := ShortString(fOrg.inn);
            decl.sal.sKpp := ShortString(KppR);
            decl.sal.sName := ShortString(fOrg.SelfName);
            decl.product := mv.product;
            decl.TypeDecl := 6;
            SLAllDecl.AddObject(IntToStr(PK), TDecl_obj.Create);
            TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;

            SLData.AddObject(IntToStr(PK), TDecl_obj.Create);
            TDecl_obj(SLData.Objects[SLData.Count - 1]).data := decl;

            inc(PK);

            sql.sql.Clear;
            sql.Parameters.Clear;
            sql.sql.Text :=
              'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, inn, kpp ) '
              + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :inn, :kpp)';

            sql.Parameters.AddParameter.Name := 'PK';
            sql.Parameters.ParamByName('PK').DataType := ftInteger;
            sql.Parameters.ParamByName('PK').value := PK;

            sql.Parameters.AddParameter.Name := 'DeclType';
            sql.Parameters.ParamByName('DeclType').DataType := ftString;
            sql.Parameters.ParamByName('DeclType').value := IntToStr(7);

            sql.Parameters.AddParameter.Name := 'ReportDate';
            sql.Parameters.ParamByName('ReportDate').DataType := ftDate;
            sql.Parameters.ParamByName('ReportDate').value := dEnd;

            sql.Parameters.AddParameter.Name := 'SelfKPP';
            sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
            sql.Parameters.ParamByName('SelfKPP').value := KppR;

            sql.Parameters.AddParameter.Name := 'DeclDate';
            sql.Parameters.ParamByName('DeclDate').DataType := ftDate;
            sql.Parameters.ParamByName('DeclDate').value := dtp;

            sql.Parameters.AddParameter.Name := 'SalerINN';
            sql.Parameters.ParamByName('SalerINN').DataType := ftString;
            sql.Parameters.ParamByName('SalerINN').value := fOrg.inn;

            sql.Parameters.AddParameter.Name := 'SalerKPP';
            sql.Parameters.ParamByName('SalerKPP').DataType := ftString;
            sql.Parameters.ParamByName('SalerKPP').value := KppS;

            sql.Parameters.AddParameter.Name := 'EAN13';
            sql.Parameters.ParamByName('EAN13').DataType := ftString;
            sql.Parameters.ParamByName('EAN13').value := mv.product.EAN13;

            sql.Parameters.AddParameter.Name := 'Amount';
            sql.Parameters.ParamByName('Amount').DataType := ftString;
            sql.Parameters.ParamByName('Amount').value := FloatToStr
              (Amount);

            sql.Parameters.AddParameter.Name := 'IsGood';
            sql.Parameters.ParamByName('IsGood').DataType := ftBoolean;
            sql.Parameters.ParamByName('IsGood').value := True;

            sql.Parameters.AddParameter.Name := 'INN';
            sql.Parameters.ParamByName('INN').DataType := ftString;
            sql.Parameters.ParamByName('INN').value := mv.product.prod.pINN;

            sql.Parameters.AddParameter.Name := 'kpp';
            sql.Parameters.ParamByName('kpp').DataType := ftString;
            sql.Parameters.ParamByName('kpp').value := mv.product.prod.pKPP;

            sql.ExecSQL;
            decl.PK := ShortString(IntToStr(PK));
            decl.SelfKPP := ShortString(KppR);
            decl.sal.sINN := ShortString(fOrg.inn);
            decl.sal.sKpp := ShortString(KppS);
            decl.sal.sName := ShortString(fOrg.SelfName);
            decl.product := mv.product;
            decl.TypeDecl := 7;
            SLAllDecl.AddObject(IntToStr(PK), TDecl_obj.Create);
            TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;
            SLData.AddObject(IntToStr(PK), TDecl_obj.Create);
            TDecl_obj(SLData.Objects[SLData.Count - 1]).data := decl;
            inc(PK);
          end;
      end;
    except
    end;
  finally
    sql.free;
  end;
end;

procedure FillCurOst(SGRash, SGOst: TADVStringGrid);
var
  i, idx, row: integer;
  SLEanSG: TStringList;
  mov: TMove;
  s: string;
  // prodObj: TProductionObj;
begin
  SLEanSG := TStringList.Create;

  try
    // ���������� ������ ����� ����� ��� + ��N
    for i := SGRash.FixedRows to SGRash.RowCount - SGRash.FixedRows -
      SGRash.FixedFooters do
      SLEanSG.Add(CurSG.Cells[2, i] + CurSG.Cells[4, i]);

    SGOst.ClearAll;
    ShapkaSG(SGOst, NameColRashNew);
    SGOst.RowCount := SGOst.FixedRows + SGOst.FixedFooters;
    for i := 0 to SLOstatkiKpp.Count - 1 do
    begin
      mov := Pointer(SLOstatkiKpp.Objects[i]);
      if mov <> nil then
      begin
        s := String(mov.SelfKPP + mov.product.EAN13);
        idx := SLEanSG.IndexOf(s);
        if idx = -1 then
        begin
          SGOst.InsertRows(SGOst.RowCount - SGOst.FixedFooters, 1);
          row := SGOst.RowCount - SGOst.FixedFooters - 1;
          SGOst.Cells[1, row] := String(mov.product.FNSCode);
          SGOst.Cells[2, row] := String(mov.SelfKPP);
          SGOst.Cells[3, row] := DateToStr(dEnd);
          SGOst.Cells[4, row] := String(mov.product.EAN13);
          SGOst.Cells[5, row] := String(mov.product.ProductNameF);
          // prodObj := TProductionObj.Create;
          // prodObj.data := mov.product;

          PrihProcOst(mov.product, mov.product.capacity,
            mov.dv.rash, mov.dv.ost1, mov.dv_dal.rash, mov.dv_dal.ost1);

          // SGOst.Objects[5, row] := Pointer(prodObj);
          SGOst.Cells[6, row] := FloatToStrF(mov.dv.ost0, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[7, row] := FloatToStrF(mov.dv.prih, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[8, row] := FloatToStrF(mov.dv.oder, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[9, row] := FloatToStrF(mov.dv.rash, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[10, row] := FloatToStrF(mov.dv.ost1, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[11, row] := FloatToStrF(0, ffFixed, maxint,
            ZnFld);
          SGOst.Cells[12, row] := FloatToStrF(mov.dv_dal.rash, ffFixed,
            maxint, ZnFld);
          SGOst.Cells[13, row] := FloatToStrF(mov.dv_dal.ost1, ffFixed,
            maxint, ZnFld);
          SGOst.Cells[14, row] := String(mov.product.prod.pName);
          SGOst.Cells[15, row] := String(mov.product.prod.pINN);
          SGOst.Cells[16, row] := String(mov.product.prod.pKPP);
          SGOst.Cells[17, row] := '';
        end;
      end;
    end;
    SGOst.AddCheckBoxColumn(0, False, False);
    SGOst.ColumnSize.Rows := arAll;
    SGOst.NoDefaultDraw := True;
    SGOst.AutoSize := True;
    SGOst.ColWidths[5] := Length(SGOst.ColumnHeaders.Strings[5]) * 20;
    // ������� ������� ��������
    for i := 0 to High(HideColSelAutoRash) do
    begin
      idx := HideColSelAutoRash[i];
      SGOst.HideColumn(idx);
    end;
  finally
    SLEanSG.free;
  end;
end;

procedure PrihProcOst(prod: TProduction; Cap: Extended;
  var rash, ost1, rashd, ost1d: Extended);
var
  tmp: Extended;
  s, FNSCode: string;
  idx,ProcOst, fDecl : Integer;
begin
  s := Copy(String(prod.EAN13), 0, 2);
  FNSCode := string(prod.FNSCode);
  fDecl:=FormDecl(FNSCode);
  idx := SLProcOst.IndexOfName(FNSCode);
  if idx <> -1 then
    ProcOst := StrToInt(SLProcOst.Values[SLProcOst.Names[idx]])
  else
    ProcOst := 0;

  if (s = '27') or (s = '29') then
  begin
    if s = '27' then
    begin
      tmp := (ost1 * ProcOst) / 100;
      if tmp < 1 then
        tmp:=0;
      rash := ost1 - tmp;
      ost1 := tmp;
      rashd := 0.1 * rash * Cap;
      ost1d := 0.1 * ost1 * Cap;
    end;

    if s = '29' then
    begin
      rash := ost1;
      ost1 := 0;
      rashd := 0.1 * rash * Cap;
      ost1d := 0.1 * ost1 * Cap;
    end;
  end
  else
  begin
    tmp := ROUND((ost1 * ProcOst) / 100);
    rash := ost1 - tmp;
    ost1 := tmp;
    rashd := 0.1 * rash * Cap;
    ost1d := 0.1 * ost1 * Cap;
  end;
end;

function AddSelectEan(SGSel, SG: TADVStringGrid): integer;
var
  i, row: integer;
  SLRow: TStringList;
  sel: Boolean;
begin
  SLRow := TStringList.Create;
  SGSel.UnHideColumnsAll;
  Result := 0;
  try
    for i := SGSel.FixedRows to SGSel.RowCount - SGSel.FixedRows -
      SG.FixedFooters do
      if SGSel.GetCheckBoxState(0, i, sel) then
        if sel then
        begin
          SLRow.Assign(SGSel.Rows[i]);
          SG.InsertRows(SG.RowCount - SG.FixedFooters, 1);
          row := SG.RowCount - SG.FixedFooters - 1;
          SLRow.Delete(0);
          SLRow.Insert(0, '');
          SG.Rows[row].Assign(SLRow);
          SG.RowColor[row] := clInsert;
          inc(Result);
        end;

    PropCellsSG(arNormal, SG, CalcColRashNew, NoResColRashNew, HideColRashNew);

  finally
    SLRow.free;
  end;
end;

procedure DeleteDataSLBD;
var
  i, idx: integer;
  PK,s: string;
  isOst:Boolean;
begin
  SortSLCol(1,SLAllDecl);
  // ������� ������
  for i := 0 to DelDecl.Count - 1 do
  begin
    PK := DelDecl[i];
    IsOst:=False;
    s:= Copy(PK,Length(PK),1);
    if s = 'o' then
    begin
      IsOst:=True;
      PK:=Copy(PK,0,Length(PK)-1);
    end;
    // ���� ������� �� ����, �� � ������� �� ��������
    if DelRowDB(PK, fmWork.que1_sql) then
    begin
      idx := SLAllDecl.IndexOf(PK);
      if idx <> -1 then
      begin
        TDecl_obj(SLAllDecl.Objects[idx]).free;
        SLAllDecl.Delete(idx);
      end;
      if isOst then
      begin
        idx:=GetIdxOstPK(PK);
        if idx <> -1 then
        begin
          TOst_obj(SLDataOst.Objects[idx]).free;
          SLDataOst.Delete(idx)
        end;
      end;
      fmProgress.pb.StepIt;
    end;
  end;
  DelDecl.Clear;
end;


function DelRowDB(PK: string; DBQ: TADOQuery): Boolean;
var
  sql: TADOQuery;
begin
  if PK = '' then exit;

  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text := 'DELETE FROM Declaration WHERE PK=' + PK;
      sql.ExecSQL;

      Result := True;
    except
      Result := False;
    end;
  finally
    sql.free;
  end;
end;

procedure NullSaler(var sal: TSaler);
begin
  sal.sINN := '';
  sal.sKpp := '';
  sal.sName := '';
  sal.sAddress := '';
  sal.sId:='';
  sal.lic.lId:='';
  sal.lic.ser:='';
  sal.lic.num:='';
  sal.lic.RegOrg:='';
  sal.lic.dBeg:=dBeg;
  sal.lic.dEnd:=dEnd;
end;

function SaveRowUpdDB(DBQ: TADOQuery; decl: TDeclaration; IsOst:Boolean): Boolean;
var
  sql: TADOQuery;
begin
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Clear;
      sql.Parameters.Clear;

      sql.sql.Text := 'UPDATE Declaration SET ' + 'SelfKPP = :SelfKPP, ' +
        'Declnumber = :Declnumber, ' + 'DeclDate = :DeclDate, ' +
        'SalerINN = :SalerINN, ' + 'SalerKPP = :SalerKPP, ' +
        'EAN13 = :EAN13, ' + 'Amount = :Amount, ' +
        'TM = :TM, inn = :inn, ' + 'kpp = :kpp ' +
        'WHERE PK = :PK and DeclType = :DeclType';

      sql.Parameters.AddParameter.Name := 'SelfKPP';
      sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
      sql.Parameters.ParamByName('SelfKPP').value := decl.SelfKPP;

      sql.Parameters.AddParameter.Name := 'Declnumber';
      sql.Parameters.ParamByName('Declnumber').DataType := ftString;
      sql.Parameters.ParamByName('Declnumber').value := decl.DeclNum;

      sql.Parameters.AddParameter.Name := 'DeclDate';
      sql.Parameters.ParamByName('DeclDate').DataType := ftDateTime;
      sql.Parameters.ParamByName('DeclDate').value := decl.DeclDate;

      sql.Parameters.AddParameter.Name := 'SalerINN';
      sql.Parameters.ParamByName('SalerINN').DataType := ftString;

      sql.Parameters.AddParameter.Name := 'SalerKPP';
      sql.Parameters.ParamByName('SalerKPP').DataType := ftString;

      if (decl.TypeDecl = 1) or (decl.TypeDecl = 3) then
      begin
        sql.Parameters.ParamByName('SalerINN').value := decl.sal.sINN;
        sql.Parameters.ParamByName('SalerKPP').value := decl.sal.sKpp;
      end
      else
      begin
        sql.Parameters.ParamByName('SalerINN').value := 'NULL';
        sql.Parameters.ParamByName('SalerKPP').value := 'NULL';
      end;

      sql.Parameters.AddParameter.Name := 'EAN13';
      sql.Parameters.ParamByName('EAN13').DataType := ftString;
      sql.Parameters.ParamByName('EAN13').value := decl.product.EAN13;

      sql.Parameters.AddParameter.Name := 'Amount';
      sql.Parameters.ParamByName('Amount').DataType := ftString;
      if IsOst then
        sql.Parameters.ParamByName('Amount').value := FloatToStr(decl.Amount)
      else
        sql.Parameters.ParamByName('Amount').value := FloatToStr(decl.Ost.Value);

      sql.Parameters.AddParameter.Name := 'TM';
      sql.Parameters.ParamByName('TM').DataType := ftString;
      sql.Parameters.ParamByName('TM').value := decl.TM;

      sql.Parameters.AddParameter.Name := 'inn';
      sql.Parameters.ParamByName('inn').DataType := ftString;
      sql.Parameters.ParamByName('inn').value := decl.product.prod.pINN;

      sql.Parameters.AddParameter.Name := 'kpp';
      sql.Parameters.ParamByName('kpp').DataType := ftString;
      sql.Parameters.ParamByName('kpp').value := decl.product.prod.pKPP;

      sql.Parameters.AddParameter.Name := 'PK';
      sql.Parameters.ParamByName('PK').DataType := ftInteger;
      sql.Parameters.ParamByName('PK').value := StrToInt(String(decl.PK));

      sql.Parameters.AddParameter.Name := 'DeclType';
      sql.Parameters.ParamByName('DeclType').DataType := ftString;
      sql.Parameters.ParamByName('DeclType').value := IntToStr
        (decl.TypeDecl);
      sql.ExecSQL;

      Result := True;

    except
      Result := False;
    end;
  finally
    sql.free;
  end;
end;

procedure SaveRowUpdLocal(decl: TDeclaration);
var
  PK: string;
  idx: integer;
begin
  PK := String(decl.PK);
  idx := SLAllDecl.IndexOf(PK);
  if idx <> -1 then
    TDecl_obj(SLAllDecl.Objects[idx]).data := decl;

  idx := CurSL.IndexOf(PK);
  if idx <> -1 then
    TDecl_obj(CurSL.Objects[idx]).data := decl;
//  if decl.Ost.Value > 0 then
//  begin
//    s := decl.SelfKpp+decl.product.EAN13;
//    idx:=SLDataOst.IndexOf(s);
//    if idx <> -1 then
//      TOst_obj(SLDataOst.Objects[idx]).data.Value := decl.Ost.Value
//    else
//    begin
//      SLDataOst.AddObject(s, TOst_obj.Create);
//      TOst_obj(SLDataOst.Objects[SLDataOst.Count - 1]).data.Value := decl.Ost.Value;
//    end;
//  end;
end;

procedure ErrCells(ACol, ARow: integer);
var
  idx: integer;
begin
  idx := LongInt(CurSG.Objects[ACol, ARow]);
  case idx of
    0:
      CurRowLabel.Font.Color := clNone;
    1, 2, 3:
      CurRowLabel.Font.Color := clErr;
  end;
  case idx of
    0:
      CurRowLabel.Caption := '� ������ ��� ������';
    1:
      CurRowLabel.Caption := '������ �����';
    2:
      CurRowLabel.Caption := '������ ���������� ������ � ����';
    3:
      CurRowLabel.Caption := '������ �������� ����������';
  end;
end;


function GetRowXML(SLXml: TSTrings; Text: string): integer;
var
  i: integer;
  s: string;
begin
  Result := -1;
  for i := 0 to SLXml.Count - 1 do
  begin
    s := TrimLeft(SLXml.Strings[i]);
    if Text = s then
    begin
      Result := i + 1;
      exit;
    end
  end;
end;

procedure GetErrProducer_XML(var SLErrXML: TStringList; prod: TProducer;
  row: integer);
var
  ErrInn, errKpp, b1: Boolean;
  SLRow: TStringList;
  s, s1: string;
begin
  s := '������ ' + IntToStr(row) + ' ';
  ErrInn := False;
  errKpp := False;
  SLRow := TStringList.Create;
  try
    if not ErrInn then
      if prod.pINN = '' then
      begin
        s1 := s + '��� ������������� �� ����� ���� ������';
        SLRow.Add(s1);
        ErrInn := True;
      end;
    if not ErrInn then
      if ((Length(prod.pINN) <> 10) and (Length(prod.pINN) <> 12)) then
      begin
        s1 := s + '��� ������������� �� ����� 10 ��� 12��������';
        SLRow.Add(s1);
        ErrInn := True;
      end;
    if not ErrInn then
      if ((prod.pKpp <> NullKpp8) and (prod.pKpp <> NullKpp9))  then
        if not InnCheck(String(prod.pINN), b1) then
        begin
          s1 := s + '�� ���������� ��� ������������� (���-�����)';
          if SLErrXML.IndexOf(s1) = -1 then
            SLRow.Add(s1);
          ErrInn := True;
        end;
    if not errKpp then
      if prod.pKPP = '' then
      begin
        s1 := s + '��� ������������� �� ����� ���� ������';
        SLRow.Add(s1);
        errKpp := True;
      end;
    if not errKpp then
      if Length(prod.pKPP) <> 9 then
      begin
        s1 := s + '��� ������������� �� ����� 10 ��������';
        SLRow.Add(s1);
        errKpp := True;
      end;
    if SLRow.Count > 0 then
      SLErrXML.AddStrings(SLRow);
  finally
    SLRow.free;
  end;
end;

procedure GetErrProduct_XML(var SLErrXML: TStringList; decl: TDeclaration;
  row: integer);
var
  s, s1: string;
  SLRow: TStringList;
begin
  SLRow := TStringList.Create;
  try
    s := '������ ' + IntToStr(row) + ' ';
    if decl.DeclNum = '' then
    begin
      s1 := s + '����������� ������������ ���� ����� ���������';
      SLRow.Add(s1);
    end;
    if decl.Amount = 0 then
    begin
      s1 := s +
        '�������� ���-�� ���������� �� ����� ���� ������ ��� ������ 0';
      SLRow.Add(s1);
    end;
    if decl.DeclDate = 0 then
    begin
      s1 := s + '�������� ������ ���� ���������';
      SLRow.Add(s1);
    end;

    if SLRow.Count > 0 then
      SLErrXML.AddStrings(SLRow);
  finally
    SLRow.free;
  end;
end;

procedure GetErrSaler_XML(var SLErr: TStringList; sal: TSaler;
  row: integer);
var
  ErrInn, errKpp, b1: Boolean;
  SLRow: TStringList;
  s, s1: string;
begin
  s := '������ ' + IntToStr(row) + ' ';
  ErrInn := False;
  errKpp := False;
  SLRow := TStringList.Create;
  try
    if not ErrInn then
      if sal.sINN = '' then
      begin
        s1 := s + '��� ���������� �� ����� ���� ������';
        SLRow.Add(s1);
        ErrInn := True;
      end;
    if not ErrInn then
      if Length(sal.sINN) <> 10 then
      begin
        s1 := s + '��� ���������� �� ����� 10 ��������';
        SLRow.Add(s1);
        ErrInn := True;
      end;
    if not ErrInn then
      if not InnCheck(String(sal.sINN), b1) then
      begin
        s1 := s + '�� ���������� ��� ���������� (���-�����)';
        if SLErrXML.IndexOf(s1) = -1 then
          SLRow.Add(s1);
        ErrInn := True;
      end;
    if not errKpp then
      if sal.sKpp = '' then
      begin
        s1 := s + '��� ���������� �� ����� ���� ������';
        SLRow.Add(s1);
        errKpp := True;
      end;
    if not errKpp then
      if Length(sal.sKpp) <> 9 then
      begin
        s1 := s + '��� ���������� �� ����� 10 ��������';
        SLRow.Add(s1);
        errKpp := True;
      end;
    if SLRow.Count > 0 then
      SLErrXML.AddStrings(SLRow);
  finally
    SLRow.free;
  end;
end;

// ������ ������������� ������������
function SetECP: Boolean;
var
  CrProInst, OldCrPro: Boolean;
  Reg: TRegistry;
  Key: string;
  // i: integer;
  // d: TDateTime;

begin
  Result := False;
  Reg := TRegistry.Create;
  try
    // ��������� ��������� ������-���
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    Key := 'SOFTWARE\Crypto Pro\';
    if Reg.KeyExists(Key) then
      CrProInst := True
    else
      CrProInst := False;
    Key := 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\';
    if Reg.KeyExists(Key + '{02CB7B88-4F11-4575-A1BB-103A40B83A4D}')
      or Reg.KeyExists(Key + '{247F4CC0-723C-40A5-9A38-E2E2C24DEB46}')
      or Reg.KeyExists(Key + '{54A08450-B343-40B0-924E-68F031450996}')
      then
      CrProInst := True
    else
      CrProInst := False;
    if Reg.KeyExists(Key + '{247F4CC0-723C-40A5-9A38-E2E2C24DEB46}')
      or Reg.KeyExists(Key + '{54A08450-B343-40B0-924E-68F031450996}') then
      OldCrPro := False
    else
      OldCrPro := True;

    // if not CrProInst then
    // Show_Inf('������ ������ ������������. �� ���������� ������-���', fError);

    Result := GetSetECP(OldCrPro);
  finally
    Reg.free;
  end;
end;

function ExistConnectServer: Boolean;
var
  s: string;
  TCP: TIdTCPClient;
begin
  TCP := TIdTCPClient.Create(nil);
  TCP.Host:=BMHost;
  TCP.Port:=TCPPort;
  Result := False;
//  try
    try
      TCP.Connect;
      s := TCP.IOHandler.ReadLn;
      if Copy(s, 1, 20) = 'Connected to server.' then
      begin
        TCP.IOHandler.WriteLn('EXEC' + fOrg.inn + ' ' + exe_version); // 3
        s := TCP.IOHandler.ReadLn;
        TCP.IOHandler.WriteLn('OK');
        if TCP.Socket <> nil then
        begin
          TCP.Socket.Close;
          TCP.Disconnect;
          Result := True;
        end
        else
          Result := False;
      end;
    finally
      TCP.Disconnect;
      TCP.Free;
    end;
//  except
//      Result := False;
//      raise ;
//  end;
end;

function InsertEanLocal(DBQ: TADOQuery): Boolean;
var
  qr: TADOQuery;
  s: AnsiString;
  Str: array [1 .. 15] of AnsiString;
  er: Boolean;
  StrSQL, PK: string;
  i:integer;
begin
  Result := False;
  for i := 0 to High(RefsAddEAN[0]) do
  if RefsAddEAN[0, i] <> '' then
  begin
    s := AnsiString(RefsAddEAN[1, i]);
    Str[1] := AnsiString(RefsAddEAN[2, i]); // ean13
    Str[2] := Copy(s, 0, 3); // FNSCode
    Str[3] := Copy(s, 4, Length(s) - 3); // Productname
    Str[4] := AnsiString(RefsAddEAN[3, i]); // AlcVol
    Str[5] := AnsiString(RefsAddEAN[4, i]);
    // Capacity
    Str[6] := AnsiString(RefsAddEAN[6, i]);
    // Prodkod
    Str[7] := AnsiString(RefsAddEAN[7, i]);
    // Prodcod
    Str[8] := AnsiString(RefsAddEAN[5, i]);
    // ProducerName

    Str[10] := Str[6];
    Str[11] := Str[7];
    Str[12] := '643';
    Str[13] := Copy(Str[6], 0, 2); // region code
    Str[14] := AnsiString(RefsAddEAN[8, i]);
    // addredd

    try
      qr := TADOQuery.Create(DBQ.Owner);
      qr.Connection := DBQ.Connection;
      qr.ParamCheck := False;

      PrepareText(s);
      qr.sql.Text := 'Select EAN13 From Production where EAN13=''' + String
        (Str[1]) + '''';
      qr.Open;
      if qr.RecordCount > 0 then
      begin
        if qr.RecordCount > 1 then
        begin
          qr.sql.Text := 'DELETE from Production WHERE EAN13=''' + String
            (Str[1]) + '''';
          qr.ExecSQL;
        end;
      end
      else
      begin
        StrSQL :=
          'INSERT INTO Production(EAN13, FNSCode, ProductName, AlcVol, Capacity, LocRef, SelfMade, prodkod, prodcod) ';
        StrSQL := StrSQL +
          'VALUES (:EAN13, :FNSCode, :ProductName, :AlcVol, :Capacity, :LocRef, :SelfMade, :prodkod, :prodcod)';
        qr.sql.Text := StrSQL;

        qr.Parameters.AddParameter.Name := 'EAN13';
        qr.Parameters.ParamByName('EAN13').DataType := ftString;

        qr.Parameters.AddParameter.Name := 'FNSCode';
        qr.Parameters.ParamByName('FNSCode').DataType := ftString;

        qr.Parameters.AddParameter.Name := 'ProductName';
        qr.Parameters.ParamByName('ProductName').DataType := ftString;

        qr.Parameters.AddParameter.Name := 'AlcVol';
        qr.Parameters.ParamByName('AlcVol').DataType := ftFloat;

        qr.Parameters.AddParameter.Name := 'Capacity';
        qr.Parameters.ParamByName('Capacity').DataType := ftFloat;

        qr.Parameters.AddParameter.Name := 'LocRef';
        qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;

        qr.Parameters.AddParameter.Name := 'SelfMade';
        qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

        qr.Parameters.AddParameter.Name := 'prodkod';
        qr.Parameters.ParamByName('prodkod').DataType := ftString;

        qr.Parameters.AddParameter.Name := 'prodcod';
        qr.Parameters.ParamByName('prodcod').DataType := ftString;

        qr.Parameters.ParamByName('EAN13').value := Str[1];
        qr.Parameters.ParamByName('FNSCode').value := Str[2];
        qr.Parameters.ParamByName('ProductName').value := Str[3];
        qr.Parameters.ParamByName('AlcVol').value := Str[4];
        qr.Parameters.ParamByName('Capacity').value := Str[5];
        qr.Parameters.ParamByName('LocRef').value := True;
        qr.Parameters.ParamByName('SelfMade').value := True;
        qr.Parameters.ParamByName('prodkod').value := Str[6];
        qr.Parameters.ParamByName('prodcod').value := Str[7];
        qr.sql.Text := StrSQL;
        qr.ExecSQL;

        qr.sql.Text := 'Select * From Producer where inn=''' + String(Str[6])
          + ''' and kpp=''' + String(Str[7]) + '''';
        qr.Open;
        if qr.RecordCount = 0 then
        begin
          qr.sql.Text := 'select MAX(PK) from producer';
          qr.Open;
          PK := IntToStr(qr.Fields[0].AsInteger + 1);
          qr.sql.Clear;
          qr.Parameters.Clear;

          Str[9] := AnsiString(PK);

          qr.ParamCheck := False;
          StrSQL :=
            'INSERT INTO producer(PK, FullName, INN, KPP, countrycode, RegionCode, Address, LocRef, SelfMade) ';
          StrSQL := StrSQL +
            'VALUES (:PK, :FullName, :INN, :KPP, :countrycode, :RegionCode, :Address, :LocRef, :SelfMade)';
          qr.sql.Text := StrSQL;

          qr.Parameters.AddParameter.Name := 'PK';
          qr.Parameters.ParamByName('PK').DataType := ftInteger;

          qr.Parameters.AddParameter.Name := 'FullName';
          qr.Parameters.ParamByName('FullName').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'INN';
          qr.Parameters.ParamByName('INN').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'KPP';
          qr.Parameters.ParamByName('KPP').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'countrycode';
          qr.Parameters.ParamByName('countrycode').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'RegionCode';
          qr.Parameters.ParamByName('RegionCode').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'Address';
          qr.Parameters.ParamByName('Address').DataType := ftString;

          qr.Parameters.AddParameter.Name := 'LocRef';
          qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;

          qr.Parameters.AddParameter.Name := 'SelfMade';
          qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

          qr.Parameters.ParamByName('PK').value := Str[9];
          qr.Parameters.ParamByName('FullName').value := Str[8];
          qr.Parameters.ParamByName('INN').value := Str[6];
          qr.Parameters.ParamByName('KPP').value := Str[7];
          qr.Parameters.ParamByName('countrycode').value := Str[12];
          qr.Parameters.ParamByName('RegionCode').value := Str[13];
          qr.Parameters.ParamByName('Address').value := Str[14];
          qr.Parameters.ParamByName('LocRef').value := True;
          qr.Parameters.ParamByName('SelfMade').value := True;

          qr.sql.Text := StrSQL;
          qr.ExecSQL;

          qr.sql.Text := 'UPDATE Production Set ProducerCode=''' + String
            (Str[9]) + ''' WHERE EAN13=''' + String(Str[1]) + '''';
          qr.ExecSQL;
        end
        else
        begin
          PK := qr.FieldByName('pk').AsString;
          Str[9] := AnsiString(PK);
          qr.sql.Text := 'UPDATE Production Set ProducerCode=''' + String
            (Str[9]) + ''' WHERE EAN13=''' + String(Str[1]) + '''';
          qr.ExecSQL;
        end;
        ShowRefProduction(fmRef.SG_Product, fmWork.que2_sql);
        Result := True;
      end;
    except
      er := True;
      Result := False;
    end;
  end;
end;

function InsertInnLocal(DBQ: TADOQuery): Boolean;
var
  qr: TADOQuery;
  // s: AnsiString;
  Str: array [1 .. 15] of AnsiString;
  // er: Boolean;
  StrSQL, PK: string;
begin
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  qr.ParamCheck := False;

  Str[1] := AnsiString(RefsAddINN[0, Length(RefsAddINN[0]) - 1]);
  // nameorg
  Str[2] := AnsiString(RefsAddINN[2, Length(RefsAddINN[0]) - 1]);
  // le1_Inn.Text;
  Str[3] := AnsiString(RefsAddINN[3, Length(RefsAddINN[0]) - 1]);
  // le1_KPP.Text;
  Str[4] := AnsiString(RefsAddINN[4, Length(RefsAddINN[0]) - 1]);
  // le1_adress.Text;
  Str[5] := AnsiString(RefsAddINN[5, Length(RefsAddINN[0]) - 1]);
  // le1_adress.Text;
  Str[6] := AnsiString(RefsAddINN[6, Length(RefsAddINN[0]) - 1]);
  // le1_Lic.Text;
  Str[7] := AnsiString(RefsAddINN[7, Length(RefsAddINN[0]) - 1]);
  // le1_num.Text;
  Str[8] := AnsiString(RefsAddINN[8, Length(RefsAddINN[0]) - 1]);
  // dtp1_beg.Datetime
  Str[9] := AnsiString(RefsAddINN[9, Length(RefsAddINN[0]) - 1]);
  // dtp1_end.Datetime
  Str[10] := AnsiString(RefsAddINN[10, Length(RefsAddINN[0]) - 1]);
  // le1_org.Text;

  qr.sql.Text := 'Select OrgINN From Saler where OrgINN=''' + String
    (Str[2]) + ''' and OrgKPP=''' + String(Str[3]) + '''';
  qr.Open;
  if qr.RecordCount = 0 then
  begin
    qr.sql.Text := 'select MAX(PK) from Saler';
    qr.Open;
    PK := IntToStr(qr.Fields[0].AsInteger + 1);
    qr.sql.Clear;
    qr.Parameters.Clear;

    StrSQL :=
      'INSERT INTO Saler(PK,OrgName,OrgINN,OrgKPP,RegionCode,Address,LocRef,SelfMade) ';
    StrSQL := StrSQL +
      'VALUES (:PK,:OrgName,:OrgINN,:OrgKPP,:RegionCode,:Address,:LocRef,:SelfMade)';
    qr.sql.Text := StrSQL;

    qr.Parameters.AddParameter.Name := 'PK';
    qr.Parameters.ParamByName('PK').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'OrgName';
    qr.Parameters.ParamByName('OrgName').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'OrgINN';
    qr.Parameters.ParamByName('OrgINN').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'OrgKPP';
    qr.Parameters.ParamByName('OrgKPP').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'RegionCode';
    qr.Parameters.ParamByName('RegionCode').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'Address';
    qr.Parameters.ParamByName('Address').DataType := ftString;

    qr.Parameters.AddParameter.Name := 'LocRef';
    qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;

    qr.Parameters.AddParameter.Name := 'SelfMade';
    qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;

    qr.Parameters.ParamByName('PK').value := PK;
    qr.Parameters.ParamByName('OrgName').value := Str[1];
    qr.Parameters.ParamByName('OrgINN').value := Str[2];
    qr.Parameters.ParamByName('OrgKPP').value := Str[3];
    qr.Parameters.ParamByName('RegionCode').value := Copy(Str[2], 0, 2);
    qr.Parameters.ParamByName('Address').value := Str[5];
    qr.Parameters.ParamByName('LocRef').value := True;
    qr.Parameters.ParamByName('SelfMade').value := True;

    qr.ExecSQL;

    qr.sql.Text := 'Select * From Saler_license where saler_pk=''' + String
      (Str[2]) + ''' and saler_kpp = ''' + String(Str[3]) + '''';
    qr.Open;
    PK := qr.FieldByName('pk').AsString;
    qr.Open;
    if qr.RecordCount > 0 then
    begin
      qr.sql.Text := 'UPDATE Saler_license ' + #13#10 + 'Set PK=''' + PK +
        ''', saler_pk=''' + String(Str[2]) + ''',saler_kpp=''' + String
        (Str[3]) + ''', ser=''' + String(Str[6]) + ''', num=''' + String
        (Str[7]) + ''', date_start=''' + String(Str[8])
        + ''', date_finish=''' + String(Str[9]) + ''', regorg=''' + String
        (Str[10]) + '''' + #13#10 + 'WHERE pk=''' + PK + '''';
      qr.ExecSQL;
    end
    else
    begin
      qr.sql.Text := 'select MAX(PK) from Saler_license';
      qr.Open;
      PK := IntToStr(qr.Fields[0].AsInteger + 1);
      qr.sql.Clear;
      qr.sql.Text :=
        'INSERT INTO Saler_license(PK,saler_pk, ser, num, date_start, ' +
        'date_finish,regorg,saler_kpp)' + #13#10 + 'Values(''' + PK +
        ''',''' + String(Str[2]) + ''', ''' + String(Str[6])
        + ''', ''' + String(Str[7]) + ''', ''' + String(Str[8])
        + ''', ''' + String(Str[9]) + ''', ''' + Copy
        (String(Str[2]), 0, 2) + ''',''' + String(Str[3]) + ''')';
      qr.ExecSQL;
    end;
    Result := True;
  end;

end;

function EditLicLocal(DBQ: TADOQuery): Boolean;
var
  qr: TADOQuery;
  PK: string;
  sal: TSaler;
  upd: Boolean;
begin
  upd := False;
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  qr.ParamCheck := False;

  sal.sName := ShortString(RefsAddINN[0, Length(RefsAddINN[0]) - 1]);
  // nameorg
  sal.sINN := ShortString(RefsAddINN[2, Length(RefsAddINN[0]) - 1]);
  // le1_Inn.Text;
  sal.sKpp := ShortString(RefsAddINN[3, Length(RefsAddINN[0]) - 1]);
  // le1_KPP.Text;
  sal.sAddress := ShortString(RefsAddINN[4, Length(RefsAddINN[0]) - 1]);
  // le1_adress.Text;
  sal.lic.ser := ShortString(RefsAddINN[6, Length(RefsAddINN[0]) - 1]);
  // le1_Lic.Text;
  sal.lic.num := ShortString(RefsAddINN[7, Length(RefsAddINN[0]) - 1]);
  // le1_num.Text;
  sal.lic.dBeg := StrToDate(RefsAddINN[8, Length(RefsAddINN[0]) - 1]);
  // dtp1_beg.Datetime
  sal.lic.dEnd := StrToDate(RefsAddINN[9, Length(RefsAddINN[0]) - 1]);
  // dtp1_end.Datetime
  sal.lic.RegOrg := ShortString(RefsAddINN[10, Length(RefsAddINN[0]) - 1]);
  // le1_org.Text;

  try
    qr.sql.Text := 'Select * From Saler_license where saler_pk=''' + String
      (sal.sINN) + ''' and saler_kpp = ''' + String(sal.sKpp) + '''';
    qr.Open;
    PK := qr.FieldByName('pk').AsString;

    qr.Open;
    if qr.RecordCount > 0 then
    begin
      sal.lic.lId := ShortString(PK);
      qr.sql.Text := 'UPDATE Saler_license ' + #13#10 + 'Set PK=''' + PK +
        ''', saler_pk=''' + String(sal.sINN) + ''',saler_kpp=''' + String
        (sal.sKpp) + ''', ser=''' + String(sal.lic.ser)
        + ''', num=''' + String(sal.lic.num)
        + ''', date_start=''' + DateToStr(sal.lic.dBeg)
        + ''', date_finish=''' + DateToStr(sal.lic.dEnd)
        + ''', regorg=''' + String(sal.lic.RegOrg)
        + '''' + #13#10 + 'WHERE pk=''' + PK + '''';
      qr.ExecSQL;
      upd := UpdateLockRefLic(sal, fEditEdit);
    end
    else
    begin
      qr.sql.Text := 'select MAX(PK) from Saler_license';
      qr.Open;
      PK := IntToStr(qr.Fields[0].AsInteger + 1);
      sal.lic.lId := ShortString(PK);
      qr.sql.Clear;
      qr.sql.Text :=
        'INSERT INTO Saler_license(PK,saler_pk, ser, num, date_start, date_finish,regorg,saler_kpp)' + #13#10 + 'Values(''' + PK + ''',''' + String(sal.sINN) + ''', ''' + String(sal.lic.ser) + ''', ''' + String(sal.lic.num) + ''', ''' + DateToStr(sal.lic.dBeg) + ''', ''' + DateToStr(sal.lic.dEnd) + ''', ''' + String(sal.lic.RegOrg) + ''',''' + String(sal.sKpp) + ''')';
      qr.ExecSQL;
      upd := UpdateLockRefLic(sal, fNewEdit);
    end;
    UpdateSalerLicDecl(sal);
    Result := True;
  finally
    qr.free;
  end;
end;

function UpdateLockRefLic(sal: TSaler; fEdit: TTypeEdit): Boolean;
var
  idx: integer;
  SLRow: TStringList;
begin
  Result := False;
  idx := -1;
  case fEdit of
    fEditEdit:
      begin
        idx := RefLic.IndexOf(String(sal.lic.lId));
        SLRow := Pointer(RefLic.Objects[idx]);
      end;
    fNewEdit:
      begin
        SLRow := TStringList.Create;
        RefLic.AddObject(String(sal.lic.lId), SLRow);
      end;
  end;
  if idx <> -1 then
  begin
    SLRow.Add(String(sal.lic.lId));
    SLRow.Add(String(sal.lic.ser));
    SLRow.Add(String(sal.lic.num));
    SLRow.Add(DateToStr(sal.lic.dBeg));
    SLRow.Add(DateToStr(sal.lic.dEnd));
    SLRow.Add(String(sal.lic.RegOrg));
    SLRow.Add(String(sal.sINN));
    SLRow.Add(String(sal.sKpp));
    Result := True;
  end;
end;

function UpdateSalerLicDecl(sal: TSaler): Boolean;
var
  i: integer;
  decl: TDecl_obj;
begin
  Result := False;
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    decl := Pointer(SLAllDecl.Objects[i]);
    if (decl.data.sal.sINN = sal.sINN) and (decl.data.sal.sKpp = sal.sKpp)
      then
    begin
      decl.data.sal.lic := sal.lic;
      Result := True;
    end;
  end;
end;

function AutoAddRashod(Table: TADVStringGrid): integer;
var
  i, row: integer;
  mov: TMove;
  SLEanSG: TStringList;

  SLAutoRash: TStringList;
  y,m,d:word;
  dat:TDateTime;
begin
  Result := 0;
  SLAutoRash := TStringList.Create;
  SLEanSG := TStringList.Create;
  try
    Table.Visible := False;
    row := Table.RowCount - Table.FixedFooters;
    Table.RowCount := Table.RowCount + SLOstatkiKpp.Count;
    for i := 0 to SLOstatkiKpp.Count - 1 do
    begin
      mov := Pointer(SLOstatkiKpp.Objects[i]);
      Table.RowEnabled[row] := True;
      Table.RowColor[row] := clInsert;
      Table.Cells[2, row] := String(mov.product.FNSCode);
      Table.Cells[3, row] := String(mov.SelfKPP);

      DecodeDate(Now,y,m,d);
      dat := DateOf(Kvart_End(CurKvart, NowYear));

      if (DateOf(fmMove.dtp_WorkDate.Date) <= dEnd) and
        (DateOf(fmMove.dtp_WorkDate.Date) >= dBeg) then
        CurSG.Cells[4, Row] := DateToStr(fmMove.dtp_WorkDate.Date)
      else
        CurSG.Cells[4, Row] := DateToStr(dEnd);

      Table.Cells[5, row] := TimeToStr(Time);

      PrihProcOst(mov.product, mov.product.capacity,
        mov.dv.rash, mov.dv.ost1, mov.dv_dal.rash, mov.dv_dal.ost1);

      Table.Cells[6, row] := String(mov.product.EAN13);
      Table.Cells[7, row] := String(mov.product.ProductNameF);
      Table.Cells[8, row] := FloatToStrF(mov.dv.rash, ffFixed, maxint, ZnFld);
      Table.Floats[9, row] := 0.1 * mov.dv.rash * mov.product.Capacity;

      Table.Cells[11, row] := String(mov.product.prod.pINN);
      Table.Cells[12, row] := String(mov.product.prod.pKPP);
      Table.Cells[17, row] := '';
      if i <> SLOstatkiKpp.Count - 1 then
        inc(row);
    end;
    Table.Visible := True;
    PropCellsSG(arNormal, Table, CalcColRashNew, NoResColRashNew, HideColRashNew);
    Result := SLOstatkiKpp.Count;

  finally
    SLEanSG.free;
    SLAutoRash.free;
  end;
end;

function DeleteRash: integer;
var
  i, c, idx: integer;
  declPK: string;
  find: Boolean;
begin
  c := 0;
  try
    try
      SortSLCol(1, SLDataRash);
      SortSLCol(1, SLAllDecl);
      for i := 0 to SLDataRash.Count - 1 do
      begin
        declPK := SLDataRash.Strings[i];
        if not NoValue(declPK) then
        begin

          find := SLAllDecl.find(declPK, idx);
          if find then
          begin
            DelDecl.Add(declPK); // ��������� ���� � ������ ����� ��� ��������
            if SLAllDecl.Objects[idx] <> nil then
            begin
              SLAllDecl.Objects[idx].free;
              // SLAllDecl.Objects[idx] := nil;
            end;
            SLAllDecl.Delete(idx);
            inc(c);
          end;
        end;
      end;
      SLDataRash.Clear;
      CurSG.ClearNormalRows(CurSG.FixedRows,
        CurSG.RowCount - CurSG.FixedRows - CurSG.FixedFooters);
    except
      Result := 0;
    end;
  finally
    Result := c;
  end;
end;

function AutoNewRashod(Table: TADVStringGrid): integer;
var
  i: integer;
  mov: TMove;
  // prodObj: TProductionObj;
begin
  Result := 0;
  Table.SortIndexes.Clear;
  Table.IgnoreColumns.Clear;
  Table.ClearNormalCells;
  Table.ClearComboString;
  Table.DetailPickerDropDown.Items.Clear;
  Table.LookupItems.Clear;
  Table.Filter.Clear;
  Table.UniCombo.Items.Clear;
  Table.RowCount := Table.FixedRows + Table.FixedFooters +
    SLOstatkiKpp.Count;
  for i := 0 to SLOstatkiKpp.Count - 1 do
  begin
    mov := Pointer(SLOstatkiKpp.Objects[i]);
    if mov <> nil then
    begin
      Table.Cells[1, i + Table.FixedRows] := String(mov.product.FNSCode);
      Table.Cells[2, i + Table.FixedRows] := String(mov.SelfKPP);
      Table.Cells[3, i + Table.FixedRows] := DateToStr(dEnd);
      Table.Cells[4, i + Table.FixedRows] := String(mov.product.EAN13);
      Table.Cells[5, i + Table.FixedRows] := String
        (mov.product.ProductNameF);

      PrihProcOst(mov.product, mov.product.capacity,
        mov.dv.rash, mov.dv.ost1, mov.dv_dal.rash, mov.dv_dal.ost1);

      Table.Cells[6, i + Table.FixedRows] := FloatToStrF(mov.dv.ost0,
        ffFixed, maxint, ZnFld);
      Table.Cells[7, i + Table.FixedRows] := FloatToStrF(mov.dv.prih,
        ffFixed, maxint, ZnFld);
      Table.Cells[8, i + Table.FixedRows] := FloatToStrF(mov.dv.oder,
        ffFixed, maxint, ZnFld);
      Table.Cells[9, i + Table.FixedRows] := FloatToStrF(mov.dv.rash,
        ffFixed, maxint, ZnFld);
      Table.Cells[10, i + Table.FixedRows] := FloatToStrF(mov.dv.ost1,
        ffFixed, maxint, ZnFld);
      Table.Cells[11, i + Table.FixedRows] := FloatToStrF(mov.dv_dal.rash,
        ffFixed, maxint, ZnFld);
      Table.Cells[12, i + Table.FixedRows] := FloatToStrF(mov.dv_dal.ost1,
        ffFixed, maxint, ZnFld);
      Table.Cells[13, i + Table.FixedRows] := String
        (mov.product.prod.pName);
      Table.Cells[14, i + Table.FixedRows] := String(mov.product.prod.pINN);
      Table.Cells[15, i + Table.FixedRows] := String(mov.product.prod.pKPP);
      Table.Cells[16, i + Table.FixedRows] := '';
      Table.RowColor[i + Table.FixedRows] := clInsert;
      inc(Result);
    end;
  end;

  PropCellsSG(arNormal, Table, CalcColRashNew, NoResColRashNew, HideColRashNew);

end;

function InDecl(grup: string; form11: Boolean): Boolean;
begin
  InDecl := False;
  if (grup = '261') or (grup = '262') or (grup = '263') then
  begin
    if form11 then
      InDecl := False
    else
      InDecl := True;
    Exit;
  end;

  if not form11 and ((grup = '261') or (grup = '262') or (grup = '263')) then
  begin
    InDecl := True;
    exit;
  end;
  if form11 and ((grup > '000') and (grup < '500')) then
    InDecl := True;
  if not form11 and ((grup >= '500') and (grup <= '999')) then
    InDecl := True;
end;

function ValidOrgname(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) > 3 then
    rez := True
  else
    rez := False;
  ValidOrgname := rez;
end;

function InnUL(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) = 10 then
    rez := True
  else
    rez := False;
  InnUL := rez;
end;

function UseStr(s, error, use: string): string;
begin
  if (error = '') and Empty(s) then
    s := use
  else if pos(error, s) > 0 then
    s := use;
  UseStr := s;
end;

function GetKPPAdres(DBQ: TADOQuery; kpp: string;
  var adr: TOrgAdres): string;
var
  sql: TADOQuery;
begin
  GetKPPAdres := '';

  with adr do
  begin
    kpp := '';
    name := '';
    kod_str := '';
    postindex := '';
    kod_reg := '';
    raion := '';
    gorod := '';
    naspunkt := '';
    ulica := '';
    dom := '';
    korpus := '';
    litera := '';
    kvart := '';
  end;
  sql := TADOQuery.Create(DBQ.Owner);
  try
    try

      sql.Connection := DBQ.Connection;
      sql.ParamCheck := False;
      sql.sql.Text := 'SELECT top 1 * ' + #13#10 + 'FROM self_adres' +
        #13#10 + 'WHERE KPP=''' + kpp + ''' order by KPP';
      sql.Open;
      if not sql.Eof then
        with adr do
        begin
          sql.First;
          kpp := ShortString(sql.FieldByName('KPP').AsString);
          name := sql.FieldByName('namename').AsString;
          kod_str := ShortString(sql.FieldByName('Countrycode').AsString);
          postindex := ShortString(sql.FieldByName('IndexP').AsString);
          kod_reg := ShortString(sql.FieldByName('Codereg').AsString);
          raion := sql.FieldByName('Raipo').AsString;
          gorod := sql.FieldByName('City').AsString;
          naspunkt := sql.FieldByName('NasPunkt').AsString;
          ulica := sql.FieldByName('Street').AsString;
          dom := ShortString(sql.FieldByName('House').AsString);
          korpus := ShortString(sql.FieldByName('Struct').AsString);
          litera := ShortString(sql.FieldByName('ABC').AsString);
          kvart := ShortString(sql.FieldByName('Quart').AsString);
        end;
      GetKPPAdres := sql.FieldByName('namename').AsString;
    except
      GetKPPAdres := 'Null';
    end;
  finally
    sql.free;
  end;
end;

function MinusMove(dv: TMove): Boolean;
begin
  Result := False;
  if dv.dv.ost0 < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.prih < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.rash < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.vozv_post < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.vozv_pokup < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.brak < 0 then
  begin
    Result := True;
    exit;
  end;
  if dv.dv.ost1 < 0 then
  begin
    Result := True;
    exit;
  end;
end;

procedure SHapkaXML(node: IXMLNode; kv, yr, kor: integer;
  f11, kor1, f43: Boolean);
var
  s: string;
begin
  with node do
  begin
    Attributes['�������'] := DateToStr(Date());
    if f43 then
      Attributes['��������'] := '4.31'
    else
      Attributes['��������'] := '4.20';
    Attributes['��������'] := '���������� ������ ' + exe_version;
    with AddChild('��������') do
    begin
      if f11 then
        s := '37'
      else
        s := '38';
      if not f43 then
        s := s + '-�';

      Attributes['�������'] := s;
      Attributes['�������������'] := IntToStr(Period_Otch(kv));
      if not f43 then
        Attributes['��������'] := '4';
      Attributes['������������'] := IntToStr(yr);

     {
       Attributes['xsi:noNamespaceSchemaLocation'] := 'ALK_11.xsd';
       Attributes['xmlns:xsi'] :=
        'http://www.w3.org/2001/XMLSchema-instance';
      }

      if not kor1 then
        ChildValues['���������'] := ''
      else
        with AddChild('��������������') do
          Attributes['���������'] := IntToStr(kor);
    end;
  end;
end;


//procedure SHapkaXML(node: IXMLNode; kv, yr, kor: integer;
//  f11, kor1, f43: Boolean);
//var
//  s: string;
//begin
//  with node do
//  begin
//    Attributes['�������'] := DateToStr(Date());
//    if f43 then
//      Attributes['��������'] := '4.31'
//    else
//      Attributes['��������'] := '4.20';
//    Attributes['��������'] := '���������� ������ ' + exe_version;
//    with AddChild('��������') do
//    begin
//      if f11 then
//        s := '11'
//      else
//        s := '12';
//      if not f43 then
//        s := s + '-�';
//
//      Attributes['�������'] := s;
//      Attributes['�������������'] := IntToStr(Period_Otch(kv));
//      if not f43 then
//        Attributes['��������'] := '4';
//      Attributes['������������'] := IntToStr(yr);
//      Attributes['xsi:noNamespaceSchemaLocation'] := 'ALK_11.xsd';
//      Attributes['xmlns:xsi'] :=
//        'http://www.w3.org/2001/XMLSchema-instance';
//      if not kor1 then
//        ChildValues['���������'] := ''
//      else
//        with AddChild('��������������') do
//          Attributes['���������'] := IntToStr(kor);
//    end;
//  end;
//end;

function WriteXML(fn: string): Boolean;
var
  CertContext: PCCERT_CONTEXT;
begin
  Result := False;
  if fSert.path = '' then
    exit;
  if GetSert(fSert, CertContext) then
    if AddECP(fn, CertContext) then
      if ZipFileXML(fn) then
        if CryptoXML(fn) then
        begin
          DeleteFile(PWideChar(fn + '.sig'));
          DeleteFile(PWideChar(fn + '.sig.zip'));
          Result := True;
        end;
end;

function GetSert(fSert: TSert; var CertContext: PCCERT_CONTEXT): Boolean;
var
  hCert_Store: HCERTSTORE;
  hProv: HCRYPTPROV;
  hKeySig: HCRYPTKEY;
  PAC: PAnsiChar;
  PACs: AnsiString;
  CertLen, pubKeyInfoLen: DWORD;
  L: PWideChar;
  pubKeyInfo: PCERT_PUBLIC_KEY_INFO;
  find, bol: Boolean;
  Cert: PByte;
  er: Int64;
begin
  Result := False;
  PACs := AnsiString(fSert.path);
  PAC := Addr(PACs[1]);
  CryptAcquireContextA(@hProv, PAC, GOST_34, 75, CRYPT_SILENT);
  CryptGetUserKey(hProv, AT_KEYEXCHANGE, @hKeySig);
  // �������� ������ �� �������� ����
  CryptGetKeyParam(hKeySig, KP_CERTIFICATE, nil, @CertLen, 0);
  CertContext := nil;
  pubKeyInfo := nil;
  if CertContext = nil then
  begin
    L := PWideChar(WideString('MY'));
    hCert_Store := CertOpenStore(CERT_STORE_PROV_SYSTEM, encType, hProv,
      CERT_SYSTEM_STORE_CURRENT_USER, L); // ��������� ���������
    find := False;
    CertContext := CertEnumCertificatesInStore(hCert_Store, CertContext);
    if CertContext <> nil then
    begin
      CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType, nil,
        @pubKeyInfoLen);
      er := GetLastError;
      if pubKeyInfoLen > 0 then
      begin
        Screen.Cursor := crHourGlass;
        GetMem(pubKeyInfo, pubKeyInfoLen);
        CryptExportPublicKeyInfo(hProv, AT_KEYEXCHANGE, encType,
          pubKeyInfo, @pubKeyInfoLen);
        Screen.Cursor := crDefault;
      end; // if pubKeyInfoLen>0
    end; // if CertContext=nil
    bol := False;
    while (CertContext <> nil) and (not find) do
    begin
      if bol then
        CertContext := CertEnumCertificatesInStore(hCert_Store,
          CertContext);
      if (pubKeyInfoLen > 0) and (CertContext <> nil) then
        if CertComparePublicKeyInfo(CertContext.dwCertEncodingType,
          pubKeyInfo, @CertContext.pCertInfo.SubjectPublicKeyInfo) then
          find := True;
      bol := True;
    end; // while (CertContext<>nil)
  end; // if CertContext=nil
  FreeMem(pubKeyInfo, pubKeyInfoLen);
  Result := True;
  if CertContext = nil then
    if CertLen > 0 then
    begin
      GetMem(Cert, CertLen);
      CryptGetKeyParam(hKeySig, KP_CERTIFICATE, Cert, @CertLen, 0);
      CertContext := CertCreateCertificateContext(encType, Cert, CertLen);
      // ������� �������� �����������
      if CertContext <> nil then
      begin
        // Gr[ComboBox1.ItemIndex+1, 4]:='cont';
        Result := True;
      end;
      FreeMem(Cert, CertLen); // ���������� ������
    end;
end;

// ���������� ���������� �������� �������
function AddECP(fXML: string; CertContext: PCCERT_CONTEXT): Boolean;
var
  ms, ms1: TMemoryStream;
  er: Boolean;
  s: string;
begin
  Result := False;
  try
    try
      ms := TMemoryStream.Create;
      ms1 := TMemoryStream.Create;
      // ����� ����� XML
      ms.Clear;
      ms.LoadFromFile(fXML);

      SignXNL(ms, ms1, er, fSert.path, CertContext);
      if not er then
      begin
        s := fXML + '.sig';
        if FileExists(s) then
          DeleteFile(PWideChar(s));
        ms1.SaveToFile(s);
        if not er then // ShowMessage('������� ����� ��������� �������');
          Result := True;
      end;
    except

    end;
  finally
    ms1.free;
    ms.free;
  end;
end;

function ZipFileXML(fn: string): Boolean;
var
  Zip: TFWZipWriter;
  // s: TStringStream;
  f: string;
  procedure CheckResult(value: integer);
  begin
    if value < 0 then
      raise Exception.Create('������ ���������� ������');
  end;

begin
  Result := False;
  try
    Zip := TFWZipWriter.Create;
    try
      CheckResult(Zip.AddFile(fn + '.sig'));
      f := fn + '.sig.zip';
      if FileExists(f) then
        DeleteFile(PWideChar(f));

      Zip.BuildZip(f);
      // ShowMessage('������������� ����� ��������� �������');
      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;

function CryptoXML(fn: string): Boolean;
var
  ms, ms1: TMemoryStream;
  er: Boolean;
  f: string;
  r1, r2: TResourceStream;
  ACert: array of PCCERT_CONTEXT;
begin
  r1 := TResourceStream.Create(HInstance, 'res_59', RT_RCDATA);
  r2 := TResourceStream.Create(HInstance, 'res_fsrar', RT_RCDATA);
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  er := False;
  try
    try

      SetLength(ACert, 2);

      ms.LoadFromStream(r1);
      ACert[0] := CertCreateCertificateContext(encType, ms.Memory, ms.Size);
      ms.Clear;

      ms.LoadFromStream(r2);
      ACert[1] := CertCreateCertificateContext(encType, ms.Memory, ms.Size);

      ms.Clear;
      ms.LoadFromFile(fn + '.sig.zip');

      Crypt(2, ACert, ms, ms1, er);

      if not er then
      begin
        f := fn + '.sig.zip.enc';
        if FileExists(f) then
          DeleteFile(PWideChar(f));
        ms1.SaveToFile(f);

        // ShowMessage('���������� ����� ��������� �������');
      end;
    except
    end;
  finally
    FreeAndNil(ms1);
    FreeAndNil(ms);
    r1.free;
    r2.free;
  end;
end;

procedure SignXNL(Var ms, ms1: TMemoryStream; Var err: Boolean;
  ECP: string; CertContext: PCCERT_CONTEXT);
var
  SigParams: CRYPT_SIGN_MESSAGE_PARA;
  hProv: HCRYPTPROV;
  hKeySig: HCRYPTKEY;
  provInfoSig: CRYPT_KEY_PROV_INFO;

  dwNameLength, dwProvLength, pdw: DWORD;
  Data_Array_pointer: pacarPByte;
  Size_Array_pointer: pacarDWORD;
  pbSignedBlob: PByte;
  pcbSignedBlob: DWORD;
  n: Int64;

  // dwName: PByte; // ������ �����������
  // ContFull, CertPlace: string;

  PAC: PAnsiChar;
  PACs: AnsiString;
begin
  ms1.Clear;
  ms.Position := 0;
  err := False;
  PACs := AnsiString(ECP);
  PAC := Addr(PACs[1]);
  CryptAcquireContextA(@hProv, PAC, GOST_34, 75, 0);
  CryptGetUserKey(hProv, AT_KEYEXCHANGE, @hKeySig);
  // �������� ������ �� �������� ����
  try
    try
      if CertContext <> nil then
      begin
        CryptGetProvParam(hProv, PP_CONTAINER, nil, @dwNameLength, 0);
        // �������� ���������� ��� ����������
        provInfoSig.pwszContainerName := AllocMem(dwNameLength);
        CryptGetProvParam(hProv, PP_CONTAINER,
          PByte(provInfoSig.pwszContainerName), @dwNameLength, 0);

        CryptGetProvParam(hProv, PP_NAME, nil, @dwProvLength, 0);
        // �������� ��� ����������������
        provInfoSig.pwszProvName := AllocMem(dwProvLength);
        CryptGetProvParam(hProv, PP_NAME, PByte(provInfoSig.pwszProvName),
          @dwProvLength, 0);

        provInfoSig.dwFlags := 0; // ������������� �����
        provInfoSig.dwKeySpec := AT_KEYEXCHANGE; // AT_SIGNATURE;
        pdw := SizeOf(provInfoSig.dwProvType); // �������� ��� ����������
        CryptGetProvParam(hProv, PP_PROVTYPE, @provInfoSig.dwProvType,
          @pdw, 0);
        provInfoSig.cProvParam := 0;
        provInfoSig.rgProvParam := nil;

        FillChar(SigParams, SizeOf(CRYPT_SIGN_MESSAGE_PARA), #0);

        // �������������� ��������� �������
        SigParams.cbSize := SizeOf(CRYPT_SIGN_MESSAGE_PARA);
        SigParams.dwMsgEncodingType := encType;
        SigParams.pSigningCert := CertContext;
        // SigParams.HashAlgorithm.pszObjId := szOID_CP_GOST_R3411;  //("1.2.643.2.2.9")
        SigParams.HashAlgorithm.pszObjId :=
          CertContext.pCertInfo.SignatureAlgorithm.pszObjId;
        SigParams.HashAlgorithm.Parameters.cbData := 0;
        SigParams.pvHashAuxInfo := nil;
        SigParams.cMsgCert := 1; // �������� ���������� � ��������� - 1
        SigParams.rgpMsgCert := @CertContext;
        SigParams.cMsgCrl := 0; // �������� CRL context
        SigParams.rgpMsgCrl := nil;
        SigParams.cAuthAttr := 0;
        SigParams.rgAuthAttr := nil;
        SigParams.cUnauthAttr := 0;
        SigParams.rgUnauthAttr := nil;
        SigParams.dwFlags := 0;
        SigParams.dwInnerContentType := 0;

        SetLength(Data_Array_pointer, 1);
        SetLength(Size_Array_pointer, 1);
        // ����������� ���� ��� �������
        Size_Array_pointer[0] := ms.Size; // FileSize(f1);
        GetMem(Data_Array_pointer[0], Size_Array_pointer[0]);
        ms.ReadBuffer(Data_Array_pointer[0]^, Size_Array_pointer[0]);

        if not CryptSignMessage(@SigParams, False, 1,
          @Data_Array_pointer[0], @Size_Array_pointer[0], nil,
          @pcbSignedBlob) then
        begin
          n := GetLastError;
          if (n = -2148081675) or (n = 2148081675) then
            Show_Inf(
              '�� ������� ��������� ���������. ���������� ���������� ' +
                #13#10 +
                '��� ���������� ��������� ����� � ��������� "������" �' +
                #13#10 +
                '��������� � ��������� ���������� � �������� ������.' +
                #13#10 + '(CryptSignMessage ' + IntToStr(n) + ')', fError)
          else
            Show_inf('�� ������� ��������� ���������' + #13#10 +
                '(CryptSignMessage, ' + IntToStr(n) + ')', fError);
          err := True;
          exit;
        end;
        GetMem(pbSignedBlob, pcbSignedBlob);
        if not CryptSignMessage(@SigParams, False, 1, Data_Array_pointer,
          Size_Array_pointer, pbSignedBlob, @pcbSignedBlob) then
        begin
          n := GetLastError;
          if (n = -2148081675) or (n = 2148081675) then
            show_inf(
              '�� ������� ��������� ���������. ���������� ���������� ' +
                #13#10 +
                '��� ���������� ��������� ����� � ��������� "������" �' +
                #13#10 +
                '��������� � ��������� ���������� � �������� ������.' +
                #13#10 + '(CryptSignMessage ' + IntToStr(n) + ')', fError)
          else
            show_inf('�� ������� ��������� ���������' + #13#10 +
                '(CryptSignMessage, ' + IntToStr(n) + ')', fError);
          err := True;
          exit;
        end;

        ms1.WriteBuffer(pbSignedBlob^, pcbSignedBlob);

        FreeMem(pbSignedBlob);

        CryptDestroyKey(hProv);

        CryptReleaseContext(hProv, 0);

      end
      else
      begin
        ms1.LoadFromStream(ms);
        err := True;
        Show_Inf('������ ������� �������� �������!' + #13#10 +
            '�� ������ ������ ����������', fError);
      end;
    except
      ms1.LoadFromStream(ms);
      err := True;
      Show_inf('������ ������� �������� �������!', fError);
    end;
  finally
    FreeMem(provInfoSig.pwszContainerName);
    FreeMem(provInfoSig.pwszProvName);
    FreeMem(Data_Array_pointer[0]);
  end;
end;

procedure Crypt(CountS: integer; ArrayCert: array of PCCERT_CONTEXT;
  Var ms, ms1: TMemoryStream; Var err: Boolean); { ���� "�����������" }
var
  // Store: HCERTSTORE;
  EncParams: CRYPT_ENCRYPT_MESSAGE_PARA;
  cbEncrypted: DWORD;
  i: integer;
  hProv: HCRYPTPROV;
begin
  with ms do
    try
      hProv := 0;
      CryptAcquireContextW(@hProv, Nil, GOST_34, 75, CRYPT_VERIFYCONTEXT);
      try
        ZeroMemory(@EncParams, SizeOf(CRYPT_ENCRYPT_MESSAGE_PARA));
        EncParams.cbSize := SizeOf(CRYPT_ENCRYPT_MESSAGE_PARA);
        EncParams.dwMsgEncodingType := encType;
        EncParams.ContentEncryptionAlgorithm.pszObjId := CertAlgIdToOID
          (CALG_G28147);
        EncParams.HCRYPTPROV := hProv;

        Win32Check(CryptEncryptMessage(@EncParams, CountS, @ArrayCert,
            Memory, Size, nil, @cbEncrypted));
        try
          ms1.SetSize(cbEncrypted);
          Win32Check(CryptEncryptMessage(@EncParams, CountS, @ArrayCert,
              Memory, Size, ms1.Memory, @cbEncrypted));
          ms1.SetSize(cbEncrypted);
        finally
        end;
      finally
        for i := 0 to High(ArrayCert) do
          CertFreeCertificateContext(ArrayCert[i]);
      end;
    finally
    end;
end;

function ZipDir(fn: string): Boolean;
var
  Zip: TFWZipWriter;
  // s: TStringStream;
  f: string;
  procedure CheckResult(value: integer);
  begin
    if value < 0 then
      raise Exception.Create('������ ���������� ������');
  end;

begin
  Result := False;
  try
    Zip := TFWZipWriter.Create;
    try
      // CheckResult(Zip.AddFolder(ExtractFileName(ExcludeTrailingPathDelimiter(fn)), fn, '*', True));
      // CheckResult(Zip.AddFile(fn + '.sig'));
      f := fn + '.zip';
      if FileExists(f) then
        DeleteFile(PWideChar(f));

      Zip.BuildZip(f);
      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;

function DelDir(dir: string): Boolean;
var
  fos: TSHFileOpStruct;
begin
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_DELETE;
    fFlags := FOF_SILENT or FOF_NOCONFIRMATION;
    pFrom := PChar(dir + #0);
  end;
  Result := (0 = ShFileOperation(fos));
end;

function FindFiles(StartFolder, Mask: string): string;
var
  SearchRec: TSearchRec;
  FindResult: integer;
begin
  Result := '';

  StartFolder := IncludeTrailingBackslash(StartFolder);
  FindResult := FindFirst(StartFolder + Mask, faAnyFile, SearchRec);
  if FindResult = 0 then
    Result := StartFolder + SearchRec.Name;
end;

function UnZip(path: string; var NewPath: string): Boolean;
var
  Zip: TFWZipReader;
  // s: TStringStream;
  // f: string;
begin
  Result := False;
  // NewPath:=
  try
    Zip := TFWZipReader.Create;
    try
      Zip.LoadFromFile(path);
      NewPath := WinTemp + NameProg;

      if DirectoryExists(NewPath) then
        RemoveDir(NewPath);
//        DelDir(NewPath);


      if not DirectoryExists(NewPath) then
        MkDir(NewPath);
      try
        Zip.ExtractAll(NewPath);
      except
        Show_Inf('������������ ��������� ����� ������.' + #10#13 +
            '���������� ������� ������ ����� ��� ����������� ���������������'
            + #10#13 + '� ������� �������������� �� �����', fError);
      end;



      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;

procedure Shapka_PrintTable1(SG: TADVStringGrid);
begin
  SG.Alignments[0, 0] := taCenter;
  SG.ColWidths[0] := 30;
  SG.Cells[0, 0] := '� �/�';
  SG.Alignments[0, 4] := taCenter;
  SG.Cells[0, 4] := '�';
  SG.MergeCells(0, 0, 1, 4);

  SG.Alignments[1, 0] := taCenter;
  SG.MergeCells(1, 0, 1, 4);
  SG.ColWidths[1] := 100;
  SG.Cells[1, 0] := '��� ���������';
  SG.Alignments[1, 4] := taCenter;
  SG.Cells[1, 4] := '1';

  SG.Alignments[2, 0] := taCenter;
  SG.MergeCells(2, 0, 1, 4);
  SG.ColWidths[2] := 65;
  SG.Cells[2, 0] := '��� ���� ���������';
  SG.Alignments[2, 4] := taCenter;
  SG.Cells[2, 4] := '2';

  SG.Alignments[3, 0] := taCenter;
  SG.MergeCells(3, 0, 3, 1);
  SG.ColWidths[3] := 100;
  SG.ColWidths[4] := 75;
  SG.ColWidths[5] := 75;
  SG.RowHeights[0] := 40;
  SG.Cells[3, 0] :=
    '�������� � ������������� ���������� ���������/�����������-��������� ���������� ���������';
  SG.Alignments[3, 4] := taCenter;
  SG.Alignments[4, 4] := taCenter;
  SG.Alignments[5, 4] := taCenter;
  SG.Cells[3, 4] := '3';
  SG.Cells[4, 4] := '4';
  SG.Cells[5, 4] := '5';
  SG.MergeCells(3, 1, 1, 3);
  SG.Alignments[3, 1] := taCenter;
  SG.Cells[3, 1] := '������������   �������������/ �����������-���������';
  SG.MergeCells(4, 1, 1, 3);
  SG.Alignments[4, 1] := taCenter;
  SG.Cells[4, 1] := '���';
  SG.MergeCells(5, 1, 1, 3);
  SG.Alignments[5, 1] := taCenter;
  SG.Cells[5, 1] := '���';

  SG.Alignments[6, 0] := taCenter;
  SG.MergeCells(6, 0, 1, 4);
  SG.ColWidths[6] := 60;
  SG.Cells[6, 0] := '������� �� ������ ��������� �������';
  SG.Alignments[6, 4] := taCenter;
  SG.Cells[6, 4] := '6';

  SG.Alignments[7, 0] := taCenter;
  SG.MergeCells(7, 0, 8, 1);
  SG.Cells[7, 0] := '�����������';
  SG.Alignments[7, 1] := taCenter;
  SG.MergeCells(7, 1, 4, 1);
  SG.Cells[7, 1] := '�������';
  SG.Alignments[7, 2] := taCenter;
  SG.MergeCells(7, 2, 3, 1);
  SG.Cells[7, 2] := '� ��� �����';
  SG.Cells[7, 3] := '�� ���.-����������.';
  SG.RowHeights[3] := 45;
  SG.Alignments[7, 3] := taCenter;
  SG.Cells[7, 4] := '7';
  SG.Alignments[7, 4] := taCenter;
  SG.ColWidths[7] := 55;
  SG.Cells[8, 3] := '�� ���. ���. ����.';
  SG.Alignments[8, 3] := taCenter;
  SG.Cells[8, 4] := '8';
  SG.Alignments[8, 4] := taCenter;
  SG.ColWidths[8] := 55;
  SG.Cells[9, 3] := '�� �������';
  SG.Alignments[9, 3] := taCenter;
  SG.Cells[9, 4] := '9';
  SG.Alignments[9, 4] := taCenter;
  SG.ColWidths[9] := 55;
  SG.Cells[10, 2] := '�����';
  SG.MergeCells(10, 2, 1, 2);
  SG.Alignments[10, 2] := taCenter;
  SG.Cells[10, 4] := '10';
  SG.Alignments[10, 4] := taCenter;
  SG.ColWidths[10] := 55;
  SG.Cells[11, 1] := '������� �� ����������';
  SG.MergeCells(11, 1, 1, 3);
  SG.Alignments[11, 1] := taCenter;
  SG.Cells[11, 4] := '11';
  SG.Alignments[11, 4] := taCenter;
  SG.ColWidths[11] := 55;
  SG.Cells[12, 1] := '������ �����������';
  SG.MergeCells(12, 1, 1, 3);
  SG.Alignments[12, 1] := taCenter;
  SG.Cells[12, 4] := '12';
  SG.Alignments[12, 4] := taCenter;
  SG.ColWidths[12] := 55;
  SG.Cells[13, 1] := '����������� ������ ����� �����������';
  SG.MergeCells(13, 1, 1, 3);
  SG.Alignments[13, 1] := taCenter;
  SG.Cells[13, 4] := '13';
  SG.Alignments[13, 4] := taCenter;
  SG.ColWidths[13] := 55;
  SG.Cells[14, 1] := '�����';
  SG.MergeCells(14, 1, 1, 3);
  SG.Alignments[14, 1] := taCenter;
  SG.Cells[14, 4] := '14';
  SG.Alignments[14, 4] := taCenter;
  SG.ColWidths[14] := 55;

  SG.Alignments[15, 0] := taCenter;
  SG.MergeCells(15, 0, 5, 1);
  SG.Cells[15, 0] := '������';
  SG.Cells[15, 1] := '����� ��������� �������';
  SG.MergeCells(15, 1, 1, 3);
  SG.Alignments[15, 1] := taCenter;
  SG.Cells[15, 4] := '15';
  SG.Alignments[15, 4] := taCenter;
  SG.ColWidths[15] := 55;
  SG.Cells[16, 1] := '������ ������';
  SG.MergeCells(16, 1, 1, 3);
  SG.Alignments[16, 1] := taCenter;
  SG.Cells[16, 4] := '16';
  SG.Alignments[16, 4] := taCenter;
  SG.ColWidths[16] := 55;
  SG.Cells[17, 1] := '������� ����������';
  SG.MergeCells(17, 1, 1, 3);
  SG.Alignments[17, 1] := taCenter;
  SG.Cells[17, 4] := '17';
  SG.Alignments[17, 4] := taCenter;
  SG.ColWidths[17] := 55;
  SG.Cells[18, 1] := '����������� ������ ����� �����������';
  SG.MergeCells(18, 1, 1, 3);
  SG.Alignments[18, 1] := taCenter;
  SG.Cells[18, 4] := '18';
  SG.Alignments[18, 4] := taCenter;
  SG.ColWidths[18] := 55;
  SG.Cells[19, 1] := '�����';
  SG.MergeCells(19, 1, 1, 3);
  SG.Alignments[19, 1] := taCenter;
  SG.Cells[19, 4] := '19';
  SG.Alignments[19, 4] := taCenter;
  SG.ColWidths[19] := 55;

  SG.Alignments[20, 0] := taCenter;
  SG.MergeCells(20, 0, 1, 4);
  SG.ColWidths[20] := 60;
  SG.Cells[20, 0] := '������� �� ����� ��������� �������';
  SG.Alignments[20, 4] := taCenter;
  SG.Cells[20, 4] := '20';

  // SG.MergeCells(0,5,21,1);

  // SG.RowCount := 100;
  // SG.RandomFill(false,100);
  // for i := 6 to SG.RowCount - 1 do
  // SG.Ints[1,i] := random(5);
  // SG.Grouping.Summary := true;
  // SG.Grouping.MergeHeader := true;
  // SG.Grouping.ShowGroupCount := true;
  // SG.Group(1);
  // SG.GroupCustomCalc(1);
end;

procedure Shapka_PrintTable2(SG: TADVStringGrid);
begin
  SG.RowCount := 5;
  SG.FixedRows := 4;
  SG.Alignments[0, 0] := taCenter;
  SG.ColWidths[0] := 30;
  SG.Cells[0, 0] := '� �/�';
  SG.Alignments[0, 3] := taCenter;
  SG.Cells[0, 3] := '�';
  SG.MergeCells(0, 0, 1, 3);

  SG.Alignments[1, 0] := taCenter;
  SG.MergeCells(1, 0, 1, 3);
  SG.ColWidths[1] := 100;
  SG.Cells[1, 0] := '��� ���������';
  SG.Alignments[1, 3] := taCenter;
  SG.Cells[1, 3] := '1';

  SG.Alignments[2, 0] := taCenter;
  SG.MergeCells(2, 0, 1, 3);
  SG.ColWidths[2] := 65;
  SG.Cells[2, 0] := '��� ���� ���������';
  SG.Alignments[2, 3] := taCenter;
  SG.Cells[2, 3] := '2';

  SG.Alignments[3, 0] := taCenter;
  SG.MergeCells(3, 0, 3, 1);
  SG.ColWidths[3] := 200;
  SG.ColWidths[4] := 75;
  SG.ColWidths[5] := 75;
  SG.RowHeights[0] := 40;
  SG.Cells[3, 0] :=
    '�������� � ������������� ���������� ���������/�����������-��������� ���������� ���������';
  SG.Alignments[3, 3] := taCenter;
  SG.Alignments[4, 3] := taCenter;
  SG.Alignments[5, 3] := taCenter;
  SG.Cells[3, 3] := '3';
  SG.Cells[4, 3] := '4';
  SG.Cells[5, 3] := '5';
  SG.MergeCells(3, 1, 1, 2);
  SG.Alignments[3, 1] := taCenter;
  SG.Cells[3, 1] := '������������   �������������/ �����������-���������';
  SG.MergeCells(4, 1, 1, 2);
  SG.Alignments[4, 1] := taCenter;
  SG.Cells[4, 1] := '���';
  SG.MergeCells(5, 1, 1, 2);
  SG.Alignments[5, 1] := taCenter;
  SG.Cells[5, 1] := '���';

  SG.Alignments[6, 0] := taCenter;
  SG.MergeCells(6, 0, 7, 1);
  SG.ColWidths[6] := 200;
  SG.ColWidths[7] := 65;
  SG.ColWidths[8] := 65;
  SG.ColWidths[9] := 65;
  SG.ColWidths[10] := 65;
  SG.Cells[6, 1] := '������������ �����������';
  SG.MergeCells(6, 1, 1, 2);
  SG.Alignments[6, 1] := taCenter;
  SG.Alignments[6, 3] := taCenter;
  SG.Cells[6, 3] := '6';
  SG.Cells[7, 1] := '���';
  SG.MergeCells(7, 1, 1, 2);
  SG.Alignments[7, 1] := taCenter;
  SG.Alignments[7, 3] := taCenter;
  SG.Cells[7, 3] := '7';
  SG.Cells[8, 1] := '���';
  SG.MergeCells(8, 1, 1, 2);
  SG.Alignments[8, 1] := taCenter;
  SG.Alignments[8, 3] := taCenter;
  SG.Cells[8, 3] := '8';
  SG.Cells[9, 1] := '��������';
  SG.MergeCells(9, 1, 4, 1);
  SG.Alignments[9, 1] := taCenter;
  SG.Alignments[9, 3] := taCenter;
  SG.Cells[9, 3] := '9';
  SG.RowHeights[2] := 40;
  SG.Cells[9, 2] := '�����, �����';
  SG.Alignments[9, 2] := taCenter;
  SG.ColWidths[10] := 70;
  SG.ColWidths[11] := 70;
  SG.Cells[10, 2] := '���� ������';
  SG.Alignments[10, 2] := taCenter;
  SG.Alignments[10, 3] := taCenter;
  SG.Cells[10, 3] := '10';
  SG.Cells[11, 2] := '���� ���������';
  SG.Alignments[11, 2] := taCenter;
  SG.Alignments[11, 3] := taCenter;
  SG.Cells[11, 3] := '11';
  SG.Cells[12, 2] := '��� ������';
  SG.Alignments[12, 2] := taCenter;
  SG.Alignments[12, 3] := taCenter;
  SG.Cells[12, 3] := '12';

  SG.Alignments[13, 0] := taCenter;
  SG.MergeCells(13, 0, 1, 3);
  SG.ColWidths[13] := 60;
  SG.Cells[13, 0] := '���� �������';
  SG.Alignments[13, 3] := taCenter;
  SG.Cells[13, 3] := '13';

  SG.Alignments[14, 0] := taCenter;
  SG.MergeCells(14, 0, 1, 3);
  SG.ColWidths[14] := 55;
  SG.Cells[14, 0] := '����� �������-������������ ��������';
  SG.Alignments[14, 3] := taCenter;
  SG.Cells[14, 3] := '14';

  SG.Alignments[15, 0] := taCenter;
  SG.MergeCells(15, 0, 1, 3);
  SG.ColWidths[15] := 55;
  SG.Cells[15, 0] := '�����  ���������� ����������';
  SG.Alignments[15, 3] := taCenter;
  SG.Cells[15, 3] := '15';

  SG.Alignments[16, 0] := taCenter;
  SG.MergeCells(16, 0, 1, 3);
  SG.ColWidths[16] := 55;
  SG.Cells[16, 0] := '����� ����������� ���������';
  SG.Alignments[16, 3] := taCenter;
  SG.Cells[16, 3] := '16';
end;

procedure DeclTable1(f11: Boolean; SG1: TADVStringGrid; xf: TXLSFile;
  NumSheets: integer);
var
  i, j, row, RowXLS: integer;
  SLDataKpp: TStringList;
  kpp, s: string;
  dv_org, dv_kpp: TDvizenie;
  r: TRange;
begin
  SLDataKpp := TStringList.Create;
  try
    row := 1;
    SG1.RowCount := SG1.FixedRows + 1;
    SG1.MergeCells(0, SG1.RowCount - 1, SG1.ColCount, 1);

    xf.Workbook.Sheets[NumSheets].Cells[0,
      1].value := fOrg.SelfName + ', ' + fOrg.inn;
    xf.Workbook.Sheets[NumSheets].Cells[1, 1].value := fOrg.Address;

    RowXLS := SG1.RowCount - 1 + 5;
    s := '�� �����������';
    xf.Workbook.Sheets[NumSheets].Freeze(SG1.RowCount - 1 + SG1.FixedRows,
      0);
    SG1.CellProperties[0, SG1.RowCount - 1].FontSize := 9;
    SG1.CellProperties[0, SG1.RowCount - 1].FontStyle := [fsBold];
    SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;

    r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
    r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
      SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1);
    r.MergeCells;
    r.value := s;
    r.FontBold := True;
    r.BordersOutline(xlColorBlack, bsThin);

    if SLOborotKpp.Count > 0 then
    begin
      FillPrintFNSSG1(f11, SLOborotKpp, SG1, row, dv_org, xf, NumSheets);
      SG1.RowCount := SG1.RowCount + 1;
      SG1.MergeCells(1, SG1.RowCount - 1, 5, 1);
      for j := 1 to SG1.ColCount - 1 do
      begin
        SG1.CellProperties[j, SG1.RowCount - 1].FontSize := 9;
        SG1.CellProperties[j, SG1.RowCount - 1].FontStyle := [fsBold];
      end;
      s := '����� �� �����������:';
      SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;

      r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
      r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
        SG1.RowCount - 1 + SG1.FixedRows, 0, 5);
      r.MergeCells;
      r.value := s;
      BorderRowXLS(SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1,
        NumSheets, xf, True);

      WriteSumDv(dv_org, SG1, SG1.RowCount - 1, xf, NumSheets);
      SG1.RowColor[SG1.RowCount - 1] := clTable;
    end
    else
      exit;

    NullDviz(dv_org);
    for i := 0 to SLKPP.Count - 1 do
    begin
      SG1.RowCount := SG1.RowCount + 1;
      SG1.MergeCells(0, SG1.RowCount - 1, SG1.ColCount, 1);
      kpp := SLKPP.Strings[i];
      s := '�� ������������� �������������: ' + fOrg.SelfName + ', ��� ' +
        kpp;
      SG1.CellProperties[0, SG1.RowCount - 1].FontSize := 9;
      SG1.CellProperties[0, SG1.RowCount - 1].FontStyle := [fsBold];
      SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;

      r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
      r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
        SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1);
      r.MergeCells;
      r.value := s;
      r.FontBold := True;
      r.BordersOutline(xlColorBlack, bsThin);

      // ����������� ������ �� ���
      FilterSLKPP(kpp, SLOborotKpp, SLDataKpp);
      NullDviz(dv_kpp);
      if SLDataKpp.Count > 0 then
      begin
        FillPrintFNSSG1(f11, SLDataKpp, SG1, row, dv_kpp, xf, NumSheets);
        SG1.RowCount := SG1.RowCount + 1;
        SG1.MergeCells(1, SG1.RowCount - 1, 5, 1);
        for j := 1 to SG1.ColCount - 1 do
        begin
          SG1.CellProperties[j, SG1.RowCount - 1].FontSize := 9;
          SG1.CellProperties[j, SG1.RowCount - 1].FontStyle := [fsBold];
        end;

        s := '����� �� ��� ' + kpp + ':';
        SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;
        WriteSumDv(dv_kpp, SG1, SG1.RowCount - 1, xf, NumSheets);
        SG1.RowColor[SG1.RowCount - 1] := clTable;

        r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
        r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
          SG1.RowCount - 1 + SG1.FixedRows, 0, 5);
        r.MergeCells;
        r.value := s;
        BorderRowXLS(SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1,
          NumSheets, xf, True);

      end;
      SumDviz(dv_org, dv_kpp);
    end;
    SG1.RowCount := SG1.RowCount + 1;
    for i := 1 to SG1.ColCount - 1 do
    begin
      SG1.CellProperties[i, SG1.RowCount - 1].FontSize := 9;
      SG1.CellProperties[i, SG1.RowCount - 1].FontStyle := [fsBold];
    end;
    SG1.MergeCells(1, SG1.RowCount - 1, 5, 1);

    s := '����� �� �����������:';
    SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;
    WriteSumDv(dv_org, SG1, SG1.RowCount - 1, xf, NumSheets);
    SG1.RowColor[SG1.RowCount - 1] := clTable;

    r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
    r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
      SG1.RowCount - 1 + SG1.FixedRows, 0, 5);
    r.MergeCells;
    r.value := s;
    BorderRowXLS(SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1,
      NumSheets, xf, True);

  finally
    SLDataKpp.free;
  end;
end;

procedure FilterSLKPP(FilterStr: string; SLSourse: TStringList;
  var SLRez: TStringList);
var
  i: integer;
  kpp: string;
begin
  SLRez.Clear;
  if FilterStr = '' then
    exit;
  for i := 0 to SLSourse.Count - 1 do
  begin
    kpp := Copy(SLSourse.Strings[i], 0, 9);
    if kpp = FilterStr then
      SLRez.AddObject(SLSourse.Strings[i], SLSourse.Objects[i]);
  end;
end;

procedure FillPrintFNSSG1(f11: Boolean; SL: TStringList;
  SG1: TADVStringGrid; var row: integer; var m: TDvizenie;
  xf: TXLSFile; NumSheets: integer);
var
  i: integer;
  SL_FNS, SL_DataFNS: TStringList;
  dv: TMove;
  FNS: string;
  dvFNS: TDvizenie;
begin
  SL_FNS := TStringList.Create;
  SL_DataFNS := TStringList.Create;
  NullDviz(m);
  try
    // C����� ����� ���������
    for i := 0 to SL.Count - 1 do
    begin
      dv := TMove(SL.Objects[i]);
      if f11 then
      begin
        if dv.forma = 11 then
          if TMove(SL.Objects[i]) <> nil then
            if TMove(SL.Objects[i]) is TMove then
            begin
              FNS := String(TMove(SL.Objects[i]).FNS);
              if SL_FNS.IndexOf(FNS) = -1 then
                SL_FNS.Add(String(TMove(SL.Objects[i]).FNS));
            end;
      end
      else
      begin
        if dv.forma = 12 then
          if TMove(SL.Objects[i]) <> nil then
            if TMove(SL.Objects[i]) is TMove then
            begin
              FNS := String(TMove(SL.Objects[i]).FNS);
              if SL_FNS.IndexOf(FNS) = -1 then
                SL_FNS.Add(String(TMove(SL.Objects[i]).FNS));
            end;
      end
    end;

    SL_FNS.Sort;
    // ��������� ������ �� ���� ������
    for i := 0 to SL_FNS.Count - 1 do
    begin
      NullDviz(dvFNS);
      FNS := SL_FNS.Strings[i];
      FilterDataFNSMove(FNS, SL, SL_DataFNS);
      WriteDataFNS_Table1(SL_DataFNS, SG1, row, dvFNS, xf, NumSheets);
      SumDviz(m, dvFNS);
    end;
  finally
    SL_FNS.free;
    SL_DataFNS.free;
  end;
end;

procedure RefreshFileFolder(path: string);
var
  fDecl, fRef, fn, fn1, inn, dir_path, main_dir: string;
  // rez:Boolean;
  tIniF: TIniFile;
  ms, ms1: TMemoryStream;
begin
  inn := '';
  // ����� INI  �����
  fn := FindFiles(path, '*.ini');
  if fn <> '' then
  begin
    tIniF := TIniFile.Create(fn);
    try
      inn := tIniF.ReadString(SectionName, 'INN', inn);
      if inn <> '' then
      begin
        fOrg.inn := inn;
        CreatePath(inn);
        main_dir := GetSpecialPath(CSIDL_PROGRAM_FILES)
          + '\' + NameProg + '\';

        // �������� ��������� ����� ��� �����������
        if not DirectoryExists(path_db) then
        begin
          if not DirectoryExists(main_dir) then
            MkDir(main_dir);
          dir_path := main_dir + fOrg.inn + '\';
          if not DirectoryExists(dir_path) then
            MkDir(dir_path);
          dir_path := dir_path + 'DataBase\';
          if not DirectoryExists(dir_path) then
            MkDir(dir_path);
          path_db := dir_path;
        end;
        // ���� ��� �������� ����
        db_decl := path_db + '\' + NameProg + '.dat';
        // ���� ��� �������� ������������
        db_spr := path_db + '\' + NameProg + '.ref';

        if not DirectoryExists(path_arh) then
        begin
          // dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)
          // + '\' + NameProg + '\';
          if not DirectoryExists(main_dir) then
            MkDir(main_dir);
          dir_path := main_dir + fOrg.inn + '\';
          if not DirectoryExists(dir_path) then
            MkDir(dir_path);
          dir_path := dir_path + 'BackUp\';
          if not DirectoryExists(dir_path) then
            MkDir(dir_path);
          // path_arh := Copy(dir_path, 0, Length(dir_path) - 1);
          path_arh := dir_path;
        end;

        // path_arh := Copy(dir_path, 0, Length(dir_path) - 1);

        // ����������� INI �����
        fn1 := main_dir + fOrg.inn + '\' + NameProg + '.ini';

        if FileExists(fn1) then
          DeleteFile(PWideChar(fn1));
        if FileExists(fn) then
          CopyFile(PChar(fn), PChar(fn1), True);

        // ����������� ����� ����������� � ����������
        fDecl := FindFiles(path, 'Decl_*.*');
        CopyFile(PChar(fDecl),
          PChar(path_arh + '\' + ExtractFileName(fDecl)), True);

        fRef := FindFiles(path, 'Refs_*.*');
        CopyFile(PChar(fRef),
          PChar(path_arh + '\' + ExtractFileName(fRef)), True);

        fmWork.con1ADOCon.Close;
        fmWork.con2ADOCon.Close;

        // �������������� ����� �����������
        if fRef <> '' then
        begin
          ms := TMemoryStream.Create;
          ms1 := TMemoryStream.Create;
          try
            ms.LoadFromFile(fRef);
            if UnZipFilePas(ms, ms1, psw_loc) then
            begin
              if FileIsReadOnly(fRef) then
                FileSetReadOnly(fRef, False);
              if FileExists(db_spr) then
                DeleteFile(PWideChar(db_spr));
              ms1.SaveToFile(db_spr);
            end;
          finally
            ms.free;
            ms1.free;
          end;
        end;

        // �������������� ����� ����������
        if fDecl <> '' then
        begin
          ms := TMemoryStream.Create;
          ms1 := TMemoryStream.Create;
          try
            ms.LoadFromFile(fDecl);
            if UnZipFilePas(ms, ms1, psw_loc) then
            begin
              if FileIsReadOnly(fDecl) then
                FileSetReadOnly(fDecl, False);
              if FileExists(db_decl) then
                DeleteFile(PWideChar(db_decl));
              ms1.SaveToFile(db_decl);
            end;
          finally
            ms.free;
            ms1.free;
          end;
        end;
      end;
    finally
      tIniF.free;
    end;
  end;
end;

procedure FilterDataFNSMove(FNS: string; SL: TStringList;
  var SL_Data: TStringList);
var
  i: integer;
  dv: TMove;
begin
  SL_Data.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    dv := TMove(SL.Objects[i]);
    if TMove(SL.Objects[i]) <> nil then
      if TMove(SL.Objects[i]) is TMove then
      begin
        if String(TMove(SL.Objects[i]).FNS) = FNS then
          SL_Data.AddObject(SL.Strings[i], SL.Objects[i]);
      end;
  end;
end;

procedure WriteDataFNS_Table1(SL: TStringList; SG: TADVStringGrid;
  var row: integer; var m: TDvizenie; xf: TXLSFile; NumSheets: integer);
const
  ColSum: array [0 .. 9] of integer = (6, 8, 10, 11, 14, 15, 16, 17, 19,
    20);
var
  dv, dv1: TMove;
  i, StartRow, idx: integer;
  s, pId: string;
  SL_Pr, SL_PrData, SL_Data: TStringList;
  rez: Extended;
  r: TRange;
begin
  SL_Pr := TStringList.Create;
  SL_PrData := TStringList.Create;
  SL_Data := TStringList.Create;
  NullDviz(m);
  try
    StartRow := SG.RowCount;

    // ���������� �� �������������
    SortSLCol_dv(1, SL);
    // ��������� ������ ��������������
    for i := 0 to SL.Count - 1 do
    begin
      dv := TMove(SL.Objects[i]);
      pId := String(dv.product.prod.pId);
      idx := SL_Pr.IndexOf(pId);
      if idx = -1 then
        SL_Pr.Add(pId);
    end;

    // ��������� ������ �� �������������
    for i := 0 to SL_Pr.Count - 1 do
    begin
      dv1 := TMove.Create;
      pId := SL_Pr.Strings[i];
      // ���������� ������ �� �������������
      FiltrSLPr(pId, SL, SL_PrData);
      // ������������ ������ �� �������������
      SumDataDv(SL_PrData, dv1);
      SL_Data.AddObject(pId, dv1)
    end;

    SG.RowCount := StartRow + SL_Data.Count;
    for i := 0 to SL_Data.Count - 1 do
    begin
      dv := TMove(SL_Data.Objects[i]);
      idx := SLFNS.IndexOfObject(Pointer(StrToInt(String(dv.FNS))));
      if idx <> -1 then
      begin
        s := SLFNS.Strings[idx];
        s := IntToStr(LongInt(SLFNS.Objects[idx])) + SLFNS.Strings[idx];
        SG.Cells[0, StartRow + i] := IntToStr(row);
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5,
          0].value := row;

        SG.Cells[1, StartRow + i] := s;
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5, 1].value := s;

        SG.Cells[2, StartRow + i] := String(dv.FNS);
        // xf.Workbook.Sheets[NumSheets].Cells[StartRow+i,2].FormatStringIndex:=1;
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5,
          2].value := StrToInt(String(dv.FNS));

        SG.Cells[3, StartRow + i] := String(dv.product.prod.pName);
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5,
          3].value := dv.product.prod.pName;

        SG.Cells[4, StartRow + i] := String(dv.product.prod.pINN);
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5,
          4].value := dv.product.prod.pINN;

        SG.Cells[5, StartRow + i] := String(dv.product.prod.pKPP);
        xf.Workbook.Sheets[NumSheets].Cells[StartRow + i + 5,
          5].value := dv.product.prod.pKPP;

        WriteSumDv(dv.dv_dal, SG, StartRow + i, xf, NumSheets);
        BorderRowXLS(StartRow + i + SG.FixedRows, 0, SG.ColCount - 1,
          NumSheets, xf, False);
        SumDviz(m, dv.dv_dal);
        inc(row);
      end;
    end;
    SG.RowCount := SG.RowCount + 1;
    SG.MergeCells(1, SG.RowCount - 1, 5, 1);
    for i := 1 to SG.ColCount - 1 do
    begin
      SG.CellProperties[i, SG.RowCount - 1].FontSize := 9;
      SG.CellProperties[i, SG.RowCount - 1].FontStyle := [fsBold];
    end;

    s := '����� �� ���� ' + String(dv.FNS);
    r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
    r.AddRect(SG.RowCount - 1 + SG.FixedRows,
      SG.RowCount - 1 + SG.FixedRows, 0, 5);
    r.MergeCells;
    r.value := s;

    SG.Cells[1, SG.RowCount - 1] := s;
    SG.RowColor[SG.RowCount - 1] := clTable;
    WriteSumDv(m, SG, SG.RowCount - 1, xf, NumSheets);
    BorderRowXLS(SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1,
      NumSheets, xf, True);

    for i := 0 to High(ColSum) do
    begin
      rez := SG.ColumnSum(ColSum[i], StartRow, SG.RowCount - 2);
      if rez > 0 then
        SG.Floats[ColSum[i], SG.RowCount - 1] := rez;
    end;

  finally
    SL_Pr.free;
    SL_PrData.free;
    FreeStringList(SL_Data);
  end;
end;

procedure SortSLCol_dv(cols: integer; SL: TStringList;
  aCompare: TStringListSortCompare = nil);
var
  SlSort: TStringList;
  dv: TMove;
  i: integer;
begin
  // ����������� ������.
  SlSort := TStringList.Create;

  for i := 0 to SL.Count - 1 do
  begin
    // ������ ��������� ��� ����� ������ �������.
    dv := Pointer(SL.Objects[i]);
    case cols of
      1:
        SlSort.AddObject(String(dv.product.prod.pId), dv);
      2:
        SlSort.AddObject(String(dv.FNS), dv);
      3:
        SlSort.AddObject(String(dv.sal.sId), dv);
    end;
  end;

  // ��������� �������.
  if Assigned(aCompare) then
    SlSort.CustomSort(aCompare)
  else
    SlSort.Sort;

  SL.Clear;
  SL.Assign(SlSort);

  // ���������� ����������� ������.
  FreeAndNil(SlSort);
end;

procedure SumDataDv(SL: TStringList; var dv: TMove);
var
  i: integer;
  dv_pr: TMove;
begin
  dv.dv_dal.ost0 := 0;
  dv.dv_dal.prih := 0;
  dv.dv_dal.rash := 0;
  dv.dv_dal.vozv_post := 0;
  dv.dv_dal.vozv_pokup := 0;
  dv.dv_dal.brak := 0;
  dv.dv_dal.ost1 := 0;
  dv.dv_dal.move_in := 0;
  dv.dv_dal.move_out := 0;

  for i := 0 to SL.Count - 1 do
  begin
    dv_pr := TMove(SL.Objects[i]);
    dv.dv_dal.ost0 := dv.dv_dal.ost0 + dv_pr.dv_dal.ost0;
    dv.dv_dal.prih := dv.dv_dal.prih + dv_pr.dv_dal.prih;
    dv.dv_dal.rash := dv.dv_dal.rash + dv_pr.dv_dal.rash;
    dv.dv_dal.vozv_post := dv.dv_dal.vozv_post + dv_pr.dv_dal.vozv_post;
    dv.dv_dal.vozv_pokup := dv.dv_dal.vozv_pokup + dv_pr.dv_dal.vozv_pokup;
    dv.dv_dal.brak := dv.dv_dal.brak + dv_pr.dv_dal.brak;
    dv.dv_dal.ost1 := dv.dv_dal.ost1 + dv_pr.dv_dal.ost1;
    dv.dv_dal.move_in := dv.dv_dal.move_in + dv_pr.dv_dal.move_in;
    dv.dv_dal.move_out := dv.dv_dal.move_out + dv_pr.dv_dal.move_out;
  end;
  dv.product := dv_pr.product;
  dv.FNS := dv_pr.FNS;
end;

procedure DeclTable2(f11: Boolean; SG: TADVStringGrid; xf: TXLSFile;
  NumSheets: integer);
var
  i, row: integer;
  SLDataKpp: TStringList;
  kpp, s: string;
  SumOrg, SumKpp: Extended;
  r: TRange;
begin
  SLDataKpp := TStringList.Create;
  try
    row := 1;
    SG.RowCount := SG.FixedRows + 1;
    s := '�� �����������';
    xf.Workbook.Sheets[NumSheets].Freeze(SG.RowCount - 1 + SG.FixedRows, 0);
    SG.MergeCells(0, SG.RowCount - 1, SG.ColCount, 1);
    SG.CellProperties[0, SG.RowCount - 1].FontSize := 9;
    SG.CellProperties[0, SG.RowCount - 1].FontStyle := [fsBold];
    SG.Cells[SG.FixedCols, SG.RowCount - 1] := s;

    xf.Workbook.Sheets[NumSheets].Cells[0,
      1].value := fOrg.SelfName + ', ' + fOrg.inn;
    xf.Workbook.Sheets[NumSheets].Cells[1, 1].value := fOrg.Address;

    r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
    r.AddRect(SG.RowCount - 1 + SG.FixedRows,
      SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1);
    r.MergeCells;
    r.value := s;
    r.FontBold := True;
    r.BordersOutline(xlColorBlack, bsThin);

    if SLOborotKpp.Count > 0 then
    begin
      SumOrg := 0;
      SumOrg := FillPrintFNSSG2(f11, SLDataPrih, SG, row, xf, NumSheets);
      SG.RowCount := SG.RowCount + 1;
      SG.MergeCells(1, SG.RowCount - 1, SG.ColCount - 2, 1);
      SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontSize := 9;
      SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontStyle :=
        [fsBold];
      SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontSize := 9;
      SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontStyle :=
        [fsBold];
      s := '����� �� �����������:';
      SG.Cells[SG.FixedCols, SG.RowCount - 1] := s;
      SG.Floats[SG.ColCount - 1, SG.RowCount - 1] := SumOrg;
      SG.RowColor[SG.RowCount - 1] := clTable;
      xf.Workbook.Sheets[NumSheets].Cells[SG.RowCount - 1 + SG.FixedRows,
        SG.ColCount - 1].value := SumOrg;
      r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
      r.AddRect(SG.RowCount - 1 + SG.FixedRows,
        SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 2);
      r.MergeCells;
      r.value := s;
      BorderRowXLS(SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1,
        NumSheets, xf, True);
    end
    else
      exit;

    SumOrg := 0;
    if SLKPP.Count > 1 then
      for i := 0 to SLKPP.Count - 1 do
      begin
        SumKpp := 0;
        SG.RowCount := SG.RowCount + 1;
        SG.MergeCells(0, SG.RowCount - 1, SG.ColCount, 1);
        kpp := SLKPP.Strings[i];
        s := '�� ������������� �������������: ' + fOrg.SelfName + ', ���' +
          kpp;
        SG.CellProperties[0, SG.RowCount - 1].FontSize := 9;
        SG.CellProperties[0, SG.RowCount - 1].FontStyle := [fsBold];
        SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontSize := 9;
        SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontStyle :=
          [fsBold];
        SG.Cells[SG.FixedCols, SG.RowCount - 1] := s;

        r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
        r.AddRect(SG.RowCount - 1 + SG.FixedRows,
          SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1);
        r.MergeCells;
        r.value := s;
        r.FontBold := True;
        r.BordersOutline(xlColorBlack, bsThin);

        SortSLCol(2, SLDataPrih);
        // //����������� ������ �� ���
        FilterSLKPP(kpp, SLDataPrih, SLDataKpp);
        if SLDataKpp.Count > 0 then
        begin
          SumKpp := FillPrintFNSSG2(f11, SLDataKpp, SG, row, xf, NumSheets);
          SG.RowCount := SG.RowCount + 1;
          SG.MergeCells(1, SG.RowCount - 1, SG.ColCount - 2, 1);
          SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontSize := 9;
          SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontStyle :=
            [fsBold];
          SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontSize := 9;
          SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontStyle :=
            [fsBold];
          s := '����� �� ���' + kpp + ':';
          SG.Cells[SG.FixedCols, SG.RowCount - 1] := s;
          SG.Floats[SG.ColCount - 1, SG.RowCount - 1] := SumKpp;
          SG.RowColor[SG.RowCount - 1] := clTable;

          xf.Workbook.Sheets[NumSheets].Cells
            [SG.RowCount - 1 + SG.FixedRows,
            SG.ColCount - 1].value := SumKpp;

          r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
          r.AddRect(SG.RowCount - 1 + SG.FixedRows,
            SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 2);
          r.MergeCells;
          r.value := s;
          BorderRowXLS(SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1,
            NumSheets, xf, True);

        end;
        SumOrg := SumOrg + SumKpp;
      end;
    SG.RowCount := SG.RowCount + 1;
    SG.MergeCells(1, SG.RowCount - 1, SG.ColCount - 2, 1);
    SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontSize := 9;
    SG.CellProperties[SG.FixedCols, SG.RowCount - 1].FontStyle := [fsBold];
    SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontSize := 9;
    SG.CellProperties[SG.ColCount - 1, SG.RowCount - 1].FontStyle :=
      [fsBold];
    s := '����� �� �����������:';
    SG.Cells[SG.FixedCols, SG.RowCount - 1] := s;
    SG.Floats[SG.ColCount - 1, SG.RowCount - 1] := SumOrg;
    SG.RowColor[SG.RowCount - 1] := clTable;

    xf.Workbook.Sheets[NumSheets].Cells[SG.RowCount - 1 + SG.FixedRows,
      SG.ColCount - 1].value := SumOrg;

    r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
    r.AddRect(SG.RowCount - 1 + SG.FixedRows,
      SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 2);
    r.MergeCells;
    r.value := s;
    BorderRowXLS(SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1,
      NumSheets, xf, True);

  finally
    SLDataKpp.free;
  end;
end;

function FillPrintFNSSG2(f11: Boolean; SL: TStringList;
  SG1: TADVStringGrid; var row: integer; xf: TXLSFile;
  NumSheets: integer): Extended;
var
  i: integer;
  SL_FNS, SL_DataFNS: TStringList;
  decl: TDecl_obj;
  FNS, s: string;
  rez: Extended;
  r: TRange;
begin
  Result := 0;
  SL_FNS := TStringList.Create;
  SL_DataFNS := TStringList.Create;
  try
    // C����� ����� ���������
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]);
      if f11 then
      begin
        if decl.data.forma = 11 then
          if TDecl_obj(SL.Objects[i]) <> nil then
            if TDecl_obj(SL.Objects[i]) is TDecl_obj then
            begin
              FNS := String(TDecl_obj(SL.Objects[i]).data.product.FNSCode);
              if SL_FNS.IndexOf(FNS) = -1 then
                SL_FNS.Add
                  (String(TDecl_obj(SL.Objects[i]).data.product.FNSCode));
            end;
      end
      else
      begin
        if decl.data.forma = 12 then
          if TDecl_obj(SL.Objects[i]) <> nil then
            if TDecl_obj(SL.Objects[i]) is TDecl_obj then
            begin
              FNS := String(TDecl_obj(SL.Objects[i]).data.product.FNSCode);
              if SL_FNS.IndexOf(FNS) = -1 then
                SL_FNS.Add
                  (String(TDecl_obj(SL.Objects[i]).data.product.FNSCode));
            end;
      end
    end;

    SL_FNS.Sort;
    // ��������� ������ �� ���� ������
    for i := 0 to SL_FNS.Count - 1 do
    begin
      FNS := SL_FNS.Strings[i];
      FilterDataFNSDecl(FNS, SL, SL_DataFNS);
      rez := WriteDataFNS_Table2(SL_DataFNS, SG1, row, xf, NumSheets);
      Result := Result + rez;
      SG1.RowCount := SG1.RowCount + 1;

      SG1.MergeCells(1, SG1.RowCount - 1, SG1.ColCount - 2, 1);
      SG1.CellProperties[SG1.ColCount - 1, SG1.RowCount - 1].FontSize := 9;
      SG1.CellProperties[SG1.ColCount - 1, SG1.RowCount - 1].FontStyle :=
        [fsBold];
      SG1.CellProperties[1, SG1.RowCount - 1].FontSize := 9;
      SG1.CellProperties[1, SG1.RowCount - 1].FontStyle := [fsBold];
      s := '����� �� ���� ' + SL_FNS.Strings[i];
      SG1.Cells[SG1.FixedCols, SG1.RowCount - 1] := s;
      SG1.Floats[SG1.ColCount - 1, SG1.RowCount - 1] := rez;
      SG1.RowColor[SG1.RowCount - 1] := clTable;

      xf.Workbook.Sheets[NumSheets].Cells[SG1.RowCount - 1 + SG1.FixedRows,
        SG1.ColCount - 1].value := rez;
      r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
      r.AddRect(SG1.RowCount - 1 + SG1.FixedRows,
        SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 2);
      r.MergeCells;
      r.value := s;
      BorderRowXLS(SG1.RowCount - 1 + SG1.FixedRows, 0, SG1.ColCount - 1,
        NumSheets, xf, True);
    end;
  finally
    SL_FNS.free;
    SL_DataFNS.free;
  end;
end;

function WriteDataFNS_Table2(SL: TStringList; SG: TADVStringGrid;
  var row: integer; xf: TXLSFile; NumSheets: integer): Extended;
var
  decl: TDecl_obj;
  i, j, StartRow, idx: integer;
  s, pId: string;
  rez: Extended;
  SL_Pr, SL_PrData: TStringList;
  r: TRange;
begin
  Result := 0;
  SL_Pr := TStringList.Create;
  SL_PrData := TStringList.Create;
  try
    // ���������� �� �������������
    SortSLCol(8, SL);
    // ��������� ������ ��������������
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]);
      pId := String(decl.data.product.prod.pId);
      idx := SL_Pr.IndexOf(pId);
      if idx = -1 then
        SL_Pr.Add(pId);
    end;

    // ��������� ������ �� �������������
    for i := 0 to SL_Pr.Count - 1 do
    begin
      pId := SL_Pr.Strings[i];
      // ���������� ������ �� �������������
      FiltrSLPrDecl(pId, SL, SL_PrData);

      StartRow := SG.RowCount;

      SG.RowCount := StartRow + SL_PrData.Count;
      for j := 0 to SL_PrData.Count - 1 do
      begin
        decl := TDecl_obj(SL_PrData.Objects[j]);
        idx := SLFNS.IndexOfObject
          (Pointer(StrToInt(String(decl.data.product.FNSCode))));
        if idx <> -1 then
        begin
          s := SLFNS.Strings[idx];
          s := IntToStr(LongInt(SLFNS.Objects[idx])) + SLFNS.Strings[idx];
          SG.Cells[0, StartRow + j] := IntToStr(row);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            0].value := row;

          SG.Cells[1, StartRow + j] := s;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            1].value := s;

          SG.Cells[2, StartRow + j] := String(decl.data.product.FNSCode);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            2].value := StrToInt(String(decl.data.product.FNSCode));

          SG.Cells[3, StartRow + j] := String(decl.data.product.prod.pName);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            3].value := decl.data.product.prod.pName;

          SG.Cells[4, StartRow + j] := String(decl.data.product.prod.pINN);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            4].value := decl.data.product.prod.pINN;

          SG.Cells[5, StartRow + j] := String(decl.data.product.prod.pKPP);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            5].value := decl.data.product.prod.pKPP;

          SG.Cells[6, StartRow + j] := String(decl.data.sal.sName);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            6].value := decl.data.sal.sName;

          SG.Cells[7, StartRow + j] := String(decl.data.sal.sINN);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            7].value := decl.data.sal.sINN;

          SG.Cells[8, StartRow + j] := String(decl.data.sal.sKpp);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            8].value := decl.data.sal.sKpp;

          SG.Cells[9, StartRow + j] := String(decl.data.sal.lic.ser)
            + ', ' + String(decl.data.sal.lic.num);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            9].value := decl.data.sal.lic.ser + ', ' +
            decl.data.sal.lic.num;

          SG.Dates[10, StartRow + j] := decl.data.sal.lic.dBeg;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            10].FormatStringIndex := 13;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            10].value := decl.data.sal.lic.dBeg;

          SG.Dates[11, StartRow + j] := decl.data.sal.lic.dEnd;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            11].FormatStringIndex := 13;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            11].value := decl.data.sal.lic.dEnd;

          SG.Cells[12, StartRow + j] := String(decl.data.sal.lic.RegOrg);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            12].value := decl.data.sal.lic.RegOrg;

          SG.Dates[13, StartRow + j] := decl.data.DeclDate;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            13].FormatStringIndex := 13;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            13].value := decl.data.DeclDate;

          SG.Cells[14, StartRow + j] := String(decl.data.DeclNum);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            14].value := decl.data.DeclNum;

          SG.Cells[15, StartRow + j] := String(decl.data.TM);
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            15].value := decl.data.TM;

          SG.Floats[16, StartRow + j] := 0.1 * decl.data.Amount *
            decl.data.product.capacity;
          xf.Workbook.Sheets[NumSheets].Cells[StartRow + j + SG.FixedRows,
            16].value := 0.1 * decl.data.Amount *
            decl.data.product.capacity;
          BorderRowXLS(StartRow + j + SG.FixedRows, 0, SG.ColCount - 1,
            NumSheets, xf, False);
          inc(row);
        end;
      end;
      SG.RowCount := SG.RowCount + 1;
      SG.MergeCells(1, SG.RowCount - 1, SG.ColCount - 2, 1);
      for j := 1 to SG.ColCount - 1 do
      begin
        SG.CellProperties[j, SG.RowCount - 1].FontSize := 9;
        SG.CellProperties[j, SG.RowCount - 1].FontStyle := [fsBold];
      end;
      s := '����� �� ������������� ' + String(decl.data.product.prod.pName)
        + ' � ���� ' + String(decl.data.product.FNSCode);
      SG.Cells[1, SG.RowCount - 1] := s;
      SG.RowColor[SG.RowCount - 1] := clTable;

      r := xf.Workbook.Sheets[NumSheets].Ranges.Add;
      r.AddRect(SG.RowCount - 1 + SG.FixedRows,
        SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 2);
      r.MergeCells;
      r.value := s;

      rez := SG.ColumnSum(SG.ColCount - 1, StartRow, SG.RowCount - 2);

      BorderRowXLS(SG.RowCount - 1 + SG.FixedRows, 0, SG.ColCount - 1,
        NumSheets, xf, True);
      if rez > 0 then
      begin
        SG.Floats[SG.ColCount - 1, SG.RowCount - 1] := rez;
        Result := Result + rez;
        xf.Workbook.Sheets[NumSheets].Cells[SG.RowCount - 1 + SG.FixedRows,
          SG.ColCount - 1].value := rez;

      end;
    end;

  finally
    SL_Pr.free;
    SL_PrData.free;
  end;
end;

procedure FilterDataFNSDecl(FNS: string; SL: TStringList;
  var SL_Data: TStringList);
var
  i: integer;
  decl: TDecl_obj;
begin
  SL_Data.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]);
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
      begin
        if String(TDecl_obj(SL.Objects[i]).data.product.FNSCode) = FNS then
          SL_Data.AddObject(SL.Strings[i], SL.Objects[i]);
      end;
  end;
end;

procedure FiltrSLPr(pId: string; SL: TStringList; var SL_Data: TStringList);
var
  i: integer;
  dv: TMove;
begin
  SL_Data.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    dv := TMove(SL.Objects[i]);
    if String(dv.product.prod.pId) = pId then
      SL_Data.AddObject(SL.Strings[i], SL.Objects[i]);
  end;
end;

procedure FiltrSLPrDecl(pId: string; SL: TStringList;
  var SL_Data: TStringList);
var
  i: integer;
  decl: TDecl_obj;
begin
  SL_Data.Clear;
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]);
    if String(decl.data.product.prod.pId) = pId then
      SL_Data.AddObject(SL.Strings[i], SL.Objects[i]);
  end;
end;

procedure ShapkaStat(SG: TADVStringGrid; Year: word);
begin
  SG.RowCount := 5;
  SG.FixedRows := 4;
  SG.MergeCells(0, 0, 1, 3);
  SG.ColWidths[0] := 200;
  SG.Alignments[0, 0] := taCenter;
  SG.Cells[0, 0] := '������������ �������� �����';
  SG.Cells[0, 3] := '1';

  SG.MergeCells(1, 0, 1, 3);
  SG.Alignments[1, 0] := taCenter;
  SG.Cells[1, 0] := '� ������';
  SG.Cells[1, 3] := '2';
  SG.ColWidths[1] := 50;

  SG.MergeCells(2, 0, 1, 3);
  SG.Alignments[2, 0] := taCenter;
  SG.Cells[2, 0] := '��� �� ������ ����';
  SG.Cells[2, 3] := '3';
  SG.ColWidths[2] := 100;

  SG.MergeCells(3, 0, 4, 1);
  SG.Alignments[3, 0] := taCenter;
  SG.Cells[3, 0] := '������� � ' + IntToStr(Year) + ' ����';
  // SG.Cells[3,3]:='4';
  // SG.ColWidths[3] := 70;

  SG.MergeCells(4, 1, 2, 1);
  SG.Alignments[2, 0] := taCenter;
  SG.Cells[2, 0] := '���';
  SG.Cells[2, 3] := '4';
  SG.ColWidths[1] := 280;
end;

procedure NullDviz(var dv: TDvizenie);
begin
  dv.ost0 := 0;
  dv.prih := 0;
  dv.rash := 0;
  dv.vozv_post := 0;
  dv.vozv_pokup := 0;
  dv.brak := 0;
  dv.ost1 := 0;
  dv.oder := 0;
  dv.move_in := 0;
  dv.move_out := 0;
end;

procedure SumDviz(var dv1: TDvizenie; dv2: TDvizenie);
begin
  dv1.ost0 := dv1.ost0 + dv2.ost0;
  dv1.prih := dv1.prih + dv2.prih;
  dv1.rash := dv1.rash + dv2.rash;
  dv1.vozv_post := dv1.vozv_post + dv2.vozv_post;
  dv1.vozv_pokup := dv1.vozv_pokup + dv2.vozv_pokup;
  dv1.brak := dv1.brak + dv2.brak;
  dv1.ost1 := dv1.ost1 + dv2.ost1;
  dv1.oder := dv1.oder + dv2.oder;
  dv1.move_in := dv1.move_in + dv2.move_in;
  dv1.move_out := dv1.move_out + dv2.move_out;
end;

procedure WriteSumDv(dv: TDvizenie; SG: TADVStringGrid; row: integer;
  xf: TXLSFile; NumSheets: integer);
begin
  if dv.ost0 > 0 then
  begin
    SG.Floats[6, row] := dv.ost0;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 6].value := dv.ost0;
  end;

  if dv.prih > 0 then
  begin
    SG.Floats[8, row] := dv.prih;
    SG.Floats[10, row] := dv.prih;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 8].value := dv.prih;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 10].value := dv.prih;
  end;

  if dv.vozv_pokup > 0 then
  begin
    SG.Floats[11, row] := dv.vozv_pokup;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 11].value := dv.vozv_pokup;
  end;

  if (dv.vozv_pokup + dv.prih) > 0 then
  begin
    SG.Floats[14, row] := dv.vozv_pokup + dv.prih;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5,
      14].value := dv.vozv_pokup + dv.prih;
  end;

  if dv.rash > 0 then
  begin
    SG.Floats[15, row] := dv.rash;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 15].value := dv.rash;
  end;

  if dv.brak > 0 then
  begin
    SG.Floats[16, row] := dv.brak;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 16].value := dv.brak;
  end;

  if dv.vozv_post > 0 then
  begin
    SG.Floats[17, row] := dv.vozv_post;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 17].value := dv.vozv_post;
  end;

  if (dv.rash + dv.vozv_post + dv.brak) > 0 then
  begin
    SG.Floats[19, row] := dv.rash + dv.vozv_post + dv.brak;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5,
      19].value := dv.rash + dv.vozv_post + dv.brak;
  end;

  if dv.ost1 > 0 then
  begin
    SG.Floats[20, row] := dv.ost1;
    xf.Workbook.Sheets[NumSheets].Cells[row + 5, 20].value := dv.ost1;
  end;
end;

procedure ImportExcelSG(SG: TADVStringGrid);
var
  XL, Workbook, ArrayData, Sheet: Variant;
  rez: HRESULT;
  i, j, BeginCol, BeginRow, ColCnt, RowCnt, col, ColFl: integer;
  ClassID: TCLSID;

  r_xls: TResourceStream;
  ms: TMemoryStream;
begin
  r_xls := TResourceStream.Create(HInstance, 'res_printf', RT_RCDATA);
  ms := TMemoryStream.Create;
  try

    ms.LoadFromStream(r_xls);
    ms.SaveToFile('c:\f1.xls');

    rez := CLSIDFromProgID(PWideChar(WideString('Excel.Application')),
      ClassID);
    if rez = S_OK then
      try
        // Screen.Cursor := crHourglass;
        BeginCol := 1; // ���������� ������ �������� ���� �������, � ������� ����� �������� ������
        BeginRow := 12;
        XL := CreateOleObject('Excel.Application'); // �������� Excel
        XL.DisplayAlerts := False;
        XL.Application.EnableEvents := False; // ��������� ������� Excel �� �������, ����� �������� ����� ����������
        Workbook := XL.WorkBooks.Open('c:\f1.xls');
        ColCnt := SG.ColCount;
        RowCnt := SG.RowCount - SG.FixedRows;
        XL.WorkBooks[1].WorkSheets[1].Name := '������� ��';
        ArrayData := VarArrayCreate([1, RowCnt, 1, ColCnt], varVariant);
        // ������� ���������� ������, ������� �������� ��������� �������
        col := 1;
        for j := 0 to RowCnt - 1 do
        begin
          col := 1;
          for i := 1 to ColCnt + SG.NumHiddenColumns - 1 do
            if SG.IsHiddenColumn(i) = False then
            begin
              if j = 0 then
                ArrayData[j + 1, col] := SG.Cells[i, j]
              else
              begin
                if i < ColFl then
                  ArrayData[j + 1, col] := SG.Cells[i, j]
                else
                  ArrayData[j + 1, col] := StrToFloat(SG.Cells[i, j]);
              end;
              inc(col);
            end;
        end;
      finally
        XL.DisplayAlerts := False;
        XL.ActiveWorkBook.Close;
        XL.Application.Quit;
        Sheet := UnAssigned;
        XL := UnAssigned;
      end;
  finally
    ms.free;
    r_xls.free;
  end;
  //
end;

procedure BorderRowXLS(row, ColStart, ColEnd, nSheet: integer;
  xls: TXLSFile; FontBold: Boolean);
var
  i: integer;
begin
  for i := ColStart to ColEnd do
  begin
    xls.Workbook.Sheets[nSheet].Cells[row, i].BorderColorIndex[xlBorderAll]
      := xlColorBlack;
    xls.Workbook.Sheets[nSheet].Cells[row, i].BorderStyle[xlBorderAll] :=
      bsThin;
    xls.Workbook.Sheets[nSheet].Cells[row, i].FontBold := FontBold;
  end;
end;

procedure ShapkaStatistic(SG: TADVStringGrid);
begin
  SG.MergeCells(0, 0, 1, 3);
  SG.Alignments[0, 0] := taCenter;
  SG.Cells[0, 0] := '������������ �������� �����';
  SG.Cells[0, 3] := '1';
  SG.Alignments[0, 3] := taCenter;
  SG.ColWidths[0] := 300;

  SG.MergeCells(1, 0, 1, 3);
  SG.Alignments[1, 0] := taCenter;
  SG.Cells[1, 0] := '� ������';
  SG.Cells[1, 3] := '2';
  SG.Alignments[1, 3] := taCenter;
  SG.ColWidths[1] := 50;

  SG.MergeCells(2, 0, 1, 3);
  SG.Alignments[2, 0] := taCenter;
  SG.Cells[2, 0] := '��� �� ������ ����';
  SG.Cells[2, 3] := '3';
  SG.Alignments[2, 3] := taCenter;
  SG.ColWidths[2] := 100;

  SG.MergeCells(3, 0, 4, 1);
  SG.Alignments[3, 0] := taCenter;
  SG.Cells[3, 0] := '������� �����';
  SG.MergeCells(3, 1, 2, 1);
  SG.Alignments[3, 1] := taCenter;
  SG.Cells[3, 1] := '���';
  SG.MergeCells(5, 1, 2, 1);
  SG.Alignments[5, 1] := taCenter;
  SG.Cells[5, 1] := '���. ���.';
  SG.Cells[3, 2] := '�����';
  SG.Alignments[3, 2] := taCenter;
  SG.RowHeights[2] := 60;
  SG.Cells[3, 3] := '4';
  SG.Alignments[3, 3] := taCenter;
  SG.ColWidths[3] := 75;
  SG.Cells[4, 2] := '� ��� ����� ���������� ������������';
  SG.Alignments[4, 2] := taCenter;
  SG.Cells[4, 3] := '5';
  SG.Alignments[4, 3] := taCenter;
  SG.ColWidths[4] := 75;
  SG.Cells[5, 2] := '�����';
  SG.Alignments[5, 2] := taCenter;
  SG.Cells[5, 3] := '6';
  SG.Alignments[5, 3] := taCenter;
  SG.ColWidths[5] := 75;
  SG.Cells[6, 2] := '� ��� ����� ���������� ������������';
  SG.Alignments[6, 2] := taCenter;
  SG.Cells[6, 3] := '7';
  SG.Alignments[6, 3] := taCenter;
  SG.ColWidths[6] := 75;

  SG.MergeCells(7, 0, 1, 3);
  SG.Alignments[7, 0] := taCenter;
  SG.Cells[7, 0] := '������ �� ����� ��������� �������';
  SG.Cells[7, 3] := '8';
  SG.Alignments[7, 3] := taCenter;
  SG.ColWidths[7] := 75;

  SG.Cells[0, 4] := '�������� ������� - �����';
  SG.Cells[0, 5] := #9#9 + '�� ���:' + #10#13 + #9 + '�����';
  SG.AutoSizeRow(5);
  SG.Cells[0, 6] := #9 +
    '�������������� ������� � ����������� ������ �� 25% ������������';
  SG.AutoSizeRow(6);
  SG.Cells[0, 7] := #9 +
    '�������������� ������� � ����������� ������ ����� 25%';
  SG.AutoSizeRow(7);
  SG.Cells[0, 8] := #9 +
    '�������, ��������� ������� (������� ������, ����������)';
  SG.AutoSizeRow(8);
  SG.Cells[0, 9] := #9 + '�����';
  SG.Cells[0, 10] := #9 +
    '������� ���������������� (� ����������� ��������� ������ �� ����� 9%)';
  SG.AutoSizeRow(10);
  SG.Cells[0, 11] := '�������������� ��������� - �����';
  SG.Cells[0, 12] := #9#9 + '� ��� �����:' + #10#13#9 + '���� ';
  SG.AutoSizeRow(12);
  SG.Cells[0, 13] := #9 + '���� ��������';
  SG.Cells[0, 14] := #9 + '���� ��������� (��������)';
  SG.Cells[0, 15] := #9 + '������� �������� � ����������';
  SG.Cells[0, 16] := #9 + '������� ������';
  SG.Cells[0, 17] := '����, ����� ��������� ������ � ������� ����������';
  SG.AutoSizeRow(17);
  SG.Cells[0, 18] := '������� ������������� �� ������ ����';
  SG.Cells[0, 19] :=
    '������ ����������� ��������� (����, �����, �������� � ��.)';
  SG.AutoSizeRow(19);
end;

procedure InsertNewEanXML(DBQ: TADOQuery; ean: string);
//var
//  qr: TADOQuery;
//  j: integer;
//  inn, kpp, FNS, gEAN: string;
begin
  // if SLFNS.Count = 0 then
  // FillSLFNS(SLFNS, FNSCode);
  // qr := TADOQuery.Create(DBQ.Owner);
  // try
  // try
  // qr.Connection := DBQ.Connection;
  // qr.ParamCheck := False;
  // qr.sql.Text := 'select * from producer';
  // qr.Open;
  // if not qr.Eof then
  // begin
  // qr.First;
  // while not qr.Eof do
  // begin
  // inn := qr.FieldByName('INN').AsString;
  // kpp := qr.FieldByName('KPP').AsString;
  // for j := 0 to SLFNS.Count - 1 do
  // begin
  // FNS := IntToStr(LongInt(SLFNS.Objects[j]));
  // gEAN := GenNewEAN('27', inn, kpp, FNS);
  // if gEAN = ean then
  // begin
  // // ������� ���������� ��� � ����
  /// /                  FNS := '';
  // //
  // end;
  // end;
  // qr.Next;
  // end;
  // end;
  // except
  //
  // end;
  // finally
  // qr.free;
  // end;
end;

procedure ColorEdit(NullTxt: string; ed: TEdit);
begin
  if ed.Text = NullTxt then
    ed.Color := clCheck
  else
    ed.Color := clWindow;
end;

procedure ColorEditMask(NullTxt: string; ed: TMaskEdit);
begin
  if (pos(NullTxt, ed.Text) > 0) or (pos(' ', ed.Text) > 0) then
    ed.Color := clCheck
  else
    ed.Color := clWindow;
end;

procedure UpdateServerProduction(EAN13: string; DBQ: TADOQuery;
  SLRow: TSTrings);
var
  butsel: integer;
  s, cont: string;
  recount, k: integer;
  qr2: TADOQuery;
  ms, ms1: TMemoryStream;
  er: Boolean;
  FSize: Int64;
  Str: array [1 .. 15] of AnsiString;
  TCP: TIdTCPClient;
begin
  qr2 := TADOQuery.Create(Application);
  qr2.Connection := fmWork.con2ADOCon;
  TCP := TIdTCPClient.Create(nil);
  TCP.Host:=BMHost;
  TCP.Port:=TCPPort;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    try
      s := '����� ��������� ������:' + #10#13 +
        '������������ ��������� = ''' + SLRow.Strings[4]
        + #10#13 + ''' EAN13 = ' + EAN13 + #10#13;
      butsel := Show_Inf(s + #10#13 + '����������?', fConfirm);
      if butsel <> mrOk then
        exit;
      if not TCP.Connected then
      begin
        TCP.Host := BMHost;
        TCP.Port:=TCPPort;
        TCP.Connect;
      end
      else
      begin

      end;

      if TCP.Connected then
        s := TCP.IOHandler.ReadLn;
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');
      TCP.IOHandler.WriteLn('INN ' + fOrg.inn + ' ' + exe_version);
      s := TCP.IOHandler.ReadLn;

      if SameText(String(s), 'RFEX') then
        TCP.IOHandler.WriteLn('OK');

      if SameText(String(s), 'OK') then
        TCP.IOHandler.WriteLn('EAN' + EAN13);
      MemoAdd('������ ����������� ��������� �� EAN13 ' + EAN13);
      cont := TCP.IOHandler.ReadLn;

      for recount := 1 to StrToInt(cont) do
      begin
        s := TCP.IOHandler.ReadLn;
        if Copy(s, 1, 2) = 'OK' then
        begin
          MemoAdd
            ('����� �� ������ ������. ����������� ����� ����������... ');
          FSize := StrToInt(Copy(String(s), 3, Length(String(s)) - 2));
          Application.ProcessMessages;

          try
            er := False;
            ms.Clear;
            ms1.Clear;
            TCP.IOHandler.ReadStream(ms, FSize);
            if not TCP.Connected then
              er := True;
            ms.Position := 0;
            if not UnZipFilePas(ms, ms1, psw_loc) then
              er := True;
            if er then
            begin
              MemoEdit('������ ������������');
              MemoAdd('����� ����������� ������� - ������ ������������');
            end;
            if not er and TCP.Connected then
            begin
              er := False;
              ms1.Position := 0;
              SetLength(Str[1], 13);
              Str[1] := AnsiString(ReadBuf(ms1, 13));
              SetLength(Str[2], 3);
              Str[2] := AnsiString(ReadBuf(ms1, 3));
              SetLength(Str[3], 180);
              Str[3] := AnsiString(ReadBuf(ms1, 180));
              SetLength(Str[4], 8);
              Str[4] := AnsiString(ReadBuf(ms1, 8));
              SetLength(Str[5], 8);
              Str[5] := AnsiString(ReadBuf(ms1, 8));
              SetLength(Str[6], 12);
              Str[6] := AnsiString(ReadBuf(ms1, 12));
              SetLength(Str[7], 9);
              Str[7] := AnsiString(ReadBuf(ms1, 9));
              SetLength(Str[8], 180);
              Str[8] := AnsiString(ReadBuf(ms1, 180));
              SetLength(Str[9], 8);
              Str[9] := AnsiString(ReadBuf(ms1, 8));
              SetLength(Str[10], 12);
              Str[10] := AnsiString(ReadBuf(ms1, 12));
              SetLength(Str[11], 9);
              Str[11] := AnsiString(ReadBuf(ms1, 9));
              SetLength(Str[12], 3);
              Str[12] := AnsiString(ReadBuf(ms1, 3));
              SetLength(Str[13], 2);
              Str[13] := AnsiString(ReadBuf(ms1, 2));
              SetLength(Str[14], 180);
              Str[14] := AnsiString(ReadBuf(ms1, 180));
              for k := 1 to 14 do
              begin
                Str[k] := AnsiString(TrimRight(String(Str[k])));
              end;
              try
                s := String(Str[9]);
                PrepareText(Str[3]);
                qr2.sql.Text :=
                  'Select EAN13 From Production where EAN13=''' + String
                  (Str[1]) + '''';
                qr2.Open;
                if qr2.RecordCount > 0 then
                begin
                  // ���������� ��������� � ��������� ����������� (� ����)
                  if qr2.RecordCount > 1 then
                  begin
                    qr2.sql.Text :=
                      'DELETE from Production WHERE EAN13=''' + String(Str[1]) + '''';
                    qr2.ExecSQL;
                    qr2.sql.Text :=
                      'INSERT INTO Production(EAN13, FNSCode, ' +
                      'Productname, AlcVol, Capacity, ProducerCode, ' +
                      'Prodkod, Prodcod)' + #13#10 + 'Values(''' + String
                      (Str[1]) + ''', ''' + String(Str[2])
                      + ''', ''' + String(Str[3]) + ''', ''' + String
                      (Str[4]) + ''', ''' + String(Str[5])
                      + ''', ''' + String(Str[9]) + ''', ''' + String
                      (Str[6]) + ''', ''' + String(Str[7]) + ''')';
                    qr2.ExecSQL;
                  end
                  else
                  begin
                    if qr2.RecordCount > 0 then
                      qr2.sql.Text := 'UPDATE Production ' + #13#10 +
                        'Set FNSCode=''' + String(Str[2])
                        + ''', Productname=''' + String(Str[3])
                        + ''', AlcVol=''' + String(Str[4])
                        + ''', Capacity=''' + String(Str[5])
                        + ''', ProducerCode=''' + String(Str[9])
                        + ''', Prodkod=''' + String(Str[6])
                        + ''', Prodcod=''' + String(Str[7])
                        + '''' + #13#10 + 'WHERE EAN13=''' + String
                        (Str[1]) + '''';
                    qr2.ExecSQL;
                    // ���������� ���������� ����������� � ����������
                  end;
                  if StrCheck(String(Str[9])) and not er then
                    try
                      Str[9] := AnsiString(TrimRight(String(s)));
                      PrepareText(Str[8]);
                      PrepareText(Str[14]);
                      qr2.sql.Text :=
                        'Select * From Producer where inn=''' + String(Str[6]) + ''' and kpp=''' + String(Str[7]) + '''';
                      qr2.Open;
                      if qr2.RecordCount > 0 then
                        qr2.sql.Text :=
                          'UPDATE Producer ' + #13#10 + 'Set pk=''' +
                          String(Str[9]) + ''', FullName=''' + String
                          (Str[8]) + ''', INN=''' + String(Str[10])
                          + ''', KPP=''' + String(Str[11])
                          + ''', countrycode=''' + String(Str[12])
                          + ''', RegionCode=''' + String(Str[13])
                          + ''', Address=''' + String(Str[14])
                          + '''' + #13#10 + 'WHERE inn=''' + String
                          (Str[6]) + ''' and kpp=''' + String(Str[7])
                          + ''''
                      else
                        qr2.sql.Text :=
                          'INSERT INTO Producer(PK, FullName, INN, KPP, ' +
                          'countrycode, RegionCode, Address)' + #13#10 +
                          'Values(' + String(Str[9]) + ', ''' + String
                          (Str[8]) + ''', ''' + String(Str[10])
                          + ''', ''' + String(Str[11])
                          + ''', ''' + String(Str[12]) + ''', ''' + String
                          (Str[13]) + ''', ''' + String(Str[14]) + ''')';
                      qr2.ExecSQL;
                    except
                      MemoEdit('������');
                      MemoAdd
                        ('������ ���������� ����������� ��������������');
                      er := True;
                    end;

                  UpdateMasProduction(Str, True);
                end
              except
                  on e:exception do
                  begin
                    MemoAdd(e.Message);
                    er := True;
                    MemoEdit('������');
                    MemoAdd('������ ���������� ����������� ���������');
                  end;
              end;
                if not er then
                begin
                  MemoEdit('���������');
                  MemoAdd(
                    '���������� ��������� ��������. ��������� ������ � EAN13 '
                      + EAN13);
                end
            end; // if not Er
          finally

          end;

        end;
      end;

    except

    end;
  finally
    qr2.free;
    ms1.free;
    ms.free;
    TCP.Socket.Close;
    TCP.Disconnect;
    TCP.Free;
  end;
end;

procedure UpdateServerSaler(PK: string; DBQ: TADOQuery;
  SLRow: TSTrings);
var
  butsel: integer;
  s: string;
  TCP: TIdTCPClient;
begin

  TCP := TIdTCPClient.Create(nil);
  try
    TCP.Host:=BMHost;
    TCP.Port:=TCPPort;
    s := '����� ��������� ������:' + #10#13 +
      '������������ ���������� = ''' + SLRow.Strings[2]
      + '''' + #10#13 + '��� = ' + SLRow.Strings[3]
      + '    ��� = ' + SLRow.Strings[4] + '.' + #10#13;
    butsel := Show_Inf(s + #10#13 + '����������?', fConfirm);
    if butsel <> mrOk then
      exit;

    TCP.Connect;

    if TCP.Connected then
      s := TCP.IOHandler.ReadLn;
    if Copy(s, 1, 20) = 'Connected to server.' then
      MemoAdd('C�����: �� ���������� � �������');
    TCP.IOHandler.WriteLn('INN ' + fOrg.inn + ' ' + exe_version);
    s := TCP.IOHandler.ReadLn;

    if SameText(String(s), 'RFEX') then
      TCP.IOHandler.WriteLn('OK');

    if SameText(String(s), 'OK') then
    begin
    end;
  finally
    TCP.Disconnect;
    TCP.Free;
  end;

end;

// ���������� ���������� ��������� � ��������� ��������
procedure UpdateMasProduction(Str: array of AnsiString; Updt:Boolean);
var
  i, idx: integer;
  decl: TDeclaration;
  SLRow: TStringList;
  // s:string;
  // b:Boolean;
begin
  for i :=
    fmRef.SG_Product.FixedRows to fmRef.SG_Product.RowCount - 1 do
    if String(Str[0]) = fmRef.SG_Product.Cells[2, i] then
    begin
      fmRef.SG_Product.Cells[3, i] := String(Str[1]); // FNSCode
      fmRef.SG_Product.Cells[4, i] := String(Str[2]); // Productname
      fmRef.SG_Product.Cells[5, i] := String(Str[3]); // AlcVol
      fmRef.SG_Product.Cells[6, i] := String(Str[4]); // Capacity
      fmRef.SG_Product.Cells[7, i] := String(Str[7]); // pName
      fmRef.SG_Product.Cells[8, i] := String(Str[9]); // pInn
      fmRef.SG_Product.Cells[9, i] := String(Str[10]); // pInn
    end;
  // ���������� ��������� ���������� (��� ������)
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    decl := TDecl_obj(SLAllDecl.Objects[i]).data;
    if decl.product.EAN13 = Str[0] then
    begin
      decl.product.FNSCode := ShortString(Str[1]);
      decl.product.Productname := Str[2];

      decl.product.AlcVol := StrToFloat(String(Str[3]));
      decl.product.capacity := StrToFloat(String(Str[4]));
      decl.product.prod.pId := ShortString(Str[8]);
      decl.product.prod.pINN := ShortString(Str[9]);
      decl.product.prod.pKPP := ShortString(Str[10]);
      decl.product.prod.pName := ShortString(Str[7]);
      decl.product.prod.pAddress := ShortString(Str[13]);
      decl.product.ProductNameF := ShortString
        (FullNameProduction(String(decl.product.Productname),
          decl.product.AlcVol, decl.product.capacity));
      TDecl_obj(SLAllDecl.Objects[i]).data := decl;
    end;
  end;
  // ���������� ����������� ���������
  idx := RefProduction.IndexOf(String(Str[0]));
  if idx <> -1 then
  begin
    SLRow := Pointer(RefProduction.Objects[idx]);
    SLRow.Strings[1] := String(Str[1]); // FNSCode
    SLRow.Strings[2] := String(Str[2]); // Productname
    SLRow.Strings[3] := String(Str[3]); // AlcVol
    SLRow.Strings[4] := String(Str[4]); // Capacity
    SLRow.Strings[8] := String(Str[9]); // pInn
    SLRow.Strings[9] := String(Str[10]); // pKpp
  end;
  if Updt then
    fmMove.cbAllKvartChange(fmMove.cbAllKvart);

end;

function DownloadFile(SourceFile, DestFile: string): Boolean;
begin
  try
    Result := UrlDownloadToFile(nil, PChar(SourceFile),
      PChar(DestFile), 0, nil) = 0;
  except
    Result := False;
  end;
end;

function GetInetFile(const fileURL, filename: String): Boolean;
const
  BufferSize = 1024;
var
  hSession, hURL: HInternet;
  Buffer: array [1 .. BufferSize] of Byte;
  BufferLen: DWORD;
  f: File;
  sAppName: String;
begin
  Result := False;
  sAppName := ExtractFileName(Application.ExeName);
  hSession := InternetOpen(PChar(sAppName),
    INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  try
    hURL := InternetOpenURL(hSession, PChar(fileURL), nil, 0, 0, 0);
    try
      AssignFile(f, filename);
      Rewrite(f, 1);
      repeat
        InternetReadFile(hURL, @Buffer, SizeOf(Buffer), BufferLen);
        BlockWrite(f, Buffer, BufferLen)
      until BufferLen = 0;
      CloseFile(f);
      Result := True;
    finally
      InternetCloseHandle(hURL)
    end
  finally
    InternetCloseHandle(hSession)
  end
end;

// ��������� ��������� ��� ����������
procedure FillDataStatistic(SLOborot: TStringList;
  var SLStat: TStringList);
var
  i: integer;
  dMove: TMove;
  dStat: TStat;
begin
  FreeStringList(SLStat);
  SLStat := TStringList.Create;
  try
    for i := 0 to SLOborot.Count - 1 do
    begin
      dMove := Pointer(SLOborot.Objects[i]);
      if (dMove <> nil) and (dMove.product.Productname <> NoProd) then
      begin
        dStat := TStat.Create;

        dStat.EAN13 := dMove.product.EAN13;
        dStat.FNSCode := StrToInt(String(dMove.product.FNSCode));
        dStat.Productname := dMove.product.Productname;
        dStat.Prodaza := dMove.dv_dal.rash;
        dStat.Ostatok := dMove.dv_dal.ost1;
        dStat.InStat := False;

        SLStat.AddObject(String(dStat.EAN13), dStat);
      end;
    end;
  finally
//    SLStat.Free;
  end;

end;

procedure FillMasStatistic(SL: TStringList; SG: TADVStringGrid);
const
  Imp: array [1 .. 16] of integer = (0, 9, 11, 11, 50, 100, 11, 0,
    35, 35, 35, 35, 35, 18, 17, 13);
  kod: array [1 .. 16] of String = ('15.91.10.002*',
    '15.91.10.111*', '15.91.10.103*',
    '15.91.10.106*', '15.91.10.108*',
    '15.91.10.112*', '15.94.10.101*',
    '15.93.94.002*', '15.93.12.102*',
    '15.93.12.201*', '15.93.12.106*',
    '15.93.11.101*', '15.94.10.504*',
    '15.96.10.004*', '15.96.10.003*',
    '15.94.10.011*');
  col: array [0 .. 4] of integer = (3, 4, 5, 6, 7);

var
  i, j, row: integer;
  dStat: TStat;
  MasStat: array [1 .. 16, 1 .. 5] of Single;
  find: Boolean;
  s: string;
  tIniF: TIniFile;
  fn: string;
  sl1: TSTrings;
  CenaR, CenaI: array [1 .. 16] of Extended;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  sl1 := TStringList.Create;
  try
    tIniF.ReadSectionValues('Cena', sl1);
    for i := 0 to sl1.Count - 1 do
    begin
      s := sl1.ValueFromIndex[i];
      if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
        s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
        s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      CenaR[i + 1] := StrToFloat(s);
    end;
    sl1.Clear;
    tIniF.ReadSectionValues('CenaImp', sl1);
    for i := 0 to sl1.Count - 1 do
    begin
      s := sl1.ValueFromIndex[i];
      if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
        s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
        s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      CenaI[i + 1] := StrToFloat(s);
    end;

    for i := Low(MasStat) to High(MasStat) do
      for j := 1 to 5 do
        MasStat[i, j] := 0;

    for i := 0 to SL.Count - 1 do
    begin
      dStat := Pointer(SL.Objects[i]);
      find := False;
      row := -1;
      s := String(dStat.Productname);

      if row <> -1 then
        find := True;

      if find then
      begin
        MasStat[row, 1] := RoundTo(MasStat[row, 1] + dStat.Prodaza,
          -1);
        MasStat[row, 5] := RoundTo(MasStat[row, 5] + dStat.Ostatok,
          -1);
        dStat.InStat := True;
      end;
    end;

    for i := 0 to SL.Count - 1 do
    begin
      dStat := Pointer(SL.Objects[i]);
      find := False;
      row := -1;
      if not dStat.InStat then
        case dStat.FNSCode of
          200:
            row := 2;
          211, 270:
            row := 3;
          212:
            row := 4;
          229, 230, 231, 232, 238, 239, 241, 242, 252, 300:
            row := 5;
          280:
            row := 6;
          260:
            row := 7;
          400, 401, 402, 403, 410, 460:
            row := 9;
          411:
            row := 10;
          420, 421:
            row := 11;
          440, 450:
            row := 12;
          250, 251, 461, 462:
            row := 13;
          500, 510:
            row := 14;
          520:
            row := 15;
          261,262,263:
            row := 16;
        end;
      if row <> -1 then
        find := True;

      if find then
      begin
        MasStat[row, 1] := RoundTo(MasStat[row, 1] + dStat.Prodaza,
          -1);
        MasStat[row, 5] := RoundTo(MasStat[row, 5] + dStat.Ostatok,
          -1);
        dStat.InStat := True;
      end;
    end;

    for i := 2 to High(MasStat) do
    begin
      MasStat[i, 2] := RoundTo(MasStat[i, 1] * Imp[i] / 100, -1);
      MasStat[i, 3] := RoundTo(MasStat[i, 1] * (CenaR[i] / 100),
        -1);
      MasStat[i, 4] := RoundTo(MasStat[i, 2] * (CenaI[i] / 100),
        -1);
    end;

    for i := 0 to High(col) do
    begin
      MasStat[1, col[i] - 2] := MasStat[2, col[i] - 2] + MasStat[3,
        col[i] - 2] + MasStat[4, col[i] - 2] + MasStat[5,
        col[i] - 2] + MasStat[6, col[i] - 2] + MasStat[7,
        col[i] - 2];
      MasStat[8, col[i] - 2] := MasStat[9, col[i] - 2] + MasStat
        [10, col[i] - 2] + MasStat[11, col[i] - 2] + MasStat[12,
        col[i] - 2] + MasStat[13, col[i] - 2];
    end;
    try
      row := 1;
      for i := SG.FixedRows to SG.RowCount - 1 do
      begin
        for j := 0 to SG.ColCount - 1 do
          case row of
            1, 8, 14, 15, 16:
              SG.CellProperties[j, row + SG.FixedRows - 1]
                .FontStyle := [fsBold];
          end;
        s := IntToStr(row + 8);
        SG.Cells[1, row + SG.FixedRows - 1] := s;

        s := kod[row];
        SG.Cells[2, row + SG.FixedRows - 1] := s;

        for j := 0 to High(col) do
        begin
          s := FloatToStrF(MasStat[row, col[j] - 2], ffFixed,
            maxint, 2);
          SG.Cells[col[j], row + SG.FixedRows - 1] := s;
        end;
        inc(row);
      end;
    except
    end;
  finally
    sl1.free;
    tIniF.free;
  end;
end;

procedure EnabledMainBut(En: Boolean);
var
  i, j: integer;
begin
  for i := 0 to fmWork.Panel_JvRoll.ControlCount - 1 do
    if fmWork.Panel_JvRoll.Controls[i] is TJvRollOut then
    begin
      if En then
      begin (fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
        .Colors.Color := $00F0FFF1;
(fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
        .Colors.ButtonColor := $00CBE2CB;
      end
      else
      begin (fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
        .Colors.Color := $00E5E5E5;
(fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
        .Colors.ButtonColor := clSilver
      end; (fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
      .Enabled := En;

      for j := 0 to (fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
        .ControlCount - 1 do
      begin
        if (fmWork.Panel_JvRoll.Controls[i] as TJvRollOut)
          .Controls[j] is TLabel then
((fmWork.Panel_JvRoll.Controls[i] as TJvRollOut).Controls[j] as TLabel)
          .Enabled := En;
      end;
    end;
end;

function FindErrXSD(fXML: string; f11, f43: Boolean;
  var sErr: string): Boolean;
var
  ms: TMemoryStream;
  r: TResourceStream;
  s1: integer;
  NewPath, fXsd, S2: string;
  Validator: TXSDValidator;
begin
  Result := False;

  if f11 then
    r := TResourceStream.Create(HInstance, '11_o', RT_RCDATA)
  else
    r := TResourceStream.Create(HInstance, '12_o', RT_RCDATA);

  ms := TMemoryStream.Create;
  try
    try
      ms.LoadFromStream(r);

      NewPath := WinTemp + NameProg;
      if not DirectoryExists(NewPath) then
        MkDir(NewPath);

      fXsd := NewPath + '\tmp.xsd';
      if FileExists(fXsd) then
        DeleteFile(PWideChar(fXsd));

      ms.SaveToFile(fXsd);

      Validator := TXSDValidator.Create;

      if (Validator.ParseXmlFile(fXML)) then
        if (Validator.AddXsdFile(fXsd)) then
          if (Validator.Valid(s1, S2)) then
            sErr := XmlValid
          else
            sErr := Validator.error
        else
          sErr := Validator.error
      else
        sErr := Validator.error;

      if sErr <> XmlValid then
        Result := False
      else
        Result := True;
    finally
      FreeAndNil(ms);
      r.free;
      if DirectoryExists(NewPath) then
        RemoveDir(NewPath)
//        DelDir(NewPath);
//      FreeAndNil(Validator);
//      Validator.Destroy;
    end;
  except
  end;
end;

function GetCurDate(kv: integer): TDateTime;
var
  cYear, cMonth, cDay: word;
begin
  DecodeDate(Now(), cYear, cMonth, cDay);
  if cYear = NowYear then
    Result := Now
  else
    Result := Kvart_End(kv, NowYear);
end;

function UpdateKppBD(kpp1, kpp2: string; DBQ: TADOQuery): Boolean;
var
  qr: TADOQuery;
  StrSQL: string;
begin
  Result := False;
  qr := TADOQuery.Create(DBQ.Owner);
  TRY
    qr.Connection := DBQ.Connection;
    qr.ParamCheck := False;
    StrSQL := 'UPDATE Declaration SET ' + 'SelfKPP = ''' + kpp2 +
      ''' WHERE PK in (SELECT PK FROM DECLARATION WHERE SELFKPP = '''
      + kpp1 + ''')';
    qr.sql.Text := StrSQL;
    //
    // qr.Parameters.AddParameter.Name := 'KPP1';
    // qr.Parameters.ParamByName('KPP1').DataType := ftString;
    // qr.Parameters.ParamByName('KPP1').value := KPP1;
    //
    // qr.Parameters.AddParameter.Name := 'KPP2';
    // qr.Parameters.ParamByName('KPP2').DataType := ftString;
    // qr.Parameters.ParamByName('KPP2').value := kpp2;
    qr.ExecSQL;
    Result := True;
  finally
    qr.free;
  end;
end;

function UpdateKppDecl(kpp1, kpp2: string): Boolean;
var
  i: integer;
  decl: TDecl_obj;
begin
  Result := False;
  try
    for i := 0 to SLAllDecl.Count - 1 do
    begin
      decl := Pointer(SLAllDecl.Objects[i]);
      if decl.data.SelfKPP = ShortString(kpp1) then
        decl.data.SelfKPP := ShortString(kpp2);
      Result := True;
    end;
  except
    Result := False;
  end;
end;

procedure GetDateKvart(s: string; var DB, dE: TDateTime);
var
  cKvart, cYear: string;
begin
  if Length(s) > 0 then
  begin
    cKvart := Copy(s, 0, pos(nkv, s) - 2);
    cYear := Copy(s, Length(s) - 8, 4);
    DB := Kvart_Beg(StrToInt(cKvart), StrToInt(cYear));
    dE := Kvart_End(StrToInt(cKvart), StrToInt(cYear));
  end;
end;

procedure ShowDataCorectSalerFull(SG: TADVStringGrid;
  SLSaler: TStringList; SalerInn: string);
var
  i: integer;
  SL, SLRow: TStringList;

begin
  SL := TStringList.Create;
  try
    for i := 0 to SLSaler.Count - 1 do
      if TSalerDvizenie(SLSaler.Objects[i]) <> nil then
        if TSalerDvizenie(SLSaler.Objects[i]).sINN = SalerInn then
        begin
          SLRow := TStringList.Create;
          SLRow.Add(TSalerDvizenie(SLSaler.Objects[i]).SelfKPP);
          SLRow.Add(FloatToStrF(TSalerDvizenie(SLSaler.Objects[i])
                .prihod_dal, ffFixed, maxint, ZnFld));
          SL.AddObject(TSalerDvizenie(SLSaler.Objects[i]).sINN,
            SLRow);
        end;
    SG.RowCount := SG.FixedRows + SL.Count + SG.FixedFooters;
    for i := 0 to SL.Count - 1 do
    begin
      SLRow := Pointer(SL.Objects[i]);
      SG.Cells[1, i + SG.FixedRows] := SLRow.Strings[0];
      SG.Cells[2, i + SG.FixedRows] := SLRow.Strings[1];
    end;
    SG.AutoNumberCol(0);
    SG.ColumnSize.Rows := arNormal;
    SG.AutoSize := True;
    SG.FloatingFooter.ColumnCalc[2] := acSum;
    SG.CalcFooter(2);
  finally
    FreeStringList(SL);
  end;
end;

procedure ShowDataCorectSaler(SG: TADVStringGrid;
  SLData: TStringList);
var
  i, idx: integer;
  SLAllCorrect: TStringList;
begin
  SLAllCorrect := TStringList.Create;
  try
    SumCorrectData(SLData, SLAllCorrect, True);
    WriteSGCorrect(SG, SLAllCorrect);
  finally
    SLAllCorrect.free;
  end;
  SG.AutoNumberOffset := 0;
  SG.AutoNumberStart := 0;

  SG.AutoNumberCol(0);

  SG.ColumnSize.Rows := arFixed;
  SG.NoDefaultDraw := True;
  SG.AutoSize := True;

  SG.FloatingFooter.ColumnCalc[0] := acCount;
  SG.FloatingFooter.ColumnCalc[12] := acSum;
  SG.FloatingFooter.ColumnCalc[17] := acSum;
  SG.CalcFooter(12);
  SG.CalcFooter(17);
  // ������� ������� ��������
  for i := 0 to High(HideColCorrectSaler) do
  begin
    idx := HideColCorrectSaler[i];
    SG.HideColumn(idx);
  end;

end;

procedure PrihodSalerSKpp(var SLEan: TStringList;
  SLData: TStringList; sKpp: string; sal: TSaler;
  DB, dE: TDateTime);
var
  i: integer;
begin
  ClearSL(SLEan);
  for i := 0 to SLData.Count - 1 do
    if TDecl_obj(SLData.Objects[i]) <> nil then
    begin
      if (String(TDecl_obj(SLData.Objects[i]).data.SelfKPP) = sKpp) and
         (String(TDecl_obj(SLData.Objects[i]).data.sal.sINN) = String(sal.sINN)) and
         (String(TDecl_obj(SLData.Objects[i]).data.sal.sKpp) = String(sal.sKpp)) and
         (TDecl_obj(SLData.Objects[i]).data.sal.sINN <> '1234567894') and
         (TDecl_obj(SLData.Objects[i]).data.DeclDate <= dE) and
         (TDecl_obj(SLData.Objects[i]).data.TypeDecl = 1) and
         (TDecl_obj(SLData.Objects[i]).data.DeclDate >= DB) then
        SLEan.AddObject(String(TDecl_obj(SLData.Objects[i]).data.PK),
          TDecl_obj(SLData.Objects[i]));
    end;
end;

procedure FillDataFNS(SLData: TStringList; FNS: string;
  var SLAll: TStringList; AllData: Boolean);
var
  i: integer;
  decl: TDeclaration;
  SL, SLDate: TStringList;
  s: string;
begin
  SL := TStringList.Create;
  SLDate := TStringList.Create;

  try
    // ��������� ������ � ������� ����� ���������
    for i := 0 to SLData.Count - 1 do
    begin
      NullDecl(decl);
      decl := TDecl_obj(SLData.Objects[i]).data;
      if decl.product.FNSCode = ShortString(FNS) then
        SL.AddObject(SLData.Strings[i], SLData.Objects[i]);
    end;

    // ��������� ������ ��� ��� �������� ���� ���������
    for i := 0 to SL.Count - 1 do
    begin
      NullDecl(decl);
      decl := TDecl_obj(SL.Objects[i]).data;
      s := DateToStr(decl.DeclDate);
      if SLDate.IndexOf(s) = -1 then
        SLDate.Add(s);
    end;
    SLDate.CustomSort(CompDateAsc);
    // ���������� ������� ��� ������� ���� �� � ������ ����
    for i := 0 to SLDate.Count - 1 do
      FillDataCorrectDate(SL, SLDate.Strings[i], SLAll, AllData);
  finally
    SLDate.free;
    SL.free;
  end;
end;

procedure FillDataCorrectDate(SLData: TStringList; sDate: string;
  var SLAll: TStringList; AllData: Boolean);
var
  i: integer;
  decl: TDeclaration;
  SL, SLNackl: TStringList;
  s: string;
begin
  SL := TStringList.Create;
  SLNackl := TStringList.Create;

  try
    // ��������� ������ � ������� �����
    for i := 0 to SLData.Count - 1 do
    begin
      decl := TDecl_obj(SLData.Objects[i]).data;
      if DateToStr(decl.DeclDate) = sDate then
        SL.AddObject(SLData.Strings[i], SLData.Objects[i]);
    end;

    // ������ ��������� ��� ������� ����
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]).data;
      s := String(decl.DeclNum);
      if SLNackl.IndexOf(s) = -1 then
        SLNackl.Add(s);
    end;
    SLNackl.Sort;
    // ���������� ������� ��� ������� ���� �� � ������ ����
    for i := 0 to SLNackl.Count - 1 do
      FillDataCorrectDateNackl(SL, sDate, SLNackl.Strings[i], SLAll, AllData);

  finally
    SLNackl.free;
    SL.free;
  end;
end;

procedure FillDataCorrectDateNackl(SLData: TStringList;
  sDate, Nackl: string; var SLAll: TStringList;
  AllData: Boolean);
var
  i: integer;
  decl: TDeclaration;
  vol: Extended;
  SLRow: TStringList;
begin
  vol := 0;
  SLRow := TStringList.Create;
  SLRow.Add(sDate);
  SLRow.Add(Nackl);
  try
    for i := 0 to SLData.Count - 1 do
    begin
      decl := TDecl_obj(SLData.Objects[i]).data;
      decl.vol := 0.1 * decl.Amount * decl.product.capacity;
      if decl.DeclNum = ShortString(Nackl) then
      begin
        vol := vol + decl.vol;
        if AllData then
          SLAll.AddObject(SLData.Strings[i], SLData.Objects[i]);
      end;
    end;
    SLRow.Add(FloatToStr(vol));
    SLAll.AddObject(sDate + Nackl, Pointer(SLRow));
  finally
  end;
end;

procedure WriteSGCorrect(SG: TADVStringGrid; SLData: TStringList);
var
  i, row, idx: integer;
  decl: TDeclaration;
  SL, SLRow: TStringList;
begin
  if SLData.Count = 0 then
    exit;
  SL := TStringList.Create;
  try
    SG.RowCount := SLData.Count + SG.FixedFooters + SG.FixedRows;

    for i := 0 to SLData.Count - 1 do
    begin
      row := i + SG.FixedRows;
      if SLData.Objects[i] is TDecl_obj then
      begin
        decl := TDecl_obj(SLData.Objects[i]).data;
        SG.RowColor[row] := clNone;
        SG.Objects[0, row] := Pointer(0);
        decl.forma := FormDecl(String(decl.product.FNSCode));
        decl.vol := 0.1 * decl.Amount * decl.product.capacity;

        SG.Cells[19, row] := String(decl.PK);

        SG.Cells[1, row] := IntToStr(decl.forma);

        SG.Cells[2, row] := String(decl.product.FNSCode);
        SG.RowEnabled[row] := True;

        SG.Cells[3, row] := String(decl.SelfKPP);
        idx := CheckSelfKpp(String(decl.SelfKPP));

        SG.Cells[4, row] := String(decl.DeclNum);
        if decl.TypeDecl = 1 then
        begin
          idx := CheckValueCells(String(decl.DeclNum));
          SetColorCells(SG, [4], row, idx);
        end;

        SG.Cells[5, row] := DateToStr(decl.DeclDate);
        idx := CheckDate(decl.DeclDate);
        SetColorCells(SG, [5], row, idx);

        SG.Cells[6, row] := String(decl.sal.sINN);
        SG.Cells[7, row] := String(decl.sal.sKpp);
        SG.Cells[8, row] := String(decl.sal.sName);
        idx := CheckSaler(decl.sal);
        if idx <> -1 then
          AddDataExchange(idx,
            cQueryINN + String(decl.sal.sINN) + AddSal2 + String
              (decl.sal.sKpp));

        SetColorCells(SG, [6, 7, 8], row, idx);

        SG.Cells[9, row] := String(decl.product.EAN13);
        SG.Cells[10, row] := String(decl.product.ProductNameF);
        idx := CheckProduction(decl.product);
        if idx <> -1 then
          AddDataExchange(idx, cQueryEAN + String(decl.product.EAN13));
        SetColorCells(SG, [9, 10], row, idx);

        SG.Cells[11, row] := FloatToStrF(decl.Amount, ffFixed,
          maxint, ZnFld);
        SetColorCells(SG, [11], row,
          CheckChislo(True, decl.Amount));

        SG.Cells[12, row] := FloatToStrF(decl.vol, ffFixed, maxint,
          ZnFld);
        SetColorCells(SG, [12], row, CheckChislo(True, decl.vol));

        SG.Cells[13, row] := String(decl.TM);

        SG.Cells[14, row] := String(decl.product.prod.pName);
        SG.Cells[15, row] := String(decl.product.prod.pINN);
        SG.Cells[16, row] := String(decl.product.prod.pKPP);
        idx := CheckProducer(decl.product.prod);
        if idx <> -1 then
          AddDataExchange(2, cQueryEAN + String(decl.product.EAN13));
        SetColorCells(SG, [14, 15, 16], row, idx);
      end;
      if SLData.Objects[i] is TStringList then
      begin
        SG.MergeCells(11, row, 2, 1);
        SLRow := Pointer(SLData.Objects[i]);
        SL.AddObject(SLData.Strings[i], SLRow);
        SG.Alignments[11, row] := taRightJustify;
        SG.CellProperties[11, row].FontSize := 9;
        SG.CellProperties[11, row].FontStyle := [fsBold];

        SG.Cells[11, row] := SLRow.Strings[2];
        SG.Cells[4, row] := SG.Cells[5,
          row - 1] + SG.Cells[4, row - 1];
        SG.MergeCells(9, row, 2, 1);
        SG.Alignments[9, row] := taRightJustify;
        SG.CellProperties[9, row].FontStyle := [fsBold];
        SG.CellProperties[9, row].FontSize := 9;
        SG.Cells[9, row] := cDate + SLRow.Strings[0] + cNacl + SLRow.Strings[1];
      end;
    end;

    SG.ColumnSize.Rows := arFixed;
    SG.AutoSize := True;
    SG.Resize;
  finally
    FreeStringList(SL);
  end;
end;

function ReadXMLCorrect(fName: String; var SLRez: TStringList;
  DBQ: TADOQuery; sal: TSaler; sKppXML:string):integer;
var
  fOtch: TTypeXML;
  v:boolean;
begin
  fOtch := GetTypeXML(fName);
  case fOtch of
    t_12o: Result:=Load_XML_f12(SLRez, fName, DBQ, CurDate, sKppXML);
    t_11o: Result:=Load_XML_f11(SLRez, fName, DBQ, CurDate, sal, sKppXML);
    t_6o: Result:=Load_XML_f6(SLRez, fName, DBQ, CurDate, sal, sKppXML);
    t_o: Result:=Load_XML(fPrihod, SLRez, fName, DBQ, CurDate, sal, sKppXML, v);
  end;
end;

function ReadXML(fTF: TTypeForm;Xml: IXMLDocument; var SLXml, SLErrXML: TStringList;
  DBQ: TADOQuery; sal: TSaler; dat: TDateTime; SelfKppXML:string;
  var ExtVozvr:Boolean): integer;
var
  NFile, NDoc, nOb, nPr, nProd: IXMLNode;
  i, j, k, XMLObCount, XMLPrCoumt, XMLProdCount,l: integer;
  GrFNS, DNacl, NNacl, TM, Amount, s: string;
  V: Extended;
  decl: TDeclaration;
  fl: Boolean;
  pXML: fPrihXML;
  producer: TProducer;
  SLEan: TStringList;
  rowXML: integer;
  err, errRow, errFns, errPr, IsWrite: Boolean;
  SL: TStringList;
begin
  Result :=0;
  ExtVozvr :=False;
  pXML.sINN := String(sal.sINN);
  pXML.sKpp := String(sal.sKpp);
  err := False;
  SLEan := TStringList.Create;
  try
    try
      NFile := Xml.DocumentElement;
      NDoc := NFile.ChildNodes.FindNode('��������');
      if NDoc <> nil then
      begin
        nOb := NDoc.ChildNodes.FindNode('������');
        if nOb <> nil then
        begin
          XMLObCount := NDoc.ChildNodes.Count;
          for i := 0 to XMLObCount - 1 do
          begin
            nOb := NDoc.ChildNodes.Nodes[i];
            errFns := False;
            try
              GrFNS := String(nOb.Attributes['�000000000003']);
            except
              errFns := True;
              GrFNS := '';
            end;
            if GrFNS = '' then
            begin
              s := TrimLeft
                (Copy(nOb.Xml, 0, pos(#$d#$a, nOb.Xml) - 1));
              rowXML := GetRowXML(Xml.Xml, s);
              s := '������ ' + IntToStr(rowXML) + ' ';
              s := s +
                '����������� ������������ ���� ��� ����������� ���������';
              SLErrXML.Add(s);
              errFns := True;
            end;
            decl.product.FNSCode := ShortString(GrFNS);
            // ����������������
            if pXML.ProdName = '' then
              pXML.ProdName := '��������� ������ ' + GrFNS;
            nPr := nOb.ChildNodes.FindNode('����������������');
            if nPr <> nil then
            begin
              XMLPrCoumt := nOb.ChildNodes.Count;
              for j := 0 to XMLPrCoumt - 1 do
              begin
                nPr := nOb.ChildNodes.Nodes[j];
                errPr := False;
                try
                  pXML.pName := String(nPr.Attributes['NameOrg']);
                except
                  errPr := True;
                  pXML.pName := '';
                end;
                try
                  pXML.pINN := String(nPr.Attributes['INN']);
                except
                  errPr := True;
                  pXML.pINN := '';
                end;

                l := Length(pXML.pINN);
                case l of
                  8:
                    try
                      pXML.pINN := CreateNewInn(pXML.pINN);
                      pXML.pKPP := NullKPP8;
                    except
                      pXML.pKPP := NullKPP8;
                    end;
                  9:
                    try
                      pXML.pINN := CreateNewInn(pXML.pINN);
                      pXML.pKPP := NullKPP9;
                    except
                      pXML.pKPP := NullKPP9;
                    end;
                  10:
                    try
                      pXML.pKPP := String(nPr.Attributes['KPP']);
                    except
                      pXML.pKPP := '';
                    end;
                  12:
                    try
                      pXML.pINN := CreateNewInn(pXML.pINN);
                      pXML.pKPP := NullKPP9;
                    except
                      pXML.pKPP := NullKPP9;
                    end;
                end;

                producer.pINN := ShortString(pXML.pINN);
                producer.pKPP := ShortString(pXML.pKPP);

                fl := GetProducer(fmWork.que2_sql, producer, True,
                  False);
                if not fl then
                begin
                  producer.pName := ShortString(pXML.pName);
                  producer.pAddress := '������';
                end;

                s := TrimLeft
                  (Copy(nPr.Xml, 0, pos(#$d#$a, nPr.Xml) - 1));
                rowXML := GetRowXML(Xml.Xml, s);
                GetErrProducer_XML(SLErrXML, producer, rowXML);
                if (pXML.pKPP = '') or (pXML.pINN = '') then
                  errPr := True;

                nProd := nPr.ChildNodes.FindNode('���������');
                if nProd <> nil then
                begin
                  XMLProdCount := nPr.ChildNodes.Count;
                  decl.product.prod := producer;
                  // �������� �� ������ �������������
                  for k := 0 to XMLProdCount - 1 do
                  begin
                    IsWrite := False;
                    errRow := False;
                    Application.ProcessMessages;
                    decl.SelfKPP := ShortString(SelfKppXML);

                    decl.sal := sal;

                    nProd := nPr.ChildNodes.Nodes[k];
                    try
                      DNacl := String
                        (nProd.Attributes['�200000000013']);
                    except
                      DNacl := '';
                      errRow := True;
                    end;
                    try
                      NNacl := String
                        (nProd.Attributes['�200000000014']);
                    except
                      NNacl := '';
                      errRow := True;
                    end;
                    decl.DeclNum := ShortString(NNacl);
                    try
                      TM := String
                        (nProd.Attributes['�200000000015']);
                    except
                      TM := '';
                    end;
                    decl.TM := ShortString(TM);

                    try
                      Amount := String(nProd.Attributes['�200000000016']);
                    except
                      Amount := '0';
                      errRow := True;
                    end;

                    if not ValidFloat(Amount, V) then
                    begin
                      V := 0;
                      decl.vol := V;
                    end;

                    case fTF of
                      fVPost:
                        if V < 0 then
                        begin
                          V:= Abs(V);
                          IsWrite := True;
                        end;
                      fPrihod:
                        if V > 0 then
                          IsWrite := True
                        else
                          ExtVozvr :=True;
                    end;

                    if (NNacl = '') or (Amount = '0') or
                      (Amount = '') or (DNacl = '') then
                      errRow := True;

                    pXML.grup := GrFNS;
                    pXML.vol := V;
                    pXML.kol := V * 10;
                    decl.Amount := pXML.kol;
                    decl.vol := pXML.vol;
                    pXML.nnakl := NNacl;
                    pXML.TM := TM;
                    s := Copy(DNacl, 1, 2) + '.' + Copy(DNacl, 4,
                      2) + '.' + Copy(DNacl, 7, 4);

                    try
                      pXML.dnakl := StrToDateTime(s);
                      decl.DeclDate := StrToDateTime(s);
                    except
                      decl.DeclDate := 0;
                      pXML.dnakl := 0;
                    end;
                    decl.product.AlcVol := 1;
                    decl.product.capacity := 1;
                    case fTF of
                      fPrihod: decl.TypeDecl := 1;
                      fVPost: decl.TypeDecl := 3;
                    end;

                    decl.RepDate := dEnd;
                    decl.forma := FormDecl(String(decl.product.FNSCode));

                    rowXML := GetRowXML(Xml.Xml, nProd.Xml);
                    GetErrProduct_XML(SLErrXML, decl, rowXML);

                    if errRow or errFns or errPr then
                      err := True
                    else
                      err := False;

                    if IsWrite and (not err) then
                    begin
                      WriteFullDataXML(SLEan, DBQ, decl);
                      SLXml.AddObject(string(decl.product.EAN13),
                        TDecl_obj.Create);
                      TDecl_obj(SLXml.Objects[SLXml.Count - 1])
                        .data := decl;
                      Result := 0;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    except
      Result := 1;
    end;
  finally
    SLEan.free;
    SL.free;
  end;
end;

procedure WriteXMLCorrect(SG: TADVStringGrid; SL: TStringList);
var
  i, idx, rc: integer;
  s, s1, sDate, NNacl: string;
  SLAdd, SLRow, SLCol: TSTrings;

begin
  SLAdd := TStringList.Create;
  try
    SLCol := SG.cols[4];
    for i := 0 to SL.Count - 1 do
      if SL.Objects[i] is TStringList then
      begin
        SLRow := Pointer(SL.Objects[i]);
        sDate := SLRow.Strings[0];
        NNacl := SLRow.Strings[1];
        s := sDate + NNacl;
//        idx := SLCol.IndexOf(s);
        idx:=FindSL(s,SLCol);
        s1 := cDate + sDate + cNacl + NNacl;
        if idx <> -1 then
        begin
          SG.MergeCells(9, idx, 2, 1);
          SG.Alignments[9, idx] := taRightJustify;
          SG.CellProperties[9, idx].FontSize := 9;
          SG.CellProperties[9, idx].FontStyle := [fsBold];
          SG.Cells[9, idx] := s1;
          SG.Alignments[17, idx] := taLeftJustify;
          SG.CellProperties[17, idx].FontSize := 9;
          SG.CellProperties[17, idx].FontStyle := [fsBold];
          SG.Cells[17, idx] := SLRow.Strings[2];
          if SG.Floats[11, idx] <> SG.Floats[17, idx] then
          begin
            SG.Floats[18, idx] := SG.Floats[11,
              idx] - SG.Floats[17, idx];
            SG.CellProperties[18, idx].FontSize := 9;
            SG.CellProperties[18, idx].FontStyle := [fsBold];

            SG.CellProperties[9, idx].BrushColor := clYellow;
            SG.CellProperties[17, idx].BrushColor := clYellow;
            SG.CellProperties[11, idx].BrushColor := clYellow;
            SG.CellProperties[18, idx].BrushColor := clRed;
            SG.CellProperties[18, idx].FontColor := clWhite;
          end;
        end
        else
        begin
          SLAdd.AddObject(SLRow.Strings[2], Pointer(SLRow));
        end;
      end;
    for i := 0 to SG.RowCount - 1 do
      if (SG.Cells[1, i] = '') and (SG.Cells[2, i] = '') then
        if SG.Cells[17, i] = '' then
        begin
          SG.Floats[18, i] := SG.Floats[11, i];
          SG.CellProperties[18, i].FontSize := 9;
          SG.CellProperties[18, i].FontStyle := [fsBold];

          SG.CellProperties[9, i].BrushColor := clYellow;
          SG.CellProperties[17, i].BrushColor := clYellow;
          SG.CellProperties[11, i].BrushColor := clYellow;
          SG.CellProperties[18, i].BrushColor := clRed;
          SG.CellProperties[18, i].FontColor := clWhite;
        end;
    rc := SG.RowCount - SG.FixedFooters;

    if SLAdd.Count > 0 then
    begin
      SG.InsertRows(SG.RowCount - SG.FixedFooters, SLAdd.Count);
      for i := 0 to SLAdd.Count - 1 do
      begin
        SLRow := Pointer(SLAdd.Objects[i]);
        SG.MergeCells(9, i + rc, 2, 1);
        sDate := SLRow.Strings[0];
        NNacl := SLRow.Strings[1];
        s1 := cDate + sDate + cNacl + NNacl;
        SG.Alignments[9, i + rc] := taRightJustify;
        SG.CellProperties[9, i + rc].FontSize := 9;
        SG.CellProperties[9, i + rc].FontStyle := [fsBold];
        SG.Cells[9, i + rc] := s1;
        SG.Cells[17, i + rc] := SLAdd.Strings[i];
        SG.CellProperties[17, i + rc].FontSize := 9;
        SG.CellProperties[17, i + rc].FontStyle := [fsBold];

        SG.Cells[18, i + rc] := '-' + SLAdd.Strings[i];
        SG.CellProperties[18, i + rc].FontSize := 9;
        SG.CellProperties[18, i + rc].FontStyle := [fsBold];
        SG.CellProperties[18, i + rc].BrushColor := clRed;
        SG.CellProperties[18, i + rc].FontColor := clWhite;
        SG.CellProperties[9, i + rc].BrushColor := clYellow;
        SG.CellProperties[17, i + rc].BrushColor := clYellow;
        SG.CellProperties[11, i + rc].BrushColor := clYellow;
      end;
    end;

    SG.FloatingFooter.ColumnCalc[17] := acSum;
    SG.CalcFooter(17);
    SG.FloatingFooter.ColumnCalc[18] := acSum;
    SG.CalcFooter(18);
  finally
    SLAdd.free;
  end;
end;

procedure SumCorrectData(SLData: TStringList;
  var SLSum: TStringList; AllData: Boolean);
var
  i: integer;
  decl: TDeclaration;
  SLDate: TStringList;
  s: string;
begin
  SLDate := TStringList.Create;

  try
    // ��������� ������ ��� ��� �������� ���� ���������
    for i := 0 to SLData.Count - 1 do
    begin
      NullDecl(decl);
      decl := TDecl_obj(SLData.Objects[i]).data;
      s := DateToStr(decl.DeclDate);
      if SLDate.IndexOf(s) = -1 then
        SLDate.Add(s);
    end;

    SLDate.CustomSort(CompDateAsc);
    // ���������� ������� ��� ������ ����
    for i := 0 to SLDate.Count - 1 do
      FillDataCorrectDate(SLData,  SLDate.Strings[i], SLSum, AllData);
  finally
    SLDate.free;
  end;
end;

function ReadXMLCorrectF(fName: TSTrings; var SLRez: TStringList;
  DBQ: TADOQuery; sal: TSaler; sKppXML:string):integer;
var
  i: integer;
begin
  SLRez.Clear;
  for i := 0 to fName.Count - 1 do
    ReadXMLCorrect(fName.Strings[i], SLRez, DBQ, sal, sKppXML);
end;

procedure ProizXML(node: IXMLNode; SL: TStringList;
  f11, fix: Boolean);
var
  i: integer;
  PrImp: IXMLNode;
  pr: TProducer_Obj;
begin
  for i := 0 to SL.Count - 1 do
  begin
    pr := Pointer(SL.Objects[i]);
    if pr <> nil then
      if pr is TProducer_Obj then
      begin
        PrImp := node.AddChild('����������������������');
        with PrImp do
          XMLPoizvImp(PrImp, f11, fix, pr);
      end;
  end;
end;

procedure PostXML(node: IXMLNode; SL: TStringList;
  f11, fix: Boolean);
var
  i: integer;
  Post: IXMLNode;
  sal: TSaler_Obj;
begin
  for i := 0 to SL.Count - 1 do
  begin
    sal := Pointer(SL.Objects[i]);
    if sal <> nil then
      if sal is TSaler_Obj then
      begin
        Post := node.AddChild('����������');
        with Post do
          XMLPost(Post, f11, fix, sal);
      end;
  end;
end;

procedure XMLPoizvImp(node: IXMLNode; f11, af: Boolean;
  pr: TProducer_Obj);
begin
  if af and not ValidOrgname(String(pr.data.pName)) then
    pr.data.pName := '��� "�������"';
  with node do
  begin
    Attributes['�����������'] := pr.data.pId;
    Attributes['�000000000004'] := pr.data.pName;
    if f11 then
    begin
      if ((pr.data.pKPP = NullKPP9) or (pr.data.pKPP = NullKPP8)) then
      begin
        if pr.data.pKPP = NullKPP8 then
          Attributes['�000000000005'] := Copy(pr.data.pInn,3,Length(pr.data.pInn)-2);
        if pr.data.pKPP = NullKPP9 then
          Attributes['�000000000005'] := Copy(pr.data.pInn,2,Length(pr.data.pInn)-1);
      end
      else
      begin
        if ((pr.data.pKPP = NullKPP7) or (pr.data.pKPP = NullKPP7)) then
        begin
          Attributes['�000000000005'] := '';
          Attributes['�000000000006'] := '';
        end
        else
        begin
          Attributes['�000000000005'] := pr.data.pInn;
          Attributes['�000000000006'] := pr.data.pKpp ;
        end;
      end;
    end
    else
    begin
      if Length(pr.data.pINN) = 12 then
        with AddChild('��') do
        begin
          Attributes['�000000000005'] := pr.data.pINN;
        end
        else
          with AddChild('��') do
          begin
            Attributes['�000000000005'] := pr.data.pINN;
            Attributes['�000000000006'] := pr.data.pKPP;
          end;
    end;
  end;
end;

procedure XMLPost(node: IXMLNode; f11, af: Boolean;
  sal: TSaler_Obj);
begin
  try
    try
      with node do
      begin
        if af and not ValidOrgname(String(sal.data.sName)) then
          sal.data.sName := '��� "�������"';

        if not Empty(String(sal.data.sName)) then
        begin
          Attributes['��������'] := sal.data.sId;
          Attributes['�000000000007'] := sal.data.sName;

          if f11 then
          begin
            with AddChild('��������') do
              if sal.data.lic.RegOrg <> NoLic then
                with AddChild('��������') do
                begin
                  Attributes['����������'] := sal.data.lic.lId;
                  Attributes['�000000000011'] :=
                    sal.data.lic.ser + ',' + sal.data.lic.num;
                  Attributes['�000000000012'] := DateToStr
                    (sal.data.lic.dBeg);
                  Attributes['�000000000013'] := DateToStr
                    (sal.data.lic.dEnd);
                  Attributes['�000000000014'] :=
                    sal.data.lic.RegOrg;
                end;
          end;

          if InnUL(String(sal.data.sINN)) then
          begin
            with AddChild('��') do
            begin
              Attributes['�000000000009'] := sal.data.sINN;
              Attributes['�000000000010'] := sal.data.sKpp;
            end
          end
          else
            with AddChild('��') do
              Attributes['�000000000009'] := sal.data.sINN;
        end;
      end;
    except
    end;
  finally
  end;
end;

procedure OrgXML(node: IXMLNode; f11, fix, f43: Boolean;
  d1, d2: TDateTime);
begin
  if Length(fOrg.Kpp0) > 0 then
    GetKPPAdres(fmWork.que1_sql, String(fOrg.Kpp0), fOrg.adres);

  if Copy(fOrg.inn, 1, 10) = '0000000000' then
    fOrg.inn := '1234567890' + Copy(fOrg.inn, 11, 2);
  fOrg.adres.kod_str := ShortString
    (UseStr(String(fOrg.adres.kod_str), '', '643'));
  fOrg.adres.kod_reg := ShortString
    (UseStr(String(fOrg.adres.kod_reg), '',
      Copy(String(fOrg.inn), 1, 2)));
  if fOrg.Kpp0 = '000000000' then
    fOrg.Kpp0 := '123456789';

  OrgRecXML(node, f11, fix, fOrg); // ���������
  OrgOtvXML(node, fOrg.buh, fOrg.ruk); // ���������
  if f11 then
    OrgLicXML(fOrg, node, d1, d2, f43); // ��������
end;

procedure OrgRecXML(node: IXMLNode; f11, fix: Boolean;
  org: TOrganization);
begin
  with org do
    with node do
      with AddChild('���������') do
      begin
        with adres do
          with AddChild('������') do
          begin
            AddChild('���������').Text := '643';

            if not Empty(String(postindex)) then
              AddChild('������').Text := String(postindex)
            else
              AddChild('������');

            if not Empty(String(kod_reg)) then
              AddChild('���������').Text := String(kod_reg)
            else
              AddChild('���������');

            if not Empty(raion) then
              AddChild('�����').Text := raion
            else
              AddChild('�����');

            if not Empty(gorod) then
              AddChild('�����').Text := gorod
            else
              AddChild('�����');

            if not Empty(naspunkt) then
              AddChild('����������').Text := naspunkt
            else
              AddChild('����������');

            if not Empty(ulica) then
              AddChild('�����').Text := ulica
            else
              AddChild('�����');

            if not Empty(String(dom)) then
              AddChild('���').Text := String(dom)
            else
              AddChild('���');

            if not Empty(String(korpus)) then
              AddChild('������').Text := String(korpus)
            else
              AddChild('������');

            if not Empty(String(litera)) then
              AddChild('������').Text := String(litera)
            else
              AddChild('������');

            if not Empty(String(kvart)) then
              AddChild('�����').Text := String(kvart)
            else
              AddChild('�����');
          end;

        if InnUL(inn) then
          with AddChild('��') do
            begin
              Attributes['�����'] := inn;
              Attributes['�����'] := Kpp0;
            end
        else
          AddChild('��').Attributes['�����'] := inn;

        Attributes['����'] := SelfName;
        Attributes['������'] := NPhone;
        Attributes['Email����'] := Email;
      end;
end;

procedure OrgOtvXML(node: IXMLNode; buh: TBuh; ruk: TRuk);
begin
  with node do
    with AddChild('���������') do
    begin
      with AddChild('������������') do
        with ruk do
        begin
          AddChild('�������').Text := RukF;
          AddChild('���').Text := RukI;
          AddChild('��������').Text := RukO;
        end;
      with AddChild('�������') do
        with buh do
        begin
          AddChild('�������').Text := BuhF;
          AddChild('���').Text := BuhI;
          AddChild('��������').Text := BuhO;
        end;
    end;
end;

procedure OrgLicXML(org: TOrganization; node: IXMLNode; d1, d2: TDateTime;
  f43: Boolean);
var
  DBQ: TADOQuery;
  StrSQL: string;
  atr: string;
begin
  DBQ := TADOQuery.Create(Application);

  try
    try
      if (fOrg.lic.SerLic = '') or (fOrg.lic.NumLic = '') or
        (fOrg.lic.BegLic = 0) or (fOrg.lic.EndLic = 0) then
      begin
        DBQ := fmWork.que1_sql;
        DBQ.Close;
        DBQ.ParamCheck := False;
        DBQ.sql.Clear;
        StrSQL := 'select distinct * from self_license';
        DBQ.sql.Text := StrSQL;
        DBQ.Open;
        DBQ.First;
        while not DBQ.Eof do
        begin
          if ValidLic(DBQ.FieldByName('ser').AsString,
            DBQ.FieldByName('num').AsString) then
          begin
            fOrg.lic.SerLic := DBQ.FieldByName('ser').AsString;
            fOrg.lic.NumLic := DBQ.FieldByName('num').AsString;
            fOrg.lic.BegLic := DBQ.FieldByName('BegLic').AsDateTime;
            fOrg.lic.EndLic := DBQ.FieldByName('EndLic').AsDateTime;
          end;
          DBQ.Next;
        end
      end;

      if (fOrg.lic.SerLic = '') or (fOrg.lic.NumLic = '') or
        (fOrg.lic.BegLic = 0) or (fOrg.lic.EndLic = 0) then
      begin
        fOrg.lic.SerLic := '�';
        fOrg.lic.NumLic := '000000';
        fOrg.lic.BegLic := d1;
        fOrg.lic.EndLic := d2;
      end;
      with node do
        with AddChild('������������') do
          if InnUL(org.inn) then
            with AddChild('�������������') do
              with AddChild('��������') do
              begin
                Attributes['�������'] := '06';
                Attributes['���������'] :=
                  fOrg.lic.SerLic + ' ' + fOrg.lic.NumLic;
                Attributes['����������'] := DateToStr
                  (fOrg.lic.BegLic);
                Attributes['�����������'] := DateToStr
                  (fOrg.lic.EndLic);
              end
          else
            with AddChild('���������������') do
              Attributes['�������'] := '12';
    except
    end;
  finally
    DBQ.free;
  end;
end;

function ValidLic(ser, num: string): Boolean;
var
  rez: Boolean;
begin
  if Valid_SerLic(ser) and Valid_NumLic(num) then
    rez := True
  else
    rez := False;
  ValidLic := rez;
end;

function Valid_SerLic(s: string): Boolean;
var
  rez: Boolean;
begin
  if Length(s) > 0 then
    rez := True
  else
    rez := False;
  if rez then
    if pos('*', s) > 0 then
      rez := False;
  Valid_SerLic := rez;
end;

function Valid_NumLic(s: string): Boolean;
var
  rez: Boolean;
  i: integer;
begin
  if Length(s) > 3 then
    rez := True
  else
    rez := False;
  if rez then
    for i := 1 to Length(s) do
      if not(s[i] in digits) then
        rez := False;
  Valid_NumLic := rez;
end;

procedure ReplaceKpp(all_Node: array of IXMLNode;
  var new_Node: array of IXMLNode);
//var
//  i, j, n: integer;
//  sKpp, sKpp_a: string;
//  ex: Boolean;
begin
  // sKpp := String(Node.Attributes['�����']);
  // for i := 0 to High(aNode) do
  // if aNode[i] <> nil then
  // begin
  // sKpp_a := String(aNode[i].Attributes['�����']);
  // if sKpp = sKpp_a then
  // Node.ParentNode.DOMNode.replaceChild(aNode[i].DOMNode,Node.DOMNode);
  // end;
  // for i := 0 to High(all_Node) do
  // begin
  // ex:=False;
  // sKpp := String(all_Node[i].Attributes['�����']);
  // for j := 0 to High(new_node) do
  // begin
  // sKpp_a := String(new_node[i].Attributes['�����']);
  // if sKpp = sKpp_a then
  // ex:=True;
  // end;
  // if not ex then
  // begin
  // N := High(new_Node)+1;
  // SetLength(new_Node, N);
  // new_Node[High(new_Node)] := all_Node[i];
  // end;
  // end;
end;

procedure LoadOstXML();
var
  SLProiz: TStringList;
  Xml: IXMLDocument;
  NFile, NDoc, nRef, nPr, nOO, nOb, nSvPrImp, nProd, nOrg, nr, nDv,
    nFo, nYl: IXMLNode;
  kv, y, i, j, k, L, idx, CountPrXML, CountOO, CountOb, CountPrImp,
    countProd, fDecl: integer;
  s, idPr, Amount, SelfKPP: string;
  V: Extended;
  decl: TDeclaration;
  prod: TProducer;
  fl: Boolean;
  prXML: TProizvXML;
  FNS: string;
  sal: TSaler;
  filename: string;
  SLXml, SLEan: TStringList;
  dDecl: TDateTime;
begin
  sal.sINN := '1234567894';
  sal.sKpp := '123400001';
  SLEan := TStringList.Create;
  SLXml := TStringList.Create;
  SLProiz := TStringList.Create;

  GetSaler(fmWork.que2_sql, sal, fl, True,False);
  fmWork.dlgOpen_Import.Title := '������ �������� �� XML';
  fmWork.dlgOpen_Import.Filter := '���� ���������� (*.xml';
  if fmWork.dlgOpen_Import.Execute then
  begin
    Xml := TXMLDocument.Create(nil);
    Xml.Active := False;
    try
      try
        if fmProgress = nil then
          fmProgress:= TfmProgress.Create(Application);
        fmWork.Enabled := False;
        fmProgress.Show;
        fmProgress.pb.Position := 0;
        fmProgress.lblcap.Caption := '����������� ������ �������� XML ... ';
        fmProgress.pb.UpdateControlState;
        Application.ProcessMessages;
        fmProgress.labAbout.Visible :=True;
        fmProgress.pb.Min := 0;
        fmProgress.pb.Step := 1;

        filename := fmWork.dlgOpen_Import.filename;
        Xml.LoadFromFile(filename);
        NFile := Xml.DocumentElement; // �������� ��� ����
        NDoc := NFile.ChildNodes.FindNode('��������');
        nRef := NFile.ChildNodes.FindNode('�����������');
        nFo := NFile.ChildNodes.FindNode('��������');
        if nFo <> nil then
        begin
          s := String(nFo.Attributes['�������']);
          if pos('11', s) > 0 then
            fDecl := 11;
          if pos('12', s) > 0 then
            fDecl := 12;
          s := String(nFo.Attributes['�������������']);
          i := StrToInt(s);
          case i of
            3:
              kv := 1;
            6:
              kv := 2;
            9:
              kv := 3;
            0:
              kv := 4;
          end;
          s := String(nFo.Attributes['������������']);
          y := StrToInt(s);
          dDecl := Kvart_End(kv, y);
        end;

        if nRef <> nil then
        begin
          nPr := nRef.ChildNodes.FindNode('����������������������');
          if nPr <> nil then
          begin
            CountPrXML := nRef.ChildNodes.Count;
            fmProgress.pb.Max := CountPrXML-1;
            fmProgress.pb.Position := 0;
            fmProgress.lblcap.Caption := '����������� ������ ������������ ... ';
            for i := 0 to CountPrXML - 1 do
            begin

              Application.ProcessMessages;
              fmProgress.pb.StepIt;

              nPr := nRef.ChildNodes.Nodes[i];
              if nPr.NodeName = '����������������������' then
              begin
                prXML := TProizvXML.Create;
                prXML.pName := String
                  (nPr.Attributes['�000000000004']);
                prXML.pId := String(nPr.Attributes['�����������']);
                if fDecl = 11 then
                begin
                  prXML.pINN := String
                    (nPr.Attributes['�000000000005']);
                  prXML.pKPP := String
                    (nPr.Attributes['�000000000006']);
                end;
                if fDecl = 12 then
                begin
                  nYl := nPr.ChildNodes.FindNode('��');
                  if nYl <> nil then
                  begin
                    prXML.pINN := String
                      (nYl.Attributes['�000000000005']);
                    prXML.pKPP := String
                      (nYl.Attributes['�000000000006']);
                  end;
                end;
                SLProiz.AddObject(prXML.pId, Pointer(prXML));
              end;
            end;
          end;
        end;

        if NDoc <> nil then
        begin
          nOrg := NDoc.ChildNodes.FindNode('�����������');
          if nOrg <> nil then
          begin
            nr := nOrg.ChildNodes.FindNode('���������');
            if fDecl = 11 then
            begin
              if nr <> nil then
                SelfKPP := String(nr.Attributes['�����']);
            end;
            if fDecl = 12 then
            begin
              nYl := nr.ChildNodes.FindNode('��');
              if nYl <> nil then
                SelfKPP := String(nYl.Attributes['�����']);
            end;
          end;

          nOO := NDoc.ChildNodes.FindNode('������������');
          if nOO <> nil then
          begin
            CountOO := NDoc.ChildNodes.Count;
            for i := 0 to CountOO - 1 do
            begin
              nOO := NDoc.ChildNodes.Nodes[i];
              if nOO.NodeName = '������������' then
              begin
                // pXML.SelfKPP := String(nOO.Attributes['�����']);
                s := String(nOO.Attributes['��������������']);
                if (UpperCase(s) = 'TRUE') or (UpperCase(s) = '1')
                  then
                begin
                  nOb := nOO.ChildNodes.FindNode('������');
                  if nOb <> nil then
                  begin
                    CountOb := nOO.ChildNodes.Count;
                    fmProgress.pb.Max := CountOb-1;
                    fmProgress.pb.Position := 0;
                    fmProgress.lblcap.Caption := '����������� ������ ������ XML ... ';
                    for j := 0 to CountOb - 1 do
                    begin
                      nOb := nOO.ChildNodes.Nodes[j];
                      Application.ProcessMessages;
                      fmProgress.pb.StepIt;

                      if nOb.NodeName = '������' then
                      begin
                        FNS := String
                          (nOb.Attributes
                            ['�000000000003']);
                        nSvPrImp := nOb.ChildNodes.FindNode
                          ('����������������');
                        if nSvPrImp <> nil then
                        begin
                          CountPrImp := nOb.ChildNodes.Count;
                          for k := 0 to CountPrImp - 1 do
                          begin
                            nSvPrImp := nOb.ChildNodes.Nodes[k];
                            if nSvPrImp.NodeName =
                            '����������������' then
                            begin
                              idPr := String
                              (nSvPrImp.Attributes['�����������']);
                              idx := SLProiz.IndexOf(idPr);
                              if idx <> -1 then
                              begin
                                prXML := Pointer(SLProiz.Objects[idx]);
                                prod.pINN := ShortString(prXML.pINN);
                                prod.pKPP := ShortString(prXML.pKPP);
                                fl := GetProducer(fmWork.que2_sql,
                                prod, True, False);
                                if not fl then
                                begin
                                  prod.pName := ShortString(prXML.pName);
                                  prod.pAddress := '������';
                                end;
                              end;
                              nDv := nSvPrImp.ChildNodes.FindNode
                              ('��������');

                              if nDv <> nil then
                              begin
                                countProd := nSvPrImp.ChildNodes.Count;
                                for L := 0 to countProd - 1 do
                                begin
                                  nProd := nSvPrImp.ChildNodes.Nodes[L];
                                  if nProd.NodeName = '��������' then
                                  begin
                                    if fDecl = 11 then
                                      Amount := String
                                      (nProd.Attributes['�100000000020']);
                                    if fDecl = 12 then
                                      Amount := String
                                      (nProd.Attributes['�100000000018']);
                                    if not ValidFloat(Amount, V) then
                                    V := 0;
                                    if V <> 0 then
                                    begin
                                      decl.Amount := V * 10;
                                      decl.vol := V;
                                      decl.DeclNum := '�������';
                                      decl.product.AlcVol := 1;
                                      decl.product.capacity := 1;
                                      decl.TypeDecl := 1;
                                      decl.DeclDate := dDecl;
                                      decl.RepDate := dEnd;
                                      if Length(fOrg.inn) = 12 then
                                        decl.SelfKpp:=fOrg.Kpp0
                                      else
                                        decl.SelfKPP := ShortString(SelfKPP);
                                      decl.sal := sal;
                                      decl.product.prod := prod;
                                      decl.product.FNSCode := ShortString
                                      (FNS);
                                      WriteFullDataXML(SLEan,
                                      fmWork.que2_sql, decl);
                                      SLXml.AddObject
                                      (String(decl.SelfKPP) + String
                                      (decl.product.EAN13),
                                      TDecl_obj.Create);
                                      TDecl_obj
                                      (SLXml.Objects[SLXml.Count - 1])
                                      .data := decl;
                                    end;
                                  end;
                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
        if SLXml.Count > 0 then
        begin
          fmProgress.pb.Max := SLXml.Count - 1;
          fmProgress.pb.Position := 0;
          fmProgress.lblcap.Caption := '����������� �������� ������ � ���� ... ';

          for i := 0 to SLXml.Count - 1 do
            if SLXml.Objects[i] <> nil then
              if SLXml.Objects[i] is TDecl_obj then
              begin
                Application.ProcessMessages;
                fmProgress.pb.StepIt;
                decl := TDecl_obj(SLXml.Objects[i]).data;
                // ������ �������� � ����
                SaveRowNewDeclDB(fmWork.que1_sql, decl, true);
                // ������ �������� � ������
                SaveRowDeclNewLocal(decl);
              end;
        end;
      if fmMain <> nil then
        fmMain.cbAllKvartChange(Application);

      Show_Inf('������ ������ �� XML ������� ��������', fInfo);

      except
        Show_Inf('������ ������� ����� XML', fError);
        fmWork.Enabled := True;
      end;
    finally
      FreeStringList(SLProiz);
      FreeStringList(SLXml);
      Xml := nil;
      SLEan.free;
      fmWork.Enabled := True;
      fmProgress.labAbout.Visible :=False;
      fmProgress.Close;
    end;
  end;
end;

procedure LoadFullXML(ost, dataOrg: Boolean);
var
  SLProiz, SLSaler: TStringList;
  Xml: IXMLDocument;
  NFile, NDoc, nRef, nPr, nOO, nOb, nSvPrImp, nProd, tmp, nOrg, nr,
    nDv, nFo, nYl, nLics, nLic, nProduct, nAdrOrg, nOtvLic, nRuk, nBuh, nDeyat: IXMLNode;
  kv, y, i, j, k, L, m, idx, CountPrXML, CountOO, CountOb,
    CountPrImp, countProd, fDecl, countProduct: integer;
  s, s1, idPr, idSal, Amount, SelfKPP: string;
  V: Extended;
  decl: TDeclaration;
  prod: TProducer;
  fl, FindLic: Boolean;
  prXML: TProizvXML;
  salXML: TSalerXML;
  FNS: string;
  sal: TSaler;
  filename, fn: string;
  SLXml, SLEan: TStringList;
  dDecl_end, dDecl_beg: TDateTime;
  tInif :TIniFile;
begin
  SLEan := TStringList.Create;
  SLXml := TStringList.Create;
  SLProiz := TStringList.Create;
  SLSaler := TStringList.Create;
  GetSaler(fmWork.que2_sql, sal, fl, True,False);
  fmWork.dlgOpen_Import.Title := '������ �������� �� XML';
  fmWork.dlgOpen_Import.Filter := '���� ���������� (*.xml';

  if dataOrg then
  begin
    fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
      + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
    tIniF := TIniFile.Create(fn);
  end;

  if fmWork.dlgOpen_Import.Execute then
  begin
    if fmProgress = nil then
      fmProgress:= TfmProgress.Create(Application);
    fmWork.Enabled := False;
    fmProgress.Show;
    fmProgress.pb.Position := 0;
    fmProgress.lblcap.Caption := '����������� ������ XML ... ';
    fmProgress.pb.UpdateControlState;
    Application.ProcessMessages;
    fmProgress.labAbout.Visible :=True;
    fmProgress.pb.Min := 0;
    fmProgress.pb.Step := 1;
    Xml := TXMLDocument.Create(nil);
    Xml.Active := False;
    try
      try
        filename := fmWork.dlgOpen_Import.filename;
        Xml.LoadFromFile(filename);
        NFile := Xml.DocumentElement; // �������� ��� ����
        NDoc := NFile.ChildNodes.FindNode('��������');
        nRef := NFile.ChildNodes.FindNode('�����������');
        nFo := NFile.ChildNodes.FindNode('��������');
        if nFo <> nil then
        begin
          s := String(nFo.Attributes['�������']);
          if pos('11', s) > 0 then
            fDecl := 11;
          if pos('12', s) > 0 then
            fDecl := 12;
          s := String(nFo.Attributes['�������������']);
          i := StrToInt(s);
          case i of
            3:
              kv := 1;
            6:
              kv := 2;
            9:
              kv := 3;
            0:
              kv := 4;
          end;
          s := String(nFo.Attributes['������������']);
          y := StrToInt(s);
          dDecl_end := Kvart_End(kv, y);
          dDecl_beg := Kvart_Beg(kv, y);
        end;

        if nRef <> nil then
        begin
          nPr := nRef.ChildNodes.FindNode('����������������������');
          if nPr <> nil then
          begin
            CountPrXML := nRef.ChildNodes.Count;
            fmProgress.lblcap.Caption := '����������� ������ ������������ �� XML ... ';
            fmProgress.pb.Max := CountPrXML-1;
            fmProgress.pb.Position := 0;

            for i := 0 to CountPrXML - 1 do
            begin
              Application.ProcessMessages;
              fmProgress.pb.StepIt;
              nPr := nRef.ChildNodes.Nodes[i];
              if nPr.NodeName = '����������������������' then
              begin
                prXML := TProizvXML.Create;
                prXML.pName := String
                  (nPr.Attributes['�000000000004']);
                prXML.pId := String(nPr.Attributes['�����������']);
                if fDecl = 11 then
                begin
                  prXML.pINN := String
                    (nPr.Attributes['�000000000005']);
                  l := Length(prXML.pINN);
                  case l of
                    8:
                      try
                        prXML.pInn := CreateNewInn(prXML.pInn);
                        prXML.pKPP := NullKPP8;
                      except
                        prXML.pKPP := NullKPP8;
                      end;
                    9:
                      try
                        prXML.pInn := CreateNewInn(prXML.pInn);
                        prXML.pKPP := NullKPP9;
                      except
                        prXML.pKPP := NullKPP9;
                      end;
                    10:
                      try
                        s := String(nPr.Attributes['�000000000006']);
                        prXML.pKPP := s;
                      except
                        prXML.pKPP := '';
                      end;
                  end;
                end;
                if fDecl = 12 then
                begin
                  nYl := nPr.ChildNodes.FindNode('��');
                  if nYl <> nil then
                  begin
                    prXML.pINN := String
                      (nYl.Attributes['�000000000005']);
                    prXML.pKPP := String
                      (nYl.Attributes['�000000000006']);
                  end;
                end;
                SLProiz.AddObject(prXML.pId, Pointer(prXML));
                prod.pId := ShortString(prXML.pId);
                prod.pINN := ShortString(prXML.pINN);
                prod.pKPP := ShortString(prXML.pKPP);
                prod.pName := ShortString(prXML.pName);
                prod.pAddress := '������';
                GetProducer(fmWork.que2_sql, prod, True, True);
              end;

              if nPr.NodeName = '����������' then
              begin
                s := String(nPr.Attributes['�000000000007']);
                if UpperCase(s) <> '����' then
                begin
                  salXML := TSalerXML.Create;
                  s := String(nPr.Attributes['��������']);
                  salXML.sId := s;

                  salXML.sName := s;

                  sal.sAddress := '������';
                  nYl := nPr.ChildNodes.FindNode('��');
                  if nYl <> nil then
                  begin
                    s := String(nYl.Attributes['�000000000009']);
                    salXML.sINN := s;
                    s := String(nYl.Attributes['�000000000010']);
                    salXML.sKpp := s;
                  end;
                  if fDecl = 11 then
                  begin
                    nLics := nPr.ChildNodes.FindNode('��������');
                    if nLics <> nil then
                    begin
                      nLic := nLics.ChildNodes.FindNode('��������');
                      if nLic <> nil then
                      begin
                        s := String(nLic.Attributes['����������']);
                        salXML.slic.lId := ShortString(s);
                        s := String(nLic.Attributes['�000000000011']);
                        idx := pos(',', s);
                        if idx = 0 then
                          idx := pos(' ', s);
                        s1 := Copy(s, 0, idx - 1);
                        salXML.slic.ser := ShortString(s1);
                        s1 := Copy(s, idx + 1, Length(s) - idx);
                        salXML.slic.num := ShortString(s1);
                        s := String(nLic.Attributes['�000000000012']);
                        salXML.slic.dBeg := StrToDate(s);
                        s := String(nLic.Attributes['�000000000013']);
                        salXML.slic.dEnd := StrToDate(s);
                        s := String(nLic.Attributes['�000000000014']);
                        salXML.slic.RegOrg := ShortString(s);
                        sal.sId := ShortString(salXML.sId);
                        sal.sInn := ShortString(salXML.sInn);
                        sal.sKpp := ShortString(salXML.sKpp);
                        sal.sName := ShortString(salXML.sName);

                        sal.lic := salXML.slic;
                        GetSalerLic(fmWork.que2_sql, sal, True, True);
                        salXML.sLic := sal.lic;
                      end;
                    end;
                  end;
                  if fDecl = 12 then
                    begin
                      sal.sId := ShortString(salXML.sId);
                      sal.sInn := ShortString(salXML.sInn);
                      sal.sKpp := ShortString(salXML.sKpp);
                      sal.sName := ShortString(salXML.sName);
                      GetSalerLic(fmWork.que2_sql, sal, True, True);
                      salXML.sLic := sal.lic;
                    end;
                  if SLSaler.IndexOf(salXML.sId) = -1 then
                    SLSaler.AddObject(salXML.sId, Pointer(salXML))
                  else
                    salXML.free;
                  GetSaler(fmWork.que2_sql, sal, FindLic, True, True);
                end;
              end;
            end;
          end;
        end;

        if NDoc <> nil then
        begin
          nOrg := NDoc.ChildNodes.FindNode('�����������');

          if nOrg <> nil then
          begin
            nr := nOrg.ChildNodes.FindNode('���������');
            if dataOrg then
              if nr <> nil then
              begin
                try
                  fOrg.Email := String(nr.Attributes['Email����']);
                  if DataOrg then
                    tIniF.WriteString(SectionName, 'Address', fOrg.Email);
                except
                  fOrg.Email := '';
                end;
                try
                  fOrg.NPhone := String(nYl.Attributes['������']);
                  if DataOrg then
                    tIniF.WriteString(SectionName, 'NPhone', fOrg.NPhone);
                except
                  fOrg.NPhone := '(342)000-00-00';
                end;
              end;

            nAdrOrg := nr.ChildNodes.FindNode('������');
            s := '';
            if nAdrOrg <> nil then
            begin
              tmp := nAdrOrg.ChildNodes.FindNode('���������');
              s1:='';
              if tmp <> nil then
                s1 := tmp.NodeValue
              else
                s1:='';
              if s1 <> '' then s:= s + s1;

              tmp := nAdrOrg.ChildNodes.FindNode('������');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='614600';
              if s1 <> '' then s:= s +', '+ s1;

              tmp := nAdrOrg.ChildNodes.FindNode('�����');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s +', '+ s1;

              tmp := nAdrOrg.ChildNodes.FindNode('�����');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s +', '+ s1;

              tmp := nAdrOrg.ChildNodes.FindNode('����������');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s +', '+ s1;

              tmp := nAdrOrg.ChildNodes.FindNode('�����');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s +', '+ s1;


              tmp := nAdrOrg.ChildNodes.FindNode('���');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s + ', �.' + s1;

              tmp := nAdrOrg.ChildNodes.FindNode('������');
              s1:='';
              if tmp <> nil then
                if tmp.NodeValue <> Null then
                  s1 := tmp.NodeValue
                else
                  s1:='';
              if s1 <> '' then s:= s + ', ������.' + s1;

              if s <> '' then
              begin
                fOrg.Address := s;
                if DataOrg then
                  tIniF.WriteString(SectionName, 'AddressOrg', fOrg.Address);
              end;

            end;

            nOtvLic := nOrg.ChildNodes.FindNode('���������');
            if nOtvLic <> nil then
            begin
              nRuk := nOtvLic.ChildNodes.FindNode('������������');
              if nRuk <> nil then
              begin
                tmp := nRuk.ChildNodes.FindNode('�������');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Ruk.RukF := tmp.NodeValue
                  else
                    fOrg.Ruk.RukF:=''
                else
                  fOrg.Ruk.RukF:='';

                tmp := nRuk.ChildNodes.FindNode('���');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Ruk.RukI := tmp.NodeValue
                  else
                    fOrg.Ruk.RukI:=''
                else
                  fOrg.Ruk.RukI:='';

                tmp := nRuk.ChildNodes.FindNode('��������');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Ruk.RukO := tmp.NodeValue
                  else
                    fOrg.Ruk.RukO := ''
                else
                  fOrg.Ruk.RukO:='';
                if DataOrg then
                begin
                  tIniF.WriteString(SectionName, 'RukF', fOrg.Ruk.RukF);
                  tIniF.WriteString(SectionName, 'RukI', fOrg.Ruk.RukI);
                  tIniF.WriteString(SectionName, 'RukO', fOrg.Ruk.RukO);
                end;
              end;

              nBuh := nOtvLic.ChildNodes.FindNode('�������');
              if nBuh <> nil then
              begin
                tmp := nBuh.ChildNodes.FindNode('�������');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Buh.BuhF := tmp.NodeValue
                  else
                    fOrg.Buh.BuhF := fOrg.Ruk.RukF
                else
                  fOrg.Buh.BuhF := fOrg.Ruk.RukF;

                tmp := nBuh.ChildNodes.FindNode('���');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Buh.BuhI := tmp.NodeValue
                  else
                    fOrg.Buh.BuhI := fOrg.Ruk.RukI
                else
                  fOrg.Buh.BuhI := fOrg.Ruk.RukI;;

                tmp := nBuh.ChildNodes.FindNode('��������');
                if tmp <> nil then
                  if tmp.NodeValue <> Null then
                    fOrg.Buh.BuhO := tmp.NodeValue
                  else
                    fOrg.Buh.BuhO := fOrg.Buh.BuhO
                else
                  fOrg.Buh.BuhO := fOrg.Buh.BuhO;
                if DataOrg then
                begin
                  tIniF.WriteString(SectionName, 'BuhF', fOrg.Buh.BuhF);
                  tIniF.WriteString(SectionName, 'BuhI', fOrg.Buh.BuhI);
                  tIniF.WriteString(SectionName, 'BuhO', fOrg.Buh.BuhO);
                end;
              end;
            end;
            nDeyat := nOrg.ChildNodes.FindNode('������������');
            if nDeyat <> nil then
            begin
              nLics := nDeyat.ChildNodes.FindNode('�������������');
              if nLics <> nil then
              begin
                nLic := nLics.ChildNodes.FindNode('��������');
                if nLic <> nil then
                begin
                  try
                    s := String(nLic.Attributes['���������']);
                    fOrg.Lic.SerLic := Copy(s,1,1);
                    fOrg.Lic.NumLic := Copy(s,2,Length(s)-1);
                  except
                    fOrg.Lic.SerLic := '���';
                    fOrg.Lic.NumLic := '���';
                  end;

                  fOrg.Lic.OrgLic := '�����';

                  try
                    s := String(nLic.Attributes['����������']);
                    fOrg.Lic.BegLic := StrToDateTime(s);
                  except
                    fOrg.Lic.BegLic := dBeg;
                  end;

                  try
                    s := String(nLic.Attributes['�����������']);
                    fOrg.Lic.EndLic := StrToDateTime(s);
                  except
                    fOrg.Lic.EndLic := dEnd;
                  end;
                  if DataOrg then
                  begin
                    tIniF.WriteString(SectionName, 'SerLic', fOrg.Lic.SerLic);
                    tIniF.WriteString(SectionName, 'NumLic', fOrg.Lic.NumLic);
                    tIniF.WriteString(SectionName, 'BegLic', DateTimeToStr(fOrg.Lic.BegLic));
                    tIniF.WriteString(SectionName, 'EndLic', DateTimeToStr(fOrg.Lic.EndLic));
                    tIniF.WriteString(SectionName, 'OrgLic', fOrg.Lic.OrgLic);
                  end;
                end;
              end;
            end;
          end;

          nOO := NDoc.ChildNodes.FindNode('������������');
          if nOO <> nil then
          begin
            CountOO := NDoc.ChildNodes.Count;
            for i := 0 to CountOO - 1 do
            begin
              nOO := NDoc.ChildNodes.Nodes[i];
              if nOO.NodeName = '������������' then
              begin
                s := String(nOO.Attributes['��������������']);
                if (UpperCase(s) = 'TRUE') or (UpperCase(s) = '1')
                  then
                begin
//                  if fDecl = 11 then
                  if Length(fOrg.Inn) = 10 then
                    SelfKPP := String(nOO.Attributes['�����']);

                  nOb := nOO.ChildNodes.FindNode('������');
                  if nOb <> nil then
                  begin
                    CountOb := nOO.ChildNodes.Count;
                    fmProgress.lblcap.Caption := '����������� ������ ������ �� XML ... ';
                    fmProgress.pb.Max := CountOb-1;
                    fmProgress.pb.Position := 0;

                    for j := 0 to CountOb - 1 do
                    begin
                      nOb := nOO.ChildNodes.Nodes[j];

                      Application.ProcessMessages;
                      fmProgress.pb.StepIt;

                      if nOb.NodeName = '������' then
                      begin
                        FNS := String
                          (nOb.Attributes
                            ['�000000000003']);
                        nSvPrImp := nOb.ChildNodes.FindNode
                          ('����������������');
                        if nSvPrImp <> nil then
                        begin
                          CountPrImp := nOb.ChildNodes.Count;
                          for k := 0 to CountPrImp - 1 do
                          begin
                            nSvPrImp := nOb.ChildNodes.Nodes[k];
                            //
                            if nSvPrImp.NodeName =
                            '����������������' then
                            begin
                              idPr := String
                              (nSvPrImp.Attributes['�����������']);
                              idx := SLProiz.IndexOf(idPr);
                              if idx <> -1 then
                              begin
                                prXML := Pointer(SLProiz.Objects[idx]);
                                prod.pINN := ShortString(prXML.pINN);
                                prod.pKPP := ShortString(prXML.pKPP);
                                prod.pName := ShortString(prXML.pName);
                                prod.pAddress := '������';
                                fl := GetProducer(fmWork.que2_sql,
                                prod, True, False);
                              end;
                              nDv := nSvPrImp.ChildNodes.FindNode
                              ('��������');

                              if nDv <> nil then
                              begin
                                countProd := nSvPrImp.ChildNodes.Count;
                                for L := 0 to countProd - 1 do
                                begin
                                  nProd := nSvPrImp.ChildNodes.Nodes[L];
                                  if nProd.NodeName = '���������' then
                                  begin
                                    idSal := String
                                    (nProd.Attributes['������������']);
                                    idx := SLSaler.IndexOf(idSal);
                                    if idx <> -1 then
                                    begin
                                      salXML := Pointer(SLSaler.Objects[idx]);
                                      sal.sINN := ShortString(salXML.sINN);
                                      sal.sKpp := ShortString(salXML.sKpp);
                                      sal.sName := ShortString(salXML.sName);
                                      sal.lic := salXML.slic;

                                      tmp := nProd.ChildNodes.FindNode
                                      ('���������');
                                      if tmp <> nil then
                                      begin
                                        countProduct := nProd.ChildNodes.Count;
                                        for m := 0 to countProduct - 1 do
                                        begin
                                          nProduct := nProd.ChildNodes.Nodes[m];
                                          Amount := String
                                          (nProduct.Attributes['�200000000016']);
                                          if not ValidFloat(Amount, V) then
                                          V := 0;
                                          if V <> 0 then
                                          begin
                                            decl.forma := FormDecl(String(decl.product.FNSCode));
                                            decl.Amount := V * 10;
                                            decl.vol := V;
                                            decl.DeclNum := ShortString
                                              (nProduct.Attributes['�200000000014']);
                                            decl.product.AlcVol := 1;
                                            decl.product.capacity := 1;
                                            decl.TypeDecl := 1;
                                            decl.DeclDate := StrToDate
                                            (String
                                            (nProduct.Attributes['�200000000013'])
                                            );
                                            decl.RepDate := dDecl_end;
                                            if Length(fOrg.inn) = 12 then
                                              decl.SelfKpp:=fOrg.Kpp0
                                            else
                                              decl.SelfKPP := ShortString(SelfKPP);
                                            decl.sal := sal;
                                            decl.product.prod := prod;
                                            decl.product.FNSCode := ShortString
                                            (FNS);
                                            WriteFullDataXML(SLEan,
                                            fmWork.que2_sql, decl);
                                            SLXml.AddObject
                                            (String(decl.SelfKPP) + String
                                            (decl.product.EAN13),
                                            TDecl_obj.Create);
                                            TDecl_obj
                                            (SLXml.Objects[SLXml.Count - 1])
                                            .data := decl;
                                          end;
                                        end;
                                      end;
                                    end;
                                  end;

                                  if nProd.NodeName = '��������' then
                                  begin
                                    // ��������� ������
                                    if fDecl = 11 then
                                    Amount := String
                                    (nProd.Attributes['�100000000019']);
                                    if fDecl = 12 then
                                    Amount := String
                                    (nProd.Attributes['�100000000017']);
                                    if not ValidFloat(Amount, V) then
                                    V := 0;
                                    if V <> 0 then
                                    begin
                                      decl.Amount := V * 10;
                                      decl.vol := V;
                                      decl.DeclNum := '������ XML';
                                      decl.product.AlcVol := 1;
                                      decl.product.capacity := 1;
                                      decl.TypeDecl := 2;
                                      decl.DeclDate := dDecl_end;
                                      decl.RepDate := dDecl_end;
                                      if Length(fOrg.inn) = 12 then
                                        decl.SelfKpp:=fOrg.Kpp0
                                      else
                                        decl.SelfKPP := ShortString(SelfKPP);
                                      // decl.sal := sal;
                                      decl.product.prod := prod;
                                      decl.product.FNSCode := ShortString
                                      (FNS);
                                      WriteFullDataXML(SLEan,
                                      fmWork.que2_sql, decl);
                                      SLXml.AddObject
                                      (String(decl.SelfKPP) + String
                                      (decl.product.EAN13),
                                      TDecl_obj.Create);
                                      TDecl_obj
                                      (SLXml.Objects[SLXml.Count - 1])
                                      .data := decl;
                                    end;
                                    // ��������� ������� �� ������ �������
                                    if ost then
                                    begin
                                      Amount := String
                                      (nProd.Attributes['�100000000006']);
                                      if not ValidFloat(Amount, V) then
                                      V := 0;
                                      if V <> 0 then
                                      begin
                                        decl.Amount := V * 10;
                                        decl.vol := V;
                                        decl.DeclNum := '������� �.�. XML';
                                        decl.product.AlcVol := 1;
                                        decl.product.capacity := 1;
                                        decl.TypeDecl := 1;
                                        decl.DeclDate := dDecl_beg - 1;
                                        decl.RepDate := dDecl_beg - 1;
                                        if Length(fOrg.Inn) = 12 then
//                                        if fDecl = 12 then
                                          decl.SelfKpp:=fOrg.Kpp0
                                        else
                                          decl.SelfKPP := ShortString(SelfKPP);
                                        decl.sal.sINN := '1234567894';
                                        decl.sal.sKpp := '123400001';
                                        decl.sal.sName :=
                                        '������� �� ������ �������';
                                        if fDecl = 12 then
                                          GetSalerLic(fmWork.que2_sql, decl.sal, True, False);
                                        decl.product.prod := prod;
                                        decl.product.FNSCode := ShortString
                                        (FNS);
                                        WriteFullDataXML(SLEan,
                                        fmWork.que2_sql, decl);
                                        SLXml.AddObject
                                        (String(decl.SelfKPP) + String
                                        (decl.product.EAN13),
                                        TDecl_obj.Create);
                                        TDecl_obj
                                        (SLXml.Objects[SLXml.Count - 1])
                                        .data := decl;
                                      end;
                                    end
                                  end;
                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
        if SLXml.Count > 0 then
        begin
          fmProgress.pb.Max := SLXml.Count-1;
          fmProgress.pb.Position := 0;
          fmProgress.lblcap.Caption := '����������� ����������� ������ � ���� ... ';
          for i := 0 to SLXml.Count - 1 do
            if SLXml.Objects[i] <> nil then
              if SLXml.Objects[i] is TDecl_obj then
              begin
                Application.ProcessMessages;
                fmProgress.pb.StepIt;
                decl := TDecl_obj(SLXml.Objects[i]).data;
                // ������ �������� � ����
                SaveRowNewDeclDB(fmWork.que1_sql, decl, true);
                // ������ �������� � ������
                SaveRowDeclNewLocal(decl);
              end;
          if fmMain <> nil then
            fmMain.cbAllKvartChange(Application);

          Show_Inf('������ ������ �� XML ������� ��������', fInfo);
        end
      else
        Show_Inf('������ ������� ����� XML', fError);
      except
        Show_Inf('������ ������� ����� XML', fError);
        fmWork.Enabled := True;
      end;
    finally
      FreeStringList(SLSaler);
      FreeStringList(SLProiz);
      FreeStringList(SLXml);
      Xml := nil;
      SLEan.free;
      fmWork.Enabled := True;
      fmProgress.labAbout.Visible :=False;
      fmProgress.Close;
      if dataOrg then
        tIniF.Free;
    end;
  end;
end;

procedure UpdateServerProductionSaler(DBQ: TADOQuery;
  SLRow: TSTrings);
var
  butsel: integer;
  s, cont, EAN13: string;
  recount, k,i: integer;
  qr2: TADOQuery;
  ms, ms1: TMemoryStream;
  er: Boolean;
  FSize: Int64;
  Str: array [1 .. 15] of AnsiString;
  decl:TDeclaration;
  SL:TStringList;
  TCP: TIdTCPClient;
begin
  SL:=TStringList.Create;
  try
    for i := 0 to SLDataPrih.Count-1 do
    begin
      decl:=TDecl_obj(SLDataPrih.Objects[i]).data;
      if TDecl_obj(SLDataPrih.Objects[i]) <> nil then
        if TDecl_obj(SLDataPrih.Objects[i]) is TDecl_obj then
          if (String(decl.sal.sInn) =  SLRow.Strings[1]) and (String(decl.sal.sKpp) = SLRow.Strings[2]) then
            if SL.IndexOf(String(decl.product.EAN13)) = -1 then
              if Copy(decl.product.EAN13,0,2) <> '27' then
                SL.Add(String(decl.product.EAN13))
    end;

    qr2 := TADOQuery.Create(Application);
    qr2.Connection := fmWork.con2ADOCon;
    ms := TMemoryStream.Create;
    ms1 := TMemoryStream.Create;

    TCP := TIdTCPClient.Create(nil);
    TCP.Host:=BMHost;
    TCP.Port:=TCPPort;
    try
      try
        s := '����� ��������� ' + IntToStr(SL.Count) + ' ������� c ����������' + #10#13 +
          '��������� = ' + SLRow.Strings[0]
          + #10#13 + ' ��� = ' + SLRow.Strings[1] + ' ��� = ' + SLRow.Strings[2] + #10#13;
        butsel := Show_Inf(s + #10#13 + '����������?', fConfirm);
        if butsel <> mrOk then
          exit;
          TCP.Connect;

        if TCP.Connected then
          s := TCP.IOHandler.ReadLn;
        if Copy(s, 1, 20) = 'Connected to server.' then
          MemoAdd('C�����: �� ���������� � �������');
        TCP.IOHandler.WriteLn('INN ' + fOrg.inn + ' ' + exe_version);
        s := TCP.IOHandler.ReadLn;

        if SameText(String(s), 'RFEX') then
          TCP.IOHandler.WriteLn('OK');

        if fmProgress = nil then
          fmProgress:= TfmProgress.Create(Application);
        fmWork.Enabled := False;
        fmProgress.Show;
        fmProgress.pb.Position := 0;
        fmProgress.lblcap.Caption := '����������� ���������� ��������� ���������� ... ';
        fmProgress.pb.UpdateControlState;
        Application.ProcessMessages;
        fmProgress.labAbout.Visible :=True;
        fmProgress.pb.Min := 0;
        fmProgress.pb.Step := 1;
        fmProgress.pb.Max := SL.Count-1;
        fmProgress.pb.Position := 0;

        for i := 0 to SL.Count - 1 do
        begin
          EAN13 := SL.Strings[i];
          if i = 0 then
          begin
            if SameText(String(s), 'OK') then
            begin
              TCP.IOHandler.WriteLn('EAN' + EAN13);
            end
            else
            begin

            end;
          end
          else
            TCP.IOHandler.WriteLn('EAN' + EAN13);

          cont := TCP.IOHandler.ReadLn;
          Application.ProcessMessages;
          fmProgress.pb.StepIt;
          for recount := 1 to StrToInt(cont) do
          begin
            s := TCP.IOHandler.ReadLn;
            if Copy(s, 1, 2) = 'OK' then
            begin
//              MemoAdd
//                ('����� �� ������ ������. ����������� ����� ����������... ');
              FSize := StrToInt(Copy(String(s), 3, Length(String(s)) - 2));
//              Application.ProcessMessages;
              try
                er := False;
                ms.Clear;
                ms1.Clear;
                TCP.IOHandler.ReadStream(ms, FSize);
                if not TCP.Connected then
                  er := True;
                ms.Position := 0;
                if not UnZipFilePas(ms, ms1, psw_loc) then
                  er := True;
                if er then
                begin
                  MemoEdit('������ ������������');
                  MemoAdd('����� ����������� ������� - ������ ������������');
                end;
                if not er and TCP.Connected then
                begin
                  er := False;
                  ms1.Position := 0;
                  SetLength(Str[1], 13);
                  Str[1] := AnsiString(ReadBuf(ms1, 13));
                  SetLength(Str[2], 3);
                  Str[2] := AnsiString(ReadBuf(ms1, 3));
                  SetLength(Str[3], 180);
                  Str[3] := AnsiString(ReadBuf(ms1, 180));
                  SetLength(Str[4], 8);
                  Str[4] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[5], 8);
                  Str[5] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[6], 12);
                  Str[6] := AnsiString(ReadBuf(ms1, 12));
                  SetLength(Str[7], 9);
                  Str[7] := AnsiString(ReadBuf(ms1, 9));
                  SetLength(Str[8], 180);
                  Str[8] := AnsiString(ReadBuf(ms1, 180));
                  SetLength(Str[9], 8);
                  Str[9] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[10], 12);
                  Str[10] := AnsiString(ReadBuf(ms1, 12));
                  SetLength(Str[11], 9);
                  Str[11] := AnsiString(ReadBuf(ms1, 9));
                  SetLength(Str[12], 3);
                  Str[12] := AnsiString(ReadBuf(ms1, 3));
                  SetLength(Str[13], 2);
                  Str[13] := AnsiString(ReadBuf(ms1, 2));
                  SetLength(Str[14], 180);
                  Str[14] := AnsiString(ReadBuf(ms1, 180));
                  for k := 1 to 14 do
                  begin
                    Str[k] := AnsiString(TrimRight(String(Str[k])));
                  end;
                  try
                    s := String(Str[9]);
                    PrepareText(Str[3]);
                    qr2.sql.Text :=
                      'Select EAN13 From Production where EAN13=''' + String
                      (Str[1]) + '''';
                    qr2.Open;
                    if qr2.RecordCount > 0 then
                    begin
                      // ���������� ��������� � ��������� ����������� (� ����)
                      if qr2.RecordCount > 1 then
                      begin
                        qr2.sql.Text :=
                          'DELETE from Production WHERE EAN13=''' + String(Str[1]) + '''';
                        qr2.ExecSQL;
                        qr2.sql.Text :=
                          'INSERT INTO Production(EAN13, FNSCode, ' +
                          'Productname, AlcVol, Capacity, ProducerCode, ' +
                          'Prodkod, Prodcod)' + #13#10 + 'Values(''' + String
                          (Str[1]) + ''', ''' + String(Str[2])
                          + ''', ''' + String(Str[3]) + ''', ''' + String
                          (Str[4]) + ''', ''' + String(Str[5])
                          + ''', ''' + String(Str[9]) + ''', ''' + String
                          (Str[6]) + ''', ''' + String(Str[7]) + ''')';
                        qr2.ExecSQL;
                      end
                      else
                      begin
                        if qr2.RecordCount > 0 then
                          qr2.sql.Text := 'UPDATE Production ' + #13#10 +
                            'Set FNSCode=''' + String(Str[2])
                            + ''', Productname=''' + String(Str[3])
                            + ''', AlcVol=''' + String(Str[4])
                            + ''', Capacity=''' + String(Str[5])
                            + ''', ProducerCode=''' + String(Str[9])
                            + ''', Prodkod=''' + String(Str[6])
                            + ''', Prodcod=''' + String(Str[7])
                            + '''' + #13#10 + 'WHERE EAN13=''' + String
                            (Str[1]) + '''';
                        qr2.ExecSQL;
                        // ���������� ���������� ����������� � ����������
                      end;
                      if StrCheck(String(Str[9])) and not er then
                        try
                          Str[9] := AnsiString(TrimRight(String(s)));
                          PrepareText(Str[8]);
                          PrepareText(Str[14]);
                          qr2.sql.Text :=
                            'Select * From Producer where inn=''' + String(Str[6]) + ''' and kpp=''' + String(Str[7]) + '''';
                          qr2.Open;
                          if qr2.RecordCount > 0 then
                            qr2.sql.Text :=
                              'UPDATE Producer ' + #13#10 + 'Set pk=''' +
                              String(Str[9]) + ''', FullName=''' + String
                              (Str[8]) + ''', INN=''' + String(Str[10])
                              + ''', KPP=''' + String(Str[11])
                              + ''', countrycode=''' + String(Str[12])
                              + ''', RegionCode=''' + String(Str[13])
                              + ''', Address=''' + String(Str[14])
                              + '''' + #13#10 + 'WHERE inn=''' + String
                              (Str[6]) + ''' and kpp=''' + String(Str[7])
                              + ''''
                          else
                            qr2.sql.Text :=
                              'INSERT INTO Producer(PK, FullName, INN, KPP, ' +
                              'countrycode, RegionCode, Address)' + #13#10 +
                              'Values(' + String(Str[9]) + ', ''' + String
                              (Str[8]) + ''', ''' + String(Str[10])
                              + ''', ''' + String(Str[11])
                              + ''', ''' + String(Str[12]) + ''', ''' + String
                              (Str[13]) + ''', ''' + String(Str[14]) + ''')';
                          qr2.ExecSQL;
                        except
//                          MemoEdit('������');
//                          MemoAdd
//                            ('������ ���������� ����������� ��������������');
                          er := True;
                        end;

                      UpdateMasProduction(Str, False);
                    end
                    except
                      er := True;
//                      MemoEdit('������');
//                      MemoAdd('������ ���������� ����������� ���������');
                    end;
                    if not er then
                    begin
//                      MemoEdit('���������');
//                      MemoAdd(
//                        '���������� ��������� ��������. ��������� ������ � EAN13 '
//                          + EAN13);
                    end
                end; // if not Er
              finally

              end;

            end;
          end;
        end;
        fmMove.cbAllKvartChange(fmMove.cbAllKvart);
        s := '������� ��������� ' + IntToStr(SL.Count) + ' ������� c ����������' + #10#13 +
          '��������� = ' + SLRow.Strings[0]
          + #10#13 + ' ��� = ' + SLRow.Strings[1] + ' ��� = ' + SLRow.Strings[2] + #10#13;
        Show_Inf(s, fInfo);
      except
        fmWork.Enabled := True;
        fmProgress.labAbout.Visible :=False;
        fmProgress.Close;
      end;
    finally
      qr2.free;
      ms1.free;
      ms.free;
      TCP.Socket.Close;
      TCP.Disconnect;
      TCP.Free;
    end;
  finally
    SL.Free;
    fmWork.Enabled := True;
    fmProgress.labAbout.Visible :=False;
    fmProgress.Close;
  end;
end;

function GetTypeXML(fname:string):TTypeXML;
var
  Xml: IXMLDocument;
  NFile, nFormO: IXMLNode;
  sOtch: string;
  SL: TStringList;
begin
  Result := t_null;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  SL := TStringList.Create;
  try

    SL.LoadFromFile(fName);
    SL.Text := FormatXMLData(SL.Text);
    Xml := TXMLDocument.Create(nil);
    Xml.Options := Xml.Options + [doNodeAutoIndent] -
      [doNodeAutoCreate, doAutoSave];
    Xml.NodeIndentStr := #9;
    Xml.Active := False;
    Xml.Xml.Assign(SL);
    Xml.Xml.Strings[0] :=
      '<?xml version="1.0" encoding="windows-1251"?>';
    Xml.Active := True;
    Xml.SaveToFile(fName);

    Xml.LoadFromFile(fName);
    NFile := Xml.DocumentElement; // �������� ��� ����
    nFormO := NFile.ChildNodes.FindNode('��������');
    if nFormO <> nil then
    begin
      try
        sOtch := String(nFormO.Attributes['�������']);
        if pos('11', sOtch) > 0 then
          Result := t_11o
        else
        if pos('12', sOtch) > 0 then
          Result := t_12o
        else
        if pos('6', sOtch) > 0 then
          Result := t_6o;
      except
        Result := t_null;
      end;
    end
    else
      Result := t_o;
  finally
    XML:=nil;
    SL.Free;
  end;
end;

function Load_XML_f12(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
  data: TDateTime;SelfKppXML:string): integer;
var
  SLProiz, SLEan, SLPost: TStringList;
  Xml: IXMLDocument;
  NFile,nRef,nPr, tmp, NDoc, nOO, nOb,nSvPrImp,nPostav,nProd:IXMLNode;
  CountPrXML,i, ii,rowXML,CountOO,CountOb,j,CountPrImp,k,idx,countProd,l, CountSal:integer;
  errPr, errSl,errFns,fl,f2,errRow,err:Boolean;
  prXML: TProizvXML;
  salXML: TSalerXML;
  pXML: fPrihXML;
  s,idPr, idSl,DNacl,NNacl,TM,Amount:string;
  prod: TProducer;
  saler:TSaler;
  decl: TDeclaration;
  V: Extended;
begin
  Result := 1;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  SLProiz := TStringList.Create;
  SLPost := TStringList.Create;
  SLEan := TStringList.Create;
  try
    try
      Xml.LoadFromFile(filename);
      NFile := Xml.DocumentElement; // �������� ��� ����
      nRef := NFile.ChildNodes.FindNode('�����������');
      if nRef <> nil then
      begin
        nPr := nRef.ChildNodes.FindNode('����������������������');
        if nPr <> nil then
        begin
          CountPrXML := nRef.ChildNodes.Count;

          for i := 0 to CountPrXML - 1 do
          begin
            nPr := nRef.ChildNodes.Nodes[i];
            if nPr.NodeName = '����������������������' then
            begin
              errPr := False;
              prXML := TProizvXML.Create;
              s := String(nPr.Attributes['�000000000004']);
              prXML.pName := s;
              tmp := nPr.ChildNodes.FindNode('��');
              if tmp <> nil then
              begin
                Result := 1;
                try
                  s := String(tmp.Attributes['�000000000005']);
                  prXML.pINN := s;
                except
                  prXML.pINN := '';
                  errPr := True;
                end;

                l := Length(prXML.pINN);
                case l of
                  0:
                  begin
                    try
                      prXml.pInn := NULLINN7;
                      prXML.pKPP := NullKPP7;
                    except
                      prXML.pKPP := NullKPP7;
                    end;
                  end;
                  8:
                    try
                      prXml.pInn := CreateNewInn(prXML.pInn);
                      prXML.pKPP := NullKPP8;
                    except
                      prXML.pKPP := NullKPP8;
                    end;
                  9:
                    try
                      prXml.pInn := CreateNewInn(prXML.pInn);
                      prXML.pKPP := NullKPP9;
                    except
                      prXML.pKPP := NullKPP9;
                    end;
                  10:
                    try
                      prXML.pKPP := tmp.Attributes['�000000000006'];
                    except
                      prXML.pKPP := '';
                    end;
                end;
              end;

              if (prXML.pKPP = '') or (prXML.pINN = '') then
                errPr := True;
              s := String(nPr.Attributes['�����������']);
              prXML.pId := s;
              prod.pINN := ShortString(prXML.pINN);
              prod.pKPP := ShortString(prXML.pKPP);
              if not errPr then
                SLProiz.AddObject(s, Pointer(prXML))
              else
              begin
                rowXML := GetRowXML(Xml.Xml, nPr.Xml);
                GetErrProducer_XML(SLErrXML, prod, rowXML);
                prXML.free;
              end;
            end;

            if nPr.NodeName = '����������' then
            begin
              errSl := False;
              salXML := TSalerXML.Create;
              s := String(nPr.Attributes['�000000000007']);
              salXML.sName := s;
              tmp := nPr.ChildNodes.FindNode('��');
              if tmp <> nil then
              begin
                Result := 1;
                try
                  s := String(tmp.Attributes['�000000000009']);
                  salXML.sINN := s;
                except
                  salXML.sINN := '';
                  errSl := True;
                end;

                try
                  s := tmp.Attributes['�000000000010'];
                  salXML.sKPP :=s;
                except
                  salXML.sKPP := '';
                end;
              end;

              if (salXML.sKPP = '') or (salXML.sINN = '') then
                errSl := True;
              s := String(nPr.Attributes['��������']);
              saler.sId := s;
              saler.sINN := ShortString(salXML.sINN);
              saler.sKPP := ShortString(salXML.sKPP);
              if not errPr then
                SLPost.AddObject(s, Pointer(salXML))
              else
              begin
                rowXML := GetRowXML(Xml.Xml, nPr.Xml);
                GetErrSaler_XML(SLErrXML, saler, rowXML);
                salXML.free;
              end;
            end;
          end;
        end;
      end;

      NDoc := NFile.ChildNodes.FindNode('��������');

      if NDoc <> nil then
      begin
        nOO := NDoc.ChildNodes.FindNode('������������');
        if nOO <> nil then
        begin
          CountOO := NDoc.ChildNodes.Count;
          for i := 0 to CountOO - 1 do
          begin
            nOO := NDoc.ChildNodes.Nodes[i];
            if nOO.NodeName = '������������' then
            begin
              pXML.SelfKPP := String(nOO.Attributes['�����']);
              s := String(nOO.Attributes['��������������']);
//              pXML.selfkpp := SelfKppXML;
              if UpperCase(s) = 'TRUE' then
              begin
                nOb := nOO.ChildNodes.FindNode('������');
                if nOb <> nil then
                begin
                  CountOb := nOO.ChildNodes.Count;
                  for j := 0 to CountOb - 1 do
                  begin
                    nOb := nOO.ChildNodes.Nodes[j];
                    if nOb.NodeName = '������' then
                    begin
                      try
                        pXML.grup := String
                          (nOb.Attributes['�000000000003']);
                      except
                        errFns := True;
                        pXML.grup := '';
                      end;
                      if pXML.grup = '' then
                      begin
                        s := TrimLeft
                          (Copy(nOb.Xml, 0, pos(#$d#$a, nOb.Xml) - 1));
                        rowXML := GetRowXML(Xml.Xml, s);
                        s := '������ ' + IntToStr(rowXML) + ' ';
                        s := s +
                          '����������� ������������ ���� ��� ����������� ���������';
                        SLErrXML.Add(s);
                        errFns := True;
                      end;

                      nSvPrImp := nOb.ChildNodes.FindNode
                        ('����������������');
                      if nSvPrImp <> nil then
                      begin
                        CountPrImp := nOb.ChildNodes.Count;
                        for k := 0 to CountPrImp - 1 do
                        begin
                          nSvPrImp := nOb.ChildNodes.Nodes[k];
                          if nSvPrImp.NodeName = '����������������' then
                          begin
                            errPr := False;
                            // ErrSal:=False;
                            idPr := String
                              (nSvPrImp.Attributes['�����������']);
                            idx := SLProiz.IndexOf(idPr);
                            if idx <> -1 then
                            begin
                              prXML := Pointer(SLProiz.Objects[idx]);
                              prod.pINN := ShortString(prXML.pINN);
                              prod.pKPP := ShortString(prXML.pKPP);
                              fl := GetProducer(fmWork.que2_sql, prod,
                                True, False);
                              if not fl then
                              begin
                                prod.pName := ShortString(prXML.pName);
                                prod.pAddress := '������';
                              end;
                            end
                            else
                              errPr := True;

                            nPostav := nSvPrImp.ChildNodes.FindNode
                              ('���������');
                            if nPostav <> nil then
                            begin
                              countSal := nSvPrImp.ChildNodes.Count;
                              for ii := 0 to countSal - 1 do
                              begin
                                nPostav := nSvPrImp.ChildNodes.Nodes[ii]; 

                                errSl := False;
                                            idSl := String
                                              (nPostav.Attributes['������������']);
                                            idx := SLPost.IndexOf(idSl);
                                            if idx <> -1 then
                                            begin
                                              salXML := Pointer(SLPost.Objects[idx]);
                                              saler.sINN := ShortString(salXML.sINN);
                                              saler.sKPP := ShortString(salXML.sKPP);
                                              fl := GetSaler(fmWork.que2_sql, saler,
                                                f2, True, False);
                                              if not fl then
                                              begin
                                                saler.sName := ShortString(salXML.sName);
                                                saler.sAddress := '������';
                                              end;
                                            end
                                            else
                                              errSl := True;

                                            nProd := nPostav.ChildNodes.FindNode
                                              ('���������');
                                            if nProd <> nil then
                                            begin
                                              countProd := nPostav.ChildNodes.Count;
                                              for L := 0 to countProd - 1 do
                                              begin
                                                Result := 0;
                                                errRow := False;
                                                NullDecl(decl);
                                                nProd := nPostav.ChildNodes.Nodes[L];
                                                try
                                                  DNacl := String
                                                  (nProd.Attributes['�200000000013']);
                                                except
                                                  DNacl := '';
                                                  errRow := True;
                                                end;
                                                try
                                                  NNacl := String
                                                  (nProd.Attributes['�200000000014']);
                                                except
                                                  NNacl := '';
                                                  errRow := True;
                                                end;
                                                decl.DeclNum := ShortString(NNacl);
                                                TM := String
                                                  (nProd.Attributes['�200000000015']);
                                                decl.TM := ShortString(TM);
                                                try
                                                  Amount := String
                                                  (nProd.Attributes['�200000000016']);
                                                except
                                                  Amount := '0';
                                                  errRow := True;
                                                end;
                                                if not ValidFloat(Amount, V) then
                                                  V := 0;

                                                if (NNacl = '') or (Amount = '0') or
                                                  (Amount = '') or (DNacl = '') then
                                                  errRow := True;

                                                pXML.vol := V;
                                                pXML.kol := V * 10;
                                                decl.Amount := pXML.kol;
                                                decl.vol := V;
                                                pXML.nnakl := NNacl;
                                                pXML.TM := TM;
                                                s := Copy(DNacl, 1, 2) + '.' + Copy
                                                  (DNacl, 4, 2) + '.' + Copy(DNacl, 7,
                                                  4);
                                                try
                                                  pXML.dnakl := StrToDateTime(s);
                                                  decl.DeclDate := StrToDateTime(s);
                                                except
                                                  decl.DeclDate := 0;
                                                  pXML.dnakl := 0;
                                                end;

                                                decl.product.AlcVol := 1;
                                                decl.product.capacity := 1;
                                                decl.TypeDecl := 1;
                                                decl.RepDate := dEnd;
                                                decl.SelfKPP := ShortString(pXML.SelfKPP);
                                                decl.sal := saler;
                                                decl.product.prod := prod;
                                                decl.product.FNSCode := ShortString
                                                  (pXML.grup);

                                                rowXML := GetRowXML(Xml.Xml, nProd.Xml);
                                                GetErrProduct_XML(SLErrXML, decl, rowXML);

                                                if errRow or errFns or errPr then
                                                  err := True
                                                else
                                                  err := False;

                                                if not err then
                                                begin
                                                  WriteFullDataXML(SLEan, DBQ, decl);
                                                  SLXml.AddObject
                                                  (String(decl.product.EAN13),
                                                  TDecl_obj.Create);
                                                  TDecl_obj
                                                  (SLXml.Objects[SLXml.Count - 1])
                                                  .data := decl;
                                                end;

                                              end;
                                            end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    except
      Result := 1;
    end;
  finally
    FreeStringList(SLProiz);
    FreeStringList(SLPost);
    Xml := nil;
    SLEan.free;
  end;
end;


function Load_XML_f11(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
  data: TDateTime;sal:TSaler; SelfKppXML:string): integer;
var
  SLProiz,SLEan: TStringList;
  Xml: IXMLDocument;
  NFile,nRef,nPr, tmp, NDoc, nOO, nOb,nSvPrImp,nPostav,nProd:IXMLNode;
  CountPrXML,i,rowXML,CountOO,CountOb,j,CountPrImp,k,idx,countProd,l:integer;
  errPr,errFns,fl,errRow,err:Boolean;
  prXML: TProizvXML;
  pXML: fPrihXML;
  s,idPr,DNacl,NNacl,TM,Amount:string;
  prod: TProducer;
  decl: TDeclaration;
  V: Extended;
begin
  Result := 1;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  SLProiz := TStringList.Create;
  SLEan := TStringList.Create;
  try
    try
      Xml.LoadFromFile(filename);
      NFile := Xml.DocumentElement; // �������� ��� ����
      nRef := NFile.ChildNodes.FindNode('�����������');
      if nRef <> nil then
      begin
        nPr := nRef.ChildNodes.FindNode('����������������������');
        if nPr <> nil then
        begin
          CountPrXML := nRef.ChildNodes.Count;
          for i := 0 to CountPrXML - 1 do
          begin

            nPr := nRef.ChildNodes.Nodes[i];
            if nPr.NodeName = '����������������������' then
            begin
              errPr := False;
              prXML := TProizvXML.Create;
              s := String(nPr.Attributes['�000000000004']);
              prXML.pName := s;
              tmp := nPr.ChildNodes.FindNode('��');
              if tmp <> nil then
              begin
                Result := 1;
                prXML.free;
                exit;
              end;
              try
                s := String(nPr.Attributes['�000000000005']);
                prXML.pINN := s;
              except
                prXML.pINN := '';
                errPr := True;
              end;
              l := Length(prXML.pINN);
              case l of
                8:
                  try
                    prXml.pInn := CreateNewInn(prXML.pInn);
                    prXML.pKPP := NullKPP8;
                  except
                    prXML.pKPP := NullKPP8;
                  end;
                9:
                  try
                    prXml.pInn := CreateNewInn(prXML.pInn);
                    prXML.pKPP := NullKPP9;
                  except
                    prXML.pKPP := NullKPP9;
                  end;
                10:
                  try
                    prXML.pKPP := nPr.Attributes['�000000000006'];
                  except
                    prXML.pKPP := '';
                  end;
              end;

              if (prXML.pKPP = '') or (prXML.pINN = '') then
                errPr := True;
              s := String(nPr.Attributes['�����������']);
              prXML.pId := s;
              prod.pINN := ShortString(prXML.pINN);
              prod.pKPP := ShortString(prXML.pKPP);
              if not errPr then
                SLProiz.AddObject(s, Pointer(prXML))
              else
              begin
                rowXML := GetRowXML(Xml.Xml, nPr.Xml);
                GetErrProducer_XML(SLErrXML, prod, rowXML);
                prXML.free;
              end;
            end;
          end;
        end;
      end;

      NDoc := NFile.ChildNodes.FindNode('��������');

      if NDoc <> nil then
      begin
        nOO := NDoc.ChildNodes.FindNode('������������');
        if nOO <> nil then
        begin
          CountOO := NDoc.ChildNodes.Count;
          for i := 0 to CountOO - 1 do
          begin
            nOO := NDoc.ChildNodes.Nodes[i];
            if nOO.NodeName = '������������' then
            begin
//              pXML.SelfKPP := String(nOO.Attributes['�����']);
              s := String(nOO.Attributes['��������������']);
              pXML.selfkpp := SelfKppXML;
              if UpperCase(s) = 'TRUE' then
              begin
                nOb := nOO.ChildNodes.FindNode('������');
                if nOb <> nil then
                begin
                  CountOb := nOO.ChildNodes.Count;
                  for j := 0 to CountOb - 1 do
                  begin
                    nOb := nOO.ChildNodes.Nodes[j];
                    if nOb.NodeName = '������' then
                    begin
                      try
                        pXML.grup := String
                          (nOb.Attributes['�000000000003']);
                      except
                        errFns := True;
                        pXML.grup := '';
                      end;
                      if pXML.grup = '' then
                      begin
                        s := TrimLeft
                          (Copy(nOb.Xml, 0, pos(#$d#$a, nOb.Xml) - 1));
                        rowXML := GetRowXML(Xml.Xml, s);
                        s := '������ ' + IntToStr(rowXML) + ' ';
                        s := s +
                          '����������� ������������ ���� ��� ����������� ���������';
                        SLErrXML.Add(s);
                        errFns := True;
                      end;

                      nSvPrImp := nOb.ChildNodes.FindNode
                        ('����������������');
                      if nSvPrImp <> nil then
                      begin
                        CountPrImp := nOb.ChildNodes.Count;
                        for k := 0 to CountPrImp - 1 do
                        begin
                          nSvPrImp := nOb.ChildNodes.Nodes[k];
                          if nSvPrImp.NodeName = '����������������' then
                          begin
                            errPr := False;
                            // ErrSal:=False;
                            idPr := String
                              (nSvPrImp.Attributes
                                ['�����������']);
                            idx := SLProiz.IndexOf(idPr);
                            if idx <> -1 then
                            begin
                              prXML := Pointer(SLProiz.Objects[idx]);
                              prod.pINN := ShortString(prXML.pINN);
                              prod.pKPP := ShortString(prXML.pKPP);
                              fl := GetProducer(fmWork.que2_sql, prod,
                                True, False);
                              if not fl then
                              begin
                                prod.pName := ShortString(prXML.pName);
                                prod.pAddress := '������';
                              end;
                            end
                            else
                              errPr := True;

                            nPostav := nSvPrImp.ChildNodes.FindNode
                              ('���������');
                            if nPostav <> nil then
                            begin
                              nProd := nPostav.ChildNodes.FindNode
                                ('���������');
                              if nProd <> nil then
                              begin
                                countProd := nPostav.ChildNodes.Count;
                                for L := 0 to countProd - 1 do
                                begin
                                  Result := 0;
                                  errRow := False;
                                  NullDecl(decl);
                                  nProd := nPostav.ChildNodes.Nodes[L];
                                  try
                                    DNacl := String
                                    (nProd.Attributes['�200000000013']);
                                  except
                                    DNacl := '';
                                    errRow := True;
                                  end;
                                  try
                                    NNacl := String
                                    (nProd.Attributes['�200000000014']);
                                  except
                                    NNacl := '';
                                    errRow := True;
                                  end;
                                  decl.DeclNum := ShortString(NNacl);
                                  TM := String
                                    (nProd.Attributes['�200000000015']);
                                  decl.TM := ShortString(TM);
                                  try
                                    Amount := String
                                    (nProd.Attributes['�200000000016']);
                                  except
                                    Amount := '0';
                                    errRow := True;
                                  end;
                                  if not ValidFloat(Amount, V) then
                                    V := 0;

                                  if (NNacl = '') or (Amount = '0') or
                                    (Amount = '') or (DNacl = '') then
                                    errRow := True;

                                  pXML.vol := V;
                                  pXML.kol := V * 10;
                                  decl.Amount := pXML.kol;
                                  decl.vol := V;
                                  pXML.nnakl := NNacl;
                                  pXML.TM := TM;
                                  s := Copy(DNacl, 1, 2) + '.' + Copy
                                    (DNacl, 4, 2) + '.' + Copy(DNacl, 7,
                                    4);
                                  try
                                    pXML.dnakl := StrToDateTime(s);
                                    decl.DeclDate := StrToDateTime(s);
                                  except
                                    decl.DeclDate := 0;
                                    pXML.dnakl := 0;
                                  end;

                                  decl.product.AlcVol := 1;
                                  decl.product.capacity := 1;
                                  decl.TypeDecl := 1;
                                  decl.RepDate := dEnd;
                                  decl.SelfKPP := ShortString(pXML.SelfKPP);
                                  decl.sal := sal;
                                  decl.product.prod := prod;
                                  decl.product.FNSCode := ShortString
                                    (pXML.grup);

                                  rowXML := GetRowXML(Xml.Xml, nProd.Xml);
                                  GetErrProduct_XML(SLErrXML, decl, rowXML);

                                  if errRow or errFns or errPr then
                                    err := True
                                  else
                                    err := False;

                                  if not err then
                                  begin
                                    WriteFullDataXML(SLEan, DBQ, decl);
                                    SLXml.AddObject
                                    (String(decl.product.EAN13),
                                    TDecl_obj.Create);
                                    TDecl_obj
                                    (SLXml.Objects[SLXml.Count - 1])
                                    .data := decl;
                                  end;

                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    except
      Result := 1;
    end;
  finally
    FreeStringList(SLProiz);
    Xml := nil;
    SLEan.free;
  end;
end;

function GetSalerXML(typeXML:TTypeXML; filename:string; var sal:TSaler):Boolean;
var
  Xml: IXMLDocument;
  NFile: IXMLNode;
begin
  Result:=True;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  try
    try
      Xml.LoadFromFile(filename);
      NFile := Xml.DocumentElement; // �������� ��� ����
      case typeXML of
        t_6o: GetSalerXML_6(XML,NFile, sal);
        t_11o: GetSalerXML_11(XML,NFile, sal);
        t_12o: GetSalerXML_12(XML,NFile, sal);
      end;
    except

    end;
  finally
    XML:=nil;
  end;
end;

function GetSalerXML_11(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
var
  nRef, nPost: IXMLNode;
  rowXML:integer;
  f1:Boolean;
begin
  Result:=True;
  nRef := NFile.ChildNodes.FindNode('�����������');
  if nRef <> nil then
  begin
    nPost := nRef.ChildNodes.FindNode('����������');
    if nPost <> nil then
    begin
      sal.sName := ShortString(nPost.Attributes['�000000000007']);
      nPost := nPost.ChildNodes.FindNode('��');
      if nPost <> nil then
      begin
        Result := False;
        try
          sal.sINN := ShortString(nPost.Attributes['�000000000009']);
        except
          Result := True;
          sal.sINN := '';
        end;
        try
          sal.sKpp := ShortString(nPost.Attributes['�000000000010']);
        except
          Result := True;
          sal.sKpp := '';
        end;
        if (sal.sINN = '') or (sal.sKpp = '') then
          Result := True;

        if Result and (sal.sInn <> '') and (sal.sKpp <> '') then
          GetSaler(fmWork.que2_sql, sal, f1, True, False)
        else
        begin
          rowXML := GetRowXML(Xml.Xml, nPost.Xml);
          GetErrSaler_XML(SLErrXML, sal, rowXML);
        end;
      end;
    end
    else
    begin
      Result := False;
      exit;
    end;
  end;
end;


function GetSalerXML_6(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
var
  nDoc, nOrg, nRekv, nOO: IXMLNode;
  rowXML, CountOO, i:integer;
  f1:Boolean;
  s:string;
begin
  Result:=True;
  nDoc := NFile.ChildNodes.FindNode('��������');
  if nDoc <> nil then
  begin
    nOrg := nDoc.ChildNodes.FindNode('�����������');
    if nOrg <> nil then
    begin
      nRekv := nOrg.ChildNodes.FindNode('���������');
      sal.sInn := ShortString(nRekv.Attributes['�����']);
      sal.sName := ShortString(nRekv.Attributes['������']);
    end
    else
    begin
      Result := False;
      exit;
    end;
    nOO := nDoc.ChildNodes.FindNode('�����������');
    if nOO <> nil then
    begin
      CountOO := NDoc.ChildNodes.Count;
      for i := 0 to CountOO - 1 do
      begin
        nOO := NDoc.ChildNodes.Nodes[i];
        if nOO.NodeName = '������������' then
        begin
          s := String(nOO.Attributes['���������������']);
          if UpperCase(s) = 'TRUE' then
            sal.sKpp := ShortString(nOO.Attributes['�����']);
        end
      end;
    end
    else
      Result:=False;

    if Result and (sal.sInn <> '') and (sal.sKpp <> '') then
      GetSaler(fmWork.que2_sql, sal, f1, True, False)
    else
    begin
      Result:=False;
      rowXML := GetRowXML(Xml.Xml, nOrg.Xml);
      GetErrSaler_XML(SLErrXML, sal, rowXML);
    end;
  end
  else
    Result:=False;
end;

function GetSelfKppXML(typeXML:TTypeXML; filename:string; var sInn,sKpp:string):Boolean;
var
  Xml: IXMLDocument;
  NFile: IXMLNode;
begin
  Result:=False;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  try
    try
      Xml.LoadFromFile(filename);
      NFile := Xml.DocumentElement; // �������� ��� ����end;
      case typeXML of
        t_6o:  Result:=GetSelfKppXML_6(Xml,nFile, sInn,sKpp);
        t_11o: Result:=GetSelfKppXML_11(Xml,nFile, sInn,sKpp);
        t_12o: Result:=GetSelfKppXML_12(Xml,nFile, sInn,sKpp);
      end;
    except
    end;
  finally
    XML:=nil;
  end;
end;

function GetSelfKppXML_6(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
var
  nRef, nKontr, nRez, nUL :IXMLNode;
begin
  Result:=True;
  sInn:='';
  sKpp:='';
  nRef := nFile.ChildNodes.FindNode('�����������');
  if nRef <> nil then
  begin
    nKontr := nRef.ChildNodes.FindNode('�����������');
    if nKontr <> nil then
    begin
      nRez := nKontr.ChildNodes.FindNode('��������');
      if nRez <> nil then
      begin
        nUL := nRez.ChildNodes.FindNode('��');
        if nUL <> nil then
        begin
          sInn:=String(nUL.Attributes['�000000000009']);
          sKpp:=String(nUL.Attributes['�000000000010']);
        end
        else
          begin
            nUL := nRez.ChildNodes.FindNode('��');
            if nUL <> nil then
            begin
              sInn:=String(nUL.Attributes['�000000000009']);
              sKpp:=Copy(sInn,1,4) + '01001';
            end
            else
              Result:=False;
          end;


      end
      else
        Result:=False;
    end
    else
      Result:=False;
  end;
end;



function GetSelfKppXML_11(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
var
  nDoc, nOrg, nRekv, nOO :IXMLNode;
  CountOO, i : integer;
  s:string;
begin
  Result:=True;
  sInn:='';
  sKpp:='';
  nDoc := nFile.ChildNodes.FindNode('��������');
  if nDoc <> nil then
  begin
    nOrg := nDoc.ChildNodes.FindNode('�����������');
    if nOrg <> nil then
    begin
      nRekv:=nOrg.ChildNodes.FindNode('���������');
      if nRekv <> nil then
        sInn := String(nRekv.Attributes['�����'])
      else
        Result:=False;
    end
    else
      Result:=False;
    nOO := nDoc.ChildNodes.FindNode('������������');
    if nOO <> nil then
    begin
      CountOO := NDoc.ChildNodes.Count;
      for i := 0 to CountOO - 1 do
      begin
        nOO := NDoc.ChildNodes.Nodes[i];
        if nOO.NodeName = '������������' then
        begin
          s := String(nOO.Attributes['��������������']);
          if UpperCase(s) = 'TRUE' then
            sKpp := String(nOO.Attributes['�����']);
        end
      end;
    end
    else
        Result:=False;
  end
  else
    Result:=False;
end;

function Load_XML_f6(var SLXml: TStringList; filename: string;
  DBQ: TADOQuery; data: TDateTime; sal: TSaler; SelfKppXML:string): integer;
var
  SLProiz, SLRow, SLEan: TStringList;
  Xml: IXMLDocument;
  NFile, nRef, NDoc, nTemp, nOOb, nOb, nImp, nPol, nPost, nPr, nKontr,
    nKontrR, nKontrRul, nFormO, nOrg, nRekv: IXMLNode;
  i, j, k, L, m, idPr, idx, XMLOObCount, XMLObCount, XMLImpCoumt,
    nXMLPolCount, nXMLPostCount, CountPr, rowXML: integer;
  GrFNS, DNacl, NNacl, TM, Amount, s, idPol, fOtch: string;
  V: Extended;
  decl: TDeclaration;
  prod: TProducer;
  fl: Boolean;
  err, errPr, errFns, errRow, f1: Boolean;
  pXML: fPrihXML;
begin
  Result := 0;
  SLProiz := TStringList.Create;
  SLEan := TStringList.Create;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  Xml.LoadFromFile(filename);
  try
    try
      NFile := Xml.DocumentElement; // �������� ��� ����

      NDoc := NFile.ChildNodes.FindNode('��������');
      nRef := NFile.ChildNodes.FindNode('�����������');
      if nRef <> nil then
      begin
        nPr := nRef.ChildNodes.FindNode('����������������������');
        if nPr <> nil then
        begin
          CountPr := nRef.ChildNodes.Count;
          for i := 0 to CountPr - 1 do
          begin
            errPr := False;
            nPr := nRef.ChildNodes.Nodes[i];
            if nPr.NodeName = '����������������������' then
            begin
              SLRow := TStringList.Create;
              try
                s := String(nPr.Attributes['�000000000004']);
                prod.pName := ShortString(s);
              except
                errPr := True;
                prod.pName := '';
              end;

              if not errPr then
                SLRow.Add(s);
              try
                s := String(nPr.Attributes['�000000000005']);
                prod.pINN := ShortString(s);
              except
                errPr := True;
                prod.pINN := '';
              end;


              l := Length(prod.pINN);
              case l of
                8:
                  try
                    prod.pInn := ShortString(CreateNewInn(String(prod.pInn)));
                    prod.pKPP := NullKPP8;
                  except
                    prod.pKPP := NullKPP8;
                  end;
                9:
                  try
                    prod.pInn := ShortString(CreateNewInn(String(prod.pInn)));
                    prod.pKPP := NullKPP9;
                  except
                    prod.pKPP := NullKPP9;
                  end;
                10:
                  try
                    prod.pKPP := ShortString(String(nPr.Attributes['�000000000006']));
                  except
                    prod.pKPP := '';
                  end;
              end;

              if not errPr then
              begin
                SLRow.Add(String(prod.pInn));
                SLRow.Add(String(prod.pKPP));
              end;

              if (prod.pINN = '') or (prod.pKPP = '') then
                errPr := True;

              s := String(nPr.Attributes['�����������']);

              if not errPr then
                SLProiz.AddObject(s, SLRow)
              else
              begin
                rowXML := GetRowXML(Xml.Xml, nPr.Xml);
                GetErrProducer_XML(SLErrXML, prod, rowXML);
                if (prod.pKPP = '') or (prod.pINN = '') then
                  Result := 1;
                SLRow.free;
              end;
            end;
          end;
        end;
      end;

      if NDoc <> nil then
      begin
        nTemp := NDoc.ChildNodes.FindNode('������������');
        if nTemp <> nil then
        begin
          XMLOObCount := NDoc.ChildNodes.Count;
          for i := 0 to XMLOObCount - 1 do
          begin
            nOOb := NDoc.ChildNodes.Nodes[i];
            if nOOb.NodeName = '������������' then
            begin
              nOb := nOOb.ChildNodes.FindNode('������');
              if nOb <> nil then
              begin
                XMLObCount := nOOb.ChildNodes.Count;
                for j := 0 to XMLObCount - 1 do
                begin
                  nOb := nOOb.ChildNodes.Nodes[j];
                  if nOb.NodeName = '������' then
                  begin
                    errFns := False;
                    try
                      GrFNS := String(nOb.Attributes['�000000000003']);
                    except
                      GrFNS := '';
                      errFns := True;
                    end;
                    if GrFNS = '' then
                    begin
                      s := TrimLeft
                        (Copy(nOb.Xml, 0, pos(#$d#$a, nOb.Xml) - 1));
                      rowXML := GetRowXML(Xml.Xml, s);
                      s := '������ ' + IntToStr(rowXML) + ' ';
                      s := s +
                        '����������� ������������ ���� ��� ����������� ���������';
                      SLErrXML.Add(s);
                      errFns := True;
                    end;

                    if pXML.ProdName = '' then
                      pXML.ProdName := '��������� ������ ' + GrFNS;
                    nImp := nOb.ChildNodes.FindNode('����������������');
                    if nImp <> nil then
                    begin
                      XMLImpCoumt := nOb.ChildNodes.Count;
                      for k := 0 to XMLImpCoumt - 1 do
                      begin
                        nImp := nOb.ChildNodes.Nodes[k];
                        idPr := StrToInt
                          (String(nImp.Attributes['�����������']));
                        nPol := nImp.ChildNodes.FindNode('����������');
                        decl.SelfKPP := ShortString(SelfKppXML);
                        if nPol <> nil then
                        begin
                          nXMLPolCount := nImp.ChildNodes.Count;
                          for L := 0 to nXMLPolCount - 1 do
                          begin
                            nPol := nImp.ChildNodes.Nodes[L];
                            nPost := nPol.ChildNodes.FindNode('��������');
                            if nPost <> nil then
                            begin
                              Result := 0;
                              errRow := False;
                              errPr := False;
                              nXMLPostCount := nPol.ChildNodes.Count;
                              for m := 0 to nXMLPostCount - 1 do
                              begin
                                decl.product.FNSCode := ShortString(GrFNS);
                                // decl.SelfKPP := kppCur;
                                nPost := nPol.ChildNodes.Nodes[m];
                                try
                                  DNacl := String
                                    (nPost.Attributes['�000000000018']);
                                except
                                  DNacl := '';
                                  errRow := True;
                                end;
                                try
                                  NNacl := String
                                    (nPost.Attributes['�000000000019']);
                                except
                                  NNacl := '';
                                  errRow := True;
                                end;
                                decl.DeclNum := ShortString(NNacl);
                                TM := String
                                  (nPost.Attributes['�000000000020']);
                                decl.TM := ShortString(TM);
                                try
                                  Amount := String
                                    (nPost.Attributes['�000000000021']);
                                except
                                  Amount := '0';
                                  errRow := True;
                                end;
                                if (NNacl = '') or (Amount = '0') or
                                  (Amount = '') or (DNacl = '') then
                                  errRow := True;

                                if not ValidFloat(Amount, V) then
                                  V := 0;
                                pXML.grup := GrFNS;
                                pXML.vol := V;
                                pXML.kol := V * 10;
                                decl.Amount := pXML.kol;
                                decl.vol := V;

                                decl.product.AlcVol := 1;
                                decl.product.capacity := 1;
                                pXML.nnakl := NNacl;

                                decl.TypeDecl := 1;
                                decl.RepDate := dEnd;
                                decl.forma := FormDecl
                                  (String(decl.product.FNSCode));

                                pXML.nnakl := NNacl;
                                pXML.TM := TM;
                                s := Copy(DNacl, 1, 2) + '.' + Copy(DNacl,
                                  4, 2) + '.' + Copy(DNacl, 7, 4);

                                try
                                  pXML.dnakl := StrToDateTime(s);
                                  decl.DeclDate := StrToDateTime(s);
                                except
                                  decl.DeclDate := 0;
                                  pXML.dnakl := 0;
                                end;

                                idx := SLProiz.IndexOf(IntToStr(idPr));
                                if idx <> -1 then
                                begin
                                  SLRow := Pointer(SLProiz.Objects[idx]);
                                  pXML.pName := SLRow.Strings[0];
                                  pXML.pINN := SLRow.Strings[1];
                                  pXML.pKPP := SLRow.Strings[2];
                                  prod.pINN := ShortString(pXML.pINN);
                                  prod.pKPP := ShortString(pXML.pKPP);
                                  fl := GetProducer(fmWork.que2_sql, prod,
                                    True, False);
                                  if not fl then
                                  begin
                                    prod.pName := ShortString(pXML.pName);
                                    prod.pAddress := '������';
                                  end;
                                end
                                else
                                  errPr := True;

                                decl.sal := sal;
                                decl.product.prod := prod;

                                rowXML := GetRowXML(Xml.Xml, nPost.Xml);
                                GetErrProduct_XML(SLErrXML, decl, rowXML);

                                if errRow or errFns or errPr then
                                  err := True
                                else
                                  err := False;

                                if not err then
                                begin
                                  WriteFullDataXML(SLEan, DBQ, decl);
                                  SLXml.AddObject
                                    (String(decl.product.EAN13),
                                    TDecl_obj.Create);
                                  TDecl_obj(SLXml.Objects[SLXml.Count - 1])
                                    .data := decl;
                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    except
      Result := 1;
    end;
  finally
    FreeStringList(SLProiz);
    Xml := nil;
    SLEan.free;
  end;
end;

function GetSalerXML_12(Xml: IXMLDocument;NFile: IXMLNode;var sal:TSaler):Boolean;
var
  nRef, nPost: IXMLNode;
  rowXML:integer;
  f1:Boolean;
begin
  Result:=True;
  nRef := NFile.ChildNodes.FindNode('�����������');
  if nRef <> nil then
  begin
    nPost := nRef.ChildNodes.FindNode('����������');
    if nPost <> nil then
    begin
      sal.sName := ShortString(nPost.Attributes['�000000000007']);
      nPost := nPost.ChildNodes.FindNode('��');
      if nPost <> nil then
      begin
        Result := False;
        try
          sal.sINN := ShortString(nPost.Attributes['�000000000009']);
        except
          Result := True;
          sal.sINN := '';
        end;
        try
          sal.sKpp := ShortString(nPost.Attributes['�000000000010']);
        except
          Result := True;
          sal.sKpp := '';
        end;
        if (sal.sINN = '') or (sal.sKpp = '') then
          Result := True;

        if Result and (sal.sInn <> '') and (sal.sKpp <> '') then
          GetSaler(fmWork.que2_sql, sal, f1, True, False)
        else
        begin
          rowXML := GetRowXML(Xml.Xml, nPost.Xml);
          GetErrSaler_XML(SLErrXML, sal, rowXML);
        end;
      end;
    end
    else
    begin
      Result := False;
      exit;
    end;
  end;
end;

function GetSelfKppXML_12(Xml:IXMLDocument;nFile:IXMLNode;var sInn,sKpp:string):Boolean;
var
  nDoc, nOrg, nRekv, nOO, nUl, nFL :IXMLNode;
  CountOO, i : integer;
  s:string;
begin
  Result:=True;
  sInn:='';
  sKpp:='';
  nDoc := nFile.ChildNodes.FindNode('��������');
  if nDoc <> nil then
  begin
    nOrg := nDoc.ChildNodes.FindNode('�����������');
    if nOrg <> nil then
    begin
      nRekv:=nOrg.ChildNodes.FindNode('���������');
      if nRekv <> nil then
      begin
        nUl := nRekv.ChildNodes.FindNode('��');
        nFL := nRekv.ChildNodes.FindNode('��');
        if nUl <> nil then
          sInn := String(nUl.Attributes['�����'])
        else
          if nFL <> nil  then
          begin
            sInn := String(nFL.Attributes['�����']);
            sKpp:= Copy(sInn,0,4) + '01001';
            exit;
          end
          else
            Result:=False;
      end
      else
        Result:=False;
    end
    else
      Result:=False;
    nOO := nDoc.ChildNodes.FindNode('������������');
    if nOO <> nil then
    begin
      CountOO := NDoc.ChildNodes.Count;
      for i := 0 to CountOO - 1 do
      begin
        nOO := NDoc.ChildNodes.Nodes[i];
        if nOO.NodeName = '������������' then
        begin
          s := String(nOO.Attributes['��������������']);
          if UpperCase(s) = 'TRUE' then
            sKpp := String(nOO.Attributes['�����']);
        end
      end;
    end
    else
        Result:=False;
  end
  else
    Result:=False;
end;

//function Load_XML_f12(var SLXml: TStringList; filename: string; DBQ: TADOQuery;
//  data: TDateTime; sal: TSaler; selfKppXML:string): integer;
//var
//  Xml: IXMLDocument;
//  NFile, NDoc, nRef, nOb, nParObor, NSal, nProd, nTemp, nFormO: IXMLNode;
//  GrFNS, s, NameOrg, inn, kpp, DNacl, NNacl, sINN, sKpp, Amount, TM,
//    fOtch: string;
//  DataFile: TDateTime;
//  idSal, idPr, i, j, k, L, idx, XMLRecCout, XMLProizCount, XMLSalCount,
//    XMLProdCount, rowXML: integer;
//  SLProiz, SLSal, SLRow: TStringList;
//  pXML: fPrihXML;
//  V: Extended;
//  prod: TProducer;
//  fl: Boolean;
//  decl: TDeclaration;
//  SLEan, SL: TStringList;
//  errPr, errSal, errFns, err, errRow: Boolean;
//begin
//  SLXml.Clear;
//  SLEan := TStringList.Create;
//  Result := 1;
//  SLProiz := TStringList.Create;
//  SLSal := TStringList.Create;
//
//  // ����������� XML ���� � ����������� �����
//  SL := TStringList.Create;
//  SL.LoadFromFile(filename);
//  SL.Text := FormatXMLData(SL.Text);
//  Xml := TXMLDocument.Create(nil);
//  Xml.Options := Xml.Options + [doNodeAutoIndent] - [doNodeAutoCreate,
//    doAutoSave];
//  Xml.NodeIndentStr := #9;
//  Xml.Active := False;
//  Xml.Xml.Assign(SL);
//  Xml.Xml.Strings[0] := '<?xml version="1.0" encoding="windows-1251"?>';
//  Xml.Active := True;
//  Xml.SaveToFile(filename);
//  err := False;
//
//  try
//    try
//      Xml.LoadFromFile(filename);
//      NFile := Xml.DocumentElement; // �������� ��� ����
//
//      if SLKPP.Count > 1 then
//      begin
//        if fmSelKpp = nil then
//          fmSelKpp := TfmSelKpp.Create(Application);
//        fmSelKpp.fSKPPTypeF := fKppXML;
//        fmSelKpp.ShowModal;
//        if fmSelKpp.ModalResult = mrOk then
//          fOrg.Kpp0 := ShortString(SLKPP.Strings[fmSelKpp.Tag]);
//      end
//      else
//        fOrg.Kpp0 := ShortString(SLKPP.Strings[0]);
//
//      NDoc := NFile.ChildNodes.FindNode('��������');
//      nRef := NFile.ChildNodes.FindNode('�����������');
//
//      if nRef <> nil then
//      begin
//        // ��������� ������ ��������������
//        GetProizXML(Xml.Xml, nRef, SLProiz);
//        // ��������� ������ �����������
//        GetPostXML(Xml.Xml, nRef, SLSal);
//      end;
//
//      if Assigned(NDoc) then
//      begin
//        nTemp := NDoc.ChildNodes.FindNode('������������');
//        if nTemp <> nil then
//        begin
//          nTemp := nTemp.ChildNodes.FindNode('������');
//          if Assigned(nTemp) then
//            nParObor := nTemp.ParentNode
//        end;
//      end;
//
//      if nParObor <> nil then
//      begin
//        XMLRecCout := nParObor.ChildNodes.Count;
//        for i := 0 to XMLRecCout - 1 do // ���� ���������
//        begin
//          errFns := False;
//          nOb := nParObor.ChildNodes.Nodes[i];
//          if nOb.NodeName = '������' then
//          begin
//            try
//              GrFNS := String(nOb.Attributes['�000000000003']);
//            except
//              GrFNS := '';
//              errFns := True;
//            end;
//            if GrFNS = '' then
//            begin
//              s := TrimLeft(Copy(nOb.Xml, 0, pos(#$d#$a, nOb.Xml) - 1));
//              rowXML := GetRowXML(Xml.Xml, s);
//              s := '������ ' + IntToStr(rowXML) + ' ';
//              s := s +
//                '����������� ������������ ���� ��� ����������� ���������';
//              SLErrXML.Add(s);
//              errFns := True;
//            end;
//
//            if pXML.ProdName = '' then
//              pXML.ProdName := '��������� ������ ' + GrFNS;
//            XMLProizCount := nOb.ChildNodes.Count;
//            for j := 0 to XMLProizCount - 1 do // �������������
//            begin
//              nTemp := nOb.ChildNodes.Nodes[j];
//              if nTemp.NodeName = '����������������' then
//              begin
//                idPr := StrToInt(String(nTemp.Attributes['�����������']));
//                if nTemp <> nil then
//                begin
//                  XMLSalCount := nTemp.ChildNodes.Count;
//                  for k := 0 to XMLSalCount - 1 do // ����������
//                  begin
//                    nTemp := nTemp.ChildNodes.FindNode('���������');
//                    if nTemp <> nil then
//                    begin
//                      idSal := StrToInt
//                        (String(nTemp.Attributes['������������']));
//                      nProd := nTemp.ChildNodes.FindNode('���������');
//                      if nProd <> nil then
//                      begin
//                        XMLProdCount := nTemp.ChildNodes.Count;
//                        for L := 0 to XMLProdCount - 1 do
//                        begin
//                          Result := 0;
//                          errPr := False;
//                          errSal := False;
//                          errRow := False;
//                          decl.product.FNSCode := ShortString(GrFNS);
//                          decl.SelfKPP := ShortString(selfKppXML);
//                          nProd := nTemp.ChildNodes.Nodes[L];
//                          try
//                            DNacl := String
//                              (nProd.Attributes
//                                ['�200000000013']);
//                          except
//                            DNacl := '';
//                            errRow := True;
//                          end;
//                          try
//                            NNacl := String
//                              (nProd.Attributes
//                                ['�200000000014']);
//                          except
//                            NNacl := '';
//                            errRow := True;
//                          end;
//                          decl.DeclNum := ShortString(NNacl);
//                          TM := String(nProd.Attributes['�200000000015']);
//                          decl.TM := ShortString(TM);
//                          try
//                            Amount := String
//                              (nProd.Attributes['�200000000016']);
//                          except
//                            Amount := '0';
//                          end;
//
//                          if (NNacl = '') or (Amount = '0') or
//                            (Amount = '') or (DNacl = '') then
//                            errRow := True;
//
//                          if not ValidFloat(Amount, V) then
//                            V := 0;
//                          pXML.grup := GrFNS;
//                          pXML.vol := V;
//                          pXML.kol := V * 10;
//                          decl.Amount := pXML.kol;
//                          decl.vol := V;
//                          decl.product.AlcVol := 1;
//                          decl.product.capacity := 1;
//                          pXML.nnakl := NNacl;
//                          pXML.TM := TM;
//                          s := Copy(DNacl, 1, 2) + '.' + Copy(DNacl, 4, 2)
//                            + '.' + Copy(DNacl, 7, 4);
//                          try
//                            pXML.dnakl := StrToDateTime(s);
//                            decl.DeclDate := StrToDateTime(s);
//                          except
//                            decl.DeclDate := 0;
//                            pXML.dnakl := 0;
//                          end;
//
//                          idx := SLProiz.IndexOf(IntToStr(idPr));
//                          if idx <> -1 then
//                          begin
//                            SLRow := Pointer(SLProiz.Objects[idx]);
//                            pXML.pName := SLRow.Strings[0];
//                            pXML.pINN := SLRow.Strings[1];
//                            pXML.pKPP := SLRow.Strings[2];
//                            prod.pINN := ShortString(pXML.pINN);
//                            prod.pKPP := ShortString(pXML.pKPP);
//                            fl := GetProducer(fmWork.que2_sql, prod, True,
//                              False);
//                            if not fl then
//                            begin
//                              prod.pName := ShortString(pXML.pName);
//                              prod.pAddress := '������';
//                            end;
//                          end
//                          else
//                            errPr := True;
//                          idx := SLSal.IndexOf(IntToStr(idSal));
//                          if idx <> -1 then
//                          begin
//                            SLRow := Pointer(SLSal.Objects[idx]);
//                            pXML.sName := SLRow.Strings[0];
//                            pXML.sINN := SLRow.Strings[1];
//                            pXML.sKpp := SLRow.Strings[2];
//                            sal.sINN := ShortString(pXML.sINN);
//                            sal.sKpp := ShortString(pXML.sKpp);
//                            GetSaler(fmWork.que2_sql, sal, fl, True, False);
//                          end
//                          else
//                            errSal := True;
//
//                          rowXML := GetRowXML(Xml.Xml, nProd.Xml);
//                          GetErrProduct_XML(SLErrXML, decl, rowXML);
//
//                          if errRow or errFns or errPr or errSal then
//                            err := True
//                          else
//                            err := False;
//                          decl.product.prod := prod;
//                          decl.sal := sal;
//                          decl.TypeDecl := 1;
//                          decl.RepDate := dEnd;
//                          decl.forma := FormDecl
//                            (String(decl.product.FNSCode));
//                          if not err then
//                          begin
//                            WriteFullDataXML(SLEan, DBQ, decl);
//                            SLXml.AddObject(String(decl.product.EAN13),
//                              TDecl_obj.Create);
//                            TDecl_obj(SLXml.Objects[SLXml.Count - 1])
//                              .data := decl;
//                          end;
//                        end;
//                      end;
//                    end;
//                  end; // ����������
//                end;
//              end;
//            end // �������������
//          end;
//        end; // ���� ���������
//      end;
//    except
//      Result := 1;
//    end;
//  finally
//    Xml := nil;
//    FreeStringList(SLSal);
//    FreeStringList(SLProiz);
//    SLEan.free;
//    SL.free;
//  end;
//end;

procedure DownLoadHelp;
const
//  SourceFile = 'https://dl.dropboxusercontent.com/s/85nvki092yysel6/TeamViewerQS_8.exe?dl=1&token_hash=AAFKqtRDlxjxrmV8b3D3oU4WfRt25eNlbywMs7wPhOKRyA';
  SourceFile = 'http://yadi.sk/d/s97g4dOALW9hx';
var
  DestFile :string;
  butsel:integer;
begin
  butsel := Show_Inf('����� ������� ������� �� ��������� ��������������. ����������?', fConfirm);
  if butsel <> mrOk then
    exit
  else
    begin
      DestFile := GetSpecialPath(CSIDL_DESKTOP)+'\Kniga.pdf';
      Show_Inf('���������� ����� ������ ��������� ����� .... ', fConfirm);
      try
        Screen.Cursor := crHourGlass;
        if DownloadFile(SourceFile, DestFile) then
           begin
           ShellExecute(Application.Handle, PChar('open'), PChar(DestFile),
             PChar(''), nil, SW_NORMAL)
           end
        else
          Show_Inf('������ �������� �������',fError);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
end;

procedure ProcDownload;
const
//  SourceFile = 'https://dl.dropboxusercontent.com/s/85nvki092yysel6/TeamViewerQS_8.exe?dl=1&token_hash=AAFKqtRDlxjxrmV8b3D3oU4WfRt25eNlbywMs7wPhOKRyA';
  SourceFile = 'http://bmsystem.dyndns.org/UpdateAlcoRetail/TeamViewerQS_ru.exe';
var
  DestFile :string;
  fn:string;
  butsel:integer;
begin
  //����� ����� TeamViewer �� ������� �����
  fn:='';
  fn:=FindFiles(GetSpecialPath(CSIDL_DESKTOP), 'TeamViewer*.exe');
  if fn <> '' then
    ShellExecute(Application.Handle, PChar('open'), PChar(fn),
       PChar(''), nil, SW_NORMAL)
  else
    begin
      butsel := Show_Inf('���� ���������� ��������� �� ������� ����� �� ������, ������� ����?', fConfirm);
      if butsel <> mrOk then
        exit
      else
        begin
          DestFile := GetSpecialPath(CSIDL_DESKTOP)+'\TeamViewerQS8.exe';
          Show_Inf('���������� ����� ������ ��������� ����� .... ', fConfirm);
          try
            Screen.Cursor := crHourGlass;
            if DownloadFile(SourceFile, DestFile) then
               begin
               ShellExecute(Application.Handle, PChar('open'), PChar(DestFile),
                 PChar(''), nil, SW_NORMAL)
               end
            else
              Show_Inf('������ �������� TeamViewer',fError);
          finally
            Screen.Cursor := crDefault;
          end;
        end;
    end;
end;


procedure HelpDownload;
const
  SourceFile = 'http://bmsystem.dyndns.org/UpdateAlcoRetail/Help_3.2.10.0.pdf';
var
  DestFile :string;
  fn:string;
  butsel:integer;
  dl:Boolean;
begin
  DestFile := GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\Help_3.2.10.0.pdf';
  dl:=False;
  //����� ����� �������
  fn:='';
  fn:=FindFiles(GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\', 'Help*.pdf');
  if fn <> '' then
  begin
    if fn <> DestFile then
    begin
      butsel := Show_Inf('���� ������� ���������. �� ������ ������� ����� ����?', fConfirm);
      if butsel <> mrOk then
        ShellExecute(Application.Handle, PChar('open'), PChar(fn),
           PChar(''), nil, SW_NORMAL)
      else
        dl:=True;
    end
    else
      ShellExecute(Application.Handle, PChar('open'), PChar(fn),
             PChar(''), nil, SW_NORMAL)
  end
  else
    begin
      butsel := Show_Inf('���� ������� �� ������, ������� ����?', fConfirm);
      if butsel <> mrOk then
        exit
      else
        dl:=True;
    end;
  if dl then
  begin
    Show_Inf('���������� ����� ������� ������ ��������� ����� .... ', fConfirm);
    try
      Screen.Cursor := crHourGlass;
      if DownloadFile(SourceFile, DestFile) then
         begin
         ShellExecute(Application.Handle, PChar('open'), PChar(DestFile),
           PChar(''), nil, SW_NORMAL)
         end
      else
        Show_Inf('������ �������� �������',fError);
    finally
      Screen.Cursor := crDefault;
    end;
    //������ ������ Help
    fn:=FindFiles(GetSpecialPath(CSIDL_PROGRAM_FILES) + '\' + NameProg + '\', 'Help*.pdf');
    if fn <> DestFile then
      DeleteFile(PWideChar(fn));
  end;
end;

procedure LoadDir;
var dir_path:string;
begin
 try
   if not DirectoryExists(path_db) then
     begin
       dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
       dir_path:=dir_path+fOrg.Inn+'\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
       dir_path:=dir_path+'DataBase\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
     end;
   if not DirectoryExists(path_arh) then
     begin
       dir_path := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
       dir_path:=dir_path+fOrg.Inn+'\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
       dir_path:=dir_path+'BackUp\';
       if not DirectoryExists(dir_path) then
         MkDir(dir_path);
     end;
   if not DirectoryExists(path_arh) then
     MkDir(path_arh);
   if not FileExists(db_decl) then
     Create_DBDecl(fmWork.que1_sql,db_decl,fOrg.Inn);
   if not FileExists(db_spr) then
     Create_DBSpr(fmWork.que2_sql,db_spr,psw_loc);
  except
    Show_Inf('������ �������� �������� ���� ������. ��������� ����� �������.', fError);
   Application.Terminate;
  end;
end;

procedure GetDataGroupXML(s: string; SL: TStringList; var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.FNSCode) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;

procedure GetDataProizvXML(s: string; SL: TStringList;
  var SLFiltr: TStringList);
var
  i: integer;
  decl: TDeclaration;
begin
//  SLFiltr.clear;
  ClearSL(SLFiltr);
  for i := 0 to SL.Count - 1 do
  begin
    decl := TDecl_obj(SL.Objects[i]).Data;
    if String(decl.product.prod.pId) = s then
      begin
        SLFiltr.AddObject(SL.Strings[i], TDecl_obj.Create);
        TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
      end;
  end;
end;



function Load_XMLF(fTF: TTypeForm;var SLXml: TStringList; filename: string;
  DBQ: TADOQuery; dat: TDateTime; var sal:TSaler; var ExtV:Boolean):integer;
var idx,p:integer;
  s, sName, sInn, sKpp, SelfKppXML:string;
begin
  Result := 0;
  //�������� �� ������� ������ � XML
  if not ExtDataXML(filename) then
  begin
    Result:=2;
    exit;
  end;

  if SLKPP.Count > 1 then
    begin
      if fmSelKpp = nil then
        fmSelKpp := TfmSelKpp.Create(Application);
      fmSelKpp.fSKPPTypeF := fKppXML;
      fmSelKpp.ShowModal;
      if fmSelKpp.ModalResult = mrOk then
        SelfKppXML := String(SLKPP.Strings[fmSelKpp.Tag]);
    end
    else
      SelfKppXML := String(SLKPP.Strings[0]);

    if fmFiltrSal = nil then
      fmFiltrSal := TfmFiltrSal.Create(Application);
    fmFiltrSal.StrSQLS := StrSQLSaler;
    fmFiltrSal.fStat := fEdit;
    fmFiltrSal.ShowModal;
    if fmFiltrSal.ModalResult = mrOk then
    begin
      idx := fmFiltrSal.cbFullName.ItemIndex;
      if idx <> -1 then
      begin
        s := fmFiltrSal.cbFullName.Items.Strings[idx];
        p := pos('-', s);
        sName := Copy(s, p + 2, Length(s) - p - 1);
        sINN := Copy(s, 0, p - 2);
        sal.sName := ShortString(sName);
        sal.sINN := ShortString(sINN);
        sKpp := fmFiltrSal.cbKPP.Items.Strings
          [fmFiltrSal.cbKPP.ItemIndex];
        sal.sKpp := ShortString(sKpp);
      end;
      SLXml.Clear;
      Result := Load_XML(fTF,SLXml, filename, DBQ, dat, sal,SelfKppXML, ExtV)
    end
    else
      Result := 0;

end;

procedure PrihodSaler(sKpp:string; Sal:TSaler; SLXML,SLDecl:TStringList;
  dBeg,dEnd:TDateTime; SG:TAdvStringGrid);
var i,j,l,k,n,m,h,idx,SGRow,SGRowN,SGRowK, SGRowD:Integer;
  decl:TDeclaration;
  sd: TSalerDvizenie;
  SL, SLDate, SLDeclDate, SLNakl, SLDeclNacl, SLFNS, SLDeclFNS:TStringList;
  SLDateXML, SLDeclDateXML, SLNaklXML, SLDeclNaclXML, SLFNSXML{, SLDeclFNS}:TStringList;
  ss,sDate, sNakl, sFNS:string;
  vol_kod, vol_nakl, vol_kod_xml, vol_nakl_xml, e:Extended;
  bXML, IsErr:Boolean;
begin
  if SLXML.Count > 0 then bXML := True else bXML := False;

  SGRow:= 0;
  SGRowD := 0;
  IsErr:= False;

  SL:=TStringList.Create;
  SLDate := TStringList.Create;
  SLDeclDate := TStringList.Create;
  SLNakl := TStringList.Create;
  SLDeclNacl := TStringList.Create;
  SLFNS:=TStringList.Create;
  SLDeclFNS := TStringList.Create;
  SLDateXML := TStringList.Create;
  SLDeclDateXML := TStringList.Create;
  SLNaklXML := TStringList.Create;
  SLDeclNaclXML := TStringList.Create;
  SLFNSXML:=TStringList.Create;
  try
    //����������� ������ ��� ���������� ����������, ���������� ��� � �� ������� �������
    for i := 0 to SLDecl.Count - 1 do
    begin
      decl:=TDecl_obj(SLDecl.Objects[i]).Data;
      if(decl.SelfKpp = sKpp) and
        (decl.sal.sInn = Sal.sInn) and
        (decl.sal.sKpp = sal.sKpp) and
        (DBetween(Int(decl.DeclDate), dBeg, dEnd)) then
      begin
        SL.AddObject(decl.pk, TDecl_obj.Create);
        TDecl_obj(SL.Objects[SL.Count - 1]).data := decl;
      end;
    end;

    SLDate.Clear;
    //������� ������ ���
    for i := 0 to SL.Count - 1 do
    begin
      decl:=TDecl_obj(SL.Objects[i]).Data;
      ss:= DateToStr(decl.DeclDate);
      if SLDate.IndexOf(ss) = -1 then
        SLDate.Add(ss);
    end;  
    SLDate.CustomSort(CompDateAsc);

    //������� ������ ��� XML
    if bXML then
    begin
      SLDateXML.Clear;
      for i := 0 to SLXML.Count - 1 do
      begin
        decl:=TDecl_obj(SLXML.Objects[i]).Data;
        ss:= DateToStr(decl.DeclDate);
        if SLDateXML.IndexOf(ss) = -1 then
          SLDateXML.Add(ss);
      end;
      SLDateXML.CustomSort(CompDateAsc);
    end;
    
    for i := 0 to SLDate.Count - 1 do
    begin
      sDate:=SLDate.Strings[i];
      SLDeclDate.clear;
      //����������� ������ �� ������� ����
      for j:=0 to SL.Count - 1 do
      begin
        decl:=TDecl_obj(SL.Objects[j]).Data;
        if DateToStr(decl.DeclDate) = sDate then
        begin
          SLDeclDate.AddObject(decl.pk, TDecl_obj.Create);
          TDecl_obj(SLDeclDate.Objects[SLDeclDate.Count - 1]).data := decl;
        end;
      end;

      if bXML and (SLDateXML.IndexOf(sDate) <> -1) then
      begin
        SLDeclDateXML.clear;
        //����������� ������ �� ������� ����
        for j:=0 to SLXML.Count - 1 do
        begin
          decl:=TDecl_obj(SLXML.Objects[j]).Data;
          if DateToStr(decl.DeclDate) = sDate then
          begin
            SLDeclDateXML.AddObject(decl.pk, TDecl_obj.Create);
            TDecl_obj(SLDeclDateXML.Objects[SLDeclDateXML.Count - 1]).data := decl;
          end;
        end;
      end;
      
      SLNakl.Clear;
      //������ ������ ��������� �� ����
      for l:=0 to SLDeclDate.Count - 1 do
      begin
        decl:=TDecl_obj(SLDeclDate.Objects[l]).Data;
        ss:= decl.DeclNum;
        if SLNakl.IndexOf(ss) = -1 then
          SLNakl.Add(ss);
      end;
      SLNakl.Sort;

      if bXML then
      begin
        SLNaklXML.Clear;
        //������ ������ ��������� �� ����
        for l:=0 to SLDeclDateXML.Count - 1 do
        begin
          decl:=TDecl_obj(SLDeclDateXML.Objects[l]).Data;
          ss:= decl.DeclNum;
          if SLNaklXML.IndexOf(ss) = -1 then
            SLNaklXML.Add(ss);
        end;
        SLNaklXML.Sort;
      end;
      
      //����� ��� ������ ���������
      for l := 0 to SLNakl.Count - 1 do
      begin
        sNakl:=SLNakl.Strings[l];
        SLDeclNacl.clear;

        Inc(SGRow);
        SG.RowCount := SG.RowCount + 1;

        SG.MergeCells(9, SGRow, 3, 1);
        SG.Alignments[9, SGRow] := taRightJustify;
        SG.CellProperties[9, SGRow].FontSize := 9;
        SG.CellProperties[9, SGRow].FontStyle := [fsBold];
        SG.Cells[9,SGRow] := '���� ' + sDate + ' ��������� ' + sNakl;
        vol_nakl := 0;
        vol_nakl_xml := 0;
        SGRowN:= SGRow;

        for k := 0 to SLDeclDate.Count - 1 do
        begin  
          decl:=TDecl_obj(SLDeclDate.Objects[k]).Data;
          if decl.DeclNum = sNakl then
          begin
            SLDeclNacl.AddObject(decl.pk, TDecl_obj.Create);
            TDecl_obj(SLDeclNacl.Objects[SLDeclNacl.Count - 1]).data := decl;
          end;
        end;

        if bXML and (SLNaklXML.IndexOf(sNakl) <> -1) then
        begin
          SLDeclNaclXML.clear;
          for k := 0 to SLDeclDateXML.Count - 1 do
          begin  
            decl:=TDecl_obj(SLDeclDateXML.Objects[k]).Data;
            if decl.DeclNum = sNakl then
            begin
              SLDeclNaclXML.AddObject(decl.pk, TDecl_obj.Create);
              TDecl_obj(SLDeclNaclXML.Objects[SLDeclNaclXML.Count - 1]).data := decl;
            end;
          end;
        end;
        
        SLFNS.Clear;
        for n := 0 to SLDeclNacl.Count - 1 do
        begin
          decl:=TDecl_obj(SLDeclNacl.Objects[n]).Data;
          ss:= decl.product.FNSCode;  
          if SLFNS.IndexOf(ss) = -1 then
          SLFNS.Add(ss);
        end;
        SLFNS.Sort;

        if bXML then
        begin
          SLFNSXML.Clear;
          for n := 0 to SLDeclNaclXML.Count - 1 do
          begin
            decl:=TDecl_obj(SLDeclNaclXML.Objects[n]).Data;
            ss:= decl.product.FNSCode;  
            if SLFNSXML.IndexOf(ss) = -1 then
            SLFNSXML.Add(ss);
          end;
          SLFNSXML.Sort;
        end;
        
        for n := 0 to SLFNS.Count - 1 do
        begin  
          sFNS:=SLFNS.Strings[n];
          SLDeclFNS.clear;
          Inc(SGRow);
          SG.RowCount := SG.RowCount + 1;
          SG.MergeCells(10, SGRow, 2, 1);
          SG.Alignments[10, SGRow] := taRightJustify;
          SG.CellProperties[10, SGRow].FontSize := 9;
          SG.CellProperties[10, SGRow].FontStyle := [fsBold];
          SG.Cells[10,SGRow] := '��� �� ' + sFNS;
          vol_kod := 0;
          vol_kod_xml := 0;
          SGRowK:=SGRow;
          if bXML and (SLFNSXML.IndexOf(sFNS) <> -1) then
            for m := 0 to SLDeclNaclXML.Count - 1 do
            begin
              decl:=TDecl_obj(SLDeclNaclXML.Objects[m]).Data;
              if decl.product.FNSCode = sFNS then
              begin
                vol_kod_xml := vol_kod_xml + decl.Vol;
                vol_nakl_xml := vol_nakl_xml + decl.Vol;
              end;
            end;
          
          for m := 0 to SLDeclNacl.Count - 1 do
          begin
            decl:=TDecl_obj(SLDeclNacl.Objects[m]).Data;
            if decl.product.FNSCode = sFNS then
            begin
              SLDeclFNS.AddObject(decl.pk, TDecl_obj.Create);
              TDecl_obj(SLDeclFNS.Objects[SLDeclFNS.Count - 1]).data := decl;
              Inc(SGRow);
              SG.RowCount := SG.RowCount + 1;
              SG.Cells[1,SGRow] := IntToStr(decl.forma);
              SG.Cells[2,SGRow] := decl.product.FNSCode;
              SG.Cells[3,SGRow] := decl.SelfKpp;
              SG.Cells[4,SGRow] := decl.DeclNum;
              SG.Cells[5,SGRow] := DateToStr(decl.DeclDate);
              SG.Cells[6,SGRow] := decl.sal.sInn;
              SG.Cells[7,SGRow] := decl.sal.sKpp;
              SG.Cells[8,SGRow] := decl.sal.sName;
              SG.Cells[9,SGRow] := decl.product.EAN13;
              SG.Cells[10,SGRow] := decl.product.Productname;
              SG.Floats[11,SGRow] := decl.Amount;
              SG.Floats[12,SGRow] := decl.Vol;
              SG.Cells[13,SGRow] := decl.TM;
              SG.Cells[14,SGRow] := decl.product.prod.pName;
              SG.Cells[15,SGRow] := decl.product.prod.pInn;
              SG.Cells[16,SGRow] := decl.product.prod.pKpp;
              vol_kod := vol_kod + decl.Vol;
              vol_nakl := vol_nakl + decl.Vol;

              Inc(SGRowD);
              SG.Cells[0,SGRow] := IntToStr(SGRowD);
            end;
          end;
          SG.Alignments[12, SGRowK] := taRightJustify;
          SG.CellProperties[12, SGRowK].FontSize := 9;
          SG.CellProperties[12, SGRowK].FontStyle := [fsBold];
          SG.Floats[12,SGRowK] := vol_kod;
          if bXML then
          begin
            SG.Alignments[17, SGRowK] := taCenter;
//            SG.Alignments[17, SGRowK] := taRightJustify;
            SG.CellProperties[17, SGRowK].FontSize := 9;
            SG.CellProperties[17, SGRowK].FontStyle := [fsBold];
            SG.Floats[17,SGRowK] := vol_kod_xml;

            e := RoundTo(vol_kod_xml-vol_kod,-ZnFld);
            if e <> 0 then
            begin
              SG.Alignments[18, SGRowK] := taRightJustify;
              SG.CellProperties[18, SGRowK].FontSize := 9;
              SG.CellProperties[18, SGRowK].FontStyle := [fsBold];
//              SG.CellProperties[18, SGRowK].BrushColor := clYellow;
//              SG.CellProperties[18, SGRowN].BrushColor := clYellow;
              SG.RowColor[SGRowK]:=clYellow;
              SG.RowColor[SGRowN]:=LighterS(clYellow,60);
              SG.Floats[18,SGRowK] := vol_kod_xml-vol_kod;
              if not IsErr then
                IsErr := True;
            end;
          end;
        end;
        SG.Alignments[12, SGRowN] := taRightJustify;
        SG.CellProperties[12, SGRowN].FontSize := 9;
        SG.CellProperties[12, SGRowN].FontStyle := [fsBold];
        SG.Floats[12,SGRowN] := vol_nakl;
        if bXML then
        begin
          SG.Alignments[17, SGRowN] := taRightJustify;
          SG.CellProperties[17, SGRowN].FontSize := 9;
          SG.CellProperties[17, SGRowN].FontStyle := [fsBold];
          SG.Floats[17,SGRowN] := vol_nakl_xml;
          
          e := RoundTo(vol_nakl_xml-vol_nakl,-ZnFld);
          if e <> 0 then
          begin
            SG.Alignments[18, SGRowN] := taRightJustify;
            SG.CellProperties[18, SGRowN].FontSize := 9;
            SG.CellProperties[18, SGRowN].FontStyle := [fsBold];
            SG.RowColor[SGRowN]:=clYellow;
            SG.Floats[18,SGRowN] := vol_nakl_xml-vol_nakl;
            if not IsErr then
              IsErr := True;
          end;
        end;
      end;
    end;
    SG.RowCount := SG.RowCount - 1;
    SG.Cells[0, SG.RowCount - 1] := SG.Cells[0, SG.RowCount - 2];
    if isErr then
      SG.RowColor[SG.RowCount - 1] := clYellow;
    // ������� ������� ��������
    for h := 0 to High(HideColCorrectSaler) do
    begin
      idx := HideColCorrectSaler[h];
      SG.HideColumn(idx);
    end;
  finally
    FreeStringList(SLFNSXML);
    FreeStringList(SLDeclNaclXML);
    FreeStringList(SLNaklXML);
    
    FreeStringList(SLDateXML);
    FreeStringList(SLDeclFNS);
    FreeStringList(SLFNS);
    FreeStringList(SLDeclNacl);
    FreeStringList(SLNakl);
    
    FreeStringList(SLDate);
    
    FreeStringList(SLDeclDateXML);
    FreeStringList(SLDeclDate);
    FreeStringList(SL);
  end;
end;


end.
