unit dfmStatistic;

interface

uses
  Forms, Grids, AdvObj, BaseGrid, AdvGrid, Buttons, Controls, StdCtrls,
  ComCtrls, Classes, ExtCtrls, XLSFile, SysUtils, ShellAPI, ShlObj, IniFiles,
  Windows;

type
  TfmStatistic = class(TForm)
    pnl1: TPanel;
    lbl1: TLabel;
    lblVer: TLabel;
    btn1: TButton;
    pnl5: TPanel;
    pnl6: TPanel;
    dtpDEnd: TDateTimePicker;
    pnl7: TPanel;
    lbl2: TLabel;
    pnl8: TPanel;
    lbl3: TLabel;
    pnl9: TPanel;
    cbbAllKvart: TComboBox;
    pnl10: TPanel;
    dtpDBeg: TDateTimePicker;
    pnl11: TPanel;
    btn2: TSpeedButton;
    pnl2: TPanel;
    btn3: TSpeedButton;
    SG_Stat: TAdvStringGrid;
    pnl3: TPanel;
    btn4: TSpeedButton;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbbAllKvartChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CreateStatistik;
  end;

var
  fmStatistic: TfmStatistic;
  dsBeg, dsEnd:TDateTime;
  SLStat:TStringList;
  xls: TXLSFile;


implementation
uses dfmWork, GlobalUtils, Global, GlobalConst, dfmCena;

{$R *.dfm}

procedure TfmStatistic.btn1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
  fmWork.sb_Main.Flat := False;
  fmWork.JvRoll_DeclExpand(Self);
end;

procedure TfmStatistic.btn2Click(Sender: TObject);
var SLOb, SLStat:TStringList;
begin
  SLStat:=TStringList.Create;
  SLOb:=TStringList.Create;
  try
    // �������� ��� ������� ��� � ������� �������� ��� ���� �����������
    DvizenieEAN(SLOb, SLAllDecl, dsBeg, dsEnd);
    //��������� ��������� ��� ����������
    FillDataStatistic(SLOb, SLStat);
    //���������� ������
    FillMasStatistic(SLStat, SG_Stat);
  finally
    FreeStringList(SLOb);
    FreeStringList(SLStat);
  end;
end;

procedure TfmStatistic.btn3Click(Sender: TObject);
var r: TResourceStream;
    ms : TMemoryStream;
    NewPath:string;
    i,j, Col,Row, k:integer;
    e:Extended;
begin
  xls.Clear;
  r := TResourceStream.Create(HInstance, 'res_stat', RT_RCDATA);
  ms := TMemoryStream.Create;
  NewPath := WinTemp + NameProg;
  try
    if not DirectoryExists(NewPath) then
      MkDir(NewPath);

    ms.LoadFromStream(r);
    NewPath := NewPath+'\stat13.xls';
    ms.SaveToFile(NewPath);

    xls.OpenFile(NewPath);
    k:=3;
//    j:=1;
    for i := SG_Stat.FixedRows to SG_Stat.RowCount do
      for j := 0 to High(ColExcelStat) do
        begin
          Col := ColExcelStat[j];
          Row := RowExcelStat[i-SG_Stat.FixedRows];
          if SG_Stat.Cells[k+j,i] <> '' then
            e:=StrToFloat(SG_Stat.Cells[k+j,i])
          else
            e:=0;
          xls.Workbook.Sheets[0].Cells[Row-1,Col-1].Value:=e;
        end;
    if FileExists('Stat14.xls') then
      DeleteFile(PWideChar('Stat13.xls'));

    xls.SaveAs('Stat14.xls');
    ShellExecute(0, 'open', PChar('Stat14.xls'), nil, nil, SW_SHOW);

  finally
    ms.Free;
    r.Free;
  end;
end;

procedure TfmStatistic.btn4Click(Sender: TObject);
begin
  if fmCena = nil then
    fmCena := TfmCena.Create(Application);
  if fmCena.ShowModal = mrOk then
    SG_Stat.ClearNormalCols(3,5);
end;

procedure TfmStatistic.cbbAllKvartChange(Sender: TObject);
var m,d,y:word;
  s:string;
begin
//  DecodeDate(Now(),y,m,d);
  DecodeDate(GetCurDate(CurKvart), y,m,d);
  s:=cbbAllKvart.Items.Strings[cbbAllKvart.ItemIndex];
  if Pos('�����',s) > 0 then
    begin
      dsBeg := Kvart_Beg(StrToInt(Copy(s,0,1)), y);
      dsEnd := Kvart_End(StrToInt(Copy(s,0,1)), y);
    end
  else
    begin
      dsBeg := Kvart_Beg(1, y);
      dsEnd := Kvart_End(4, y);
    end;
  dtpDBeg.Date:=dsBeg;
  dtpDEnd.Date:=dsEnd;
  SG_Stat.ClearAll;
  SG_Stat.RowCount := SG_Stat.FixedRows + 16;
  ShapkaStatistic(SG_Stat);
end;

procedure TfmStatistic.FormCreate(Sender: TObject);
var tIniF: TIniFile;

  fn,s:string;
//  e:Extended;
  sl:TStrings;
  i:integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  SL:=TStringList.Create;
  try
    tIniF.ReadSectionValues('Cena', SL);
    for i := 0 to SL.Count - 1 do
      begin
        s:=SL.ValueFromIndex[i];
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
          s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
          s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      end;

    if SL.Count = 0 then
      begin
        for i := 1 to High(CenaR) do
          tIniF.WriteFloat('Cena','cr' + IntToStr(i),CenaR[i]);
      end;

    SL.Clear;
    tIniF.ReadSectionValues('CenaImp', SL);
    for i := 0 to SL.Count - 1 do
      begin
        s:=SL.ValueFromIndex[i];
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
          s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
          s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
      end;

    if SL.Count = 0 then
      begin
        for i := 1 to High(CenaR) do
          tIniF.WriteFloat('CenaImp','cr' + IntToStr(i),CenaI[i]);
      end;


  CreateStatistik;
  xls:= TXLSFile.Create;
  finally
    tIniF.Free;
    SL.Free;
  end;
end;

procedure TfmStatistic.CreateStatistik;
var m,d,y:word;
begin
  DecodeDate(GetCurDate(CurKvart),y,m,d);
  AssignCBSL(cbbAllKvart,SLPerRegYear);
  cbbAllKvart.Items.Delete(0);
  cbbAllKvart.Items.Insert(0,IntToStr(y) + ' ���');

  cbbAllKvart.ItemIndex := 0;
  cbbAllKvartChange(cbbAllKvart);
end;
procedure TfmStatistic.FormDestroy(Sender: TObject);
begin
  xls.Destroy;
end;

procedure TfmStatistic.FormShow(Sender: TObject);
begin
  ShapkaStatistic(SG_Stat);
end;

end.
