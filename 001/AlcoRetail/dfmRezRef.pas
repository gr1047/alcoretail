unit dfmRezRef;

interface

uses
  Forms, Grids, AdvObj, BaseGrid, AdvGrid, StdCtrls, Controls, Classes,
  ExtCtrls, SysUtils;

type
  TfmRezRef = class(TForm)
    Panel2: TPanel;
    Button1: TButton;
    btOK: TButton;
    Label1: TLabel;
    Panel1: TPanel;
    SG_Filtr: TAdvStringGrid;
    procedure SG_FiltrDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure FormShow(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure SG_FiltrGetColumnFilter(Sender: TObject; Column: Integer;
      Filter: TStrings);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRezRef: TfmRezRef;

implementation

uses Global, GlobalConst, dfmMove, dfmWork, GlobalUtils;
{$R *.dfm}

procedure TfmRezRef.FormShow(Sender: TObject);
begin
  SG_Filtr.ColumnSize.Rows:=arFixed;
  SG_Filtr.Resize;
  case Tag of
    0: ShapkaSG(SG_Filtr,NameColProdRef1);
    1: ShapkaSG(SG_Filtr,NameColSalRef1);
  end;
end;

procedure TfmRezRef.Panel1Resize(Sender: TObject);
var width:integer;
    i:integer;
begin
  width:=Round((SG_Filtr.ClientWidth-SG_Filtr.ColWidths[0])/(SG_Filtr.ColCount-SG_Filtr.FixedCols));
  for i :=SG_Filtr.FixedCols  to SG_Filtr.ColCount - 1 do
    SG_Filtr.ColWidths[i]:=width;
end;

procedure TfmRezRef.SG_FiltrDblClickCell(Sender: TObject; ARow, ACol: Integer);
var V, count :Extended;
    product:TProduction;
    sal:TSaler;
    FindLic:Boolean;
begin
 if (FTypeF = fVPok) or (FTypeF = fBrak) then
  with CurSG do begin
   case Self.tag of
     0:
       begin
         //���
         Cells[6,Row]:= SG_Filtr.Cells[1,ARow];
         product.EAN13 := ShortString(SG_Filtr.Cells[1,ARow]);
         product.capacity := StrToFloat(SG_filtr.AllCells[5, aRow]);
         product.AlcVol := StrToFloat(SG_filtr.AllCells[4, aRow]);
         //������������ ���������
         product.Productname := ShortString(SG_Filtr.Cells[3,ARow]);
         Cells[7,Row] := SG_Filtr.Cells[3,ARow] + ';  '+FloatToStrF(product.alcvol, ffFixed, maxint, 1)+'%; ����� ' + FloatToStrF(product.capacity, ffFixed, maxint, 2)+'�.';
         product.ProductNameF := ShortString(Cells[7,Row]);
         //��� �������������
         Cells[12,Row] := SG_Filtr.Cells[7,ARow];
         //��� �������������
         Cells[13,Row] := SG_Filtr.Cells[8,ARow];
         //��� ��
         Cells[2,Row] := SG_Filtr.Cells[2,ARow];
         product.FNSCode := ShortString(SG_Filtr.Cells[2,ARow]);
         //�����
         Cells[1,Row] := IntToStr(FormDecl(SG_Filtr.Cells[2,ARow]));
         product.prod.pInn:=ShortString(SG_Filtr.Cells[7,ARow]);
         product.prod.pKpp:=ShortString(SG_Filtr.Cells[8,ARow]);
         GetProducer(fmWork.que2_sql, product.prod, true, False);

         Cells[11,Row] := String(product.prod.pName);
         count:=Cursg.AllFloats[8,aRow];
         V:=0.1*count*product.capacity;
         Cells[9,Row] := FloatToStrF(V, ffFixed, maxint, ZnFld);
      end;
   end;
  end;
  if (FTypeF = fPrihod) or (FTypeF = fVPost) then
  with CurSG do begin
   case Self.tag of
     0:
       begin
         //���
         Cells[9,Row]:= SG_Filtr.Cells[1,ARow];
         product.EAN13 := ShortString(SG_Filtr.Cells[1,ARow]);
         product.capacity := StrToFloat(SG_filtr.AllCells[5, aRow]);
         product.AlcVol := StrToFloat(SG_filtr.AllCells[4, aRow]);
         //������������ ���������
         product.Productname := ShortString(SG_Filtr.Cells[3,ARow]);
         Cells[10,Row] := SG_Filtr.Cells[3,ARow] + ';  '+FloatToStrF(product.alcvol, ffFixed, maxint, 1)+'%; ����� ' + FloatToStrF(product.capacity, ffFixed, maxint, 3)+'�.';
         product.ProductNameF := ShortString(Cells[10,Row]);
         //��� �������������
         Cells[15,Row] := SG_Filtr.Cells[7,ARow];
         //��� �������������
         Cells[16,Row] := SG_Filtr.Cells[8,ARow];
         //��� ��
         Cells[2,Row] := SG_Filtr.Cells[2,ARow];
         product.FNSCode := ShortString(SG_Filtr.Cells[2,ARow]);
         //�����
         Cells[1,Row] := IntToStr(FormDecl(SG_Filtr.Cells[2,ARow]));
         product.prod.pInn:=ShortString(SG_Filtr.Cells[15,ARow]);
         product.prod.pKpp:=ShortString(SG_Filtr.Cells[16,ARow]);
         GetProducer(fmWork.que2_sql, product.prod, true, False);

         Cells[14,Row] := String(product.prod.pName);
         //count:=StrToFloat(AllCells[11, aRow]);
         count:=Cursg.AllFloats[11,aRow];
         V:=0.1*count*product.capacity;
         Cells[12,Row] := FloatToStrF(V, ffFixed, maxint, ZnFld);
      end;
     1:
       begin
         sal.sInn := ShortString(SG_Filtr.Cells[2,ARow]);
         sal.sKpp := ShortString(SG_Filtr.Cells[3,ARow]);
         sal.sName := ShortString(SG_Filtr.Cells[1,ARow]);
         Cells[6,Row] := String(sal.sinn);
         Cells[7,Row] := String(sal.skpp);
         Cells[8,Row] := String(sal.sname);
         GetSaler(fmWork.que2_sql,sal, FindLic,True,False);
      end;
   end;

  end;
  if not NoValue(CurSG.AllCells[CurSG.AllColCount-1, CurSG.Row]) then
    Inc(fmMove.countEdit);
  Close;
end;

procedure TfmRezRef.SG_FiltrGetColumnFilter(Sender: TObject; Column: Integer;
  Filter: TStrings);
begin
case Self.tag of
     0: if (Column in [0,3,6]) then Filter.Clear;
     1: if (Column in [0,1,4]) then Filter.Clear;
end;
end;

end.
