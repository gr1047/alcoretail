unit dfmErrXML;

interface

uses
  Forms, StdCtrls, Classes, Controls, icmp, PingModule, IdIcmpClient;

type
  TfmErrXML = class(TForm)
    mmo_err: TMemo;
    Ch_XML: TCheckBox;
    Button1: TButton; procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IdIcmpClient1Reply(ASender: TComponent; const AReplyStatus: TReplyStatus);
  private
    { Private declarations }
    TimePing:integer;
  public
    { Public declarations }
    sal, fileName:string;
  end;

var
  fmErrXML: TfmErrXML;

implementation
uses Global,Global2;

{$R *.dfm}

procedure TfmErrXML.Button1Click(Sender: TObject);
begin
  if Ch_XML.Checked then
    SenFile(fileName, sal, SLErrXML);
  Close;
end;

procedure TfmErrXML.FormCreate(Sender: TObject);
var Reply:TsmICMP_Echo_Reply;
    cmp:TIdIcmpClient;
begin
  cmp:=TIdIcmpClient.Create(nil);
  cmp.OnReply := IdIcmpClient1Reply;
  try
    cmp.Host:='www.bmsystem.ru';
//     Self.IdIcmpClient1.TTL:=192;
    try
      Ch_XML.Checked :=True;
      cmp.Ping;
      Ping ('5.9.77.34',nil,Reply,5000);
    except
//     MessageDlg('������ �������� �����������', mtError, [mbOk], 0);
      Ch_XML.Checked :=False;
    end;
  finally
    cmp.Free;
  end;
end;

procedure TfmErrXML.IdIcmpClient1Reply(ASender: TComponent;
  const AReplyStatus: TReplyStatus);
begin
  TimePing := AReplyStatus.MsRoundTripTime;
end;

end.
