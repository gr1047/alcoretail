object fmCorrect: TfmCorrect
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'fmCorrect'
  ClientHeight = 552
  ClientWidth = 1082
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object pnl6: TPanel
    Left = 0
    Top = 121
    Width = 1082
    Height = 390
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object SG_Saler: TAdvStringGrid
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 839
      Height = 384
      Cursor = crDefault
      Align = alClient
      ColCount = 14
      Ctl3D = True
      DefaultDrawing = True
      DrawingStyle = gdsClassic
      RowCount = 3
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
      ParentCtl3D = False
      ParentShowHint = False
      ScrollBars = ssBoth
      ShowHint = True
      TabOrder = 0
      ActiveRowShow = True
      ActiveRowColor = cl3DLight
      HoverRowCells = [hcNormal, hcSelected]
      HintShowCells = True
      HintShowLargeText = True
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = []
      ActiveCellColor = clMoneyGreen
      ColumnSize.Rows = arFixed
      ColumnSize.Location = clIniFile
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.FixedDropDownButton = True
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.SizeGrip = False
      ControlLook.DropDownFooter.Buttons = <>
      EnhTextSize = True
      ExcelStyleDecimalSeparator = True
      Filter = <>
      FilterActive = True
      FilterDropDown.ColumnWidth = True
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDown.TextChecked = 'Checked'
      FilterDropDown.TextUnChecked = 'Unchecked'
      FilterDropDown.Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFF808080
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF808080FFFFFFFFFFFFFFFFFF80808080808080808080808080808080808080
        8080808080808080808080808080808080808080FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDown.GlyphActive.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF00008000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000
        FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF00800000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFF008000
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        00008000FFFFFFFFFFFFFFFFFF00800000800000800000800000800000800000
        8000008000008000008000008000008000008000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDownClear = #1054#1095#1080#1089#1090#1080#1090#1100
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedAsButtons = True
      FixedFooters = 1
      FixedColWidth = 50
      FixedRowHeight = 22
      FixedRowAlways = True
      FixedColAlways = True
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -12
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.4f'
      FloatingFooter.BorderColor = clHotLight
      FloatingFooter.CalculateHiddenRows = False
      FloatingFooter.Column = 5
      FloatingFooter.Visible = True
      Grouping.AutoSelectGroup = True
      Grouping.AutoCheckGroup = True
      HoverFixedCells = hfFixedColumns
      MouseActions.RowSelect = True
      MouseActions.RowSelectPersistent = True
      Navigation.AdvanceOnEnter = True
      Navigation.AllowClipboardRowGrow = False
      Navigation.AllowClipboardColGrow = False
      Navigation.AdvanceAuto = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Tahoma'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollColor = 14084566
      ScrollWidth = 21
      SearchFooter.AutoSearch = False
      SearchFooter.FindNextCaption = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.FindPrevCaption = #1055#1077#1088#1082#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = #1047#1072#1082#1088#1099#1090#1100
      SearchFooter.HintFindNext = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.HintFindPrev = #1055#1088#1077#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.HintHighlight = 'Highlight occurrences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SearchFooter.SearchActiveColumnOnly = True
      SearchFooter.SearchMatchStart = True
      SearchFooter.ShowHighLight = False
      SearchFooter.ShowMatchCase = False
      SelectionColor = 11075496
      SelectionColorMixer = True
      SelectionResizer = True
      SelectionRTFKeep = True
      ShowDesignHelper = False
      SizeWhileTyping.Width = True
      SortSettings.AutoSortForGrouping = False
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.IndexShow = True
      SortSettings.AutoFormat = False
      SortSettings.InitSortDirection = sdDescending
      SortSettings.FixedCols = True
      SortSettings.SortOnVirtualCells = False
      SyncGrid.SelectionRow = True
      Version = '7.2.8.0'
      WordWrap = False
      ColWidths = (
        50
        34
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20)
      RowHeights = (
        22
        22
        23)
    end
    object SG_All: TAdvStringGrid
      AlignWithMargins = True
      Left = 848
      Top = 3
      Width = 231
      Height = 384
      Cursor = crDefault
      Align = alRight
      ColCount = 14
      Ctl3D = True
      DefaultDrawing = True
      DrawingStyle = gdsClassic
      RowCount = 3
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
      ParentCtl3D = False
      ParentShowHint = False
      ScrollBars = ssBoth
      ShowHint = True
      TabOrder = 1
      ActiveRowShow = True
      ActiveRowColor = clSkyBlue
      HoverRowCells = [hcNormal, hcSelected]
      HintShowCells = True
      HintShowLargeText = True
      ActiveCellFont.Charset = DEFAULT_CHARSET
      ActiveCellFont.Color = clWindowText
      ActiveCellFont.Height = -11
      ActiveCellFont.Name = 'Tahoma'
      ActiveCellFont.Style = []
      ActiveCellColor = clMoneyGreen
      ColumnSize.Rows = arFixed
      ColumnSize.Location = clIniFile
      ControlLook.FixedGradientHoverFrom = clGray
      ControlLook.FixedGradientHoverTo = clWhite
      ControlLook.FixedGradientDownFrom = clGray
      ControlLook.FixedGradientDownTo = clSilver
      ControlLook.FixedDropDownButton = True
      ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownHeader.Font.Color = clWindowText
      ControlLook.DropDownHeader.Font.Height = -11
      ControlLook.DropDownHeader.Font.Name = 'Tahoma'
      ControlLook.DropDownHeader.Font.Style = []
      ControlLook.DropDownHeader.Visible = True
      ControlLook.DropDownHeader.Buttons = <>
      ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
      ControlLook.DropDownFooter.Font.Color = clWindowText
      ControlLook.DropDownFooter.Font.Height = -11
      ControlLook.DropDownFooter.Font.Name = 'Tahoma'
      ControlLook.DropDownFooter.Font.Style = []
      ControlLook.DropDownFooter.Visible = True
      ControlLook.DropDownFooter.SizeGrip = False
      ControlLook.DropDownFooter.Buttons = <>
      EnhTextSize = True
      ExcelStyleDecimalSeparator = True
      Filter = <>
      FilterDropDown.ColumnWidth = True
      FilterDropDown.Font.Charset = DEFAULT_CHARSET
      FilterDropDown.Font.Color = clWindowText
      FilterDropDown.Font.Height = -11
      FilterDropDown.Font.Name = 'Tahoma'
      FilterDropDown.Font.Style = []
      FilterDropDown.TextChecked = 'Checked'
      FilterDropDown.TextUnChecked = 'Unchecked'
      FilterDropDown.Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FF
        FFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF808080FFFFFFFFFFFFFFFFFFFFFFFF808080
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF808080FFFFFFFFFFFFFFFFFF80808080808080808080808080808080808080
        8080808080808080808080808080808080808080FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDown.GlyphActive.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF008000008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF00800000FF00008000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000
        FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF00008000FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000
        FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF00800000FF0000FF0000FF0000FF0000FF0000FF0000FF00008000FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00800000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FF0000FF00008000FFFFFFFFFFFFFFFFFFFFFFFF008000
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF
        00008000FFFFFFFFFFFFFFFFFF00800000800000800000800000800000800000
        8000008000008000008000008000008000008000FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      FilterDropDownClear = #1054#1095#1080#1089#1090#1080#1090#1100
      FilterEdit.TypeNames.Strings = (
        'Starts with'
        'Ends with'
        'Contains'
        'Not contains'
        'Equal'
        'Not equal'
        'Clear')
      FixedAsButtons = True
      FixedFooters = 1
      FixedColWidth = 50
      FixedRowHeight = 22
      FixedRowAlways = True
      FixedColAlways = True
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -12
      FixedFont.Name = 'Tahoma'
      FixedFont.Style = [fsBold]
      FloatFormat = '%.4f'
      FloatingFooter.BorderColor = clHotLight
      FloatingFooter.CalculateHiddenRows = False
      FloatingFooter.Column = 5
      FloatingFooter.Visible = True
      Grouping.AutoSelectGroup = True
      Grouping.AutoCheckGroup = True
      HoverFixedCells = hfFixedColumns
      MouseActions.RowSelect = True
      MouseActions.RowSelectPersistent = True
      Navigation.AdvanceOnEnter = True
      Navigation.AllowClipboardRowGrow = False
      Navigation.AllowClipboardColGrow = False
      Navigation.AdvanceAuto = True
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'Tahoma'
      PrintSettings.Font.Style = []
      PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
      PrintSettings.FixedFont.Color = clWindowText
      PrintSettings.FixedFont.Height = -11
      PrintSettings.FixedFont.Name = 'Tahoma'
      PrintSettings.FixedFont.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'Tahoma'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'Tahoma'
      PrintSettings.FooterFont.Style = []
      PrintSettings.PageNumSep = '/'
      ScrollColor = 14084566
      ScrollWidth = 21
      SearchFooter.AutoSearch = False
      SearchFooter.FindNextCaption = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.FindPrevCaption = #1055#1077#1088#1082#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.Font.Charset = DEFAULT_CHARSET
      SearchFooter.Font.Color = clWindowText
      SearchFooter.Font.Height = -11
      SearchFooter.Font.Name = 'Tahoma'
      SearchFooter.Font.Style = []
      SearchFooter.HighLightCaption = 'Highlight'
      SearchFooter.HintClose = #1047#1072#1082#1088#1099#1090#1100
      SearchFooter.HintFindNext = #1057#1083#1077#1076#1091#1102#1097#1072#1103
      SearchFooter.HintFindPrev = #1055#1088#1077#1076#1099#1076#1091#1097#1072#1103
      SearchFooter.HintHighlight = 'Highlight occurrences'
      SearchFooter.MatchCaseCaption = 'Match case'
      SearchFooter.SearchActiveColumnOnly = True
      SearchFooter.SearchMatchStart = True
      SearchFooter.ShowHighLight = False
      SearchFooter.ShowMatchCase = False
      SelectionColor = 11075496
      SelectionColorMixer = True
      SelectionResizer = True
      SelectionRTFKeep = True
      ShowDesignHelper = False
      SizeWhileTyping.Width = True
      SortSettings.AutoColumnMerge = True
      SortSettings.DefaultFormat = ssAutomatic
      SortSettings.Show = True
      SortSettings.IndexShow = True
      SortSettings.InitSortDirection = sdDescending
      SortSettings.FixedCols = True
      SortSettings.SortOnVirtualCells = False
      SyncGrid.SelectionRow = True
      Version = '7.2.8.0'
      WordWrap = False
      ColWidths = (
        50
        34
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20
        20)
      RowHeights = (
        22
        22
        23)
    end
  end
  object pnl8: TPanel
    Left = 0
    Top = 0
    Width = 1082
    Height = 121
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object pnl9: TPanel
      Left = 0
      Top = 0
      Width = 881
      Height = 121
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object pnl1: TPanel
        Left = 0
        Top = 0
        Width = 881
        Height = 33
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object pnl3: TPanel
          Left = 565
          Top = -2
          Width = 514
          Height = 35
          BevelOuter = bvNone
          TabOrder = 0
          object lb3: TLabel
            Left = 9
            Top = 11
            Width = 77
            Height = 18
            Alignment = taCenter
            Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cbbSal: TComboBox
            Left = 92
            Top = 5
            Width = 184
            Height = 24
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = cbbSalChange
          end
          object btn4: TBitBtn
            Left = 282
            Top = 2
            Width = 27
            Height = 27
            Glyph.Data = {
              06030000424D060300000000000036000000280000000F0000000F0000000100
              180000000000D0020000120B0000120B00000000000000000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFEDEFEFDFE3E1EDEDEDFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFC7D3CD31A35D00A92900
              A51B009D19279343B5C7BBFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFF8FBDA300B71D00A500008100007D00008F0000B70000AD0065A575FFFF
              FFDDE1DDD9DDD9000000FFFFFFFFFFFFC7D5CD00BF1900A100007B00007B0000
              9D09009700008D0000BD0000C30065AB7700AD2F63A779000000FFFFFFFFFFFF
              19A95300D50F00A50017B94DF7FBF9FFFFFFFFFFFF99CDA7009D0000DB0000DB
              1700ED1771AF85000000FFFFFFF5F5F500CB1900DB0000B503FFFFFFFFFFFFFF
              FFFFFFFFFFE1E5E1039D2F00EB0000FD0000F9007DB38F000000FFFFFFEDEFED
              AFD7BFB9DBC5E7F1EBFFFFFFFFFFFFFFFFFFFDFDFD00A51F00DF1100FF0000FF
              0000FF0081B593000000E1E7E300B33F399B5FADBFB1F3F3F3FFFFFFFFFFFFFF
              FFFFFFFFFFD9F3E167D58700C73300DB0000FF0787B997000000C1D9C900ED0F
              00FF4700DD2D00AB29258F43E7EFE9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD5E1
              D96FC58BCFE5D7000000C9DBCF00E10000FF0000FF0500D500009B1BE9F1EBFF
              FFFFFFFFFFFFFFFFC7CFC929954B19B34DD9E9DFFFFFFF000000CBDFD300D900
              00FF0000FF0000A700E3E7E3FFFFFFFFFFFFFFFFFFFFFFFF27A54F00E70900CF
              05D7DFD9FFFFFF000000D7DBD700BF0000B70000E70000F300009D1D93B199D9
              DFDBC9D3CB49A16500CB0500C30000B73BFDFDFDFFFFFF000000D9DFDB00B115
              CDE1D300970300E90000F90000D11100B71900C11F00D50000BF0000C300C3DF
              CDFFFFFFFFFFFF000000FDFDFDFFFFFFFFFFFFEBEFED00B11500BF0000DB0000
              DB0000CF0000BB0000B900ABEDC1FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF9BDFAD11BF4300B92103C33F63D989EDFBF1FFFFFFFFFF
              FFFFFFFFFFFFFF000000}
            TabOrder = 1
            OnClick = btn4Click
          end
        end
        object pnl4: TPanel
          Left = 0
          Top = 0
          Width = 282
          Height = 33
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object lb1: TLabel
            Left = 7
            Top = 11
            Width = 62
            Height = 18
            Caption = #1050#1074#1072#1088#1090#1072#1083':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cbbAllKvart: TComboBox
            Left = 75
            Top = 3
            Width = 203
            Height = 24
            Style = csDropDownList
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = cbbAllKvartChange
          end
        end
        object pnl5: TPanel
          Left = 282
          Top = 0
          Width = 265
          Height = 33
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object lb2: TLabel
            Left = 6
            Top = 9
            Width = 101
            Height = 18
            Caption = #1058#1077#1082#1091#1097#1077#1077' '#1050#1055#1055':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cbbKPP: TComboBox
            Left = 113
            Top = 3
            Width = 145
            Height = 24
            Style = csDropDownList
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = cbbKPPChange
          end
          object pnl7: TPanel
            Left = 40
            Top = -104
            Width = 185
            Height = 41
            Caption = 'pnl7'
            TabOrder = 1
          end
        end
      end
      object pnl2: TPanel
        Left = 0
        Top = 33
        Width = 881
        Height = 88
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GB_1: TGroupBox
          Left = 0
          Top = 25
          Width = 881
          Height = 63
          Align = alClient
          Caption = 'XML '#1092#1072#1081#1083#1099' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
          TabOrder = 0
          object lst_file: TListBox
            Left = 2
            Top = 20
            Width = 839
            Height = 41
            Align = alClient
            ItemHeight = 18
            TabOrder = 0
          end
          object pnl13: TPanel
            Left = 841
            Top = 20
            Width = 38
            Height = 41
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object btn2: TBitBtn
              Left = 3
              Top = 1
              Width = 34
              Height = 20
              Glyph.Data = {
                06030000424D060300000000000036000000280000000F0000000F0000000100
                180000000000D0020000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF9AAA9A59775A537B5557855A99B099FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF587B562CA52F10
                AA1E3ED44E548F58FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF587E5725B22A13BA2446E457559559FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF59845726B32C16
                BA2748E55956995BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF5D895C25AF2A0EB01E3FDD5057995BFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000A1B0A2718D727390747492747591764687462AA52D00
                9A0B2FCD3F4391497A9D7B789F79789D79769977A0B5A10000004F74520B8C16
                02940E04A61204AD130CB21910911912861B38B9435ECC6564D06B6AD2716BCF
                716CCC735A895E0000004B714D007C0D05A5151DC02C2FCC3E35C74335B94238
                A0423D984543AF4E4FBE5A57C2625CC06765BA6E567C590000004674491EB02B
                42D74F6EF47A86FE9284F88F66DD7049BF554DA05563A96968B36F6EB67676B8
                7C81BC865874590000008DA78E5490575C985E64A26768A26B62B36786F18F5D
                CF686AC271609863618A6364886564826569856A96A497000000FFFFFFFCFDFC
                FDFDFDFDFDFDFDFDFD5B975E8BF5946CD6766FC4755A825AFDFDFDFDFDFDFDFD
                FDFCFCFCFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C955F88EA8F6F
                D17978C07C5C825CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF5B8E5D7EDB8572C37A82BC865C805DFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF56865986CD8C84
                BD8B96C89A5A7C5CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFF809C81507E534A764C567D58879D87FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000}
              TabOrder = 0
              OnClick = btn2Click
            end
            object btnSal: TBitBtn
              Left = 3
              Top = 21
              Width = 34
              Height = 20
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3B3CCD363BDD
                3136DA3236DA3135D93035D93034D83033D72F33D62F32D62E31D52F2FD52E2E
                D3302ED12E2DD13735C81B24D62A45EF1A31ED1B30ED172CED1629ED1327EC12
                23EC101FEC0F1BEB0D17EB0C13EB0B0FEA0A0CE90908E90402CF515BF2456FFF
                264BFF294EFF2849FF2544FF223FFF1F3BFF1C36FF1A33FF172CFF1527FF1320
                FF111BFF1016FF1211EC757AF87A98FF496BFF365AFF2B4EFF2849FF2443FF20
                3EFF1C38FF1934FF1630FF142CFF1127FF0F20FF0F1EFF0E11F37278FD8AA9FF
                7493FF7291FF6787FF5678FF4A6CFF4262FF3756FF3351FF304DFF2E48FF2A45
                FF273FFF263EFF232BFC7277FF9CB7FF8DA7FF8FA9FF8FA7FF8DA7FF8CA6FF8A
                A5FF89A1FF869FFF849BFF8299FF8094FF7D92FF7D96FF5D64FF7976FF8689FF
                8485FF8485FF8485FF8485FF8485FF8284FF8283FF8283FF8082FF8083FF8180
                FF8080FF8283FF7774FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              TabOrder = 1
              OnClick = btnSalClick
            end
          end
        end
        object pnl14: TPanel
          Left = 0
          Top = 0
          Width = 881
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          Padding.Left = 100
          TabOrder = 1
          Visible = False
          object rb_delta: TRadioButton
            Left = 100
            Top = 0
            Width = 129
            Height = 25
            Align = alLeft
            Caption = #1056#1072#1089#1093#1086#1078#1076#1077#1085#1080#1077
            TabOrder = 0
            OnClick = rb_deltaClick
          end
          object rb_all: TRadioButton
            Left = 229
            Top = 0
            Width = 112
            Height = 25
            Align = alLeft
            Caption = #1042#1089#1077' '#1076#1072#1085#1085#1099#1077
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = rb_allClick
          end
        end
      end
    end
    object pnl10: TPanel
      Left = 881
      Top = 0
      Width = 201
      Height = 121
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Align = alClient
      BevelOuter = bvNone
      Padding.Left = 10
      Padding.Top = 10
      Padding.Right = 10
      Padding.Bottom = 10
      TabOrder = 1
      object btn1: TBitBtn
        Left = 10
        Top = 10
        Width = 129
        Height = 101
        Margins.Left = 10
        Margins.Top = 10
        Margins.Right = 10
        Margins.Bottom = 10
        Align = alLeft
        Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1076#1072#1085#1085#1099#1077' '#1087#1086' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1091
        Margin = 10
        TabOrder = 0
        WordWrap = True
        OnClick = btn1Click
      end
    end
  end
  object pnl11: TPanel
    Left = 0
    Top = 511
    Width = 1082
    Height = 41
    Align = alBottom
    TabOrder = 2
    Visible = False
    object pnl12: TPanel
      Left = 728
      Top = 1
      Width = 353
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      Padding.Left = 5
      Padding.Top = 5
      Padding.Right = 200
      Padding.Bottom = 5
      TabOrder = 0
      object btn3: TButton
        Left = 5
        Top = 5
        Width = 148
        Height = 29
        Align = alClient
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
        TabOrder = 0
      end
    end
  end
end
