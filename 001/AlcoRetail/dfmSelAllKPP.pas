unit dfmSelAllKPP;

interface

uses
  Forms, Menus, StdCtrls, Buttons, Classes, Controls, CheckLst, IniFiles, ShlObj;

type
  TfmSelAllKPP = class(TForm)
    chklst_KPP: TCheckListBox;
    mm1: TMainMenu;
    mniN1: TMenuItem;
    mniN2: TMenuItem;
    btn_Cancel: TBitBtn;
    btn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure mniN1Click(Sender: TObject);
    procedure mniN2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure chklst_KPPClickCheck(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelAllKPP: TfmSelAllKPP;

implementation
uses Global, GlobalUtils, GlobalConst;

{$R *.dfm}

procedure TfmSelAllKPP.chklst_KPPClickCheck(Sender: TObject);
var idx:Integer;
begin
  idx := chklst_KPP.ItemIndex;
  if idx < SLCheckKPP.Count then
  begin
    if chklst_KPP.Checked[idx] then
      SLCheckKPP[idx] :=1
    else
      SLCheckKPP[idx] :=0
  end;
end;

procedure TfmSelAllKPP.FormClose(Sender: TObject; var Action: TCloseAction);
var
  tIniF: TIniFile;
  fn, s: string;
  i:Integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    s:='';
    for i := 0 to chklst_KPP.Items.Count - 1 do
      if chklst_KPP.Checked[i] then
        s := s + '1'
      else
        s := s + '0';
    tIniF.WriteString(SectionName,'CheckKppJourn',s);
  finally
    tIniF.Free;
  end;
end;

procedure TfmSelAllKPP.FormCreate(Sender: TObject);
var i:Integer;
begin
  for i := 0 to SLKPP.Count - 1 do
  begin
    chklst_KPP.Items.Add('��� ' + SLKPP.Strings[i]);
    chklst_KPP.Checked[i]:= True;
  end;
end;

procedure TfmSelAllKPP.FormShow(Sender: TObject);
var i, idx:Integer;
begin
  if SLCheckKPP.Count <> chklst_KPP.Items.Count then
    Exit;

  for i := 0 to SLCheckKPP.Count - 1 do
  begin
    idx := SLCheckKPP[i];
    case idx of
      0: chklst_KPP.Checked[i] := False;
      1: chklst_KPP.Checked[i] := True;
    end;
  end;
end;

procedure TfmSelAllKPP.mniN1Click(Sender: TObject);
var i:Integer;
begin
  for i := 0 to chklst_KPP.Items.Count-1 do
  begin
    chklst_KPP.Checked[i]:= True;
    SLCheckKPP[i] := 1;
  end;
end;

procedure TfmSelAllKPP.mniN2Click(Sender: TObject);
var i:Integer;
begin
  for i := 0 to chklst_KPP.Items.Count-1 do
  begin
    chklst_KPP.Checked[i]:= False;
    SLCheckKPP[i] := 0;
  end;
end;

end.
