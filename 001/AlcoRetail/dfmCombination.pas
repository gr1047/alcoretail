unit dfmCombination;

interface

uses Forms, ExtCtrls, Controls, StdCtrls, Classes, Buttons, Dialogs, SysUtils,
  XMLDoc, XMLIntf;

type
  TfmCombination = class(TForm)
    btn2: TBitBtn;
    btnSal: TBitBtn;
    btn_Comb: TButton;
    btn1: TButton;
    lst_file: TListBox;
    tmr1: TTimer;
    pnl1: TPanel;
    lb1: TLabel;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure btn_CombClick(Sender: TObject);
    procedure btnSalClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCombination: TfmCombination;

implementation

uses dfmWork, Global, GlobalConst, GlobalUtils;
{$R *.dfm}

procedure TfmCombination.btn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfmCombination.btn2Click(Sender: TObject);
var
  tFile: string;
  i: integer;
begin
  fmWork.dlgOpen_Import.Options := [ofAllowMultiSelect, ofPathMustExist,
    ofFileMustExist, ofNoReadOnlyReturn, ofEnableSizing];
  fmWork.dlgOpen_Import.Title := '����� ����� ����������';
  fmWork.dlgOpen_Import.Filter := '���� XML|*.xml';
  if fmWork.dlgOpen_Import.Execute then
    for i := 0 to fmWork.dlgOpen_Import.Files.Count - 1 do
    begin
      tFile := fmWork.dlgOpen_Import.Files.Strings[i];
      if lst_file.Items.IndexOf(tFile) = -1 then
        lst_file.Items.Add(tFile);
    end;
  lb1.Caption := '����� ���������� ����� ��������� � ����� ' +
    ExtractFilePath(lst_file.Items.Strings[0]);
end;

procedure TfmCombination.btnSalClick(Sender: TObject);
begin
  lst_file.Items.Clear;
end;

procedure TfmCombination.btn_CombClick(Sender: TObject);
var
  i, N, j, k, l: integer;
  Xml: IXMLDocument;
  aXml: array of IXMLDocument;
  aNFile, anRef, anFo, anOO, anOO_all: array of IXMLNode;
  nKor, tmp, nYl, nYlp, nLics, nLic, NDoc, nOO: IXMLNode;
  fDecl, fKvart, fYear, fKor, idx, CountSalXML, CountOO: integer;
  filename, fn: string;
  SLSal, SLPr, SL: TStringList;
  prod: TProducer_Obj;
  sal: TSaler_Obj;
  s, s1, d, sn, fname, f_name: string;
  root, sprav, doc, org: IXMLNode;
  err, Kor, f11, f43, errXsd: Boolean;
  sKpp, sKpp_a:string;
  ex:Boolean;

begin
  SLSal := TStringList.Create;
  SLPr := TStringList.Create;
  fKor := -1;
  k := 0;
  try
    N := lst_file.Items.Count;
    if N = 0 then
      exit;

    SetLength(aXml, N);
    SetLength(aNFile, N);
    SetLength(anRef, N);
    SetLength(anFo, N);
//    SetLength(anOO, N);

    Xml := TXMLDocument.Create(nil);
    Xml.Active := false;
    Xml.Options := Xml.Options + [doNodeAutoIndent];

    SL := TStringList.Create;
    SL.LoadFromFile(lst_file.Items.Strings[0]);
    SL.Text := FormatXMLData(SL.Text);
    aXml[0] := TXMLDocument.Create(nil);
    aXml[0].Options := Xml.Options + [doNodeAutoIndent] - [doNodeAutoCreate,
      doAutoSave];
    aXml[0].NodeIndentStr := #9;
    aXml[0].Active := false;
    aXml[0].Xml.Assign(SL);
    aXml[0].Xml.Strings[0] := '<?xml version="1.0" encoding="windows-1251"?>';
    aXml[0].Active := True;
    aXml[0].SaveToFile(lst_file.Items.Strings[0]);
    aXml[0].LoadFromFile(lst_file.Items.Strings[0]);

    aNFile[0] := aXml[0].DocumentElement;

    s := String(aNFile[0].Attributes['��������']);
    if pos('4.3', s) > 0 then
      f43 := True
    else
      f43 := false;

    anFo[0] := aNFile[0].ChildNodes.FindNode('��������');
    if anFo[0] <> nil then
    begin
      s := String(anFo[0].Attributes['�������']);
      if pos('11', s) > 0 then
      begin
        fDecl := 11;
        f11 := True;
      end;
      if pos('12', s) > 0 then
      begin
        fDecl := 12;
        f11 := false;
      end;
      s := String(anFo[0].Attributes['�������������']);
      if pos('0', s) > 0 then
        fKvart := 4;
      if pos('3', s) > 0 then
        fKvart := 1;
      if pos('6', s) > 0 then
        fKvart := 2;
      if pos('9', s) > 0 then
        fKvart := 3;
      s := String(anFo[0].Attributes['������������']);
      fYear := StrToInt(s);
      nKor := aNFile[0].ChildNodes.FindNode('��������������');
      if nKor <> nil then
      begin
        fKor := StrToInt(String(nKor.Attributes['���������']));
        Kor := True;
      end
      else
        Kor := false;
    end;

    for i := 0 to N - 1 do
    begin
      // filename := ;
      if aXml[i] = nil then
        aXml[i] := TXMLDocument.Create(nil);
      aXml[i].LoadFromFile(lst_file.Items.Strings[i]);
      aNFile[i] := aXml[i].DocumentElement;

      s := String(aNFile[i].Attributes['��������']);
      if (pos('4.3', s) > 0) then
        if f43 = false then
        begin
          Show_Inf(
            '��������� ����� XML ������ ���� ����� ����� ���������� (4.31 ��� 4.20)'
              , fError);
          exit;
        end;

      anFo[i] := aNFile[i].ChildNodes.FindNode('��������');
      if anFo[i] <> nil then
      begin
        s := String(anFo[i].Attributes['�������']);
        if s <> IntToStr(fDecl) then
        begin
          Show_Inf(
            '��������� ����� XML ������ ����� ���� ��� ���������� (11 ��� 12)',
            fError);
          exit;
        end;

        s := String(anFo[i].Attributes['�������������']);
        if pos('0', s) > 0 then
          idx := 4;
        if pos('3', s) > 0 then
          idx := 1;
        if pos('6', s) > 0 then
          idx := 2;
        if pos('9', s) > 0 then
          idx := 3;
        if idx <> fKvart then
        begin
          Show_Inf('��������� ����� XML ������ ����� ���� ������� ����������',
            fError);
          exit;
        end;

        s := String(anFo[i].Attributes['������������']);
        if s <> IntToStr(fYear) then
        begin
          Show_Inf('��������� ����� XML ������ ����� ���� ��� ����������',
            fError);
          exit;
        end;
      end;

      anRef[i] := aNFile[i].ChildNodes.FindNode('�����������');
      if anRef[i] <> nil then
      begin
        CountSalXML := anRef[i].ChildNodes.Count;
        for j := 0 to CountSalXML - 1 do
        begin
          tmp := anRef[i].ChildNodes.Nodes[j];
          if tmp.NodeName = '����������' then
          begin
            sal := TSaler_Obj.Create;
            s := String(tmp.Attributes['��������']);
            sal.Data.sId := ShortString(s);
            s := String(tmp.Attributes['�000000000007']);
            sal.Data.sName := ShortString(s);
            nYl := tmp.ChildNodes.FindNode('��');
            if nYl <> nil then
            begin
              s := String(nYl.Attributes['�000000000009']);
              sal.Data.sInn := ShortString(s);
              s := String(nYl.Attributes['�000000000010']);
              sal.Data.sKpp := ShortString(s);
            end;
            if fDecl = 11 then
            begin
              nLics := tmp.ChildNodes.FindNode('��������');
              if nLics <> nil then
              begin
                nLic := nLics.ChildNodes.FindNode('��������');
                if nLic <> nil then
                begin
                  s := String(nLic.Attributes['����������']);
                  sal.Data.lic.lId := ShortString(s);
                  s := String(nLic.Attributes['�000000000011']);
                  idx := pos(',', s);
                  s1 := Copy(s, 0, idx - 1);
                  sal.Data.lic.ser := ShortString(s1);
                  s1 := Copy(s, idx + 1, Length(s) - idx);
                  sal.Data.lic.num := ShortString(s1);
                  s := String(nLic.Attributes['�000000000012']);
                  sal.Data.lic.dBeg := StrToDate(s);
                  s := String(nLic.Attributes['�000000000013']);
                  sal.Data.lic.dEnd := StrToDate(s);
                  s := String(nLic.Attributes['�000000000014']);
                  sal.Data.lic.RegOrg := ShortString(s);
                end;
              end;
            end;
            if SLSal.IndexOf(String(sal.Data.sId)) = -1 then
              SLSal.AddObject(String(sal.Data.sId), Pointer(sal))
            else
              sal.Free;
          end;

          if tmp.NodeName = '����������������������' then
          begin
            prod := TProducer_Obj.Create;
            s := String(tmp.Attributes['�����������']);
            prod.Data.pId := ShortString(s);
            s := String(tmp.Attributes['�000000000004']);
            prod.Data.pName := ShortString(s);
            if fDecl = 11 then
            begin
              s := String(tmp.Attributes['�000000000005']);
              prod.Data.pInn := ShortString(s);
              s := String(tmp.Attributes['�000000000006']);
              prod.Data.pKpp := ShortString(s);
            end;
            if fDecl = 12 then
            begin
              nYlp := tmp.ChildNodes.FindNode('��');
              if nYlp <> nil then
              begin
                s := String(nYlp.Attributes['�000000000005']);
                prod.Data.pInn := ShortString(s);
                s := String(nYlp.Attributes['�000000000006']);
                prod.Data.pKpp := ShortString(s);
              end;
            end;
            if SLPr.IndexOf(String(prod.Data.pId)) = -1 then
              SLPr.AddObject(String(prod.Data.pId), Pointer(prod))
            else
              prod.Free;
          end;
        end;
      end;

      if i = 0 then
      begin
        doc := aNFile[i].ChildNodes.FindNode('��������');
        if doc <> nil then
          org := doc.ChildNodes.FindNode('�����������');
      end;

      NDoc := aNFile[i].ChildNodes.FindNode('��������');
      if NDoc <> nil then
      begin
        tmp := NDoc.ChildNodes.FindNode('������������');
        if tmp <> nil then
        begin
          CountOO := NDoc.ChildNodes.Count;
          if i = 0 then
            SetLength(anOO_all, CountOO);

          for j := 0 to CountOO - 1 do
          begin
            nOO := NDoc.ChildNodes.Nodes[j];
            if nOO.NodeName = '������������' then
            begin
              if i = 0 then
                anOO_all[j] := nOO;
              s := String(nOO.Attributes['��������������']);
              if (UpperCase(s) = 'TRUE') or (UpperCase(s) = '1') then
              begin
                SetLength(anOO, k+1);
                anOO[k] := nOO;
                Inc(k);
              end;
            end;
          end;
        end;
      end;

    end;

    for i := 0 to High(anOO_all) do
      if anOO_all[i] <> nil then
      begin
        ex:=False;
        sKpp := String(anOO_all[i].Attributes['�����']);
        for j := 0 to High(anoo) do
          begin
            sKpp_a := String(anoo[j].Attributes['�����']);
            if sKpp = sKpp_a then
              ex:=True;
          end;
        if not ex then
          begin
            l:=High(anoo)+1;
            SetLength(anoo, l+1);
            anoo[l] := anOO_all[i];
          end;
      end;



    with Xml do
    begin
      Active := True;
      Version := '1.0';
      Encoding := 'windows-1251';
      root := AddChild('����');
      with root do
      begin
        SHapkaXML(root, fKvart, fYear, fKor, f11, Kor, f43);
        sprav := AddChild('�����������');
        SLPr.CustomSort(CompNumAsc);
        ProizXML(sprav, SLPr, f11, false);
        SLSal.CustomSort(CompNumAsc);
        PostXML(sprav, SLSal, f11, false);
        doc := AddChild('��������');
        doc.DOMNode.appendChild(org.DOMNode);
        for i := 0 to High(anOO) do
          if anOO[i] <> nil then
            doc.DOMNode.appendChild(anOO[i].DOMNode);
      end;
    end;

    s := createguid;
    d := DateToStr(GetCurDate(CurKvart));
    sn := fOrg.inn + '_0' + IntToStr(Period_Otch(fKvart)) + Copy
      (IntToStr(fYear), 4, 1) + '_' + Copy(d, 1, 2) + Copy(d, 4, 2) + Copy(d,
      7, 4) + '_' + Copy(s, 2, (Length(s) - 2));

    fn := ExtractFilePath(lst_file.Items.Strings[0]);
    if f11 then
    begin
      fname := fn + 'R1_' + sn + '.xml';
      f_name := fn + 'err_R1_' + sn + '.ini';
    end
    else
    begin
      fname := fn + 'R2_' + sn + '.xml';
      f_name := fn + 'err_R2_' + sn + '.ini';
    end;

    Xml.SaveToFile(fname);

    errXsd := FindErrXSD(fname, f11, f43, s);
    if not errXsd then
      s := '���������� �� ������ �������� �� �����'
    else
      s := '���������� ������ �������� �� �����';

    if FindEcp then
    begin
      if WriteXML(fname) then
        Show_Inf(s + #10#13 + '���������� ���������', fInfo)
      else
        Show_Inf(s + #10#13 + '���������� �� ���������. ������ ������������',
          fInfo);
    end
    else
    begin
      Show_Inf(s + #10#13 + '��� �� �������, ���������� �� ���������', fInfo);
    end;
    Close;
  finally
    FreeStringList(SLPr);
    FreeStringList(SLSal);
    SL.Free;
  end;
end;

procedure TfmCombination.FormShow(Sender: TObject);
begin
  lst_file.Items.Clear;
  lb1.Caption := '';
end;

procedure TfmCombination.tmr1Timer(Sender: TObject);
begin
  if lst_file.Items.Count > 0 then
    btn_Comb.Enabled := True
  else
    btn_Comb.Enabled := false;
end;

end.
