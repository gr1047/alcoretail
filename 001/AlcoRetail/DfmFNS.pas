unit dfmFNS;

interface

uses
  Forms, StdCtrls, Buttons, Controls, Classes;

type
  TfmFNS = class(TForm)
    cbbFNS: TComboBox;
    lb1: TLabel;
    btn1: TBitBtn;
    btn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFNS: TfmFNS;

implementation

uses GlobalUtils, GlobalConst;
{$R *.dfm}

procedure TfmFNS.btn1Click(Sender: TObject);
var idx:integer;
begin
  idx := cbbFNS.ItemIndex;
  if idx <> -1 then
    Tag := LongInt(cbbFNS.Items.Objects[idx]);
end;

procedure TfmFNS.FormCreate(Sender: TObject);
var SLVap:TStringList;
begin
  SLVap := TStringList.Create;
  try
    FillFullSLFNS(SLVap, FNSCode);
    cbbFNS.Items.Assign(SLVap);
  finally
    SLVap.Free;
  end;
end;

procedure TfmFNS.FormShow(Sender: TObject);
var idx:integer;
begin
  if Tag > 0 then
    begin
      idx := cbbFNS.Items.IndexOfObject(Pointer(Tag));
      if idx <> -1 then
        cbbFNS.ItemIndex := idx;
    end;
end;

end.
