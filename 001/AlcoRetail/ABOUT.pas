unit About;

interface

uses Controls, ComCtrls, StdCtrls, Graphics, Classes, ExtCtrls, Forms,
  Vcl.Imaging.jpeg;

type
  TAboutBox = class(TForm)
    img1: TImage;
    lbl_Version: TLabel;
    lPB: TLabel;
    PB: TProgressBar;
    lHelp: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

uses GlobalUtils;
{$R *.dfm}

procedure TAboutBox.FormCreate(Sender: TObject);
begin
  ScaleForm(AboutBox);
end;

end.
 
