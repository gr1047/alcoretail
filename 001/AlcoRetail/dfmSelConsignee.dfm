object fmSelConsignee: TfmSelConsignee
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1103' '#1058#1058#1053
  ClientHeight = 483
  ClientWidth = 624
  Color = 16056309
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PrintScale = poNone
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 425
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 10
    Caption = 'pnl1'
    TabOrder = 0
    object grp1: TGroupBox
      Left = 10
      Top = 10
      Width = 287
      Height = 405
      Align = alLeft
      Caption = #1057#1087#1080#1089#1086#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
      TabOrder = 0
      object lst1: TListBox
        Left = 2
        Top = 18
        Width = 283
        Height = 385
        Align = alClient
        BevelOuter = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        OnClick = lst1Click
      end
    end
    object GroupBox1: TGroupBox
      Left = 297
      Top = 10
      Width = 317
      Height = 405
      Align = alClient
      Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      TabOrder = 1
      object ListBox1: TListBox
        Left = 2
        Top = 18
        Width = 313
        Height = 231
        Align = alTop
        BevelOuter = bvNone
        BorderStyle = bsNone
        TabOrder = 0
        OnClick = ListBox1Click
      end
      object grp2: TGroupBox
        Left = 2
        Top = 249
        Width = 313
        Height = 154
        Align = alClient
        Caption = #1054#1087#1080#1089#1072#1085#1080#1077' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
        TabOrder = 1
        object mmo1: TMemo
          Left = 2
          Top = 18
          Width = 309
          Height = 134
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 0
        end
      end
    end
  end
  object btn1: TBitBtn
    Left = 521
    Top = 443
    Width = 89
    Height = 30
    Kind = bkCancel
    TabOrder = 1
  end
  object btn2: TBitBtn
    Left = 416
    Top = 443
    Width = 89
    Height = 30
    Kind = bkOK
    TabOrder = 2
    OnClick = btn2Click
  end
  object tmr1: TTimer
    Interval = 10
    OnTimer = tmr1Timer
    Left = 32
    Top = 432
  end
end
