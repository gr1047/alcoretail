unit Global2;

interface

uses Classes, GlobalConst, XLSWorkbook, ADODB, AdvGrid, XMLIntf, IdTCPClient,
  XLSFile, SysUtils, Forms, DB, XMLDoc, Controls, IniFiles, ShlObj, ExtCtrls,
  Graphics, IdSMTP, IdAttachmentFile, IdText, IdMessage, IdFTP, BaseGrid,
  IdExplicitTLSClientServerBase, IdFTPCommon, IdFTPList, Windows, FWZipWriter,
  ActiveX, Variants, Excel2000, DateUtils, IdAllFTPListParsers, Math;

procedure MoveOstKPP(kpp1, kpp2:string; dMove:TDateTime);
procedure FilterOstatkiKPP(SLFull:TStringList;kpp:string; var SLKPP:TStringList);
procedure MoveEANKPP(SLData:TStringList;kpp1,kpp2:string; d:TDateTime);
procedure GetRowDecl(var decl:TDeclaration; EAN13, amount:string; dM:TDateTime);
procedure FindSalerMove(DBQ:TADOQuery);
function CreateXML(sFile:string;DBQ:TADOQuery):Boolean;
//�������� �������� �� ������� ��������� �������
procedure GetOstAsc(DBQ:TADOQuery);
procedure UpdateOstAM(DBQ: TADOQuery; decl: TDeclaration);
function GetIdxOstDate(str:string;dOst:TDateTime; SLOst:TStringList):integer;
function GetIdxOstPK(PK:string):Integer;
procedure SetCurrentYear(NY:integer);
procedure SetCurDate(NY:integer);
function DbfIsREF(fName:string):Boolean;
function DbfIsPrihod(fName:string; Prihod:Boolean):Boolean;
function ImportDBFRef(fName:string; SGNew,SGUpd:TADVStringGrid):Boolean;
procedure GetErrColPtoduction(prod:TProduction; Cols:array of integer; var ErrCol: array of Integer);
procedure GetErrColPtoducer(prod:TProducer; Cols:array of integer; var ErrCol: array of Integer);
procedure NullProd(var prod:TProduction);
procedure UpdateServerProductionDBF(DBQ: TADOQuery;
  SLRow: TSTrings);
procedure ShapkaDBF(SG,SGNew:TADVStringGrid);
procedure SenFile(fName, sal:string; SLErr:TStringList);
procedure CreateXMLPost(SLXML:TStringList);
procedure SvPrImpXML(node: IXMLNode; SL: TStringList);
procedure InsertProductXML(node: IXMLNode;SL:TStringList);
procedure GetDataKppXML(kpp:String;SLData:TStringList; var SLFiltr:TStringList);
procedure CreateXMLPostKpp(SL:TStringList);
function CreateNewInn(pINN:string):string;
function ExtDataXML(fName:string):Boolean;
procedure LoadFileSaler(TCP: TIdTCPClient; RegHost, inn, ver: string);
function UpdateNameContr(RegHost, inn, ver: string):Boolean;
function GetPasword(RegHost,sInn:string):string;
function LoadFilesSaler(sInn,psw, path:string):Boolean;
function TypeCorOst(ost_old,ost_cur:Real):integer;
function CorrectOst(kv1,kv2, sKpp, wap, pInn, pKpp,ostCur,ostOld:string; SG1,SG2:TAdvStringGrid):Boolean;
function FillDataOst(kv1, kv2, sKpp,wap, pInn,pKpp:string;SG1, SG2:TAdvStringGrid):Boolean;
procedure FilterCorOst(sKpp,wap, pInn,pKpp:string; SL:TStringList;  var SLF:TStringList);
procedure WriteSGCorOst(SL:TStringList;SG:TAdvStringGrid; tKv:integer);
function DeleteAllProdCorOst(dbqD, dbqB:TADOQuery;kv1, kv2:string;SG:TAdvStringGrid):Boolean;
function InsertProdCorOst(dbqD,dbqB:TADOQuery;kv:string; SG:TAdvStringGrid; sKpp, wap, pInn, pKpp:string; ost_cur,ost_old:Real):Boolean;
function DeleteProdCorOst(dbqD, dbqB:TADOQuery;kv1,kv2, sKpp:string;SG:TAdvStringGrid; ost_cur,ost_old:Real):Boolean;
function GetSalerWap(sKpp, wap:string; dB, dE:TDateTime):TSaler;
procedure FindEan(dbq:TADOQuery;wap, pInn, pKpp:string; var prod:TProduction);
procedure UpdateRashProd(dbqD, dbqB:TADOQuery; EAN13, sKpp:string; ost:Real; dE1, dE2:TDateTime);
procedure CleanRepProducer(DBQ:TADOQuery);
procedure ChangePKProducer(DBQ:TADOQuery; PK:string);
function ConnectServer(SG: TADVStringGrid; RegHost, inn, ver: string): Boolean;
function ExistFilesSalerFTP(pathFTP, pasw, Inn:string):Integer;
procedure AddLocalProduction(Str: array of AnsiString);
procedure UpdateLocalProduction(Str: array of AnsiString);
procedure AddLocalProducer(Str: array of AnsiString);
procedure UpdateLocalProducer(Str: array of AnsiString);
procedure UpdateLocalSalerLic(Str: array of AnsiString);
procedure UpdateLocalSaler(Str: array of AnsiString);
procedure AddLocalSalerLic(Str: array of AnsiString);
procedure AddLocalSaler(Str: array of AnsiString);
function WriteDeclBM(sfile, Inn:string; iKvart, iYear:integer):Boolean;
function ZipFileNew(fn: string): Boolean;
function LoadFilesFromFTP(path_disk, adr_ftp, sUser, sPasw:string; DateMod:TDateTime):Boolean;
function MoveEanKppCount(DBQ:TADOQuery; KPP1, KPP2:string;
  Kolvo:Extended; sal:TSaler; prod:TProduction):Boolean;
procedure Shapka_SGJournal(SG:TAdvStringGrid);
procedure RashodSmallOst(DBQ:TADOQuery);
procedure GetFNSOst(Sg:TAdvStringGrid);
procedure CreateInventTable(DBQ:TADOQuery);
function CheckErrOst(SKpp, EAN13:String):integer;
procedure SaveProcOst(Ost, form:Integer);
procedure FillFNSOst;
procedure CreateJournalSG(jBeg, jEnd:TDateTime;SG:TAdvStringGrid;KPP:string);
procedure WriteJournalPrihod(SG:TAdvStringGrid; Row:Integer; decl:TDeclaration);
procedure WriteJournalRashod(SG:TAdvStringGrid; Row:Integer; decl:TDeclaration);
procedure WriteCheckKpp;
procedure ImportJournalKPP(sKvart, sYear:Integer;Path:string; SG:TAdvStringGrid; Kpp:string);
procedure SaveXLSJournal(xls:TXLSFile;fPath:string; SG:TAdvStringGrid);
procedure CreateRashFromOst(SG:TAdvStringGrid;SLOst:TStringList);
function ShowDataOst(SG: TADVStringGrid; NoRes, CalcCol, HideCol: array of integer): integer;
procedure ShowOst(SG:TAdvStringGrid;SL:TStringList; NameCol,NoRes, CalcCol,
  HideCol: array of integer);
function ExistFilesDeclFTP(pathFTP, pasw, Inn:string):Integer;
function LoadFilesDecl(sInn,psw, path:string; iKvart, iYear:integer):integer;
procedure GetMinusOst(var sErr:array of integer; SLOst:TStringList);
procedure ShowRefProducer(SG: TADVStringGrid; DBQ: TADOQuery);
procedure CorrectSmallOst(DBQ:TADOQuery);
procedure SetColorSertSet(leDE, leINN:TLabeledEdit);
procedure SetColorSertMain(leDE, leINN, leMain:TLabeledEdit);
function SaveShOst(var fName:string):Boolean;
function LoadShOst(var fName:string; SG:TAdvStringGrid; var cOst:integer):Boolean;
procedure SaveDBFRef(fN:string; SG:TAdvStringGrid);
procedure SaveXLSRef(fN:string; SG:TAdvStringGrid);
procedure UpdateSimvDB(DBQ:TADOQuery);





implementation
uses Global,GlobalUtils, dfmWork, dfmMain, dfmMove, dfmProgress, About,
  dfmOstatki, dfmOborot, dfmSaler, dfmStatistic, dfmCorrect, dfmDecl, DbfTable;

procedure MoveOstKPP(kpp1, kpp2:string; dMove:TDateTime);
var SLKpp:TStringList;
begin
  SLKPP:=TStringList.Create;
  try
    DvizenieKppEan(SLOborotKpp,SLAllDecl,dEnd, dEnd);
    //������������ ������� ��������
    FilterOstatki(SLOstatkiKpp,SLOborotKpp, masErr);
    //������������ ������� �������� ��� ���������� ���
    FilterOstatkiKPP(SLOstatkiKpp,kpp1,SLKPP);
    //������� ��������� ��� � ���1 �� ���2
    if SLKPP.Count > 0 then
      MoveEANKPP(SLKpp,kpp1,kpp2, dMove)
    else
      Show_Inf('�� ��� ' + kpp1 + ' ��� ��������' , fError);
  finally
    SLKpp.Free;
  end;
end;

procedure FilterOstatkiKPP(SLFull:TStringList;kpp:string; var SLKPP:TStringList);
var i:integer;
  sKpp,sEAN:string;
  mov:TMove;
begin
  SLKpp.Clear;
  for i := 0 to SLFull.Count - 1 do
  begin
    sKpp := Copy(SLFull.Strings[i],0,9);
    if sKpp = kpp then
    begin
      sEAN := Copy(SLFull.Strings[i],10,13);

      if SLFull.Objects[i] is TMove then
      begin
        mov := TMove(SLFull.Objects[i]);
        SLKPP.Add(sEAN+'='+FloatToStr(mov.dv.ost1));
      end;
    end;
  end;
end;

procedure MoveEANKPP(SLData:TStringList;kpp1,kpp2:string; d:TDateTime);
var i:integer;
  decl:TDeclaration;
begin
  try
    //�������� ������������� ���������� ��� �������� ������
    FindSalerMove(fmWork.que2_sql);
    if fmProgress = nil then
    fmProgress := TfmProgress.Create(Application);
    fmProgress.Show;
    fmProgress.lblcap.Caption := '����������� ������� �������� � ��� ' + kpp1 + #10#13 + ' �� ��� ' + kpp2;
    fmProgress.pb.Max := SLData.Count;
    fmProgress.pb.Step := 1;
    fmProgress.pb.Position := 0;

    for i := 0 to SLData.Count - 1 do
    begin
      Application.ProcessMessages;
      fmProgress.pb.StepIt;

      NullDecl(decl);
      //������������ ������ ����������
      decl.TypeDecl :=2;
      GetRowDecl(decl, SLData.Names[i], String(SLData.Values[SLData.Names[i]]), d);
      decl.SelfKpp := ShortString(kpp1);
      decl.DeclNum := '�������� c ���';
      SaveRowNewDeclDB(fmWork.que1_sql, decl, true);
      if decl.PK <> '' then SaveRowDeclNewLocal(decl);

      NullDecl(decl);
      //������������ ������ ����������
      decl.TypeDecl := 1;
      GetRowDecl(decl, SLData.Names[i], String(SLData.Values[SLData.Names[i]]), d);
      decl.SelfKpp := ShortString(kpp2);
      decl.DeclNum := '����������� �� ���';
      SaveRowNewDeclDB(fmWork.que1_sql, decl, true);
      if decl.PK <> '' then SaveRowDeclNewLocal(decl);
    end;
    if SLData.Count>0 then
    begin
      fmMain.cbAllKvartChange(Application);
      Show_Inf('������� �������� � ��� ' + kpp1 + ' �� ��� ' + kpp2 + ' �������� �������', fInfo);
    end
    else
      Show_Inf('�� ��� ' + kpp1 + ' ��� ��������'
              , fError);
    fmProgress.Close;
  except
    Show_Inf('������ �������� ������', fError);
  end;
end;

procedure GetRowDecl(var decl:TDeclaration; EAN13, amount:string; dM:TDateTime);
var FindProduser:Boolean;
begin
  decl.DeclDate := dM;
  decl.RepDate := dM;
  decl.product.EAN13:=ShortString(EAN13);
  decl.Amount := StrToFloat(amount);
  if decl.TypeDecl = 1 then
  begin
    decl.sal.sInn := '1234567809';
    decl.sal.sKpp := '123400002';
    decl.sal.sName :='������� ������ � ���';
  end;
  GetProduction(fmWork.que2_sql, decl.product, FindProduser, True);
  decl.vol := 0.1 * decl.Amount * decl.product.capacity;
end;

procedure FindSalerMove(DBQ:TADOQuery);
var qr:TADOQuery;
  StrSQL, PK:string;
begin
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  StrSQL := 'SELECT * FROM SALER WHERE ORGINN = ''1234567809'' AND ORGKPP = ''123400002''' ;
  try
    try
      qr.SQL.Text := StrSQL;
      qr.Open;
      if qr.Eof then
      begin
        qr.sql.Text := 'select MAX(PK) from Saler';
        qr.Open;
        PK := IntToStr(qr.Fields[0].AsInteger + 1);
        qr.sql.Clear;
        qr.ParamCheck := False;
        qr.Parameters.Clear;

        StrSQL :=
          'INSERT INTO Saler(PK,OrgName,OrgINN,OrgKPP,RegionCode,Address,LocRef,SelfMade) ';
        StrSQL := StrSQL +
          'VALUES (:PK,:OrgName,:OrgINN,:OrgKPP,:RegionCode,:Address,:LocRef,:SelfMade)';
        qr.sql.Text := StrSQL;

        qr.Parameters.AddParameter.Name := 'PK';
        qr.Parameters.ParamByName('PK').DataType := ftString;
        qr.Parameters.ParamByName('PK').value := PK;

        qr.Parameters.AddParameter.Name := 'OrgName';
        qr.Parameters.ParamByName('OrgName').DataType := ftString;
        qr.Parameters.ParamByName('OrgName').value := '������� ������ � ���';

        qr.Parameters.AddParameter.Name := 'OrgINN';
        qr.Parameters.ParamByName('OrgINN').DataType := ftString;
        qr.Parameters.ParamByName('OrgINN').value := '1234567809';

        qr.Parameters.AddParameter.Name := 'OrgKPP';
        qr.Parameters.ParamByName('OrgKPP').DataType := ftString;
        qr.Parameters.ParamByName('OrgKPP').value := '123400002';

        qr.Parameters.AddParameter.Name := 'RegionCode';
        qr.Parameters.ParamByName('RegionCode').DataType := ftString;
        qr.Parameters.ParamByName('RegionCode').value := '59';

        qr.Parameters.AddParameter.Name := 'Address';
        qr.Parameters.ParamByName('Address').DataType := ftString;
        qr.Parameters.ParamByName('Address').value := '������';

        qr.Parameters.AddParameter.Name := 'LocRef';
        qr.Parameters.ParamByName('LocRef').DataType := ftBoolean;
        qr.Parameters.ParamByName('LocRef').value := True;

        qr.Parameters.AddParameter.Name := 'SelfMade';
        qr.Parameters.ParamByName('SelfMade').DataType := ftBoolean;
        qr.Parameters.ParamByName('SelfMade').value := False;

        qr.ExecSQL;

        qr.sql.Text := 'select MAX(PK) from Saler_license';
        qr.Open;
        PK := IntToStr(qr.Fields[0].AsInteger + 1);
        qr.sql.Clear;
        qr.sql.Text :=
          'INSERT INTO Saler_license(PK,saler_pk, ser, num, date_start, date_finish,regorg,saler_kpp)' +
          'Values(''' + PK + ''',''1234567809'', ''���'', ''���'', ''01.01.2010'', ''01.01.2016'', '+
          '''59'',''123400002'')';
        qr.ExecSQL;

      end;
    except

    end;
  finally
    qr.Free;
  end;
end;

function CreateXML(sFile:string;DBQ:TADOQuery):Boolean;
var SLXml, SLKontr, SLRow, SLProiz, SLEan:TStringList;
  Xml: IXMLDocument;
  NFile, nDoc, nOrg, nRekv, NRef, nKontr, nRez, nUl, nFl,
    nOOb, nOb, nImp, nPol, nPost: IXMLNode;
  sInn, sKpp, sName, idKontr, NameKontr, InnKontr, KppKontr, s,
    GrFNS, idPr, idPol, DNacl, Amount:string;
  i,j,k,l,n,idx:integer;

  producer: TProducer;
  production:TProduction;
  sal:TSaler;

  decl: TDeclaration;
  f1:Boolean;
  V:Extended;


begin
  SLXml:=TStringList.Create;
  SLKontr:=TStringList.Create;
  SLProiz:=TStringList.Create;
  SLEan := TStringList.Create;
  Xml := TXMLDocument.Create(nil);
  Xml.Active := False;
  Xml.LoadFromFile(sFile);
  try
    NFile := Xml.DocumentElement; // �������� ��� ����
    NDoc := NFile.ChildNodes.FindNode('��������');
    if nDoc <> nil then
    begin
      //��������� ��� ����������
      nOrg := nDoc.ChildNodes.FindNode('�����������');
      if nOrg <> nil then
      begin
        nRekv := nOrg.ChildNodes.FindNode('���������');
        if nRekv <> nil then
        begin
          sInn := String(nRekv.Attributes['�����']);
          sName := String(nRekv.Attributes['������']);
        end;
      end;
    end;

    //����� � ����� ���������� ����������� � ��� �����������
    NRef := NFile.ChildNodes.FindNode('�����������');
    if nRef <> nil then
    begin
      nKontr := nRef.ChildNodes.FindNode('�����������');
      if nKontr <> nil then
        for i := 0 to NRef.ChildNodes.Count -1 do
        begin
          nKontr := nRef.ChildNodes.Nodes[i];
          if nKontr.NodeName = '�����������' then
          begin
            idKontr := String(nKontr.Attributes['�������']);
            NameKontr := String(nKontr.Attributes['�000000000007']);
            nRez := nKontr.ChildNodes.FindNode('��������');
            if nRez <> nil then
            begin
              nUl := nRez.ChildNodes.FindNode('��');
              if nUl <> nil then
              begin
                InnKontr := String(nUl.Attributes['�000000000009']);
                KppKontr := String(nUl.Attributes['�000000000010']);
                if InnKontr = fOrg.Inn then
                  SLKontr.Add(idKontr+'='+KppKontr);
              end;
              nFl := nRez.ChildNodes.FindNode('��');
              if nFl <> nil then
              begin
                InnKontr := String(nFl.Attributes['�000000000009']);
                KppKontr := Copy(InnKontr,0,4)+'01001';
                if InnKontr = fOrg.Inn then
                  SLKontr.Add(idKontr+'='+KppKontr);
              end;
            end;
          end;

          //�������� ���������� ��������������
          if nKontr.NodeName = '����������������������' then
          begin
            SLRow := TStringList.Create;
            producer.pName := ShortString(String(nKontr.Attributes['�000000000004']));
            SLRow.Add(String(producer.pName));

            producer.pINN := ShortString(String(nKontr.Attributes['�000000000005']));
            SLRow.Add(String(producer.pINN));
            idx := Length(producer.pINN);
            case idx of
              0: begin
                producer.pKPP := NullKPP7;
                producer.pInn := NULLINN7;
              end;
              8,9:
                try
//                  producer.pINN := '0' + producer.pINN;
                  producer.pInn := ShortString(CreateNewInn(String(producer.pInn)));
                  producer.pKPP := ShortString(String(nKontr.Attributes['�000000000006']));
                except
                  case idx of
                    8: producer.pKPP := NullKPP8;
                    9: producer.pKPP := NullKPP9;
                  end;

                end;
              10: producer.pKPP := ShortString(String(nKontr.Attributes['�000000000006']));
            end;
            SLRow.Add(String(producer.pKPP));

            s := String(nKontr.Attributes['�����������']);
            SLProiz.AddObject(s, SLRow);
          end;
        end;
    end;

    if SLKontr.Count = 0 then
    begin
      Show_inf('� ���������� ���������� ��� �������� ��� ����������� � ��� ' + fOrg.Inn, fError);
      exit;
    end;

    if nDoc <> nil then
    begin
      nOOb := NDoc.ChildNodes.FindNode('������������');
      if nOOb <> nil then
        for i := 0 to NDoc.ChildNodes.Count - 1 do
        begin
          nOOb := NDoc.ChildNodes.Nodes[i];
          if nOOb.NodeName = '������������' then
          begin
            sKpp := String(nOOb.Attributes['�����']);
            NullSaler(Sal);
            sal.sInn := ShortString(sInn);
            sal.sKpp := ShortString(sKpp);
            sal.sName := ShortString(sName);
            s := String(nOOb.Attributes['���������������']);
            if UpperCase(s) = 'TRUE' then
            begin
              nOb := NOOb.ChildNodes.FindNode('������');
              if nOb <> nil then
                for j := 0 to nOOb.ChildNodes.Count - 1 do
                begin
                  nOb := nOOb.ChildNodes.Nodes[j];
                  if nOb.NodeName = '������' then
                  begin
                    GrFNS := String(nOb.Attributes['�000000000003']);
                    nImp := nOb.ChildNodes.FindNode('����������������');
                    if nImp <> nil then
                      for k := 0 to nOb.ChildNodes.Count - 1 do
                      begin
                        nImp := nOb.ChildNodes.Nodes[k];
                        idPr := String(nImp.Attributes['�����������']);
                        nPol := nImp.ChildNodes.FindNode('����������');
                        if nPol <> nil then
                          for n := 0 to nImp.ChildNodes.Count-1 do
                          begin
                            nPol := nImp.ChildNodes.Nodes[n];
                            idPol := String(nPol.Attributes['������������']);
                            idx := SLKontr.IndexOfName(idPol);
                            if idx <> -1 then
                            begin
                              decl.SelfKpp := ShortString(SLKontr.ValueFromIndex[idx]);
                              idx := SLProiz.IndexOf(idPr);
                              if idx <> -1 then
                              begin
                                SLRow := Pointer(SLProiz.Objects[idx]);
                                producer.pName := ShortString(SLRow.Strings[0]);
                                producer.pINN := ShortString(SLRow.Strings[1]);
                                producer.pKPP := ShortString(SLRow.Strings[2]);
                                f1 := GetProducer(fmWork.que2_sql, producer,
                                  True, False);
                                if not f1 then
                                begin
                                  producer.pName := ShortString(SLRow.Strings[0]);
                                  producer.pAddress := '������';
                                end;
                              end;
                              nPost := nPol.ChildNodes.FindNode('��������');
                              if nPost <> nil then
                                for l := 0 to nPol.ChildNodes.Count - 1 do
                                begin
                                  nPost := nPol.ChildNodes.Nodes[l];
                                  Result := True;
//                                  nPost := nPol.ChildNodes.Nodes[m];
                                  DNacl := String(nPost.Attributes['�000000000018']);
                                  s := Copy(DNacl, 1, 2) + '.' + Copy(DNacl,
                                    4, 2) + '.' + Copy(DNacl, 7, 4);
                                  decl.DeclDate := StrToDateTime(s);

                                  decl.RepDate := dEnd;
                                  decl.TypeDecl := 1;
                                  decl.DeclNum := ShortString(String(nPost.Attributes['�000000000019']));
                                  decl.TM := ShortString(String(nPost.Attributes['�000000000020']));

                                  Amount := String(nPost.Attributes['�000000000021']);
                                  if not ValidFloat(Amount, V) then V := 0;
                                  decl.Amount := V * 10;
                                  decl.vol := V;

                                  production.FNSCode := ShortString(GrFNS);

                                  production.Productname := ShortString('��������� ������ ' + GrFNS);
                                  production.AlcVol :=1;
                                  production.Capacity := 1;
                                  production.prod := producer;

                                  decl.product := production;
                                  decl.sal := sal;
                                  decl.forma := FormDecl(String(decl.product.FNSCode));
                                  WriteFullDataXML(SLEan, DBQ, decl);
                                  SLXml.AddObject(String(decl.product.EAN13),TDecl_obj.Create);
                                  TDecl_obj(SLXml.Objects[SLXml.Count - 1]).data := decl;
                                end;
                            end;
                          end;
                      end;
                  end;
                end;
            end;
          end;
        end;
    end;
    if SLXml.Count > 0 then
      CreateXMLPostKpp(SLXml);
  finally
    SLEan.free;
    FreeStringList(SLProiz);
    SLKontr.Free;
    Xml := nil;
    FreeStringList(SLXml);
  end;
end;

//�������� �������� �� ������� ��������� �������
procedure GetOstAsc(DBQ:TADOQuery);
var qr:TADOQuery;
  ss,StrSQL, PK, Ean, Kpp, s:string;
  ost:TOstOldAcz;
  Amount:Extended;
  i, idx:integer;
  decl:TDeclaration;
  d,dbegY,dendY:TDateTime;
//  SLDataOst: TStringList;
begin
  FreeStringList(SLDataOst);
  SLDataOst := TStringList.Create;
  qr := TADOQuery.Create(DBQ.Owner);
  qr.Connection := DBQ.Connection;
  qr.ParamCheck := False;
  qr.sql.Clear;

  StrSQL := 'SELECT * FROM DECLARATION WHERE DeclType = ''8'' and ReportDate >=:dB and ReportDate <= :dE';
  qr.SQL.Text := StrSQL;

  dbegY := StrToDate('01.01.' + IntToStr(NowYear) + '.');
  dendY := StrToDate('31.12.' + IntToStr(NowYear) + '.');

  qr.Parameters.AddParameter.Name := 'dB';
  qr.Parameters.ParamByName('dB').DataType := ftDate;
  qr.Parameters.ParamByName('dB').value := dbegY;

  qr.Parameters.AddParameter.Name := 'dE';
  qr.Parameters.ParamByName('dE').DataType := ftDate;
  qr.Parameters.ParamByName('dE').value := dendY;
  try
    try
      qr.Open;
      if not qr.Eof then
        while not qr.Eof do
        begin
          Kpp := qr.FieldByName('SelfKPP').AsString;
          EAN := qr.FieldByName('EAN13').AsString;
          ss := qr.FieldByName('Amount').AsString;
          Amount := StrInFloat(ss);
          PK := qr.FieldByName('PK').AsString;
          d:=qr.FieldByName('DeclDate').AsDateTime;
          if Amount <> 0 then
          begin
            ost.Value := Amount;
            ost.PK :=ShortString(pk);
            ost.DateOst :=d;
            SLDataOst.AddObject(kpp+Ean, TOst_obj.Create);
            TOst_obj(SLDataOst.Objects[SLDataOst.Count - 1]).data := ost;
          end;
          qr.Next;
        end;
      if SLDataOst.count > 0 then
        for i := 0 to SLAllDecl.Count - 1 do
        begin
          decl := TDecl_obj(SLAllDecl.Objects[i]).data;
          if (decl.TypeDecl = 2) then
          begin
            s := String(decl.SelfKpp + decl.product.EAN13);
            idx := GetIdxOstDate(s,decl.DeclDate, SLDataOst);
            if idx <> -1 then
              begin
                decl.Ost.Value := TOst_obj(SLDataOst.Objects[idx]).data.Value;
                decl.Ost.PK := TOst_obj(SLDataOst.Objects[idx]).data.PK;
                decl.Ost.PK_EAN := decl.pk;
                decl.Ost.DateOst := TOst_obj(SLDataOst.Objects[idx]).data.DateOst;
              end
            else
              begin
                decl.ost.Value := 0;
                decl.Ost.PK := '';
                decl.Ost.PK_EAN := '';
                decl.Ost.DateOst := 0;
              end;
          end
          else
          begin
            decl.Ost.Value := 0;
            decl.Ost.PK := '';
            decl.Ost.PK_EAN := '';
            decl.Ost.DateOst := 0;
          end;
          TDecl_obj(SLAllDecl.Objects[i]).data := decl;
        end;
    except

    end;
  finally
    qr.Free;
  end;
end;

procedure UpdateOstAM(DBQ: TADOQuery; decl: TDeclaration);
var ost:TOstOldAcz;
  idx : Integer;
  pk:string;
begin
  SortSLCol(1,SLAllDecl);
  pk:=String(decl.pk);
  if decl.Ost.Value <> 0 then
  begin
    decl.TypeDecl := 8;
    decl.pk := decl.Ost.PK;
    if decl.Ost.PK = '' then
      begin
        SaveRowNewDeclDB(fmWork.que1_sql, decl, false);
        ost.PK := decl.pk;
        ost.Value := decl.ost.Value;
        ost.DateOst := decl.ost.DateOSt;
        SLDataOst.AddObject(String(decl.SelfKpp+decl.product.EAN13), TOst_obj.Create);
        TOst_obj(SLDataOst.Objects[SLDataOst.Count - 1]).data := ost;
        idx := SLAllDecl.IndexOf(pk);
        if idx <> -1 then
          TDecl_obj(SLAllDecl.Objects[idx]).data.Ost := ost;
      end
    else
      begin
        SaveRowUpdDB(DBQ,decl, false);
        idx := GetIdxOstDate(String(decl.SelfKpp+decl.product.EAN13),dEnd, SLDataOst);
        if idx <> -1 then
          TOst_obj(SLDataOst.Objects[idx]).data.Value := decl.Ost.Value;
//        idx := SLAllDecl.IndexOf(pk);
//        if idx <> -1 then
//          TDecl_obj(SLAllDecl.Objects[idx]).data.Ost := ost;
      end;
  end
  else
  begin
    idx := GetIdxOstDate(String(decl.SelfKpp+decl.product.EAN13),dEnd, SLDataOst);
    if idx <> -1 then
    begin
      DelRowDB(String(decl.Ost.PK), fmWork.que1_sql);
      SLDataOst.Objects[idx].free;
      SLDataOst.Delete(idx);

      ost.PK := '';
      ost.Value := decl.ost.Value;
      ost.DateOst := decl.DeclDate;
      idx := SLAllDecl.IndexOf(pk);
      if idx <> -1 then
        TDecl_obj(SLAllDecl.Objects[idx]).data.Ost := ost;
    end;
  end;

//var fExist:Boolean;
//  sql: TADOQuery;
//  pk:string;
//  s:string;
//begin
//  fExist := False;
//  sql := TADOQuery.Create(DBQ.Owner);
//  try
//    try
//
//      sql.Connection := DBQ.Connection;
//      sql.ParamCheck := False;
//      sql.sql.Clear;
//      sql.Parameters.Clear;
//      s:=DateToStr(dEnd);
//      sql.SQL.Text :=
//        'select PK from declaration where DeclType = ''8'' and Selfkpp = :SelfKpp ' +
//        'and EAN13 = :EAN13 and DeclDate = :DeclDate';
//
//      sql.Parameters.AddParameter.Name := 'SelfKPP';
//      sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
//      sql.Parameters.ParamByName('SelfKPP').value := decl.SelfKPP;
//
//      sql.Parameters.AddParameter.Name := 'EAN13';
//      sql.Parameters.ParamByName('EAN13').DataType := ftString;
//      sql.Parameters.ParamByName('EAN13').value := decl.product.EAN13;
//
//      sql.Parameters.AddParameter.Name := 'DeclDate';
//      sql.Parameters.ParamByName('DeclDate').DataType := ftDateTime;
//      sql.Parameters.ParamByName('DeclDate').value := decl.DeclDate;
//      sql.Open;
//
//      if not sql.Eof then
//      begin
//        fExist := True;
//        pk:=sql.FieldByName('pk').AsString;
//      end;
//    except
//    end;
//  finally
//    sql.Free;
//  end;
//  decl.TypeDecl := 8;
//  decl.pk := pk;
//  if fExist then
//  begin
//    if decl.Ost.Value > 0 then
//      SaveRowUpdDB(DBQ,decl, false)
//    else
//      DelRowDB(PK, fmWork.que1_sql)
//  end
//  else
//    if decl.Ost.Value > 0 then
//      SaveRowNewDeclDB(fmWork.que1_sql, decl, false);
end;

function GetIdxOstDate(str:string;dOst:TDateTime; SLOst:TStringList):integer;
var i:Integer;
  ost:TOstOldAcz;
begin
  Result := -1;
  for i := 0 to SLOst.Count - 1 do
    if SLOst.Strings[i] = str then
    begin
      ost:=TOst_obj(SLOst.Objects[i]).data;
      if ost.DateOst = dOst then
      begin
        Result := i;
        Exit;
      end;
    end;
end;

function GetIdxOstPK(PK:string):Integer;
var i:Integer;
  ost:TOstOldAcz;
begin
  Result := -1;
  for i := 0 to SLDataOst.Count - 1 do
  begin
    ost := TOst_Obj(SLDataOst.Objects[i]).data;
    if String(ost.PK) = PK then
    begin
      Result := i;
      Exit;
    end;
  end;
end;

procedure SetCurrentYear(NY:integer);
var butSel, idx:integer;
  s:string;
begin
  butSel := Show_Inf('������� ��� ����� �������.' + #10#13 + '����������?',
      fConfirm);
  if butSel = mrOk then
  begin
    NowYear := fmMain.seYear.Value;
//    CurYear := NowYear;
    SetCurDate(NowYear);
    // ���������� ������� ����������� ��� ����������� ���������
    SetMasKvartal;
    AssignCBSL(fmMain.cbAllKvart, SLPerRegYear);
    if fmMove <> nil then
      AssignCBSL(fmMove.cbAllKvart, SLPerRegYear);


    s := IntToStr(CurKvart) + ' ' + nkv + ' ' + IntToStr(NowYear) + ' ' + ng;
    idx :=fmMain.cbAllKvart.Items.IndexOf(s);
    if fmProgress = nil then
      fmProgress:= TfmProgress.Create(Application);

    if fmOstatki <> nil then
      fmOstatki.CreateOstatki;
    if fmOborot <> nil then
      fmOborot.CreateOborot;
    if fmSaler <> nil then
      fmSaler.CreateSaler;
    if fmStatistic <> nil then
      fmStatistic.CreateStatistik;
    if fmCorrect <> nil then
      fmCorrect.CreateCorrect;
    if fmDecl <> nil then
      fmDecl.CreateDecl;

    fmWork.Enabled := False;
    fmProgress.Show;
    try
      try
        if idx <> -1 then
          fmMain.cbAllKvart.ItemIndex := idx
        else
          fmMain.cbAllKvart.ItemIndex := fmMain.cbAllKvart.Items.Count - 1;
        if fmMove <> nil then
          fmMove.cbAllKvart.ItemIndex := fmMain.cbAllKvart.ItemIndex;



        UpdateDateRef;
        ClearSL(SLSalerDecl);
        ClearSL(SLAllDecl);
        ClearSL(SLOborotKpp);
        SLOstatkiKpp.Clear;
        ClearSL(DelDecl);
        SLDataPrih.Clear;
        SLDataRash.Clear;
        SLDataVPost.Clear;
        SLDataVPok.Clear;
        SLDataBrak.Clear;
        SLDataMoving.Clear;
        ClearSL(SLDataOst);
//          ClearSL();
//          ClearSL();

        // �������� ������ ����������
        GetAllDataDecl(fmWork.que1_sql,fmWork.que2_sql,fmProgress.PB, SLAllDecl,SLDataOst, fmProgress.lblcap);
        // �������� �������� �� ������� ��������� �������
        GetOstAsc(fmWork.que1_sql);
        fmMain.cbAllKvartChange(fmMain.cbAllKvart);
      except
        fmWork.Enabled := True;
      end;
    finally
      fmProgress.Close;
      fmWork.Enabled := True;
    end;
  end
  else
    fmMain.seYear.Value:=NowYear;
end;

procedure SetCurDate(NY:integer);
var y,m,d:word;
  tIniF : TIniFile;
  fn:string;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    DecodeDate(Now(), y, m, d);
    y := tIniF.ReadInteger(SectionName, 'Year', 1);
    if y = 1 then
      DecodeDate(Now(), y, m, d);

    if y = ny then
    begin
      if tIniF.ValueExists(SectionName, 'Kvartal') then
        CurKvart := tIniF.ReadInteger(SectionName, 'Kvartal', 1)
      else
        CurKvart := Kvartal(m);
    end
    else
    begin
//      CurKvart := 4;
      CurKvart := Kvartal(m);
    end;
    dBeg := Kvart_Beg(CurKvart, CurYear);
    dEnd := Kvart_End(CurKvart, CurYear);
  finally
    tIniF.Free;
  end;
end;

function DbfIsREF(fName:string):Boolean;
var i, idx:integer;
  dbf: TDbfTable;
begin
  Result:=True;
  dbf := TDbfTable.Create(fName, dcpComma, ltTrueFalse, dfDDMMYYYY);
  try
  for i := 0 to High(DBFRefOb) do
  begin
    idx := dbf.GetFieldNumber(DBFRefOb[i]);
    if idx = 0 then
    begin
      Result := False;
      exit;
    end;
  end;
  finally
    dbf.Free;
  end;
end;

function ImportDBFRef(fName:string; SGNew,SGUpd:TADVStringGrid):Boolean;
var dbf: TDbfTable;
  idx,count_r,i,j,StartRow:integer;
  s, StrSQL:string;
  prod, prodB:TProduction;
  SLEanDBF, SLNew,SLDelta,SLUpd:TStringList;
  qr:TADOQuery;
  ArrayCol,ArrayColErr:array of Integer;
begin
  dbf := TDbfTable.Create(fName, dcpComma, ltTrueFalse, dfDDMMYYYY);
  SLEanDBF:=TStringList.Create;
  qr:=TADOQuery.Create(fmWork);
  SLUpd:=TStringList.Create;
  SLNew:=TStringList.Create;
  SLDelta:=TStringList.Create;
  SGNew.ClearAll;
  SGUpd.ClearAll;
  ShapkaDBF(SGUpd, SGNew);

  try
    count_r := dbf.RecordsCount;
    if fmProgress = nil then
      fmProgress:= TfmProgress.Create(Application);

    fmProgress.Show;
    fmProgress.pb.Position := 0;
    fmProgress.lblcap.Caption := '������ ������ DBF ... ';
    fmProgress.pb.UpdateControlState;
    Application.ProcessMessages;
    fmProgress.labAbout.Visible :=True;
    fmProgress.pb.Min := 0;
    fmProgress.pb.Max := count_r;
    fmProgress.pb.Step := 1;
    fmProgress.pb.Position:=0;

    for i := 1 to count_r do
    begin
      fmProgress.pb.StepIt;
      Application.ProcessMessages;

      idx:=dbf.GetFieldNumber('EAN13');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.EAN13 := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('PRODNAME');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.Productname := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('ALCVOL');
      if idx <> 0 then
      begin
        s:= dbf.GetField(i, idx).ValueCharacter;
        if s = '' then s:= '0';
        if pos('-',s) > 0 then
          s := Copy(s, 0, Pos('-', s) - 1);
        if pos('%',s) > 0 then
          s := Copy(s, 0, Pos('%', s) - 1);

        prod.AlcVol := StrInFloat(s);
      end;

      idx:=dbf.GetFieldNumber('CAPACITY');
      if idx <> 0 then
        begin
          s:= dbf.GetField(i, idx).ValueCharacter;
          if s = '' then s:= '0';
          if pos('�',s) > 0 then
            s := Copy(s, 0, Pos('�', s) - 1);
          prod.Capacity := StrInFloat(s);
        end;

      idx:=dbf.GetFieldNumber('MANUFNAME');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.prod.pName := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('MANUFINN');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.prod.pInn := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('MANUFKPP');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.prod.pKpp := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('MANUFADDR');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        if Trim(s) = '' then
        s := '������';
        prod.prod.pAddress := ShortString(s);
      end;

      idx:=dbf.GetFieldNumber('VAP');
      if idx <> 0 then
      begin
        s:='';
        s:= dbf.GetField(i, idx).ValueCharacter;
        prod.FNSCode := ShortString(s);
      end;

      prod.ProductNameF := ShortString(FullNameProduction(String(prod.Productname),
       prod.AlcVol, prod.capacity));

      if SLEanDBF.IndexOf(String(prod.EAN13)) = - 1 then
      begin
        SLEanDBF.AddObject(String(prod.EAN13), TProduction_Obj .Create);
        TProduction_Obj(SLEanDBF.Objects[SLEanDBF.Count - 1]).data := prod;
      end;
    end;

    fmProgress.pb.Max := SLEanDBF.Count;
    fmProgress.pb.Position:=0;
    fmProgress.lblcap.Caption := '���������� ������ ��� 1 ... ';
    qr.Connection := dfmWork.fmWork.con2ADOCon;

    for i := 0 to SLEanDBF.Count - 1 do
    begin
      fmProgress.pb.StepIt;
      Application.ProcessMessages;
      prod:=TProduction_Obj(SLEanDBF.Objects[i]).data;

      StrSQL := 'SELECT distinct Production.EAN13, Production.FNSCode, Production.Productname, Production.AlcVol, Production.Capacity, Producer.FullName, Producer.INN, Producer.KPP ' +
                'FROM Production INNER JOIN Producer ON  (Production.prodcod = Producer.KPP) AND (Production.prodkod = Producer.INN) ' +
                'where EAN13 = :EAN13';
      qr.sql.Clear;
      qr.Parameters.Clear;
      qr.ParamCheck := False;
      qr.SQL.Text := StrSQL;
      qr.Parameters.AddParameter.Name := 'EAN13';
      qr.Parameters.ParamByName('EAN13').DataType := ftString;
      qr.Parameters.ParamByName('EAN13').Value := prod.EAN13;
      qr.Open;
      case qr.RecordCount of
        1:
        begin
          prodB.EAN13 := ShortString(qr.FieldByName('EAN13').AsString);
          prodB.FNSCode := ShortString(qr.FieldByName('FNSCode').AsString);
          prodB.Productname := ShortString(qr.FieldByName('ProductName').AsString);
          prodB.AlcVol := qr.FieldByName('AlcVol').AsFloat;
          prodB.Capacity := qr.FieldByName('Capacity').AsFloat;
          prodB.prod.pINN := ShortString(qr.FieldByName('INN').AsString);
          prodB.prod.pKPP := ShortString(qr.FieldByName('KPP').AsString);
          prodB.prod.pName := ShortString(qr.FieldByName('FullName').AsString);
          if (FloatToStr(prodB.Capacity) <> FloatToStr(prod.Capacity)) or
            (prodB.FNSCode <> prod.FNSCode) then
          begin
            SLUpd.AddObject(String(prodB.EAN13), TProduction_Obj .Create);
            TProduction_Obj(SLUpd.Objects[SLUpd.Count - 1]).data := prodB;
            SLDelta.AddObject(String(prodB.EAN13), TProduction_Obj .Create);
            TProduction_Obj(SLDelta.Objects[SLDelta.Count - 1]).data := prod;

//            if FloatToStr(prodB.Capacity) <> FloatToStr(prod.Capacity) then
//            begin
//              SLUpd.AddObject(String(prodB.EAN13), TProduction_Obj .Create);
//              TProduction_Obj(SLUpd.Objects[SLUpd.Count - 1]).data := prodB;
//
//              SLCap.Add(FloatToStr(prod.Capacity));
//            end;
//            if prodB.FNSCode <> prod.FNSCode then
//            begin
//
//              TProduction_Obj(SLUpd.Objects[SLUpd.Count - 1]).data := prodB;
//              SLWap.Add(prod.FNSCode);
//            end;
          end;
        end;
        0:
        begin
          SLNew.AddObject(String(prod.EAN13), TProduction_Obj .Create);
          TProduction_Obj(SLNew.Objects[SLNew.Count - 1]).data := prod;
        end;
      end;
    end;

    fmProgress.pb.Max := SLNew.Count+SLUpd.Count;
    fmProgress.pb.Position:=0;
    fmProgress.lblcap.Caption := '���������� ������ ��� 2... ';
    SGNew.RowCount := SLNew.Count + SGNew.FixedRows;
    StartRow:=SGNew.FixedRows;
    for i := 0 to SLNew.Count - 1 do
    begin
      fmProgress.pb.StepIt;
      Application.ProcessMessages;
      prod:=TProduction_Obj(SLNew.Objects[i]).data;
      if TProduction_Obj(SLNew.Objects[i]) <> nil then
      begin
        SGNew.RowColor[i+StartRow]:=clNone;
//        SGNew.RowFontColor[i+StartRow] := clNormal;
        SGNew.Cells[1,i+StartRow]:=String(prod.EAN13);
        SGNew.Cells[2,i+StartRow]:=String(prod.Productname);
        SGNew.Cells[3,i+StartRow]:=String(FloatToStr(prod.Capacity));
        SGNew.Cells[4,i+StartRow]:=String(prod.FNSCode);
        SGNew.Cells[5,i+StartRow]:=String(FloatToStr(prod.AlcVol));
        SGNew.Cells[6,i+StartRow]:=String(prod.prod.pName);
        SGNew.Cells[7,i+StartRow]:=String(prod.prod.pInn);
        SGNew.Cells[8,i+StartRow]:=String(prod.prod.pKpp);
        SGNew.Cells[9,i+StartRow]:=String(prod.prod.pAddress);
      end;
    end;

    SGUpd.RowCount := SLUpd.Count + SGUpd.FixedRows;
    StartRow:=SGUpd.FixedRows;
    for i := 0 to SLUpd.Count - 1 do
    begin
      fmProgress.pb.StepIt;
      Application.ProcessMessages;
      prod:=TProduction_Obj(SLUpd.Objects[i]).data;
      if TProduction_Obj(SLUpd.Objects[i]) <> nil then
      begin
        SGUpd.Cells[1,i+StartRow]:=string(prod.EAN13);
        SGUpd.Cells[2,i+StartRow]:=string(prod.Productname);
        SGUpd.Cells[3,i+StartRow]:=FloatToStr(prod.Capacity);

        SGUpd.Cells[4,i+StartRow]:=FloatToStr(TProduction_Obj(SLDelta.Objects[i]).data.Capacity);
        SGUpd.Cells[5,i+StartRow]:=string(prod.FNSCode);
        SGUpd.Cells[6,i+StartRow]:=string(TProduction_Obj(SLDelta.Objects[i]).data.FNSCode);
        SGUpd.Cells[7,i+StartRow]:=FloatToStr(prod.AlcVol);
        SGUpd.Cells[8,i+StartRow]:=string(prod.prod.pInn);
        SGUpd.Cells[9,i+StartRow]:=string(prod.prod.pKpp);
        SGUpd.Cells[10,i+StartRow]:=string(prod.prod.pName);
      end;
    end;

    SGNew.Invalidate;

    for i := SGNew.FixedRows to SGNew.RowCount - 1 do
    begin
      prod.EAN13 := ShortString(SGNew.Cells[1,i]);
      prod.Productname :=ShortString(SGNew.Cells[2,i]);
      prod.Capacity := StrToFloat(SGNew.Cells[3,i]);
      prod.FNSCode := ShortString(SGNew.Cells[4,i]);
      prod.AlcVol := StrToFloat(SGNew.Cells[5,i]);
      prod.prod.pName := ShortString(SGNew.Cells[6,i]);
      prod.prod.pInn := ShortString(SGNew.Cells[7,i]);
      prod.prod.pKpp := ShortString(SGNew.Cells[8,i]);
      prod.prod.pAddress := ShortString(SGNew.Cells[9,i]);
      SetLength(ArrayColErr,2);
      SetLength(ArrayCol,2);
      ArrayCol[0]:=1; //������� EAN
      ArrayCol[1]:=2; //������� ������������ ���������
      GetErrColPtoduction(prod,ArrayCol,ArrayColErr);
      for j := 0 to High(ArrayColErr) do
        if ArrayColErr[j] > 0  then
          SetColorCells(SGNew, [ArrayCol[j]], i, 1);

      SetLength(ArrayColErr,4);
      SetLength(ArrayCol,4);
      ArrayCol[0]:=6; //������� ������������ �������������
      ArrayCol[1]:=7; //������� ��� �������������
      ArrayCol[2]:=8; //������� ��� �������������
      ArrayCol[3]:=9; //������� ����� �������������
      GetErrColPtoducer(prod.prod,ArrayCol,ArrayColErr);
      for j := 0 to High(ArrayColErr) do
        if ArrayColErr[j] > 0  then
          SetColorCells(SGNew, [ArrayCol[j]], i, 1);

      if prod.Capacity = 0 then
        SetColorCells(SGNew, [3], i, 1);

      if prod.FNSCode = '' then
        SetColorCells(SGNew, [4], i, 1);

      if prod.AlcVol = 0 then
        SetColorCells(SGNew, [5], i, 1);
    end;
    fmProgress.Close;
  finally
    dbf.Free;
    FreeStringList(SLUpd);
    FreeStringList(SLNew);
    FreeStringList(SLEanDBF);
    qr.free;
    SLDelta.Free;
  end;
end;

procedure GetErrColPtoduction(prod:TProduction; Cols:array of integer; var ErrCol: array of Integer);
var b1:Boolean;
  i:integer;
begin
  for i := 0 to High(ErrCol) do
    ErrCol[i]:=0;

  if not EANCheck(String(prod.EAN13), b1) then
    ErrCol[0]:=1;

  if (pos(NoProd, String(prod.Productname)) > 0) or (prod.Productname = '')  then
    ErrCol[1]:=1;
end;

procedure GetErrColPtoducer(prod:TProducer; Cols:array of integer; var ErrCol: array of Integer);
var b1:Boolean;
  i:integer;
begin
  for i := 0 to High(ErrCol) do
    ErrCol[i]:=0;

  if (prod.pName = NoProducer) or (prod.pName = '') then
    ErrCol[0]:=1;

  If (not InnCheck(String(prod.pINN), b1)) and
    (not INN12Check(String(prod.pINN), b1)) then
    ErrCol[1]:=1;

  if (not StrCheck(String(prod.pKPP))) or (Length(String(prod.pKPP)) <> 9)
    or (prod.pKPP = '000000000') then
    ErrCol[2]:=1;

  if prod.pAddress = '' then
    ErrCol[3]:=1;
end;

procedure NullProd(var prod:TProduction);
begin
  prod.EAN13:='';
  prod.FNSCode:='';
  prod.Productname:='';
  prod.ProductNameF:='';
  prod.AlcVol:=0;
  prod.Capacity:=0;
  prod.prod.pId:='';
  prod.prod.pInn:='';
  prod.prod.pKpp:='';
  prod.prod.pName:='';
  prod.prod.pAddress:='';
end;

procedure UpdateServerProductionDBF(DBQ: TADOQuery; SLRow: TSTrings);
var
  butsel, countRec: integer;
  s, cont, EAN13: string;
  recount, k,i: integer;
  qr2: TADOQuery;
  ms, ms1: TMemoryStream;
  er: Boolean;
  FSize: Int64;
  Str: array [1 .. 15] of AnsiString;
  TCP: TIdTCPClient;
begin

  TCP := TIdTCPClient.Create(nil);
  TCP.Host:=BMHost;
  TCP.Port:=TCPPort;
  try
    qr2 := TADOQuery.Create(Application);
    qr2.Connection := fmWork.con2ADOCon;
    ms := TMemoryStream.Create;
    ms1 := TMemoryStream.Create;
    countRec:=0;
    try
      try
        s := '����� ��������� ' + IntToStr(SLRow.Count) + ' ������� c ����������' + #10#13;
        butsel := Show_Inf(s + '����������?', fConfirm);
        if butsel <> mrOk then
          exit;
        if not TCP.Connected then
        begin
          TCP.Host := BMHost;
          TCP.Connect;
          TCP.Port:=TCPPort;
        end;

        if TCP.Connected then
          s := TCP.IOHandler.ReadLn;
        if Copy(s, 1, 20) = 'Connected to server.' then
          MemoAdd('C�����: �� ���������� � �������');
        TCP.IOHandler.WriteLn('INN ' + fOrg.inn + ' ' + exe_version);
        if TCP.Connected then
          s := TCP.IOHandler.ReadLn;
        if SameText(String(s), 'RFEX') then
          TCP.IOHandler.WriteLn('OK');

        if fmProgress = nil then
          fmProgress:= TfmProgress.Create(Application);
        fmWork.Enabled := False;
        fmProgress.Show;
        fmProgress.pb.Position := 0;
        fmProgress.lblcap.Caption := '����������� ���������� ��������� ... ';
        fmProgress.pb.UpdateControlState;
        Application.ProcessMessages;
        fmProgress.labAbout.Visible :=True;
        fmProgress.pb.Min := 0;
        fmProgress.pb.Step := 1;
        fmProgress.pb.Max := SLRow.Count-1;
        fmProgress.pb.Position := 0;

        for i := 0 to SLRow.Count - 1 do
        begin
          EAN13 := SLRow.Strings[i];
          TCP.IOHandler.WriteLn('EAN' + EAN13);
          cont := TCP.IOHandler.ReadLn;
          Application.ProcessMessages;
          fmProgress.pb.StepIt;
          for recount := 1 to StrToInt(cont) do
          begin
            s := TCP.IOHandler.ReadLn;
            if Copy(s, 1, 2) = 'OK' then
            begin
              FSize := StrToInt(Copy(String(s), 3, Length(String(s)) - 2));
              try
                er := False;
                ms.Clear;
                ms1.Clear;
                TCP.IOHandler.ReadStream(ms, FSize);
                if not TCP.Connected then
                  er := True;
                ms.Position := 0;
                if not UnZipFilePas(ms, ms1, psw_loc) then
                  er := True;
                if er then
                begin
                  MemoEdit('������ ������������');
                  MemoAdd('����� ����������� ������� - ������ ������������');
                end;
                if not er and TCP.Connected then
                begin
                  er := False;
                  ms1.Position := 0;
                  SetLength(Str[1], 13);
                  Str[1] := AnsiString(ReadBuf(ms1, 13));
                  SetLength(Str[2], 3);
                  Str[2] := AnsiString(ReadBuf(ms1, 3));
                  SetLength(Str[3], 180);
                  Str[3] := AnsiString(ReadBuf(ms1, 180));
                  SetLength(Str[4], 8);
                  Str[4] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[5], 8);
                  Str[5] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[6], 12);
                  Str[6] := AnsiString(ReadBuf(ms1, 12));
                  SetLength(Str[7], 9);
                  Str[7] := AnsiString(ReadBuf(ms1, 9));
                  SetLength(Str[8], 180);
                  Str[8] := AnsiString(ReadBuf(ms1, 180));
                  SetLength(Str[9], 8);
                  Str[9] := AnsiString(ReadBuf(ms1, 8));
                  SetLength(Str[10], 12);
                  Str[10] := AnsiString(ReadBuf(ms1, 12));
                  SetLength(Str[11], 9);
                  Str[11] := AnsiString(ReadBuf(ms1, 9));
                  SetLength(Str[12], 3);
                  Str[12] := AnsiString(ReadBuf(ms1, 3));
                  SetLength(Str[13], 2);
                  Str[13] := AnsiString(ReadBuf(ms1, 2));
                  SetLength(Str[14], 180);
                  Str[14] := AnsiString(ReadBuf(ms1, 180));
                  for k := 1 to 14 do
                  begin
                    Str[k] := AnsiString(TrimRight(String(Str[k])));
                  end;
                  try
                    s := String(Str[9]);
                    PrepareText(Str[3]);
                    qr2.sql.Text :=
                      'Select EAN13 From Production where EAN13=''' + String
                      (Str[1]) + '''';
                    qr2.Open;
                    if qr2.RecordCount > 0 then
                    begin
                      // ���������� ��������� � ��������� ����������� (� ����)
                      if qr2.RecordCount > 1 then
                      begin
                        qr2.sql.Text :=
                          'DELETE from Production WHERE EAN13=''' + String(Str[1]) + '''';
                        qr2.ExecSQL;
                        qr2.sql.Text :=
                          'INSERT INTO Production(EAN13, FNSCode, ' +
                          'Productname, AlcVol, Capacity, ProducerCode, ' +
                          'Prodkod, Prodcod)' + #13#10 + 'Values(''' + String
                          (Str[1]) + ''', ''' + String(Str[2])
                          + ''', ''' + String(Str[3]) + ''', ''' + String
                          (Str[4]) + ''', ''' + String(Str[5])
                          + ''', ''' + String(Str[9]) + ''', ''' + String
                          (Str[6]) + ''', ''' + String(Str[7]) + ''')';
                        qr2.ExecSQL;
                      end
                      else
                      begin
                        if qr2.RecordCount > 0 then
                          qr2.sql.Text := 'UPDATE Production ' + #13#10 +
                            'Set FNSCode=''' + String(Str[2])
                            + ''', Productname=''' + String(Str[3])
                            + ''', AlcVol=''' + String(Str[4])
                            + ''', Capacity=''' + String(Str[5])
                            + ''', ProducerCode=''' + String(Str[9])
                            + ''', Prodkod=''' + String(Str[6])
                            + ''', Prodcod=''' + String(Str[7])
                            + '''' + #13#10 + 'WHERE EAN13=''' + String
                            (Str[1]) + '''';
                        qr2.ExecSQL;
                        // ���������� ���������� ����������� � ����������
                      end;
                      if StrCheck(String(Str[9])) and not er then
                        try
                          Str[9] := AnsiString(TrimRight(String(s)));
                          PrepareText(Str[8]);
                          PrepareText(Str[14]);
                          qr2.sql.Text :=
                            'Select * From Producer where inn=''' + String(Str[6]) + ''' and kpp=''' + String(Str[7]) + '''';
                          qr2.Open;
                          if qr2.RecordCount > 0 then
                            qr2.sql.Text :=
                              'UPDATE Producer ' + #13#10 + 'Set pk=''' +
                              String(Str[9]) + ''', FullName=''' + String
                              (Str[8]) + ''', INN=''' + String(Str[10])
                              + ''', KPP=''' + String(Str[11])
                              + ''', countrycode=''' + String(Str[12])
                              + ''', RegionCode=''' + String(Str[13])
                              + ''', Address=''' + String(Str[14])
                              + '''' + #13#10 + 'WHERE inn=''' + String
                              (Str[6]) + ''' and kpp=''' + String(Str[7])
                              + ''''
                          else
                            qr2.sql.Text :=
                              'INSERT INTO Producer(PK, FullName, INN, KPP, ' +
                              'countrycode, RegionCode, Address)' + #13#10 +
                              'Values(' + String(Str[9]) + ', ''' + String
                              (Str[8]) + ''', ''' + String(Str[10])
                              + ''', ''' + String(Str[11])
                              + ''', ''' + String(Str[12]) + ''', ''' + String
                              (Str[13]) + ''', ''' + String(Str[14]) + ''')';
                          qr2.ExecSQL;
                        except
                          er := True;
                        end;

                      UpdateMasProduction(Str, False);
                      Inc(countRec);
                    end
                    except
                      er := True;
                    end;
                end;
              finally

              end;

            end;
            if Copy(s, 1, 4) = 'NFND' then
            begin

            end;
          end;
        end;
        fmMove.cbAllKvartChange(fmMove.cbAllKvart);
        s := '������� ��������� ' + IntToStr(countRec) + ' ������� c ����������';
        Show_Inf(s, fInfo);
      except
        fmWork.Enabled := True;
        fmProgress.labAbout.Visible :=False;
        fmProgress.Close;
      end;
    finally
      qr2.free;
      ms1.free;
      ms.free;
      TCP.Socket.Close;
      TCP.Disconnect;
      TCP.Free;
    end;
  finally
//    SLRow.Free;
    fmWork.Enabled := True;
    fmProgress.labAbout.Visible :=False;
    fmProgress.Close;
  end;
end;

procedure ShapkaDBF(SG,SGNew:TADVStringGrid);
begin
  SG.ClearAll;
  SGNew.ClearAll;

  SG.RowCount := 3;
  SG.FixedRows := 2;
  SG.ColCount := 11;
  SGNew.RowCount := 2;
  SGNew.FixedRows := 1;

  SG.ColumnHeaders.Add('�');
  SG.MergeCells(0,0,1,2);
  SG.Alignments[0,0]:=taCenter;

  SG.ColumnHeaders.Add('EAN13');
  SG.MergeCells(1,0,1,2);
  SG.Alignments[1,0]:=taCenter;

  SG.ColumnHeaders.Add('������������');
  SG.MergeCells(2,0,1,2);
  SG.Alignments[2,0]:=taCenter;

  SG.ColumnHeaders.Add('�������');
  SG.MergeCells(3,0,1,2);
  SG.Alignments[3,0]:=taCenter;

  SG.ColumnHeaders.Add('������� DBF');
  SG.MergeCells(4,0,1,2);
  SG.Alignments[4,0]:=taCenter;

  SG.ColumnHeaders.Add('��� ��');
  SG.MergeCells(5,0,1,2);
  SG.Alignments[5,0]:=taCenter;

  SG.ColumnHeaders.Add('��� �� DBF');
  SG.MergeCells(6,0,1,2);
  SG.Alignments[6,0]:=taCenter;

  SG.ColumnHeaders.Add('% ������');
  SG.MergeCells(7,0,1,2);
  SG.Alignments[7,0]:=taCenter;

  SG.ColumnHeaders.Add('�������������');
  SG.MergeCells(8,0,3,1);
  SG.Alignments[8,0]:=taCenter;

  SG.Cells[8,1]:='���';
  SG.Alignments[8,1]:=taCenter;
  SG.Cells[9,1]:='���';
  SG.Alignments[8,1]:=taCenter;
  SG.Cells[10,1]:='������������';
  SG.Alignments[9,1]:=taCenter;

  SGNew.ColumnHeaders.Add('�');
  SGNew.ColumnHeaders.Add('EAN13');
  SGNew.ColumnHeaders.Add('������������');
  SGNew.ColumnHeaders.Add('�������');
  SGNew.ColumnHeaders.Add('��� ��');
  SGNew.ColumnHeaders.Add('% ������');
  SGNew.ColumnHeaders.Add('�������������');
  SGNew.ColumnHeaders.Add('���');
  SGNew.ColumnHeaders.Add('���');
  SGNew.ColumnHeaders.Add('������');
  SGNew.ColumnHeaders.Add('������');
  SGNew.ColumnHeaders.Add('�����');
  SGNew.ColumnHeaders.Add('����� �����������');
end;

function DbfIsPrihod(fName:string; Prihod:Boolean):Boolean;
var i, idx:integer;
  dbf: TDbfTable;
begin
  Result:=False;
  dbf := TDbfTable.Create(fName, dcpComma, ltTrueFalse, dfDDMMYYYY);
  try
  for i := 0 to High(DBFRefOb) do
  begin
    if Prihod then
      idx := dbf.GetFieldNumber(DBFPrihodOb[i])
    else
      idx := dbf.GetFieldNumber(DBFRashodOb[i]);
    if idx = 0 then
    begin
      Result := True;
      exit;
    end;
  end;
  finally
    dbf.Free;
  end;
end;

procedure SenFile(fName, sal:string; SLErr:TStringList);
var
  att: TIdAttachmentFile;
  txt : TIdText;
  msg:TIdMessage;
  SMTP:TIdSMTP;
begin
  msg:=TIdMessage.Create(nil);
  SMTP:=TIdSMTP.Create(nil);
  try
    SMTP.Host := 'smtp.yandex.ru';// IP ��������� �������� smtp.mail.ru
    SMTP.Port := 25;// 25
    SMTP.Username := 'bm.klients@yandex.ru'; // ����� ������������ ��� ������� ����� ������ (Login)
    SMTP.Password := 'Bm-Sytema20!!$';  // ������ ������������ ��� ������� ����� ������
    SMTP.UseTLS   := utNoTLSSupport;
    SMTP.AuthType := satDefault;
    SMTP.ConnectTimeout := 10000;

    msg.Clear;
    msg.CharSet := 'windows-1251';
    msg.From.Address := 'bm.klients@yandex.ru';
    msg.From.Name := fOrg.Inn + ' ''' + fOrg.SelfName + '''';
    msg.ReplyTo.Clear;
    msg.ReplyTo.EMailAddresses := 'bm.klients@yandex.ru';
    msg.Recipients.EMailAddresses := 'bm.klients@yandex.ru';

    msg.ReceiptRecipient.Text := '';

    msg.Subject := 'Err_XML ' + fOrg.Inn;
    msg.CharSet := 'windows-1251';
    msg.ContentType  := 'multipart/mixed';
    msg.Encoding := meMime;

    txt := TIdText.Create(msg.MessageParts, nil);
    txt.ContentType  := 'multipart/alternative';
    txt.ParentPart := -1;

    txt := TIdText.Create(msg.MessageParts, nil);
    txt.ContentType:='text/plain';
    txt.CharSet := 'windows-1251';
    txt.Body.Assign(SLErr);
    txt.Body.Text := '''' + fOrg.SelfName + ''' ��� ' +
      fOrg.Inn + #10#13 + ' ��������� ' + sal + #10#13 +
      txt.Body.Text;
    txt.ParentPart := 0;
    // ���������� Attach (�� �������� ����� ���������)
    if FileExists(fName) then
      ATT := TIdAttachmentFile.Create(msg.MessageParts, fName);
    with ATT do
    begin
      ContentType:='Content-Type: text/plain; charset="UTF-8"';
    end;
    try
      SMTP.Connect;
    except
    end;
    try
      SMTP.Send(msg);
    finally
      SMTP.Disconnect;
      FreeAndNil(ATT);
      txt.Free;
    end;
  finally
    msg.Free;
    smtp.Free;
  end;
end;

procedure CreateXMLPost(SLXML:TStringList);
var root, doc, oborot: IXMLNode;
  Xml: IXMLDocument;
  i:integer;
  decl:TDeclaration;
  SLGroup, SLXMLGroup:TStringList;
  group, fn:string;
begin
  Xml := TXMLDocument.Create(nil);
  Xml.Active := false;
  Xml.Options := Xml.Options + [doNodeAutoIndent];
  SLGroup := TStringList.Create;
  SLXMLGroup := TStringList.Create;
  try
    with Xml do
    begin
      Active := True;
      Version := '1.0';
      Encoding := 'windows-1251';
      root := AddChild('����');
      with root do
      begin
        Attributes['�������'] := DateToStr(Date());
        Attributes['��������'] := '4.31';
        Attributes['��������'] := '���������� ������ ' + exe_version;
        doc := AddChild('��������');

        // ����������� ���� �� ������������ � ���
        for i := 0 to SLXML.Count - 1 do
        begin
          NullDecl(decl);
          decl := TDecl_obj(SLXML.Objects[i]).Data;
          if TDecl_obj(SLXML.Objects[i]) <> nil then
            if SLGroup.IndexOf(String(decl.product.FNSCode)) = -1 then
              SLGroup.Add(String(decl.product.FNSCode));
        end;

        // ���������� ����� ����� �� ���������� (�������� ������ ��� �����)
        SLGroup.CustomSort(CompNumAsc);

        for i := 0 to SLGroup.Count - 1 do
        with doc do
        begin
          oborot := AddChild('������');
          with oborot do
          begin
            group := SLGroup.Strings[i];
            Attributes['�N'] := IntToStr(i + 1);
            Attributes['�000000000003'] := group;
            // ���������� ����� ������� ���������� ��� ������ ������ ��
            GetDataGroupXML(group, SLXML, SLXMLGroup);
            SvPrImpXML(oborot,SLXMLGroup);
          end;
        end;
      end;
    end;
    fn:='_'+String(decl.SelfKpp);
    Xml.SaveToFile('c:\1.xml');
  finally
    XML:=nil;
    SLGroup.Free;
    FreeStringList(SLXMLGroup);
  end;
end;

procedure SvPrImpXML(node: IXMLNode; SL: TStringList);
var SLPrImp, SLXMLGroupPrImp:TStringList;
  i, idx:integer;
  decl:TDeclaration;
  prod:TProducer;
  idp:string;
  SvedPrImp : IXMLNode;
begin
  SLPrImp:=TStringList.Create;
  SLXMLGroupPrImp := TStringList.Create;
  try
    try
      SLPrImp := TStringList.Create;
      SLXMLGroupPrImp := TStringList.Create;

      SLPrImp.clear;
      SLPrImp.Duplicates := dupIgnore;

      // ���������� ������ �������������� ��� ������ ������ ��
      for i := 0 to SL.Count - 1 do
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        if TDecl_obj(SL.Objects[i]) <> nil then
          if TDecl_obj(SL.Objects[i]) is TDecl_obj then
          begin
            prod:=decl.product.prod;
            idx := SLPrImp.IndexOf(String(prod.pId));
            if idx = -1 then
            begin
              SLPrImp.AddObject(String(decl.product.prod.pId), TProducer_Obj.Create);
              TProducer_Obj(SLPrImp.Objects[SLPrImp.Count-1]).Data := prod;
            end;
          end;
      end;

      // ��������� ���� �� ���� ������������
      SLPrImp.CustomSort(CompNumAsc);

      for i := 0 to SLPrImp.Count - 1 do
        with node do
        begin
          prod := TProducer_Obj(SLPrImp.Objects[i]).Data;
          idp := String(prod.pId);
          GetDataProizvXML(idp, SL, SLXMLGroupPrImp);
          SvedPrImp := AddChild('����������������');
          with SvedPrImp do
          begin
            Attributes['NameOrg'] := prod.pName;
            Attributes['INN'] := prod.pInn;
            Attributes['KPP'] := prod.pKpp;
            InsertProductXML(SvedPrImp,SLXMLGroupPrImp);
          end;
        end;
    except
    end;
  finally
    FreeStringList(SLXMLGroupPrImp);
    FreeStringList(SLPrImp);
  end;
end;

procedure InsertProductXML(node: IXMLNode;SL:TStringList);
var i:integer;
  decl:TDeclaration;
  product : IXMLNode;
begin
  for i := 0 to SL.Count - 1 do
    if TDecl_obj(SL.Objects[i]) <> nil then
      if TDecl_obj(SL.Objects[i]) is TDecl_obj then
      begin
        decl := TDecl_obj(SL.Objects[i]).Data;
        product := node.AddChild('���������');
        with product do
        begin
          Attributes['�200000000013'] := decl.DeclDate;
          Attributes['�200000000014'] := decl.DeclNum;
          Attributes['�200000000015'] := decl.TM;
          Attributes['�200000000016'] := FloatToStrF(decl.Vol, ffFixed, maxint,5);
        end;
      end;
end;

procedure CreateXMLPostKpp(SL:TStringList);
var i:integer;
  decl:TDeclaration;
  SLKpp, SLKppXML:TStringList;
  kpp:string;
begin
  SLKpp:=TStringList.Create;
  SLKppXML := TStringList.Create;
  try
    for i := 0 to SL.Count - 1 do
    begin
      decl := TDecl_obj(SL.Objects[i]).Data;
      kpp := String(decl.SelfKpp);
      if SLKpp.IndexOf(kpp) = -1 then
        SLKpp.Add(kpp);
    end;

    SLKpp.CustomSort(CompNumAsc);
    if SLKpp.Count > 1 then
      for i := 0 to SLKpp.Count - 1 do
      begin
        kpp := SLKpp.Strings[i];
        GetDataKppXML(kpp,SL,SLKppXML);
        CreateXMLPost(SLKppXML);
      end
    else
      CreateXMLPost(SL);
  finally
    SLKpp.Free;
    FreeStringList(SLKppXML);
  end;
end;

procedure GetDataKppXML(kpp:String;SLData:TStringList; var SLFiltr:TStringList);
var i:integer;
  decl:TDeclaration;
begin
  for i := 0 to SLData.Count - 1 do
  begin
    decl := TDecl_obj(SLData.Objects[i]).Data;
    if decl.SelfKpp = ShortString(kpp) then
    begin
      SLFiltr.AddObject(SLData.Strings[i], TDecl_obj.Create);
      TDecl_Obj(SLFiltr.Objects[SLFiltr.Count-1]).Data := decl;
    end;
  end;
end;

function CreateNewInn(pINN:string):string;
var i:integer;
  s:string;
  b1:Boolean;
begin
  Result:= pInn;
  s:=pInn;
  if Length(pINN) = 8 then pINN := '0' + pINN;

  for i := 0 to 9 do
  begin
    s:= IntToStr(i) + pInn;
    if InnCheck(s, b1) then
    begin
      Result:=s;
      exit;
    end;
  end;
end;

function ExtDataXML(fName:string):Boolean;
var
  Xml: IXMLDocument;
  SL:TStringList;
  NFile, nDoc, nOb: IXMLNode;
begin
  Result :=False;
  SL := TStringList.Create;
  Xml := TXMLDocument.Create(nil);
  try
    Xml.LoadFromFile(fName);
    NFile := Xml.DocumentElement; // �������� ��� ����
    NDoc := NFile.ChildNodes.FindNode('��������');
    if NDoc <> nil then
    begin
      nOb := NDoc.ChildNodes.FindNode('������');
      if nOb <> nil then
        Result:=True;
    end;
  finally
    XML:=nil;
    SL.Free;
  end;
end;

procedure LoadFileSaler(TCP: TIdTCPClient; RegHost, inn, ver: string);
const byt: array [1 .. 7] of Byte = (12, 9, 200, 1, 1, 100, 1);
var s:string;
  er:Boolean;
  ms, ms1: TMemoryStream;
  contr:TContragent;
  i, FSize:integer;
  s1, sa:AnsiString;
begin
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    try
      Application.ProcessMessages;
      if not TCP.Connected then
      begin
        TCP.Host := RegHost;
        TCP.Connect;
        TCP.Port:=TCPPort;
        if TCP.Connected then
          s := String(AnsiString(TCP.IOHandler.ReadLn));
        if Copy(s, 1, 20) = 'Connected to server.' then
          MemoAdd('C�����: �� ���������� � �������');

        TCP.IOHandler.WriteLn('INN ' + inn + ' ' + ver);
        if TCP.Connected then
          s := String(AnsiString(TCP.IOHandler.ReadLn))
        else
          er := True;
        //���� ������ �� ������, �� ������� ��� � ������ ����������� (���, ���, ���.��.�����)
        if (s = 'NFND') and ((fSert.INN <> '') and (fSert.kpp <> '')) then
        begin
          contr.INN := ShortString(fSert.INN);
          contr.KPP := fSert.kpp;
          contr.EMAIL := ShortString(fSert.email);
          contr.OrgName := ShortString(fSert.INN);
          ms.Clear;
          ms.Position := 0;

//          TCP.IOHandler.WriteLn('EOQR');

//          s:='CONTR';
//          ms.Write(s[1], Length(s));

//          SetLength(aCont, Length(byt));
          for i := 1 to Length(byt) do
          begin
            case i of
              1: s1 := AnsiString(contr.INN);  //��� �����������
              2: s1 := AnsiString(contr.KPP);  //��� �����������
//              3: s1 := AnsiString(contr.EMAIL);//����������� ����� �����������
//              3: s1 := AnsiString(contr.OrgName);//������������ �����������
              3: s1 := AnsiString(contr.INN);//������������ �����������
//              5: s1 := AnsiString(ver);//������ ���������
              4, 5, 7: s1:='1'; //���-�� ���, �������� ������
              6: s1 :=fileSaler;
            end;
            if i < Length(byt) then
              sa := sa + '''' + s1 + ''','
            else
              sa := sa + '''' + s1 + '''';

            while Length(s1) < byt[i] do
              s1 := s1 + ' ';
            ms.WriteBuffer(s1[1], Length(s1));
          end;

          er:=False;
          if not ZipFilePas(ms, ms1, psw_loc) then
            er := True;
          if er then
          begin
            MemoEdit('������ ����������');
            MemoAdd('�������� ������� �������� - ������ ����������');
          end;

          if not er then
          begin
            FSize := ms1.Size;
            TCP.IOHandler.WriteLn('CONTR' + IntToStr(FSize));
            TCP.IOHandler.Write(ms1);
            s := String(AnsiString(TCP.IOHandler.ReadLn));
            if s = 'OK' then
              exit;
          end;
        end;
      end;
    except
      TCP.Socket.Close;
      TCP.Disconnect;
    end;
  finally
    TCP.Socket.Close;
    TCP.Disconnect;
    ms.Free;
  end;
end;

function UpdateNameContr(RegHost, inn, ver: string):Boolean;
const byt: array [1 .. 4] of Byte = (12, 9, 100, 200);
var s:string;
  er:Boolean;
  ms, ms1: TMemoryStream;
  contr:TContragent;
  i, FSize:integer;
  s1, sa:AnsiString;
  TCP: TIdTCPClient;
begin
  TCP := TIdTCPClient.Create(nil);
  TCP.Host := RegHost;
  TCP.Port:=TCPPort;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;
  try
    try
      if not TCP.Connected then
        TCP.Connect;
      s := String(AnsiString(TCP.IOHandler.ReadLn));
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');

      TCP.IOHandler.WriteLn('UCNT' + inn + ' ' + ver);
      TCP.ReadTimeout := 500;
      s := TCP.IOHandler.ReadLn; //

      if Copy(s, 1, 5) = 'YUCNT' then
      begin
        TCP.IOHandler.WriteLn('OK');
        TCP.ReadTimeout := 500;
        s := TCP.IOHandler.ReadLn;
        if s = 'DATA' then
        begin
          contr.INN := ShortString(fSert.INN);
          contr.KPP := fSert.kpp;
          contr.EMAIL := ShortString(fSert.email);
          contr.OrgName := ShortString(fSert.OrgName);
          ms.Clear;
          ms.Position := 0;

          for i := 1 to Length(byt) do
          begin
            case i of
              1: s1 := AnsiString(contr.INN);  //��� �����������
              2: s1 := AnsiString(contr.KPP);  //��� �����������
              3: s1 := AnsiString(contr.EMAIL);//����������� ����� �����������
              4: s1 := AnsiString(contr.OrgName);//������������ �����������
            end;
            if i < Length(byt) then
              sa := sa + '''' + s1 + ''','
            else
              sa := sa + '''' + s1 + '''';

            while Length(s1) < byt[i] do
              s1 := s1 + ' ';
            ms.WriteBuffer(s1[1], Length(s1));
          end;

          er:=False;
          if not ZipFilePas(ms, ms1, psw_loc) then
            er := True;

          if not er then
          begin
            FSize := ms1.Size;
            TCP.IOHandler.WriteLn('DUCNT' + IntToStr(FSize));
            TCP.IOHandler.Write(ms1);
            s := TCP.IOHandler.ReadLn;
          end;
        end;
      end;
    except
      Result := False;
    end;
  finally
    TCP.Socket.Close;
    TCP.Disconnect;
    TCP.Free;
  end;
end;

function GetPasword(RegHost,sInn:string):string;
var s:string;
  TCP: TIdTCPClient;
begin
  Result:='';
  TCP := TIdTCPClient.Create(nil);
  TCP.Host := RegHost;
  TCP.Port:=TCPPort;
  try
    try
      if not TCP.Connected then
        try
          TCP.Connect;
        except
          Result := '';
          TCP.Free;
          exit;
        end;
      s := string(AnsiString(TCP.IOHandler.ReadLn));
      if Copy(s, 1, 20) = 'Connected to server.' then
        MemoAdd('C�����: �� ���������� � �������');

      TCP.IOHandler.WriteLn('PCNT' + sInn );
      TCP.IOHandler.WriteLn('0');
      TCP.ReadTimeout := 500;
      s := TCP.IOHandler.ReadLn;
      if (s <> 'EPCNT') and (s <> 'NPCNT') then
        Result := s;
    except
    end;
  finally
    TCP.Socket.Close;
    TCP.Disconnect;
    TCP.Free
  end;
end;

function GetSalerName(dbq:TADOQuery; sInn:string):string;
Var i, num: integer;
  qr1: TADOQuery;
  find: Boolean;
  SLRow: TStringList;
begin
  Result:='';
  qr1 := nil;

  find := False;
  i := 0;
  num := RefSaler.Count - 1;
  while (i <= num) and (not find) do
  begin
    SLRow := Pointer(RefSaler.Objects[i]);
    if (SLRow.Strings[2] = String(sINN))then
    begin
      find := True;
      Result := SLRow.Strings[1];
    end;
    inc(i);
  end;
  if (not find) then
  begin
    try
      try
        qr1 := TADOQuery.Create(DBQ.Owner);
        qr1.Connection := DBQ.Connection;
        qr1.sql.Text := 'select top 1 * from Saler where OrgINN=''' + String(sINN) + '''';
        qr1.Open;
        if qr1.RecordCount > 0 then
          Result := qr1.FieldByName('orgname').AsString;
      finally
        qr1.free;
      end;
    except
    end;
  end;
end;


function LoadFilesSaler(sInn,psw, path:string):Boolean;
var
  s:string;
  dir_path:string;
begin
  s:='/0'+IntToStr(CurKvart)+IntToStr(CurYear);
  dir_path := path + '\' + fOrg.SelfName + '_0' + IntToStr(CurKvart) + IntToStr(CurYear);
  dir_path:=DeleteSimv('"',dir_path);
  Result := LoadFilesFromFTP(dir_path, s, sInn,psw,0);
end;

function TypeCorOst(ost_old,ost_cur:Real):integer;
begin
  Result:=-1;
  if ost_old = ost_cur then
    Result:=0;
  if (ost_old = 0) and (ost_cur > 0) then
    Result:=1;
  if (ost_old > 0) and (ost_cur = 0) then
    Result:=2;
  if (ost_old <> 0) and (ost_old < ost_cur) then
    Result:=3;
  if (ost_cur <> 0) and  (ost_old > ost_cur) then
    Result:=4;
end;

function CorrectOst(kv1,kv2, sKpp, wap, pInn, pKpp,ostCur,ostOld:string; SG1,SG2:TAdvStringGrid):Boolean;
var idx:Integer;
  ost_old,ost_cur:Real;
begin
  Result:=False;
  ost_old:=StrInFloat(ostOld);
  ost_cur:=StrInFloat(ostCur);
  idx := TypeCorOst(ost_old,ost_cur);
  case idx of
    0: Show_Inf('������� �� ������ �������� ��������� ������� (' + kv1+ ') �'+ #10#13 +
                '����� ����������� ������� ('+kv2+') ���������' + #10#13 +
                '�������� �������� ������', fError);
    // ������ ������� 5 ������ ���� 0
    1: Result:=DeleteAllProdCorOst(fmWork.que1_sql,fmWork.que2_sql,kv1, kv2,SG1);
    // ������ ������� 0 ������ ���� 5
    2,4: Result:=InsertProdCorOst(fmWork.que1_sql,fmWork.que2_sql,kv1,SG1,sKpp,wap, pInn, pKpp, ost_cur,ost_old);
    // ������ ������� 5 ������ ���� 3
    3: Result:=DeleteProdCorOst(fmWork.que1_sql,fmWork.que2_sql,kv1,kv2,sKpp,SG1, ost_cur,ost_old);
  end;
end;

function FillDataOst(kv1, kv2, sKpp,wap, pInn,pKpp:string;SG1, SG2:TAdvStringGrid):Boolean;
var k1,y1, k2, y2:Integer;
  dBeg1, dEnd1, dBeg2, dEnd2:TDateTime;
  SL1, SL2, SLF1, SLF2:TStringList;
begin
  Result:=False;
  k1 := StrToInt(Copy(kv1,1,1));
  y1 := StrToInt(Copy(kv1,11,4));
  k2 := StrToInt(Copy(kv2,1,1));
  y2 := StrToInt(Copy(kv2,11,4));
  dBeg1 := Kvart_Beg(k1, y1);
  dEnd1 := Kvart_End(k1, y1);
  dBeg2 := Kvart_Beg(k2, y2);
  dEnd2 := Kvart_End(k2, y2);
  SL1:=TStringList.Create;
  SL2:=TStringList.Create;
  SLF1:= TStringList.Create;
  SLF2:= TStringList.Create;
  try
    DvizenieKppEan(SL1, SLAllDecl, dBeg1, dEnd1);
    DvizenieKppEan(SL2, SLAllDecl, dBeg2, dEnd2);

    FilterCorOst(sKpp,wap, pInn,pKpp, SL1, SLF1);
    FilterCorOst(sKpp,wap, pInn,pKpp, SL2, SLF2);

    WriteSGCorOst(SLF1,SG1,1);
    WriteSGCorOst(SLF2,SG2,2);
  finally
    SLF2.Free;
    SLF1.Free;
    FreeStringList(SL2);
    FreeStringList(SL1);
  end;
end;

procedure FilterCorOst(sKpp,wap, pInn,pKpp:string; SL:TStringList;  var SLF:TStringList);
var i:Integer;
  mv:TMove;
begin
  for i := 0 to SL.Count - 1 do
  begin
    mv:=TMove(SL.Objects[i]);
    if (mv.SelfKpp = ShortString(sKpp)) and (mv.product.FNSCode = ShortString(wap)) and
       (mv.product.prod.pInn = ShortString(pInn)) and (mv.product.prod.pKpp = ShortString(pKpp)) then
      SLF.AddObject(string(mv.product.EAN13), mv);
  end;
end;

procedure WriteSGCorOst(SL:TStringList;SG:TAdvStringGrid; tKv:integer);
var i, Row:Integer;
  mv:TMove;
begin
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;
  case tKv of
    1: ShapkaSG(SG,NameColCorOst1);
    2: ShapkaSG(SG,NameColCorOst2);
  end;
  SG.RowCount := SL.Count + SG.FixedRows + SG.FixedFooters;
  for i := 0 to SL.Count -  1 do
  begin
    Row:= i + SG.FixedRows;
    mv:=TMove(SL.Objects[i]);
    SG.Cells[1,Row] := string(mv.SelfKpp);
    SG.Cells[2,Row] := string(mv.product.FNSCode);
    SG.Cells[3,Row] := string(mv.product.EAN13);
    case tKv of
      1: SG.Floats[4,Row] := mv.dv_dal.ost1;
      2: SG.Floats[4,Row] := mv.dv_dal.ost0;
    end;
    SG.Cells[5,Row] := string(mv.product.prod.pInn);
    SG.Cells[6,Row] := string(mv.product.prod.pKpp);
    SG.Cells[7,Row] := string(mv.product.prod.pName);
    SG.Cells[8,Row] := string(mv.sal.sInn);
    SG.Cells[9,Row] := string(mv.sal.sKpp);
    SG.Cells[10,Row] := string(mv.sal.sName);
  end;

  SG.ColumnSize.Rows := arFixed;
  SG.AutoSize := True;
  if SG.RowCount > SG.FixedFooters + SG.FixedRows then
    if SG.Cells[1,SG.FixedRows] <> '' then
    begin
      SG.AutoNumberCol(0);
      SG.FloatingFooter.ColumnCalc[4] := acSum;
      SG.CalcFooter(4);
    end;

end;

function DeleteAllProdCorOst(dbqD, dbqB:TADOQuery;kv1, kv2:string;SG:TAdvStringGrid):Boolean;
var i:Integer;
  dE1,dE2:TDateTime;
  k,y:Integer;
  EAN13, sKpp :string;
  ost:Real;
begin
  k:=StrToInt(Copy(kv1,1,1));
  y:=StrToInt(Copy(kv1,11,4));
  dE1:=Kvart_End(k, y);
  k:=StrToInt(Copy(kv2,1,1));
  y:=StrToInt(Copy(kv2,11,4));
  dE2:=Kvart_End(k, y);
  Result:=False;
  for i := SG.FixedRows to SG.RowCount - SG.FixedFooters - 1 do
  begin
    sKpp := SG.Cells[1, i];
    EAN13 := SG.Cells[3, i];
    ost := SG.Floats[4,i];
    UpdateRashProd(dbqD, dbqB, EAN13, sKpp, ost, dE1, dE2);
  end;
end;

function InsertProdCorOst(dbqD,dbqB:TADOQuery;kv:string; SG:TAdvStringGrid; sKpp, wap, pInn, pKpp:string; ost_cur,ost_old:Real):Boolean;
var ost:Real;
  k, y:Integer;
  sal:TSaler;
  dB, dE:TDateTime;
  decl:TDeclaration;
  FP:Boolean;
begin
  Result:=False;
  //��������� ������� � ��������
  ost:=Abs(ost_cur-ost_old);
  k:=StrToInt(Copy(kv,1,1));
  y:=StrToInt(Copy(kv,11,4));
  dB:=Kvart_Beg(k, y);
  dE:=Kvart_End(k, y);
  wap:= Copy(wap,1,3);
  //���� �������
  if (ost_cur = 0) and (SG.RowCount = SG.FixedRows + SG.FixedFooters) then
    begin
      //������ �������� EAN c ����� �������������� � ����� ���������
      FindEan(dbqB,wap, pInn, pKpp, decl.product);
      //���� ����� ��������� ���, �� �������� �� ����
      if decl.product.EAN13 = '' then
      begin
        decl.product.Ean13 := ShortString(GenNewEAN('28', pINN, pKPP, wap));

        decl.product.Productname := ShortString('��������� ������ ' + String(wap) + '. ');
        decl.product.AlcVol := 1;
        decl.product.capacity := 1;
        decl.product.prod.pInn := ShortString(pInn);
        decl.product.prod.pKpp := ShortString(pKpp);
        decl.product.FNSCode := ShortString(wap);

        InsertEanXML(dbqB, decl.product);
      end;
      NullSaler(sal);
      //������ ���������� � ����� ����� ���������
      sal := GetSalerWap(sKpp, wap, dB, dE);
      if (sal.sInn = '') or (sal.sInn = 'NULL') then
      begin
        decl.forma := FormDecl(wap);
        if decl.forma = 11 then
          sal := GetSalerWap(sKpp, '200', dB, dE)
        else
          sal := GetSalerWap(sKpp, '500', dB, dE);
      end;
    end
  else
    begin
      decl.product.EAN13 := ShortString(SG.Cells[3,SG.FixedRows]);
      GetProduction(dbqB, decl.product, FP, True);
      sal.sInn := ShortString(SG.Cells[8, SG.FixedRows]);
      sal.sKpp := ShortString(SG.Cells[9, SG.FixedRows]);
      GetSaler(dbqB, sal, FP, True, False);
      if sal.sInn = '1234567894' then
        sal := GetSalerWap(sKpp, wap, dB, dE);
    end;

  decl.SelfKpp :=ShortString(SKpp);
  decl.DeclDate :=dE;
  decl.RepDate := dE;
  decl.DeclNum := ShortString('O'+IntToStr(y)+IntToStr(k));
  decl.Amount := 10 * ost /decl.product.Capacity;
  decl.Amount:=OkruglDataDecl(decl.Amount);
  decl.product.ProductNameF := ShortString
    (FullNameProduction(String(decl.product.Productname),
      decl.product.AlcVol, decl.product.capacity));
  decl.sal := sal;
  decl.TypeDecl := 1;
  decl.TM := '';
  decl.forma := FormDecl(String(decl.product.FNSCode));

  SaveRowNewDeclDB(dbqD, decl, true);
  SLAllDecl.AddObject(String(decl.pk), TDecl_obj.Create);
  TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;
end;

function DeleteProdCorOst(dbqD, dbqB:TADOQuery;kv1, kv2, sKpp:string;SG:TAdvStringGrid; ost_cur,ost_old:Real):Boolean;
var i, k, y, idx:Integer;
  ost, ostcur, ost_exec, ost_max, val: Real;
  ExtVal:Boolean;
  dE1, dE2:TDateTime;
  EAN13:string;
  delta:Extended;
  SLEAN :TStringList;
begin
  Result :=False;
  k:=StrToInt(Copy(kv1,1,1));
  y:=StrToInt(Copy(kv1,11,4));
  dE1:=Kvart_End(k, y);
  k:=StrToInt(Copy(kv2,1,1));
  y:=StrToInt(Copy(kv2,11,4));
  dE2:=Kvart_End(k, y);
  SLEAN := TStringList.Create;
  try
    //��������� ������� � ��������
    ost:=Abs(ost_cur-ost_old);
    ExtVal:=False;
    ost_exec := 0;
    ostcur:=ost;
    while ost_exec < ost do
    begin
      SLEAN.Clear;
      ExtVal:=False;
      //������ ���������� �� ����� �������� � ������ �������
      for i := SG.FixedRows to SG.RowCount - SG.FixedFooters - 1 do
      begin
        val:=SG.Floats[4,i];
        delta := Abs(val - ostcur);
        delta:=OkruglDataDecl(delta);
        if delta = 0 then
        begin
          ExtVal:=True;
          EAN13:=SG.Cells[3,i];
          Break;
        end;
      end;
      if ExtVal then
      begin
        UpdateRashProd(dbqD, dbqB, EAN13, sKpp, ostcur, dE1, dE2);
        ost_exec:=ost_exec+ostcur;
      end
      else
      begin
        //������ ��� �������� �������� ������� ������ ����������
        for i := SG.FixedRows to SG.RowCount - SG.FixedFooters - 1 do
        begin
          val := SG.Floats[4,i];
          if val > ostcur then
          begin
//            SLEAN.Add(FloatToStr(val));
            SLEAN.Values[FloatToStr(val)]:=SG.Cells[3,i];
          end;
        end;
        //���� ����� ��������� ����������
        if SLEAN.Count > 0 then
        begin
          SLEAN.CustomSort(CompNumDesc);
          EAN13:=SLEAN.Values[SLEAN.Names[SLEAN.Count-1]];
          UpdateRashProd(dbqD, dbqB, EAN13, sKpp, ostcur, dE1, dE2);
          ost_exec:=ost_exec+ostcur;
        end
        else
        begin
          //������ ��� �������� �������� ������� ������ ����������
          for i := SG.FixedRows to SG.RowCount - SG.FixedFooters - 1 do
          begin
            val := SG.Floats[4,i];
            if val < ostcur then
              SLEAN.Values[FloatToStr(val)]:=SG.Cells[3,i];
          end;
          ost_max := StrToFloat(SLEAN.Names[0]);
          idx:= -1;
          for i := 1 to SLEAN.Count - 1 do
            if StrToFloat(SLEAN.Names[i]) > ost_max then
            begin
              ost_max := StrToFloat(SLEAN.Names[i]);
              idx:= i;
            end;
          if idx >= 0 then
          begin
            EAN13:=SLEAN.Values[SLEAN.Names[idx]];
            ostcur:=StrToFloat(SLEAN.Names[idx]);
            UpdateRashProd(dbqD, dbqB, EAN13, sKpp, ostcur, dE1, dE2);
            idx := SG.Cols[3].IndexOf(EAN13);
            if idx <> -1 then
            begin
              delta := SG.Floats[4,idx] - ostcur;
              delta:=OkruglDataDecl(delta);
              SG.Floats[4,idx] := delta;
            end;
            ost_exec :=ost_exec + ostcur;
            ostcur := ost - ost_exec;
          end
          else
          begin
            if SG.RowCount > SG.FixedRows + SG.FixedFooters + 1 then
              for i := SG.FixedRows + 1 to SG.RowCount - SG.FixedFooters - 1 do
              begin
                EAN13:=SG.Cells[3,i];
                delta := SG.Floats[4,i];
                delta:=OkruglDataDecl(delta);
                UpdateRashProd(dbqD, dbqB, EAN13, sKpp, delta, dE1, dE2);
              end;
            EAN13:=SG.Cells[3,SG.FixedRows];
            delta := SG.Floats[4,SG.FixedRows] - ost_old;
            UpdateRashProd(dbqD, dbqB, EAN13, sKpp, delta, dE1, dE2);
            ost_exec := ost;
          end;
        end;

      end;
    end;
  finally
    SLEAN.Free;
  end;
end;

function GetSalerWap(sKpp, wap:string; dB, dE:TDateTime):TSaler;
var i:Integer;
  decl:TDeclaration;
begin
  for i := 0 to SLAllDecl.Count - 1 do
  begin
    Decl:=TDecl_obj(SLAllDecl.Objects[i]).data;
    if (decl.TypeDecl = 1) and
       ((decl.DeclDate <= dE) and (decl.DeclDate >= dB)) and
       (decl.SelfKpp = ShortString(sKpp)) and
       (decl.product.FNSCode = ShortString(wap)) and
       (decl.sal.sInn <> '1234567894') then
      begin
        Result := decl.sal;
        Exit;
      end;
  end;
end;

procedure FindEan(dbq:TADOQuery;wap, pInn, pKpp:string; var prod:TProduction);
var qr:TADOQuery;
begin
  prod.EAN13:='';
  qr := TADOQuery.Create(dbq.Owner);
  qr.Connection := dbq.Connection;
  try
    qr.SQL.Text :=
      'SELECT * FROM PRODUCTION ' +
      'WHERE FNSCode = ''' + wap + ''' and prodkod = ''' + pInn + ''' and ' +
      'prodcod = ''' + pKpp + ''' and Capacity < 3';
    qr.Open;
    if not qr.Eof then
    begin
      prod.EAN13 := ShortString(qr.FieldByName('ean13').AsString);
      prod.FNSCode := ShortString(qr.FieldByName('FNSCode').AsString);
      prod.Capacity := qr.FieldByName('Capacity').AsFloat;
      prod.prod.pInn := ShortString(qr.FieldByName('prodkod').AsString);
      prod.prod.pKpp := ShortString(qr.FieldByName('prodcod').AsString);
      prod.Productname := ShortString(qr.FieldByName('Productname').AsString);
      prod.AlcVol := qr.FieldByName('AlcVol').AsFloat;
      GetProducer(fmWork.que2_sql, prod.prod, True, False);
    end;
  finally
    qr.Free;
  end;
end;

procedure UpdateRashProd(dbqD, dbqB:TADOQuery; EAN13, sKpp:string; ost:Real; dE1, dE2:TDateTime);
var cap, rash, vol:Extended;
  idx, k:Integer;
  pk:string;
  decl:TDeclaration;
  y,m,d:Word;
  qrD, qrB :TADOQuery;
begin
  qrD := TADOQuery.Create(dbqD.Owner);
  qrD.Connection := dbqD.Connection;
  qrB := TADOQuery.Create(dbqD.Owner);
  qrB.Connection := dbqB.Connection;
  try
    SortSLCol(1,SLAllDecl, CompNumAsc);
    // ��������� ������� � ���������
    qrB.SQL.Text := 'SELECT top 1 * FROM PRODUCTION WHERE EAN13 = ''' + EAN13 + '''';
    qrB.Open;
    if not qrB.Eof  then
    begin
      cap:= qrB.FieldByName('Capacity').AsFloat;
      decl.product.EAN13 := ShortString(qrB.FieldByName('EAN13').AsString);
      decl.product.FNSCode := ShortString(qrB.FieldByName('FNSCode').AsString);
      decl.product.AlcVol := qrB.FieldByName('AlcVol').AsFloat;
      decl.product.Capacity := qrB.FieldByName('Capacity').AsFloat;
      decl.product.prod.pInn := ShortString(qrB.FieldByName('prodkod').AsString);
      decl.product.prod.pKpp := ShortString(qrB.FieldByName('prodcod').AsString);
    end;
    //������ ������ � ���������� c �������� ������ ��������� � ������� ��������
    qrD.SQL.Text :=
      'SELECT * FROM DECLARATION WHERE ReportDate = datevalue(''' +
      DateToStr(dE1) + ''') ' + ' and DeclType = ''2'' and SelfKPP = ''' + sKpp +
      ''' and EAN13 = ''' + EAN13 + ''' order by ReportDate desc';
    qrD.Open;
    //���� ������� ���, �� �������� ���

    if qrD.EOF then
    begin
      decl.SelfKpp :=ShortString(SKpp);
      decl.DeclDate :=dE1;
      decl.RepDate := dE1;
      DecodeDate(dE1,y,m,d);
      k:=Kvartal(m);
      decl.DeclNum := ShortString('O'+IntToStr(y)+IntToStr(k));
      decl.Amount := 10 * ost /Cap;
      decl.Amount:=OkruglDataDecl(decl.Amount);
      decl.product.ProductNameF := ShortString
        (FullNameProduction(String(decl.product.Productname),
          decl.product.AlcVol, decl.product.capacity));
      decl.TypeDecl := 2;
      decl.TM := '';
      decl.forma := FormDecl(String(decl.product.FNSCode));

      SaveRowNewDeclDB(dbqD, decl, true);
      SLAllDecl.AddObject(string(decl.pk), TDecl_obj.Create);
      TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;
    end
    else
    begin
      //�������� �������� ������ �������
      rash := qrD.FieldByName('Amount').AsFloat;
      vol := 0.1 * rash * cap;
      vol := vol + ost;
      rash := 10 * Vol / cap;
      rash:=OkruglDataDecl(rash);
      pk := qrD.FieldByName('PK').AsString;
      //������� ����������
      qrD.SQL.Text :=
        'UPDATE DECLARATION SET AMOUNT = ''' + FloatToStr(rash) + ''' WHERE PK = ' + pk ;
      qrD.ExecSQL;

      idx := SLAllDecl.IndexOf(pk);
      if idx <> -1 then
      begin
        TDecl_obj(SLAllDecl.Objects[idx]).data.Amount := rash;
        TDecl_obj(SLAllDecl.Objects[idx]).data.Vol := vol;
        TDecl_obj(SLAllDecl.Objects[idx]).data.Amount_dal := vol;
      end;
    end;
    //������ ������� ������� EAN � ������� �������
    qrD.SQL.Text := 'SELECT Sum((Amount)*(Switch(DeclType=''1'',1, ' +
      'DeclType=''2'',-1, DeclType=''3'',-1, DeclType=''4'',1, ' +
      'DeclType=''5'',-1, DeclType=''6'',-1, DeclType=''7'',1))) '+
      'AS Amount1 ' + 'FROM DECLARATION ' +
      'WHERE reportdate<=datevalue(''' + DateToStr(dE2) + ''') ' +
      'and EAN13 = ''' + EAN13  + ''' and selfkpp = ''' + sKpp + '''';
    qrD.Open;
    if not qrD.Eof then
    begin
      ost := qrD.FieldByName('Amount1').AsFloat;
      if ost < 0 then
      begin
        qrD.SQL.Text :=
          'SELECT pk, Amount FROM DECLARATION WHERE ReportDate = datevalue(''' +
          DateToStr(dE2) + ''') ' + ' and DeclType = ''1'' and SelfKPP = ''' + sKpp +
          ''' and EAN13 = ''' + EAN13 + '''';
        qrD.Open;
        if not qrD.EOF then
        begin
          qrD.SQL.Text :=
            'SELECT pk, Amount FROM DECLARATION WHERE ReportDate = datevalue(''' +
            DateToStr(dE2) + ''') ' + ' and DeclType = ''2'' and SelfKPP = ''' + sKpp +
            ''' and EAN13 = ''' + EAN13 + '''';
          qrD.Open;
          if not qrD.Eof then
          begin
            rash := qrD.FieldByName('Amount').AsFloat;
            pk := qrD.FieldByName('pk').AsString;
            rash := rash + ost;
            rash:=OkruglDataDecl(rash);
            qrD.SQL.Text :=
              'UPDATE DECLARATION SET AMOUNT = ''' + FloatToStr(rash) + ''' WHERE PK = ' + pk ;
            qrD.ExecSQL;
            idx := SLAllDecl.IndexOf(pk);
            if idx <> -1 then
            begin
              TDecl_obj(SLAllDecl.Objects[idx]).data.Amount := rash;
              TDecl_obj(SLAllDecl.Objects[idx]).data.Vol := vol;
              TDecl_obj(SLAllDecl.Objects[idx]).data.Amount_dal := vol;
            end;
          end;
        end
        else
        begin
          qrD.SQL.Text :=
            'SELECT pk FROM DECLARATION WHERE ReportDate = datevalue(''' +
            DateToStr(dE2) + ''') ' + ' and DeclType = ''2'' and SelfKPP = ''' + sKpp +
            ''' and EAN13 = ''' + EAN13 + '''';
          qrD.Open;
          if not qrD.Eof then
          begin
            pk := qrD.FieldByName('pk').AsString;
            qrD.SQL.Text :=
              'DELETE FROM DECLARATION WHERE PK = ' + pk;
            qrD.ExecSQL;
            idx := SLAllDecl.IndexOf(pk);
            if idx <> -1 then
            begin
              SLAllDecl.Objects[idx].Free;
              SLAllDecl.Delete(idx);
            end;
          end;
        end;
      end;
    end;
  finally
    qrB.Free;
    qrD.Free;
  end;
end;


procedure CleanRepProducer(DBQ:TADOQuery);
var qr:TADOQuery;
  SLPK:TStringList;
  i:Integer;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  SLPK:=TStringList.Create;
  try
    qr.Connection := DBQ.Connection;
    //������ �� ������������� �������
    qr.SQL.Text := 'SELECT PK FROM Producer GROUP BY PK HAVING (((Count(PK))>1)) order by PK';
    qr.Open;
    if not qr.Eof then
      while not qr.Eof do
      begin
        SLPK.Add(qr.FieldByName('PK').AsString);
        qr.Next;
      end;
  finally
    qr.Free;
  end;

  try
    for i := 0 to SLPK.Count - 1 do
      ChangePKProducer(DBQ,SLPK.Strings[i]);
  finally
    SLPK.Free;
  end;

end;

procedure ChangePKProducer(DBQ:TADOQuery; PK:string);
var qr:TADOQuery;
  maxPK:Int64;
  sInn, sKpp:string;
  i, idx:Integer;
  SL:TStringList;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  SL:=TStringList.Create;
  try
    qr.Connection := DBQ.Connection;
    qr.SQL.Text := 'Select MAX(PK) from Producer';
    qr.Open;
    maxPK := qr.Fields[0].AsInteger + 1;

    qr.SQL.Text := 'Select * from Producer WHERE PK = ' + PK;
    qr.Open;
    if not qr.Eof then
      if qr.RecordCount > 1 then
      begin
        qr.First;
        while not qr.Eof  do
        begin
          sInn := qr.FieldByName('INN').AsString;
          sKpp := qr.FieldByName('KPP').AsString;
          SL.Add(sInn+'-'+sKpp);
          qr.Next;
        end;
      end;
    for i := 1 to SL.Count - 1 do
    begin
      idx := Pos('-',SL.Strings[i]);
      sInn := Copy(SL.Strings[i],0,idx-1);
      sKpp := Copy(SL.Strings[i],idx+1);
      qr.SQL.Text := 'UPDATE PRODUCER' + #13#10 +
            'Set PK = ' + IntToStr(maxPK) + #13#10 +
            'WHERE INN=''' + sINN + ''' and KPP=''' + sKPP + '''';
      qr.ExecSQL;
      Inc(maxPK);
    end;
  finally
    SL.Free;
    qr.Free;
  end;
end;

//function ConnectServer(SG: TADVStringGrid; RegHost, inn, ver: string): Boolean;
//const
//  uka = '�������';
//
//var
//  s1, S2, s3, cont: string;
//  RFEX: Boolean;
//  find, find1, find2, find3, find4, er, ConnErr: Boolean;
//  k, i, m, ipn, recount: integer;
//  FSize, FSize1: Int64;
//  ms, ms1, ms2: TMemoryStream;
//  Str: array [1 .. 15] of AnsiString;
//  qr2: TADOQuery;
//  SL: TStringList;
//  s, ss: AnsiString;
//  FulSpav: Boolean;
//  TCP : TIdTCPClient;
//function FillRefProduction(ms1:TMemoryStream):Boolean;
//var  qr :TADOQuery;
//  Str: array [1 .. 15] of AnsiString;
//  s:string;
//  k:Integer;
//begin
//  Result:= False;
//  qr := TADOQuery.Create(Application);
//  qr.Connection := fmWork.con2ADOCon;
//
//  SetLength(Str[1], 13);
//  Str[1] := AnsiString(ReadBuf(ms1, 13));
//  SetLength(Str[2], 3);
//  Str[2] := AnsiString(ReadBuf(ms1, 3));
//  SetLength(Str[3], 180);
//  Str[3] := AnsiString(ReadBuf(ms1, 180));
//  SetLength(Str[4], 8);
//  Str[4] := AnsiString(ReadBuf(ms1, 8));
//  SetLength(Str[5], 8);
//  Str[5] := AnsiString(ReadBuf(ms1, 8));
//  SetLength(Str[6], 12);
//  Str[6] := AnsiString(ReadBuf(ms1, 12));
//  SetLength(Str[7], 9);
//  Str[7] := AnsiString(ReadBuf(ms1, 9));
//  SetLength(Str[8], 180);
//  Str[8] := AnsiString(ReadBuf(ms1, 180));
//  SetLength(Str[9], 8);
//  Str[9] := AnsiString(ReadBuf(ms1, 8));
//  SetLength(Str[10], 12);
//  Str[10] := AnsiString(ReadBuf(ms1, 12));
//  SetLength(Str[11], 9);
//  Str[11] := AnsiString(ReadBuf(ms1, 9));
//  SetLength(Str[12], 3);
//  Str[12] := AnsiString(ReadBuf(ms1, 3));
//  SetLength(Str[13], 2);
//  Str[13] := AnsiString(ReadBuf(ms1, 2));
//  SetLength(Str[14], 180);
//  Str[14] := AnsiString(ReadBuf(ms1, 180));
//  for k := 1 to 14 do
//  begin
//    Str[k] := AnsiString
//      (TrimRight(String(Str[k])));
//  end;
//
//  try
//    s := Str[9];
//    PrepareText(Str[3]);
//    qr.sql.Text :=
//      'Select EAN13 From Production where EAN13='''
//      + String(Str[1])
//      + ''' and prodkod=''' + uka + '''';
//    qr.Open;
//    if qr.RecordCount > 0 then
//    begin
//      qr.sql.Text :=
//        'UPDATE Production ' + #13#10 +
//        'Set FNSCode=''' + String(Str[2])
//        + ''', Productname=''' + String
//        (Str[3]) + ''', AlcVol=''' + String
//        (Str[4]) + ''', Capacity=''' + String
//        (Str[5])
//        + ''', ProducerCode=''' + String
//        (Str[9]) + ''', Prodkod=''' + String
//        (Str[6]) + ''', Prodcod=' + String
//        (Str[7])
//        + #13#10 + 'WHERE EAN13=''' + String
//        (Str[1])
//        + '''and prodkod=''' + uka + '''';
//      qr.ExecSQL;
//    end;
//    qr.sql.Text :=
//      'Select EAN13 From Production where EAN13='''
//      + String(Str[1]) + '''';
//    qr.Open;
//    if qr.RecordCount > 0 then
//    begin
//      if qr.RecordCount > 1 then
//      begin
//        qr.sql.Text :=
//        'DELETE from Production WHERE EAN13='''
//        + String(Str[1]) + '''';
//        qr.ExecSQL;
//        qr.sql.Text :=
//        'INSERT INTO Production(EAN13, FNSCode, '
//        +
//        'Productname, AlcVol, Capacity, ProducerCode, '
//        + 'Prodkod, Prodcod)' +
//        #13#10 + 'Values(''' + String
//        (Str[1]) + ''', ''' + String(Str[2])
//        + ''', ''' + String(Str[3])
//        + ''', ''' + String(Str[4])
//        + ''', ''' + String(Str[5])
//        + ''', ''' + String(Str[9])
//        + ''', ''' + String(Str[6])
//        + ''', ''' + String(Str[7])
//        + ''')';
//        qr.ExecSQL;
//      end
//      else
//      begin
//        if qr.RecordCount > 0 then
//        qr.sql.Text :=
//        'UPDATE Production ' + #13#10 +
//        'Set FNSCode=''' + String(Str[2])
//        + ''', Productname=''' + String
//        (Str[3]) + ''', AlcVol=''' + String
//        (Str[4]) + ''', Capacity=''' + String
//        (Str[5])
//        + ''', ProducerCode=''' + String
//        (Str[9]) + ''', Prodkod=''' + String
//        (Str[6]) + ''', Prodcod=' + String
//        (Str[7])
//        + #13#10 + 'WHERE EAN13=''' + String
//        (Str[1]) + '''';
//        qr.ExecSQL;
//        // ���������� ���������� ����������� � ����������
//      end;
//    end
//    else
//    begin
//      qr.sql.Text :=
//        'INSERT INTO Production(EAN13, FNSCode, '
//        +
//        'Productname, AlcVol, Capacity, ProducerCode, '
//        + 'Prodkod, Prodcod)' +
//        #13#10 + 'Values(''' + String
//        (Str[1]) + ''', ''' + String(Str[2])
//        + ''', ''' + String(Str[3])
//        + ''', ''' + String(Str[4])
//        + ''', ''' + String(Str[5])
//        + ''', ''' + String(Str[9])
//        + ''', ''' + String(Str[6])
//        + ''', ''' + String(Str[7])
//        + ''')';
//      qr.ExecSQL;
//    end;
//  except
//    Result := True;
//    MemoEdit('������');
//    MemoAdd(
//      '������ ���������� ����������� ���������'
//      );
//  end;
//
//  if StrCheck(String(Str[9])) and not Result then
//  try
//    Str[9] := AnsiString
//      (TrimRight(String(s)));
//    PrepareText(Str[8]);
//    PrepareText(Str[14]);
//    qr.sql.Text :=
//      'Select * From Producer where inn=''' +
//      String(Str[6])
//      + ''' and kpp=''' + String(Str[7])
//      + '''';
//    qr.Open;
//    if qr.RecordCount > 0 then
//      qr.sql.Text :=
//      'UPDATE Producer ' + #13#10 +
//      'Set pk=''' + String(Str[9])
//      + ''', FullName=''' + String(Str[8])
//      + ''', INN=''' + String(Str[10])
//      + ''', KPP=''' + String(Str[11])
//      + ''', countrycode=''' + String
//      (Str[12])
//      + ''', RegionCode=''' + String
//      (Str[13]) + ''', Address=''' + String
//      (Str[14])
//      + '''' + #13#10 + 'WHERE inn=''' +
//      String(Str[6])
//      + ''' and kpp=''' + String(Str[7])
//      + ''''
//    else
//      qr.sql.Text :=
//      'INSERT INTO Producer(PK, FullName, INN, KPP, '
//      +
//      'countrycode, RegionCode, Address)'
//      + #13#10 + 'Values(' + String
//      (Str[9]) + ', ''' + String(Str[8])
//      + ''', ''' + String(Str[10])
//      + ''', ''' + String(Str[11])
//      + ''', ''' + String(Str[12])
//      + ''', ''' + String(Str[13])
//      + ''', ''' + String(Str[14]) + ''')';
//    qr.ExecSQL;
//  except
//    MemoEdit('������');
//    MemoAdd(
//      '������ ���������� ����������� ��������������');
//    Result := True;
//  end;
//end;
//
//function FillRefSaler(ms1:TMemoryStream; SG:TAdvStringGrid; Row, ipn:integer):Boolean;
//var  qr2 :TADOQuery;
//  Str: array [1 .. 15] of AnsiString;
//  k:integer;
//begin
//  Result:= False;
//  qr2 := TADOQuery.Create(Application);
//  qr2.Connection := fmWork.con2ADOCon;
//  SetLength(Str[1], 8);
//  Str[1] := AnsiString(ReadBuf(ms1, 8));
//  SetLength(Str[2], 180);
//  Str[2] := AnsiString(ReadBuf(ms1, 180));
//  SetLength(Str[3], 10 + ipn);
//  Str[3] := AnsiString(ReadBuf(ms1, 10 + ipn));
//  SetLength(Str[4], 9);
//  Str[4] := AnsiString(ReadBuf(ms1, 9));
//  SetLength(Str[5], 30);
//  Str[5] := AnsiString(ReadBuf(ms1, 30));
//  SetLength(Str[6], 2);
//  Str[6] := AnsiString(ReadBuf(ms1, 2));
//  SetLength(Str[7], 180);
//  Str[7] := AnsiString(ReadBuf(ms1, 180));
//  SetLength(Str[8], 8);
//  Str[8] := AnsiString(ReadBuf(ms1, 8));
//  SetLength(Str[9], 12);
//  Str[9] := AnsiString(ReadBuf(ms1, 12));
//  SetLength(Str[10], 10);
//  Str[10] := AnsiString(ReadBuf(ms1, 10));
//  SetLength(Str[11], 20);
//  Str[11] := AnsiString(ReadBuf(ms1, 20));
//  SetLength(Str[12], 10);
//  Str[12] := AnsiString(ReadBuf(ms1, 10));
//  SetLength(Str[13], 10);
//  Str[13] := AnsiString(ReadBuf(ms1, 10));
//  SetLength(Str[14], 250);
//  Str[14] := AnsiString(ReadBuf(ms1, 250));
//  SetLength(Str[15], 9);
//  Str[15] := AnsiString(ReadBuf(ms1, 9));
//  for k := 1 to 15 do
//    Str[k] := AnsiString
//      (TrimRight(String(Str[k])));
//  PrepareText(Str[2]);
//  PrepareText(Str[7]);
//  PrepareText(Str[14]);
//
//  if (Copy(Str[5], 1, 5) = 'SALER') then
//  begin
//    try
//      qr2.sql.Text :=
//        'Select OrgINN From Saler where OrgINN='''
//        + String(Str[3])
//        + ''' and OrgKPP=''' + String(Str[4])
//        + '''';
//      qr2.Open;
//      if qr2.RecordCount > 0 then
//      begin
//        qr2.sql.Text :=
//        'UPDATE Saler ' + #13#10 +
//        'Set PK=''' + String(Str[1])
//        + ''', OrgName=''' + String(Str[2])
//        + ''', RegionCode=''' + String(Str[6])
//        + ''', Address=''' + String(Str[7])
//        + '''' + #13#10 + 'WHERE OrgINN=''' +
//        String(Str[3])
//        + ''' and OrgKPP=''' + String(Str[4])
//        + '''';
//      end
//      else
//        qr2.sql.Text :=
//        'INSERT INTO Saler(pk,OrgName, OrgINN, '
//        + 'OrgKPP, RegionCode, Address)' +
//        #13#10 + 'Values(''' + String(Str[1])
//        + ''',''' + String(Str[2])
//        + ''', ''' + String(Str[3])
//        + ''', ''' + String(Str[4])
//        + ''', ''' + String(Str[6])
//        + ''', ''' + String(Str[7])
//        + ''')';
//      qr2.ExecSQL;
//      SG.Cells[2, Row] := '���������� ��������';
//      SG.Objects[2, Row] := Pointer(3);
////      FulSpav := True;
//      MemoAdd(
//        '���������� ����������� ��������. ��������� ������ � ��� ' + Copy(SG.Cells[1, Row], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, Row], (27 + ipn), 9));
//      MemoEdit('���������');
//    except
//      MemoEdit('������');
//      MemoAdd(
//        '������ ���������� ����������� �����������');
//      Result := True;
//    end;
//
//    try
//      qr2.sql.Text :=
//        'Select PK From Saler_license where PK='''
//        + String(Str[8]) + '''';
//      qr2.Open;
//      if qr2.RecordCount > 0 then
//      begin
//        qr2.sql.Text :=
//        'UPDATE Saler_license '
//        + #13#10 + 'Set PK=''' + String
//        (Str[8]) + ''', saler_pk=''' + String
//        (Str[9]) + ''',saler_kpp=''' + String
//        (Str[15]) + ''', ser=''' + String
//        (Str[10]) + ''', num=''' + String
//        (Str[11])
//        + ''', date_start=''' + String
//        (Str[12])
//        + ''', date_finish=''' + String
//        (Str[13]) + ''', regorg=''' + String
//        (Str[14])
//        + '''' + #13#10 + 'WHERE pk=''' +
//        String(Str[8]) + '''';
//      end
//      else
//        qr2.sql.Text :=
//        'INSERT INTO Saler_license(PK,saler_pk, ser, '
//        +
//        'num, date_start, date_finish,regorg,saler_kpp)' + #13#10 + 'Values(''' + String(Str[8]) + ''',''' + String(Str[9]) + ''', ''' + String(Str[10]) + ''', ''' + String(Str[11]) + ''', ''' + String(Str[12]) + ''', ''' + String(Str[13]) + ''', ''' + String(Str[14]) + ''',''' + String(Str[15]) + ''')';
//
//      qr2.ExecSQL;
//
//      SG.Cells[2, Row] := '���������� ��������';
//      SG.Objects[2, Row] := Pointer(3);
////      FulSpav := True;
//      MemoAdd(
//        '���������� ����������� ��������. ��������� ������ � ��� �������� ' + Copy(SG.Cells[1, Row], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, Row], (27 + ipn), 9));
//    except
//      MemoEdit('������');
//      MemoAdd(
//        '������ ���������� ����������� ��������'
//        );
//      Result := True;
//    end;
//  end
//  else if StrCheck(String(Str[1])) then
//  begin
//    try
//      qr2.sql.Text :=
//        'Select PK From Producer where PK=' +
//        String(Str[1]);
//      qr2.Open;
//      if qr2.RecordCount > 0 then
//        qr2.sql.Text :=
//        'UPDATE Producer ' + #13#10 +
//        'Set FullName=''' + String(Str[2])
//        + ''', INN=''' + String(Str[3])
//        + ''', KPP=''' + String(Str[4])
//        + ''', countrycode=''' + String
//        (Str[5]) + ''', RegionCode=''' + String
//        (Str[6]) + ''', Address=''' + String
//        (Str[7])
//        + '''' + #13#10 + 'WHERE PK=' + String
//        (Str[1])
//      else
//        qr2.sql.Text :=
//        'INSERT INTO Producer(PK, FullName, INN, KPP, '
//        +
//        'countrycode, RegionCode, Address)'
//        + #13#10 + 'Values(' + String
//        (Str[1]) + ', ''' + String(Str[2])
//        + ''', ''' + String(Str[3])
//        + ''', ''' + String(Str[4])
//        + ''', ''' + String(Str[5])
//        + ''', ''' + String(Str[6])
//        + ''', ''' + String(Str[7]) + ''')';
//      qr2.ExecSQL;
//
//      SG.Cells[2, Row] := '���������� ��������';
//      SG.Objects[2, Row] := Pointer(3);
////      FulSpav := True;
//      MemoEdit('���������');
//      MemoAdd(
//        '���������� ����������� ��������. ��������� ������ � ��� ' + Copy(SG.Cells[1, Row], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, Row], (27 + ipn), 9));
//    except
//      MemoEdit('������');
//      MemoAdd(
//        '������ ���������� ����������� ��������������');
//      Result := True;
//    end;
//  end;
//  if Result then
//  begin
//    SG.Cells[2, Row] :=
//      '������ ���������� �����������';
//    SG.Objects[2, Row] := Pointer(2);
//  end;
//end;
//
//function AddRefTCP(TCP : TIdTCPClient; SG:TADVStringGrid):Boolean;
//var i, recount:Integer;
//  cont, s:string;
//  fSize:Int64;
//  ms, ms1:TMemoryStream;
//  ipn:Integer;
//begin
//  Result:=False;
//  ms:=TMemoryStream.Create;
//  ms1:=TMemoryStream.Create;
//  try
//  Result:=False;
//  try
//    for i := SG.FixedRows to SG.RowCount - SG.FixedRows -
//      SG.FixedFooters do
//    begin
//      if ((Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
//          or (Copy(SG.Cells[1, i], 1, 10) = '������ ���'))
//        and (Copy(SG.Cells[2, i], 1,
//          19) <> '���������� ��������') then
//      begin
//        if (Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
//          and (Copy(SG.Cells[2, i], 1,
//            19) <> '���������� ��������') then
//        begin
//          if Length(SG.Cells[1, i]) = 50 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 13) + Copy(SG.Cells[1, i], 32,
//                10) + '  ' + Copy(SG.Cells[1, i], 42, 9))
//          else if Length(SG.Cells[1, i]) = 40 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 3) + '          ' + Copy(SG.Cells[1, i],
//                22, 10) + '  ' + Copy(SG.Cells[1, i], 32, 9))
//          else if Length(SG.Cells[1, i]) = 42 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 3) + '          ' + Copy(SG.Cells[1, i],
//                22, 21))
//          else if Length(SG.Cells[1, i]) = 47 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 13) + Copy(SG.Cells[1, i], 32,
//                7) + '     ' + Copy(SG.Cells[1, i], 39,
//                9))
//          else if Length(SG.Cells[1, i]) = 31 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 13))
//          else if Length(SG.Cells[1, i]) = 32 then
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 14))
//          else
//            TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
//                19, 34));
//          MemoAdd
//            ('������ ����������� ��������� �� EAN13 ' + Copy
//              (SG.Cells[1, i], 19, 34) + '... ');
//        end;
//
//        if (Copy(SG.Cells[1, i], 1, 10) = '������ ���') and
//          (Copy(SG.Cells[2, i], 1,
//            19) <> '���������� ��������')
//          then
//        begin
//          if (Length(SG.Cells[1, i])) = 35 then
//            ipn := 0
//          else
//            ipn := 2;
//
//          TCP.IOHandler.WriteLn('INN' + Copy(SG.Cells[1, i],
//              12, (10 + ipn)) + Copy(SG.Cells[1, i],
//              (27 + ipn), 9));
//          MemoAdd('������ ����������� ����������� ��� ' + Copy
//              (SG.Cells[1, i], 12, (10 + ipn)) + '/��� ' + Copy
//              (SG.Cells[1, i], (27 + ipn), 9) + '... ');
//
//        end;
//        if ((Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
//            or (Copy(SG.Cells[1, i], 1, 10) = '������ ���'))
//          and (Copy(SG.Cells[2, i], 1,
//            19) <> '���������� ��������') then
//        begin
//          cont := TCP.IOHandler.ReadLn;
//        end
//        else
//          s := '�������� ������';
//        if (Copy(SG.Cells[1, i], 1, 4) = '����') then
//          s := '����';
//        if (Copy(SG.Cells[1, i], 1, 10) = '����������') then
//          s := '����������';
//        for recount := 1 to StrToInt(cont) do
//        begin
//          s := AnsiString(TCP.IOHandler.ReadLn);
//          if Copy(s, 1, 2) = 'OK' then
//          begin
//            MemoAdd(
//              '����� �� ������ ������. ����������� ����� ����������... ');
//            FSize := StrToInt(Copy(String(s), 3,
//                Length(String(s)) - 2));
//            Application.ProcessMessages;
//
//            try
//              Result := False;
//              ms.Clear;
//              ms1.Clear;
//              TCP.IOHandler.ReadStream(ms, FSize);
//              if not TCP.Connected then
//                Result := True;
//              ms.Position := 0;
//              if not UnZipFilePas(ms, ms1, psw_loc) then
//                Result := True;
//              if Result then
//              begin
//                MemoEdit('������ ������������');
//                MemoAdd(
//                  '����� ����������� ������� - ������ ������������');
//              end;
//              if not Result and TCP.Connected then
//              begin
//                Result := False;
//                ms1.Position := 0;
//                if (Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN)) then
//                begin
//                  Result := FillRefProduction(ms1);
//                  if not Result then
//                  begin
//                    MemoEdit('���������');
//                    MemoAdd(
//                      '���������� ��������� ��������. ��������� ������ � EAN13 ' + Copy(SG.Cells[1, i], 19, 13));
//                    SG.Cells[2, i] := '���������� ��������';
//                    SG.Objects[2, i] := Pointer(3);
////                    FulSpav := True;
//                  end
//                  else
//                    SG.Cells[2, i] :=
//                      '������ ���������� �����������';
//                end;
//                if (Copy(SG.Cells[1, i], 1, 10) = '������ ���') then
//                  Result:=FillRefSaler(ms1, SG, i, ipn);
//              end; // if not Er
//            finally
//
//            end;
//          end
//          else // if Copy(s, 1, 2)='OK'
//
//            if (s <> '����') and (s <> '����������') and
//            (Copy(SG.Cells[2, i], 1,
//              19) <> '���������� ��������') then
//          begin
//            if ((Copy(SG.Cells[1, i], 1,
//                  17) = RTrim(cQueryEAN)) and
//                (Copy(SG.Cells[1, i], 19, 2) = '27')) then
//            begin
//              // ��������� �� ��N ������������� � ���� ������
//              InsertNewEanXML(fmWork.que2_sql,
//                Copy(SG.Cells[1, i], 19, 13));
//            end
//            else
//            begin
//              MemoEdit('����� �� ������ �� ������');
//              MemoAdd(
//                '��������� � ���������� ������� ��� ���������� ������������ � ���� ������ �������');
//              SG.Cells[2, i] := '����� �� ������ �� ������';
//              SG.Objects[2, i] := Pointer(2);
//              Application.ProcessMessages;
//            end;
//          end;
//        end;
//      end;
//    end; // for i:=0
//  except
////    ConnErr := True;
//  end;
//  finally
//    ms.Free;
//    ms1.Free;
//  end;
//end;
//
//function AddRef(TCP : TIdTCPClient; SG:TADVStringGrid):Boolean;
//const
//  byt: array [1 .. 14] of Byte = (180, 13, 10, 10, 180, 12, 9, 180, 180,
//      10, 20, 10, 10, 250);
//var ms, ms1, ms2:TMemoryStream;
//  ipn, i, k, m:Integer;
//  fSize:Int64;
//  find:Boolean;
//  s:string;
//  ss:AnsiString;
//begin
//  Result:=False;
//  ms:=TMemoryStream.Create;
//  ms1:=TMemoryStream.Create;
//  try
//    TCP.IOHandler.WriteLn('EOQR');
//
//
//    try
//      for i := 1 to SG.RowCount - 1 do
//      begin
//        Result := False;
//        ms.Clear;
//        ms1.Clear;
//        find := False;
//        if (Copy(SG.Cells[1, i], 1, 16) = '���������� EAN13')
//          and (Copy(SG.Cells[2, i], 1,
//            30) <> '������ �� ���������� ���������') then
//        begin
//          MemoAdd(
//            '�������� ������� �� ���������� � ���������� ��������� ' + Copy(SG.Cells[1, i], 12, 19));
//          k := 0;
//          while (k < Length(RefsAddEAN[0])) and (not find) do
//          begin
//            if RefsAddEAN[2, k] = Copy(SG.Cells[1, i], 18,
//              13) then
//              find := True;
//            inc(k);
//          end;
//          dec(k);
//          s := 'EAN';
//          ms1.Write(s[1], Length(s));
//          for m := 1 to Length(RefsAddEAN) - 1 do
//          begin
//            s := AnsiString(RefsAddEAN[m, k]);
//            while Length(s) < byt[m] do
//              s := s + ' ';
//
//            ms1.WriteBuffer(s[1], Length(s));
//          end;
//
//          ms2 := TMemoryStream.Create;
//          try
//            ms2.Clear;
//            ms2.LoadFromStream(ms1);
//            ms2.Position := 0;
//            ss := AnsiString(ReadBuf(ms2, Length(s)));
//            ss := ss + '';
//          finally
//            ms2.Free;
//          end;
//        end;
//        if (Copy(SG.Cells[1, i], 1, 14) = '���������� ���')
//          and (Copy(SG.Cells[2, i], 1,
//            30) <> '������ �� ���������� ���������') then
//        begin
//          if (Length(SG.Cells[1, i])) = 39 then
//            ipn := 0
//          else
//            ipn := 2;
//          MemoAdd(
//            '�������� ������� �� ���������� � ���������� ����������� ' + Copy(SG.Cells[1, i], 12, 28 + ipn));
//          k := 0;
//          Application.ProcessMessages;
//          while (k < Length(RefsAddINN[0])) and (not find) do
//          begin
//            if (RefsAddINN[2, k] + RefsAddINN[3,
//              k] = Copy(SG.Cells[1, i], 16,
//                10 + ipn) + Copy(SG.Cells[1, i], 31 + ipn,
//                9)) then
//              find := True;
//            inc(k);
//          end;
//          dec(k);
//          s := 'INN';
//          ms1.Write(s[1], Length(s));
//          for m := 1 to Length(RefsAddINN) - 1 do
//          begin
//            s := AnsiString(RefsAddINN[m, k]);
//            while Length(s) < byt[m + 4] do
//              s := s + ' ';
//            ms1.WriteBuffer(s[1], Length(s));
//          end;
//        end;
//
//        if find then
//        begin
//
//          if not ZipFilePas(ms1, ms, psw_loc) then
//            Result := True;
//          if Result then
//          begin
//            MemoEdit('������ ����������');
//            MemoAdd(
//              '�������� ������� �������� - ������ ����������'
//              );
//          end;
//          if not Result then
//          begin
//            FSize := ms.Size;
//            TCP.IOHandler.WriteLn('ADRF' + IntToStr(FSize));
//            TCP.IOHandler.Write(ms);
//            s := AnsiString(TCP.IOHandler.ReadLn);
//            if s = 'OK' then
//            begin
//              SG.Cells[2, i] :=
//                '������ �� ���������� ���������';
//
//              s := AnsiString(SG.Cells[1, i]);
//              if pos('���������� EAN13', String(s)) > 0 then
//              begin
//                s := Copy(s, 18, Length(s) - 17);
//                SG.Cells[1, i] :=
//                  cQueryEAN + String(s);
//              end;
//              if pos('���������� ���', String(s)) > 0 then
//              begin
//                s := Copy(s, 16, Length(s) - 15);
//                SG.Cells[1, i] := '������ ��� ' + String(s);
//              end;
//
//              SG.Objects[2, i] := Pointer(3);
//              if (Copy(SG.Cells[1, i], 1,
//                  16) = '���������� EAN13') then
//                MemoAdd(
//                  '������ �� ���������� � ���������� ��������� � ' + Copy
//                    (SG.Cells[1, i], 12, 19) + ' ���������');
//              if (Copy(SG.Cells[1, i], 1,
//                  14) = '���������� ���') then
//                MemoAdd(
//                  '������ �� ���������� � ���������� ����������� � '
//                    + Copy(SG.Cells[1, i], 12,
//                    28) + ' ���������');
//            end
//            else
//            begin
//              SG.Cells[2, i] :=
//                '������ �������� ������� �� ����������';
//              if (Copy(SG.Cells[1, i], 1,
//                  16) = '���������� EAN13') then
//                MemoAdd(
//                  '������ �������� ������� �� ���������� � ���������� ��������� � ' + Copy(SG.Cells[1, i], 12, 19));
//              if (Copy(SG.Cells[1, i], 1,
//                  14) = '���������� ���') then
//                MemoAdd(
//                  '������ �������� ������� �� ���������� � ���������� ����������� � ' + Copy(SG.Cells[1, i], 12, 28));
//            end;
//          end;
//        end;
//      end;
//    except
////      ConnErr := True;
//    end;
//  finally
//    ms1.Free;
//    ms.Free;
//  end;
//end;
//
//begin
//  TCP := TIdTCPClient.Create(nil);
//  TCP.Host:=BMHost;
//  TCP.Port:=TCPPort;
//  qr2 := TADOQuery.Create(Application);
//  qr2.Connection := fmWork.con2ADOCon;
//  ms := TMemoryStream.Create;
//  ms1 := TMemoryStream.Create;
//
//  SL := TStringList.Create;
//  FulSpav := False;
//  ConnErr := False;
//  er := False;
//  try
//    try
//      find4 := False;
//      Application.ProcessMessages;
//      if not TCP.Connected then
//      begin
//        TCP.Host := RegHost;
//        TCP.Connect;
//      end;
//
//      try
//        if TCP.Connected then
//          s := AnsiString(TCP.IOHandler.ReadLn);
//        if Copy(s, 1, 20) = 'Connected to server.' then
//          MemoAdd('C�����: �� ���������� � �������');
//
//        TCP.IOHandler.WriteLn('INN ' + inn + ' ' + ver);
//        if TCP.Connected then
//          s := AnsiString(TCP.IOHandler.ReadLn);
//        if not TCP.Connected then
//          er := True;
//        if Copy(s, 1, 20) = 'Connected to server.' then
//          s := 'FAULT';
//
//        if SameText(String(s), 'RFEX') then
//          TCP.IOHandler.WriteLn('OK');
//
//        if SameText(String(s), 'OK') then
//        begin
//          if not er then
//          begin
//            find := False;
//            find3 := False;
//            find4 := False;
//            k := SG.FixedRows;
//            FSize1 := 0;
//            while (k < SG.RowCount - SG.FixedFooters) do
//            begin
//              if ((Copy(SG.Cells[1, k], 1, 17) = RTrim(cQueryEAN)) or
//                  (Copy(SG.Cells[1, k], 1, 10) = '������ ���')) and
//                (Copy(SG.Cells[2, k], 1, 19) <> '���������� ��������') then
//              begin
//                find := True;
//                inc(FSize1);
//              end;
//              if ((Copy(SG.Cells[1, k], 1, 16) = '���������� EAN13') or
//                  (Copy(SG.Cells[1, k], 1, 14) = '���������� ���')) and
//                (Copy(SG.Cells[2, k], 1, 30) <>
//                  '������ �� ���������� ���������') then
//              begin
//                find3 := True;
//                inc(FSize1);
//              end;
//              inc(k);
//            end;
//            k := 1;
//
//            if find then
//              AddRefTCP(TCP, SG);
//
//            if find3 and (not ConnErr) then
//              AddRef(TCP, SG);
//          end;
//        end
//        else
//          if SameText(String(s), 'DSBL') then
//            begin
//              MemoAdd(
//                '����������� �������� �������� �����������: ������ ����������� ������� � ��� ' + inn);
//              er := True;
//            end
//          else
//            if SameText(String(s), 'NFND') then
//              begin
//                MemoAdd
//                  ('����������� �������� �������� �����������: ������ � ��� ' + inn + ' �� ������ � �����������');
//                er := True;
//              end
//            else
//              if SameText(String(s), 'FAULT') then
//                begin
//                  MemoAdd
//                    ('����������� �������� ��������: ������������� �� �������');
//                  er := True;
//                end
//              else
//                begin
//                  MemoAdd('����������� �������� ��������: ����������� ������. ("' +
//                      String(s) + '")');
//                end;
//        Application.ProcessMessages;
//
//        if FulSpav then
//        begin
//        end;
//      finally
//        MemoAdd('C�����: �� ��������� �� �������');
//        SL.free;
//        ms.free;
//        ms1.free;
//        qr2.free;
//        IsConnect := False;
//      end;
//    except
//      MemoAdd('�� ������� ������������ � �������');
//      MemoAdd('������ ����������� � �������');
//      MessageDlg('������ ����������� � �������', mtError, [mbOK], 0);
//      IsConnect := False;
//    end;
//  except
//    MemoAdd('������ ����������� � �������');
//    TCP.Socket.Close;
//    TCP.Disconnect;
//    TCP.Free;
//    MessageDlg('������ ����������� � �������', mtError, [mbOK], 0);
//    IsConnect := False;
//  end;
//  TCP.Socket.Close;
//  TCP.Disconnect;
//  TCP.Free;
//  MemoAdd('');
//end;
//
//
function ConnectServer(SG: TADVStringGrid; RegHost, inn, ver: string): Boolean;
const
  uka = '�������';
  byt: array [1 .. 14] of Byte = (180, 13, 10, 10, 180, 12, 9, 180, 180,
    10, 20, 10, 10, 250);
var
  s1, S2, s3, cont: string;
  RFEX: Boolean;
  find, find1, find2, find3, find4, er, ConnErr: Boolean;
  k, i, m, ipn, recount: integer;
  FSize, FSize1: Int64;
  ms, ms1, ms2: TMemoryStream;
  Str: array [1 .. 15] of AnsiString;
  qr2: TADOQuery;
  SL: TStringList;
  s, ss: AnsiString;
  FulSpav: Boolean;
  TCP : TIdTCPClient;
begin
  TCP := TIdTCPClient.Create(nil);
  TCP.Host:=BMHost;
  TCP.Port:=TCPPort;
  qr2 := TADOQuery.Create(Application);
  qr2.Connection := fmWork.con2ADOCon;
  ms := TMemoryStream.Create;
  ms1 := TMemoryStream.Create;

  SL := TStringList.Create;
  FulSpav := False;
  ConnErr := False;
  er := False;
  try
    try
      find4 := False;
      Application.ProcessMessages;
      if not TCP.Connected then
      begin
        TCP.Host := RegHost;
        TCP.Connect;
      end;
      try
        if TCP.Connected then
          s := AnsiString(TCP.IOHandler.ReadLn);
        if Copy(s, 1, 20) = 'Connected to server.' then
          MemoAdd('C�����: �� ���������� � �������');

        TCP.IOHandler.WriteLn('INN ' + inn + ' ' + ver);
        if TCP.Connected then
          s := AnsiString(TCP.IOHandler.ReadLn);
        if not TCP.Connected then
          er := True;
        if Copy(s, 1, 20) = 'Connected to server.' then
          s := 'FAULT';

        if SameText(String(s), 'RFEX') then
          TCP.IOHandler.WriteLn('OK');

        if SameText(String(s), 'OK') then
        begin
          if not er then
          begin
            find := False;
            find3 := False;

            find4 := False;

            k := SG.FixedRows;
            FSize1 := 0;
            while (k < SG.RowCount - SG.FixedFooters) do
            begin
              if ((Copy(SG.Cells[1, k], 1, 17) = RTrim(cQueryEAN)) or
                  (Copy(SG.Cells[1, k], 1, 10) = '������ ���')) and
                (Copy(SG.Cells[2, k], 1, 19) <> '���������� ��������') then
              begin
                find := True;
                inc(FSize1);
              end;
              if ((Copy(SG.Cells[1, k], 1, 16) = '���������� EAN13') or
                  (Copy(SG.Cells[1, k], 1, 14) = '���������� ���')) and
                (Copy(SG.Cells[2, k], 1, 30) <>
                  '������ �� ���������� ���������') then
              begin
                find3 := True;
                inc(FSize1);
              end;
              inc(k);
            end; // while
            k := 1;

            if find then
            begin
              try
                for i := SG.FixedRows to SG.RowCount - SG.FixedRows -
                  SG.FixedFooters do
                begin
                  if ((Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
                      or (Copy(SG.Cells[1, i], 1, 10) = '������ ���'))
                    and (Copy(SG.Cells[2, i], 1,
                      19) <> '���������� ��������') then
                  begin
                    if (Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
                      and (Copy(SG.Cells[2, i], 1,
                        19) <> '���������� ��������') then
                    begin
                      if Length(SG.Cells[1, i]) = 50 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 13) + Copy(SG.Cells[1, i], 32,
                            10) + '  ' + Copy(SG.Cells[1, i], 42, 9))
                      else if Length(SG.Cells[1, i]) = 40 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 3) + '          ' + Copy(SG.Cells[1, i],
                            22, 10) + '  ' + Copy(SG.Cells[1, i], 32, 9))
                      else if Length(SG.Cells[1, i]) = 42 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 3) + '          ' + Copy(SG.Cells[1, i],
                            22, 21))
                      else if Length(SG.Cells[1, i]) = 47 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 13) + Copy(SG.Cells[1, i], 32,
                            7) + '     ' + Copy(SG.Cells[1, i], 39,
                            9))
                      else if Length(SG.Cells[1, i]) = 31 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 13))
                      else if Length(SG.Cells[1, i]) = 32 then
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 14))
                      else
                        TCP.IOHandler.WriteLn('EAN' + Copy(SG.Cells[1, i],
                            19, 34));
                      MemoAdd
                        ('������ ����������� ��������� �� EAN13 ' + Copy
                          (SG.Cells[1, i], 19, 34) + '... ');
                    end;

                    if (Copy(SG.Cells[1, i], 1, 10) = '������ ���') and
                      (Copy(SG.Cells[2, i], 1,
                        19) <> '���������� ��������')
                      then
                    begin
                      if (Length(SG.Cells[1, i])) = 35 then
                        ipn := 0
                      else
                        ipn := 2;

                      TCP.IOHandler.WriteLn('INN' + Copy(SG.Cells[1, i],
                          12, (10 + ipn)) + Copy(SG.Cells[1, i],
                          (27 + ipn), 9));
                      MemoAdd('������ ����������� ����������� ��� ' + Copy
                          (SG.Cells[1, i], 12, (10 + ipn)) + '/��� ' + Copy
                          (SG.Cells[1, i], (27 + ipn), 9) + '... ');

                    end;
                    if ((Copy(SG.Cells[1, i], 1, 17) = RTrim(cQueryEAN))
                        or (Copy(SG.Cells[1, i], 1, 10) = '������ ���'))
                      and (Copy(SG.Cells[2, i], 1,
                        19) <> '���������� ��������') then
                    begin
                      cont := TCP.IOHandler.ReadLn;
                    end
                    else
                      s := '�������� ������';
                    if (Copy(SG.Cells[1, i], 1, 4) = '����') then
                      s := '����';
                    if (Copy(SG.Cells[1, i], 1, 10) = '����������') then
                      s := '����������';
                    for recount := 1 to StrToInt(cont) do
                    begin
                      s := AnsiString(TCP.IOHandler.ReadLn);
                      if Copy(s, 1, 2) = 'OK' then
                      begin
                        MemoAdd(
                          '����� �� ������ ������. ����������� ����� ����������... ');
                        FSize := StrToInt(Copy(String(s), 3,
                            Length(String(s)) - 2));
                        Application.ProcessMessages;

                        try
                          er := False;
                          ms.Clear;
                          ms1.Clear;
                          TCP.IOHandler.ReadStream(ms, FSize);
                          if not TCP.Connected then
                            er := True;
                          ms.Position := 0;
                          if not UnZipFilePas(ms, ms1, psw_loc) then
                            er := True;
                          if er then
                          begin
                            MemoEdit('������ ������������');
                            MemoAdd(
                              '����� ����������� ������� - ������ ������������');
                          end;
                          if not er and TCP.Connected then
                          begin
                            er := False;
                            ms1.Position := 0;
                            if (Copy(SG.Cells[1, i], 1,
                                17) = RTrim(cQueryEAN)) then
                            begin
                              SetLength(Str[1], 13);
                              Str[1] := AnsiString(ReadBuf(ms1, 13));
                              SetLength(Str[2], 3);
                              Str[2] := AnsiString(ReadBuf(ms1, 3));
                              SetLength(Str[3], 180);
                              Str[3] := AnsiString(ReadBuf(ms1, 180));
                              SetLength(Str[4], 8);
                              Str[4] := AnsiString(ReadBuf(ms1, 8));
                              SetLength(Str[5], 8);
                              Str[5] := AnsiString(ReadBuf(ms1, 8));
                              SetLength(Str[6], 12);
                              Str[6] := AnsiString(ReadBuf(ms1, 12));
                              SetLength(Str[7], 9);
                              Str[7] := AnsiString(ReadBuf(ms1, 9));
                              SetLength(Str[8], 180);
                              Str[8] := AnsiString(ReadBuf(ms1, 180));
                              SetLength(Str[9], 8);
                              Str[9] := AnsiString(ReadBuf(ms1, 8));
                              SetLength(Str[10], 12);
                              Str[10] := AnsiString(ReadBuf(ms1, 12));
                              SetLength(Str[11], 9);
                              Str[11] := AnsiString(ReadBuf(ms1, 9));
                              SetLength(Str[12], 3);
                              Str[12] := AnsiString(ReadBuf(ms1, 3));
                              SetLength(Str[13], 2);
                              Str[13] := AnsiString(ReadBuf(ms1, 2));
                              SetLength(Str[14], 180);
                              Str[14] := AnsiString(ReadBuf(ms1, 180));
                              for k := 1 to 14 do
                              begin
                                Str[k] := AnsiString
                                  (TrimRight(String(Str[k])));
                              end;
                              try

                                s := Str[9];
                                PrepareText(Str[3]);
                                qr2.sql.Text :=
                                  'Select EAN13 From Production where EAN13='''
                                  + String(Str[1])
                                  + ''' and prodkod=''' + uka + '''';
                                qr2.Open;
                                if qr2.RecordCount > 0 then
                                begin
                                  qr2.sql.Text :=
                                    'UPDATE Production ' + #13#10 +
                                    'Set FNSCode=''' + String(Str[2])
                                    + ''', Productname=''' + String
                                    (Str[3]) + ''', AlcVol=''' + String
                                    (Str[4]) + ''', Capacity=''' + String
                                    (Str[5])
                                    + ''', ProducerCode=''' + String
                                    (Str[9]) + ''', Prodkod=''' + String
                                    (Str[6]) + ''', Prodcod=''' + String
                                    (Str[7]) + ''' ' + #10#13 +
                                    'WHERE EAN13=''' + String
                                    (Str[1])
                                    + '''and prodkod=''' + uka + '''';
                                  qr2.ExecSQL;
                                end;
                                qr2.sql.Text :=
                                  'Select EAN13 From Production where EAN13='''
                                  + String(Str[1]) + '''';
                                qr2.Open;
                                if qr2.RecordCount > 0 then
                                begin
                                  if qr2.RecordCount > 1 then
                                  begin
                                    qr2.sql.Text :=
                                    'DELETE from Production WHERE EAN13='''
                                    + String(Str[1]) + '''';
                                    qr2.ExecSQL;
                                    qr2.sql.Text :=
                                    'INSERT INTO Production(EAN13, FNSCode, '
                                    +
                                    'Productname, AlcVol, Capacity, ProducerCode, '
                                    + 'Prodkod, Prodcod)' +
                                    #13#10 + 'Values(''' + String
                                    (Str[1]) + ''', ''' + String(Str[2])
                                    + ''', ''' + String(Str[3])
                                    + ''', ''' + String(Str[4])
                                    + ''', ''' + String(Str[5])
                                    + ''', ''' + String(Str[9])
                                    + ''', ''' + String(Str[6])
                                    + ''', ''' + String(Str[7])
                                    + ''')';
                                    qr2.ExecSQL;
                                    UpdateLocalProduction(Str);
                                  end
                                  else
                                  begin
                                    if qr2.RecordCount > 0 then
                                    qr2.sql.Text :=
                                    'UPDATE Production ' + #13#10 +
                                    'Set FNSCode=''' + String(Str[2])
                                    + ''', Productname=''' + String
                                    (Str[3]) + ''', AlcVol=''' + String
                                    (Str[4]) + ''', Capacity=''' + String
                                    (Str[5])
                                    + ''', ProducerCode=''' + String
                                    (Str[9]) + ''', Prodkod=''' + String
                                    (Str[6]) + ''', Prodcod=' + String
                                    (Str[7])
                                    + #13#10 + 'WHERE EAN13=''' + String
                                    (Str[1]) + '''';
                                    qr2.ExecSQL;
                                    // ���������� ���������� ����������� � ����������
                                    UpdateLocalProduction(Str);
                                  end;
                                end
                                else
                                begin
                                  qr2.sql.Text :=
                                    'INSERT INTO Production(EAN13, FNSCode, '
                                    +
                                    'Productname, AlcVol, Capacity, ProducerCode, '
                                    + 'Prodkod, Prodcod)' +
                                    #13#10 + 'Values(''' + String
                                    (Str[1]) + ''', ''' + String(Str[2])
                                    + ''', ''' + String(Str[3])
                                    + ''', ''' + String(Str[4])
                                    + ''', ''' + String(Str[5])
                                    + ''', ''' + String(Str[9])
                                    + ''', ''' + String(Str[6])
                                    + ''', ''' + String(Str[7])
                                    + ''')';
                                  qr2.ExecSQL;

                                  //���������� � ��������� ���������� ���������
                                  AddLocalProduction(Str);
                                end;
                              except
                                er := True;
                                MemoEdit('������');
                                MemoAdd('������ ���������� ����������� ���������');
                              end;
                              /// //////////////////////////////////////////////////////////
                              if StrCheck(String(Str[9])) and not er then
                                try
                                  Str[9] := AnsiString
                                    (TrimRight(String(s)));
                                  PrepareText(Str[8]);
                                  PrepareText(Str[14]);
                                  qr2.sql.Text :=
                                    'Select * From Producer where inn=''' +
                                    String(Str[6])
                                    + ''' and kpp=''' + String(Str[7])
                                    + '''';
                                  qr2.Open;
                                  if qr2.RecordCount > 0 then
                                  begin
                                    qr2.sql.Text :=
                                    'UPDATE Producer ' + #13#10 +
                                    'Set pk=''' + String(Str[9])
                                    + ''', FullName=''' + String(Str[8])
                                    + ''', INN=''' + String(Str[10])
                                    + ''', KPP=''' + String(Str[11])
                                    + ''', countrycode=''' + String
                                    (Str[12])
                                    + ''', RegionCode=''' + String
                                    (Str[13]) + ''', Address=''' + String
                                    (Str[14])
                                    + '''' + #13#10 + 'WHERE inn=''' +
                                    String(Str[6])
                                    + ''' and kpp=''' + String(Str[7])
                                    + '''';
                                    UpdateLocalProducer(Str);
                                  end
                                  else
                                  begin
                                    qr2.sql.Text :=
                                    'INSERT INTO Producer(PK, FullName, INN, KPP, '
                                    +
                                    'countrycode, RegionCode, Address)'
                                    + #13#10 + 'Values(' + String
                                    (Str[9]) + ', ''' + String(Str[8])
                                    + ''', ''' + String(Str[10])
                                    + ''', ''' + String(Str[11])
                                    + ''', ''' + String(Str[12])
                                    + ''', ''' + String(Str[13])
                                    + ''', ''' + String(Str[14]) + ''')';
                                    AddLocalProducer(Str);
                                  end;
                                  qr2.ExecSQL;
                                except
                                  MemoEdit('������');
                                  MemoAdd(
                                    '������ ���������� ����������� ��������������');
                                  er := True;
                                end;
                              if not er then
                              begin
                                MemoEdit('���������');
                                MemoAdd(
                                  '���������� ��������� ��������. ��������� ������ � EAN13 ' + Copy(SG.Cells[1, i], 19, 13));
                                SG.Cells[2, i] := '���������� ��������';
                                SG.Objects[2, i] := Pointer(3);
                                FulSpav := True;
                              end
                              else
                                SG.Cells[2, i] :=
                                  '������ ���������� �����������';
                            end; // if Length(ComboBox1.Items[i])=13

                            if (Copy(SG.Cells[1, i], 1, 10) = '������ ���')
                              then
                            begin
                              SetLength(Str[1], 8);
                              Str[1] := AnsiString(ReadBuf(ms1, 8));
                              SetLength(Str[2], 180);
                              Str[2] := AnsiString(ReadBuf(ms1, 180));
                              SetLength(Str[3], 10 + ipn);
                              Str[3] := AnsiString(ReadBuf(ms1, 10 + ipn));
                              SetLength(Str[4], 9);
                              Str[4] := AnsiString(ReadBuf(ms1, 9));
                              SetLength(Str[5], 30);
                              Str[5] := AnsiString(ReadBuf(ms1, 30));
                              SetLength(Str[6], 2);
                              Str[6] := AnsiString(ReadBuf(ms1, 2));
                              SetLength(Str[7], 180);
                              Str[7] := AnsiString(ReadBuf(ms1, 180));
                              SetLength(Str[8], 8);
                              Str[8] := AnsiString(ReadBuf(ms1, 8));
                              SetLength(Str[9], 12);
                              Str[9] := AnsiString(ReadBuf(ms1, 12));
                              SetLength(Str[10], 10);
                              Str[10] := AnsiString(ReadBuf(ms1, 10));
                              SetLength(Str[11], 20);
                              Str[11] := AnsiString(ReadBuf(ms1, 20));
                              SetLength(Str[12], 10);
                              Str[12] := AnsiString(ReadBuf(ms1, 10));
                              SetLength(Str[13], 10);
                              Str[13] := AnsiString(ReadBuf(ms1, 10));
                              SetLength(Str[14], 250);
                              Str[14] := AnsiString(ReadBuf(ms1, 250));
                              SetLength(Str[15], 9);
                              Str[15] := AnsiString(ReadBuf(ms1, 9));
                              for k := 1 to 15 do
                                Str[k] := AnsiString
                                  (TrimRight(String(Str[k])));
                              PrepareText(Str[2]);
                              PrepareText(Str[7]);
                              PrepareText(Str[14]);

                              if (Copy(Str[5], 1, 5) = 'SALER') then
                              begin
                                try
                                  qr2.sql.Text :=
                                    'Select OrgINN From Saler where OrgINN='''
                                    + String(Str[3])
                                    + ''' and OrgKPP=''' + String(Str[4])
                                    + '''';
                                  qr2.Open;
                                  if qr2.RecordCount > 0 then
                                  begin
                                    qr2.sql.Text :=
                                    'UPDATE Saler ' + #13#10 +
                                    'Set PK=''' + String(Str[1])
                                    + ''', OrgName=''' + String(Str[2])
                                    + ''', RegionCode=''' + String(Str[6])
                                    + ''', Address=''' + String(Str[7])
                                    + '''' + #13#10 + 'WHERE OrgINN=''' +
                                    String(Str[3])
                                    + ''' and OrgKPP=''' + String(Str[4])
                                    + '''';
                                    UpdateLocalSaler(Str);
                                  end
                                  else
                                    qr2.sql.Text :=
                                    'INSERT INTO Saler(pk,OrgName, OrgINN, '
                                    + 'OrgKPP, RegionCode, Address)' +
                                    #13#10 + 'Values(''' + String(Str[1])
                                    + ''',''' + String(Str[2])
                                    + ''', ''' + String(Str[3])
                                    + ''', ''' + String(Str[4])
                                    + ''', ''' + String(Str[6])
                                    + ''', ''' + String(Str[7])
                                    + ''')';
                                  qr2.ExecSQL;
                                  SG.Cells[2, i] := '���������� ��������';
                                  SG.Objects[2, i] := Pointer(3);
                                  FulSpav := True;
                                  MemoAdd(
                                    '���������� ����������� ��������. ��������� ������ � ��� ' + Copy(SG.Cells[1, i], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, i], (27 + ipn), 9));
                                  MemoEdit('���������');
                                  AddLocalSaler(Str);
                                except
                                  MemoEdit('������');
                                  MemoAdd(
                                    '������ ���������� ����������� �����������');
                                  er := True;
                                end;

                                try
                                  qr2.sql.Text :=
                                    'Select PK From Saler_license where PK='''
                                    + String(Str[8]) + '''';
                                  qr2.Open;
                                  if qr2.RecordCount > 0 then
                                  begin
                                    qr2.sql.Text :=
                                    'UPDATE Saler_license '
                                    + #13#10 + 'Set PK=''' + String
                                    (Str[8]) + ''', saler_pk=''' + String
                                    (Str[9]) + ''',saler_kpp=''' + String
                                    (Str[15]) + ''', ser=''' + String
                                    (Str[10]) + ''', num=''' + String
                                    (Str[11])
                                    + ''', date_start=''' + String
                                    (Str[12])
                                    + ''', date_finish=''' + String
                                    (Str[13]) + ''', regorg=''' + String
                                    (Str[14])
                                    + '''' + #13#10 + 'WHERE pk=''' +
                                    String(Str[8]) + '''';
                                    UpdateLocalSalerLic(Str);
                                  end
                                  else
                                    qr2.sql.Text :=
                                    'INSERT INTO Saler_license(PK,saler_pk, ser, '
                                    +
                                    'num, date_start, date_finish,regorg,saler_kpp)' + #13#10 + 'Values(''' + String(Str[8]) + ''',''' + String(Str[9]) + ''', ''' + String(Str[10]) + ''', ''' + String(Str[11]) + ''', ''' + String(Str[12]) + ''', ''' + String(Str[13]) + ''', ''' + String(Str[14]) + ''',''' + String(Str[15]) + ''')';

                                  qr2.ExecSQL;

                                  SG.Cells[2, i] := '���������� ��������';
                                  SG.Objects[2, i] := Pointer(3);
                                  FulSpav := True;
                                  MemoAdd(
                                    '���������� ����������� ��������. ��������� ������ � ��� �������� ' + Copy(SG.Cells[1, i], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, i], (27 + ipn), 9));
                                  AddLocalSalerLic(Str);
                                except
                                  MemoEdit('������');
                                  MemoAdd(
                                    '������ ���������� ����������� ��������'
                                    );
                                  er := True;
                                end;
                              end
                              else if StrCheck(String(Str[1])) then
                              begin
                                try
                                  qr2.sql.Text :=
                                    'Select PK From Producer where PK=' +
                                    String(Str[1]);
                                  qr2.Open;
                                  if qr2.RecordCount > 0 then
                                    qr2.sql.Text :=
                                    'UPDATE Producer ' + #13#10 +
                                    'Set FullName=''' + String(Str[2])
                                    + ''', INN=''' + String(Str[3])
                                    + ''', KPP=''' + String(Str[4])
                                    + ''', countrycode=''' + String
                                    (Str[5]) + ''', RegionCode=''' + String
                                    (Str[6]) + ''', Address=''' + String
                                    (Str[7])
                                    + '''' + #13#10 + 'WHERE PK=' + String
                                    (Str[1])
                                  else
                                    qr2.sql.Text :=
                                    'INSERT INTO Producer(PK, FullName, INN, KPP, '
                                    +
                                    'countrycode, RegionCode, Address)'
                                    + #13#10 + 'Values(' + String
                                    (Str[1]) + ', ''' + String(Str[2])
                                    + ''', ''' + String(Str[3])
                                    + ''', ''' + String(Str[4])
                                    + ''', ''' + String(Str[5])
                                    + ''', ''' + String(Str[6])
                                    + ''', ''' + String(Str[7]) + ''')';
                                  qr2.ExecSQL;

                                  SG.Cells[2, i] := '���������� ��������';
                                  SG.Objects[2, i] := Pointer(3);
                                  FulSpav := True;
                                  MemoEdit('���������');
                                  MemoAdd(
                                    '���������� ����������� ��������. ��������� ������ � ��� ' + Copy(SG.Cells[1, i], 12, (10 + ipn)) + '/��� ' + Copy(SG.Cells[1, i], (27 + ipn), 9));
                                except
                                  MemoEdit('������');
                                  MemoAdd(
                                    '������ ���������� ����������� ��������������');
                                  er := True;
                                end;
                              end; // if Copy(str[1], 1, 1)='0' and Copy(str[5], 1, 5)='SALER'
                              if er then
                              begin
                                SG.Cells[2, i] :=
                                  '������ ���������� �����������';
                                SG.Objects[2, i] := Pointer(2);
                              end;
                            end; // if Length(ComboBox1.Items[i])=19
                          end; // if not Er
                        finally

                        end;
                      end
                      else // if Copy(s, 1, 2)='OK'

                        if (s <> '����') and (s <> '����������') and
                        (Copy(SG.Cells[2, i], 1,
                          19) <> '���������� ��������') then
                      begin
                        if ((Copy(SG.Cells[1, i], 1,
                              17) = RTrim(cQueryEAN)) and
                            (Copy(SG.Cells[1, i], 19, 2) = '27')) then
                        begin
                          // ��������� �� ��N ������������� � ���� ������
                          InsertNewEanXML(fmWork.que2_sql,
                            Copy(SG.Cells[1, i], 19, 13));
                        end
                        else
                        begin
                          MemoEdit('����� �� ������ �� ������');
                          MemoAdd(
                            '��������� � ���������� ������� ��� ���������� ������������ � ���� ������ �������');
                          SG.Cells[2, i] := '����� �� ������ �� ������';
                          SG.Objects[2, i] := Pointer(2);
                          Application.ProcessMessages;
                        end;
                      end;
                    end;
                  end;
                end; // for i:=0
              except
               on e:exception do
                  begin
                  MemoAdd(e.Message);
                  ConnErr := True;
                  end;
              end;
            end; // if find

            if TCP.Socket.Connected then
              if TCP.Connected then
              begin
                TCP.IOHandler.WriteLn('EOQR');
                if find3 and (not ConnErr) then
                  try
                    for i := 1 to SG.RowCount - 1 do
                    begin
                      er := False;
                      ms.Clear;
                      ms1.Clear;
                      find := False;
                      if (Copy(SG.Cells[1, i], 1, 16) = '���������� EAN13')
                        and (Copy(SG.Cells[2, i], 1,
                          30) <> '������ �� ���������� ���������') then
                      begin
                        MemoAdd(
                          '�������� ������� �� ���������� � ���������� ��������� ' + Copy(SG.Cells[1, i], 12, 19));
                        k := 0;
                        while (k < Length(RefsAddEAN[0])) and (not find) do
                        begin
                          if RefsAddEAN[2, k] = Copy(SG.Cells[1, i], 18,
                            13) then
                            find := True;
                          inc(k);
                        end;
                        dec(k);
                        s := 'EAN';
                        ms1.Write(s[1], Length(s));
                        for m := 1 to Length(RefsAddEAN) - 1 do
                        begin
                          s := AnsiString(RefsAddEAN[m, k]);
                          while Length(s) < byt[m] do
                            s := s + ' ';

                          ms1.WriteBuffer(s[1], Length(s));
                        end;

                        ms2 := TMemoryStream.Create;
                        try
                          ms2.Clear;
                          ms2.LoadFromStream(ms1);
                          ms2.Position := 0;
                          ss := AnsiString(ReadBuf(ms2, Length(s)));
                          ss := ss + '';
                        finally
                          ms2.Free;
                        end;
                      end;
                      if (Copy(SG.Cells[1, i], 1, 14) = '���������� ���')
                        and (Copy(SG.Cells[2, i], 1,
                          30) <> '������ �� ���������� ���������') then
                      begin
                        if (Length(SG.Cells[1, i])) = 39 then
                          ipn := 0
                        else
                          ipn := 2;
                        MemoAdd(
                          '�������� ������� �� ���������� � ���������� ����������� ' + Copy(SG.Cells[1, i], 12, 28 + ipn));
                        k := 0;
                        Application.ProcessMessages;
                        while (k < Length(RefsAddINN[0])) and (not find) do
                        begin
                          if (RefsAddINN[2, k] + RefsAddINN[3,
                            k] = Copy(SG.Cells[1, i], 16,
                              10 + ipn) + Copy(SG.Cells[1, i], 31 + ipn,
                              9)) then
                            find := True;
                          inc(k);
                        end;
                        dec(k);
                        s := 'INN';
                        ms1.Write(s[1], Length(s));
                        for m := 1 to Length(RefsAddINN) - 1 do
                        begin
                          s := AnsiString(RefsAddINN[m, k]);
                          while Length(s) < byt[m + 4] do
                            s := s + ' ';
                          ms1.WriteBuffer(s[1], Length(s));
                        end;
                      end;

                      if find then
                      begin
                        if not ZipFilePas(ms1, ms, psw_loc) then
                          er := True;
                        if er then
                        begin
                          MemoEdit('������ ����������');
                          MemoAdd(
                            '�������� ������� �������� - ������ ����������'
                            );
                        end;
                        if not er then
                        begin
                          FSize := ms.Size;
                          TCP.IOHandler.WriteLn('ADRF' + IntToStr(FSize));
                          TCP.IOHandler.Write(ms);
                          s := AnsiString(TCP.IOHandler.ReadLn);
                          if s = 'OK' then
                          begin
                            SG.Cells[2, i] :=
                              '������ �� ���������� ���������';
                            s := AnsiString(SG.Cells[1, i]);
                            if pos('���������� EAN13', String(s)) > 0 then
                            begin
                              s := Copy(s, 18, Length(s) - 17);
                              SG.Cells[1, i] :=
                                cQueryEAN + String(s);
                            end;
                            if pos('���������� ���', String(s)) > 0 then
                            begin
                              s := Copy(s, 16, Length(s) - 15);
                              SG.Cells[1, i] := '������ ��� ' + String(s);
                            end;

                            SG.Objects[2, i] := Pointer(3);
                            if (Copy(SG.Cells[1, i], 1,
                                16) = '���������� EAN13') then
                              MemoAdd(
                                '������ �� ���������� � ���������� ��������� � ' + Copy
                                  (SG.Cells[1, i], 12, 19) + ' ���������');
                            if (Copy(SG.Cells[1, i], 1,
                                14) = '���������� ���') then
                              MemoAdd(
                                '������ �� ���������� � ���������� ����������� � '
                                  + Copy(SG.Cells[1, i], 12,
                                  28) + ' ���������');
                          end
                          else
                          begin
                            SG.Cells[2, i] :=
                              '������ �������� ������� �� ����������';
                            if (Copy(SG.Cells[1, i], 1,
                                16) = '���������� EAN13') then
                              MemoAdd(
                                '������ �������� ������� �� ���������� � ���������� ��������� � ' + Copy(SG.Cells[1, i], 12, 19));
                            if (Copy(SG.Cells[1, i], 1,
                                14) = '���������� ���') then
                              MemoAdd(
                                '������ �������� ������� �� ���������� � ���������� ����������� � ' + Copy(SG.Cells[1, i], 12, 28));
                          end;
                        end;
                      end;
                    end;
                  except
                    ConnErr := True;
                  end;
              end;
          end; // if not Er
        end
        else // else begin //if SameText(s, 'OK')
          if SameText(String(s), 'DSBL') then
        begin
          MemoAdd(
            '����������� �������� �������� �����������: ������ ����������� ������� � ��� ' + inn);
          er := True;
        end
        else if SameText(String(s), 'NFND') then
        begin
          MemoAdd
            ('����������� �������� �������� �����������: ������ � ��� ' + inn + ' �� ������ � �����������');
          er := True;
        end
        else if SameText(String(s), 'FAULT') then
        begin
          MemoAdd
            ('����������� �������� ��������: ������������� �� �������');
          er := True;
        end
        else
        begin
          MemoAdd('����������� �������� ��������: ����������� ������. ("' +
              String(s) + '")');
        end;
        Application.ProcessMessages;
        if not SameText(String(s), 'UPDT') then
        begin
        end;
        if FulSpav then
        begin
        end;
      finally
        MemoAdd('C�����: �� ��������� �� �������');
        SL.free;
        ms.free;
        ms1.free;
//        ms2.Free;
        qr2.free;
        IsConnect := False;
      end;
    except
      MemoAdd('�� ������� ������������ � �������');
      MemoAdd('������ ����������� � �������');
      Show_Inf('������ ����������� � �������', fError);
      IsConnect := False;
    end;
  except
    MemoAdd('������ ����������� � �������');
    TCP.Socket.Close;
    TCP.Disconnect;
    TCP.Free;
    Show_Inf('������ ����������� � �������', fError);
    IsConnect := False;
  end;
  TCP.Socket.Close;
  TCP.Disconnect;
  TCP.Free;
  MemoAdd('');
end;

function ExistFilesSalerFTP(pathFTP, pasw, Inn:string):Integer;
var
  idftp: TIdFTP;
  dirName:string;
  i, len:integer;
begin
  Result:=1;
  idftp:=TIdFTP.Create(nil);
  try
    try
      IdFTP.Host:='SoftAlco.space';// ftp ����� �������
      IdFTP.Username:='0-'+INN;//�����
      IdFTP.Password:=pasw;//������
      IdFTP.Passive := True;
      IdFTP.TransferType := ftBinary;
      try
        IdFTP.Connect;
      except
        Result :=0;
        Exit;
      end;

      try
        idftp.ChangeDir(pathFTP);
        try
          idFTP.List;
        except
        end;

        if IdFTP.DirectoryListing.Count = 0 then
        begin
          Result := 1;
          exit;
        end;

        for i:=0 to IdFTP.DirectoryListing.Count-1 do
        if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
        begin
          dirName := IdFTP.DirectoryListing.Items[i].FileName;
          len := Length(dirName);
          //���� ������� ��� ��� ����������
          if (len = 10) or (len = 12) then //�������� ������� ����������
          begin
            Result:= -1;
            exit;
          end;
        end;
      except
        Result :=1;
        exit;
      end;

    finally
      if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
    end;
  except
  end;
end;

procedure AddLocalProduction(Str: array of AnsiString);
var SLRow:TStringList;
  idx, i:integer;
  decl:TDeclaration;
  prod:TProduction;
begin
  idx := RefProduction.IndexOf(String(Str[0]));
  if idx = -1 then
  begin
    SLRow:=TStringList.Create;
    RefProduction.Add(String(Str[0]));
    SLRow.Add(string(Str[0]));
    SLRow.Add(string(Str[1]));
    SLRow.Add(string(Str[2]));
    SLRow.Add(string(Str[3]));
    SLRow.Add(string(Str[4]));
    SLRow.Add(string(Str[8]));
    SLRow.Add('TRUE');
    SLRow.Add('TRUE');
    SLRow.Add(string(Str[5]));
    SLRow.Add(string(Str[6]));
    RefProduction.Objects[RefProduction.Count - 1] := SLRow;

    prod.EAN13 := ShortString(SLRow.Strings[0]);
    prod.FNSCode := ShortString(SLRow.Strings[1]);
    prod.Productname := ShortString(SLRow.Strings[2]);
    prod.AlcVol := StrToFloat(SLRow.Strings[3]);
    prod.capacity := StrToFloat(SLRow.Strings[4]);
    prod.ProductNameF := ShortString(FullNameProduction(String(prod.Productname),
      prod.AlcVol, prod.capacity));
    prod.prod.pINN := ShortString(SLRow.Strings[8]);
    prod.prod.pKPP := ShortString(SLRow.Strings[9]);
    GetProducer(fmWork.que2_sql, prod.prod, True, False);

    for i := 0 to SLAllDecl.Count - 1 do
    begin
      decl:=TDecl_obj(SLAllDecl.Objects[i]).data;
      if decl.product.EAN13 = Str[0] then
        decl.product := prod;
    end;

  end;
end;

procedure UpdateLocalProduction(Str: array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefProduction.IndexOf(String(Str[0]));
  if idx <> -1 then
  begin
    SLRow:=Pointer(RefProduction.Objects[idx]);
    SLRow.Clear;
    SLRow.Add(string(Str[0]));
    SLRow.Add(string(Str[1]));
    SLRow.Add(string(Str[2]));
    SLRow.Add(string(Str[3]));
    SLRow.Add(string(Str[4]));
    SLRow.Add(string(Str[8]));
    SLRow.Add('TRUE');
    SLRow.Add('TRUE');
    SLRow.Add(string(Str[5]));
    SLRow.Add(string(Str[6]));
    RefProduction.Objects[idx] := SLRow;
  end;
end;

procedure AddLocalProducer(Str:array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefProducer.IndexOf(String(Str[8]));
  if idx = -1 then
  begin
    SLRow:=TStringList.Create;
    RefProducer.Add(String(Str[8]));
    SLRow.Add(string(Str[8]));
    SLRow.Add(string(Str[7]));
    SLRow.Add(string(Str[9]));
    SLRow.Add(string(Str[10]));
    SLRow.Add(string(Str[11]));
    SLRow.Add(string(Str[12]));
    SLRow.Add(string(Str[13]));
    SLRow.Add('TRUE');
    SLRow.Add('TRUE');
    RefProducer.Objects[RefProducer.Count - 1] := SLRow;
  end;
end;

procedure UpdateLocalProducer(Str:array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefProducer.IndexOf(String(Str[8]));
  if idx <> -1 then
  begin
    SLRow:=Pointer(RefProducer.Objects[idx]);
    SLRow.Clear;
    SLRow.Add(string(Str[8]));
    SLRow.Add(string(Str[7]));
    SLRow.Add(string(Str[9]));
    SLRow.Add(string(Str[10]));
    SLRow.Add(string(Str[11]));
    SLRow.Add(string(Str[12]));
    SLRow.Add(string(Str[13]));
    SLRow.Add('TRUE');
    SLRow.Add('TRUE');
    RefProducer.Objects[idx] := SLRow;
  end;
end;

procedure AddLocalSalerLic(Str: array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefLic.IndexOf(String(Str[7]));
  if idx = -1 then
  begin
    SLRow:=TStringList.Create;
    RefLic.Add(String(Str[7]));
    SLRow.Add(string(Str[7]));
    SLRow.Add(string(Str[9]));
    SLRow.Add(string(Str[10]));
    SLRow.Add(string(Str[11]));
    SLRow.Add(string(Str[12]));
    SLRow.Add(string(Str[13]));
    SLRow.Add(string(Str[8]));
    SLRow.Add(string(Str[14]));
    RefLic.Objects[RefLic.Count - 1] := SLRow;
  end;
end;

procedure UpdateLocalSalerLic(Str: array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefLic.IndexOf(String(Str[7]));
  if idx <> -1 then
  begin
    SLRow:=Pointer(RefLic.Objects[idx]);
    SLRow.Clear;
    SLRow.Add(string(Str[7]));
    SLRow.Add(string(Str[9]));
    SLRow.Add(string(Str[10]));
    SLRow.Add(string(Str[11]));
    SLRow.Add(string(Str[12]));
    SLRow.Add(string(Str[13]));
    SLRow.Add(string(Str[8]));
    SLRow.Add(string(Str[14]));
    RefLic.Objects[idx] := SLRow;
  end;
end;

procedure UpdateLocalSaler(Str: array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefSaler.IndexOf(String(Str[0]));
  if idx <> -1 then
  begin
    SLRow:=Pointer(RefSaler.Objects[idx]);
    SLRow.Clear;
    SLRow.Add(string(Str[0]));
    SLRow.Add(string(Str[1]));
    SLRow.Add(string(Str[2]));
    SLRow.Add(string(Str[3]));
    SLRow.Add(string(Str[5]));
    SLRow.Add(string(Str[6]));
    SLRow.Add('True');
    SLRow.Add('True');
    RefSaler.Objects[idx] := SLRow;
  end;
end;



procedure AddLocalSaler(Str: array of AnsiString);
var SLRow:TStringList;
  idx:integer;
begin
  idx := RefSaler.IndexOf(String(Str[0]));
  if idx = -1 then
  begin
    SLRow:=TStringList.Create;
    RefSaler.Add(String(Str[0]));
    SLRow.Add(string(Str[0]));
    SLRow.Add(string(Str[1]));
    SLRow.Add(string(Str[2]));
    SLRow.Add(string(Str[3]));
    SLRow.Add(string(Str[5]));
    SLRow.Add(string(Str[6]));
    SLRow.Add('True');
    SLRow.Add('True');
    RefSaler.Objects[RefSaler.Count - 1] := SLRow;
  end;
end;

function WriteDeclBM(sfile, Inn:string; iKvart, iYear:integer):Boolean;
var sPasw, sDir, dirName, Dir, fName, fZip:string;
  idftp: TIdFTP;
  findDir, putFile:Boolean;
  i:Integer;
begin
  Result:=False;
  sPasw := '';
  try
      //������� ������ ������� �� ftp
    try
      sPasw := GetPasword(BMHost,Inn);
    except
      exit;
    end;
    if sPasw = '' then sPasw := GetPasword(BMHost,Inn);

    if sPasw = '' then exit;

    if ZipFileNew(sfile) then
      fZip := sFile + '.zip'
    else
      fZip := sFile;

    IdFTP:=TIdFTP.Create(nil);
    IdFTP.Host:=BMHost;

    IdFTP.Username:='0-'+INN;//�����
    IdFTP.Password:=sPasw;//������
    IdFTP.Passive := True;
    IdFTP.TransferType := ftBinary;

    IdFTP.Connect;
    fName := ExtractFileName(fZip);

    try
      idFTP.List;
    except
    end;

    findDir := False;
    sDir := '0'+ IntToStr(iKvart) + IntToStr(iYear);
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = sDir then
          findDir :=True;
      end;

    if not findDir then
      IDFTP.MakeDir('/'+sDir+'/');
    IdFTP.ChangeDir(sDir);

    //��������� ������ ����������
    try
      idFTP.List;
    except
    end;

    Dir := 'Decl';

    sDir := '/'+sDir+'/'+dir+'/';

    findDir :=False;
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = Dir then
          findDir :=True;
      end;

    if not findDir then
      IDFTP.MakeDir(sDir);
    IdFTP.ChangeDir(Dir);


    //��������� ������ ����������
    try
      idFTP.List;
    except
    end;

    findDir :=False;
    for i:=0 to IdFTP.DirectoryListing.Count-1 do
      if IdFTP.DirectoryListing.Items[i].ItemType = ditFile then
      begin
        //������������� ��������/�����
        dirName := IdFTP.DirectoryListing.Items[i].FileName;
        if dirName = fName then
          findDir :=True;
      end;
    putFile := False;

    try
      IdFTP.Put(fZip,fName,true); //���� ������-����;
      putFile := True;
      DeleteFile(PWideChar(fZip));
    except
    end;

    if putFile then
      Result:=True;


  finally
    if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
  end;
end;


function LoadFilesFromFTP(path_disk, adr_ftp, sUser, sPasw:string; DateMod:TDateTime):Boolean;
var
  idftp: TIdFTP;
  i, len:integer;
  s, dirName, dir, sName:string;
  findLast, EndLast:Boolean;
  SLDate:TStringList;
  dDir:TDateTime;
begin
  Result:=False;
  idftp:=TIdFTP.Create(nil);
  SLDate:=TStringList.Create;
  try
    try
      IdFTP.Host:='SoftAlco.space';// ftp ����� �������
      IdFTP.Username:='0-'+sUser;//�����
      IdFTP.Password:=sPasw;//������
      IdFTP.Passive := True;
      IdFTP.TransferType := ftBinary;

      IdFTP.Connect;
      try
        idftp.ChangeDir(adr_ftp);
        if not DirectoryExists(path_disk) then
          MkDir(path_disk);
      except
        Show_inf('�� ������� ��� ��� ������ �� ��������� ���������' + #10#13 +
                 '��� ����������� ' + fOrg.SelfName + #10#13 +
                 '���������� ����� �����.', fError);
        exit;
      end;
      try
        idFTP.List;
      except
      end;
      for i:=0 to IdFTP.DirectoryListing.Count-1 do
        if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
        begin
          //������������� ��������/�����
          dirName := IdFTP.DirectoryListing.Items[i].FileName;
          len := Length(dirName);
          //���� ������� ��� ��� ����������
          if (len = 10) or (len = 12) then //�������� ������� ����������
          begin
            sName := GetSalerName(fmWork.que2_sql,dirName);
            if sName <> '' then
            begin
              sName:=DeleteSimv('"', sName);
              dir := path_disk + '\' + sName + '\';
            end
            else
              dir := path_disk + '\' + dirName + '\';
            if not DirectoryExists(dir) then
              MkDir(dir);
            FindLast:=False;
            EndLast:=True;
            LoadFilesFromFTP(dir, IdFTP.RetrieveCurrentDir + '/' +
              IdFTP.DirectoryListing.Items[i].FileName, sUser, sPasw,0);
          end
          else //������� � ������� ������
          begin
            if DateMod = 0 then
            begin
              dDir:=IdFTP.DirectoryListing.Items[i].ModifiedDate;
              s:=DateToStr(dDir);
              SLDate.Add(s);
            end
            else
              begin
                dDir:=IdFTP.DirectoryListing.Items[i].ModifiedDate;
                s:=DateToStr(dDir);
                dDir:=StrToDate(s);
                if dDir = DateMod then
                  LoadFilesFromFTP(path_disk, IdFTP.RetrieveCurrentDir + '/' +
                    IdFTP.DirectoryListing.Items[i].FileName, sUser, sPasw,0);
              end;
          end;
        end
        else
          IdFTP.Get(IdFTP.DirectoryListing.Items[i].FileName, path_disk +
            idFTP.DirectoryListing.Items[i].FileName, True, False);

      if SLDate.Count > 0 then
      begin
        SLDate.CustomSort(CompDateDesc);
        s:=SLDate.Strings[0];
        dDir:=StrToDateTime(s);
        LoadFilesFromFTP(path_disk, IdFTP.RetrieveCurrentDir, sUser, sPasw, dDir);
      end;
      Result:=True;
    finally
      if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
      SLDate.Free;
    end;
  except
  end;
end;

function ZipFileNew(fn: string): Boolean;
var
  Zip: TFWZipWriter;
  // s: TStringStream;
  f: string;
  procedure CheckResult(value: integer);
  begin
    if value < 0 then
      raise Exception.Create('������ ���������� ������');
  end;

begin
  Result := False;
  try
    Zip := TFWZipWriter.Create;
    try
      CheckResult(Zip.AddFile(fn));
      f := fn + '.zip';
      if FileExists(f) then
        DeleteFile(PWideChar(f));

      Zip.BuildZip(f);
      // ShowMessage('������������� ����� ��������� �������');
      Result := True;
    finally
      Zip.free;
    end;
  except
    on E: Exception do
      WriteLn(E.ClassName, ': ', E.Message);
  end;
end;

function MoveEanKppCount(DBQ:TADOQuery; KPP1, KPP2:string;
  Kolvo:Extended; sal:TSaler; prod:TProduction):Boolean;
var sql:TADOQuery;
  PK:integer;
  decl:TDeclaration;
begin
  Result:=False;

  sql:=TADOQuery.Create(DBQ.Owner);
  try
    sql.Connection := DBQ.Connection;

    sql.ParamCheck := False;
    sql.Parameters.Clear;

    sql.sql.Text := 'select MAX(PK) from Declaration';
    sql.Open;
    PK := sql.Fields[0].AsInteger + 1;

    sql.sql.Clear;
    sql.Parameters.Clear;
    sql.sql.Text :=
      'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, inn, kpp ) '
      + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :inn, :kpp)';

    sql.Parameters.AddParameter.Name := 'PK';
    sql.Parameters.ParamByName('PK').DataType := ftInteger;
    sql.Parameters.ParamByName('PK').value := PK;

    sql.Parameters.AddParameter.Name := 'DeclType';
    sql.Parameters.ParamByName('DeclType').DataType := ftString;
    sql.Parameters.ParamByName('DeclType').value := IntToStr(6);

    sql.Parameters.AddParameter.Name := 'ReportDate';
    sql.Parameters.ParamByName('ReportDate').DataType := ftDate;
    sql.Parameters.ParamByName('ReportDate').value := dEnd;

    sql.Parameters.AddParameter.Name := 'SelfKPP';
    sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
    sql.Parameters.ParamByName('SelfKPP').value := Kpp1;

    sql.Parameters.AddParameter.Name := 'DeclDate';
    sql.Parameters.ParamByName('DeclDate').DataType := ftDate;
    sql.Parameters.ParamByName('DeclDate').value := CurDate;

    sql.Parameters.AddParameter.Name := 'SalerINN';
    sql.Parameters.ParamByName('SalerINN').DataType := ftString;
    sql.Parameters.ParamByName('SalerINN').value := sal.sInn;

    sql.Parameters.AddParameter.Name := 'SalerKPP';
    sql.Parameters.ParamByName('SalerKPP').DataType := ftString;
    sql.Parameters.ParamByName('SalerKPP').value := sal.sKpp;

    sql.Parameters.AddParameter.Name := 'EAN13';
    sql.Parameters.ParamByName('EAN13').DataType := ftString;
    sql.Parameters.ParamByName('EAN13').value := prod.EAN13;

    sql.Parameters.AddParameter.Name := 'Amount';
    sql.Parameters.ParamByName('Amount').DataType := ftString;
    sql.Parameters.ParamByName('Amount').value := FloatToStr(Kolvo);

    sql.Parameters.AddParameter.Name := 'IsGood';
    sql.Parameters.ParamByName('IsGood').DataType := ftBoolean;
    sql.Parameters.ParamByName('IsGood').value := True;

    sql.Parameters.AddParameter.Name := 'INN';
    sql.Parameters.ParamByName('INN').DataType := ftString;
    sql.Parameters.ParamByName('INN').value := prod.prod.pINN;

    sql.Parameters.AddParameter.Name := 'kpp';
    sql.Parameters.ParamByName('kpp').DataType := ftString;
    sql.Parameters.ParamByName('kpp').value := prod.prod.pKPP;

    sql.ExecSQL;

    NullDecl(decl);
    decl.PK := ShortString(IntToStr(PK));
    decl.SelfKPP := ShortString(KPP1);
    decl.sal := sal;
    decl.product := prod;
    decl.TypeDecl := 6;
    if DBetween(Int(CurDate), dBeg, dEnd) then
    begin
      decl.DeclDate := CurDate;
      decl.RepDate := CurDate;
    end
    else
    begin
      decl.DeclDate := dEnd;
      decl.RepDate := dEnd;
    end;
    decl.Amount := Kolvo;
    decl.Vol := 0.1 * decl.Amount * decl.product.Capacity;
    decl.forma := FormDecl(String(decl.product.FNSCode));
    SLAllDecl.AddObject(IntToStr(PK), TDecl_obj.Create);
    TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;

    inc(PK);

    sql.sql.Clear;
    sql.Parameters.Clear;
    sql.sql.Text :=
      'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclDate, SalerINN, SalerKPP, EAN13, Amount, IsGood, inn, kpp ) '
      + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclDate, :SalerINN, :SalerKPP, :EAN13, :Amount, :IsGood, :inn, :kpp)';

    sql.Parameters.AddParameter.Name := 'PK';
    sql.Parameters.ParamByName('PK').DataType := ftInteger;
    sql.Parameters.ParamByName('PK').value := PK;

    sql.Parameters.AddParameter.Name := 'DeclType';
    sql.Parameters.ParamByName('DeclType').DataType := ftString;
    sql.Parameters.ParamByName('DeclType').value := IntToStr(7);

    sql.Parameters.AddParameter.Name := 'ReportDate';
    sql.Parameters.ParamByName('ReportDate').DataType := ftDate;
    sql.Parameters.ParamByName('ReportDate').value := dEnd;

    sql.Parameters.AddParameter.Name := 'SelfKPP';
    sql.Parameters.ParamByName('SelfKPP').DataType := ftString;
    sql.Parameters.ParamByName('SelfKPP').value := KPP2;

    sql.Parameters.AddParameter.Name := 'DeclDate';
    sql.Parameters.ParamByName('DeclDate').DataType := ftDate;
    sql.Parameters.ParamByName('DeclDate').value := CurDate;

    sql.Parameters.AddParameter.Name := 'SalerINN';
    sql.Parameters.ParamByName('SalerINN').DataType := ftString;
    sql.Parameters.ParamByName('SalerINN').value :=sal.sInn;

    sql.Parameters.AddParameter.Name := 'SalerKPP';
    sql.Parameters.ParamByName('SalerKPP').DataType := ftString;
    sql.Parameters.ParamByName('SalerKPP').value := sal.sKpp;

    sql.Parameters.AddParameter.Name := 'EAN13';
    sql.Parameters.ParamByName('EAN13').DataType := ftString;
    sql.Parameters.ParamByName('EAN13').value := prod.EAN13;

    sql.Parameters.AddParameter.Name := 'Amount';
    sql.Parameters.ParamByName('Amount').DataType := ftString;
    sql.Parameters.ParamByName('Amount').value := FloatToStr
      (Kolvo);

    sql.Parameters.AddParameter.Name := 'IsGood';
    sql.Parameters.ParamByName('IsGood').DataType := ftBoolean;
    sql.Parameters.ParamByName('IsGood').value := True;

    sql.Parameters.AddParameter.Name := 'INN';
    sql.Parameters.ParamByName('INN').DataType := ftString;
    sql.Parameters.ParamByName('INN').value := prod.prod.pInn;

    sql.Parameters.AddParameter.Name := 'kpp';
    sql.Parameters.ParamByName('kpp').DataType := ftString;
    sql.Parameters.ParamByName('kpp').value := prod.prod.pKPP;

    sql.ExecSQL;

    NullDecl(decl);
    decl.PK := ShortString(IntToStr(PK));
    decl.SelfKPP := ShortString(KPP2);
    decl.sal := sal;
    decl.product := prod;
    decl.TypeDecl := 7;
    if DBetween(Int(CurDate), dBeg, dEnd) then
    begin
      decl.DeclDate := CurDate;
      decl.RepDate := CurDate;
    end
    else
    begin
      decl.DeclDate := dEnd;
      decl.RepDate := dEnd;
    end;
    decl.Amount := Kolvo;
    decl.Vol := 0.1 * decl.Amount * decl.product.Capacity;
    decl.forma := FormDecl(String(decl.product.FNSCode));
    SLAllDecl.AddObject(IntToStr(PK), TDecl_obj.Create);
    TDecl_obj(SLAllDecl.Objects[SLAllDecl.Count - 1]).data := decl;
  finally
    sql.Free;
  end;
end;

procedure Shapka_SGJournal(SG:TAdvStringGrid);
begin
  SG.ColCount :=15;
  SG.FixedRows := 4;
  SG.RowCount := SG.FixedRows + 1;
  SG.RowHeights[2]:=60;
  SG.Alignments[0, 0] := taCenter;
  SG.ColWidths[0] := 45;
  SG.Cells[0, 0] := '� �/�';
  SG.MergeCells(0, 0, 1, 3);

  SG.Alignments[0, 3] := taCenter;
  SG.Cells[0, 3] := '1';

  SG.Alignments[1, 0] := taCenter;
  SG.MergeCells(1, 0, 9, 1);
  SG.Cells[1, 0] := '�����������';

  SG.Alignments[1, 1] := taCenter;
  SG.MergeCells(1, 1, 1, 2);
  SG.ColWidths[1] := 225;
  SG.Cells[1, 1] := '��� � ������������ ���������';

  SG.Alignments[1, 3] := taCenter;
  SG.Cells[1, 3] := '2';

  SG.Alignments[2, 1] := taCenter;
  SG.MergeCells(2, 1, 1, 2);
  SG.ColWidths[2] := 50;
  SG.Cells[2, 1] := '��� ���� ���������';

  SG.Alignments[2, 3] := taCenter;
  SG.Cells[2, 3] := '3';

  SG.Alignments[3, 1] := taCenter;
  SG.MergeCells(3, 1, 2, 1);
  SG.Cells[3, 1] := '��������� ���������';

  SG.Alignments[3, 2] := taCenter;
  SG.ColWidths[3] := 100;
  SG.Cells[3, 2] := '������������ �����������';

  SG.Alignments[3, 3] := taCenter;
  SG.Cells[3, 3] := '4';

  SG.Alignments[4, 2] := taCenter;
  SG.Cells[4, 2] := '���';

  SG.Alignments[4, 3] := taCenter;
  SG.Cells[4, 3] := '5';

  SG.Alignments[5, 1] := taCenter;
  SG.MergeCells(5, 1, 4, 1);
  SG.Cells[5, 1] := '���';

  SG.Alignments[5, 2] := taCenter;
  SG.ColWidths[5] := 85;
  SG.Cells[5, 2] := '����';

  SG.Alignments[5, 3] := taCenter;
  SG.Cells[5, 3] := '6';

  SG.Alignments[6, 2] := taCenter;
  SG.Cells[6, 2] := '�����';

  SG.Alignments[6, 3] := taCenter;
  SG.Cells[6, 3] := '7';

  SG.Alignments[7, 2] := taCenter;
  SG.ColWidths[7] := 72;
  SG.Cells[7, 2] := '������� ���� (��������) (�)';

  SG.Alignments[7, 3] := taCenter;
  SG.Cells[7, 3] := '8';

  SG.Alignments[8, 2] := taCenter;
  SG.ColWidths[8] := 72;
  SG.Cells[8, 2] := '���-�� ���� (��������)';

  SG.Alignments[8, 3] := taCenter;
  SG.Cells[8, 3] := '9';

  SG.Alignments[9, 1] := taCenter;
  SG.MergeCells(9, 1, 1, 2);
  SG.ColWidths[9] := 75;
  SG.Cells[9, 1] := '����� ��������� �� �������� ������';

  SG.Alignments[9, 3] := taCenter;
  SG.Cells[9, 3] := '10';

  SG.Alignments[10, 0] := taCenter;
  SG.MergeCells(10, 0, 5, 1);
  SG.Cells[10, 0] := '������';

  SG.Alignments[10, 1] := taCenter;
  SG.MergeCells(10, 1, 1, 2);
  SG.ColWidths[10] := 95;
  SG.Cells[10, 1] := '���������� ������';

  SG.Alignments[10, 3] := taCenter;
  SG.Cells[10, 3] := '11';

  SG.Alignments[11, 1] := taCenter;
  SG.MergeCells(11, 1, 1, 2);
  SG.ColWidths[11] := 225;
  SG.Cells[11, 1] := '��� � ������������ ���������';

  SG.Alignments[11, 3] := taCenter;
  SG.Cells[11, 3] := '12';

  SG.Alignments[12, 1] := taCenter;
  SG.MergeCells(12, 1, 1, 2);
  SG.ColWidths[12] := 80;
  SG.Cells[12, 1] := '������� ���� (��������) (�)';

  SG.Alignments[12, 3] := taCenter;
  SG.Cells[12, 3] := '13';

  SG.Alignments[13, 1] := taCenter;
  SG.MergeCells(13, 1, 1, 2);
  SG.ColWidths[13] := 80;
  SG.Cells[13, 1] := '���-�� ���� (��������) (�)';

  SG.Alignments[13, 3] := taCenter;
  SG.Cells[13, 3] := '14';

  SG.Alignments[14, 1] := taCenter;
  SG.MergeCells(14, 1, 1, 2);
  SG.ColWidths[14] := 80;
  SG.Cells[14, 1] := '����� ������ �� �������� ������';

  SG.Alignments[14, 3] := taCenter;
  SG.Cells[14, 3] := '15';
end;

procedure RashodSmallOst(DBQ:TADOQuery);
var qr, qr1:TADOQuery;
  PK, kv, kv1:Integer;
  qrDate:TDateTime;
  cYear, cMonth, cDay:word;
  prod:TProduction;
  FindProduser:Boolean;
  IsIns:Boolean;
  Vol:Extended;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  qr1:=TADOQuery.Create(DBQ.Owner);
  try
    qr.Connection := DBQ.Connection;
    qr1.Connection := DBQ.Connection;

    qr.ParamCheck := False;
    qr.Parameters.Clear;

    qr1.ParamCheck := False;
    qr1.Parameters.Clear;

    qr.SQL.Text :=
      'SELECT * ' +
      'FROM (SELECT Round(Sum((Amount)*(Switch(DeclType=''1'',1, ' +
                   'DeclType=''2'',-1, DeclType=''3'',-1, ' +
                   'DeclType=''4'',1, DeclType=''5'',-1, '+
                   'DeclType=''6'',-1, DeclType=''7'',1))),5) AS Amount1, ' +
                   'ean13, selfkpp ' +
            'FROM DECLARATION ' +
            'GROUP BY EAN13, selfkpp)  AS AA ' +
      'WHERE AA.Amount1 > 0 and AA.Amount1 < 0.001 ' +
      'ORDER BY AA.Amount1';

    qr.Open;
    if qr.RecordCount = 0 then Exit;

    qr1.sql.Text := 'select MAX(PK) from Declaration';
    qr1.Open;
    PK := qr1.Fields[0].AsInteger + 1;


    DecodeDate(Now() - 20, cYear, cMonth, cDay);
    kv := Kvartal(cMonth);

    DecodeDate(dEnd, cYear, cMonth, cDay);
    kv1 := Kvartal(cMonth);

    if kv = kv1 then
      qrDate := dBeg
    else
      if (kv - kv1) = 1 then
        qrDate := dEnd + 1;


    qr1.sql.Clear;

    qr1.sql.Text :=
      'INSERT INTO Declaration (PK, DeclType, ReportDate, SelfKPP, DeclDate,  EAN13, Amount, IsGood, inn, kpp ) '
      + 'VALUES (:PK, :DeclType, :ReportDate, :SelfKPP, :DeclDate, :EAN13, :Amount, :IsGood, :inn, :kpp)';

    qr.First;
    while not qr.Eof do
    begin
      IsIns := False;
      qr1.Parameters.Clear;

      qr1.Parameters.AddParameter.Name := 'PK';
      qr1.Parameters.ParamByName('PK').DataType := ftInteger;
      qr1.Parameters.ParamByName('PK').value := PK;

      qr1.Parameters.AddParameter.Name := 'DeclType';
      qr1.Parameters.ParamByName('DeclType').DataType := ftString;
      qr1.Parameters.ParamByName('DeclType').value := IntToStr(2);

      qr1.Parameters.AddParameter.Name := 'ReportDate';
      qr1.Parameters.ParamByName('ReportDate').DataType := ftDate;
      qr1.Parameters.ParamByName('ReportDate').value := qrDate-1;

      qr1.Parameters.AddParameter.Name := 'SelfKPP';
      qr1.Parameters.ParamByName('SelfKPP').DataType := ftString;
      qr1.Parameters.ParamByName('SelfKPP').value := qr.FieldByName('SelfKpp').AsString;

      qr1.Parameters.AddParameter.Name := 'DeclDate';
      qr1.Parameters.ParamByName('DeclDate').DataType := ftDate;
      qr1.Parameters.ParamByName('DeclDate').value := qrDate-1;

      NullProd(prod);
      prod.EAN13 := ShortString(qr.FieldByName('EAN13').AsString);
      GetProduction(fmWork.que2_sql, prod, FindProduser, True);

      qr1.Parameters.AddParameter.Name := 'EAN13';
      qr1.Parameters.ParamByName('EAN13').DataType := ftString;
      qr1.Parameters.ParamByName('EAN13').value := prod.EAN13;

      qr1.Parameters.AddParameter.Name := 'Amount';
      qr1.Parameters.ParamByName('Amount').DataType := ftString;
      qr1.Parameters.ParamByName('Amount').value := qr.FieldByName('Amount1').AsString;

      Vol := 0.1 * qr.FieldByName('Amount1').AsFloat * prod.Capacity;
//      Vol := SimpleRoundTo(Vol, -ZnFld);
      Vol:=OkruglDataDecl(Vol);
      if Vol = 0 then IsIns := True;


      qr1.Parameters.AddParameter.Name := 'IsGood';
      qr1.Parameters.ParamByName('IsGood').DataType := ftBoolean;
      qr1.Parameters.ParamByName('IsGood').value := True;

      qr1.Parameters.AddParameter.Name := 'INN';
      qr1.Parameters.ParamByName('INN').DataType := ftString;
      qr1.Parameters.ParamByName('INN').value := prod.prod.pInn;

      qr1.Parameters.AddParameter.Name := 'kpp';
      qr1.Parameters.ParamByName('kpp').DataType := ftString;
      qr1.Parameters.ParamByName('kpp').value := prod.prod.pKpp;

      if IsIns then
      begin
        qr1.ExecSQL;
        Inc(PK);
      end;

      qr.Next;
    end;
  finally
    qr1.Free;
    qr.Free;
  end;
end;

procedure GetFNSOst(Sg:TAdvStringGrid);
var i, Row, idx:Integer;
  mov:TMove;
  FNS:string;
  SL:TStringList;
begin
  SL:=TStringList.Create;
  try
    for i := 0 to SLOborotKpp.Count - 1 do
    begin
      mov:=TMove(SLOborotKpp.Objects[i]);
      FNS:= String(mov.FNS);
      if FNS <> '' then
        if SL.IndexOfObject(Pointer(StrToInt(FNS))) = -1 then
        begin
          idx := SLFNS.IndexOfObject(Pointer(StrToInt(FNS)));
          if idx <> -1 then
            SL.AddObject(SLFNS.Strings[idx], Pointer(StrToInt(FNS)));
        end;
    end;

    if SL.Count = 0 then
    begin
      SG.RowCount := SG.FixedRows + SG.FixedFooters + 1;
      Exit;
    end;
    SG.ClearRows(SG.FixedRows, SG.RowCount - SG.FixedRows);
    SG.RowCount := SG.FixedRows + SG.FixedFooters + SL.Count;
    SL.CustomSort(CompNumObjAsc);

    for i := 0 to SL.Count - 1 do
    begin
      Row:=i+SG.FixedRows;
      FNS := IntToStr(LongInt(SL.Objects[i]));
      SG.Cells[1,Row]:=FNS;
      SG.Cells[2,Row]:=SL.Strings[i];
    end;
  finally
    SL.Free;
  end;
end;

procedure CreateInventTable(DBQ:TADOQuery);
var qr:TADOQuery;
  SL:TStringList;
begin
  qr:=TADOQuery.Create(DBQ.Owner);
  try
    qr.Connection := DBQ.Connection;
    SL:= TStringList.Create;
    qr.ParamCheck := False;
    qr.Parameters.Clear;


    fmWork.con1ADOCon.GetTableNames(SL,False);

    //�������� ������������� ������� ��� ��������������
    fmWork.con1ADOCon.GetTableNames(SL,False);

    if SL.IndexOf('Invent') <> -1 then Exit;

    qr.sql.Text :=
      'CREATE TABLE Invent (InvDate DateTime, SelfKPP VarChar(9), ' +
      'EAN13 VarChar(13), Amount numeric(10,2))';
    qr.ExecSQL;

  finally
    qr.Free;
    SL.Free;
  end;
end;

function CheckErrOst(SKpp, EAN13:String):integer;
var i:Integer;
  mv:TMove;
begin
  Result:= -1;
  for i := 0 to SLOborotKpp.Count - 1 do
    begin
      mv := TMove(SLOborotKpp.Objects[i]);
      if (mv.product.EAN13 = ShortString(EAN13)) and
         (mv.SelfKpp = ShortString(SKpp)) then
        if mv.dv.ost1 < 0 then
          Result := 5;

    end;

end;

procedure SaveProcOst(Ost, form:Integer);
var fn:string;
  tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  try
    case form of
      11: begin
        ProcOst_11:=Ost;
        tIniF.WriteInteger(SectionName,'ProcOst_11',ProcOst_11);
      end;
      12: begin
        ProcOst_12:=Ost;
        tIniF.WriteInteger(SectionName,'ProcOst_12',ProcOst_12);
      end;
    end;
  finally
    tIniF.Free;
  end;
end;

procedure FillFNSOst;
var s:string;
  i, fd:integer;
begin
  for i := 1 to High(FNSCode) do
  begin
    s := FNSCode[i];
    s := Copy(s,0,3);
    fd:=FormDecl(s);
    case fd of
      11: SLProcOst.Values[s] := IntToStr(ProcOst_11);
      12: SLProcOst.Values[s] := IntToStr(ProcOst_12);
    end;
  end;
end;

procedure CreateJournalSG(jBeg, jEnd:TDateTime;SG:TAdvStringGrid;KPP:string);
var decl : TDeclaration;
  i, Row:Integer;
  SLJournal:TStringList;
begin
  SortSLCol(7, SLAllDecl, CompDateAsc);
  SLJournal := TStringList.Create;
  try

    for i := 0 to SLAllDecl.Count - 1 do
    begin
      decl := TDecl_obj(SLAllDecl.Objects[i]).Data;
      if DBetween(Int(decl.DeclDate), jBeg, jEnd) then
        if decl.SelfKpp = ShortString(KPP) then
          if (decl.sal.sId = '') or (decl.sal.sInn <> '1234567894') then
          begin
            SLJournal.AddObject(String(decl.pk), TDecl_obj.Create);
            TDecl_obj(SLJournal.Objects[SLJournal.Count - 1]).data := decl;
          end;
    end;

    SG.RowCount := SG.FixedRows + SG.FixedFooters + SLJournal.Count;
    SG.ClearNormalCells;
    for i := 0 to SLJournal.Count - 1 do
    begin
      decl := TDecl_obj(SLJournal.Objects[i]).Data;
      Row := i + SG.FixedRows;
      case decl.TypeDecl of
        1: WriteJournalPrihod(SG, Row, decl);
        2,3,5: WriteJournalRashod(SG, Row, decl);
      end;
    end;

    SG.AutoNumberCol(0);
    SG.Cells[0,SG.RowCount - 1] := IntToStr(SG.RowCount - SG.FixedRows - SG.FixedFooters);
    SG.Floats[9,SG.RowCount - 1] := SG.ColumnSum(8,SG.FixedRows, SG.RowCount -
      SG.FixedFooters - 1);
    SG.Floats[14,SG.RowCount - 1] := SG.ColumnSum(13,SG.FixedRows, SG.RowCount -
      SG.FixedFooters - 1);
  finally
    FreeStringList(SLJournal);
  end;
end;

procedure WriteJournalPrihod(SG:TAdvStringGrid; Row:Integer; decl:TDeclaration);
begin
  SG.Cells[1,Row] := String(decl.product.Productname);
  SG.Cells[2,Row] := String(decl.product.FNSCode);
  SG.Cells[3,Row] := String(decl.sal.sName);
  SG.Cells[4,Row] := String(decl.sal.sInn);
  SG.Cells[5,Row] := DateToStr(decl.DeclDate);
  SG.Cells[6,Row] := String(decl.DeclNum);
  SG.Cells[7,Row] := FloatToStr(decl.product.Capacity);
  SG.Floats[8,Row] := decl.Amount;
end;

procedure WriteJournalRashod(SG:TAdvStringGrid; Row:Integer; decl:TDeclaration);
begin
  case decl.TypeDecl of
    2: SG.Cells[10,Row] := '����������';
    3: SG.Cells[10,Row] := '������� ����������';
    5: SG.Cells[10,Row] := '����';
  end;
  SG.Cells[11,Row] := String(decl.product.Productname);
  SG.Floats[12,Row] := decl.product.Capacity;
  SG.Floats[13,Row] := decl.Amount;
end;

procedure WriteCheckKpp;
var
  tIniF: TIniFile;
  fn, s: string;
  c:Char;
  i, idx:Integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.Inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    s:=tIniF.ReadString(SectionName, 'CheckKppJourn', s);
    if Length(s) < SLKPP.Count then
      for i := 0 to SLKPP.Count - 1 do
        s := s + '1';
    for i := 1 to Length(s) do
    begin
      c:=s[i];
      idx := ord(c)-ord('0');
      SLCheckKPP.Add(idx);
    end;

    tIniF.WriteString(SectionName, 'CheckKppJourn', s);
  finally
    tIniF.Free;
  end;
end;

procedure ImportJournalKPP(sKvart, sYear:Integer;Path:string; SG:TAdvStringGrid; Kpp:string);
var r: TResourceStream;
  ms : TMemoryStream;
  NewPath:string;
begin
  xls:= TXLSFile.Create;
  try
    xls.Clear;
    r := TResourceStream.Create(HInstance, 'res_journal', RT_RCDATA);
    ms := TMemoryStream.Create;

    NewPath := Path + '\Journal_' + fOrg.Inn;
    if not DirectoryExists(NewPath) then
      MkDir(NewPath);
    if SLKPP.Count > 1 then
    begin
      NewPath := NewPath + '\0' + IntToStr(sKvart) + IntToStr(sYear);
      if not DirectoryExists(NewPath) then
        MkDir(NewPath);
    end;

    ms.LoadFromStream(r);
    if SLKPP.Count > 1 then
      NewPath := NewPath+'\journal_'+ KPP+'_0' + IntToStr(sKvart) + IntToStr(CurYear) +'.xls'
    else
      NewPath := NewPath+'\journal_0' + IntToStr(sKvart) + IntToStr(CurYear) +'.xls';
    ms.SaveToFile(NewPath);
    SaveXLSJournal(xls,NewPath, SG);
  finally
    r.free;
    ms.Free;
    xls.Destroy;
  end;
end;

procedure SaveXLSJournal(xls:TXLSFile; fPath:string; SG:TAdvStringGrid);
var
  XL, ArrayData, Workbook, Cell1, Cell2, Range, Sheet: Variant;
  rez: HRESULT;
  i, j, BeginColSG, BeginRowSG,ColCnt, RowCnt, ColStart,ColEnd,RowStart,
    RowEnd: integer;
  ClassID: TCLSID;
begin
  rez := CLSIDFromProgID(PWideChar(WideString('Excel.Application')), ClassID);
  // ���� CLSID OLE-�������
  if rez = S_OK then
  try
    try
      BeginColSG := 0; // ���������� ������ �������� ���� �������, � ������� ����� �������� ������
      BeginRowSG := 4;
      XL := CreateOleObject('Excel.Application'); // �������� Excel
      XL.DisplayAlerts := False;
      XL.Application.EnableEvents := False; // ��������� ������� Excel �� �������, ����� �������� ����� ����������
      Workbook := XL.WorkBooks.Add(fPath);
      ColCnt := SG.ColCount;
      RowCnt := SG.RowCount;

//      XL.WorkBooks[1].WorkSheets[1].Name := '������ ��';
      ArrayData := VarArrayCreate([BeginRowSG+1, RowCnt+1,
        BeginColSG+1, ColCnt+1], varVariant);

      for j := BeginRowSG to RowCnt do
        for i:= BeginColSG to ColCnt do
          case i of
            7,8,9,12,13,14:
              if SG.Cells[i, j] <> '' then
                ArrayData[j+1, i+1] := SG.Floats[i, j]
              else
                ArrayData[j+1, i+1] := SG.Cells[i, j];
            else  ArrayData[j+1,i+1] := SG.Cells[i, j];
          end;

      Sheet := XL.ActiveWorkBook.Sheets[1];
      RowStart := BeginRowSG + 1;
      RowEnd := RowCnt + 1;

      ColStart := 1;
      ColEnd := 7;
      Sheet.Range[Sheet.Cells[RowStart, ColStart],
        Sheet.Cells[RowEnd, ColEnd]].NumberFormat := '';

      ColStart := 8;
      ColEnd := 10;
      Sheet.Range[Sheet.Cells[RowStart, ColStart],
        Sheet.Cells[RowEnd, ColEnd]].NumberFormat :='0' + {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator + '000';

      ColStart := 11;
      ColEnd := 12;
      Sheet.Range[Sheet.Cells[RowStart, ColStart],
        Sheet.Cells[RowEnd, ColEnd]].NumberFormat := '';

      ColStart := 13;
      ColEnd := 15;
      Sheet.Range[Sheet.Cells[RowStart, ColStart],
        Sheet.Cells[RowEnd, ColEnd]].NumberFormat :='0' + {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator + '000';

      Cell1 := Workbook.WorkSheets[1].Cells[BeginRowSg + 1, BeginColSG+1]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[RowCnt + 1,  ColCnt + 1]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      // �������, � ������� ����� �������� ������
      Range.value := ArrayData; // � ��� � ��� ����� ������. ������� ������� ����������� ����������

      Cell1 := Workbook.WorkSheets[1].Cells[RowStart, 10]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[RowEnd-2, 10]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Merge;

      Cell1 := Workbook.WorkSheets[1].Cells[RowStart, 15]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[RowEnd-2, 15]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Merge;

      Workbook.WorkSheets[1].Cells[RowEnd-1,1].value := '';

      Cell1 := Workbook.WorkSheets[1].Cells[RowEnd-1, 1]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[RowEnd-1, 9]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Merge;

      Cell1 := Workbook.WorkSheets[1].Cells[RowEnd-1, 11]; // ����� ������� ������ �������, � ������� ����� �������� ������
      Cell2 := Workbook.WorkSheets[1].Cells[RowEnd-1, 14]; // ������ ������ ������ �������, � ������� ����� �������� ������
      Range := Workbook.WorkSheets[1].Range[Cell1, Cell2];
      Range.Merge;

      ColStart := 1;
      Sheet.Range[Sheet.Cells[RowStart, ColStart], Sheet.Cells[RowCnt, ColCnt]]
        .Borders.Weight := 2;
      Sheet.Range[Sheet.Cells[RowStart, 1], Sheet.Cells[RowCnt, 1]]
        .HorizontalAlignment := xlCenter;
      Sheet.Range[Sheet.Cells[RowStart, 3], Sheet.Cells[RowCnt, 3]]
        .HorizontalAlignment := xlCenter;
      Sheet.Range[Sheet.Cells[RowStart, 8], Sheet.Cells[RowCnt, 10]]
        .HorizontalAlignment := xlCenter;
      Sheet.Range[Sheet.Cells[RowStart, 13], Sheet.Cells[RowCnt, 15]]
        .HorizontalAlignment := xlCenter;
      XL.ActiveWorkBook.SaveAs(fPath);
      Screen.Cursor := crDefault;
    except
      XL.DisplayAlerts := False;
      XL.ActiveWorkBook.Close;
      XL.Application.Quit;
      Sheet := UnAssigned;
      XL := UnAssigned;
    end;
  finally
    XL.Visible := False;
    XL.DisplayAlerts := False;
    XL.ActiveWorkBook.Close;
    XL.Application.Quit;
    Sheet := UnAssigned;
    XL := UnAssigned;
  end;
end;


function ShowDataOst(SG: TADVStringGrid; NoRes, CalcCol, HideCol: array of integer): integer;
begin
//  DvizenieKppEan(SLO,SLAllDecl,dBeg, dEnd);
  //���������� �����
  ShowOst(SG,SLOborotKpp,NameOstRash,NoRes, CalcCol, HideCol);
end;

procedure ShowOst(SG:TAdvStringGrid;SL:TStringList;
  NameCol,NoRes, CalcCol, HideCol: array of integer);
var
  i: integer;
  SL_:TStringList;

begin
  SG.FilterActive := False;
  SG.ClearAll;
  SG.RowCount := SG.FixedRows + SG.FixedFooters;
  ShapkaSG(SG, NameCol);
  if SL.Count = 0 then
    exit;

  SL_:= TStringList.Create;
  try
    for i := 0 to SL.Count - 1 do
      if TMove(SL.Objects[i]) <> nil then
        if TMove(SL.Objects[i]).dv.ost1 > 0 then
          SL_.AddObject(SL.Strings[i],SL.Objects[i]);

    SortSLCol(1, SL_);
    SG.RowCount := SG.FixedRows + SG.FixedFooters + +SL_.Count;
    EnabledAllRowSG(SG, True);
    SG.Font.Style := [];

    for i := 0 to SL_.Count - 1 do
    begin
      if TMove(SL.Objects[i]) <> nil then
      begin
        SG.Cells[1, i + SG.FixedRows] := String(TMove(SL_.Objects[i]).SelfKPP);
        SG.Cells[2, i + SG.FixedRows] := String
          (TMove(SL_.Objects[i]).product.FNSCode);
        SG.Cells[3, i + SG.FixedRows] := String
          (TMove(SL_.Objects[i]).product.EAN13);
        SG.Cells[4, i + SG.FixedRows] := String
          (TMove(SL_.Objects[i]).product.ProductNameF);
        SG.Floats[5, i + SG.FixedRows] := TMove(SL_.Objects[i]).product.Capacity;
        SG.Floats[6, i + SG.FixedRows] := TMove(SL_.Objects[i]).product.AlcVol;
        SG.Floats[7, i + SG.FixedRows] := TMove(SL_.Objects[i]).dv.ost0;
        SG.Floats[8, i + SG.FixedRows] := TMove(SL_.Objects[i]).dv.prih;
        SG.Floats[9, i + SG.FixedRows] := TMove(SL_.Objects[i]).dv.vozv_pokup
          - TMove(SL_.Objects[i]).dv.vozv_post
          - TMove(SL_.Objects[i]).dv.brak
          - TMove(SL_.Objects[i]).dv.move_out
          + TMove(SL_.Objects[i]).dv.move_in;
        SG.Floats[10, i + SG.FixedRows] := TMove(SL_.Objects[i]).dv.rash;
        SG.Floats[11, i + SG.FixedRows] := 0;
        SG.Floats[12, i + SG.FixedRows] := TMove(SL_.Objects[i]).dv.ost1;
      end;
    end;
    SG.Refresh;
    PropCellsSG(arNormal, SG, CalcCol, NoRes, HideCol);
    SG.FilterActive := True;
  finally
    SL_.Free;
  end;


end;


procedure CreateRashFromOst(SG:TAdvStringGrid;SLOst:TStringList);
var i, idx, Row:Integer;
  sEAN, sKpp, s:string;
  ost, rash:Extended;
  mov : TMove;
  prod:TProduction;
  f1 :Boolean;
  decl:TDeclaration;
  y,m,d:Word;
  dat:TDateTime;
begin
  if SLOst.Count = 0 then Exit;

  Row := SG.RowCount-1;
  SG.RowCount := SG.RowCount + SLOst.Count;
  fmMove.CountNew := SLOst.Count;

  for i := 0 to SLOst.Count - 1 do
  begin
    s := SLOst.Names[i];
    idx := SLOborotKpp.IndexOf(s);
    if idx <> -1 then
    begin

      sKpp := Copy(s,1,9);
      sEAN := Copy(s,10,13);
      ost := StrToFloat(SLOst.Values[SLOst.Names[i]]);
      Mov := Pointer(SLOborotKpp.Objects[idx]);
      if mov <> nil then
      begin
        rash := mov.dv.ost0 + mov.dv.prih - mov.dv.rash - mov.dv.vozv_post +
          mov.dv.vozv_pokup - mov.dv.brak + mov.dv.move_in - mov.dv.move_out -
          ost;
        prod.EAN13 := sEAN;
        GetProduction(fmWork.que2_sql, prod, f1, True);

        NullDecl(decl);
        decl.Amount := rash;
        decl.product := prod;

        decl.forma := FormDecl(String(prod.FNSCode));
        decl.SelfKPP := sKpp;
        decl.RepDate := dEnd;
        decl.TypeDecl:= 2;

        DecodeDate(Now,y,m,d);
        dat := DateOf(Kvart_End(CurKvart, NowYear));

        if (DateOf(fmMove.dtp_WorkDate.Date) <= dEnd) and
          (DateOf(fmMove.dtp_WorkDate.Date) >= dBeg) then
          decl.DeclDate := fmMove.dtp_WorkDate.Date
        else
          decl.DeclDate := dEnd;

        decl.vol := 0.1 * decl.product.capacity * decl.Amount;
//        decl.vol := RoundTo(decl.vol, -ZnFld);
        decl.vol := OkruglDataDecl(decl.vol);

        WriteRowRashodSG(SLErrRash, SLOborotKpp, decl, SG, row + i);
      end;
    end;
  end;
  SG.Row := SG.RowCount - SG.FixedFooters -1 ;
end;


function ExistFilesDeclFTP(pathFTP, pasw, Inn:string):Integer;
var
  idftp: TIdFTP;
  dirName:string;
  i:integer;
begin
  Result:=1;
  idftp:=TIdFTP.Create(nil);
  try
    try
      IdFTP.Host:='SoftAlco.space';// ftp ����� �������
      IdFTP.Username:='0-'+INN;//�����
      IdFTP.Password:=pasw;//������
      IdFTP.Passive := True;
      IdFTP.TransferType := ftBinary;
      try
        IdFTP.Connect;
      except
        Result :=0;
        Exit;
      end;

      try
        idftp.ChangeDir(pathFTP);
        try
          idFTP.List;
        except
        end;
        if IdFTP.DirectoryListing.Count = 0 then
        begin
          exit;
        end;

        for i:=0 to IdFTP.DirectoryListing.Count-1 do
        if IdFTP.DirectoryListing.Items[i].ItemType = ditDirectory then
        begin
          dirName := IdFTP.DirectoryListing.Items[i].FileName;
          if UpperCase(dirName) = 'DECL' then //�������� ������� ����������
          begin
            Result:=-1;
            exit;
          end;
        end;
      except
        Result :=1;
        exit;
      end;

    finally
      if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
    end;
  except
  end;
end;

function LoadFilesDecl(sInn,psw, path:string; iKvart, iYear:integer):integer;
var
  s1,s2,dir_path, adr_ftp, dirName, dirDate, fR1, fR2:string;
  idftp: TIdFTP;
  i, idxR1, idxR2:integer;
  SLDate:TStringList;

  SLr1, SLr2:TStringList;

  function MaxDateDecl(SL:TStringList):Integer;
  var i:Integer;
    SLDate: TStringList;
    d:string;
  begin
    SLDate:=TStringList.Create;
    try
      for i:= 0 to SL.Count - 1 do
      begin
        d:= SL.Values[SL.Names[i]];
        SLDate.Add(d);
      end;
      SLDate.CustomSort(CompDateDesc);
      d:= SLDate[0];
      for i := 0 to SL.Count - 1 do
        if SL.Values[SL.Names[i]] = d then
        begin
          Result := i;
          Exit;
        end;
    finally
      SLDate.Free;
    end;
  end;
begin
  Result := 0;

  adr_ftp:='/0'+IntToStr(iKvart)+IntToStr(iYear);
  dir_path := path + '\DECL_' + fOrg.SelfName + '_0' + IntToStr(iKvart) + IntToStr(iYear);
  dir_path:=DeleteSimv('"',dir_path);

  idftp:=TIdFTP.Create(nil);
  SLDate:=TStringList.Create;
  try
    try
      IdFTP.Host:='SoftAlco.space';// ftp ����� �������
      IdFTP.Username:='0-'+sInn;//�����
      IdFTP.Password:=psw;//������
      IdFTP.Passive := True;
      IdFTP.TransferType := ftBinary;
      IdFTP.Connect;
      try
        idftp.ChangeDir(adr_ftp);
        if not DirectoryExists(dir_path) then
          MkDir(dir_path);
      except
      end;
      try
        idFTP.List;
      except
      end;
      SLr1:=TStringList.Create;
      SLr2:=TStringList.Create;
      try
        idftp.ChangeDir('Decl');
        try
          idFTP.List;
        except
        end;
        //�������� � ������� ������������ ����� � ���� ��������
        for i:=0 to IdFTP.DirectoryListing.Count-1 do
          if IdFTP.DirectoryListing.Items[i].ItemType = ditFile then
          begin
            //������������� ��������/�����
            dirName := IdFTP.DirectoryListing.Items[i].FileName;
            dirDate := DateTimeToStr(IdFTP.DirectoryListing.Items[i].ModifiedDate);
            if Pos('R1_',dirName) > 0 then
              SLr1.Add(dirName + '=' + dirDate);
            if Pos('R2_',dirName) > 0 then
              SLr2.Add(dirName + '=' + dirDate);
          end;
        idxR1 := -1;  idxR2 := -1;
        //��������� ������ ������� � ���������� �����
        if SLr1.Count > 0 then
          idxR1 := MaxDateDecl(SLr1);
        if SLr2.Count > 0 then
          idxR2 := MaxDateDecl(SLr2);

        fR1:=''; fR2 := '';
        if idxR1 <> -1 then
          fR1 := SLr1.Names[idxR1];
        if idxR2 <> -1 then
          fR2 := SLr2.Names[idxR2];

        for i:=0 to IdFTP.DirectoryListing.Count-1 do
        begin
          //������������� ��������/�����
          dirName := IdFTP.DirectoryListing.Items[i].FileName;
          dirDate := DateTimeToStr(IdFTP.DirectoryListing.Items[i].ModifiedDate);

          s1:= dir_path + '\'+ idFTP.DirectoryListing.Items[i].FileName;

          if (dirName = fR1) or (dirName = fR2) then
          begin
            idftp.get(IdFTP.DirectoryListing.Items[i].FileName,s1, True, False);

            UnZip(s1, s2);
            s1 := Copy(s1,0,Length(s1)-4);
            s2 := s2+'\' + Copy(dirName,0,Length(dirName)-4);
            if MoveFile(PWideChar(s2),PWideChar(s1)) then
            begin
              DeleteFile(PWideChar(s1+'.zip'));
              DeleteFile(PWideChar(s2+'.zip'));
            end;
            Inc(Result);
          end;
        end;
      finally
        SLr2.Free;
        SLr1.Free
      end;
    finally
      if Assigned(idftp) then
      begin
        idftp.Disconnect;
        idftp.Free;
      end;
      SLDate.Free;
    end;
  except
  end;
end;

procedure GetMinusOst(var sErr:array of integer; SLOst:TStringList);
var i:integer;
  mov:TMove;
begin
  sErr[5]:=0;
  sErr[6]:=0;
  for i := 0 to SLOstatkiKpp.Count - 1 do
  begin
    mov:=TMove(SLOstatkiKpp.Objects[i]);
    if mov.dv.ost0 < 0 then
      Inc(sErr[5]);
    if mov.dv.ost1 < 0 then
      Inc(sErr[6]);
  end;
end;

procedure ShowRefProducer(SG: TADVStringGrid; DBQ: TADOQuery);
var
  i: integer;
  qr: TADOQuery;
  StrSQL: string;
begin
  qr := TADOQuery.Create(DBQ.Owner);
  SG.ClearAll;
  ShapkaSG(SG, NameColProducerRef);
  try
    try
      qr.Connection := DBQ.Connection;
      qr.ParamCheck := False;
      StrSQL := 'SELECT Producer.FullName, Producer.INN, Producer.KPP, ' +
        'Producer.countrycode, Producer.RegionCode, Producer.Address ' +
        'FROM Producer ' +
        'WHERE (((Producer.LocRef)=True)) ' +
        'order by inn, kpp';
      qr.sql.Text := StrSQL;
      qr.Open;
      if qr.RecordCount > 0 then
      begin
        SG.RowCount := SG.FixedRows + SG.FixedFooters + qr.RecordCount;
        qr.First;
        i := 0;
        while not qr.Eof do
        begin
          SG.Cells[1, i + SG.FixedRows] := qr.FieldByName('INN').AsString;
          SG.Cells[2, i + SG.FixedRows] := qr.FieldByName('KPP').AsString;
          SG.Cells[3, i + SG.FixedRows] := qr.FieldByName('FullName').AsString;
          SG.Cells[4, i + SG.FixedRows] := qr.FieldByName('RegionCode').AsString;
          SG.Cells[5, i + SG.FixedRows] := qr.FieldByName('Address').AsString;
          qr.Next;
          inc(i);
        end;
      end;

    except

    end;
  finally
    qr.free;
  end;

  SG.AutoNumberOffset := 0;
  SG.AutoNumberStart := 0;

  SG.AutoNumberCol(0);

  SG.ColumnSize.Rows := arNormal;
  SG.NoDefaultDraw := True;
  SG.AutoSize := True;

  SG.CalcFooter(0);
  SG.ColWidths[3] := Length(SG.ColumnHeaders.Strings[3]) * 10;
//  SG.ColWidths[7] := Length(SG.ColumnHeaders.Strings[7]) * 10;
end;

procedure CorrectSmallOst(DBQ:TADOQuery);
var qr:TADOQuery;
  IniF:TIniFile;
  fn:string;
  idx:Integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
      + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  try
    IniF := TIniFile.Create(fn);

    qr:=TADOQuery.Create(DBQ.Owner);

    idx := IniF.ReadInteger(SectionName, 'CorSmalOst', 0);
    if idx = 1 then Exit;


    qr.Connection := DBQ.Connection;

    qr.ParamCheck := False;
    qr.Parameters.Clear;

    qr.SQL.Text :=
      'DELETE FROM Declaration ' +
      'WHERE ((AMOUNT < "0,001") AND (ReportDate=DateValue(''30.09.2014'')) ' +
         'AND (SalerINN Is Null) AND (DeclType=''2''))';

    qr.ExecSQL;
    IniF.WriteInteger(SectionName, 'CorSmalOst', 1);
  finally
    IniF.Free;
    qr.Free;
  end;
end;

procedure SetColorSertMain(leDE, leINN, leMain:TLabeledEdit);
var dEndOtch:TDate;
begin
  dEndOtch := GetDateEndOtch(dEnd);
  if fSert.dEnd <= dEndOtch then
  begin
    leDE.Font.Color := clErr;
    leDE.Font.Style := [fsBold];
    leMain.Visible := True;
    leMain.Text := DateToStr(fSert.dEnd);
  end
  else
  begin
    leDE.Font.Style := [];
    leDE.Font.Color := clNormal;
    leMain.Visible := False;
  end;

  if fOrg.Inn <> fSert.INN then
  begin
    leINN.Font.Color := clErr;
    leINN.Font.Style := [fsBold];
  end
  else
  begin
    leINN.Font.Style := [];
    leINN.Font.Color := clNormal;
  end;
end;

procedure SetColorSertSet(leDE, leINN:TLabeledEdit);
var dEndOtch:TDate;
begin
  dEndOtch := GetDateEndOtch(dEnd);
  if fSert.dEnd <= dEndOtch then
  begin
    leDE.Font.Color := clErr;
    leDE.Font.Style := [fsBold];
  end
  else
  begin
    leDE.Font.Style := [];
    leDE.Font.Color := clNormal;
  end;

  if fOrg.Inn <> fSert.INN then
  begin
    leINN.Font.Color := clErr;
    leINN.Font.Style := [fsBold];
  end
  else
  begin
    leINN.Font.Style := [];
    leINN.Font.Color := clNormal;
  end;
end;

function SaveShOst(var fName:string):Boolean;
var r: TResourceStream;
  ms : TMemoryStream;
  BrowseInfo: TBrowseInfo;
  DisplayName, Path, TempPath: array [0 .. MAX_PATH] of char;
  TitleName, NewPath:string;
  lpItemID: PItemIDList;
begin
  Result:=false;
  fName := '';
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  BrowseInfo.hwndOwner := fmWork.Handle;
  BrowseInfo.pszDisplayName := @DisplayName;
  TitleName := '�������� ����� ��� ���������� ������� ��������';
  BrowseInfo.lpszTitle := PChar(TitleName);
  BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemID <> nil then
  begin
    SHGetPathFromIDList(lpItemID, TempPath);
    GlobalFreePtr(lpItemID);
    Path := TempPath;
    r := TResourceStream.Create(HInstance, 'shOst', RT_RCDATA);
    ms := TMemoryStream.Create;
    try
      ms.LoadFromStream(r);
      NewPath := Path+'\shOst.xls';
      ms.SaveToFile(NewPath);
      Result := True;
      fName := NewPath;
    finally
      ms.Free;
      r.Free;
    end;
  end;
end;

function LoadShOst(var fName:string; SG:TAdvStringGrid; var cOst:integer):Boolean;
var rez: HRESULT;
  XL, Workbook, ArrayData, Cell1, Cell2, Range, Sheet: Variant;
  ClassID: TCLSID;
  Row:Integer;
  SL, SLIns, SLNo:TStringList;
  decl:TDeclaration;
  s:string;
  FindProduser:Boolean;
  idx, i:Integer;
  tmp,Rash, Ost:Extended;
begin
  Result:=False;
  rez := CLSIDFromProgID(PWideChar(WideString('Excel.Application')), ClassID);
  SL:=TStringList.Create;
  SLIns:=TStringList.Create;
  SLNo:=TStringList.Create;
  cOst:=0;
  Screen.Cursor := crHourGlass;
  // ���� CLSID OLE-�������
  if rez = S_OK then
  try
    try
      XL := CreateOleObject('Excel.Application'); // �������� Excel
      XL.DisplayAlerts := False;
      XL.Application.EnableEvents := False; // ��������� ������� Excel �� �������, ����� �������� ����� ����������
      Workbook := XL.WorkBooks.Open(fName);
      Sheet := XL.ActiveWorkBook.Sheets[1];

      Row:=2;
      s := Sheet.Cells[Row, 2];
      while s <> '' do
      begin
        NullDecl(decl);

        decl.SelfKpp := Sheet.Cells[Row, 1];
        decl.product.EAN13 := Sheet.Cells[Row, 2];
        decl.Amount := StrInFloat(Sheet.Cells[Row, 3]);
        decl.TypeDecl := 2;
        GetProduction(fmWork.que2_sql, decl.product, FindProduser, True);

        SL.AddObject(decl.SelfKpp + decl.product.EAN13, TDecl_obj.Create);
        TDecl_obj(SL.Objects[SL.Count - 1]).data := decl;

        Inc(Row);
        s := Sheet.Cells[Row, 2];
      end;

      if SL.Count > 0 then
      begin
        for i := SG.FixedRows to SG.RowCount - 1 do
        begin
          s := SG.Cells[1,i]+SG.Cells[3,i]; // ��� + ���
          idx := SL.IndexOf(s);
          if idx <> -1 then //�������� ��� ���������� �������
            begin
              SLIns.Add(s);
              NullDecl(decl);
              decl := TDecl_obj(SL.Objects[idx]).data;
              Ost := decl.Amount;
              tmp := SG.Floats[12,i];
              Rash := tmp - Ost;
              SG.Floats[12,i]:=Ost;
              SG.Floats[11,i]:=Rash;
            end
          else
            begin
              SG.Floats[11,i]:=SG.Floats[12,i];
              SG.Floats[12,i] := 0;
            end;
        end;

        for i := 0 to SL.Count - 1 do
        begin
          NullDecl(decl);
          decl := TDecl_obj(SL.Objects[i]).data;
          s := decl.SelfKpp + decl.product.EAN13;
          idx := SLIns.IndexOf(s);
          if idx = -1 then
          begin
            s := '��� - ' + decl.SelfKpp + ';  EAN13 - ' + decl.product.EAN13 +
              ';  ������������ ��������� - ' + decl.product.Productname +
              ';  ���-�� ' + FloatToStr(decl.Amount);
            SLNo.Add(s);
          end;
        end;
        Result:=True;
      end;

      SG.CalcFooter(11);
      SG.CalcFooter(12);
      if SLNo.Count > 0 then
        begin
          s:= ExtractFilePath(fName) + 'error_ost.txt';
          SLNo.SaveToFile(s);
          cOst:=SLNo.count;
          fName := s;
        end;
    except
      Screen.Cursor := crDefault;
    end;
  finally
    SLNo.Free;
    SLIns.Free;
    FreeStringList(SL);
    Sheet := UnAssigned;
    XL.UserControl := True; // ������� ���������� ������������
    XL.Quit; // ������� Excel
    XL := UnAssigned;
    FreeAndNil(XL);
    Screen.Cursor := crDefault;
  end;
end;




procedure SaveDBFRef(fN:string; SG:TAdvStringGrid);
begin

end;

procedure SaveXLSRef(fN:string; SG:TAdvStringGrid);
begin

end;

procedure UpdateSimvDB(DBQ:TADOQuery);
var
  StrSQL:string;
begin
  StrSQL:='SELECT PK FROM DECLARATION WHERE AMOUNT LIKE "%.%"';

  DBQ.Close;

  DBQ.ParamCheck := False;
  DBQ.Parameters.Clear;
  DBQ.SQL.Text := StrSQL;
  DBQ.Open;

  if DBQ.RecordCount > 0 then
  begin
    StrSQL := 'UPDATE DECLARATION SET AMOUNT = VAl(AMOUNT) WHERE PK IN (' + StrSQL + ')';
    DBQ.SQL.Text := StrSQL;
    DBQ.ExecSQL;

    StrSQL:='SELECT PK FROM DECLARATION ' + #10#13 +
      'WHERE AMOUNT LIKE "%.%"';

    DBQ.SQL.Text := StrSQL;
    DBQ.Open;

    if DBQ.RecordCount > 0 then
      exit
  end;

end;
end.
