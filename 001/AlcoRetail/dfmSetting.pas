unit dfmSetting;

interface

uses
  Forms, Buttons, StdCtrls, ExtCtrls, Controls, Classes, SysUtils, shellapi,
  Windows, IniFiles, ShlObj, FWZipWriter, Graphics;

type
  TthProcedure = procedure of Object;

  TthCustom = class(TThread)
  private
    FExecProcedure: TthProcedure;
  protected
    procedure Execute; override;
  public
    constructor Create(Suspended: Boolean = True);
    procedure RunIt;
    property ExecProcedure: TthProcedure read FExecProcedure write FExecProcedure;
  end;

  TfmSetting = class(TForm)
    Panel2: TPanel;
    Label6: TLabel;
    Button1: TButton;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel3: TPanel;
    cbKPP: TComboBox;
    Label1: TLabel;
    leINN: TLabeledEdit;
    Label2: TLabel;
    cbReg: TComboBox;
    Panel4: TPanel;
    Panel6: TPanel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    SpeedButton5: TSpeedButton;
    Label4: TLabel;
    GroupBox5: TGroupBox;
    SpeedButton8: TSpeedButton;
    Panel11: TPanel;
    leFIO: TLabeledEdit;
    leNameOrg: TLabeledEdit;
    le_Inn: TLabeledEdit;
    leDateBeg: TLabeledEdit;
    leDateEnd: TLabeledEdit;
    leEmail: TLabeledEdit;
    lePath: TLabeledEdit;
    GroupBox6: TGroupBox;
    Panel15: TPanel;
    SpeedButton9: TSpeedButton;
    SpeedButton11: TSpeedButton;
    btn2: TSpeedButton;
    labUralKom: TLabel;
    pnlTW: TPanel;
    btn5: TSpeedButton;
    pnl1: TPanel;
    btn1: TSpeedButton;
    btn3: TSpeedButton;
    btn4: TSpeedButton;
    btn6: TSpeedButton;
    GB_2: TGroupBox;
    btn7: TSpeedButton;
    btn8: TSpeedButton;
    btn9: TSpeedButton;
    cbMainKpp: TComboBox;
    lb3: TLabel;
    sb1: TSpeedButton;
    btn_ShOst: TSpeedButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure labUralKomClick(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);

    procedure btn9Click(Sender: TObject);
    procedure cbMainKppChange(Sender: TObject);
    procedure sb1Click(Sender: TObject);
    procedure btn_ShOstClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetting: TfmSetting;

implementation

uses dfmWork, Global, GlobalUtils, dfmBD, dfmInfo, dfmSelSert, ABOUT,
  GlobalConst, dfmMove, dfmMain, dfmProgress, dfmDataExchange, Global2;
{$R *.dfm}

procedure TfmSetting.Button1Click(Sender: TObject);
begin
  fmWork.sb_MainClick(Self);
end;

procedure TfmSetting.cbMainKppChange(Sender: TObject);
begin
  if cbMainKpp.Items.Count > 0 then
    if cbMainKpp.ItemIndex <> -1 then
    begin
      fOrg.Kpp0 := ShortString(cbMainKpp.Items.Strings[cbMainKpp.ItemIndex]);
      if SLKpp.Count = 1 then
        fmWork.lbOrg.Caption := String(fOrg.SelfName) + '   ��� ' +  String(fOrg.Inn) + '   ��� ' + String(fOrg.Kpp0)
      else
        fmWork.lbOrg.Caption := String(fOrg.SelfName) + '   ��� ' +  String(fOrg.Inn) + '   �������� ��� ' + String(fOrg.Kpp0);
    end;

end;

procedure TfmSetting.FormCreate(Sender: TObject);
var idx:Integer;
begin
  leINN.Text := fOrg.Inn;
  AssignCBSL(cbKPP, SLKPP);
  AssignCBSL(cbMainKpp, SLKPP);
  idx := cbMainKpp.Items.IndexOf(string(fOrg.Kpp0));
  if idx <> -1 then
    cbMainKpp.ItemIndex := idx;

  if (Length(fOrg.Inn) = 12) or (SLKPP.Count = 1) then
  begin
    cbMainKpp.Visible := False;
    lb3.Visible := False;
  end;

  if cbKPP.Items.Count > 0 then
    cbKPP.ItemIndex := 0;
  AssignCBSL(cbReg, SLPerReg);
  if cbReg.Items.Count > 0 then
    cbReg.ItemIndex := 0;
  if fSert.FIO = '' then
  begin
    lePath.Text := '���������� �� ������';
  end
  else
  begin
    leFIO.Text := fSert.FIO;
    leNameOrg.Text := fSert.OrgName;
    leDateBeg.Text := DateToStr(fSert.dBeg);
    le_Inn.Text := fSert.Inn;
    leDateEnd.Text := DateToStr(fSert.dEnd);

    SetColorSertSet(leDateEnd, le_INN);

    leEmail.Text := fSert.email;
    lePath.Text := fSert.path;
  end;
  leINN.Text := fOrg.Inn;
end;

procedure TfmSetting.labUralKomClick(Sender: TObject);
begin
  ShellExecute(Application.Handle, nil, 'http://www.uralkom.com/', nil, nil,SW_SHOWNOACTIVATE);
end;

procedure TfmSetting.sb1Click(Sender: TObject);
var
  butsel: integer;
  ost, dataOrg:Boolean;
begin
  butsel := Show_Inf('��������� �� XML ������� �� ������ �������?', fConfirm);
  if butsel = mrOk then
    ost:=True
  else
    ost:=False;

  butsel := Show_Inf('��������� �� XML ������ �����������?', fConfirm);
  if butsel = mrOk then
    dataOrg:=True
  else
    dataOrg:=False;

  LoadFullXML(ost, dataOrg);
end;

procedure TfmSetting.btn1Click(Sender: TObject);
var
  butSel: integer;
  s: string;
  b: Boolean;
  v: Boolean;
begin
  try
    butSel := Show_Inf('����������� ����� ���������.' + #10#13 + '����������?',
      fConfirm);
    if butSel = mrOk then
    begin
      fmSetting.Enabled := False;
      b := UpdateRefLocal(True, False, fmWork.que2_sql, fOrg.Inn, s,
        fmWork.pbWork);
      if b then
      begin
        RenameSprav(s, db_spr, fOrg.Inn);
        v := fmWork.pbWork.Visible;
        if not v then
          fmWork.pbWork.Visible := True;
        Upd_LocRef(fmWork.que1_sql, fmWork.que2_sql, fmWork.pbWork,
          AboutBox.lHelp);
        UpdateDateRef;
        ChangeKvart(dBeg, dEnd);
        if fmMove <> nil then
          fmMove.ShowDataAll;
        if fmMain <> nil then
          fmMain.WriteCaptionCount;
        fmWork.pbWork.Visible := v;
        Show_Inf('����������� ������� ���������', fInfo);
      end
      else
        begin
          Show_Inf('����������� �� ���������', fInfo);
          fmWork.pbWork.Visible := v;
          fmSetting.Enabled := True;
        end;
    end;
  finally
    fmSetting.Enabled := True;
  end;
end;

procedure TfmSetting.SpeedButton11Click(Sender: TObject);
begin
  UpdateApp(BMHost, fOrg.inn, exe_version, path_exe);
end;

procedure TfmSetting.SpeedButton1Click(Sender: TObject);
var idx:integer;
begin
  if fmMain <> nil then
    idx := fmMain.cbAllKvart.ItemIndex;

  if RegOnLine(fmWork.que1_sql, SLPerReg) then
    UpdateRegData(idx);
end;

procedure TfmSetting.btn3Click(Sender: TObject);
var ms1, ms2, ms3: TMemoryStream;
    s,s1, fn:string;
    idx, tINN:integer;
    Ini:TIniFile;
    Res:Boolean;
begin
  Res :=False;
  ms1 := TMemoryStream.Create;
  ms2 := TMemoryStream.Create;
  ms3 := TMemoryStream.Create;
  try
    fmWork.dlgOpen_Import.Title := '�������� ���� ����������� �������� ����� :';
    fmWork.dlgOpen_Import.Filter := '����� ����������� (*.nrt)|*.nrt';
    if fmWork.dlgOpen_Import.Execute then
      begin
        if (ExtractFileExt(fmWork.dlgOpen_Import.filename) = '.nrt') then
          begin
            s:=fmWork.dlgOpen_Import.FileName;
            s1:=ExtractFileDir(s);
            s:=Copy(s,Length(s1)+2,Length(s)-Length(s1));
            s1:='OnLine';
            idx:=Pos(UpperCase(s1),UpperCase(s));
            case idx of
              11: tINN :=0;
              13: tINN :=2;
              else tINN:=0;
            end;
            ms1.LoadFromFile(fmWork.dlgOpen_Import.filename);
            ms2.LoadFromStream(ms1);
            //if reg.OpenKey(Key, true) then
            if SetBinaryStream(ms2,ms3) then //�������������� ������ � �������� ������
              begin
                //������ INN ����������� �� ����� �����������
                fOrg.Inn:=ReadInnFileReg(ms1,tINN);
                //������ ����������������� ��� � �������� ����������� �� ������
                ReadKPP_MS(ms2, SLPerReg);
                fn := GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn+'\'+NameProg+'.ini';
                Ini := TIniFile.Create(fn);
                try
                  ms3.Position :=0;
                  Ini.WriteBinaryStream(SectionName,'Registration', ms3);
                finally
                  Ini.free;
                end;
                Res:=True;
              end;
          end
        else
          Show_Inf('������������ ��������� ����� ����������� �������� �����', fError);
      end;
  finally
    ms3.Free;
    ms2.free;
    ms1.Free;
  end;
  if not Res then
  begin
    //Application.Terminate;
    exit;
  end
  else
  begin
    AssignCBSL(cbReg, SLPerReg);
    AssignCBSL(cbKPP, SLKPP);

    if fmMain <> nil then
      begin
        SetMasKvartal;
        AssignCBSL(fmMain.cbAllKvart, SLPerRegYear);
//        fmMain.lb_MainKPP.Caption := fOrg.Kpp0;
//        AssignCBSL(fmMain.cbKPP, SLKPP);
//        if fOrg.Kpp0 = '' then
//          fmMain.cbKPP.ItemIndex:=0
//        else
//          begin
//            idx:=fmMain.cbKPP.Items.IndexOf(String(fOrg.Kpp0));
//            if idx <> -1 then
//              fmMain.cbKPP.ItemIndex := idx;
//          end;
        fmMain.cbAllKvartChange(Application);
    Show_Inf('����������� ��������� �������', fInfo);
  end;
  end;
end;

procedure TfmSetting.btn4Click(Sender: TObject);
begin
  LoadOstXML;
end;

procedure ExecProc(ThreadProcedure: TthProcedure);
begin
  with TthCustom.Create do
  begin
    ExecProcedure := ThreadProcedure;
    RunIt;
  end;
end;

procedure TfmSetting.btn5Click(Sender: TObject);
begin
  ProcDownload;
end;

procedure TfmSetting.btn6Click(Sender: TObject);
var path,f1,f2, fn, TitleName:string;
    lpItemID: PItemIDList;
    TempPath, DisplayName: array [0 .. MAX_PATH] of char;
    Zip: TFWZipWriter;
    BrowseInfo: TBrowseInfo;
begin
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  BrowseInfo.hwndOwner := fmWork.Handle;
  BrowseInfo.pszDisplayName := @DisplayName;
  TitleName := '�������� ����� ��� ���������� ������';
  BrowseInfo.lpszTitle := PChar(TitleName);
  BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemID <> nil then
  begin
    SHGetPathFromIDList(lpItemID, TempPath);
    GlobalFreePtr(lpItemID);
    Path := TempPath;
    Zip := TFWZipWriter.Create;
    try
      fmWork.con1ADOCon.Close;
      //�������� ��������� ����� ����������
      f1:=path_arh+'\Decl_BackUp_'+fOrg.Inn+'_'+DTPart+'.rbk';
      DB2Arh(db_decl,f1);
      Zip.AddFile(f1);

      fmWork.con2ADOCon.Close;
      //�������� ��������� ����� ������������
      f2:=path_arh+'\Refs_BackUp_'+fOrg.Inn+'_'+DTPart+'.rbk';
      DB2Arh(db_spr,f2);
      Zip.AddFile(f2);

      //����������� INI �����
      fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)
        + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
      Zip.AddFile(fn);
      fn:=path+'\Arh_'+fOrg.Inn+'_'+DTPart+'.zip';
      Zip.BuildZip(fn);
      DeleteFile(PWideChar(f1));
      DeleteFile(PWideChar(f2));
      Show_Inf('������ ������� ��������� � ' + fn, fInfo);
    finally
      Zip.free;
    end;
  end;
end;

procedure TfmSetting.btn7Click(Sender: TObject);
var fDecl, fRef, path, fn, fn1:string;
    rez:Boolean;
    DisplayName: array [0 .. MAX_PATH] of char;
    TitleName: string;
    lpItemID: PItemIDList;
    TempPath: array [0 .. MAX_PATH] of char;
    BrowseInfo: TBrowseInfo;
begin
  FillChar(BrowseInfo, sizeof(TBrowseInfo), #0);
  BrowseInfo.hwndOwner := fmWork.Handle;
  BrowseInfo.pszDisplayName := @DisplayName;
  TitleName := '�������� ����� � ������� ��� ��������������';
  BrowseInfo.lpszTitle := PChar(TitleName);
  BrowseInfo.ulFlags := BIF_RETURNONLYFSDIRS;
  lpItemID := SHBrowseForFolder(BrowseInfo);
  if lpItemID <> nil then
  begin
    SHGetPathFromIDList(lpItemID, TempPath);
    GlobalFreePtr(lpItemID);
    Path := TempPath;

    fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)
       + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
    fn1:=GetSpecialPath(CSIDL_PROGRAM_FILES)
       + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '_.ini';
    if FileExists(fn1) then DeleteFile(PWideChar(fn));
    if FileExists(fn) then MoveFile(PChar(fn), PChar(fn1));

    CopyFile(PChar(path+'\'+NameProg+'.ini'),PChar(fn), True);

    fDecl:=FindFiles(path, 'Decl_*.*');
    CopyFile(PChar(fDecl),PChar(path_arh + '\' + ExtractFileName(fDecl)), True);

    fRef:=FindFiles(path, 'Refs_*.*');
    CopyFile(PChar(fRef),PChar(path_arh + '\' + ExtractFileName(fRef)), True);

    fmWork.con1ADOCon.Close;
    fmWork.con2ADOCon.Close;
    if fDecl <> '' then
      rez := Arh2DB(db_decl,fDecl, fOrg.Inn, False);
    if fRef <> '' then
      rez := Arh2DB(db_spr,fRef, psw_loc, False);
    if rez then Application.Terminate;
  end;
end;

procedure TfmSetting.btn8Click(Sender: TObject);
var fDecl, fRef, path, fn, fn1, newPath:string;
    rez:Boolean;
begin
  fmWork.dlgOpen_Import.Title := '����� ������ ��� ��������������: ';
  fmWork.dlgOpen_Import.Filter := '����� ������|*.zip';
  Screen.Cursor := crHourGlass;
  try
    if fmWork.dlgOpen_Import.Execute then
      begin
        path := fmWork.dlgOpen_Import.FileName;

        if path <> '' then
          UnZip(path, newPath);

        fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)
           + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
        fn1:=GetSpecialPath(CSIDL_PROGRAM_FILES)
           + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '_.ini';
        if FileExists(fn1) then DeleteFile(PWideChar(fn));
        if FileExists(fn) then MoveFile(PChar(fn), PChar(fn1));

        path:=NewPath;

        CopyFile(PChar(path+'\'+NameProg+'.ini'),PChar(fn), True);

        fDecl:=FindFiles(path, 'Decl_*.*');
        CopyFile(PChar(fDecl),PChar(path_arh + '\' + ExtractFileName(fDecl)), True);

        fRef:=FindFiles(path, 'Refs_*.*');
        CopyFile(PChar(fRef),PChar(path_arh + '\' + ExtractFileName(fRef)), True);

        fmWork.con1ADOCon.Close;
        fmWork.con2ADOCon.Close;
        if fDecl <> '' then
          rez := Arh2DB(db_decl,fDecl, fOrg.Inn, False);
        if fRef <> '' then
          rez := Arh2DB(db_spr,fRef, psw_loc, False);
//        DelDir(path);
          RemoveDir(path);
        if rez then Application.Terminate;
      end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crHourGlass;
  end;


end;

procedure TfmSetting.btn9Click(Sender: TObject);
begin
//  ShellExecute(Application.Handle, nil, 'http://yadi.sk/d/oQ8TQ5jVLWAZf', nil, nil,SW_SHOWNOACTIVATE);
  HelpDownload;
end;

procedure TfmSetting.btn_ShOstClick(Sender: TObject);
var fName:string;
begin
  if SaveShOst(fName) then
    Show_Inf('���� ������� �������� �������� �� ������' + #10#13 + fName, fInfo)
  else
    Show_Inf('��������� ������ ��� ���������� ����� ������� ��������', fError);
end;

procedure TfmSetting.SpeedButton5Click(Sender: TObject);
var
  dfmBD: TfmBD;
  dfmInfo: TfmInfo;
begin
  dfmInfo := TfmInfo.Create(Self);
  try
    dfmInfo.lText.Caption :=
      '������ "������ � ������ ������" �������� ����������� ��� �������� ' +
      '� �������������� ��� ������ �� ��������� �����, ������������ ' +
      '������������� ������� ����� ������� ������ ������ ���������� ' +
      '�� ��������������.' + #10#13 + '���������� �������� �������?';
    if dfmInfo.ShowModal = mrOk then
      begin
        dfmBD := TfmBD.Create(Self);
        try
          dfmBD.ShowModal;
        finally
          dfmBD.Free;
        end;
      end;
  finally
    dfmInfo.Free;
  end;

end;

procedure TfmSetting.SpeedButton8Click(Sender: TObject);
var
  fn:string;
  tIniF : TIniFile;
begin
  fn:=GetSpecialPath(CSIDL_PROGRAM_FILES)+'\'+NameProg+'\'+fOrg.Inn + '\'+NameProg+'.ini';
  tIniF:=TIniFile.Create(fn);
  if fmSelSert = nil then fmSelSert:=TfmSelSert.Create(Self);
  try
  FindEcp:=SetECP;
  leDateBeg.Text := '';
  leDateEnd.Text := '';
  leNameOrg.Text := '';
  leFIO.Text := '';
  le_INN.Text := '';
  leEmail.Text := '';
  lePath.Text := '';
  if not FindEcp then
    begin
      Show_Inf('�� ������� �� ������ �����������', fError);
      fmSelSert.Close;
    end
  else
    begin
      fmSelSert.ShowModal;
      if SelectCert then
        begin
          leDateBeg.Text := DateToStr(fSert.dBeg);
          leDateEnd.Text := DateToStr(fSert.dEnd);
          leNameOrg.Text := fSert.OrgName;
          leFIO.Text := fSert.FIO;
          le_INN.Text := fSert.INN;
          leEmail.Text := fSert.email;
          lePath.Text := fSert.path;

          fmMain.Notebook1.PageIndex := 1;
          fmMain.leFIO.Text := fSert.FIO;
          fmMain.leDateBeg.Text := DateToStr(fSert.dBeg);
          fmMain.leDateEnd.Text := DateToStr(fSert.dEnd);
          fmMain.leNameOrg.Text := fSert.OrgName;
          fmMain.leFIO.Text := fSert.FIO;
          fmMain.leINN.Text := fSert.INN;

          SetColorSertSet(leDateEnd, le_INN);
        end;
    end;
  tIniF.WriteString(SectionName,'Sert', fSert.path);
  finally
    fmSelSert.free;
    fmSelSert := nil;
    tIniF.Free;
  end;
end;

procedure TfmSetting.SpeedButton9Click(Sender: TObject);
var
  butSel: integer;
begin
  butSel := Show_Inf('����������� ����� ���������.' + #10#13 + '����������?',
    fConfirm);
  if butSel = mrOk then
    begin
      fmWork.sb_DataExchangeClick(Application);
      if fmProgress = nil then
        fmProgress:= TfmProgress.Create(Application);
      fmWork.Enabled := False;
      fmProgress.Show;
      try
        try
          if UpdateRefOnLine(fmWork.que2_sql, BMHost, fOrg.Inn) then
            begin
              UpdateDateRef;
              GetAllDataDecl(fmWork.que1_sql,fmWork.que2_sql,fmProgress.PB, SLAllDecl,SLDataOst, fmProgress.lblcap);
              ChangeKvart(dBeg, dEnd);
              if fmMove <> nil then
                fmMove.ShowDataAll;
              if fmMain <> nil then
                fmMain.WriteCaptionCount;
            end;
        finally
          fmProgress.Close;
          fmWork.Enabled := True;
        end;
      except
        fmWork.Enabled := True;
      end;
    end;
end;

constructor TthCustom.Create(Suspended: Boolean = True);
begin
  inherited Create(Suspended);
  FreeOnTerminate := True;
end;

procedure TthCustom.Execute;
begin
  RunIt;
end;

procedure TthCustom.RunIt;
begin
  FExecProcedure := ExecProcedure;
  Resume;
end;

end.
