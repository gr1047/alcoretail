unit dfmCena;

interface

uses Forms, StdCtrls, Buttons, Classes, Controls, Grids, AdvObj, BaseGrid,
  AdvGrid, IniFiles, ShlObj, SysUtils;

type
  TfmCena = class(TForm)
    SG_Cena: TAdvStringGrid;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btn3: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCena: TfmCena;

implementation
uses GlobalConst, Global, GlobalUtils;
{$R *.dfm}

procedure TfmCena.btn1Click(Sender: TObject);
var SL1, SL2:TStrings;
  tIniF: TIniFile;
  fn:string;
  i:Integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  SL1:=TStringList.Create;
  SL2:=TStringList.Create;
  try
    SG_Cena.UnHideRowsAll;
    SL1.Assign(SG_Cena.Cols[1]);
    SL2.Assign(SG_Cena.Cols[2]);
    SL1.Delete(0);
    SL2.Delete(0);
    SG_Cena.HideRow(1);
    SG_Cena.HideRow(2);
    SG_Cena.HideRow(13);

    for i := 0 to SL1.Count - 1 do
      tIniF.WriteString('Cena','cr'+IntToStr(i+1), SL1.Strings[i]);

    for i := 0 to SL2.Count - 1 do
      tIniF.WriteString('CenaImp','cr'+IntToStr(i+1), SL2.Strings[i]);

  finally
    SL2.Free;
    SL1.Free;
    tIniF.free;
  end;
end;

procedure TfmCena.btn3Click(Sender: TObject);
var tIniF :TIniFile;
  fn:string;
  i:integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  try
    tIniF.EraseSection('Cena');
    tIniF.EraseSection('CenaImp');
    SG_Cena.UnHideRowsAll;
    for i := 1 to High(CenaR) do
      SG_Cena.Cells[1, SG_Cena.FixedRows + i - 1]:=FloatToStr(CenaR[i]);

    for i := 1 to High(CenaI) do
      SG_Cena.Cells[2, SG_Cena.FixedRows + i - 1]:=FloatToStr(CenaI[i]);
    SG_Cena.HideRow(1);
    SG_Cena.HideRow(8);
  finally
    tIniF.Free;
  end;
end;

procedure TfmCena.FormShow(Sender: TObject);
var tIniF: TIniFile;
  fn,s:string;
//  e:Extended;
  sl:TStrings;
  i:integer;
begin
  fn := GetSpecialPath(CSIDL_PROGRAM_FILES)
    + '\' + NameProg + '\' + fOrg.inn + '\' + NameProg + '.ini';
  tIniF := TIniFile.Create(fn);
  SL:=TStringList.Create;
  SG_Cena.UnHideRowsAll;
  try
    tIniF.ReadSectionValues('Cena', SL);
    for i := 0 to SL.Count - 1 do
      begin
        s:=SL.ValueFromIndex[i];
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
          s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
          s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        SG_Cena.Cells[1, SG_Cena.FixedRows + i]:=s;
      end;

    if SL.Count = 0 then
      begin
        for i := 1 to High(CenaR) do
          begin
            tIniF.WriteFloat('Cena','cr' + IntToStr(i),CenaR[i]);
            SG_Cena.Cells[1, SG_Cena.FixedRows + i - 1]:=FloatToStr(CenaR[i]);
          end;
      end;

    SL.Clear;
    tIniF.ReadSectionValues('CenaImp', SL);
    for i := 0 to SL.Count - 1 do
      begin
        s:=SL.ValueFromIndex[i];
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = '.') then
          s := ReplaceChr(s, ',', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        if ({$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator = ',') then
          s := ReplaceChr(s, '.', {$if CompilerVersion >= 24}FormatSettings.{$ifend}DecimalSeparator);
        SG_Cena.Cells[2, SG_Cena.FixedRows + i]:=s;
      end;

    if SL.Count = 0 then
      begin
        for i := 1 to High(CenaI) do
          begin
            tIniF.WriteFloat('CenaImp','cr' + IntToStr(i),CenaI[i]);
            SG_Cena.Cells[2, SG_Cena.FixedRows + i - 1]:=FloatToStr(CenaI[i]);
          end;
      end;

    SG_Cena.HideRow(1);
    SG_Cena.HideRow(8);
  finally
    tIniF.Free;
    SL.Free;
  end;

end;

end.
