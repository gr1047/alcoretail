program Alcoretail;

uses
  EMemLeaks,
  EResLeaks,
  EDialogWinAPIMSClassic,
  EDialogWinAPIEurekaLogDetailed,
  EDialogWinAPIStepsToReproduce,
  EDebugExports,
  EFixSafeCallException,
  EMapWin32,
  EAppVCL,
  ExceptionLog7,
  Forms,
  ABOUT in 'AlcoRetail\ABOUT.pas' {AboutBox},
  CSCA in 'AlcoRetail\CSCA.pas',
  DbfTable in 'AlcoRetail\DbfTable.pas',
  dfmAddOst in 'AlcoRetail\dfmAddOst.pas' {fmAddOst},
  dfmAdmim in 'AlcoRetail\dfmAdmim.pas' {fmAdmim},
  dfmAllOst in 'AlcoRetail\dfmAllOst.pas' {fmAllOst},
  dfmAutoRash in 'AlcoRetail\dfmAutoRash.pas' {fmAutoRash},
  dfmBD in 'AlcoRetail\dfmBD.pas' {fmBD},
  dfmCena in 'AlcoRetail\dfmCena.pas' {fmCena},
  dfmCombination in 'AlcoRetail\dfmCombination.pas' {fmCombination},
  dfmCorOst in 'AlcoRetail\dfmCorOst.pas' {fmCorOst},
  dfmCorrect in 'AlcoRetail\dfmCorrect.pas' {fmCorrect},
  dfmCorSal in 'AlcoRetail\dfmCorSal.pas' {fmCorSal},
  dfmDataExchange in 'AlcoRetail\dfmDataExchange.pas' {fmDataExchange},
  dfmDecl in 'AlcoRetail\dfmDecl.pas' {fmDecl},
  dfmEditAdres in 'AlcoRetail\dfmEditAdres.pas' {fmEditAdres},
  dfmErrXML in 'AlcoRetail\dfmErrXML.pas' {fmErrXML},
  dfmFiltrProd in 'AlcoRetail\dfmFiltrProd.pas' {fmFiltrProd},
  dfmFiltrRef in 'AlcoRetail\dfmFiltrRef.pas' {fmFiltrRef},
  dfmFiltrSal in 'AlcoRetail\dfmFiltrSal.pas' {fmFiltrSal},
  DfmFNS in 'AlcoRetail\DfmFNS.pas' {fmFNS},
  dfmGetOrg in 'AlcoRetail\dfmGetOrg.pas' {fmGetOrg},
  dfmImportRefDBF in 'AlcoRetail\dfmImportRefDBF.pas' {fmImportRefDBF},
  dfmInfo in 'AlcoRetail\dfmInfo.pas' {fmInfo},
  dfmJournal in 'AlcoRetail\dfmJournal.pas' {fmJournal},
  dfmMain in 'AlcoRetail\dfmMain.pas' {fmMain},
  dfmMove in 'AlcoRetail\dfmMove.pas' {fmMove},
  dfmMoves in 'AlcoRetail\dfmMoves.pas' {fmMoves},
  dfmMoving in 'AlcoRetail\dfmMoving.pas' {fmMoving},
  dfmNewOrg in 'AlcoRetail\dfmNewOrg.pas' {fmNewOrg},
  dfmOborot in 'AlcoRetail\dfmOborot.pas' {fmOborot},
  dfmOrg in 'AlcoRetail\dfmOrg.pas' {fmOrg},
  dfmOstatki in 'AlcoRetail\dfmOstatki.pas' {fmOstatki},
  dfmProcent in 'AlcoRetail\dfmProcent.pas' {fmProcent},
  dfmProgress in 'AlcoRetail\dfmProgress.pas' {fmProgress},
  dfmQuery in 'AlcoRetail\dfmQuery.pas' {fmQuery},
  dfmRef in 'AlcoRetail\dfmRef.pas' {fmRef},
  dfmReplace in 'AlcoRetail\dfmReplace.pas' {fmReplace},
  dfmRezRef in 'AlcoRetail\dfmRezRef.pas' {fmRezRef},
  dfmSaler in 'AlcoRetail\dfmSaler.pas' {fmSaler},
  dfmSelAllKPP in 'AlcoRetail\dfmSelAllKPP.pas' {fmSelAllKPP},
  dfmSelKPP in 'AlcoRetail\dfmSelKPP.pas' {fmSelKPP},
  dfmSelSert in 'AlcoRetail\dfmSelSert.pas' {fmSelSert},
  dfmSetting in 'AlcoRetail\dfmSetting.pas' {fmSetting},
  dfmShowInf in 'AlcoRetail\dfmShowInf.pas' {fmShowInf},
  dfmSJournal in 'AlcoRetail\dfmSJournal.pas' {fmSJournal},
  dfmStatistic in 'AlcoRetail\dfmStatistic.pas' {fmStatistic},
  dfmWork in 'AlcoRetail\dfmWork.pas' {fmWork},
  FWZipConsts in 'AlcoRetail\FWZipConsts.pas',
  FWZipCrc32 in 'AlcoRetail\FWZipCrc32.pas',
  FWZipCrypt in 'AlcoRetail\FWZipCrypt.pas',
  FWZipReader in 'AlcoRetail\FWZipReader.pas',
  FWZipStream in 'AlcoRetail\FWZipStream.pas',
  FWZipWriter in 'AlcoRetail\FWZipWriter.pas',
  Global in 'AlcoRetail\Global.pas',
  Global2 in 'AlcoRetail\Global2.pas',
  GlobalConst in 'AlcoRetail\GlobalConst.pas',
  GlobalUtils in 'AlcoRetail\GlobalUtils.pas',
  icmp in 'AlcoRetail\icmp.pas',
  MSXML2_TLB in 'AlcoRetail\MSXML2_TLB.pas',
  PingModule in 'AlcoRetail\PingModule.pas',
  Wcrypt2 in 'AlcoRetail\Wcrypt2.pas',
  XSDValidator in 'AlcoRetail\XSDValidator.pas',
  dfmSelPeriod in 'AlcoRetail\dfmSelPeriod.pas' {fmSelPeriod},
  uForm78 in 'AlcoRetail\uForm78.pas';

{$R *.res}
{$R AlcoRetail\RES\res_59.res}
{$R AlcoRetail\RES\res_fsrar.res}
{$R AlcoRetail\RES\res_printf.res}
{$R AlcoRetail\RES\res_stat.res}
{$R AlcoRetail\RES\11_o.res}
{$R AlcoRetail\RES\12_o.res}
{$R AlcoRetail\RES\res_journal.res}
{$R AlcoRetail\RES\shOst.res}
//{$R Common\RES\manifest.res}


begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmWork, fmWork);
  Application.CreateForm(TfmDataExchange, fmDataExchange);
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TfmProgress, fmProgress);
  Application.CreateForm(TfmShowInf, fmShowInf);
  Application.CreateForm(TfmReplace, fmReplace);
  Application.CreateForm(TfmRezRef, fmRezRef);
  Application.CreateForm(TfmFiltrRef, fmFiltrRef);
  Application.CreateForm(TfmQuery, fmQuery);
  Application.CreateForm(TfmMove, fmMove);
  Application.CreateForm(TfmAdmim, fmAdmim);
  Application.Run;
end.
