with tab1 as 
(SELECT
 cast(replace(Amount, ',','.') as float) * 
  (case 
   when DeclType='1' then 1  
   when DeclType='2' then -1
   when DeclType='3' then -1  
   when DeclType='4' then 1  
   when DeclType='5' then -1  
   when DeclType='6' then -1 
   when DeclType='7' then 1 
   end) as     
   Amount1, 
  ean13,  reportdate as repdate 
FROM DECLARATION),
tab1_1 as 
 (select
	 case when repdate <'2021-07-01' then amount1 end as rest1,
	 case when repdate between '2021-07-01' and '2021-09-30' and amount1 > 0 then amount1 else 0 end  as rest2, 
	 case when repdate between '2021-07-01' and '2021-09-30' and amount1 < 0 then amount1 * -1 else 0 end as rest3,
	 EAN13, repdate
from tab1),
tab2 as 
(
select tab1_1.*, t2.FNSCode, t2.Capacity, t2.AlcVol, t2.prodkod as inn, t2.prodcod as kpp from tab1_1
inner join 
production t2
on tab1_1.EAN13 = t2.EAN13
),
RroducerU as 
(select t1.* from producer t1
inner join 
(select max(pk) as id, inn, kpp from Producer group by inn, kpp)t2
on t1.pk = t2.id
),
tab3 as 
(select tab2.*, FullName from tab2
 inner join 
 RroducerU t2
 on tab2.inn = t2.inn and tab2.kpp = t2.kpp
 ),
tab4 as  
(select *, (rest1 + inrest1)-outrest1 as finrest1 from 
(
select FNSCode,FullName,inn,kpp,
case when rest is null or round(rest, 3) = 0 then 0 else rest end as rest1,
case when inrest is null or round(inrest, 3) = 0 then 0 else inrest end as inrest1, 
case when outrest is null or round(outrest, 3) =0 then 0 else outrest end as outrest1 from
(select FNSCode,inn,kpp,FullName, sum(rest1)/10 as rest, sum(rest2)/10 as inrest, sum(rest3)/10 as outrest from tab3 group by FNSCode,inn,kpp,FullName
)t1 
)t1 
where rest1 >0 or inrest1 > 0 or outrest1 > 0 or finrest1 >0
)
select * from tab4

select FNSCode, sum(rest1), sum(inrest1), sum(outrest1), sum(finrest1) from tab4 group by FNSCode