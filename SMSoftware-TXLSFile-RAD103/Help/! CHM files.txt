----------------------------------------------
About HTML Help CHM files and Windows security
----------------------------------------------


Problem
-------
After you install some security updates for Microsoft Windows, 
you may experience one or both of the following symptoms after you click 
a link to an HTML Help .CHM file:
- Topics in the .CHM file cannot be viewed when you click Open 
  instead of Save in the File Download dialog box. 
- Topics in the .CHM file cannot be viewed when you click Save 
  in the File Download dialog box, and you then try to open the file. 


Resolution
----------

Method 1
1. Double-click the .CHM file. 
2. In the Open File-Security Warning dialog box, click to clear 
   the "Always ask before opening this file" check box. 
3. Click Open. 

Method 2
1. Right-click the .CHM file, and then click Properties. 
2. Click Unblock. 
3. Double-click the .CHM file to open the file. 


See also
--------
http://support.microsoft.com/kb/902225/en
