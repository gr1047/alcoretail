unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Grouping rows and columns
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      Cells[2, 2].Value:= 'Demo 1';
      Cells[2, 2].FontHeight:= 50;
      Cells[3, 2].Value:= 'Demo 2';
      Cells[3, 2].FontHeight:= 50;
      Cells[3, 3].Value:= 'Demo 3';
      Cells[3, 3].FontHeight:= 20;

      { Autofit columns for large text } 
      Columns.AutoFit;

      { Group columns and rows }
      GroupColumns(2, 3);
      GroupRows(2, 3);

      { Set groups' display method }
      PageSetup.ColumnGroupAtLeft:= True;
      PageSetup.RowGroupAtTop:= True;
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
