unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Page setup
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSWorkbook
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  R, C: Integer;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object.
   After creation it contains one sheet. }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Fill sheet with data }
      for R:= 1 to 30 do
        for C:= 0 to 30 do
          Cells[R, C].Value:= 'r' + IntToStr(R) + 'c' + IntToStr(C);

      Cells[0, 0].Value:= 'Use print preview to see print area.';
      Cells[0, 0].FontColorIndex:= xlColorDarkSky;
      Cells[0, 0].FontBold:= True;

      {Set print area }
      PageSetup.PrintArea.AddRect(0, 30, 0, 30);

      { Set page header and footer }
      PageSetup.HeaderText:= 'Header center';
      PageSetup.HeaderTextLeft:= 'Header left';
      PageSetup.HeaderTextRight:= 'Header right';
      PageSetup.FooterText:= 'Footer center';
      PageSetup.FooterTextLeft:= 'Footer left';
      PageSetup.FooterTextRight:= 'Footer right';

      { Fit printed data into one sheet }
      PageSetup.FitPagesWidth:= 1;
      PageSetup.FitPagesHeight:= 1;
      PageSetup.Zoom:= False;
      { Use landscape orientation }
      PageSetup.Orientation:= xlLandscape;
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
