unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Simple reading
------------------------------------------------------------------------------ }

interface

{$I XLSFile.inc}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
     XLSFile
   , XLSWorkbook
{$IFDEF XLF_D6}
   , Variants
{$ENDIF}
   ;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  I: Integer;
  C: TCell;
  S: String;
begin
  { 1. Prepare simple file for reading }

  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Number }
      Cells[0, 0].Value:= 123;
      { String }
      Cells[1, 0].Value:= 'Text';
      { Datetime }
      Cells[2, 0].Value:= Now;
    end;

    { Save workbook data to Excel file 'in.xls' }
    xf.SaveAs('in.xls');
  finally
    xf.Destroy;
  end;

  { 2. Read data }
  xf:= TXLSFile.Create;
  try
    xf.OpenFile('in.xls');

    { Enumerate cells in 1st sheet. Sheet's index is 0-based. }
    for I:= 0 to xf.Workbook.Sheets[0].Cells.Count - 1 do
    begin
      C:= xf.Workbook.Sheets[0].Cells.Item[I];
      S:= 'Row ' + IntToStr(C.Row) + ', Column ' + IntToStr(C.Col);

      case VarType(C.Value) of
        varInteger,
        varSmallint,
        varByte,
        varDouble,
        varSingle:
          S:= S + ', number = ' + FloatToStr(C.Value);
        varString,
        varOleStr:
          S:= S + ', string = ' + C.Value;
        varDate:
          S:= S + ', date = ' + FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', C.Value);
      end;

      Memo1.Lines.Add(S);
    end;
    Memo1.Lines.Add('');
  finally
    xf.Destroy;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
