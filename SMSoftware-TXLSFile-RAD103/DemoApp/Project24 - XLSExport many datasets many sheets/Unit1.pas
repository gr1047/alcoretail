unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Use XLSExport for multi-sheet report

    NOTE:
    This project requires BDE, and uses Borland demo database DBDEMOS.

    It creates an orders' report by customers. Each customer's
    orders will be placed into a separate Excel sheet.

------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, XLSExportComp, Db, DBTables, Grids, DBGrids;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    XLSExportFile1: TXLSExportFile;
    XLSExportDataSource1: TXLSExportDataSource;
    MyBase3: TDatabase;
    Ords: TTable;
    OrdsCustNo: TFloatField;
    OrdsOrderNo: TFloatField;
    OrdsSaleDate: TDateTimeField;
    OrdsItemsTotal: TCurrencyField;
    OrdsTaxRate: TFloatField;
    OrdsFreight: TCurrencyField;
    OrdsAmountPaid: TCurrencyField;
    Cust: TTable;
    CustCustNo: TFloatField;
    CustCompany: TStringField;
    dsCust: TDataSource;
    dsOrds: TDataSource;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
    procedure XLSExportDataSource1SaveTitle(FieldIndex: Integer;
      XLSCell: TCell);
    procedure XLSExportDataSource1SaveField(Field: TField; XLSCell: TCell);
    procedure XLSExportDataSource1SaveFooter(FieldIndex: Integer;
      XLSCell: TCell; var TotalCalcType: TTotalCalcType;
      var TotalRange: String);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;

procedure TForm1.RunTest;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  try
    { Clear previous data from Workbook object }
    XLSExportFile1.Workbook.Clear;

    { Open tables }
    Cust.Open;
    Ords.Open;

    { Cust table is master, Ords table is details table }
    while not Cust.Eof do
    begin
      if Ords.RecordCount > 0 then
      begin
        { export DataSource to sheet named [CustCompany.Value]
          starting at row 3, column 0 (indices are 0-based) }
        XLSExportDataSource1.ExportDataSheetName(CustCompany.Value, 3, 0);

        { Add some text to the sheet }
        with XLSExportFile1.Workbook.SheetByName(CustCompany.Value) do
        begin
          { Set cell value }
          Cells[0, 0].Value:= 'Customer No: ' + FormatFloat('0000',CustCustNo.Value);
          Cells[1, 0].Value:= 'Customer: ' + CustCompany.Value;
          { Set cell font and color }
          Cells[1, 0].FontBold:= True;
          Cells[1, 0].FontColorIndex:= xlColorDarkBlue;
        end;
      end;

      Cust.Next;
    end;

   { Save workbook data to Excel file out.xls }
    XLSExportFile1.SaveToFile('out.xls');
  finally
    Ords.Close;
    Cust.Close;
    MyBase3.Connected:= False;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

procedure TForm1.XLSExportDataSource1SaveTitle(FieldIndex: Integer;
  XLSCell: TCell);
begin
  { Title row export event:
    here you can customise DataSource title export }

  { Set font color for title cells }
  XLSCell.FontColorIndex:= xlColorWhite;
  { Set fill pattern and color for title cells }
  XLSCell.FillPatternBGColorIndex:= xlColorDarkBlue;
  XLSCell.FillPattern:= xlPatternSolid;
end;

procedure TForm1.XLSExportDataSource1SaveField(Field: TField;
  XLSCell: TCell);
begin
  { Cell export event:
    here you can customise default exporting options.
    Field is DataSource field, XLSCell is worksheet cell }

  { Set blue font color for ItemTotals field }
  if Field.FieldName = 'ItemsTotal' then
  begin
    XLSCell.FontColorIndex:= xlColorBlue;
  end;
end;

procedure TForm1.XLSExportDataSource1SaveFooter(FieldIndex: Integer;
  XLSCell: TCell; var TotalCalcType: TTotalCalcType;
  var TotalRange: String);
begin
  { Footer row export event:
    here you can add totals after exporting DataSource fields }

  case FieldIndex of
    0: begin
         { Place 'Totals' string to the footer for field 0}
         TotalCalcType:= tcUserDef;
         XLSCell.Value:= 'Totals';
       end;
    2: begin
         { Place sum total to the footer for fields 2, 5}
         TotalCalcType:= tcSum;
       end;
  end;
  { set bold style for all totals }
  XLSCell.FontBold:= True;
end;

end.
