unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Formatting rows and columns
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSWorkbook
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  Row: TRow;
  Column: TColumn;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object }
  xf:= TXLSFile.Create;
  try
    { Format a row }
    Row:= xf.Workbook.Sheets[0].Rows[2];
    Row.BorderColorRGB[xlBorderAll]:= RGB(200, 0, 0);
    Row.BorderStyle[xlBorderAll]:= bsThin;
    Row.FillPatternBGColorRGB:= RGB(200, 200, 0);
    Row.FillPatternFGColorRGB:= RGB(255, 255, 0 );
    Row.FillPattern:= xlPatternGray8;
    Row.HAlign:= xlHAlignCenter;
    Row.VAlign:= xlVAlignCenter;
    { Set font properties }
    Row.FontBold:= False;
    Row.FontItalic:= true;
    Row.FontName:= 'Courier New';
    Row.FontColorIndex:= xlColorBlue;
    Row.FontHeight:= 12;

    { Format a column }
    Column:= xf.Workbook.Sheets[0].Columns[2];
    Column.BorderColorIndex[xlBorderAll]:= xlColorBlue;
    Column.BorderStyle[xlBorderAll]:= bsDashed;
    Column.HAlign:= xlHAlignCenter;
    Column.VAlign:= xlVAlignCenter;
    { Set font properties }
    Column.FontBold:= False;
    Column.FontName:= 'Arial';
    Column.FontHeight:= 24;
    {Set column's width}
    Column.WidthPx:= 200;

    { Fill some cells to see formatting }
    with xf.Workbook.Sheets[0] do
    begin
      Cells[0, 0].Value:= 'test 1';
      Cells[1, 0].Value:= 'test 2';
      Cells[2, 0].Value:= 'test 3';
      Cells[0, 1].Value:= 'test 4';
      Cells[1, 1].Value:= 'test 5';
      Cells[2, 1].Value:= 'test 6';
      Cells[0, 2].Value:= 'test 7';
      Cells[1, 2].Value:= 'test 8';
      Cells[2, 2].Value:= 'test 9';
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
