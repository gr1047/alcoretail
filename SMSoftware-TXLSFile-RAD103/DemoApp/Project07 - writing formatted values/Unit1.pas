unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Writing formatted values
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  I: Integer;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object }
  xf:= TXLSFile.Create;
  try
    { Sheet's index is 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Rows' and columns' indices are 0-based }

      { Apply built-in formats }
      for I:= 0 to 35 do
      begin
        { Set cell value }
        Cells[I, 1].Value:= 12345.123456789;
        Cells[I, 2].Value:= -12345.123456789;
        { Set format index }
        Cells[I, 1].FormatStringIndex:= I;
        Cells[I, 2].FormatStringIndex:= I;
      end;

      { Add custom numeric format }
      I:= xf.Workbook.FormatStrings.AddFormat('0000-0000-0000-0000');
      Cells[0, 0].Value:= 123456789;
      Cells[0, 0].FormatStringIndex:= I;

      { Add custom date format }
      Cells[1, 5].Value:= Now;
      I:= xf.Workbook.FormatStrings.AddFormat('yyyy.mm.dd hh:mm:ss');
      Cells[1, 5].FormatStringIndex:= I;

      { Set column's width }
      Columns[0].WidthPx:= 150;
      Columns[1].WidthPx:= 150;
      Columns[2].WidthPx:= 150;
      Columns[5].WidthPx:= 150;
    end;
    
    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
