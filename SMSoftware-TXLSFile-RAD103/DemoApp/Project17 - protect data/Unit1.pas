unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Protect data

    An Excel document may contain different types of protection.

    Worksheet protection protects contents of the worksheet. When you protect
    a worksheet, all cells on the worksheet are locked by default, and users
    cannot make any changes to a locked cell. For example, they cannot insert,
    modify, delete, or format data in a locked cell.

    Workbook protection protects contents of the workbook.

    File protection protects the file itself, and restricts read and write access.
    When the file is protected, its data is encrypted.
    TXLSFile library supports Excel 97-2003 standard encryption method.
    To read more about data protection see Microsoft Excel documentation.

------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    cbSheet: TCheckBox;
    cbWorkbook: TCheckBox;
    cbFile: TCheckBox;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  Password: String;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object.
   After creation it contains one sheet. }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }

    Password:= '123';

    { Protect sheet }
    if cbSheet.Checked then
      xf.Workbook.Sheets[0].Protect(Password);

    { Protect wirkbook }
    if cbWorkbook.Checked then
      xf.Workbook.Protect(Password);

    { Save workbook data to Excel file 'out.xls' }
    if cbFile.Checked then
      { Protect file }
      xf.SaveAsProtected('out.xls', Password)
    else
      xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
