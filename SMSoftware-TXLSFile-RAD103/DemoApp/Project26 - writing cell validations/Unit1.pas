unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Simple test - writing cell validations
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSCellValidation;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  { Create TXLSFile object.
    After creation it contains one sheet }
  xf:= TXLSFile.Create;
  try
    { Sheets' and cells' indices are 0-based }

   { 1. Add integer limit validation }
    xf.Workbook.Sheets[0].CellValidations.AddValidation
      ( cvtIsInteger // integer value
      , cvoBetween   // must be between
      , '1'          // 1
      , '100'        // and 100
      , True         // Allow blank values
      , True         // Show input message
      , True         // Show error message
      , cveWarning   // Error alert style is Warning
      , 'Input title'
      , 'Input message'
      , 'Error title'
      , 'Error message'
      , 1, 2, 1, 2   // Validation for an area from row 1 to 2, and from column 1 to 2}
      );

    { 2. Add list validation }
    xf.Workbook.Sheets[0].CellValidations.AddListValidation
      ('$E$1:$E$3'   // List values from area
      , True         // Allow blank values
      , True         // Show in-cell dropdown
      , True         // Show input message
      , True         // Show error message
      , cveStop      // Error alert style is Stop
      , ''           // Empty messages
      , ''
      , ''
      , ''
      , 0, 0, 0, 0   // Validation for an area from row 0 to 0, and from column 0 to 0
      );

    { Add some values into list area }
    xf.Workbook.Sheets[0].Cells.CellByA1Ref['E1'].Value:= 1;
    xf.Workbook.Sheets[0].Cells.CellByA1Ref['E2'].Value:= 2;
    xf.Workbook.Sheets[0].Cells.CellByA1Ref['E3'].Value:= 3;

    { 3. Add explicit list validation }
    xf.Workbook.Sheets[0].CellValidations.AddExplicitListValidation
      ('item1;item1;item3' // List values
      , ';'                // List separator
      , True               // Allow blank values
      , True               // Show in-cell dropdown
      , True               // Show input message
      , True               // Show error message
      , cveStop            // Error alert style is Stop
      , ''                 // Empty input message title
      , ''                 // Empty input message
      , 'Error'            // Error title
      , 'Incorrect value!' // Error message
      , 3, 3, 3, 3         // Validation for an area from row 3 to 3, and from column 3 to 3 
      );

    { Save workbook data to Excel file 'out.xls'}
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
