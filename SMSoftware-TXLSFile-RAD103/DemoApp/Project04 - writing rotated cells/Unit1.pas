unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Writing rotated cells
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  { Create TXLSFile object.
    After creation it contains one sheet }
  xf:= TXLSFile.Create;
  try
    { Sheet's index is 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Set cell rotation style }
      Cells[0, 0].Rotation:= xlRotationNone;
      { Set cell value. Rows' and columns' indices are 0-based. }
      Cells[0, 0].Value:= 'Demo';

      Cells[1, 0].Rotation:= xlRotationTextVerticalCharHorizontal;
      Cells[1, 0].Value:= 'Demo';
      Cells[2, 0].Rotation:= xlRotation45Up;
      Cells[2, 0].Value:= 'Demo';
      Cells[3, 0].Rotation:= xlRotation45Down;
      Cells[3, 0].Value:= 'Demo';
      Cells[4, 0].Rotation:= xlRotationVerticalUp;
      Cells[4, 0].Value:= 'Demo';
      Cells[5, 0].Rotation:= xlRotationVerticalDown;
      Cells[5, 0].Value:= 'Demo';
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
