unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Writing merged cells
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSWorkbook
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  R: TRange;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object }
  xf:= TXLSFile.Create;
  try
    { Sheets' index is 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Create new range in sheet }
      R:= Ranges.Add;

      { Add 2 rectangle areas to the range. Rows' and columns' indices are 0-based. }
      R.AddRect(0,2,0,2);
      R.AddRect(3,5,3,5);

      { Create outline border for range }
      R.BordersOutline(xlColorBlue, bsThick);

      { Bulk operation - set value for all cells in the range }
      R.Value:= 10;

      { Create new range and merge its cells }
      R:= Ranges.Add;
      R.AddRect(7,10,1,5);
      { Merge cells in the range }
      R.MergeCells;
      R.Value:= 'Merged cells';
      R.HAlign:= xlHAlignCenter;
      R.Valign:= xlVAlignCenter;
      { Set font size and color }
      R.FontHeight:= 20;
      R.FontColorIndex:= xlColorWhite;
      { Set fill pattern and color }
      R.FillPatternBGColorIndex:= xlColorPaleSky;
      R.FillPattern:= xlPatternSolid;
      { Outline the range with red border }
      R.BordersOutline(xlColorRed, bsDouble);
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
