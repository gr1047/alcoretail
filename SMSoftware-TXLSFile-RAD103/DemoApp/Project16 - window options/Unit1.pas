unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Window's options
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSWorkbook
   , XLSFormat;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  R, C: Integer;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object.
   After creation it contains one sheet. }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      { Fill sheet with data }
      for R:= 1 to 30 do
        for C:= 0 to 30 do
          if (R + C) mod 5 = 0 then
            { Some zero values to test DisplayZero option }
            Cells[R, C].Value:= 0
          else
            Cells[R, C].Value:= 'r' + IntToStr(R) + 'c' + IntToStr(C);

      { Set window's options }
      WindowOptions.DisplayGrids:= False;
      WindowOptions.DisplayRowColHeaders:= False;
      WindowOptions.DisplayZero:= False;
    end;

    { 2nd sheet }
    xf.Workbook.Sheets.Add('Sheet2');
    with xf.Workbook.Sheets[1] do
    begin
      { Fill sheet with data }
      for R:= 1 to 30 do
        for C:= 0 to 30 do
          Cells[R, C].Value:= 'r' + IntToStr(R) + 'c' + IntToStr(C);

      { Set window's options }
      { Arabic style - right to left }
      WindowOptions.DisplayRightToLeft:= True;
    end;

    { 3rd sheet }
    xf.Workbook.Sheets.Add('Sheet3');
    with xf.Workbook.Sheets[2] do
    begin
      { Fill sheet with data }
      for R:= 1 to 30 do
        for C:= 0 to 30 do
          Cells[R, C].Value:= 'r' + IntToStr(R) + 'c' + IntToStr(C);

      { Set window's options }
      { View page breaks }
      WindowOptions.PageBreakPreview:= True;
    end;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
