unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Export to HTML and TXT
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSBase
   , XLSRects;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  Rect: TRangeRect;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  if not FileExists('report.xls') then
  begin
    Application.MessageBox('Required file report.xls not found!', 'Error', MB_OK);
    Exit;
  end;

  xf:= TXLSFile.Create;
  try
    { Read data }
    xf.OpenFile('report.xls');

    { Export to HTML }
    xf.Workbook.Sheets[0].SaveAsHTML('out.html');

    { Export subarea to txt }
    Rect.RowFrom:= 10;
    Rect.RowTo:= 21;
    Rect.ColumnFrom:= 1;
    Rect.ColumnTo:= 4;
    xf.Workbook.Sheets[0].SaveRectAsTXT('out.txt', txtTab, Rect);
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
  begin
    OpenFileInOSShell('out.html');
    OpenFileInOSShell('out.txt');    
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
