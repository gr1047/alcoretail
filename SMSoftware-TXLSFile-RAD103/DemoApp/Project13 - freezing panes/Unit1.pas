unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Freezing panes
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSWorkbook;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object.
   After creation it contains one sheet. }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }
    xf.Workbook.Sheets[0].Freeze(0, 1);
    xf.Workbook.Sheets[0].Cells[0, 0].Value:= 'fixed';
    xf.Workbook.Sheets[0].Cells[1, 0].Value:= 'fixed';
    xf.Workbook.Sheets[0].Cells[0, 1].Value:= 'scrollable';
    xf.Workbook.Sheets[0].Cells[1, 1].Value:= 'Scroll window right to see unscrollable area.';

    xf.Workbook.Sheets.Add('Sheet2');
    xf.Workbook.Sheets[1].Freeze(1, 0);
    xf.Workbook.Sheets[1].Cells[0, 0].Value:= 'fixed';
    xf.Workbook.Sheets[1].Cells[0, 1].Value:= 'fixed';
    xf.Workbook.Sheets[1].Cells[1, 0].Value:= 'scrollable';
    xf.Workbook.Sheets[1].Cells[1, 1].Value:= 'Scroll window down to see unscrollable area.';


    xf.Workbook.Sheets.Add('Sheet3');
    xf.Workbook.Sheets[2].Freeze(1, 1);
    xf.Workbook.Sheets[2].Cells[0, 0].Value:= 'fixed';
    xf.Workbook.Sheets[2].Cells[1, 0].Value:= 'fixed';
    xf.Workbook.Sheets[2].Cells[0, 1].Value:= 'fixed';
    xf.Workbook.Sheets[2].Cells[1, 1].Value:= 'Scroll window right and down to see unscrollable area.';    

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
