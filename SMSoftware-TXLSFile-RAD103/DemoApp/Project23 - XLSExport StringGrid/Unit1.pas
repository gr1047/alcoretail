unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Use XLSExport components for TStringGrid
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, XLSExportComp, Db, DBTables, Grids, DBGrids;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    XLSExportFile1: TXLSExportFile;
    StringGrid1: TStringGrid;
    XLSExportStringGrid1: TXLSExportStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure XLSExportStringGrid1SaveCell(Row, Col: Integer;
      XLSCell: TCell);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;

procedure TForm1.RunTest;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  { Clear previous data }
  XLSExportFile1.Workbook.Clear;
  XLSExportFile1.Workbook.Sheets.Add('Sheet1');

  { Export DataSource data to sheet 0 starting at row 0 column 0.
    All indices are 0-based }
  XLSExportStringGrid1.ExportData(0, 0, 0);

  { Save workbook data to Excel file }
  XLSExportFile1.SaveToFile('out.xls');

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  R, C: Integer;
begin
  { fill a string grid with some test data }
  StringGrid1.ColCount:= 10;
  StringGrid1.RowCount:= 100;
  for R:= 0 to StringGrid1.RowCount-1 do
    StringGrid1.RowHeights[R]:= StringGrid1.DefaultRowHeight * ((R mod 2) + 1);

  for C:= 0 to StringGrid1.ColCount-1 do
    StringGrid1.ColWidths[C]:= StringGrid1.DefaultColWidth * ((C mod 2) + 1);

  for R:= 0 to StringGrid1.RowCount-1 do
    for C:= 0 to StringGrid1.ColCount-1 do
      if ((R + C) mod 4 = 0) then
        StringGrid1.Cells[R, C]:= 'test'
      else
        StringGrid1.Cells[R, C]:= 'test 2';
end;

{ OnSaveCell event allows to override cell before saving to XLS file }
procedure TForm1.XLSExportStringGrid1SaveCell(Row, Col: Integer;
  XLSCell: TCell);
begin
  { Set color }
  XLSCell.FontColorIndex:= (Row + Col) mod 50;
  { Font }
  if (Row = 0) then
    XLSCell.FontBold:= True;
  if (Row = 1) then
    XLSCell.FontHeight:= 40;
  if (Row = 2) then
    XLSCell.Value:= 'override';
end;

end.
