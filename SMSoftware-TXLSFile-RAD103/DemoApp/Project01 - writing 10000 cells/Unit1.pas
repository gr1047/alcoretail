unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Simple test - writing 10 000 cells
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
  I: Integer;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  { Create TXLSFile object.
    After creation it contains one sheet }
  xf:= TXLSFile.Create;
  try
    { Sheets' and cells' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      for I:= 0 to 10000 - 1 do
        Cells[I, 0].Value:= 'Demo' + IntToStr(I);
    end;

    { Save workbook data to Excel file 'out.xls'}
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
