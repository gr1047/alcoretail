unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Writing hyperlinks
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile
   , XLSHyperlink;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object.
   After creation it contains one sheet. }
  xf:= TXLSFile.Create;
  try
    { Sheets', rows', columns' indices are 0-based }

    { Add new sheet }
    xf.Workbook.Sheets.Add('Sheet 2');

    { Add URL hyperlink }
    xf.Workbook.Sheets[0].Cells[0, 0].Hyperlink:= 'http://google.com';
    xf.Workbook.Sheets[0].Cells[0, 0].HyperlinkType:= hltURL;

    { Add file hyperlink }
    xf.Workbook.Sheets[0].Cells[1, 0].Hyperlink:= 'c:\games\pool.swf';
    xf.Workbook.Sheets[0].Cells[1, 0].HyperlinkType:= hltFile;

    { Add UNC hyperlink }
    xf.Workbook.Sheets[0].Cells[2, 0].Hyperlink:= '\\server\shared\file.txt';
    xf.Workbook.Sheets[0].Cells[2, 0].HyperlinkType:= hltUNC;

    { Add link to 2nd sheet }
    xf.Workbook.Sheets[0].Cells[3, 0].Hyperlink:= '''Sheet 2''!A1:B5';
    xf.Workbook.Sheets[0].Cells[3, 0].HyperlinkType:= hltCurrentWorkbook;

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
