unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Simple test - writing cell validations
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

  { Create TXLSFile object.
    After creation it contains one sheet }
  xf:= TXLSFile.Create;
  try
    { Add comment to cell in row 0, column 0 (indices are 0-based) }
    xf.Workbook.Sheets[0].CellComments.Comment[0, 0].Assign
      ( 'Text text text'  // comment text
      , 'MyName'          // author
      , ''                // no rich formatting
      , True              // add author's name before text
      );

    { Add comment to cell in row 1, column 0 (indices are 0-based) }
    xf.Workbook.Sheets[0].CellComments.Comment[1, 0].AssignWithSize
      ( 'Text demo text'             // comment text
      , 'MyName'                     // author
      , '1-4(size:20;color:ff0000);' // rich format
      , 400                          // width
      , 100                          // height
      , True);                       // add author's name before text


    { Save workbook data to Excel file 'out.xls'}
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
