unit Unit1;

{ ------------------------------------------------------------------------------
    SM Software
    TXLSFile library
    Demo projects

    Writing formulas
------------------------------------------------------------------------------ }

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    cbOpenAfterSave: TCheckBox;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    procedure RunTest;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}

uses ShellAPI
   { ---------------------------------------------------------------------------
   Use required TXLSFile library units
   --------------------------------------------------------------------------- }
   , XLSFile;


{ Procedure uses OS shell to open and view XLS file }
procedure OpenFileInOSShell(AFile: string);
begin
  ShellExecute(0, 'open', PChar(AFile), nil, nil, SW_SHOW);
end;


procedure TForm1.RunTest;
var
  xf: TXLSFile;
begin
  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' Start');

 { Create TXLSFile object }
  xf:= TXLSFile.Create;
  try
    { Sheets' and cells' indices are 0-based }
    with xf.Workbook.Sheets[0] do
    begin
      Cells[0, 0].Value:= 1;
      Cells[0, 1].Formula:= '100+A1';
      Cells[1, 1].Formula:= 'SUM(A1:A100)';
      Cells[2, 1].Formula:= 'A1+B1';
      Cells[3, 1].Formula:= 'ROW()';
      Cells[4, 1].Formula:= 'SIN(1+COS(2))';
      Cells[5, 1].Formula:= 'MAX(A1;A2;A3)';
      Cells[6, 1].Formula:= 'MAX(A1:A3)';
      Cells[7, 1].Formula:= 'LEN("text constant")';
      Cells[8, 1].Formula:= 'IF(LEN("text constant")=1;1;2)';
      Cells[9, 1].Formula:= 'IF(A1=A2;"yes";"no")';
      Cells[10, 1].Formula:= 'AND(a1;a2)';
      Cells[10, 1].Formula:= '(1 + 2) * 3';
    end;

    {cross-sheet formulas}
    xf.Workbook.Sheets[0].Name:= 'Test sheet 1';
    xf.Workbook.Sheets.Add('Test sheet 2');
    xf.Workbook.Sheets[1].Cells[0, 0].Formula:= '''Test sheet 1''!A1 + 1';
    xf.Workbook.Sheets[1].Cells[1, 0].Formula:= 'SUM(Test sheet 1!A1:A1) + Test sheet 1!A2';

    { Save workbook data to Excel file 'out.xls' }
    xf.SaveAs('out.xls');
  finally
    xf.Destroy;
  end;

  Memo1.Lines.Add(FormatDateTime('dd.mm.yyyy hh:nn:ss.zzz', Now) + ' done.');

  if cbOpenAfterSave.Checked then
    OpenFileInOSShell('out.xls');
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  RunTest;
end;

end.
