SM Software (C), 2000-2009
TXLSFile library v.4.0. and XLSExport components
================================================

What is TXLSFile
----------------
      TXLSFile is a Delphi library for reading and writing MS Excel 
      XLS files. It is based on direct reading and writing of files, 
      and works without OLE Automation with Microsoft Excel. 

      TXLSFile is  distributed  with  XLSExport  components  package. 
      XLSExport is a Delphi components package  for quick data 
      export  into  MS Excel file with  one line  of code.  


How to install TXLSFile 
-----------------------
      See HowToInstall.chm.


Migrate from TXLSFile v.3.0
---------------------------
      Read more about a migration from TXLSFile v.3.0 in migration.txt.


Contact us
----------
      SM Software 
      Technical Support  : support@sm-software.com
      World Wide Web site: http://sm-software.com 

      Feel free to tell your opinion about our software!  If you found 
      that some  useful  features  absent  in  our  product,    please  
      mail us.   
